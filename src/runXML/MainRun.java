package runXML;

import java.util.List;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.collections.Lists;


public class MainRun {

	public static void main(String[] args) {
		
		String testngXML=System.getProperty("user.dir")+"\\testng.xml";

		try {

			TestNG testng = new TestNG();
			TestListenerAdapter listener = new TestListenerAdapter();
			List<String> suites = Lists.newArrayList();
			suites.add(testngXML);// path to xml..
			// suites.add("c:/tests/testng2.xml");
			testng.setTestSuites(suites);
			testng.addListener(listener);
			testng.run();

		} catch (Exception E1) {
			System.out.println("The Main Test Suite File is not been found. Please check the path of the file and Re-Execute");
			System.out.println(E1.getMessage());
			System.out.println(testngXML);
		}

	}

}
