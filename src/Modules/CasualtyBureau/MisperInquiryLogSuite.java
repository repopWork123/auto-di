package Modules.CasualtyBureau;

import java.lang.reflect.Method;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.SeleniumBaseClass;

public class MisperInquiryLogSuite extends SeleniumBaseClass {
	private static final String MISPER_ENQUIRY_URL = "/group/holmes/missing-person-inquiry-log";
	String successMsg;
	String misperInquiryLogURN;

	@BeforeTest
	public void startBrowser() throws Exception {

		loginInIEAndSelectIncident();
	}

	public String getURN(String successMsg) {

		String[] URN = successMsg.split(" ");
		return URN[3];

	}

	@Test(priority = 1)
	public void createMissingPersonInquiryLog_withourAnyData(Method method)
			throws Exception {

		startLine(method);
		navigateToPage(MISPER_ENQUIRY_URL);
		clickAndWait("button_CasualityBureau_Search");
		clickAndWait("icon_CasualityBureau_SearchResult_CreateNew");
		clickAndWait("button_CasualityBureau_Create");
		explicitlyWaitForElement(
				"errorMsg_CasualityBureau_Create_WithOutAnyFields",
				getTotalWaitTimeInSecs());
		verifyTextContains(
				"There is no information on a Misper Inquiry Log record. Please enter at least one piece of information.",
				"errorMsg_CasualityBureau_Create_WithOutAnyFields");
		endLine(method);
	}

	@Test(priority = 2)
	public void createMissingPersonInquiryLog_withMandotoryData(Method method)
			throws Exception {

		startLine(method);
		navigateToPage(MISPER_ENQUIRY_URL);
		clickAndWait("button_CasualityBureau_Search");
		clickAndWait("icon_CasualityBureau_SearchResult_CreateNew");
		sendKeys("textField_CasualityBureau_MisperInquiry_Create_LogText",
				"Idetest Log Text");
		clickAndWait("button_CasualityBureau_Create");
		explicitlyWaitForElement("successMsg_CasualityBereau_Create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_CasualityBereau_Create");
		// Get exhibit URN
		successMsg = getText("successMsg_CasualityBereau_Create");
		misperInquiryLogURN = getURN(successMsg);
		System.out.println(misperInquiryLogURN);
		endLine(method);
	}

	@Test(priority = 3)
	public void searchMissingPersonInquiryLog(Method method) throws Exception {

		startLine(method);
		navigateToPage(MISPER_ENQUIRY_URL);
		sendKeys("textField_CasualityBureau_MisperInquiry_Search_URN",
				misperInquiryLogURN);
		clickAndWait("button_CasualityBureau_Search");
		explicitlyWaitForElement("table_CasualityBureau_SearchResults",
				getTotalWaitTimeInSecs());
		verifyElementPresent("table_CasualityBureau_SearchResults");
		clickAndWait("table_CasualityBureau_SearchResults");
		clickAndWait("icon_CasualityBureau_SearchResult_View");
		explicitlyWaitForElement("text_CasualityBereau_MisperInquiryLog_URN",
				getTotalWaitTimeInSecs());
		verifyTextContains(misperInquiryLogURN,
				"text_CasualityBereau_MisperInquiryLog_URN");
		endLine(method);
	}

	@Test(priority = 4)
	public void updateMissingPersonInquiryLog(Method method) throws Exception {
		startLine(method);
		clickAndWait("button_CasualityBureau_Update");
		sendKeys("textField_CasualityBureau_MisperInquiry_Update_LogText",
				"Idetest Log Text edited");
		clickAndWait("button_CasualityBureau_Update_Save");
		explicitlyWaitForElement("successMsg_CasualityBereau_Create",
				getTotalWaitTimeInSecs());
		verifyTextContains("updated", "successMsg_CasualityBereau_update");
		endLine(method);
	}

	@Test(priority = 5)
	public void deleteMissingPersonInquiryLog(Method method) throws Exception {
		startLine(method);
		clickAndWait("button_CasualityBureau_Delete");
		clickAndWait("button_CasualityBureau_Delete_AreYouSure_Yes");
		sendKeys("textField_Indexes_Delete_Reason",
				"Testing delete funtionality");
		clickAndWait("button_CasualityBureau_Delete_OK");
		explicitlyWaitForElement("successMsg_CasualityBereau_Delete",
				getTotalWaitTimeInSecs());
		verifyTextContains("deleted", "successMsg_CasualityBereau_Delete");
		endLine(method);
	}

}
