package Modules.CasualtyBureau;

import java.lang.reflect.Method;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Modules.Entity.Entity;

public class CasualtyBureau extends Entity{
	
	protected static final String CALLER_URL = "/group/holmes/caller";
	protected static String successMsg;
	protected static String callerURN;
	
	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}
	
	public CasualtyBureau() {
		softAssert = new SoftAssert();
	}
	
	public CasualtyBureau(WebDriver driver) {
		softAssert = new SoftAssert();
		this.driver = driver;
	}
	
	public String createCallerAndGetURN() throws Exception {

		navigateToPage(CALLER_URL);
		clickAndWait("button_CasualityBureau_Search");
		clickAndWait("icon_CasualityBureau_SearchResult_CreateNew");
		sendKeys("textField_CasualityBureau_Caller_Create_Surname",
				"IdeSurName");
		selectByVisibleText("dropDown_CasualityBureau_Create_Sex", "MALE");
		clickAndWait("button_CasualityBureau_Create");
		explicitlyWaitForElement("successMsg_CasualityBereau_Create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_CasualityBereau_Create");
		// Get exhibit URN
		successMsg = getText("successMsg_CasualityBereau_Create");
		String[] URN = successMsg.split(" ");
		return callerURN = URN[1];

	}
 
}
