package Modules.CasualtyBureau;

import java.lang.reflect.Method;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.SeleniumBaseClass;

public class CasualtySuite extends SeleniumBaseClass {
	private static final String CASUALITY_URL = "/group/holmes/casualty";
	String successMsg;
	String casualtyURN;

	@BeforeTest
	public void startBrowser() throws Exception {

		loginInIEAndSelectIncident();
	}

	public String getURN(String successMsg) {

		String[] URN = successMsg.split(" ");
		return URN[1];

	}

	@Test(priority = 1)
	public void createCasualty_withourAnyData(Method method) throws Exception {

		startLine(method);
		navigateToPage(CASUALITY_URL);
		clickAndWait("button_CasualityBureau_Search");
		clickAndWait("icon_CasualityBureau_SearchResult_CreateNew");
		clickAndWait("button_CasualityBureau_Create");
		explicitlyWaitForElement(
				"errorMsg_CasualityBureau_Create_WithOutAnyFields",
				getTotalWaitTimeInSecs());
		verifyTextContains("There are validation errors",
				"errorMsg_CasualityBureau_Create_WithOutAnyFields");
		endLine(method);
	}

	@Test(priority = 2)
	public void createCasualty_withMandotoryData(Method method) throws Exception {

		startLine(method);
		navigateToPage(CASUALITY_URL);
		clickAndWait("button_CasualityBureau_Search");
		clickAndWait("icon_CasualityBureau_SearchResult_CreateNew");
		sendKeys("textField_CasualityBureau_Casualty_Create_Surname",
				"IdeSurName");
		selectByVisibleText("dropDown_CasualityBureau_Create_Sex", "MALE");
		clickAndWait("button_CasualityBureau_Create");
		explicitlyWaitForElement(
				"successMsg_CasualityBereau_Create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_CasualityBereau_Create");
		// Get exhibit URN
		successMsg = getText("successMsg_CasualityBereau_Create");
		casualtyURN = getURN(successMsg);
		endLine(method);
	}

	@Test(priority = 3)
	public void searchCasualty(Method method) throws Exception {
		startLine(method);
		navigateToPage(CASUALITY_URL);
		sendKeys("textField_CasualityBureau_Casualty_Search_URN", casualtyURN);
		clickAndWait("button_CasualityBureau_Search");
		explicitlyWaitForElement(
				"table_CasualityBureau_SearchResults",
				getTotalWaitTimeInSecs());
		verifyElementPresent("table_CasualityBureau_SearchResults");
		clickAndWait("table_CasualityBureau_SearchResults");
		clickAndWait("icon_CasualityBureau_SearchResult_View");
		explicitlyWaitForElement(
				"text_CasualityBereau_Casulty_URN",
				getTotalWaitTimeInSecs());
		verifyTextContains(casualtyURN, "text_CasualityBereau_Casulty_URN");
		endLine(method);
	}

	@Test(priority = 4)
	public void updateCasualty(Method method) throws Exception {
		startLine(method);
		clickAndWait("button_CasualityBureau_Update");
		selectByVisibleText("dropDown_CasualityBureau_Update_Sex", "FEMALE");
		clickAndWait("button_CasualityBureau_Update_Save");
		explicitlyWaitForElement(
				"successMsg_CasualityBereau_update",
				getTotalWaitTimeInSecs());
		verifyTextContains("updated", "successMsg_CasualityBereau_update");
		endLine(method);
	}

	@Test(priority = 5)
	public void deleteCasualty(Method method) throws Exception {
		startLine(method);
		clickAndWait("button_CasualityBureau_Delete");
		clickAndWait("button_CasualityBureau_Delete_AreYouSure_Yes");
		sendKeys("textField_Indexes_Delete_Reason",
				"Testing delete funtionality");
		clickAndWait("button_CasualityBureau_Delete_OK");
		explicitlyWaitForElement(
				"successMsg_CasualityBereau_Delete",
				getTotalWaitTimeInSecs());
		verifyTextContains("deleted", "successMsg_CasualityBereau_Delete");
		endLine(method);
	}

}
