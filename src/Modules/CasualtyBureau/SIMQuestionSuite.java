package Modules.CasualtyBureau;

import java.lang.reflect.Method;

import org.testng.annotations.*;

import ReusableUtilities.SeleniumBaseClass;

public class SIMQuestionSuite extends SeleniumBaseClass {
	private static final String SIM_QUESTION_URL = "/group/holmes/sim-question";
	String successMsg;
	String simURN;

	@BeforeTest
	public void startBrowser() throws Exception {

		loginInIEAndSelectIncident();
	}

	public String getURN(String successMsg) {

		String[] URN = successMsg.split(" ");
		return URN[2];

	}

	@Test(priority = 1)
	public void createSIMQuestion_withourAnyData(Method method)
			throws Exception {

		startLine(method);
		navigateToPage(SIM_QUESTION_URL);
		clickAndWait("button_CasualityBureau_Search");
		clickAndWait("icon_CasualityBureau_SearchResult_CreateNew");
		clickAndWait("button_CasualityBureau_Create");
		explicitlyWaitForElement(
				"errorMsg_CasualityBureau_Create_WithOutAnyFields",
				getTotalWaitTimeInSecs());
		verifyTextContains("Involvement Grading is required",
				"errorMsg_CasualityBureau_Create_WithOutAnyFields");
		endLine(method);
	}

	@Test(priority = 2)
	public void createSIMQuestion_withMandotoryData(Method method)
			throws Exception {

		startLine(method);
		navigateToPage(SIM_QUESTION_URL);
		explicitlyWaitForElement("button_CasualityBureau_Search", 10);
		clickAndWait("button_CasualityBureau_Search");
		clickAndWait("icon_CasualityBureau_SearchResult_CreateNew");
		sendKeys("textField_CasualityBureau_Create_InvolvementReason",
				"Idetest Involvement reason");
		selectTextByIndex("dropDown_CasualityBureau_InvolvementGrading", 2);
		clickAndWait("button_CasualityBureau_Create");
		explicitlyWaitForElement("successMsg_CasualityBereau_Create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_CasualityBereau_Create");
		// Get exhibit URN
		successMsg = getText("successMsg_CasualityBereau_Create");
		simURN = getURN(successMsg);
		System.out.println(simURN);
		endLine(method);
	}

	@Test(priority = 3)
	public void searchSIMQuestion(Method method) throws Exception {

		startLine(method);
		navigateToPage(SIM_QUESTION_URL);
		sendKeys("textField_CasualityBureau_SIM_search_URN", simURN);
		clickAndWait("button_CasualityBureau_Search");
		explicitlyWaitForElement("table_CasualityBureau_SearchResults",
				getTotalWaitTimeInSecs());
		verifyElementPresent("table_CasualityBureau_SearchResults");
		clickAndWait("table_CasualityBureau_SearchResults");
		clickAndWait("icon_CasualityBureau_SearchResult_View");
		explicitlyWaitForElement("text_CasualityBereau_SIM_URN",
				getTotalWaitTimeInSecs());
		verifyTextContains(simURN, "text_CasualityBereau_SIM_URN");
		endLine(method);
	}

	@Test(priority = 4)
	public void updateSIMQuestion(Method method) throws Exception {
		startLine(method);
		clickAndWait("button_CasualityBureau_Update");
		sendKeys("textField_CasualityBureau_SIM_Update_InvolvementReason",
				"Idetest Involvement reason edited");
		clickAndWait("button_CasualityBureau_Update_Save");
		explicitlyWaitForElement("successMsg_CasualityBereau_Create",
				getTotalWaitTimeInSecs());
		verifyTextContains("updated", "successMsg_CasualityBereau_update");
		endLine(method);
	}

	@Test(priority = 5)
	public void deleteSIMQuestion(Method method) throws Exception {
		startLine(method);
		clickAndWait("button_CasualityBureau_Delete");
		clickAndWait("button_CasualityBureau_Delete_AreYouSure_Yes");
		sendKeys("textField_Indexes_Delete_Reason",
				"Testing delete funtionality");
		clickAndWait("button_CasualityBureau_Delete_OK");
		explicitlyWaitForElement("successMsg_CasualityBereau_Delete",
				getTotalWaitTimeInSecs());
		verifyTextContains("deleted", "successMsg_CasualityBereau_Delete");
		endLine(method);
	}

}
