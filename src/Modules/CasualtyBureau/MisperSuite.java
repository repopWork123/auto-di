package Modules.CasualtyBureau;

import java.lang.reflect.Method;

import org.testng.annotations.*;

import ReusableUtilities.SeleniumBaseClass;

public class MisperSuite extends SeleniumBaseClass {
	private static final String MISPER_URL = "/group/holmes/missing-person";

	String successMsg;
	String misperURN;

	@BeforeTest
	public void startBrowser() throws Exception {

		loginInIEAndSelectIncident();
	}

	public String getURN(String successMsg) {

		String[] URN = successMsg.split(" ");
		return URN[1];

	}

	@Test(priority = 1)
	public void createMisper_withourAnyData(Method method) throws Exception {

		startLine(method);
		navigateToPage(MISPER_URL);
		clickAndWait("button_CasualityBureau_Search");
		clickAndWait("icon_CasualityBureau_SearchResult_CreateNew");
		clickAndWait("button_CasualityBureau_Create");
		explicitlyWaitForElement(
				"errorMsg_CasualityBureau_Create_WithOutAnyFields",
				getTotalWaitTimeInSecs());
		verifyTextContains("Involvement Grading is required",
				"errorMsg_CasualityBureau_Create_WithOutAnyFields");
		endLine(method);
	}

	@Test(priority = 2)
	public void createMisper_withMandotoryData(Method method) throws Exception {

		startLine(method);
		navigateToPage(MISPER_URL);
		clickAndWait("button_CasualityBureau_Search");
		clickAndWait("icon_CasualityBureau_SearchResult_CreateNew");
		sendKeys("textField_CasualityBureau_Caller_Create_Surname",
				"IdeSurName");
		selectByVisibleText("dropDown_CasualityBureau_Create_Sex", "MALE");
		sendKeys("textField_CasualityBureau_Create_InvolvementReason",
				"Idetest Involvement reason");
		selectTextByIndex("dropDown_CasualityBureau_InvolvementGrading", 2);
		clickAndWait("button_CasualityBureau_Create");
		explicitlyWaitForElement(
				"successMsg_CasualityBereau_Create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_CasualityBereau_Create");
		// Get exhibit URN
		successMsg = getText("successMsg_CasualityBereau_Create");
		misperURN = getURN(successMsg);
		System.out.println(misperURN);
		endLine(method);
	}

	@Test(priority = 3)
	public void searchMisper(Method method) throws Exception {

		startLine(method);
		navigateToPage(MISPER_URL);
		sendKeys("textField_CasualityBureau_Misper_Search_URN", misperURN);
		clickAndWait("button_CasualityBureau_Search");
		explicitlyWaitForElement(
				"table_CasualityBureau_SearchResults",
				getTotalWaitTimeInSecs());
		verifyElementPresent("table_CasualityBureau_SearchResults");
		clickAndWait("table_CasualityBureau_SearchResults");
		clickAndWait("icon_CasualityBureau_SearchResult_View");
		explicitlyWaitForElement(
				"text_CasualityBereau_Misper_URN",
				getTotalWaitTimeInSecs());
		verifyTextContains(misperURN, "text_CasualityBereau_Misper_URN");
		endLine(method);
	}

	@Test(priority = 4)
	public void updateMisper(Method method) throws Exception {
		startLine(method);
		clickAndWait("button_CasualityBureau_Update");
		selectByVisibleText("dropDown_CasualityBureau_Update_Sex", "FEMALE");
		clickAndWait("button_CasualityBureau_Update_Save");
		explicitlyWaitForElement(
				"successMsg_CasualityBereau_Create",
				getTotalWaitTimeInSecs());
		verifyTextContains("updated", "successMsg_CasualityBereau_update");
		endLine(method);
	}

	@Test(priority = 5)
	public void deleteMisper(Method method) throws Exception {
		startLine(method);
		click("button_CasualityBureau_Delete");
		click("button_CasualityBureau_Delete_AreYouSure_Yes");
		sendKeys("textField_Indexes_Delete_Reason",
				"Testing delete funtionality");
		click("button_CasualityBureau_Delete_OK");
		explicitlyWaitForElement(
				"successMsg_CasualityBereau_Delete",
				getTotalWaitTimeInSecs());
		verifyTextContains("deleted", "successMsg_CasualityBereau_Delete");
		endLine(method);
	}

}
