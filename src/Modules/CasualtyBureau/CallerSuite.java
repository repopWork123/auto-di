package Modules.CasualtyBureau;

import java.lang.reflect.Method;

import org.testng.annotations.*;

import ReusableUtilities.SeleniumBaseClass;

public class CallerSuite extends CasualtyBureau {
	/*private static final String CALLER_URL = "/group/holmes/caller";
	String successMsg;
	String callerURN;*/

	@BeforeTest
	public void startBrowser() throws Exception {

		loginInIEAndSelectIncident();
	}

	public String getURN(String successMsg) {

		String[] URN = successMsg.split(" ");
		return URN[1];

	}

	@Test(priority = 1)
	public void createCaller_withourAnyData(Method method) throws Exception {

		startLine(method);
		navigateToPage(CALLER_URL);
		clickAndWait("button_CasualityBureau_Search");
		clickAndWait("icon_CasualityBureau_SearchResult_CreateNew");
		clickAndWait("button_CasualityBureau_Create");
		explicitlyWaitForElement(
				"errorMsg_CasualityBureau_Create_WithOutAnyFields",
				getTotalWaitTimeInSecs());
		verifyTextContains("There are validation errors",
				"errorMsg_CasualityBureau_Create_WithOutAnyFields");
		endLine(method);
	}

	@Test(priority = 2)
	public void createCaller_withMandotoryData(Method method) throws Exception {

		startLine(method);
		navigateToPage(CALLER_URL);
		clickAndWait("button_CasualityBureau_Search");
		clickAndWait("icon_CasualityBureau_SearchResult_CreateNew");
		sendKeys("textField_CasualityBureau_Caller_Create_Surname",
				"IdeSurName");
		selectByVisibleText("dropDown_CasualityBureau_Create_Sex", "MALE");
		clickAndWait("button_CasualityBureau_Create");
		explicitlyWaitForElement("successMsg_CasualityBereau_Create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_CasualityBereau_Create");
		// Get exhibit URN
		successMsg = getText("successMsg_CasualityBereau_Create");
		callerURN = getURN(successMsg);
		endLine(method);

	}

	@Test(priority = 3)
	public void searchCaller(Method method) throws Exception {
		startLine(method);
		navigateToPage(CALLER_URL);
		sendKeys("textField_CasualityBureau_Caller_Search_URN", callerURN);
		clickAndWait("button_CasualityBureau_Search");
		explicitlyWaitForElement("table_CasualityBureau_SearchResults",
				getTotalWaitTimeInSecs());
		verifyElementPresent("table_CasualityBureau_SearchResults");
		clickAndWait("table_CasualityBureau_SearchResults");
		clickAndWait("icon_CasualityBureau_SearchResult_View");
		explicitlyWaitForElement("text_CasualityBereau_Caller_URN",
				getTotalWaitTimeInSecs());
		verifyTextContains(callerURN, "text_CasualityBereau_Caller_URN");
		endLine(method);
	}

	@Test(priority = 6)
	public void searchResultsScreenCaller(Method method) throws Exception{
		startLine(method);
		navigateToPage(CALLER_URL);
		sendKeys("textField_CasualityBureau_Caller_Search_URN", callerURN);
		clickAndWait("button_CasualityBureau_Search"); // click on search
		explicitlyWaitForElement("table_CasualityBureau_SearchResults", getTotalWaitTimeInSecs()); //search Res Table 
		verifyElementPresent("table_CasualityBureau_SearchResults"); 
		clickAndWait("table_CasualityBureau_SearchResults"); //
		clickAndWait("//td[@title='Create new entity']");
		clickAndWait("//div[@id='softEntityContainer']/ul/li[2]/a/span"); // Click Search Res Tab
		clickAndWait("icon_CasualityBureau_SearchResult_View"); // 
		clickAndWait("//div[@id='softEntityContainer']/ul/li[2]/a/span"); // Click Search Res Tab
		clickAndWait("//td[@id='searchResultsPager_left']/table/tbody/tr/td[3]/div/span");
		clickAndWait("");
	
	}
	
	
	@Test(priority = 4)
	public void updateCaller(Method method) throws Exception {
		startLine(method);
		clickAndWait("button_CasualityBureau_Update");
		selectByVisibleText("dropDown_CasualityBureau_Update_Sex", "FEMALE");
		clickAndWait("button_CasualityBureau_Update_Save");
		explicitlyWaitForElement("successMsg_CasualityBereau_Create",
				getTotalWaitTimeInSecs());
		verifyTextContains("updated", "successMsg_CasualityBereau_update");
		endLine(method);

	}

	@Test(priority = 5)
	public void deleteCaller(Method method) throws Exception {
		startLine(method);
		clickAndWait("button_CasualityBureau_Delete");
		clickAndWait("button_CasualityBureau_Delete_AreYouSure_Yes");
		sendKeys("textField_Indexes_Delete_Reason",
				"Testing delete funtionality");
		clickAndWait("button_CasualityBureau_Delete_OK");
		explicitlyWaitForElement("successMsg_CasualityBereau_Delete",
				getTotalWaitTimeInSecs());
		verifyTextContains("deleted", "successMsg_CasualityBereau_Delete");
		endLine(method);
	}
	
	
}
