package Modules.CasualtyBureau;

import java.lang.reflect.Method;

import org.testng.annotations.*;

import ReusableUtilities.SeleniumBaseClass;

public class MisperGroupSuite extends SeleniumBaseClass {
	private static final String MISPER_GROUP_URL = "/group/holmes/missing-person-group";
	String successMsg;
	String misperGroupURN;

	@BeforeTest
	public void startBrowser() throws Exception {

		loginInIEAndSelectIncident();
	}

	public String getURN(String successMsg) {

		String[] URN = successMsg.split(" ");
		return URN[2];

	}

	@Test(priority = 1)
	public void createMisperGroup_withourAnyData(Method method)
			throws Exception {

		startLine(method);
		navigateToPage(MISPER_GROUP_URL);
		clickAndWait("button_CasualityBureau_Search");
		clickAndWait("icon_CasualityBureau_SearchResult_CreateNew");
		clickAndWait("button_CasualityBureau_Create");
		explicitlyWaitForElement(
				"errorMsg_CasualityBureau_Create_WithOutAnyFields",
				getTotalWaitTimeInSecs());
		verifyTextContains(
				"There is no information on a Misper Group record. Please enter at least one piece of information.",
				"errorMsg_CasualityBureau_Create_WithOutAnyFields");
		endLine(method);
	}

	@Test(priority = 2)
	public void createMisperGroup_withMandotoryData(Method method)
			throws Exception {

		startLine(method);
		navigateToPage(MISPER_GROUP_URL);
		clickAndWait("button_CasualityBureau_Search");
		clickAndWait("icon_CasualityBureau_SearchResult_CreateNew");
		sendKeys("textField_CasualityBureau_MisperGroup_Create_NameOfGroup",
				"Idetest NameOfGroup");
		sendKeys(
				"textField_CasualityBureau_MisperGroup_Create_DescriptionOfGroup",
				"Idetest DescriptionOfGroup");
		clickAndWait("button_CasualityBureau_Create");
		explicitlyWaitForElement("successMsg_CasualityBereau_Create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_CasualityBereau_Create");
		// Get exhibit URN
		successMsg = getText("successMsg_CasualityBereau_Create");
		misperGroupURN = getURN(successMsg);
		System.out.println(misperGroupURN);
		endLine(method);
	}

	@Test(priority = 3)
	public void searchMisperGroup(Method method) throws Exception {

		startLine(method);
		navigateToPage(MISPER_GROUP_URL);
		sendKeys("textField_CasualityBureau_MisperGroup_Search_URN",
				misperGroupURN);
		clickAndWait("button_CasualityBureau_Search");
		explicitlyWaitForElement("table_CasualityBureau_SearchResults",
				getTotalWaitTimeInSecs());
		verifyElementPresent("table_CasualityBureau_SearchResults");
		clickAndWait("table_CasualityBureau_SearchResults");
		clickAndWait("icon_CasualityBureau_SearchResult_View");
		explicitlyWaitForElement("text_CasualityBereau_MisperGroup_URN",
				getTotalWaitTimeInSecs());
		verifyTextContains(misperGroupURN,
				"text_CasualityBereau_MisperGroup_URN");
		endLine(method);
	}

	@Test(priority = 4)
	public void updateMisperGroup(Method method) throws Exception {
		startLine(method);
		clickAndWait("button_CasualityBureau_Update");
		sendKeys(
				"textField_CasualityBureau_MisperGroup_Update_DescriptionOfGroup",
				"Idetest DescriptionOfGroup edited");
		clickAndWait("button_CasualityBureau_Update_Save");
		explicitlyWaitForElement("successMsg_CasualityBereau_Create",
				getTotalWaitTimeInSecs());
		verifyTextContains("updated", "successMsg_CasualityBereau_update");
		endLine(method);
	}

	@Test(priority = 5)
	public void deleteMisperGroup(Method method) throws Exception {
		startLine(method);
		clickAndWait("button_CasualityBureau_Delete");
		clickAndWait("button_CasualityBureau_Delete_AreYouSure_Yes");
		sendKeys("textField_Indexes_Delete_Reason",
				"Testing delete funtionality");
		clickAndWait("button_CasualityBureau_Delete_OK");
		explicitlyWaitForElement("successMsg_CasualityBereau_Delete",
				getTotalWaitTimeInSecs());
		verifyTextContains("deleted", "successMsg_CasualityBereau_Delete");
		endLine(method);
	}

}
