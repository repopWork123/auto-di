package Modules.Entity;

import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import ReusableUtilities.Report;
import ReusableUtilities.SeleniumBaseClass;

public class Entity extends SeleniumBaseClass{
	
	private static final String LIST_MAINTENANCE_WINDOW_TITLE = "List Maintenance - Liferay";
	private static final String NEW_OR_EXISTING_LIST_DIALOG_CONTENT = "List Management has been opened with the selected records";
	private static final String DEFAULT_LIST_DIALOG_CONTENT = "The selected records have been added to your Default list";
	private static final String FAST_ACTION_WINDOW_TITLE = "Fast Action";
	private static final String VISUALISATION_PORTLET_WINDOW_TITLE = "Visualisation Portlet";
	
	protected static final String PRIORITY_LOW = "Low";
	protected static final String PRIORITY_HIGH = "High";
	protected static final String PRIORITY_MEDIUM= "Medium";
	
	protected static final String PROTECTIVE_MARKING_SECRET = "SECRET";
	protected static final String PROTECTIVE_MARKING_RESTRICTED = "RESTRICTED";
	protected static final String PROTECTIVE_MARKING_CONFIDENTIAL = "CONFIDENTIAL";
	
	protected static final String SEARCH_RESULT_LIMIT__1_000_RECORDS_POPUP = "The search result limit has been exceeded so only the first 1,000 records have been displayed";

	
	
	public Entity() {
		softAssert = new SoftAssert();
	}
	
	public Entity(WebDriver driver) {
		softAssert = new SoftAssert();
		this.driver = driver;
	}
	
	public void viewHistoryTabSearchResult(String actvityDescription)
			throws Exception {
		
		
		click("icon_Action_HistoryTab_View");
		explicitlyWaitForElement("header_Action_HistoryTab_view",
 				getTotalWaitTimeInSecs());
		verifyText(usernameUpperCase,"text_Action_ViewHistoryRecord_Username");
		verifyTextContains(actvityDescription,"text_Action_ViewHistoryRecord_Activity");
		click("button_Action_ViewHistoryRecord_Close");

	}
	
	public void clickOnViewInNewWindowIcon(String viewInNewWinIconIdenticationInfo, String childWindowTitle)
			throws Exception {
		String childWindowHandle1;
		childWindowHandle1 = getCurrentWindowHandle();
		Report.info("Child window handle before invoking another child fore view in new Window: "
				+ childWindowHandle1);
		Set<String> parentWindowHandlers = driver.getWindowHandles();
		int winCount = getWindowCount();
		WebDriver childWindowHandle2 = clickAndWaitForChildWindow(
				childWindowTitle, parentWindowHandlers,
				viewInNewWinIconIdenticationInfo, winCount, winCount+1);
		explicitlyWaitAndGetWebElements("label_Action_View_URN",
				getTotalWaitTimeInSecs());
		/*verifyText(fastActionURN_allFieldValues, "label_Action_View_URN");
		// Verify currently incident id is selected
		verifyTextContains(incidentIdFull, "label_Action_View_Page_Title");
		// View Fast Action " Incident -URN" in Incident -URN Title"
		verifyTextContains(incidentIdFull + "-" + fastActionURN_allFieldValues,
				"label_Action_View_Page_Title");
		// Verify Title and Priority field value matches user input.
		// Validate inserted title in create page same as in view page
		verifyText(ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE,
				"label_Action_View_Title");*/
		// Validate Priority : Low is set as default value
		verifyText(PRIORITY_MEDIUM, "label_Action_Mirsap_View_Priority");
		// closePopupWindowAndNavigateToParentWindow(FAST_ACTION_WINDOW_TITLE,
		// FAST_ACTION_WINDOW_TITLE);
		int winCount1 = getWindowCount();
		WebDriver childWindowHandleAfterViewInNewWindow = closechildWindowAndNavigateToItsParentWindowHandle(
				childWindowHandle2, childWindowHandle1, winCount1, winCount1-1);
		Report.info("Child window handle after done closing view in New Window: "
				+ childWindowHandleAfterViewInNewWindow.getWindowHandle()
				+ " Expected is: " + childWindowHandle1);
	}


	public void addToNewOrExistingListIcon(String newOrExistListIconIdenticationInfo)
			throws Exception {
		String childWindowHandle1;
		explicitlyWaitForElement(newOrExistListIconIdenticationInfo,
				getTotalWaitTimeInSecs());
		
		click(newOrExistListIconIdenticationInfo);
		childWindowHandle1 = getCurrentWindowHandle();
		Report.info("Child window handle before invoking another child: " + childWindowHandle1);
		verifyText(NEW_OR_EXISTING_LIST_DIALOG_CONTENT,	"text_Action_View_NewOrExistingListDialogboxContent");
		Set<String> parentWindowHandler1 = driver.getWindowHandles();
		int winCount = getWindowCount();
		WebDriver listManagementWinHandle = clickAndWaitForChildWindow(
				LIST_MAINTENANCE_WINDOW_TITLE, parentWindowHandler1,
				"button_Action_View_VisualizationDialogboxOk", winCount, winCount+1);
		Report.info("Child window handle before closing another child: "
				+ listManagementWinHandle.getWindowHandle());
		int winCount1 = getWindowCount();
		WebDriver childWindowHandleAfterListMangementPopupClosed = closechildWindowAndNavigateToItsParentWindowHandle(
				listManagementWinHandle, childWindowHandle1, winCount1, winCount1-1);
		Report.info("Child window handle after done closing another child: "
				+ childWindowHandleAfterListMangementPopupClosed
						.getWindowHandle() + " Expected is: "
				+ childWindowHandle1);
	}


	public void clickOnAddToDefaultListIcon(String defaultListIconIdenticationInfo) throws Exception {
		explicitlyWaitForElement(defaultListIconIdenticationInfo,
				getTotalWaitTimeInSecs());
		WebElement iconDefaultList = explicitlyWaitAndGetWebElement(defaultListIconIdenticationInfo, getTotalWaitTimeInSecs());
		clickElementUsingJS(iconDefaultList);
		explicitlyWaitForElement("text_Action_View_DefaultListDialogboxContent", getTotalWaitTimeInSecs());
		verifyTextContains("list",
				"text_Action_View_DefaultListDialogboxContent");
		click("button_Action_View_DefaultListDialogboxOk");
	}


	public void clickOnAddToFavListIcon(String favListIconIdenticationInfo) throws Exception {
		String addToFavListDialogContent = "record(s) added successfully";
		explicitlyWaitForElement(favListIconIdenticationInfo,
				getTotalWaitTimeInSecs());
		click(favListIconIdenticationInfo);
		explicitlyWaitForElement("text_Action_View_FavListDialogboxContent",
				getTotalWaitTimeInSecs());
		verifyTextContains("list",
				"text_Action_View_FavListDialogboxContent");
		click("button_Action_View_FavListDialogboxOk");
	}


	public void clickOnAddToVisualizationIcon(String visualizationIconIdenticationInfo) throws Exception {
		String childWindowHandle1 = getCurrentWindowHandle();
		Report.info("Child window handle before invoking another child: "
				+ childWindowHandle1);

		String addToVisualizationDialogContent = "record(s) added successfully";
		/*String addToVisualizationDialogContent = "Selected record(s) added successfully to Visualiser\n"
				+ incidentIdFull + "-" + fastActionURN_allFieldValues;*/
		explicitlyWaitForElement(visualizationIconIdenticationInfo, getTotalWaitTimeInSecs());
		click("icon_Action_View_Visualization");
		explicitlyWaitForElement(
				"text_Action_View_VisualizationDialogboxContent",
				getTotalWaitTimeInSecs());
		verifyText(addToVisualizationDialogContent,
				"text_Action_View_VisualizationDialogboxContent");
		int noOfWinCount = getWindowCount();
		WebDriver visualizationWinWebDriver = clickAndWaitForBrowserPopup(VISUALISATION_PORTLET_WINDOW_TITLE,
				"button_Action_View_VisualizationDialogboxOk", noOfWinCount, noOfWinCount+1);
		
		driver = closechildWindowAndNavigateToItsParentWindowHandle(visualizationWinWebDriver, childWindowHandle1, 3, 2);
	}

}
