package Modules.Indexes;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class Exhibits extends Indexes {
	
	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}
	
	public Exhibits() {
		softAssert = new SoftAssert();
	}
	
	public Exhibits(WebDriver driver) {
		softAssert = new SoftAssert();
		this.driver = driver;
	}
}
