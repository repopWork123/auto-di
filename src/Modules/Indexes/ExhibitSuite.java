package Modules.Indexes;

import java.lang.reflect.Method;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.Report;
import ReusableUtilities.SeleniumBaseClass;

public class ExhibitSuite extends SeleniumBaseClass {
	String INDEXES_EXHIBITS_URL = "/group/holmes/enquiry";

	String successMsg;
	String exhibitURN;
	String exhibitURN_Similar;

	 @BeforeTest
	public void startBrowser() throws Exception {

		loginInIEAndSelectIncident();
	}

	public String getURN(String successMsg) {

		String[] URN = successMsg.split(" ");
		return URN[1];

	}

	@Test(priority = 1)
	public void createExhibit_WithoutAnyData(Method method) throws Exception {
		startLine(method);
		navigateToPage("INDEXES_EXHIBITS_URL");
		clickAndWait("button_Indexes_Search");
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("errorMsg_Indexes_Create_WithOutAnyFields",
				getTotalWaitTimeInSecs());
		verifyTextContains("There are validation errors",
				"errorMsg_Indexes_Create_WithOutAnyFields");
		verifyTextContains("Label Description is required",
				"errorMsg_Indexes_Exhibits_LabelDescription");
		endLine(method);
	}

	@Test(priority = 2)
	public void createExhibit_WithRequiredData(Method method) throws Exception {

		startLine(method);
		navigateToPage("INDEXES_EXHIBITS_URL");
		clickAndWait("button_Indexes_Search");
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		explicitlyWaitForElement("textField_Indexes_Exhibits_LabelDescription",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Indexes_Exhibits_LabelDescription",
				"IdeTestExhibitLabelDes");
		selectByVisibleText("dropDown_Indexes_Exhibits_Hazardous", "NO");
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("successMsg_Indexes_create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_Indexes_create");
		// Get exhibit URN
		successMsg = getText("successMsg_Indexes_create");
		exhibitURN = getURN(successMsg);
		System.out.println(exhibitURN);
		endLine(method);
	}

	@Test(priority = 3)
	public void createExhibit_WithSimilar(Method method) throws Exception {

		startLine(method);
		navigateToPage("INDEXES_CATEGORIES_URL");
		clickAndWait("button_Indexes_Search");
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		explicitlyWaitForElement("textField_Indexes_Exhibits_LabelDescription",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Indexes_Exhibits_LabelDescription",
				"IdeTestExhibitLabelDes");
		selectByVisibleText("dropDown_Indexes_Exhibits_Hazardous", "NO");
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("successMsg_Indexes_create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_Indexes_create");
		// Create Exhibit by clicking similar button
		clickAndWait("button_Indexes_Similar");
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("successMsg_Indexes_create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_Indexes_create");
		// Get exhibit URN
		successMsg = getText("successMsg_Indexes_create");
		exhibitURN_Similar = getURN(successMsg);
		System.out.println(exhibitURN_Similar);
		endLine(method);
	}

	@Test(priority = 4)
	public void searchViewAndUpdate_Exhibit(Method method) throws Exception {

		startLine(method);
		navigateToPage("INDEXES_CATEGORIES_URL");
		explicitlyWaitForElement("textField_Indexes_Exhibits_URN",
				getTotalWaitTimeInSecs());
		sendKeys("textField_ExhibitURN", exhibitURN);
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("table_Indexes_SearchResults",
				getTotalWaitTimeInSecs());
		verifyElementPresent("table_Indexes_SearchResults");
		clickAndWait("table_Indexes_SearchResults");
		clickAndWait("icon_Indexes_SearchResult_View");
		explicitlyWaitForElement("text_Indexes_Exhibit_URN",
				getTotalWaitTimeInSecs());
		verifyTextContains(exhibitURN, "text_Indexes_Exhibit_URN");
		endLine(method);
	}

}
