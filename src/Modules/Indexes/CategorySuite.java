package Modules.Indexes;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;

import org.testng.annotations.*;

import ReusableUtilities.SeleniumBaseClass;

public class CategorySuite extends SeleniumBaseClass {
	java.util.Date d = new java.util.Date();
	String CATEGORY_NAME = new SimpleDateFormat("ddhhmmss").format(d.getTime());
	String INDEXES_CATEGORIES_URL = "/group/holmes/categories";

	// String catName="Org00010";
	String successMsg;
	String categoryURN;

	@BeforeTest
	public void startBrowser() throws Exception {

		// loginInIEAndSelectIncident();
		loginAndSelectIncident();
	}

	public String getURN(String successMsg) {

		String[] URN = successMsg.split(" ");
		return URN[1];

	}

	
	@Test(priority = 1)
	public void createCategory_withoutAnyData(Method method) throws Exception {
		try{
		startLine(method);
		navigateToPage(INDEXES_CATEGORIES_URL);
		explicitlyWaitForElement("button_Indexes_Search", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Indexes_Search");
		clickAndWait("button_Indexes_Search");
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("errorMsg_Indexes_Create_WithOutAnyFields", getTotalWaitTimeInSecs());
		verifyElementPresent("errorMsg_Indexes_Create_WithOutAnyFields");
		endLine(method);
		}catch(Exception e){
			e.printStackTrace();
			
		}

	}

	@Test(priority = 2)
	public void createCategory_withMandotoryData(Method method)
			throws Exception {
		startLine(method);
		navigateToPage(INDEXES_CATEGORIES_URL);
		explicitlyWaitForElement("button_Indexes_Search", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Indexes_Search");
		clickAndWait("button_Indexes_Search");
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		sendKeys("textField_Indexes_Category_Name", CATEGORY_NAME);
		click("button_Indexes_Create");
		explicitlyWaitForElement("successMsg_Indexes_create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Indexes_create");
		// Get exhibit URN
		successMsg = getText("successMsg_Indexes_create");
		categoryURN = getURN(successMsg);
		System.out.println(categoryURN);
		endLine(method);
	}

	@Test(priority = 3)
	public void searchCategory(Method method) throws Exception {
		startLine(method);
		navigateToPage(INDEXES_CATEGORIES_URL);
		explicitlyWaitForElement("textField_Indexes_Category_Name", getTotalWaitTimeInSecs());
		verifyElementPresent("textField_Indexes_Category_Name");
		sendKeys("textField_Indexes_Category_Name", CATEGORY_NAME);
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("table_Indexes_SearchResults", getTotalWaitTimeInSecs());
		verifyElementPresent("table_Indexes_SearchResults");
		click("table_Indexes_SearchResults");
		clickAndWait("icon_Indexes_SearchResult_View");
		explicitlyWaitForElement("text_Indexes_Category_Name", getTotalWaitTimeInSecs());
		verifyElementPresent("text_Indexes_Category_Name");
		endLine(method);

	}

	@Test(priority = 4)
	public void updateCategory(Method method) throws Exception {
		startLine(method);
		explicitlyWaitForElement("button_Indexes_Update", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Indexes_Update");
		click("button_Indexes_Update");
		explicitlyWaitForElement("textField_Indexes_Category_Update_OtherInformation", getTotalWaitTimeInSecs());
		verifyElementPresent("textField_Indexes_Category_Update_OtherInformation");
		sendKeys("textField_Indexes_Category_Update_OtherInformation",
				"editedIDEOtherInfo");
		clickAndWait("button_Indexes_Update_Save");
		explicitlyWaitForElement("successMsg_Indexes_create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Indexes_create");
		endLine(method);
	}

}
