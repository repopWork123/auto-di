package Modules.Indexes;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class Vehicle extends Indexes {
	
	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}
	
	public Vehicle() {
		softAssert = new SoftAssert();
	}
	
	public Vehicle(WebDriver driver) {
		softAssert = new SoftAssert();
		this.driver = driver;
	}
	
	public void createVehicle() throws Exception {

		navigateToPage(INDEXES_VEHICLE_URL);
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("icon_Indexes_SearchResult_CreateNew",getTotalWaitTimeInSecs());
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		sendKeys("textField_Indexes_Vehicle_Create_VRM", "Vehicle");
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("successMsg_Indexes_create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_Indexes_create");
	
	}

}
