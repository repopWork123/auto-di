package Modules.Indexes;

import java.lang.reflect.Method;

import org.testng.annotations.*;

import ReusableUtilities.SeleniumBaseClass;

public class NominalSuite extends SeleniumBaseClass{
	String INDEXES_NOMINAL_URL = "/group/di/nominals";
	
	String successMsg;
	String nominalURN;
	String exhibitURN_Similar;

	@BeforeTest
	public void startBrowser() throws Exception {

		loginInIEAndSelectIncident();
	}

	/*//@AfterTest
	public void signOut() throws Exception
	{
			Click("link_Home");
			explicitlyWaitForElement("icon_signOut_downArrow", 10);
			Click("icon_signOut_downArrow");
			Click("link_Home_SignOut");
			explicitlyWaitForElement("textField_Login_UserName", 10);
	}*/
	
	public String getURN(String successMsg){
	
		String[] URN = successMsg.split(" ");
		return URN[1];
		
	}

	@Test(priority=1)
	public void createNominal_withoutAnyData(Method method) throws Exception {
		startLine(method);
		navigateToPage(INDEXES_NOMINAL_URL);
		clickAndWait("button_Indexes_Search_Reason");
		explicitlyWaitForElement("button_Indexes_Search_Reason_CreateDefaultList",
				getTotalWaitTimeInSecs());
		clickAndWait("button_Indexes_Search_Reason_CreateDefaultList");
		explicitlyWaitForElement("button_Indexes_Search",
				getTotalWaitTimeInSecs());
		clickAndWait("button_Indexes_Search");
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("errorMsg_Indexes_Create_WithOutAnyFields",
				getTotalWaitTimeInSecs());
		verifyTextContains("There are validation errors",
				"errorMsg_Indexes_Create_WithOutAnyFields");
		endLine(method);
	}
	
	@Test(priority=2)
	public void createNominal_withOnly_MandotoryData(Method method) throws Exception {

		startLine(method);
		navigateToPage(INDEXES_NOMINAL_URL);
		clickAndWait("button_Indexes_Search");
		clickAndWait("icon_Indexes_SearchResult_CreateNew");		
		explicitlyWaitForElement("textField_Indexes_Nominal_Surname", 10);
		sendKeys("textField_Indexes_Nominal_Surname", "IdeTestNomSurname");
		selectByVisibleText("dropDown_Indexes_Create_Sex", "MALE");
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("successMsg_Indexes_create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_Indexes_create");
		//Get exhibit URN
		successMsg=getText("successMsg_Indexes_create");
		nominalURN=getURN(successMsg);
		System.out.println(nominalURN);
		endLine(method);
	}
	
	
	@Test(priority=3)
	public void searchViewAnd_updateNominal(Method method) throws Exception {

		startLine(method);
		navigateToPage(INDEXES_NOMINAL_URL);
		explicitlyWaitForElement("textField_Indexes_Nominal_URN", 10);
		sendKeys("textField_Indexes_Nominal_URN", nominalURN);
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("table_Indexes_SearchResults",
				getTotalWaitTimeInSecs());
		verifyElementPresent("table_Indexes_SearchResults");
		clickAndWait("table_Indexes_SearchResults");
		clickAndWait("icon_Indexes_SearchResult_View");
		explicitlyWaitForElement("text_Indexes_Exhibit_URN", 10);
		verifyTextContains(nominalURN,
				"text_Indexes_Exhibit_URN");
		endLine(method);
		
	}

}
