package Modules.Indexes;

import java.lang.reflect.Method;

import org.testng.annotations.*;

import ReusableUtilities.SeleniumBaseClass;

public class LocationSuite extends SeleniumBaseClass {
	String INDEXES_LOCATION_URL = "/group/holmes/location";
	String COUNTRY = "India";
	String successMsg;
	String locationURN;

	 @BeforeTest
	public void startBrowser() throws Exception {

		loginInIEAndSelectIncident();
	}

	public String getURN(String successMsg) {

		String[] URN = successMsg.split(" ");
		return URN[1];

	}

	@Test(priority = 1)
	public void createLocation_withoutAnyStreetDetails(Method method)
			throws Exception {

		startLine(method);
		navigateToPage(INDEXES_LOCATION_URL);
		clickAndWait("button_Indexes_Search");
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		explicitlyWaitForElement("dropDown_Indexes_Location_Type",
				getTotalWaitTimeInSecs());
		selectTextByValue("dropDown_Indexes_Location_Type", "S");
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("errorMsg_Indexes_Create_WithOutAnyFields",
				getTotalWaitTimeInSecs());
		verifyTextContains("Please enter a Related Street or Street details",
				"errorMsg_Indexes_Create_WithOutAnyFields");
		endLine(method);
	}

	@Test(priority = 2)
	public void createLocation_withoutAnyHouseDetails(Method method) throws Exception {

		startLine(method);
		navigateToPage(INDEXES_LOCATION_URL);
		clickAndWait("button_Indexes_Search");
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		explicitlyWaitForElement("dropDown_Indexes_Location_Type", 10);
		selectTextByValue("dropDown_Indexes_Location_Type", "H");
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("errorMsg_Indexes_Create_WithOutAnyFields",
				getTotalWaitTimeInSecs());
		verifyTextContains("Please enter a Related Street or Street details",
				"errorMsg_Indexes_Create_WithOutAnyFields");
		endLine(method);
	}

	@Test(priority = 3)
	public void createLocation_withOnlyStreetDetails(Method method) throws Exception {

		startLine(method);
		navigateToPage(INDEXES_LOCATION_URL);
		clickAndWait("button_Indexes_Search");
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		explicitlyWaitForElement("dropDown_Indexes_Location_Type", 10);
		selectTextByValue("dropDown_Indexes_Location_Type", "S");
		sendKeys("textField_Indexes_Location_Country", COUNTRY);
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("successMsg_Indexes_create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_Indexes_create");
		// Get exhibit URN
		successMsg = getText("successMsg_Indexes_create");
		locationURN = getURN(successMsg);
		System.out.println(locationURN);
		endLine(method);
	}

	@Test(priority = 4)
	public void searchLocation(Method method) throws Exception {

		startLine(method);
		navigateToPage(INDEXES_LOCATION_URL);
		sendKeys("textField_Indexes_Location_Search_Urn", locationURN);
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("table_Indexes_SearchResults",
				getTotalWaitTimeInSecs());
		verifyElementPresent("table_Indexes_SearchResults");
		clickAndWait("table_Indexes_SearchResults");
		clickAndWait("icon_Indexes_SearchResult_View");
		explicitlyWaitForElement("text_Indexes_Location_URN",
				getTotalWaitTimeInSecs());
		verifyTextContains(locationURN, "text_Indexes_Location_URN");
		endLine(method);
	}

	@Test(priority = 5)
	public void updateLocation(Method method) throws Exception {
		startLine(method);
		clickAndWait("button_Indexes_Update");
		sendKeys("textField_Indexes_Location_Update_OtherInfo",
				"editedIDEOtherInfo");
		clickAndWait("button_Indexes_Update_Save");
		explicitlyWaitForElement("successMsg_Indexes_update",
				getTotalWaitTimeInSecs());
		verifyTextContains("updated", "successMsg_Indexes_update");
		endLine(method);
	}

	@Test(priority = 6)
	public void deleteLocation(Method method) throws Exception {
		startLine(method);
		clickAndWait("button_Indexes_Delete");
		clickAndWait("button_Indexes_Delete_AreYouSure_Yes");
		sendKeys("textField_Indexes_Delete_Reason",
				"Testing delete funtionality");
		clickAndWait("button_Indexes_Delete_OK");
		explicitlyWaitForElement("successMsg_Indexes_Delete",
				getTotalWaitTimeInSecs());
		verifyTextContains("deleted", "successMsg_Indexes_Delete");
		endLine(method);
	}

}
