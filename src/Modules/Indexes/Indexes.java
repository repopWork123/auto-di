package Modules.Indexes;

import java.lang.reflect.Method;

import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import ReusableUtilities.SeleniumBaseClass;

public class Indexes extends SeleniumBaseClass{
	
	protected static final String CATEGORY_URL = "/group/di/categories";
	protected static final String INDEXES_NOMINAL_URL = "/group/di/nominals";
	protected static final String INDEXES_VEHICLE_URL = "/group/di/vehicle";
	protected static final String INDEXES_LOCATION_URL = "/group/di/location";
	protected static final String INDEXES_TELEPHONE_URL = "/group/di/telephone";
	protected static String nominal_URN;
	protected static String vehicle_URN;
	protected static String location_URN;
	protected static String telephone_URN;
	protected static String organisation_URN;
	
	
	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}
	
	public Indexes() {
		softAssert = new SoftAssert();
	}
	
	public Indexes(WebDriver driver) {
		softAssert = new SoftAssert();
		this.driver = driver;
	}
	
	public void createCategory() throws Exception {
		//click on nominal link
		navigateTo(CATEGORY_URL);
	    explicitlyWaitForElement("button_Indexes_Search",getTotalWaitTimeInSecs());
		//click on search button
		click("button_Indexes_Search");
		explicitlyWaitForElement("icon_Indexes_SearchResult_CreateNew",getTotalWaitTimeInSecs());
		//click on create new entity icon in search result tab
		click("icon_Indexes_SearchResult_CreateNew");
		explicitlyWaitForElement("textField_Indexes_Category_Name",getTotalWaitTimeInSecs());
		sendKeys("textField_Indexes_Category_Name","CATNAME"+ getCurrentDataStampwithoutSlash());
		System.out.println("CATNAME"+ getCurrentDataStampwithoutSlash());
		click("button_Indexes_Create");
		//verify success message is displayed once nominal is created
		explicitlyWaitForElement("successMsg_Indexes_create", getTotalWaitTimeInSecs());
	}
	


	public String createBulkNominalAndGetURNs(String surname, String sex,
			String bulkCount) throws Exception {

		navigateToPage(INDEXES_NOMINAL_URL);
		clickAndWait("button_Indexes_Search_Reason");
		explicitlyWaitForElement("button_Indexes_Search_Reason_CreateDefaultList",
				getTotalWaitTimeInSecs());
		clickAndWait("button_Indexes_Search_Reason_CreateDefaultList");
		explicitlyWaitForElement("button_Indexes_Search",
				getTotalWaitTimeInSecs());
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("icon_Indexes_SearchResult_CreateNew",
				getTotalWaitTimeInSecs());
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		explicitlyWaitForElement("textField_Indexes_Nominal_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Indexes_Nominal_Surname", surname);
		selectByVisibleText("dropDown_Indexes_Create_Sex", sex);
		// Bulk create Entering bulk count
		click("checkBox_Action_BulkRecord");
		sendKeys("textField_Action_BulkRecordCount", bulkCount);
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("successMsg_Indexes_create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_Indexes_create");
		String succMess = getText("successMsg_Document_Create");
		String[] nominalUrn = succMess.split(" ");
		nominal_URN = nominalUrn[4];
		System.out.println(nominal_URN + "  nominal_URN");

		return nominal_URN;

	}
	
	public String createBulkVehicleAndGetURNs(String VRM,
			String bulkCount) throws Exception {

		navigateToPage(INDEXES_VEHICLE_URL);
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("icon_Indexes_SearchResult_CreateNew",
				getTotalWaitTimeInSecs());
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		explicitlyWaitForElement("textField_Indexes_Vehicle_Create_VRM",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Indexes_Vehicle_Create_VRM", VRM);
		// Bulk create Entering bulk count
		click("checkBox_Action_BulkRecord");
		sendKeys("textField_Action_BulkRecordCount", bulkCount);
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("successMsg_Indexes_create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_Indexes_create");
		String succMess = getText("successMsg_Document_Create");
		String[] nominalUrn = succMess.split(" ");
		vehicle_URN = nominalUrn[5];
		System.out.println(nominal_URN + "  nominal_URN");

		return vehicle_URN;

	}


	public String createBulkLocationAndGetURNs(String street,
			String bulkCount) throws Exception {

		navigateToPage(INDEXES_LOCATION_URL);
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("icon_Indexes_SearchResult_CreateNew",
				getTotalWaitTimeInSecs());
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		sendKeys("textField_Indexes_Location_Street", street);
		// Bulk create Entering bulk count
		click("checkBox_Action_BulkRecord");
		sendKeys("textField_Action_BulkRecordCount", bulkCount);
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("successMsg_Indexes_create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_Indexes_create");
		String succMess = getText("successMsg_Document_Create");
		String[] nominalUrn = succMess.split(" ");
		vehicle_URN = nominalUrn[4];
		System.out.println(nominal_URN + "  nominal_URN");

		return vehicle_URN;

	}
	
	public String createBulkTelephoneAndGetURNs(String teleNumber,
			String bulkCount) throws Exception {

		navigateToPage(INDEXES_TELEPHONE_URL);
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("icon_Indexes_SearchResult_CreateNew",
				getTotalWaitTimeInSecs());
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		sendKeys("textField_Indexes_Telephone_TeleNumber", teleNumber);
		// Bulk create Entering bulk count
		click("checkBox_Action_BulkRecord");
		sendKeys("textField_Action_BulkRecordCount", bulkCount);
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("successMsg_Indexes_create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_Indexes_create");
		String succMess = getText("successMsg_Document_Create");
		String[] nominalUrn = succMess.split(" ");
		vehicle_URN = nominalUrn[4];
		System.out.println(nominal_URN + "  nominal_URN");

		return vehicle_URN;

	}

	
	
	
	
	

}
