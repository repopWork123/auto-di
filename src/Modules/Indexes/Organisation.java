package Modules.Indexes;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class Organisation extends Indexes {
	
	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}
	
	public Organisation() {
		softAssert = new SoftAssert();
	}
	
	public Organisation(WebDriver driver) {
		softAssert = new SoftAssert();
		this.driver = driver;
	}
}
