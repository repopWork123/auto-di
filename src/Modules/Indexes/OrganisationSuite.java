package Modules.Indexes;

import java.lang.reflect.Method;

import org.testng.annotations.*;

import ReusableUtilities.SeleniumBaseClass;

public class OrganisationSuite extends SeleniumBaseClass {
	String INDEXES_ORGANISATION_URL = "/group/holmes/organisation";
	String orgName = "orgIDE002";
	String successMsg;
	String organisationURN;

	 @BeforeTest
	public void startBrowser() throws Exception {

		loginInIEAndSelectIncident();
	}

	public String getURN(String successMsg) {

		String[] URN = successMsg.split(" ");
		return URN[1];

	}

	@Test(priority = 1)
	public void createOrganisation_withoutAnyData(Method method)
			throws Exception {

		startLine(method);
		navigateToPage(INDEXES_ORGANISATION_URL);
		clickAndWait("button_Indexes_Search");
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("errorMsg_Indexes_Create_WithOutAnyFields",
				getTotalWaitTimeInSecs());
		verifyTextContains("There are validation errors",
				"errorMsg_Indexes_Create_WithOutAnyFields");
		verifyTextContains("Organisation Name is required",
				"errorMsg_Indexes_Organisation_Name");
		endLine(method);
	}

	@Test(priority = 2)
	public void createOrganisation_withMandotoryData(Method method)
			throws Exception {

		startLine(method);
		navigateToPage(INDEXES_ORGANISATION_URL);
		clickAndWait("button_Indexes_Search");
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		sendKeys("textField_Indexes_Organisation_Name", orgName);
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("successMsg_Indexes_create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_Indexes_create");
		// Get exhibit URN
		successMsg = getText("successMsg_Indexes_create");
		organisationURN = getURN(successMsg);
		System.out.println(organisationURN);
		endLine(method);
	}

	@Test(priority = 3)
	public void searchOrganisation(Method method) throws Exception {

		startLine(method);
		navigateToPage(INDEXES_ORGANISATION_URL);
		sendKeys("textField_Indexes__Organisation_Search_Name", orgName);
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("table_Indexes_SearchResults",
				getTotalWaitTimeInSecs());
		verifyElementPresent("table_Indexes_SearchResults");
		clickAndWait("table_Indexes_SearchResults");
		clickAndWait("icon_Indexes_SearchResult_View");
		explicitlyWaitForElement("text_Indexes_Organisation_Name",
				getTotalWaitTimeInSecs());
		verifyTextContains(orgName, "text_Indexes_Organisation_Name");
		endLine(method);
	}

	@Test(priority = 4)
	public void updateOrganisation(Method method) throws Exception {
		startLine(method);
		clickAndWait("button_Indexes_Update");
		explicitlyWaitForElement("textField_Indexes_Organisation_Edit_OtherInfo",
				60);
		sendKeys("textField_Indexes_Organisation_Edit_OtherInfo",
				"editedIDEOtherInfo");
		clickAndWait("button_Indexes_Update_Save");
		explicitlyWaitForElement("SuccessMsg_createEntity",
				getTotalWaitTimeInSecs());
		verifyTextContains("updated", "successMsg_Indexes_update");
		endLine(method);
	}

	@Test(priority = 5)
	public void deleteOrganisation(Method method) throws Exception {
		startLine(method);
		clickAndWait("button_Indexes_Delete");
		click("button_Indexes_Delete_AreYouSure_Yes");
		sendKeys("textField_Indexes_Delete_Reason",
				"Testing delete funtionality");
		clickAndWait("button_Indexes_Delete_OK");
		explicitlyWaitForElement("successMsg_Indexes_Delete",
				getTotalWaitTimeInSecs());
		verifyTextContains("deleted", "successMsg_Indexes_Delete");
		endLine(method);
	}

}