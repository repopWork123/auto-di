package Modules.Indexes;

import java.lang.reflect.Method;

import org.testng.annotations.*;

import ReusableUtilities.Report;
import ReusableUtilities.SeleniumBaseClass;

public class VehicleSuite extends SeleniumBaseClass {

	String vehicleVRM = "12121211";
	String successMsg;
	String vehicleURN;
	private static final String VEHICLE_URL = "/group/holmes/vehicle";

	@BeforeTest
	public void startBrowser() throws Exception {

		loginInIEAndSelectIncident();
	}

	// @AfterTest
	public void closeBrowser() throws Exception {

		closeApp();

	}

	public String getURN(String successMsg) {

		String[] URN = successMsg.split(" ");
		return URN[1];

	}

	private void navigateToVehicle() {
		navigateToPage(VEHICLE_URL);
	}

	@Test(priority = 1)
	public void createVehicle_withoutAnyData(Method method) throws Exception {
		startLine(method);

		/*
		 * Click("link_Indexes"); Click("link_Vehicle");
		 */

		// this code is used to run on ie 11
		navigateToVehicle();
		clickAndWait("button_Indexes_Search");
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("errorMsg_Indexes_Create_WithOutAnyFields",
				getTotalWaitTimeInSecs());
		verifyTextContains(
				"There is no information on a Vehicle record. Please enter at least one piece of information.",
				"errorMsg_Indexes_Create_WithOutAnyFields");
		endLine(method);
	}

	@Test(priority = 2)
	public void createVehicle_withMandotoryData(Method method) throws Exception {
		startLine(method);

		/*
		 * Click("link_Indexes"); Click("link_Vehicle");
		 */

		// this code is used to run on ie 11
		navigateToVehicle();
		clickAndWait("button_Indexes_Search");
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		sendKeys("textField_Indexes_Vehicle_Create_URN", vehicleVRM);
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("successMsg_Indexes_create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_Indexes_create");
		// Get exhibit URN
		successMsg = getText("successMsg_Indexes_create");
		vehicleURN = getURN(successMsg);
		System.out.println(vehicleURN);
		endLine(method);
	}

	@Test(priority = 3)
	public void searchVehicle(Method method) throws Exception {
		startLine(method);

		/*
		 * Click("link_Indexes"); Click("link_Vehicle");
		 */

		// this code is used to run on ie 11
		navigateToVehicle();
		sendKeys("textField_Indexes_Vehicle_Search_URN", vehicleURN);
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("table_Indexes_SearchResults",
				getTotalWaitTimeInSecs());
		verifyElementPresent("table_Indexes_SearchResults");
		click("table_Indexes_SearchResults");
		clickAndWait("icon_Indexes_SearchResult_View");
		explicitlyWaitForElement("text_Indexes_Vehicle_URN", getTotalWaitTimeInSecs());
		verifyTextContains(vehicleURN, "text_Indexes_Vehicle_URN");
		endLine(method);
	}

	@Test(priority = 4)
	public void updateOrganisation(Method method) throws Exception {
		startLine(method);
		navigateToVehicle();
		sendKeys("textField_Indexes_Vehicle_Search_URN", vehicleURN);
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("table_Indexes_SearchResults",
				getTotalWaitTimeInSecs());
		verifyElementPresent("table_Indexes_SearchResults");
		click("table_Indexes_SearchResults");
		clickAndWait("icon_Indexes_SearchResult_View");
		clickAndWait("button_Indexes_Update");
		sendKeys("textField_Eidt_OtherInformation", "editedIDEOtherInfo");
		clickAndWait("button_Indexes_Update_Save");
		explicitlyWaitForElement("successMsg_Indexes_create",
				getTotalWaitTimeInSecs());
		verifyTextContains("updated", "successMsg_Indexes_update");
		endLine(method);
	}

	@Test(priority = 5)
	public void deleteOrganisation(Method method) throws Exception {
		startLine(method);
		navigateToVehicle();
		sendKeys("textField_Indexes_Vehicle_Search_URN", vehicleURN);
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("table_Indexes_SearchResults",
				getTotalWaitTimeInSecs());
		verifyElementPresent("table_Indexes_SearchResults");
		click("table_Indexes_SearchResults");
		clickAndWait("icon_Indexes_SearchResult_View");
		click("button_Indexes_Delete");
		click("button_Indexes_Delete_AreYouSure_Yes");
		sendKeys("textField_Indexes_Delete_Reason", "Testing delete funtionality");
		clickAndWait("button_Indexes_Delete_OK");
		explicitlyWaitForElement("successMsg_Indexes_Delete",
				getTotalWaitTimeInSecs());
		verifyTextContains("deleted", "successMsg_Indexes_Delete");
		endLine(method);
	}

}
