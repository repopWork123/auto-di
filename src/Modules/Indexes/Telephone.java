package Modules.Indexes;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class Telephone  extends Indexes {
	
	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}
	
	public Telephone() {
		softAssert = new SoftAssert();
	}
	
	public Telephone(WebDriver driver) {
		softAssert = new SoftAssert();
		this.driver = driver;
	}
	
	public void createTelephone(String teleNumber) throws Exception {

		navigateToPage(INDEXES_TELEPHONE_URL);
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("icon_Indexes_SearchResult_CreateNew",getTotalWaitTimeInSecs());
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		sendKeys("textField_Indexes_Telephone_TeleNumber", teleNumber);
		
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("successMsg_Indexes_create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_Indexes_create");
	
	}

}
