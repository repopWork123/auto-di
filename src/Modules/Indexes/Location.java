package Modules.Indexes;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class Location extends Indexes {
	
	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}
	
	public Location() {
		softAssert = new SoftAssert();
	}
	
	public Location(WebDriver driver) {
		softAssert = new SoftAssert();
		this.driver = driver;
	}
	public void createLocation(String street) throws Exception {

		navigateToPage(INDEXES_LOCATION_URL);
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("icon_Indexes_SearchResult_CreateNew",getTotalWaitTimeInSecs());
		clickAndWait("icon_Indexes_SearchResult_CreateNew");		
		sendKeys("textField_Indexes_Location_Street", street);
		
		clickAndWait("button_Indexes_Create");
		explicitlyWaitForElement("successMsg_Indexes_create",
				getTotalWaitTimeInSecs());
		verifyTextContains("created", "successMsg_Indexes_create");
	
	}
	
}
