package Modules.AssociationSearch;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import ReusableUtilities.Report;
import ReusableUtilities.SeleniumBaseClass;

public class CategorytoCategory extends SeleniumBaseClass{
	
	
	private String Surname= "FOSTER";
	private String forename = "ROBIN";
	java.util.Date d = new java.util.Date();
	String Date = new SimpleDateFormat("ddmmhh").format(d.getTime());
	
//    @BeforeTest	
	public void beforeTest() throws Exception
	{
		loginInIEAndSelectIncident();
	}
  	
  	//@Test (priority=1)
 	public void createAssociationBetweenNomToOtherEntities() throws Exception
	{  
		  createAssociation("N1");
		  sleep(4000);
		  createAssociation("C1");
		  sleep(4000);
		  createAssociation("X1");
		  sleep(4000);
    	  createAssociation("L1");
		  sleep(4000);
		  createAssociation("V1");
		  sleep(4000);
		  createAssociation("O1");
		  sleep(4000);
		  createAssociation("T1");	  
	  
	}
  	
  	@Test (priority=2)
  	public void AssoSrch_Without_Mandatory_Fields(Method method) throws Exception
	{
  		startLine(method); 
  		sleep(6000);
  		
  		//hoverAndClick(new String[]{"link_Search"}, "link_Asso_Search");
/*  		Click("link_Search");
  		sleep(4000);
  		Click("link_Asso_Search");
  		*/
  		driver.get("http://ustr-erl-4427.na.uis.unisys.com:8080/group/holmes/association-search");
  		sleep(4000);
  		click("button_AssociationSearch_Next");
  		sleep(4000);
  		//VerifyTextPresent("Reason for Search is mandatory", "link_Search");
  		//VerifyTextPresent("Please select at least two Entities", "link_Asso_Search");
  		
  		endLine(method);	
	}
  	
  	@Test (priority=3)
  	public void AssoSrch_With_NewQuery(Method method) throws Exception
	{
  		startLine(method);
  		//hoverAndClick(new String[]{"link_Search"}, "link_Asso_Search");
  		sleep(6000);
  		driver.get("http://ustr-erl-4427.na.uis.unisys.com:8080/group/holmes/association-search");
  		/*Click("link_Search");
  		sleep(4000);
  		Click("link_Asso_Search");
  		*/
 		/*sleep(4000);
 		String str = GetText("link_Default_Grp_Selected");
   		String[] GrpArr = str.split(":");
   		Report.info("Default Group Selected :" + GrpArr[1]);  		
  		String DefGrpSel = GrpArr[1];*/
  		
 		/*sleep(4000);
  		SendKeys("text_Categories_SearchReason", "Create default list");
  		driver.findElement(By.id("Create default list")).click();
 		sleep(4000);
  		Click("link_Select_Grp");
  		sleep(4000);
  		SendKeys("text_Fiter_SelectedGrp",DefGrpSel);
  		sleep(4000);*/
  		
 		/*driver.findElement(By.xpath("html/body/div[8]/div/ul/li[1]/a/span[2]")).click();
  		sleep(4000);
  		Click("link_Select_Grp");
  		sleep(4000);*/	
  		WebElement e = driver.findElement(By.xpath("//td[@id='entityDropDown']"));  	
  	    
  		new Select(e.findElement(By.xpath("(//select[@id='entityId'])[1]"))).selectByVisibleText("Nominal");
  		Report.info("Selecting Entity: Nominal");
  		sleep(4000);
  		new Select(driver.findElement(By.xpath("(//select[@id='entityId'])[2]"))).selectByVisibleText("Nominal");
  		Report.info("Selecting Entity: Nominal");
  		sleep(4000);
 		click("button_AssociationSearch_Next"); 		
  		sleep(4000);
  		click("button_AssociationSearch_NewQuery");
  		sleep(4000);
  		endLine(method);	
  			
	}
  	
  	public void searchAssoBTWEntity1Entity2(String entity1, String entity2) throws Exception
  	{
  		sleep(6000);
  		//hoverAndClick(new String[]{"link_Search"}, "link_Asso_Search");
  		driver.get("http://ustr-erl-4427.na.uis.unisys.com:8080/group/holmes/association-search");
  		/*Click("link_Search");
  		sleep(4000);
  		Click("link_Asso_Search");*/
  		
  		/*sleep(4000);
  		String str = GetText("link_Default_Grp_Selected");
   		String[] GrpArr = str.split(":");
   		Report.info("Default Group Selected :" + GrpArr[1]);  		
  		String DefGrpSel = GrpArr[1];
  		
  		sleep(4000);
  		SendKeys("text_Categories_SearchReason", "Create default list");
  		driver.findElement(By.id("Create default list")).click();
  		sleep(4000);
  		Click("link_Select_Grp");
  		sleep(4000);
  		SendKeys("text_Fiter_SelectedGrp",DefGrpSel);
  		sleep(4000);
  		
  		driver.findElement(By.xpath("html/body/div[8]/div/ul/li[1]/a/span[2]")).click();
  		sleep(4000);
  		Click("link_Select_Grp");
  		sleep(4000); */ 
  		WebElement e = driver.findElement(By.xpath("//td[@id='entityDropDown']"));		
  	
  		new Select(e.findElement(By.xpath("(//select[@id='entityId'])[1]"))).selectByVisibleText(entity1);
  		Report.info("Selecting Entity: Nominal");
  		sleep(4000);
  		new Select(driver.findElement(By.xpath("(//select[@id='entityId'])[2]"))).selectByVisibleText(entity2);
  		Report.info("Selecting Entity: Category");
  		
  		sleep(4000);
  		click("button_AssociationSearch_Next");  		
  		
  		//Nominal > Association > Nominal
  		sleep(6000);
  		driver.findElement(By.xpath("//input[(@type='button' or @type='submit') and @value='next']")).click();  		
  		sleep(4000);
  		driver.findElement(By.xpath("//span[contains(text(),'Check all')]")).click();
  		sleep(6000);
  		driver.findElement(By.xpath("//input[(@type='button' or @type='submit') and @value='Next']")).click();
  		sleep(7000);
  		driver.findElement(By.xpath("//input[(@type='button' or @type='submit') and @value='next']")).click(); 
  		sleep(7000);
  		driver.findElement(By.xpath("//section[@id='portlet_crossIndexSearch_WAR_softentityui']/div/div/div/span")).click();
  		sleep(4000);
  	}
  	
  	@Test (priority=4)
	public void AssoSrch_for_NomAssoWithCat(Method method) throws Exception
	{
  		startLine(method);
  		searchAssoBTWEntity1Entity2("Nominal", "Category");  
  		endLine(method);	
	}
	
  	@Test (priority=5)
	public void AssoSrch_for_NomAssoWithNom(Method method) throws Exception
	{
  		startLine(method);
  		searchAssoBTWEntity1Entity2("Nominal", "Nominal");
  		endLine(method);	
	}
  	@Test (priority=6)
	public void AssoSrch_for_NomAssoWithTel(Method method) throws Exception
	{
  		startLine(method);
  		searchAssoBTWEntity1Entity2("Nominal", "Telephone");  		
  		endLine(method);	
	}
  	@Test (priority=7)
	public void AssoSrch_for_NomAssoWithOrg(Method method) throws Exception
	{
  		startLine(method);
  		searchAssoBTWEntity1Entity2("Nominal", "Organisation");  	
  		endLine(method);	
	}
  	@Test (priority=8)
	public void AssoSrch_for_NomAssoWithLoc(Method method) throws Exception
	{
  		startLine(method);
  		searchAssoBTWEntity1Entity2("Nominal", "Location");  		
  		endLine(method);	
	}
	  	
	@Test (priority=9)
	public void AssoSrch_for_NomAssoWithVeh(Method method) throws Exception
	{
		startLine(method);
		searchAssoBTWEntity1Entity2("Nominal", "Vehicle");  	
		endLine(method);	
	}
	
	@Test (priority=10)
	public void View_AssoSrch_Result(Method method) throws Exception
	{
		
		startLine(method);
		explicitlyWaitForElement("link_AssociationSearch_SearchResults_Select", 10);
		verifyElementPresent("link_AssociationSearch_SearchResults_Select");	 
		endLine(method);	
	}
	
	@Test (priority=11)
	public void AssoSrch_Results_ViewRecord(Method method) throws Exception
	{
		startLine(method);
		sleep(30000);
		click("link_AssociationSearch_SearchResults_Select");  		
  	  	
		explicitlyWaitForElement("link_AssociationSearch_SearchResults_Select_View", 10);
		click("link_AssociationSearch_SearchResults_Select_View");  	
		
		sleep(14000);		
  		getHandleToWindow("Association Search"); 
  		driver.close();
  		sleep(4000);
  		endLine(method);	
	}
	
	//@Test (priority=12)
	public void AssoSrch_Results_Added_to_NewList(Method method)throws Exception
	{	
			startLine(method);  	
			explicitlyWaitForElement("link_AssociationSearch_Add_NewList", 10);
			click("link_AssociationSearch_Add_NewList");
			sleep(5000);
			driver.findElement(By.className("msgBoxButtons")).findElement(By.xpath("//*[@name='Ok']")).click(); 
			getHandleToWindow("List Maintenance - Liferay");
			
			driver.findElement(By.id("listNameText")).sendKeys("FromAssoSearch" + Date);
			driver.findElement(By.id("descriptionText")).sendKeys("FromAssoSearch" + Date);
			 //Selecting Group value
			sleep(4000);
			driver.findElement(By.id("incidentId")).click();
		    new Select(driver.findElement(By.id("incidentId"))).selectByIndex(1); 
		    driver.findElement(By.id("incidentId")).submit();
		    sleep(4000);
		    driver.findElement(By.id(".//*[@id='actionBarInputButtons']/input[1]")).click();
		    
		    /*//To check Default & Fav
		    driver.findElement(By.xpath("//button[@type='button']")).click();
		    sleep(5000);
		    driver.findElement(By.xpath("//li/a/span[2]")).click();
		    sleep(5000);
		    driver.findElement(By.xpath(".//*[@id='actionButtons']/input[1]")).click();*/ 	    	   
		  
		    driver.close();
		    
		    getHandleToWindow("Association Search - U-LEAF");
		    endLine(method);	
						
	}
	@Test (priority=13)
	public void AssoSrch_Results_Added_to_Default(Method method) throws Exception
	{
		startLine(method);
		explicitlyWaitForElement("link_Asso_Srch_AddToDefList", 10);
  		click("link_Asso_Srch_AddToDefList");
  		sleep(5000);
  		driver.findElement(By.className("msgBoxButtons")).findElement(By.xpath(".//*[@name='Ok']")).click();  		 
  		endLine(method);	
	}
	@Test (priority=14)
	public void AssoSrch_Results_Added_to_Fav(Method method) throws Exception
	{
		startLine(method);
		explicitlyWaitForElement("link_AssociationSearch_Add_FavList", 10);
  		click("link_AssociationSearch_Add_FavList");
  		sleep(5000);
  		driver.findElement(By.className("msgBoxButtons")).findElement(By.xpath(".//*[@name='Ok']")).click();
  		endLine(method);	
	}
	
	@Test (priority=15)
	public void VerifyNavigationBTWAsso(Method method) throws Exception
	{
		startLine(method);
		searchAssoBTWEntity1Entity2("Nominal", "Vehicle"); 		
		click("link_AssociationSearch_Navigation_EntitySecond");		
		verifyElementPresent("textfield_AssociationSearch_URN");
		
		click("link_AssociationSearch_Navigation_EntityOne");	
		verifyElementPresent("textfield_AssociationSearch_Surname");
		
		click("link_AssociationSearch_Navigation_Asso");		
		verifyElementPresent("link_AssociationSearch_CheckAll");
		endLine(method);	
	}
	@Test (priority=16)
	public void AddAssoForMultipleSoftEntities(Method method) throws Exception
	{
		startLine(method);
		sleep(6000);
		//hoverAndClick(new String[]{"link_Search"}, "link_Asso_Search");
		driver.get("http://ustr-erl-4427.na.uis.unisys.com:8080/group/holmes/association-search");
  		/*Click("link_Search");
  		sleep(4000);
  		Click("link_Asso_Search");
  		*/
  		/*sleep(4000);
  		String str = GetText("link_Default_Grp_Selected");
   		String[] GrpArr = str.split(":");
   		Report.info("Default Group Selected :" + GrpArr[1]);  		
  		String DefGrpSel = GrpArr[1];
  		
  		sleep(4000);
  		SendKeys("text_Categories_SearchReason", "Create default list");
  		driver.findElement(By.id("Create default list")).click();
  		sleep(4000);
  		Click("link_Select_Grp");
  		sleep(4000);
  		SendKeys("text_Fiter_SelectedGrp",DefGrpSel);
  		sleep(4000);
  		
  		driver.findElement(By.xpath("html/body/div[8]/div/ul/li[1]/a/span[2]")).click();
  		sleep(4000);
  		Click("link_Select_Grp");
  		sleep(4000);*/
  		WebElement e = driver.findElement(By.xpath("//td[@id='entityDropDown']"));  		
  	
  		new Select(e.findElement(By.xpath("(//select[@id='entityId'])[1]"))).selectByVisibleText("nominal");
  		Report.info("Selecting Entity: Nominal");
  		sleep(4000);
  		new Select(driver.findElement(By.xpath("(//select[@id='entityId'])[2]"))).selectByVisibleText("vehicle");
  		Report.info("Selecting Entity: Vehicle");	
  		
  		click("button_AssociationSearch_AddEntity");
  		endLine(method);	
	}
	@Test (priority=17)
	public void DeleteThridSoftSoftentity(Method method) throws Exception
	{
		startLine(method);
		sleep(6000);
		//hoverAndClick(new String[]{"link_Search"}, "link_Asso_Search");
		driver.get("http://ustr-erl-4427.na.uis.unisys.com:8080/group/holmes/association-search");
  		/*Click("link_Search");
  		sleep(4000);
  		Click("link_Asso_Search");*/
  		
  		/*sleep(4000);
  		String str = GetText("link_Default_Grp_Selected");
   		String[] GrpArr = str.split(":");
   		Report.info("Default Group Selected :" + GrpArr[1]);  		
  		String DefGrpSel = GrpArr[1];
  		
  		sleep(4000);
  		SendKeys("text_Categories_SearchReason", "Create default list");
  		driver.findElement(By.id("Create default list")).click();
  		sleep(4000);
  		Click("link_Select_Grp");
  		sleep(4000);
  		SendKeys("text_Fiter_SelectedGrp",DefGrpSel);
  		sleep(4000);
  		
  		driver.findElement(By.xpath("html/body/div[8]/div/ul/li[1]/a/span[2]")).click();
  		sleep(4000);
  		Click("link_Select_Grp");
  		sleep(4000);*/
  		WebElement e = driver.findElement(By.xpath("//td[@id='entityDropDown']"));  		
  	
  		new Select(e.findElement(By.xpath("(//select[@id='entityId'])[1]"))).selectByVisibleText("Nominal");
  		Report.info("Selecting Entity: Nominal");
  		sleep(4000);
  		new Select(driver.findElement(By.xpath("(//select[@id='entityId'])[2]"))).selectByVisibleText("Vehicle");
  		Report.info("Selecting Entity: Vehicle");	
  		
  		click("button_AssociationSearch_AddEntity");  		
  		click("link_AssociationSearch_Delete_AddedEntity");
  		endLine(method);	
	}
	@Test (priority=18)
	public void AssoSearchForMultipleSoftEntities(Method method) throws Exception
	{
		startLine(method);
		sleep(6000);
		//hoverAndClick(new String[]{"link_Search"}, "link_Asso_Search");
		driver.get("http://ustr-erl-4427.na.uis.unisys.com:8080/group/holmes/association-search");
  		/*Click("link_Search");
  		sleep(4000);
  		Click("link_Asso_Search");
  		*/
  		/*sleep(4000);
  		String str = GetText("link_Default_Grp_Selected");
   		String[] GrpArr = str.split(":");
   		Report.info("Default Group Selected :" + GrpArr[1]);  		
  		String DefGrpSel = GrpArr[1];
  		
  		sleep(4000);
  		SendKeys("text_Categories_SearchReason", "Create default list");
  		driver.findElement(By.id("Create default list")).click();
  		sleep(4000);
  		Click("link_Select_Grp");
  		sleep(4000);
  		SendKeys("text_Fiter_SelectedGrp",DefGrpSel);
  		sleep(4000);
  		
  		driver.findElement(By.xpath("html/body/div[8]/div/ul/li[1]/a/span[2]")).click();
  		sleep(4000);
  		Click("link_Select_Grp");
  		sleep(4000);*/
  		WebElement e = driver.findElement(By.xpath("//td[@id='entityDropDown']"));  		
  	
  		new Select(e.findElement(By.xpath("(//select[@id='entityId'])[1]"))).selectByVisibleText("Nominal");
  		Report.info("Selecting Entity: Nominal");
  		sleep(4000);
  		new Select(driver.findElement(By.xpath("(//select[@id='entityId'])[2]"))).selectByVisibleText("Nominal");
  		Report.info("Selecting Entity: Nominal");	
  		
  		click("button_AssociationSearch_AddEntity");
  		sleep(4000);
  		new Select(driver.findElement(By.xpath("(//select[@id='entityId'])[3]"))).selectByVisibleText("Vehicle");  		
  		Report.info("Selecting Entity: Vehicle");
  		
  		sleep(4000);
  		click("button_AssociationSearch_Next");  		
  		
  		//Nominal > Association > Nominal
  		 sleep(6000);
   		driver.findElement(By.xpath("//input[(@type='button' or @type='submit') and @value='next']")).click();  		
   		sleep(4000);
   		driver.findElement(By.xpath("//span[contains(text(),'Check all')]")).click();
   		sleep(6000);
   		driver.findElement(By.xpath("//input[(@type='button' or @type='submit') and @value='Next']")).click();
   		sleep(7000);
        driver.findElement(By.xpath("//input[(@type='button' or @type='submit') and @value='next']")).click();
        sleep(4000);
   		driver.findElement(By.xpath("//span[contains(text(),'Check all')]")).click();
   		sleep(6000);
        driver.findElement(By.xpath("//input[(@type='button' or @type='submit') and @value='Next']")).click();
   		sleep(7000);
        driver.findElement(By.xpath("//input[(@type='button' or @type='submit') and @value='next']")).click();
        sleep(7000);
   		driver.findElement(By.xpath("//section[@id='portlet_crossIndexSearch_WAR_softentityui']/div/div/div/span")).click();
   		sleep(4000);
   		endLine(method);	
	}
	@Test (priority=19)
	public void ResetoftEntitySearchCriteria(Method method) throws Exception
	{
		
		startLine(method);
		//hoverAndClick(new String[]{"link_Search"}, "link_Asso_Search");
		sleep(6000);
		driver.get("http://ustr-erl-4427.na.uis.unisys.com:8080/group/holmes/association-search");
  		/*Click("link_Search");
  		sleep(4000);
  		Click("link_Asso_Search");
  		
  		sleep(4000);
  		String str = GetText("link_Default_Grp_Selected");
   		String[] GrpArr = str.split(":");
   		Report.info("Default Group Selected :" + GrpArr[1]);  		
  		String DefGrpSel = GrpArr[1];
  		
  		sleep(4000);
  		SendKeys("text_Categories_SearchReason", "Create default list");
  		driver.findElement(By.id("Create default list")).click();
  		sleep(4000);
  		Click("link_Select_Grp");
  		sleep(4000);
  		SendKeys("text_Fiter_SelectedGrp",DefGrpSel);
  		sleep(4000);
  		
  		driver.findElement(By.xpath("html/body/div[8]/div/ul/li[1]/a/span[2]")).click();
  		sleep(4000);
  		Click("link_Select_Grp");
  		sleep(4000);*/
  		WebElement e = driver.findElement(By.xpath("//td[@id='entityDropDown']"));  		
  	
  		new Select(e.findElement(By.xpath("(//select[@id='entityId'])[1]"))).selectByVisibleText("Nominal");
  		Report.info("Selecting Entity: Nominal");
  		sleep(4000);
  		new Select(driver.findElement(By.xpath("(//select[@id='entityId'])[2]"))).selectByVisibleText("Nominal");
  		Report.info("Selecting Entity: Nominal");	
  		
  		sleep(4000);
  		click("button_AssociationSearch_Next");  
  		
		driver.findElement(By.xpath("//input[@value='reset']")).click();
		endLine(method);	
	}
	
	//@Test (priority=20)
	public void VerifyAssoUsagesSelected(Method method) throws Exception
  	{
		startLine(method);
		sleep(6000);
		//hoverAndClick(new String[]{"link_Search"}, "link_Asso_Search");
		driver.get("http://ustr-erl-4427.na.uis.unisys.com:8080/group/holmes/association-search");
  		/*Click("link_Search");
  		sleep(4000);
  		Click("link_Asso_Search");
  		*/
  		sleep(4000);
  		String str = getText("link_Default_Grp_Selected");
   		String[] GrpArr = str.split(":");
   		Report.info("Default Group Selected :" + GrpArr[1]);  		
  		String DefGrpSel = GrpArr[1];
  		
  		sleep(4000);
  		sendKeys("text_Categories_SearchReason", "Create default list");
  		driver.findElement(By.id("Create default list")).click();
  		sleep(4000);
  		click("link_Select_Grp");
  		sleep(4000);
  		sendKeys("text_Fiter_SelectedGrp",DefGrpSel);
  		sleep(4000);
  		
  		driver.findElement(By.xpath("html/body/div[8]/div/ul/li[1]/a/span[2]")).click();
  		sleep(4000);
  		click("link_Select_Grp");
  		sleep(4000);
  		WebElement e = driver.findElement(By.xpath("//td[@id='entityDropDown']"));  		
  	
  		new Select(e.findElement(By.xpath("(//select[@id='entityId'])[1]"))).selectByVisibleText("Nominal");
  		Report.info("Selecting Entity: Nominal");
  		sleep(4000);
  		new Select(driver.findElement(By.xpath("(//select[@id='entityId'])[2]"))).selectByVisibleText("Nominal");
  		Report.info("Selecting Entity: Nominal");
  		
  		sleep(4000);
  		click("button_AssociationSearch_Next");  		
  		
  		//Nominal > Association > Nominal
  		sleep(6000);
  		driver.findElement(By.xpath(".//*[@id='crossIndexSearchForm']/div[3]/ul/li[2]/input[2]")).click();  		
  		sleep(4000);
  		driver.findElement(By.xpath("//input[@value='Next']")).click();
  		sleep(9000);
	     
  		String selAsso = getText("text_AssoSearch_ErrorMsgToSelectAsso");
  		Report.info(selAsso);
  		if(selAsso.contains("Must select one or more associations.") )
  		{
  			Report.pass("Message to select one or more associations is been dispalyed");
  		}
  		else
  		{
  			Report.fail("Message to select one or more associations is not been dispalyed");
  		}
  		endLine(method);	
	}
	
	public void createAssociation(String Entities) throws Exception
  	{
  
	      Report.info("****** Add AssociationLink Nominal  Started*********");
		  searchNominal();
		  sleep(2000);
		  click("link_Association");
		  sleep(4000);
		  click("button_Asso_Srch_AddAssociation");
		  sleep(2000);
		  driver.findElement(By.xpath(".//*[@id='type']/option[1]")).click();
		  sleep(2000);
		  click("text_Association_TargetURN");
		  sleep(2000);
		  sendKeys("text_Association_TargetURN",Entities);
		  sleep(3000);
		  click("text_Association_Usage");
		  sleep(1000);
		  click("text_Association_Usage");
		  sleep(2000);
		  driver.findElement(By.xpath("//div[@id='usage_ctr']/div/div[1]")).click();
		  sleep(2000);
		  sendKeys("text_Association_ProvenanceURN","S1");
		  sleep(2000);
		  click("button_AddSingleAssociation");
		  sleep(2000);
		  Report.info("Add AssociationLink Nominal  is Successfull");
		  
	  }
 
  	public void searchNominal() throws Exception {	
  	  
	    Report.info("********Search Nominals Started***********");
		sleep(5000);
		click("link_Index");
		sleep(3000);
		click("link_Index_Nominals");
		/*sleep(7000);
		SendKeys("text_Nominals_SearchReason", "Create default list");*/
		sleep(2000);
		driver.findElement(By.xpath("(//td[contains(.,'Surname')]/following-sibling::td[1]/input)[1]")).sendKeys(Surname);
		
		sleep(3000);
		click("button_Index_SearchButton");
		sleep(7000);
		driver.findElement(By.xpath("id=jqg_searchResultsTable_1")).click();
		driver.findElement(By.xpath("//td[@title='View the selected entity']")).click();
		sleep(2000);
		Report.info("Search and View  Nominal is successful");
	  
  	}
    
   
  @AfterTest
  public void afterTest() {
  }

}

