package Modules.Systems;

import java.lang.reflect.Method;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.Report;
import ReusableUtilities.SeleniumBaseClass;

public class TaskSuite extends SeleniumBaseClass {

	private String GroupCharInitial = randomChar();
	private String GroupNoInitial = randomNumber() + randomNumber() ;
	private String GroupSuffix = GroupCharInitial + GroupNoInitial; 
	private static final String TASK_URL = "/group/holmes/tasks";

	public String randomChar(){
		final String alphabet = "ABCDEFGHIJKLMNOPQRST";
		final int N = alphabet.length();
		Random r = new Random();

		String randomString=Character.toString(alphabet.charAt(r.nextInt(N)));

		return randomString;
	}
	public String randomNumber(){
		final String alphabet = "123456789";
		final int N = alphabet.length();
		Random r = new Random();

		String randomString=Character.toString(alphabet.charAt(r.nextInt(N)));

		return randomString;
	}

	@BeforeTest
	public void beforeTest() throws Exception {
//				loginInIEAndSelectIncident();
		loginAndSelectIncident();

	}

	@Test(priority = 1)
	public void importIncident(Method method) throws Exception{
		navigateToPage(TASK_URL);
		for(int i = 0;i < 1000; i++){
			GroupCharInitial = randomChar();
			GroupNoInitial = randomNumber() + randomNumber() ;
			GroupSuffix = GroupCharInitial + GroupNoInitial; 
			acceptAlertBoxIfPresent();
			click("icon_System_Tasks_AddJob");
			acceptAlertBoxIfPresent();
			explicitlyWaitForElement("button_System_Task_ImportIncident", getTotalWaitTimeInSecs());
			acceptAlertBoxIfPresent();
			doubleClick("button_System_Task_ImportIncident");
			acceptAlertBoxIfPresent();
			explicitlyWaitForElement("textField_System_Tasks_LaunchJob_IncidentSuffix", getTotalWaitTimeInSecs());
			acceptAlertBoxIfPresent();
			sendKeysWithDriver("textField_System_Tasks_LaunchJob_IncidentSuffix", GroupSuffix);
			acceptAlertBoxIfPresent();
			sendKeysWithDriver("textField_System_Tasks_LaunchJob_File", "x22");
			String xpathForIncidentDropDown = "//div[@id='export_99UN020115X22_20160829_085248_1_4_3.zip']";
			acceptAlertBoxIfPresent();
			selectIncidentToImport(xpathForIncidentDropDown);
			click("button_System_Task_ImportIncident_LaunchJob_Launch");
			acceptAlertBoxIfPresent();
			explicitlyWaitForElement("dialog_System_Tasks_ImportIncident_Confirmation", getTotalWaitTimeInSecs());
			acceptAlertBoxIfPresent();
			click("button_System_Task_ImportIncident_ConfirmationDialog_Continue");
			acceptAlertBoxIfPresent();
			explicitlyWaitForElement("dialog_Information", getTotalWaitTimeInSecs());
			acceptAlertBoxIfPresent();
			click("button_System_Task_ImportIncident_InformationDialog_Ok");
			acceptAlertBoxIfPresent();

		}

	}
	public void acceptAlertBoxIfPresent() {
		if(isAlertPresent()){
			acceptAlertBox(driver);
		}
	}

	private void selectIncidentToImport(String xpathForCurrentIncident)
			throws InterruptedException {
		click("icon_System_Tasks_LaunchJob_File");
	
		Report.info("xpathForCurrentIncident" + xpathForCurrentIncident);
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				getTotalWaitTimeInSecs());
		WebElement element = null;

		try {
			element = webDriverWaitForTask.until(ExpectedConditions
					.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
			acceptAlertBoxIfPresent();
			element.click();
			Report.info("Element is clicked");
		} catch (Exception e) {
			//	 			e.printStackTrace();
			Report.fail("Could not able to find the element to click from drop down");
			Assert.fail("Could not able to find the element to click from drop down");

		}

}
}
