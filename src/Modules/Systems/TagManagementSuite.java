package Modules.Systems;

import java.lang.reflect.Method;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.Report;
import ReusableUtilities.SeleniumBaseClass;

public class TagManagementSuite extends SeleniumBaseClass {

	private static final String TAG_MANAGEMENT_URL = "/tag-ui/TagModuleAction.action";
	String newtagName = "AT" + randomChar() + getSecondsStamp();
	String newValName = "V" + randomChar() + getSecondsStamp();
	String incidentID = readConfigFile("incidentID");
	String incidentID_full = readConfigFile("incidentID_full");

	@BeforeTest
	public void startBrowser() throws Exception {

		loginInIEAndSelectIncident();
	}

	@Test(priority = 1)
	public void createTAG(Method method) throws Exception {
		startLine(method);
		navigateToPage(TAG_MANAGEMENT_URL);
		getHandleToWindow("Uleaf - Tag Management");
		getHandleToWindow("Uleaf - Tag Management").manage().window()
				.maximize();
		click("link_TagManagement__CreateTag");
		explicitlyWaitForElement("textField_TagManagement_CreateTag_TagName",
				getTotalWaitTimeInSecs());
		sendKeys("textField_TagManagement_CreateTag_TagName", newtagName);
		sendKeys("textField_TagManagement_CreateTag_TagDesc", "AutoTag Desc");
		click("button_TagManagement_CreateTag_Search");
		click("button_TagManagement_CreateTag_Create");
		String Expected = "Tag Added"; // Tag added successful message
		verifyTextContains(Expected, "text_TagManagement_TagAdded_SuccMsg");
		endLine(method);
	}

	@Test(priority = 2)
	public void AddTAGValueToExistingTAG(Method method) throws Exception {

		startLine(method);
		click("button_TagManagement_CreateTag_Values");
		getHandleToWindow("Values");
		getHandleToWindow("Values").manage().window().maximize();
		sendKeys("textField_TagManagement_Values_SelectExistValues", "v");
		click("textField_TagManagement_Values_ValuesToExistTag");
		click("button_TagManagement_Values_ExistValue_AddButton");
		click("button_TagManagement_Values_Submit");
		getHandleToWindow("Messages");
		getHandleToWindow("Messages").close();
		getHandleToWindow("Uleaf - Tag Management");
	}

	

}
