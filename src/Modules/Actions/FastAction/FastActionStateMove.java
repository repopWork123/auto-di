package Modules.Actions.FastAction;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Modules.AuditLogs.AuditLogSearch;
import ReusableUtilities.Report;

import org.testng.annotations.DataProvider;

import ReusableUtilities.Dataprovider;

public class FastActionStateMove extends FastAction {
	public String OFFICER_NAME = "SurName";
	public String FOR_ALLOCATION = "For Allocation";
	public String ALLOCATED = "Allocated";
	public String SUBMITTED = "Submitted";
	public String RECEIVED = "Received";
	protected static String actionThemeOne;
	protected static String actionThemeTwo;
	protected static String userName;
	protected static String passWord;
	private String fieldDateEmpty = "";

	// Data for Login read from Xl sheet
	private String linkedDocumentsRecordURN2;


	// private String takenDocumentsRecordURN;

	//@DataProvider(name = "login")
	public String[][] dataProvider() {

		String[][] testData = Dataprovider.readExcelData("Actions",
				testDataFilePath, "Login");
		return testData;
	}



	// default constructor
	public FastActionStateMove() {
		softAssert = new SoftAssert();
	}

	@BeforeTest
	public void beforeTest() throws Exception {
		// Steps starting from IE driver instance creation, Navigate to app URl,
		// Login , select default incident on Home Page are executed
		loginInIEAndSelectIncident();
		//		loginAndSelectIncident();

	}
	
	@Test
	public void testData() {
		actionThemeOne = readConfigFile("actionThemeOne");
		actionThemeTwo = readConfigFile("actionThemeTwo");
		userName = readConfigFile("username2");
		passWord = readConfigFile("password2");

		System.out.println("userName: " + userName);
		System.out.println("passWord: " + passWord);
		System.out.println("actionThemeOne: " + actionThemeOne);
		System.out.println("actionThemeTwo: " + actionThemeTwo);

	}

	// Test case 8608:001. Fast Action - Single Queue Move-For Allocation to
	// Allocated
	@Test
	//(priority = 2)
	public void fastAction_Queue_Allocated(Method method) throws Exception {
		startLine(method);
		raiseAndGetFastActionURN();
		verifyText(FOR_ALLOCATION, "label_Action_View_State");
		// getList and verify state in dropdown
		getAllListItemAndVerify(ALLOCATED, "dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",
				getTotalWaitTimeInSecs());
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficer",
				getTotalWaitTimeInSecs());
		verifyFastActionFieldsInSingleQueueMove();
		String text = "";
		verifyTextCompare(text, getTextFieldText());
		verifyText(ALLOCATED, "label_Action_Create_State");
		verifyAssociatedDocuments();
		verifyAllocatedOfficer();
		verifyReceiversInstructions();
		click("checkBox_Action_Themes_Item1");
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_CreateOfficer");
		explicitlyWaitForElement("textfield_Action_CreateOfficer_Surname", getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_Forename",
				CREATE_OFFICER_FORENAME);
		clickAndWait("button_Action_FastAction_Edit_CreateOfficeDialog_Create");
		explicitlyWaitForElement("textfield_Action_FastAction_QueueAllocatedOfficer",
				getTotalWaitTimeInSecs());
		click("textfield_Action_FastAction_QueueAllocatedOfficer");
		sendKeysAndEnter("textfield_Action_FastAction_QueueAllocatedOfficer",
				CREATE_OFFICER_SURNAME);
		explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",
				getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_StateMove_OfficerName");
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficerName",
				getTotalWaitTimeInSecs());
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_FastAction_QueueAllocatedOfficerName");
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(ALLOCATED, "label_Action_View_State");
		// getList and verify item in dropdown
		getAllListItemAndVerify(SUBMITTED, "dropDown_Action_Select_State");
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textField_Action_view_CreateOfficer");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE,
				getTextFieldText());
		verifyText(fieldDateEmpty,
				"textField_Action_FastAction_Create_ReceiversInstructions");
		verifyText(actionThemeOne, "table_Action_ThemeItem");

		notificationCheck();
		endLine(method);

	}

	// Test case 9470:002. Fast Action - Single Queue Move-Allocated to
	// Submitted
	// Submitted
	@Test
	//(priority = 3,dependsOnMethods = {"fastAction_Queue_Allocated"})
	public void fastAction_Queue_Submitted(Method method) throws Exception {
		startLine(method);
		viewAndGetFastActionURN();
		verifyText(ALLOCATED, "label_Action_View_State");
		// getList and verify state in dropdown
		getAllListItemAndVerify(SUBMITTED, "dropDown_Action_Select_State");
		getAllListItemAndVerify(RECEIVED, "dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",
				getTotalWaitTimeInSecs());
		verifyFastActionFieldsInSingleQueueMove();
		verifyText(SUBMITTED, "label_Action_Create_State");
		verifySubmissionText();
		verifyReceiversInstructions();
		// click("checkBox_Action_Themes_Item1");
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(SUBMITTED, "label_Action_View_State");
		getAllListItemAndVerify(RECEIVED, "dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION, "dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE,
				getTextandResultFieldText());
		verifyText(fieldDateEmpty,
				"textField_Action_FastAction_Create_ReceiversInstructions");
		endLine(method);

	}

	// Test case 9481:003. Fast Action - Single Queue Move-Submitted to Received
	// Received-single
	@Test
	//(priority = 4,dependsOnMethods = {"fastAction_Queue_Submitted"})
	public void fastAction_Queue_Received(Method method) throws Exception {
		startLine(method);
		viewAndGetFastActionURN();
		verifyText(SUBMITTED, "label_Action_View_State");
		// getList and verify state in dropdown
		getAllListItemAndVerify(RECEIVED, "dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION, "dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",
				getTotalWaitTimeInSecs());
		verifyFastActionFieldsInSingleQueueMove();
		verifyText(RECEIVED, "label_Action_Create_State");
		verifyReceiversInstructions();
		click("checkBox_Action_Themes_Item2");
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		sendKeys("textField_Action_FastAction_Edit_ReceiversInstructions",
				TEXTFIELD_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(RECEIVED, "label_Action_View_State");
		getAllListItemAndVerify(RESULTED, "dropDown_Action_Select_State");
		getAllListItemAndVerify(PARTIALLY_RESULTED,
				"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION, "dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE,
				getTinyMCEText("tinyMCE_Action_View_AllText"));
		verifyText(TEXTFIELD_TEXT_FOR_SINGLE_QUEUE_MOVE,
				"textField_Action_FastAction_Create_ReceiversInstructions");
		verifyText(actionThemeTwo, "table_Action_ThemeItem2");
		endLine(method);

	}

	// Test case 9485:005. Fast Action -Single Queue Move-Received to Resulted
	// Resulted-single
	@Test
	//(priority = 5 ,dependsOnMethods = {"fastAction_Queue_Received"})
	public void fastAction_Queue_Resulted(Method method) throws Exception {
		startLine(method);
		viewAndGetFastActionURN();
		verifyText(RECEIVED, "label_Action_View_State");
		// getList and verify state in dropdown
		getAllListItemAndVerify(RESULTED, "dropDown_Action_Select_State");
		getAllListItemAndVerify(PARTIALLY_RESULTED,
				"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION, "dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",
				getTotalWaitTimeInSecs());
		verifyFastActionFieldsInResultedPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);

		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		// Adding Document Taken Grid- URN
		explicitlyWaitForElement("button_Action_ChangeState_DocumentsTaken_Add", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_ChangeState_DocumentsTaken_Add");
		explicitlyWaitAndGetWebElement(
				"textField_Action_Update_DocumentTaken_Document_URN",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",
				linkedDocumentsRecordURN2);
		click("dropDown_Action_SelectRecord_ReferenceIncident");
		pressReturnKey("dropDown_Action_SelectRecord_ReferenceIncident");
		clickAndWait("button_Action_Update_DocumentTaken_Documents_Add");
		explicitlyWaitForElement("button_Action_ChangeState_Save", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(RESULTED, "label_Action_View_State");
		getAllListItemAndVerify(FILED, "dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION, "dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE,
				getAllTextFieldText());
		verifyText(TEXTFIELD_TEXT_FOR_SINGLE_QUEUE_MOVE,
				"textField_Action_FastAction_Create_ReceiversInstructions");
		endLine(method);
	}

	// Test case 9492:006. Fast Action - Single Queue Move- Resulted to Filed
	// Filed-single
	@Test
	//(priority = 6,dependsOnMethods = {"fastAction_Queue_Resulted"})
	public void fastAction_Queue_Filed(Method method) throws Exception {
		startLine(method);
		viewAndGetFastActionURN();
		verifyText(RESULTED, "label_Action_View_State");
		// getList and verify state in dropdown
		getAllListItemAndVerify(FILED, "dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION, "dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",
				getTotalWaitTimeInSecs());
		verifyFastActionFieldsInFiledPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		//click("checkBox_Action_Themes_Item1");
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(FILED, "label_Action_View_State");
		getAllListItemAndVerify(FOR_ALLOCATION, "dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE,
				getAllTextFieldText());
		verifyText(TEXTFIELD_TEXT_FOR_SINGLE_QUEUE_MOVE,
				"textField_Action_FastAction_Create_ReceiversInstructions");
		endLine(method);
	}

	// Test case 9489:004. Fast Action - Single Queue Move-Received to Partially
	// Resulted
	// Partially Resulted-single
	@Test
	//(priority = 7, dependsOnMethods = {"fastAction_Queue_Filed"})
	public void fastAction_Queue_PartiallyResulted(Method method)
			throws Exception {

		startLine(method);
		raiseAndGetFastActionURN();

		// change state to allocated and check mandatory fields
		clickAndWait("button_Action_ChangeState");
		changeStateAllocated(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);

		// change state to submitted and check mandatory fields
		clickAndWait("button_Action_ChangeState");
		changeStateSubmitted(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE,
				TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);

		// change state to received (No Mandatory fields)
		clickAndWait("button_Action_ChangeState");
		changeStateReceived(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE,
				TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);

		verifyText(RECEIVED, "label_Action_View_State");
		// getList and verify state in dropdown
		getAllListItemAndVerify(RESULTED, "dropDown_Action_Select_State");
		getAllListItemAndVerify(PARTIALLY_RESULTED,
				"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION, "dropDown_Action_Select_State");
		selectByVisibleText("dropDown_Action_Select_State", PARTIALLY_RESULTED);
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",
				getTotalWaitTimeInSecs());
		verifyFastActionFieldsInPartially_ResultedPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);

		// Adding Document Taken Grid- URN
		clickAndWait("button_Action_ChangeState_DocumentsTaken_Add");
		explicitlyWaitAndGetWebElement(
				"textField_Action_Update_DocumentTaken_Document_URN",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",
				linkedDocumentsRecordURN2);
		click("dropDown_Action_SelectRecord_ReferenceIncident");
		pressReturnKey("dropDown_Action_SelectRecord_ReferenceIncident");
		clickAndWait("button_Action_Update_DocumentTaken_Documents_Add");
		explicitlyWaitAndGetWebElement(
				"button_Action_ChangeState_Save",
				getTotalWaitTimeInSecs());
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		explicitlyWaitForElement("label_Action_View_State", getTotalWaitTimeInSecs());
		verifyText(PARTIALLY_RESULTED, "label_Action_View_State");
		getAllListItemAndVerify(ALLOCATED, "dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION, "dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE,
				getTextandResultFieldText());
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE,
				getAllTextFieldText());
		verifyText(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE,
				"textField_Action_FastAction_Create_ReceiversInstructions");
		endLine(method);

	}

	/*
	 * Test case 9497: 007, Single Queue move - Cancel button
	 */
	@Test
	//(priority = 8)
	public void fastAction_CancelQueueMove(Method method) throws Exception {
		startLine(method);
		raiseAndGetFastActionURN();

		// change state to allocated checking cancel button
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("label_Action_Urn_QueueMove",
				getTotalWaitTimeInSecs());
		verifyFastActionFieldsInAllocatedPage();
		click("button_Action_ChangeState_CreateOfficer");
		explicitlyWaitForElement("textfield_Action_CreateOfficer_Surname", getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_Forename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_FastAction_Edit_CreateOfficeDialog_Create");
		click("textfield_Action_FastAction_QueueAllocatedOfficer");
		sendKeysAndEnter("textfield_Action_FastAction_QueueAllocatedOfficer",
				CREATE_OFFICER_SURNAME);
		explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",
				getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_StateMove_OfficerName");
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficerName",
				getTotalWaitTimeInSecs());
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_FastAction_QueueAllocatedOfficerName");
		// click cancel
		click("button_Action_FastAction_Move_State_Cancel");
		verifyText(ALLOCATION, "label_Action_View_State");
		clickAndWait("button_Action_ChangeState");
		changeStateAllocated("");

		// change state to submitted checking cancel button
		clickAndWait("button_Action_ChangeState");
		verifyFastActionFieldsInSubmittedPage("");
		click("button_Action_FastAction_Move_State_Cancel");
		verifyText(ALLOCATED, "label_Action_View_State");
		clickAndWait("button_Action_ChangeState");
		changeStateSubmitted("", "automation testing");

		// change state to received checking cancel button
		clickAndWait("button_Action_ChangeState");
		verifyFastActionFieldsInReceivedPage("automation testing");
		click("button_Action_FastAction_Move_State_Cancel");
		verifyText(SUBMITTED, "label_Action_View_State");
		clickAndWait("button_Action_ChangeState");
		changeStateReceived("automation testing", "Instructions Received");

		// change state to Resulted checking cancel button
		clickAndWait("button_Action_ChangeState");
		verifyFastActionFieldsInResultedPage("automation testing");
		click("button_Action_FastAction_Move_State_Cancel");
		verifyText(RECEIVED, "label_Action_View_State");
		clickAndWait("button_Action_ChangeState");
		changeStateResulted("automation testing",
				"Automation testing - resulted");

		// change state to Filed checking cancel button
		clickAndWait("button_Action_ChangeState");
		verifyFastActionFieldsInFiledPage("Automation testing - resulted");
		click("button_Action_FastAction_Move_State_Cancel");
		verifyText(RESULTED, "label_Action_View_State");
		clickAndWait("button_Action_ChangeState");
		changeStateFiled("Automation Testing - Filed");

		endLine(method);
	}

	/*
	 * Test case 26505: 007A. Single Queue Move- Missing Mandatory Field Value
	 */
	@Test
	//(priority = 9)
	public void fastAction_Change_State_Mandatory_Fields(Method method)
			throws Exception {
		startLine(method);
		raiseAndGetFastActionURN();

		// change state to allocated and check mandatory fields
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("label_Action_Urn_QueueMove",
				getTotalWaitTimeInSecs());
		verifyFastActionFieldsInAllocatedPage();
		clickAndWait("button_Action_ChangeState_Save");
		// check error message
		explicitlyWaitForElement("errorMsg_Action_Move_State",
				getTotalWaitTimeInSecs());
		verifyElementPresent("errorMsg_Action_Move_State");
		verifyText("Allocated Officer is required",
				"errorMsg_Action_Move_State");
		changeStateAllocated("");

		// change state to submitted and check mandatory fields
		clickAndWait("button_Action_ChangeState");
		// verifyFastActionFieldsInSubmittedPage("");
		clickAndWait("button_Action_ChangeState_Save");
		// check error message
		explicitlyWaitForElement("errorMsg_Action_Move_State",
				getTotalWaitTimeInSecs());
		verifyElementPresent("errorMsg_Action_Move_State");
		verifyText("Submission Text is required", "errorMsg_Action_Move_State");
		changeStateSubmitted("", "automation testing");

		// change state to received (No Mandatory fields)
		clickAndWait("button_Action_ChangeState");
		changeStateReceived("automation testing", "Instructions Received");

		// change state to Resulted checking cancel button
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("label_Action_Urn_QueueMove",
				getTotalWaitTimeInSecs());
		verifyFastActionFieldsInResultedPage("automation testing");
		clickAndWait("button_Action_ChangeState_Save");
		// check error message
		explicitlyWaitForElement("errorMsg_Action_Move_State", getTotalWaitTimeInSecs());
		verifyElementPresent("errorMsg_Action_Move_State");
		verifyText("Result Text is required", "errorMsg_Action_Move_State");
		changeStateResulted("automation testing",
				"Automation testing - resulted");

		// change state to Filed checking cancel button
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("label_Action_Urn_QueueMove",
				getTotalWaitTimeInSecs());
		verifyFastActionFieldsInFiledPage("Automation testing - resulted");
		clickAndWait("button_Action_ChangeState_Save");
		// check error message
		explicitlyWaitForElement("errorMsg_Action_Move_State",
				getTotalWaitTimeInSecs());
		verifyElementPresent("errorMsg_Action_Move_State");
		verifyText("Filing Reason is required", "errorMsg_Action_Move_State");
		changeStateFiled("Automation Testing - Filed");

		endLine(method);
	}

	/*
	 * 007B. Single Queue Move- Large Text Fields and Special Characters
	 */
	@Test
	//(priority = 10)
	public void FastAction_Change_State_Large_Fields(Method method)
			throws Exception {
		startLine(method);
		String docContent = readTextFile(System.getProperty("user.dir")
				+ "\\hugeDataForTextField.txt");

		raiseAndGetFastActionURN();

		// change state to allocated and check mandatory fields
		clickAndWait("button_Action_ChangeState");
		changeStateAllocated(docContent);

		// change state to submitted and check mandatory fields
		clickAndWait("button_Action_ChangeState");
		changeStateSubmitted(docContent, docContent);

		// change state to received (No Mandatory fields)
		clickAndWait("button_Action_ChangeState");
		changeStateReceived(docContent, docContent);

		// change state to Resulted checking cancel button
		clickAndWait("button_Action_ChangeState");
		changeStateResulted(docContent, docContent);

		// change state to Filed checking cancel button
		clickAndWait("button_Action_ChangeState");
		changeStateFiledForChangeStateLargeFields(docContent);

		endLine(method);
	}

	/* Test case 9530:012. Multiple Queue Move-For Allocation to Allocated */
	// Predata: Office need to create
	@Test
	//(priority = 11)
	public void fastAction_MultipleQueue_Allocated(Method method)
			throws Exception {
		startLine(method);
		// raise Bulk fast action
		raiseFastActionBulkRecord();
		// Search Fast Action By ForAllocation state
		searchFastActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(FOR_ALLOCATION);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		explicitlyWaitForElement("dropDown_Action_BulkStateMove_Popup_AllowedStates", getTotalWaitTimeInSecs());
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");

		// verify header Moving BulkState move :allocated
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		// verify BulkUpdate tab
		verifyElementPresent("tab_Action_BulkUpdate");
		// verify label Moving and allocated URN
		explicitlyWaitForElement("label_Action_BulkStateMove_Allocated", getTotalWaitTimeInSecs());
		verifyElementPresent("label_Action_BulkStateMove_Allocated");
		// verifying incidents,URN,State and Fast Action		
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_FastActionFirstURN + "Fast Action , "
				+ incidentIdFull + " - " + searchResult_FastActionSecondURN
				+ "Fast Action to : Allocated",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(ALLOCATED, "label_Action_BulkUpdate_State");
		// verify theme
		verifyElementPresent("checkBox_Action_Themes_Item1");
		// Verify Tag link
		verifyElementPresent("link_Action_BulkUpdate_MoveAll_Tags");
		// Entering officer name
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficer",
				getTotalWaitTimeInSecs());
		sendKeysAndEnter("textfield_Action_FastAction_QueueAllocatedOfficer",
				OFFICER_NAME);
		explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",
				getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_StateMove_OfficerName");
		// verify cancel button in bulk update page
		verifyElementPresent("button_Action_BulkUpdate_Cancel");
		verifyElementPresent("button_Action_BulkUpdate_Save");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_FastActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_FastActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move :allocated
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(FOR_ALLOCATION, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		String firstBulkMoveURN1 = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
		// click Move all button
		/*click("button_Action_BulkSateMovePopup_MoveIndividual");*/
		// get Handle to Move Individual update window
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Cancel");
		// verify Skip button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Skip");
		// verify apply button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Apply");
		// verify MoveIndividual button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual");
		// verify label MoveIndividual
		verifyElementPresent("label_Action_BulkStateMove_MoveIndividual_Allocated");
		// Verify URN and state
		// Move Fast Action A27 in Incident MORSTA16M21 to Allocated
		verifyTextContains("Move Fast Action " + firstBulkMoveURN1
				+ " in Incident " + incidentIdFull + " to Allocated",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state allocted
		// verifyText(ALLOCATED,"label_Action_BulkUpdate_MoveIndividual_State");
		// verify theme
		verifyElementPresent("checkBox_Action_Themes_Item1");
		// Entering officer name
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficer",
				getTotalWaitTimeInSecs());
		sendKeysAndEnter("textfield_Action_FastAction_QueueAllocatedOfficer",
				OFFICER_NAME);
		explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",
				getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_StateMove_OfficerName");
		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");

		explicitlyWaitForElement(
				"button_Action_BulkUpdate_MoveIndividual_Update_OK",
				getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(firstBulkMoveURN1, "successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated", "successMsg_Action_Update");
		// click OK button
		/*		click("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// getHandle to parent Action Dashboard window
		getHandleToWindow("Action Dashboard");*/
		/* clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK");*/
		Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCount, 1);

		endLine(method);
	}

	/* Test case 9576:010. Cancel button in Bulk State Move Popup */
	@Test
	//(priority = 12)
	public void fastAction_BulkUpdatePopup_Cancel(Method method)
			throws Exception {
		startLine(method);
		// navigate to Action Dash board
		navigateTo(ACTION_DASHBOARD);
		explicitlyWaitForElement("dropDown_Action_ActionDashboard_Search_Type",
				getTotalWaitTimeInSecs());
		// Search Fast Action in Drop down
		selectByVisibleText("dropDown_Action_ActionDashboard_Search_Type",
				SEARCH_TYPE);
		// Enter Title field for search
		sendKeys("textField_Action_Title",
				RAISE_FAST_ACTION_MULTIPLE_QUEU_TITLE);
		// Select For Allocation State
		click("button_Action_ActionDashboard_Search_State");
		click("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		// click Search button
		click("button_Action_Search");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verifySearchResultTabEnabled();
		// select multiple recored in search result
		click("checkBox_Action_SearchResulttab_Firstrow");
		click("checkBox_Action_SearchResulttab_Secondrow");
		// getting bulk state move URNs
		String firstBulkMoveURN = getText("table_Action_SearchResults_URN");
		String secondBulkMoveURN = getText("table_Action_SearchResults_SecondRow_URN");
		// verify For allocation State
		verifyText(FOR_ALLOCATION, "table_Action_SearchResults_FirstRow_State");
		verifyText(FOR_ALLOCATION, "table_Action_SearchResults_SecondRow_State");

		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
		// click Cancel button
		click("button_Action_BulkSateMovePopup_Cancel");

		endLine(method);

	}

	/*
	 * Test case 9564:013. Multiple Queue Move-Allocated to Submitted
	 */
	@Test
	//(priority = 13,dependsOnMethods = {"fastAction_MultipleQueue_Allocated"})
	public void fastAction_MultipleQueue_AllocatedTo_Submitted(Method method)
			throws Exception {
		startLine(method);
		// Search Fast Action By Submitted state
		//explicitlyWaitForElement("checkBox_Action_ActionDashboard_Search_State_Allocated", getTotalWaitTimeInSecs());
		searchFastActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Allocated");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(ALLOCATED);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popu window
		verifyFastActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(RECEIVED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", SUBMITTED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInFastActionBulkUpdate();
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_FastActionFirstURN + ", " + incidentIdFull
				+ " - " + searchResult_FastActionSecondURN + ": Submitted",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(SUBMITTED, "label_Action_BulkUpdate_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("SubmittedText");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_FastActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_FastActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move :Submitted
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(ALLOCATED, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		searchResult_FastActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyFastActionBulkUpdate_Popup();
		getAllListItemAndVerify(RECEIVED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", SUBMITTED);
		// click Move all button
		/*click("button_Action_BulkSateMovePopup_MoveIndividual");*/
		// get Handle to Move Individual update window
		/*getHandleToWindow("Soft Entity Portlet");*/
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		// verify all element in bulk update individual page
		verifyAllElementsInFastActionBulkUpdate_Individual();
		// Verify URN and state
		// Move Fast Action A27 in Incident MORSTA16M21 to Allocated
		verifyTextContains("Move " + searchResult_FastActionFirstURN
				+ " in Incident " + incidentIdFull + " to Submitted",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state allocted
		// verifyText(ALLOCATED,"label_Action_BulkUpdate_MoveIndividual_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("SubmittedText");

		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");

		explicitlyWaitForElement(
				"button_Action_BulkUpdate_MoveIndividual_Update_OK",
				getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(searchResult_FastActionFirstURN,
				"successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated", "successMsg_Action_Update");
		Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCount, 1);

		endLine(method);
	}

	/*
	 * Test case 9568:014. Multiple Queue Move- Submitted to Received
	 */
	@Test
	//(priority = 14,dependsOnMethods = {"fastAction_MultipleQueue_AllocatedTo_Submitted"})
	public void fastAction_MultipleQueue_SubmittedTo_Received(Method method)
			throws Exception {
		startLine(method);
		// Search Fast Action By Submitted state
		//explicitlyWaitForElement("checkBox_Action_ActionDashboard_Search_State_Submitted", getTotalWaitTimeInSecs());
		searchFastActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Submitted");
		// verifySearchResultTabEnabled();
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(SUBMITTED);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popu window
		verifyFastActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(RECEIVED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", RECEIVED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInFastActionBulkUpdate();
		// verify element Receive instructions
		verifyElementPresent("textField_Action_FastAction_Edit_ReceiversInstructions");
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_FastActionFirstURN + ", " + incidentIdFull
				+ " - " + searchResult_FastActionSecondURN + ": Submitted",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(RECEIVED, "label_Action_BulkUpdate_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("SubmittedText");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_FastActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_FastActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move :Submitted
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(SUBMITTED, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		searchResult_FastActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyFastActionBulkUpdate_Popup();
		getAllListItemAndVerify(RECEIVED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", RECEIVED);
		// click Move all button
		explicitlyWaitForElement("button_Action_BulkSateMovePopup_MoveIndividual",
				getTotalWaitTimeInSecs());

		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		// verify all element in bulk update individual page
		verifyAllElementsInFastActionBulkUpdate_Individual();
		// Verify URN and state
		verifyTextContains("Move " + searchResult_FastActionFirstURN
				+ " in Incident " + incidentIdFull + " to Received",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state allocted
		// verifyText(ALLOCATED,"label_Action_BulkUpdate_MoveIndividual_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("Received Text");

		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");

		explicitlyWaitForElement(
				"button_Action_BulkUpdate_MoveIndividual_Update_OK",
				getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(searchResult_FastActionFirstURN,
				"successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated", "successMsg_Action_Update");
		// click OK button

		Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCount, 1);

		endLine(method);
	}

	/*
	 * Test case 9578:011. Cancel button in Bulk Update screen TODO
	 */
	@Test
	//(priority = 15)
	public void fastAction_BulkUpdate_Cancel(Method method) throws Exception {
		startLine(method);
		// Search Fast Action By ForAllocation state
		searchFastActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(FOR_ALLOCATION);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popu window
		verifyFastActionBulkUpdate_Popup();
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInFastActionBulkUpdate();
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_FastActionFirstURN + "Fast Action , "
				+ incidentIdFull + " - " + searchResult_FastActionSecondURN
				+ "Fast Action to : Allocated",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(ALLOCATED, "label_Action_BulkUpdate_State");
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficer",
				getTotalWaitTimeInSecs());
		sendKeysAndEnter("textfield_Action_FastAction_QueueAllocatedOfficer",
				OFFICER_NAME);
		explicitlyWaitForElement("button_Action_BulkUpdate_Cancel",
				getTotalWaitTimeInSecs());
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Cancel");
		endLine(method);
	}

	/*
	 * Test case 9571:015. Multiple Queue Move- Received to Resulted TODO
	 */
	@Test
	//(priority = 16,dependsOnMethods = {"fastAction_MultipleQueue_SubmittedTo_Received"})
	public void fastAction_MultipleQueue_ReceivedTo_Resulted(Method method)
			throws Exception {
		startLine(method);
		// Search Fast Action By Submitted state
		//explicitlyWaitForElement("checkBox_Action_ActionDashboard_Search_State_Received", getTotalWaitTimeInSecs());
		searchFastActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Received");
		// verifySearchResultTabEnabled();
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(RECEIVED);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popu window
		verifyFastActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(FOR_ALLOCATION,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", RESULTED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInFastActionBulkUpdate();
		// verify element Receive instructions
		verifyElementPresent("textField_Action_FastAction_Edit_ReceiversInstructions");
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_FastActionFirstURN + ", " + incidentIdFull
				+ " - " + searchResult_FastActionSecondURN + ":  Resulted",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(RESULTED, "label_Action_BulkUpdate_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("SubmittedText");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_FastActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_FastActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move :Submitted
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(RECEIVED, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		searchResult_FastActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyFastActionBulkUpdate_Popup();
		getAllListItemAndVerify(FOR_ALLOCATION,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", RESULTED);
		// click Move all button
		/*click("button_Action_BulkSateMovePopup_MoveIndividual");*/
		// get Handle to Move Individual update window
		/*getHandleToWindow("Soft Entity Portlet");*/
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		// verify all element in bulk update individual page
		verifyAllElementsInFastActionBulkUpdate_Individual();
		// Verify URN and state
		verifyTextContains("Move " + searchResult_FastActionFirstURN
				+ " in Incident " + incidentIdFull + " to Received",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("Received Text");

		// click Move Individual button
		explicitlyWaitForElement("button_Action_BulkUpdate_MoveIndividual",
				getTotalWaitTimeInSecs());
		click("button_Action_BulkUpdate_MoveIndividual");

		explicitlyWaitForElement(
				"button_Action_BulkUpdate_MoveIndividual_Update_OK",
				getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(searchResult_FastActionFirstURN,
				"successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated", "successMsg_Action_Update");
		// click OK button

		Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCount, 1);

		endLine(method);
	}

	/*
	 * Test case 9573:016. Multiple Queue Move- Resulted to Filed TODO
	 */
	@Test
	//(priority = 17,dependsOnMethods = {"fastAction_MultipleQueue_ReceivedTo_Resulted"})
	public void fastAction_MultipleQueue_ResultedTo_Filed(Method method)
			throws Exception {
		startLine(method);
		// Search Fast Action By Submitted state
		//explicitlyWaitForElement("checkBox_Action_ActionDashboard_Search_State_Resulted", getTotalWaitTimeInSecs());
		searchFastActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Resulted");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(RESULTED);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popu window
		verifyFastActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(FOR_ALLOCATION,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", FILED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInFastActionBulkUpdate();
		// verify element Receive instructions
		verifyElementPresent("textField_Action_FastAction_Edit_ReceiversInstructions");
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_FastActionFirstURN + ", " + incidentIdFull
				+ " - " + searchResult_FastActionSecondURN + ": Filed",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(FILED, "label_Action_BulkUpdate_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("Submitted Text");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_FastActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_FastActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move :Submitted
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(RESULTED, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		searchResult_FastActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyFastActionBulkUpdate_Popup();
		getAllListItemAndVerify(FOR_ALLOCATION,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from drop down
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", FILED);
		// click Move all button

		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		// verify all element in bulk update individual page
		verifyAllElementsInFastActionBulkUpdate_Individual();
		// Verify URN and state
		verifyTextContains("Move " + searchResult_FastActionFirstURN
				+ " in Incident " + incidentIdFull + " to ",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state allocted
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("Received Text");
		explicitlyWaitForElement("button_Action_BulkUpdate_MoveIndividual",
				getTotalWaitTimeInSecs());
		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");

		explicitlyWaitForElement(
				"button_Action_BulkUpdate_MoveIndividual_Update_OK",
				getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(searchResult_FastActionFirstURN,
				"successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated", "successMsg_Action_Update");
		// click OK button

		Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCount, 1);

		endLine(method);
	}

	/*
	 * Test case 9512:009. Multiple Queue Move-Move ALL, Apply & Move Individual
	 * & Skip TODO
	 */
	@Test
	//(priority = 18)
	public void fastAction_MultipleQueue_MoveAllAndMoveIndividual(Method method)
			throws Exception {
		startLine(method);
		// raise Bulk fast action
		raiseFastActionBulkRecord();
		// Search Fast Action By Submitted state
		searchFastActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(FOR_ALLOCATION);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popu window
		verifyFastActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(ALLOCATED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInFastActionBulkUpdate();
		// verify element Receive instructions
		verifyElementPresent("textField_Action_FastAction_Edit_ReceiversInstructions");
		// verifying incidents,URN,State and Fast Action
		// verify label Moving and allocated URN
		verifyElementPresent("label_Action_BulkStateMove_Allocated");
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_FastActionFirstURN + "Fast Action , "
				+ incidentIdFull + " - " + searchResult_FastActionSecondURN
				+ "Fast Action to : Allocated",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(ALLOCATED, "label_Action_BulkUpdate_State");
		// verify theme
		verifyElementPresent("checkBox_Action_Themes_Item1");
		// Verify Tag link
		verifyElementPresent("link_Action_BulkUpdate_MoveAll_Tags");
		// Entering officer name
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficer",
				getTotalWaitTimeInSecs());
		insertTextInAllocatedOfficerTextField("textfield_Action_FastAction_QueueAllocatedOfficer",OFFICER_NAME);
		selectOptionWithText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME, "textfield_Action_StateMove_AllocatedOfficerName");

		// verify state allocted
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_FastActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_FastActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");
		waitForPageLoad();
		// MoveIndividual StateMove
		// verify header Moving BulkState move :Submitted
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(FOR_ALLOCATION, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Secondrow");
		clickAndWait("checkBox_Action_SearchResulttab_Thirdrow");
		// getting first URN from search table
		searchResult_FastActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyFastActionBulkUpdate_Popup();
		getAllListItemAndVerify(ALLOCATED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from drop down
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// click Move all button
		// get Handle to Move Individual update window
		String parentWinHandle = getCurrentWindowHandle();
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		// verify all element in bulk update individual page
		verifyAllElementsInFastActionBulkUpdate_Individual();
		// Verify URN and state
		verifyTextContains("Move " + searchResult_FastActionFirstURN
				+ " in Incident " + incidentIdFull + " to ",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state allocted
		// Entering officer name
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficer",
				getTotalWaitTimeInSecs());
		insertTextInAllocatedOfficerTextField("textfield_Action_FastAction_QueueAllocatedOfficer",OFFICER_NAME);
		selectOptionWithText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME, "textfield_Action_StateMove_AllocatedOfficerName");

		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");


		verifyTextContains("updated", "successMsg_Action_Update"); // click
		/*//  OK button 
		  click("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		 */

		// getHandle to parent Action Dashboard window
		getHandleToWindow("Action Dashboard");

		getHandleToWindow("Soft Entity Portlet");
		click("button_Action_BulkUpdate_MoveIndividual_Skip");
		verifyTextContains("Previous entity skipped","successMsg_Action_Update");
		explicitlyWaitForElement("button_Action_BulkUpdate_MoveIndividual_Cancel", getTotalWaitTimeInSecs());
		click("button_Action_BulkUpdate_MoveIndividual_Cancel");
		// getHandle to parent Action Dashboard window
		getHandleToWindow("Action Dashboard");


		endLine(method);
	}

	/* Test case 9501:008. Audit Log- Action State Moves */
	@Test
	(priority = 19)
	public void FastAction_AuditLog_SingleStateMoves(Method method)
			throws Exception {
		startLine(method);
		// handling the exception : Type error
		if(isAlertPresent()){
			acceptAlertBox(driver);
		}
		AuditLogSearch auditLog = new AuditLogSearch(driver);
		// verify Audit Single Allocated
		auditLog.auditLogSearchByActivity("Allocated Fast Action");

		verifyAuditLogSearchResult("Allocated Fast Action");

		// verify Audit Single Submitted
		auditLog.auditLogSearchByActivity("Submitted Fast Action");
		verifyAuditLogSearchResult("Submitted Fast Action");

		// verify Audit Single Resulted
		auditLog.auditLogSearchByActivity("Resulted Fast Action");
		verifyAuditLogSearchResult("Resulted Fast Action");

		// verify Audit Single Received
		auditLog.auditLogSearchByActivity("Received Fast Action");
		verifyAuditLogSearchResult("Received Fast Action");

		// verify Audit Single Filed
		auditLog.auditLogSearchByActivity("Filed Fast Action");
		verifyAuditLogSearchResult("Filed Fast Action");
		endLine(method);

	}

	/* Test case 9581:017. Audit Log- Multiple Queue Moves */
	@Test
	(priority = 20)
	public void fastAction_AuditLog_MultipleStateMoves(Method method)
			throws Exception {

		startLine(method);
		// verify Audit Multiple Allocated
		AuditLogSearch auditLog = new AuditLogSearch(driver);
		auditLog.auditLogSearchByActivity(ALLOCATED_FAST_ACTION);
		verifyAuditLogSearchResult(ALLOCATED_FAST_ACTION);

		// verify Audit Multiple Submitted
		auditLog.auditLogSearchByActivity(SUBMITTED_FAST_ACTION);
		verifyAuditLogSearchResult(SUBMITTED_FAST_ACTION);

		// verify Audit Multiple Resulted
		auditLog.auditLogSearchByActivity(RESULTED_FAST_ACTION);
		verifyAuditLogSearchResult(RESULTED_FAST_ACTION);

		// verify Audit Multiple Received
		auditLog.auditLogSearchByActivity(RECEIVED_FAST_ACTION);
		verifyAuditLogSearchResult(RECEIVED_FAST_ACTION);

		// verify Audit Multiple Filed
		auditLog.auditLogSearchByActivity(FILED_FAST_ACTION);
		verifyAuditLogSearchResult(FILED_FAST_ACTION);

		endLine(method);

	}

	public void verifyAuditLogSearchResult(String actvityDescription)
			throws Exception {
		verifyText(usernameUpperCase,
				"table_AuditLog_AuditRecord_FirstRecord_Username");
		verifyText(usernameUpperCase,
				"table_AuditLog_AuditRecord_SecondRecord_Username");
		verifyTextContains(incidentIdFull,
				"table_AuditLog_AuditRecord_FirstRecord_Incident");
		verifyTextContains(incidentIdFull,
				"table_AuditLog_AuditRecord_SecondRecord_Incident");
		verifyTextContains(actvityDescription,
				"table_AuditLog_AuditRecord_FirstRecord_Activity");
		verifyTextContains(actvityDescription,
				"table_AuditLog_AuditRecord_SecondRecord_Activity");

	}

	/*private void reLoginasAllocatedOfficer() throws Exception {
		// Relogin as allocated officer- surName
		handleSecurityWindow1();
		explicitlyWaitForElement("textField_Login_UserName",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Login_UserName", userName);
		sendKeys("textField_Login_PassWord", passWord);
		click("button_Login_SignIn");
		waitForPageLoad();
		selectAGroupInHomePage();
	}*/

	/*private void notificationCheck() throws Exception {

		// Log Out Method
		signOut();

		// Relogin as allocated officer
		reLoginasAllocatedOfficer();
		waitForJQuery(driver);
		waitForPageLoad();

		explicitlyWaitForElement("icon_Home_Workspace_Notification",
				getTotalWaitTimeInSecs());

		// Notification checking
		click("icon_Home_Workspace_Notification");
		// TODO- Liyakath( Notification click not working)
		explicitlyWaitForElement(
				"table_Action_Workspace_NotificationItems_FirstRow",
				getTotalWaitTimeInSecs());
		verifyTextContains("Low priority Action has been allocated to you",
				"table_Action_Workspace_NotificationItems_FirstRow");

		// endLine(method);

		// check Notification and sign out
		signOut();
		driver.close();
		// Relogin as System Admin
		loginInIEAndSelectIncident();

	}*/

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
