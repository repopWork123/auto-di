package Modules.Actions.FastAction;

import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.channels.AcceptPendingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import ReusableUtilities.Dataprovider;
import ReusableUtilities.Report;

public class FastActionActivityLog extends FastAction {
	private static final String FIELD_OFFICER = "SurName ForeName";
	// Data for Login read from Xl sheet
	private String userName, userName2;
	private String passWord, passWord2;
	private String searchResult_FastActionFirstURN;
	private String searchResult_FastActionSecondURN;
	private String linkedDocumentsRecordURN2;
	private String LinkedSeriesPrivateName;
	private String LinkedSeriesPublicName;
	private String LINKEDSERIESNAME;
	protected static final String RAISE_FAST_ACTION_TITLE = "Test Action";
	private static final String VISUALISATION_PORTLET_WINDOW_TITLE = "Visualisation Portlet";
	/*protected static final String CREATE_OFFICER_SURNAME = "Steve";
	protected static final String CREATE_OFFICER_FORENAME = "Jobs";*/

	//private String takenDocumentsRecordURN;

	//@DataProvider(name = "ActivityLog")
	public String[][] dataProvider() {

		String[][] testData = Dataprovider.readExcelData("Actions",
				testDataFilePath, "ActivityLog");
		return testData;
	}


	

		// default constructor
		public FastActionActivityLog() {
			softAssert = new SoftAssert();
		}

		@BeforeTest
		public void beforeTest() throws Exception {
			// Steps starting from IE driver instance creation, Navigate to app URl,
			// Login , select default incident on Home Page are executed
			loginInIEAndSelectIncident();
//			loginAndSelectIncident();
		}
		
		@Test
		public void testData(){
		
			
			userName = readConfigFile("username3");
			passWord = readConfigFile("password3");
			LinkedSeriesPublicName = readConfigFile("LinkedSeriesPublicName");
			LinkedSeriesPrivateName = readConfigFile("LinkedSeriesPrivateName");
			
			Report.info("userName: " + userName);
			Report.info("passWord: " + passWord);
		}
		
		/*
	 * Test Case 26510: 002. Fast Action Activity Log - Search
	 */
	 @Test(priority=2)
	public void fastActivityLogSearch(Method method) throws Exception {
		startLine(method);
		navigateTo(RAISE_FAST_ACTION_URL);
		// Wait for Create button on Action create page for 60secs
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		// Confirm Create Button on Action Create Page is present
		verifyElementPresent("button_Action_Raise");
		// Pass data only to mandatory fields
		sendKeys("textField_Action_Title", RAISE_FAST_ACTION_TITLE);
		click("button_Action_ChangeState_CreateOfficer");
		explicitlyWaitForElement("textfield_Action_Create_CreateOfficer_Surname", getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_Create_CreateOfficer_Surname", CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_Create_CreateOfficer_Forename", CREATE_OFFICER_FORENAME);
		click("button_Action_FastAction_CreateOfficeDialog_Create");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		// Verification of View Action Page
		verifyElementPresent("successMsg_Action_Create");
		String succMess = getText("successMsg_Action_Create");

		// navigate to Activity Log
		navigateTo(ACTIVITY_LOG_SEARCH_URL);
		verifyFastActionSearch();
		clickAndWait("button_Fast_Action_Activity_Log_Search");
		if(isAlertPresent()){
			Report.info("Trying to handle alert");
			waitForAlertTextAndClose(SEARCH_RESULT_LIMIT__1_000_RECORDS_POPUP);
		}
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());

		// test AND searching
		navigateTo(ACTIVITY_LOG_SEARCH_URL);
		verifyFastActionSearch();
		sendKeys("textField_Officer", CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME);
		sendKeys("textField_Title", RAISE_FAST_ACTION_TITLE);
		clickAndWait("button_Fast_Action_Activity_Log_Search");
		if(isAlertPresent()){
			waitForAlertTextAndClose(SEARCH_RESULT_LIMIT__1_000_RECORDS_POPUP);
		}
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		verifyActivityLogSearchResult();
		// test OR searching
		navigateTo(ACTIVITY_LOG_SEARCH_URL);
		verifyFastActionSearch();
		sendKeys("textField_Officer", CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME);
		sendKeys("textField_Title", RAISE_FAST_ACTION_TITLE);
		click("radioButton_Or_Search");
		clickAndWait("button_Fast_Action_Activity_Log_Search");
		if(isAlertPresent()){
			waitForAlertTextAndClose(SEARCH_RESULT_LIMIT__1_000_RECORDS_POPUP);
		}
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		verifyActivityLogSearchResult();
		// test NOT searching
		navigateTo(ACTIVITY_LOG_SEARCH_URL);
		verifyFastActionSearch();
		selectByVisibleText("dropDown_Action_Mirsap_Priority", "Medium");
		click("radioButton_Not_Search");
		clickAndWait("button_Fast_Action_Activity_Log_Search");
		if(isAlertPresent()){
			waitForAlertTextAndClose(SEARCH_RESULT_LIMIT__1_000_RECORDS_POPUP);
		}
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// Search via theme
		navigateTo(ACTIVITY_LOG_SEARCH_URL);
		verifyFastActionSearch();
		click("fastAction_Activity_Log_Search_Theme1");
		clickAndWait("button_Fast_Action_Activity_Log_Search");
		if(isAlertPresent()){
			waitForAlertTextAndClose(SEARCH_RESULT_LIMIT__1_000_RECORDS_POPUP);
		}
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// hit reset button
		navigateTo(ACTIVITY_LOG_SEARCH_URL);
		verifyFastActionSearch();
		click("button_Fast_Action_Activity_Log_Reset");
		// re-select incident
		clickAndWait("dropDown_Incident");
		checkCheckBox("fastAction_Activity_Log_SelectIncident1");
		clickAndWait("button_Fast_Action_Activity_Log_Search");
		if(isAlertPresent()){
			waitForAlertTextAndClose(SEARCH_RESULT_LIMIT__1_000_RECORDS_POPUP);
		}
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// wild card search
		navigateTo(ACTIVITY_LOG_SEARCH_URL);
		verifyFastActionSearch();
		sendKeys("textField_Title", "%test%");
		clickAndWait("button_Fast_Action_Activity_Log_Search");
		if(isAlertPresent()){
			waitForAlertTextAndClose(SEARCH_RESULT_LIMIT__1_000_RECORDS_POPUP);
		}
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		endLine(method);
	}

		//Test case 9433:003. Fast Action Activity Log Search result Tab 
		// Bulk state move icons working checked in Fast Action State Move Test Case and hence ignoring here 
		// Visualisation, Geo Map , Export icons Not Applicable 
		@Test(priority = 3)
		public void fastActionActivityLog_SearchResultTab(Method method) throws Exception{
			startLine(method);
			navigateTo(ACTIVITY_LOG_SEARCH_URL);
			//verifyFastActionSearch();
			clickAndWait("button_Fast_Action_Activity_Log_Search");
			if(isAlertPresent()){
				waitForAlertTextAndClose(SEARCH_RESULT_LIMIT__1_000_RECORDS_POPUP);
			}
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
					
			//Verify elements present on Action search result page
			Report.info("");
			verifyFastActionSearchResultPage();
					
			// Create FastAction from ActionSearchResult- Create New Entity Icon
			Report.info("");
			createNewEntityIcon();
					
			//View FastAction from View the selected entity icon
			Report.info("");
			viewTheSelectedEntityIcon();
					
			//View FastAction by double click
			Report.info("View FastAction by double click");
			viewFastActionByDoubleClick();

			//Refresh
			Report.info("Refresh");
			refreshTheGrid();
			
			//AutoRefresh
			Report.info("AutoRefresh");
			autoRefresh();
			
			//Bulk Update
			Report.info("Bulk Update");
			bulkUpdate();
			
			//Add to Favourite List
			Report.info("Add to Favourite List");
			addtoFavouriteList();
			
			//Add to Visualization List
			Report.info("Add to Visualization List");
			addtoVisualizationList();
	
			//Add to Default List
			Report.info("Add to Default List");
			addtoDefaultList();
			
			//Addto New/Existing List
			Report.info("Addto New/Existing List");
			 addtoNeworExistingList();
			
			//Select All columns
			Report.info("Select All columns");
			navigateTo(ACTIVITY_LOG_SEARCH_URL);
			clickAndWait("button_Fast_Action_Activity_Log_Search");
			if(isAlertPresent()){
				waitForAlertTextAndClose(SEARCH_RESULT_LIMIT__1_000_RECORDS_POPUP);
			}
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
			defineorReorderColumns();
			
			
					
			endLine(method);
		}
	
		private void addtoVisualizationList() throws Exception {
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
			checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
			String addToVisualizationDialogContent = "added successfully to Visualiser";
			explicitlyWaitForElement("icon_Action_Searchresult_Visualise", getTotalWaitTimeInSecs());
			String childWindowHandle1 = getCurrentWindowHandle();
			Report.info("Child window handle before invoking another child: "
					+ childWindowHandle1);
			click("icon_Action_Searchresult_Visualise");
			explicitlyWaitForElement(
					"text_Action_View_VisualizationDialogboxContent",
					getTotalWaitTimeInSecs());
			verifyTextContains(addToVisualizationDialogContent,
					"text_Action_View_VisualizationDialogboxContent");
			int currentWinCount1 = getWindowCount();
			WebDriver visualizationWinWebDriver = clickAndWaitForBrowserPopup(VISUALISATION_PORTLET_WINDOW_TITLE,
					"button_Action_View_VisualizationDialogboxOk", currentWinCount1, currentWinCount1+1);
			int currentWinCount = getWindowCount();
			driver = closechildWindowAndNavigateToItsParentWindowHandle(visualizationWinWebDriver, childWindowHandle1, currentWinCount, currentWinCount-1);
		}
	
		/*
		 *Test Case 23840: 015. Search Fast Action with public Linked Series
		 *Public group set for linkedSeries needs to be already setup
		 */
	@Test(priority=4)
	public void searchFromPublicGroupSet(Method method) throws Exception {
		startLine(method);
		navigateTo(HOME_PAGE_URL);
		explicitlyWaitForElement("dropDown_Home_LinkedSeries",
				getTotalWaitTimeInSecs());
		// select LinkedSeries from home page
		selectByVisibleText("dropDown_Home_LinkedSeries",
				LinkedSeriesPublicName);
		click("button_Home_Filter");
		click("radioButton_Home_Select_IncidentId");
		click("button_Home_Apply");

		// Go to activity log search
		navigateTo(ACTIVITY_LOG_SEARCH_URL);
		verifyFastActionSearch();

		// uncheck the incident
		clickAndWait("dropDown_Incident");
		click("fastAction_Activity_Log_SelectIncident1");

		// check the linked series
		clickAndWait("dropDown_LinkedSeries");
		// filter by text
		sendKeys("text_LinkedSeries_Filter", LinkedSeriesPublicName);
		click("fastAction_Activity_Log_SelectLinkedSeries2");

		// click search
		clickAndWait("button_Fast_Action_Activity_Log_Search");
		if(isAlertPresent()){
			waitForAlertTextAndClose(SEARCH_RESULT_LIMIT__1_000_RECORDS_POPUP);
		}
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());

		// logout and login with 2nd user
		signOut();

		// Relogin as allocated officer
		LoginAsSecondUser();

		// select LinkedSeries from home page
		explicitlyWaitForElement("dropDown_Home_LinkedSeries",
				getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Home_LinkedSeries",
				LinkedSeriesPublicName);
		click("button_Home_Filter");
		click("radioButton_Home_Select_IncidentId");
		click("button_Home_Apply");

		// sign out and log back in with main user for next test case
		sleep(5000);
		signOut();
		login();

		endLine(method);
	}
		
		/*
		 *Test Case 23841: 014. Search Fast Action with private Linked Series
		 *Public group set for linkedSeries needs to be already setup
		 */
		@Test(priority=5)
		public void searchFromPrivateGroupSet(Method method) throws Exception{
			//LINKEDSERIESNAME = "Automation Private Set";
			startLine(method);
			navigateTo(HOME_PAGE_URL);
			explicitlyWaitForElement("dropDown_Home_LinkedSeries",
					getTotalWaitTimeInSecs());
			//select LinkedSeries from home page
			selectByVisibleText("dropDown_Home_LinkedSeries",LinkedSeriesPrivateName);
			click("button_Home_Filter");
			click("radioButton_Home_Select_IncidentId");
			click("button_Home_Apply");
			
			//Go to activity log search
			navigateTo(ACTIVITY_LOG_SEARCH_URL);
			verifyFastActionSearch();
			
			//uncheck the incident
			clickAndWait("dropDown_Incident");
			click("fastAction_Activity_Log_SelectIncident1");
			
			//check the linked series
			clickAndWait("dropDown_LinkedSeries");
			click("fastAction_Activity_Log_SelectLinkedSeries1");
			
			//click search
			clickAndWait("button_Fast_Action_Activity_Log_Search");
			if(isAlertPresent()){
				waitForAlertTextAndClose(SEARCH_RESULT_LIMIT__1_000_RECORDS_POPUP);
			}
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
			
			//logout and login with 2nd user
			signOut();

			//Relogin as allocated officer
			LoginAsSecondUser();
			
			//check that the drop down does not contain the private set
			verifyDropDownNotContainText("dropDown_Home_LinkedSeries",LINKEDSERIESNAME);
			
			//sign out and log back in with main user for next test case
			signOut();
			login();
			
			endLine(method);
		}
		
		// Create FastAction from ActionSearchResult- Create New Entity Icon
		public void createNewEntityIcon() throws Exception{
			// Create FastAction from ActionSearchResult- Create New Entity Icon
			sleep(3000);
			String currentParentWindowHandle = getCurrentWindowHandle();
			// get Handle to Create Fast Action- child Window
			ArrayList<String> parentWinHandler1 = new ArrayList<String>();
			parentWinHandler1.add(currentParentWindowHandle);
			int noOfWindows = getWindowCount();
			WebDriver childWindDriver = clickAndWaitForChildWindow(FAST_ACTION_TITLE_WINDOW, parentWinHandler1, "icon_Action_Searchresult_CreateNew", noOfWindows, noOfWindows+1);
			
			explicitlyWaitForElement("button_Action_Raise",
					getTotalWaitTimeInSecs());
			
			verifyFastActionFieldsInCreatePage();
			//Pass data only to mandatory fields
			sendKeys("textField_Action_Title", ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE);
			clickAndWait("button_Action_Raise");
			explicitlyWaitForElement("successMsg_Action_Create",
					getTotalWaitTimeInSecs());
			// Verification of View Action Page
			verifyElementPresent("successMsg_Action_Create");
			verifyText(ALLOCATION, "label_Action_View_State");
			driver = closePopupWindowAndNavigateToParentWindowHandle(FAST_ACTION_TITLE_WINDOW, currentParentWindowHandle);

		}
		
		//View FastAction from View the selected entity icon
		public void viewTheSelectedEntityIcon()throws Exception{
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
			//select the first record
			checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
	
			/*explicitlyWaitForElement("icon_Action_Searchresult_View",getTotalWaitTimeInSecs());*/
			/*sleep(3000);*/
			String currentParentWindowHandle = getCurrentWindowHandle();
			// get Handle to Create Fast Action- child Window
			ArrayList<String> parentWinHandler1 = new ArrayList<String>();
			parentWinHandler1.add(currentParentWindowHandle);
			int winCount = getWindowCount();
			WebDriver childWindDriver = clickAndWaitForChildWindow(FAST_ACTION_TITLE_WINDOW, parentWinHandler1, "icon_Action_Searchresult_View", winCount, winCount+1);
			explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
			isInFastActionViewPage();
			//Verify currently incident id is selected
			verifyTextContains(incidentIdFull, "label_Action_View_Page_Title");
			driver = closePopupWindowAndNavigateToParentWindowHandle(FAST_ACTION_TITLE_WINDOW, currentParentWindowHandle);
			
		}
		
		//View FastAction by double click
		public void viewFastActionByDoubleClick()throws Exception{
			
			String currentParentWindowHandle = getCurrentWindowHandle();
			// get Handle to Create Fast Action- child Window
			ArrayList<String> parentWinHandler1 = new ArrayList<String>();
			parentWinHandler1.add(currentParentWindowHandle);
			int currentWinCount = getWindowCount();
			WebDriver childWindDriver = doubleClickAndWaitForChildWindow(FAST_ACTION_TITLE_WINDOW, parentWinHandler1, "checkBox_Action_SearchResulttab_Firstrow", currentWinCount, currentWinCount+1);
			explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
			isInFastActionViewPage();
			//Verify currently incident id is selected
			verifyTextContains(incidentIdFull, "label_Action_View_Page_Title");
			driver = closePopupWindowAndNavigateToParentWindowHandle(FAST_ACTION_TITLE_WINDOW, currentParentWindowHandle);
			
		}
		
		//Refresh and check Pageinfo- Total Record count before refresh and after refresh
		public void refreshTheGrid()throws Exception{
			sleep(3000);
			//wait for pageinfo div and store the total action record count in total_ActionRecord_Count_BeforeRefresh
			explicitlyWaitForElement("label_Action_PageInfo", getTotalWaitTimeInSecs());
			String paging_Info_BeforeRefresh = getText("label_Action_PageInfo");
			String[] paginginfo_Count_BeforeRefresh=paging_Info_BeforeRefresh.split(" ");
			int size1=paginginfo_Count_BeforeRefresh.length;
			String total_ActionRecord_Count_BeforeRefresh= paginginfo_Count_BeforeRefresh[size1-1];
			int totalActionRecordCountBeforeRefresh = Integer.parseInt(total_ActionRecord_Count_BeforeRefresh);
			Report.info("total_ActionRecord_Count_BeforeRefresh:" + total_ActionRecord_Count_BeforeRefresh);
			
			// Create FastAction from ActionSearchResult- Create New Entity Icon
			
			String currentParentWindowHandle = getCurrentWindowHandle();
			// get Handle to Create Fast Action- child Window
			ArrayList<String> parentWinHandler1 = new ArrayList<String>();
			parentWinHandler1.add(currentParentWindowHandle);
			int noOfWindowCount = getWindowCount();
			WebDriver childWindDriver = clickAndWaitForChildWindow(FAST_ACTION_TITLE_WINDOW, parentWinHandler1, "icon_Action_Searchresult_CreateNew", noOfWindowCount,noOfWindowCount+1);
			
			explicitlyWaitForElement("button_Action_Raise",
					getTotalWaitTimeInSecs());
			verifyFastActionFieldsInCreatePage();
			//Pass data only to mandatory fields
			sendKeys("textField_Action_Title", ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE);
			clickAndWait("button_Action_Raise");
			explicitlyWaitForElement("successMsg_Action_Create",
					getTotalWaitTimeInSecs());
			driver = closePopupWindowAndNavigateToParentWindowHandle(FAST_ACTION_TITLE_WINDOW, currentParentWindowHandle);
			
			
			//After Raise New Fast Action click on refresh icon
			click("icon_Action_Searchresult_Refresh");
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
			//wait for pageinfo div and store the total action record count in total_ActionRecord_Count_AfterRefresh
			explicitlyWaitForElement("label_Action_PageInfo", getTotalWaitTimeInSecs());
			String paging_Info_AfterRefresh = getText("label_Action_PageInfo");
			String[] paginginfo_Count_AfterRefresh=paging_Info_AfterRefresh.split(" ");
			int size2=paginginfo_Count_AfterRefresh.length;
			String total_ActionRecord_Count_AfterRefresh= paginginfo_Count_AfterRefresh[size1-1];
			Report.info("total_ActionRecord_Count_AfterRefresh:"+total_ActionRecord_Count_AfterRefresh);
			int totalActionRecordCountAfterRefresh = Integer.parseInt(total_ActionRecord_Count_AfterRefresh);
//			verifyTextCompare(String.valueOf((Integer.parseInt(total_ActionRecord_Count_BeforeRefresh)+1)), total_ActionRecord_Count_AfterRefresh);
	
             
             if (totalActionRecordCountBeforeRefresh < totalActionRecordCountAfterRefresh){
               	 Report.info("Refresh increments the total number of record count in page info");
                }
                else{
               	 Report.warn("Refresh has not incremented the total number of record count in page info");
                }
        
		}
		
		
		//AutoRefresh set to 1 min and check Pageinfo- Total Record count before refresh and after refresh
		public void autoRefresh()throws Exception{
			//wait for pageinfo div and store the total action record count in total_ActionRecord_Count_BeforeRefresh
			explicitlyWaitForElement("label_Action_PageInfo", getTotalWaitTimeInSecs());
			String paging_Info_BeforeRefresh = getText("label_Action_PageInfo");
			String[] paginginfo_Count_BeforeRefresh=paging_Info_BeforeRefresh.split(" ");
			int size1=paginginfo_Count_BeforeRefresh.length;
			String total_ActionRecord_Count_BeforeRefresh= paginginfo_Count_BeforeRefresh[size1-1];
			int totalActionRecordCountBeforeRefresh = Integer.parseInt(total_ActionRecord_Count_BeforeRefresh);
			Report.info(total_ActionRecord_Count_BeforeRefresh);

			// Create FastAction from ActionSearchResult- Create New Entity Icon

			String currentParentWindowHandle = getCurrentWindowHandle();
			// get Handle to Create Fast Action- child Window
			ArrayList<String> parentWinHandler1 = new ArrayList<String>();
			parentWinHandler1.add(currentParentWindowHandle);
			int noOfWindows = getWindowCount();
			WebDriver childWindDriver = clickAndWaitForChildWindow(FAST_ACTION_TITLE_WINDOW, parentWinHandler1, "icon_Action_Searchresult_CreateNew", noOfWindows, noOfWindows+1);
			explicitlyWaitForElement("button_Action_Raise",
					getTotalWaitTimeInSecs());
			verifyFastActionFieldsInCreatePage();
			//Pass data only to mandatory fields
			sendKeys("textField_Action_Title", ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE);
			clickAndWait("button_Action_Raise");
			explicitlyWaitForElement("successMsg_Action_Create",
					getTotalWaitTimeInSecs());
			driver = closePopupWindowAndNavigateToParentWindowHandle(FAST_ACTION_TITLE_WINDOW, currentParentWindowHandle);
			// Noticed an alert when records are more than 1000
			/*if(isAlertPresent()){
				acceptAlertBox(driver);
			}*/
			//After Raise New Fast Action setauto refresh to 1 min
			//set autorefreshperiod to 1 min
			//sendKeys("textfield_Action_Searchresult_Refresh_Period", "1");
			click("button_Action_Searchresult_RefreshPeriod_Set");
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
			//Action suspended for 1 min as to later check count after AutoRefresh period
			TimeUnit.SECONDS.sleep(120);
			//wait for pageinfo div and store the total action record count in total_ActionRecord_Count_AfterRefresh
			explicitlyWaitForElement("label_Action_PageInfo", getTotalWaitTimeInSecs());
			String paging_Info_AfterRefresh = getText("label_Action_PageInfo");
			String[] paginginfo_Count_AfterRefresh=paging_Info_AfterRefresh.split(" ");
			int size2=paginginfo_Count_AfterRefresh.length;
			String total_ActionRecord_Count_AfterRefresh= paginginfo_Count_AfterRefresh[size2-1];
			int totalActionRecordCountAfterRefresh = Integer.parseInt(total_ActionRecord_Count_AfterRefresh);
			Report.info("total_ActionRecord_Count_AfterRefresh:"+total_ActionRecord_Count_AfterRefresh);
//			verifyTextCompare(String.valueOf(Integer.parseInt(total_ActionRecord_Count_BeforeRefresh)+1), total_ActionRecord_Count_AfterRefresh);
            if (totalActionRecordCountBeforeRefresh < totalActionRecordCountAfterRefresh){
           	 Report.info("Refresh increments the total number of record count in page info");
            }
            else{
           	 Report.warn("Refresh has not incremented the total number of record count in page info");
            }

		}
		
		
		private void LoginAsSecondUser() throws Exception{
			 handleSecurityWindow1();
			 explicitlyWaitForElement("textField_Login_UserName", getTotalWaitTimeInSecs());
			 sendKeys("textField_Login_UserName", userName);
			 sendKeys("textField_Login_PassWord", passWord);
			 click("button_Login_SignIn");
		 }
	
		//Add record to Favourite list
		public void addtoFavouriteList()throws Exception{
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
			checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
			explicitlyWaitForElement("icon_Action_Searchresult_FavouriteList",
					getTotalWaitTimeInSecs());
			click("icon_Action_Searchresult_FavouriteList");
			explicitlyWaitForElement("dialog_Action_Searchresult_AddtoList",
					getTotalWaitTimeInSecs());
			verifyTextContains("list","dialog_Action_Searchresult_FavouriteOrDefault_Text");
			click("dialog_Action_Searchresult_AddtoList_Ok");
			
			
		}
		
		
		//Add record to Default list
		public void addtoDefaultList()throws Exception{
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
			checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
			explicitlyWaitForElement("icon_Action_Searchresult_DefaultList",
					getTotalWaitTimeInSecs());
			click("icon_Action_Searchresult_DefaultList");
			explicitlyWaitForElement("dialog_Action_Searchresult_AddtoList",
					getTotalWaitTimeInSecs());
			verifyTextContains("list","dialog_Action_Searchresult_FavouriteOrDefault_Text");
			click("dialog_Action_Searchresult_AddtoList_Ok");
			
		}

		//Add record to  addtoNeworExistingList
		public void addtoNeworExistingList()throws Exception{
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
			checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
			explicitlyWaitForElement("icon_Action_Searchresult_NeworExistingList",
					getTotalWaitTimeInSecs());
			click("icon_Action_Searchresult_NeworExistingList");
			explicitlyWaitForElement("dialog_Action_Searchresult_AddtoList",
					getTotalWaitTimeInSecs());
			int noOfWindows = getWindowCount();
			clickAndWaitForChildWindow(ADD_TO_NEW_EXISTING_LIST, "dialog_Action_Searchresult_AddtoList_Ok", noOfWindows, noOfWindows+1);
			explicitlyWaitForElement("button_ListManagement_Create", getTotalWaitTimeInSecs());
			selectTextByIndex ("dropDown_ListManagement_NeworExistingList" , 1);
			click("button_ListManagement_Add");
			explicitlyWaitForElement("successMsg_ListManagement_Create", getTotalWaitTimeInSecs());
			click("button_ListManagement_Update_Save");
			explicitlyWaitForElement("successMsg_ListManagement_Create", getTotalWaitTimeInSecs());
			driver = closePopupWindowAndNavigateToParentWindow(ADD_TO_NEW_EXISTING_LIST, FAST_ACTION_ACTIVITYLOG_TITLE_WINDOW);
			
		}	
		
		//define or reordercolumn
		public void defineorReorderColumns() throws Exception {
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
			click("icon_Action_Searchresult_DefineorReorderColumns");
			waitForJQuery(driver);
			sleep(2000);
			if(!verifyElementDisplayed("dialog_Action_Searchresult_defineorReorderColumn")){
				click("icon_Action_Searchresult_DefineorReorderColumns");
			}
			explicitlyWaitForElement("dialog_Action_Searchresult_defineorReorderColumn",getTotalWaitTimeInSecs());
			click("dialog_Action_Searchresult_defineorReorderColumn_selected");
			/*explicitlyWaitForElement("dialog_Action_SearchResult_defineorReorderColumn_CheckALL", getTotalWaitTimeInSecs());*/
			/*clickElementUsingJS("dialog_Action_SearchResult_defineorReorderColumn_UnCheckALL");*/
			/*WebElement checkAllLink = explicitlyWaitAndGetWebElement("dialog_Action_SearchResult_defineorReorderColumn_CheckALL", getTotalWaitTimeInSecs());
			clickElementUsingJS(checkAllLink);*/
			click("dialog_Action_SearchResult_defineorReorderColumn_CheckALL");
			explicitlyWaitForElement("dialog_Action_SearchResult_defineorReorderColumn_Ok", getTotalWaitTimeInSecs());
			click("dialog_Action_SearchResult_defineorReorderColumn_Ok");
			
		}
		
		// Bulk Update
		public void bulkUpdate()throws Exception{
			// Bulk Update- Single Record
			//click("label_Action_SearchResults_URN_Column");
			//explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
			//click("label_Action_SearchResults_URN_Column");
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
			checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
			/*click("table_Action_Dashboard_SearchResults_ActionTheme_FirstRow");
			String ThemesBeforeBulkUpdate=getText("table_Action_Dashboard_SearchResults_ActionTheme_FirstRow");
			Report.info("Themes before Bulk Update:"+ ThemesBeforeBulkUpdate);*/
			click("icon_Action_Searchresult_BulkUpdate");
			explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
			//change PM
			explicitlyWaitForElement("dropDown_Action_Create_ProtectiveMarking", getTotalWaitTimeInSecs());
			selectByVisibleText("dropDown_Action_Create_ProtectiveMarking",PROTECTIVE_MARKING_SECRET);
			//change priority
			selectByVisibleText("dropDown_Action_Fast_Priority", PRIORITY_HIGH );
			clickAndWait("button_Action_BulkUpdate_Save");

			//Overwrite during Bulk Update- Does not Retain Existing Theme Values
			explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Overwrite",getTotalWaitTimeInSecs());
			clickAndWait("dialog_Action_Searchresult_BulkUpdate_Overwrite");
			explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Text",getTotalWaitTimeInSecs());
			verifyTextContains("Action updated","dialog_Action_Searchresult_BulkUpdate_Text");
			clickAndWait("dialog_Action_Searchresult_BulkUpdate_Ok");
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow", getTotalWaitTimeInSecs());


			//Append during Bulk Update- Retains Existing Theme Values
			checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
			click("icon_Action_Searchresult_BulkUpdate");
			explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
			clickAndWait("button_Action_BulkUpdate_Save");
			explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Append",getTotalWaitTimeInSecs());
			clickAndWait("dialog_Action_Searchresult_BulkUpdate_Append");
			explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Text",getTotalWaitTimeInSecs());
			verifyTextContains("Action updated","dialog_Action_Searchresult_BulkUpdate_Text");
			clickAndWait("dialog_Action_Searchresult_BulkUpdate_Ok");
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow", getTotalWaitTimeInSecs());

			//cancel Bulk Update- Bulk Update Flow cancelled
			checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
			click("icon_Action_Searchresult_BulkUpdate");
			explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
			clickAndWait("button_Action_BulkUpdate_Save");
			explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Cancel",getTotalWaitTimeInSecs());
			clickAndWait("dialog_Action_Searchresult_BulkUpdate_Cancel");
			explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
			explicitlyWaitForElement("button_Action_BulkUpdate_Cancel",getTotalWaitTimeInSecs());
			clickAndWait("button_Action_BulkUpdate_Cancel");
			waitForJQuery(driver);
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow", getTotalWaitTimeInSecs());
			sleep(3000);

			// Bulk Update- Two Record's
			// Search Fast Action By ForAllocation state
			//raiseFastActionBulkRecord();
			//searchFastActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
			// select multiple recored in search result
			//click("label_Action_SearchResults_URN_Column");
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
			//click("label_Action_SearchResults_URN_Column");
			//explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
			selectTwoRecordsFromSearchResultAndVerifyState(ALLOCATION);
			click("icon_Action_Searchresult_BulkUpdate");
			explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
			//change PM
			explicitlyWaitForElement("dropDown_Action_Create_ProtectiveMarking", getTotalWaitTimeInSecs());
			selectByVisibleText("dropDown_Action_Create_ProtectiveMarking",PROTECTIVE_MARKING_SECRET);
			//change priority
			selectByVisibleText("dropDown_Action_Fast_Priority", PRIORITY_HIGH );
			click("button_Action_BulkUpdate_Save");
			
			//Overwrite during Bulk Update- Does not Retain Existing Theme Values
			explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Overwrite",getTotalWaitTimeInSecs());
			clickAndWait("dialog_Action_Searchresult_BulkUpdate_Overwrite");
			explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Text",getTotalWaitTimeInSecs());
			verifyTextContains("Action updated","dialog_Action_Searchresult_BulkUpdate_Text");
			clickAndWait("dialog_Action_Searchresult_BulkUpdate_Ok");
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow", getTotalWaitTimeInSecs());


			//Append during Bulk Update- Retains Existing Theme Values
			checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
			checkCheckBox("checkBox_Action_SearchResulttab_Secondrow");
			click("icon_Action_Searchresult_BulkUpdate");
			explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
			clickAndWait("button_Action_BulkUpdate_Save");
			explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Append",getTotalWaitTimeInSecs());
			clickAndWait("dialog_Action_Searchresult_BulkUpdate_Append");
			explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Text",getTotalWaitTimeInSecs());
			verifyTextContains("Action updated","dialog_Action_Searchresult_BulkUpdate_Text");
			clickAndWait("dialog_Action_Searchresult_BulkUpdate_Ok");
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow", getTotalWaitTimeInSecs());


			//cancel Bulk Update- Bulk Update Flow cancelled
			checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
			checkCheckBox("checkBox_Action_SearchResulttab_Secondrow");
			click("icon_Action_Searchresult_BulkUpdate");
			explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
			clickAndWait("button_Action_BulkUpdate_Save");
			explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Cancel",getTotalWaitTimeInSecs());
			clickAndWait("dialog_Action_Searchresult_BulkUpdate_Cancel");
			explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
			explicitlyWaitForElement("button_Action_BulkUpdate_Cancel",getTotalWaitTimeInSecs());
			clickAndWait("button_Action_BulkUpdate_Cancel");
			waitForJQuery(driver);
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow", getTotalWaitTimeInSecs());
			sleep(3000);
		}
		
		public void verifyActivityLogSearchResult()
				throws Exception {
			verifyText(RAISE_FAST_ACTION_TITLE,
					"table_Action_ActivityLog_SearchResults_Title");
			List<WebElement> tableHeaderForSearchResults = explicitlyWaitAndGetWebElements("table_Action_ActivityLog_SearchResults_OfficerNameTableHeader", getTotalWaitTimeInSecs());
			int positonOfOfficerInTable = tableHeaderForSearchResults.size() + 1;
			String officerNameInSearchresultXpath = "//table[@id='searchResultsTable']/tbody/tr[2]/td[" + positonOfOfficerInTable + "]";
			String officerNameInSearchresult = driver.findElement(By.xpath(officerNameInSearchresultXpath)).getText();
			verifyTextCompare(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
					officerNameInSearchresult);
		}

		@AfterTest
		public void afterTest() {
			driver.quit();
		}
}
