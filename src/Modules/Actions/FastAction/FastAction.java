package Modules.Actions.FastAction;

import java.lang.reflect.Method;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import Modules.Actions.Actions;
import ReusableUtilities.Report;

public class FastAction extends Actions{
	
	protected static final String NOMINAL_SEX = "MALE";

	protected static final String SEARCH_TYPE = "Fast Action";
    protected static final String ACTION_DASHBOARD = "/group/di/search-for-actions";
	protected static final String ACTION_ORIGINATING_DETAIL_MAX_CONTENT = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ012345678901234567";
	protected static final String RAISE_FAST_ACTION_URL = "/group/di/raise-action";
	protected static final String NOMINAL_URL = "/group/di/nominal";
	protected static final String ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE = "Action-Auto Test for Raise Action";
	protected static final String RAISE_FAST_ACTION_MULTIPLE_QUEU_TITLE = "Action-Auto Test for Raise Action";
	protected static final String ACTION_AUTO_TEST_FOR_UPDATE_FAST_ACTION_TITLE = "Action-Auto Test for Raise Action update";
	protected static final String ACTION_ORIGINATING_DETAIL = "Originating Desc";
	protected static final String ALPHA_NUMERIC_TEXT="A1a29F";
	protected static final String SPECIALCHARACTER_TEXT="@%^*(";
	protected static final String ACTION_TITLE_MAX_CONTENT = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ012345678901234567";
	protected static final String VIEWACTION_URL = "/group/holmes/view-action";
	protected static final String TINY_MCE_TEXT_FOR_RAISE_FAST_ACTION= "TinyMCEText";
	protected static final String FAST_ACTION_TITLE_WINDOW = "Fast Action";
	protected static final String ACTION_DASHBOARD_TITLE_WINDOW = "Action Dashboard";
	protected static final String ADD_TO_NEW_EXISTING_LIST = "List Maintenance - Liferay";
	protected static final String NOMINAL_TITLE_WINDOW = "Nominal";
	protected static final String bulkCreateCount="50";
	protected static final String bulkCreateCountForSateMove="5";
	protected static final String bulkCreateCount_Limit="100";
	protected static String fastActionURN_ReUse;
	protected static String bulkAction;
	public String getFastActionURN;
	protected static String TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE="TinyMCEText-Action - Auto Test for single Queue Move";
	protected static String searchResult_FastActionFirstURN;
	protected static String searchResult_FastActionSecondURN;
	protected static String SEARCH_FOR_AUDITS= "/group/di/search-for-audits";
	
	// Data used to create Officer
	protected static final String CREATE_OFFICER_OFFICERNO_MAX_CHAR = "0123456789";

	//Data used to Verify Current/Future Action State
	protected static final String ALLOCATION= "For Allocation";
	protected static final String ALLOCATED = "Allocated";
	protected static final String SUBMITTED = "Submitted";
	protected static final String RECEIVED = "Received";
	protected static final String RESULTED = "Resulted";
	protected static final String FILED = "Filed";
	protected static final String PARTIALLY_RESULTED="Partially Resulted";
	
	//Data used for creating association
	protected static final String ASSOCIATION_TYPE_LINK = "Link";
	protected static final String ASSOCIATION_TYPE_DESCRIBED = "Described";
	protected static final String ASSOCIATION_TYPE_SUBJECT = "Subject";
	protected static final String ASSOCIATION_TYPE_MANUALPOTENTIALMATCH = "Manual Potential Match";
	protected static final String ASSOCIATION_UPDATE_DESCRIPTION ="Desc";
	
	//Data for Marker tab
	protected static final String MARKER_EXPIRYDATEADD = "18/02/2017 21:26";
	protected static final String MARKER_EXPIRYDATEEDITED = "19/02/2017 21:26";
	protected static final String MARKER_DELETE_POPUP = "Would you like to remove selected Marker(s)?";
	protected static final String MARKER_EDIT_POPUP_ERROR = "Please select one row to edit";
	protected static final String MARKER_DELETE_POPUP_ERROR = "Please select one or more rows to delete";
	protected static final String ASSOCIATION_DELETE_POPUP_MESSAGE = "Are you sure you want to remove the cross reference?";
	
	//Data for Transfer association 
	protected static final String TRANSFER_POPUP_ERROR = "Please select a record";
	
	//Data for Fast Action Activity Log
	protected static final String ACTIVITY_LOG_SEARCH_URL = "/group/holmes/fast-action-activity-log";
	protected static final String FAST_ACTION_ACTIVITYLOG_TITLE_WINDOW="Fast Action Activity Log";
	protected static final String HOME_PAGE_URL="/group/holmes/home";
	
	//Data for Activity Description / History Tab Details verification
	public static final String ALLOCATED_FAST_ACTION="Allocated Fast Action";
	public static final String SUBMITTED_FAST_ACTION="Submitted Fast Action";
	public static final String RESULTED_FAST_ACTION="Resulted Fast Action";
	public static final String RECEIVED_FAST_ACTION="Received Fast Action";
	public static final String FILED_FAST_ACTION="Filed Fast Action";
	public static final String UPDATE_FAST_ACTION="UPDATE FAST ACTION";
	public static final String VIEW_FAST_ACTION="VIEW FAST ACTION";
	public static final String SEARCH_FAST_ACTION="SEARCH ACTION";
	public static final String CREATE_ASSOCIATION="CREATE ASSOCIATION";
	public static final String UPDTAE_ASSOCIATION="UPDATE ASSOCIATION";
	public static final String DELETE_ASSOCIATION="DELETE ASSOCIATION";
	public static final String ADD_ATTACHEMENT="ADD ATTACHMENT";
	public static final String REMOVE_ATTACHEMENT="REMOVE ATTACHMENT";
	public static final String CREATE_FAST_ACTION="CREATE FAST ACTION";
	public static final String ADD_TAG_TO_RECORD="ADD TAG TO RECORD";
	
	// default constructor
	public FastAction() {
		softAssert = new SoftAssert();
	}

	public FastAction(WebDriver driver) {
		this.driver = driver;
		softAssert = new SoftAssert();
	}

	
	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}
	
	
	public void verifySearchResultTabEnabled(){
		verifyTabActive("tab_Action_Search_Result");
	}
	
	public boolean verifyAssociationTabEnabled(){
		return verifyTabActive("tab_Association");
	}
	
	
	public void verifyMarkerTabEnabled(){
		verifyTabActive("tab_Marker");
	}
	
	public void verifyFastActionTabEnabled(){
		verifyTabActive("tab_Fast_Action");
	}
	
	public void isInFastActionViewPage() throws Exception{
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		verifyElementPresent("label_Action_View_Page_Title");
	}
	
	public void isInFastActionEditPage() throws Exception{
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Save");
	}
	
	public void isInFastActionSimilarPage() throws Exception{
		explicitlyWaitForElement("header_Create_Fast_Action", getTotalWaitTimeInSecs());
		verifyElementPresent("header_Create_Fast_Action");
		explicitlyWaitForElement("button_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Create");
	}

	public void verifyMandatoryFieldsInEditFastAction() throws Exception {
		explicitlyWaitForElement("label_Action_Edit_Title", getTotalWaitTimeInSecs());
		verifyElementIsMandatory("label_Action_Edit_Title");
		
	}
	
	public void verifyMandatoryFieldsInFastAction() throws Exception {
		explicitlyWaitForElement("label_Action_Title", getTotalWaitTimeInSecs());
		verifyElementIsMandatory("label_Action_Title");
		
	}
	
	protected void verifyFastActionFieldsInCreatePage() throws Exception {
		// Verify Title has text field
		assertElementPresent("textField_Action_Title");
		
		//Verify currently incident id is selected
		verifyCurrentSelectByText("dropDown_Action_Create_Incident",incidentIdFull);
		// Validate Protective Marking : Restricted is set as default
		verifyCurrentSelectByText("dropDown_Action_Create_ProtectiveMarking",PROTECTIVE_MARKING_RESTRICTED);
		// Validate  Priority : Low is set as default value
		verifyCurrentSelectByText("dropDown_Action_Mirsap_Priority",PRIORITY_LOW);
		
		verifyCreateBulkRecordsCheckBoxPresent();
		verifyAllocatedOfficer();
		verifyThemes();
		verifyState();
		verifyLinkedActions();
		verifyAssociatedDocuments();
		verifyOriginatingDetails();
		verifyReceiversInstructions();
		verifyResultText();
		verifyDocumentTaken();
		verifyMigrationLog();
		verifyCustomFields();		
	}
	
	protected void verifyFastActionFieldsInEditPage() throws Exception {
		// Verify Title has text field
		assertElementPresent("textField_Action_Title");
		
		// Validate Protective Marking : Restricted is set as default
		verifyCurrentSelectByText("dropDown_Action_Create_ProtectiveMarking",PROTECTIVE_MARKING_RESTRICTED);
		// Validate  Priority : Low is set as default value
		verifyCurrentSelectByText("dropDown_Action_Mirsap_Priority",PRIORITY_LOW);
		
		verifyAllocatedOfficer();
		verifyThemes();
		verifyState();
		verifyLinkedActions();
		verifyAssociatedDocuments();
		verifyOriginatingDetails();
		verifyReceiversInstructionsInEditPage();
		verifyResultTextInEditPage();
		verifyDocumentTakenInEditPage();
		verifyMigrationLog();
		verifyCustomFields();
	}
	
	protected void verifyFastActionFieldsInSingleQueueMove() throws Exception{
		verifyUrn();
		verifyTitleValue();
		verifyThemes();		
	}
	
	protected void verifyFastActionFieldsInAllocatedPage() throws Exception{
		verifyUrn();
		verifyTitleValue();
		verifyTextField("");
		verifyStateValue(ALLOCATED);
		verifyThemes();
		verifyAssociatedDocuments();
		verifyAllocatedOfficer();
		verifyReceiversInstructions();
	}
	
	protected void verifyFastActionFieldsInSubmittedPage(String expectedText) throws Exception{
		verifyUrn();
		verifyTitleValue();
		verifyTextField(expectedText);
		verifyStateValue(SUBMITTED);
		verifyThemes();
		verifySubmissionText();
		verifyReceiversInstructions();
	}
	
	protected void verifyFastActionFieldsInReceivedPage(String expectedText) throws Exception{
		verifyUrn();
		verifyTitleValue();
		verifyTextField(expectedText);
		verifyStateValue(RECEIVED);
		verifyThemes();
		verifyReceiversInstructions();
	}
	
	protected void verifyFastActionFieldsInResultedPage(String expectedText) throws Exception{
		verifyUrn();
		verifyTitleValue();
		verifyTextField(expectedText);
		verifyStateValue(RESULTED);
		verifyThemes();
		verifyReceiversInstructions();
	}
	
	protected void verifyFastActionFieldsInFiledPage(String expectedText) throws Exception{
		verifyUrn();
		verifyTitleValue();
		verifyTextField(expectedText);
		verifyStateValue(FILED);
		verifyThemes();
		verifyReceiversInstructions();
		verifyFilingReason();
	}
	
	protected void verifyFastActionFieldsInPartially_ResultedPage(String expectedText) throws Exception{
		verifyUrn();
		verifyTitleValue();
		verifyTextField(expectedText);
		verifyStateValue(PARTIALLY_RESULTED);
		verifyThemes();
		verifyReceiversInstructions();
		verifyResultText();
		verifyDocumentTakenInEditPage();
	}
	

	private void verifyCustomFields() throws Exception {
		explicitlyWaitForElement("section_Action_Create_CustomFields", getTotalWaitTimeInSecs());
		verifyElementPresent("section_Action_Create_CustomFields");
	}

	private void verifyMigrationLog() throws Exception {
//		expandMigrationLog();
		verifyElementPresent("textField_Action_FastAction_Create_MigrationLog");
	}

	private void verifyDocumentTaken() throws Exception {
		explicitlyWaitForElement("icon_Action_Create_DocumentTaken_View", getTotalWaitTimeInSecs());
		verifyElementPresent("icon_Action_Create_DocumentTaken_View");
	}
	
	
	private void verifyDocumentTakenInEditPage() throws Exception {
		explicitlyWaitForElement("icon_Action_Edit_DocumentTaken_Add", getTotalWaitTimeInSecs());
		verifyElementPresent("icon_Action_Edit_DocumentTaken_Add");
	}

	private void verifyResultText() throws Exception {
		verifyElementPresent("textField_Action_FastAction_Create_ResultText");
	}
	
	private void verifyResultTextInEditPage() throws Exception {
		explicitlyWaitForElement("textField_Action_FastAction_Create_ResultText", getTotalWaitTimeInSecs());
		verifyElementPresent("textField_Action_FastAction_Create_ResultText");
		verifyElementPresent("textField_Action_FastAction_Edit_ResultText");
	}

	protected void verifyReceiversInstructions() throws Exception {
		verifyElementPresent("textField_Action_FastAction_Edit_ReceiversInstructions");
	}
	
	private void verifyReceiversInstructionsInEditPage() throws Exception {
		explicitlyWaitForElement("textField_Action_FastAction_Edit_ReceiversInstructions", getTotalWaitTimeInSecs());
		verifyElementPresent("textField_Action_FastAction_Edit_ReceiversInstructions");
	}

	private void verifyOriginatingDetails() throws Exception {
		explicitlyWaitForElement("textField_Action_OriginatingDetails", getTotalWaitTimeInSecs());
		verifyElementPresent("textField_Action_OriginatingDetails");
	}

	protected void verifyAssociatedDocuments() throws Exception {
		explicitlyWaitForElement("table_Action_Create_AssociatedDocument", getTotalWaitTimeInSecs());
		verifyElementPresent("table_Action_Create_AssociatedDocument");
	}

	private void verifyLinkedActions() throws Exception {
		explicitlyWaitForElement("table_Action_Create_LinkedAction", getTotalWaitTimeInSecs());
		verifyElementPresent("table_Action_Create_LinkedAction");
	}

	private void verifyState() throws Exception {
		verifyElementPresent("label_Action_FastAction_Create_State");
	}

	private void verifyThemes() throws Exception {
		explicitlyWaitForElement("checkBox_Action_Themes", getTotalWaitTimeInSecs());
		verifyElementPresent("checkBox_Action_Themes");
	}

	protected void verifyAllocatedOfficer() throws Exception {
		explicitlyWaitForElement("button_Action_FastAction_CreateOfficer", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_FastAction_CreateOfficer");
		explicitlyWaitForElement("textField_Action_FastAction_Create_Allocated_Officer", getTotalWaitTimeInSecs());
		verifyElementPresent("textField_Action_FastAction_Create_Allocated_Officer");
	}

	private void verifyCreateBulkRecordsCheckBoxPresent() throws Exception {
		explicitlyWaitForElement("checkBox_Action_BulkRecord", getTotalWaitTimeInSecs());
		verifyElementPresent("checkBox_Action_BulkRecord");
	}

	private void verifyUrn() throws Exception{
		explicitlyWaitForElement("label_Action_Urn_QueueMove", getTotalWaitTimeInSecs());
		verifyText(fastActionURN_ReUse, "label_Action_Urn_QueueMove");
	}
	
	private void verifyTitleValue() throws Exception{
		verifyText( ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE ,
				"label_Action_View_Title");
	}
	
	protected void verifySubmissionText() throws Exception {
		verifyElementPresent("textfield_Action_StateMove_SubmissionText");
	}
	
	private void verifyStateValue(String expectedState) throws Exception{
		verifyText(expectedState ,"label_Action_Create_State");
	}
	
	private void verifyTextField(String textToCompare) throws Exception{
		verifyTextCompare(textToCompare, getTextFieldText()); 
	}
	
	private void verifyFilingReason() throws Exception{
		verifyElementPresent("textfield_Action_StateMove_Filing_Reason");
	}
	
	public boolean validateFastActionURNPattern(String URN) {
		String pattern = "^A\\d+";
		Pattern patternObj = Pattern.compile(pattern);
		Matcher matcherObj = patternObj.matcher(URN);
		
		int count = 0;
		
		while(matcherObj.find()){
			count++;
		}
		
		if (count == 1) {
			Report.pass("Matching with the Patern");
			return true;
		} else {
			Report.warn("NOT matching with the pattern, as noticed URN is: " + URN);
			return false;
		}
	}
	
	public void validateFastActionSuccMess() throws Exception {
		String succMessage = getText("successMsg_Action_Create");
		String[] actUrn = succMessage.split(" ");
		String fastActUrn = actUrn[2];
		boolean patternMatched = validateFastActionURNPattern(fastActUrn);
		String pattern = "Fast Action A\\d+ created";
		Pattern patternObj = Pattern.compile(pattern);
		Matcher matcherObj = patternObj.matcher(succMessage);
		
		int count = 0;
		
		while(matcherObj.find()){
			count++;
		}
		
		if (count == 1 & patternMatched) {
			Report.pass("Fast Action success message validated");
		} else {
			Report.warn("Fast Action success message is not matching with Actual Messaage: " + succMessage + "Expected Message: " + pattern);
			
		}
	}
	
	public void validateFastActionUpdateSuccMess(String fastActionURN) throws Exception {
		String succMess = "Fast Action " + fastActionURN + " updated";
		verifyText(succMess, "successMsg_Action_Update");
	}
	public void validateBulkFastActionSuccMess() throws Exception {
		String succMessage = getText("successMsg_Action_Create");
		String[] actUrn = succMessage.split(" ");
		String bulkCount= actUrn[0];
		//Verify 50(variablebulkCount) bulk records are created
		verifyTextCompare(bulkCreateCount,bulkCount );
		String fastActUrn_BulkCount = actUrn[5];
		String fastAction_firstUrn = fastActUrn_BulkCount.substring(0,4);
		String fastAction_lastUrn = fastActUrn_BulkCount.substring(5,9);
		//TODO -Shalini/Suresh ( Not Matching because return type is boolean True-True returned in place of URNs)
		boolean patternMatched_BulkFirst = validateFastActionURNPattern(fastAction_firstUrn);
		boolean patternMatched_BulkLast = validateFastActionURNPattern(fastAction_lastUrn);
		String pattern=  bulkCount+" "+"fast action created successfully"+" "+fastAction_firstUrn+"-"+fastAction_lastUrn+" "+"for group"+" "+incidentIdFull;
		
		Pattern patternObj = Pattern.compile(pattern);
		Matcher matcherObj = patternObj.matcher(succMessage);
		
		int count = 0;
		
		while(matcherObj.find()){
			count++;
		}
		
		if (count == 1 & patternMatched_BulkFirst & patternMatched_BulkLast ) {
			Report.pass("Fast Action success message validated");
		} else {
			Report.warn("Fast Action success message is not matching with Actual Messaage: " + succMessage + "Expected Message: " + pattern);
		}
	}
	
	public String raiseAndGetFastActionURN() throws Exception {
		navigateTo(RAISE_FAST_ACTION_URL);
		// Wait for Create button on Action create page for 60secs
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		// Confirm Create Button on Action Create Page is present
		verifyElementPresent("button_Action_Raise");
		//Pass data only to mandatory fields
		sendKeys("textField_Action_Title", ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE);
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		// Verification of View Action Page
		verifyElementPresent("successMsg_Action_Create");
		verifyText(ALLOCATION, "label_Action_View_State");
		//getList and verify state in dropdown
		getAllListItemAndVerify( "Allocated","dropDown_Action_Select_State");
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		fastActionURN_ReUse = actUrn[2];
		return getFastActionURN=fastActionURN_ReUse;				
	}
	
	public String raiseAndGetFastActionURN(WebDriver driver) throws Exception {
		navigateTo(RAISE_FAST_ACTION_URL);
		// Wait for Create button on Action create page for 60secs
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		// Confirm Create Button on Action Create Page is present
		verifyElementPresent("button_Action_Raise");
		//Pass data only to mandatory fields
		sendKeys("textField_Action_Title", ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE);
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		// Verification of View Action Page
		verifyElementPresent("successMsg_Action_Create");
		verifyText(ALLOCATION, "label_Action_View_State");
		//getList and verify state in dropdown
		getAllListItemAndVerify( "Allocated","dropDown_Action_Select_State");
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		fastActionURN_ReUse = actUrn[2];
		return getFastActionURN=fastActionURN_ReUse;				
	}
	
	public void changeStateAllocated(String tinyMceText) throws Exception {
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyFastActionFieldsInAllocatedPage();
		explicitlyWaitForElement("textfield_Action_FastAction_QueueAllocatedOfficer",getTotalWaitTimeInSecs());
		insertIntoAllTinyMCEinPage(tinyMceText);
		sendKeysAndEnter("textfield_Action_FastAction_QueueAllocatedOfficer", CREATE_OFFICER_SURNAME);
		explicitlyWaitForElement("textfield_Action_StateMove_OfficerName", getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_StateMove_OfficerName");
		explicitlyWaitForElement("textfield_Action_FastAction_QueueAllocatedOfficerName",getTotalWaitTimeInSecs());
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,"textfield_Action_FastAction_QueueAllocatedOfficerName");
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(ALLOCATED, "label_Action_View_State");
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,"textField_Action_view_CreateOfficer");
		getAllListItemAndVerify("Submitted","dropDown_Action_Select_State");
	}
	
	public void changeStateSubmitted(String expectedText,String tinyMceText) throws Exception{
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyFastActionFieldsInSubmittedPage(expectedText);
		insertIntoAllTinyMCEinPage(tinyMceText);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(SUBMITTED, "label_Action_View_State");
		getAllListItemAndVerify("Received","dropDown_Action_Select_State");
	}
	
	public void changeStateReceived(String expectedText, String textInstructions) throws Exception{
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyFastActionFieldsInReceivedPage(expectedText);
		sendKeys("textField_Action_FastAction_Edit_ReceiversInstructions", textInstructions);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(RECEIVED, "label_Action_View_State");
		getAllListItemAndVerify("Resulted","dropDown_Action_Select_State");
	}
	
	public void changeStateResulted(String expectedText, String tinyMceText) throws Exception{
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyFastActionFieldsInResultedPage(expectedText);
		//send keys to result text
		insertIntoAllTinyMCEinPage(tinyMceText);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(RESULTED, "label_Action_View_State");
		getAllListItemAndVerify("Filed","dropDown_Action_Select_State");
	}
	
	public void changeStateFiled(String tinyMceText) throws Exception{
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyFastActionFieldsInFiledPage("Automation testing - resulted");
		//send keys to result text
		insertIntoAllTinyMCEinPage(tinyMceText);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(FILED, "label_Action_View_State");
		getAllListItemAndVerify("For Allocation","dropDown_Action_Select_State");
	}
	
	public void changeStateFiledForChangeStateLargeFields(String tinyMceText) throws Exception{
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyFastActionFieldsInFiledPage(tinyMceText);
		//send keys to result text
		insertIntoAllTinyMCEinPage(tinyMceText);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(FILED, "label_Action_View_State");
		getAllListItemAndVerify("For Allocation","dropDown_Action_Select_State");
	}
	
	public void signOut()throws Exception{
		 click("icon_Home_MyAccount");
		 click("button_Home_SignOut");

	 }
	public void viewAndGetFastActionURN() throws Exception{
		navigateTo(VIEWACTION_URL);
		explicitlyWaitForElement("textField_Action_ViewUrn", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_ViewUrn", getFastActionURN);
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
	}
	
	public void viewAndGetFastActionURNID(String fastURN) throws Exception{
		navigateTo(VIEWACTION_URL);
		explicitlyWaitForElement("textField_Action_ViewUrn", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_ViewUrn", fastURN);
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
	}
	
	
	
	protected void verifyFastActionBulkUpdate_Popup() throws Exception {
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		  // verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
	}
	
	protected void verifyAllElementsInFastActionBulkUpdate() throws Exception {

		verifyFastActionBulkUpdateHeader();
		verifyFastActionBulkUpdateTab();
		verifyFastActionBulkUpdateState();
		verifyFastActionBulkUpdateTheme();
		verifyFastActionBulkUpdateTags();
		verifyFastActionBulkUpdateCancel();
		verifyFastActionBulkUpdateSave();
		//verifyReceiversInstructions();
	}

	protected void verifyAllElementsInFastActionBulkUpdate_Individual()
			throws Exception {
		
		verifyFastActionBulkUpdateTheme();
		verifyReceiversInstructions();
		verifyFastActionBulkUpdate_IndividualCancel();
		verifyFastActionBulkUpdate_IndividualSkip();
		verifyFastActionBulkUpdate_IndividualApply();
		verifyFastActionBulkUpdate_IndividualMove();
		verifyFastActionBulkUpdate_IndividualState();
	}

	public void searchFastActionAndSelectByState(String stateObj) throws Exception {

		// navigate to Action Dash board
		navigateTo(ACTION_DASHBOARD);
		explicitlyWaitForElement("dropDown_Action_ActionDashboard_Search_Type",
				getTotalWaitTimeInSecs());
		// Search Fast Action in Drop down
		selectByVisibleText("dropDown_Action_ActionDashboard_Search_Type",
				SEARCH_TYPE);
		// Enter Title field for search
		explicitlyWaitForElement("textField_Action_Title", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Title",
				RAISE_FAST_ACTION_MULTIPLE_QUEU_TITLE);
		// Select For Allocation Satate
		click("button_Action_ActionDashboard_Search_State");
		click(stateObj);
		// click Search button
		click("button_Action_Search");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
	}
	
	public void selectTwoRecordsFromSearchResultAndVerifyState(String actionState) throws Exception {
		//select two records
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow", getTotalWaitTimeInSecs());
		click("checkBox_Action_SearchResulttab_Firstrow");
		click("checkBox_Action_SearchResulttab_Secondrow");
		// getting bulkstate move URNs
		searchResult_FastActionFirstURN = getText("table_Action_SearchResults_URN");
		searchResult_FastActionSecondURN = getText("table_Action_SearchResults_SecondRow_URN");
		// verify For allocation State
		List<WebElement> tableHeaderForSearchResults = explicitlyWaitAndGetWebElements("table_Action_ActivityLog_SearchResults_StateTableHeader", getTotalWaitTimeInSecs());
		int positonOfStateInTable = tableHeaderForSearchResults.size() + 1;
		String stateInSearchresultXpath = "//table[@id='searchResultsTable']/tbody/tr[2]/td[" + positonOfStateInTable + "]";
		String stateInSearchresult = driver.findElement(By.xpath(stateInSearchresultXpath)).getText();
		verifyTextCompare(actionState, stateInSearchresult);
		String stateInSearchresultXpath2 = "//table[@id='searchResultsTable']/tbody/tr[3]/td[" + positonOfStateInTable + "]";
		String stateInSearchresult2 = driver.findElement(By.xpath(stateInSearchresultXpath2)).getText();
		verifyTextCompare(actionState, stateInSearchresult2);
	}
	
	public void verifyFastActionSearch() throws Exception {
		explicitlyWaitForElement("dropDown_Incident",getTotalWaitTimeInSecs());
		verifyElementPresent("dropDown_Incident");
		verifyElementPresent("dropDown_LinkedSeries");
		verifyElementPresent("dropDown_priority");
		verifyElementPresent("textField_Officer");
		verifyElementPresent("fastAction_Activity_Log_Search_Theme1");
		verifyElementPresent("textField_Title");
		verifyElementPresent("radioButton_And_Search");
		verifyElementPresent("radioButton_Or_Search");
		verifyElementPresent("radioButton_Not_Search");
		verifyElementPresent("button_Fast_Action_Activity_Log_Search");
		verifyElementPresent("button_Fast_Action_Activity_Log_Reset");
	}
	
	public void verifyFastActionSearchResultPage() throws Exception {
		verifyElementPresent("icon_Action_Searchresult_CreateNew");
		verifyElementPresent("icon_Action_Searchresult_View");
		verifyElementPresent("icon_Action_Searchresult_Refresh");
		verifyElementPresent("icon_Action_Searchresult_BulkUpdate");
		verifyElementPresent("icon_Action_Searchresult_BulkStateMove");
		verifyElementPresent("icon_Action_Searchresult_FavouriteList");
		verifyElementPresent("icon_Action_Searchresult_DefaultList");
		verifyElementPresent("icon_Action_Searchresult_NeworExistingList");
		verifyElementPresent("icon_Action_Searchresult_Visualise");
		verifyElementPresent("icon_Action_Searchresult_I2Analyst");
		verifyElementPresent("icon_Action_Searchresult_ExportSelected");
		verifyElementPresent("icon_Action_Searchresult_GeoMap");
		verifyElementPresent("icon_Action_Searchresult_DefineorReorderColumns");
		verifyElementPresent("icon_Action_Searchresult_ExportAll");
		verifyElementPresent("textfield_Action_Searchresult_Refresh_Period");
		verifyElementPresent("button_Action_Searchresult_RefreshPeriod_Set");
	}
	
	public void raiseFastActionBulkRecord()throws Exception{
		navigateTo(RAISE_FAST_ACTION_URL);
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		// Confirm Create Button on Action Create Page is present
		verifyElementPresent("button_Action_Raise");
		//Pass data to all fields
		sendKeys("textField_Action_Title",  RAISE_FAST_ACTION_MULTIPLE_QUEU_TITLE );
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_RAISE_FAST_ACTION);
		
		// Bulk create Entering 50 count
		click("checkBox_Action_BulkRecord");
		sendKeys("textField_Action_BulkRecordCount", bulkCreateCountForSateMove);
		// Raise Fast Action by click on Create button 
		clickAndWait("button_Action_Raise");
		// Verification of View Action Page
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		//validate Fast Action Success Message

	}


	private void verifyFastActionBulkUpdateHeader() throws Exception {
		// verify header Moving BulkState move :allocated
		explicitlyWaitForElement("header_Action_BulkSatetMove_Allocated", getTotalWaitTimeInSecs());
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
	}   
	
	private void verifyFastActionBulkUpdateTab() throws Exception {
		// verify BulkUpdate tab
		explicitlyWaitForElement("tab_Action_BulkUpdate", getTotalWaitTimeInSecs());
		verifyElementPresent("tab_Action_BulkUpdate");
	} 
	
	private void verifyFastActionBulkUpdateState() throws Exception {
		// verify label Moving and allocated URN
		explicitlyWaitForElement("label_Action_BulkStateMove_Allocated", getTotalWaitTimeInSecs());
		verifyElementPresent("label_Action_BulkStateMove_Allocated");
	} 
	private void verifyFastActionBulkUpdateTheme() throws Exception {
		// verify theme
		explicitlyWaitForElement("checkBox_Action_Themes_Item1", getTotalWaitTimeInSecs());
		verifyElementPresent("checkBox_Action_Themes_Item1");
	} 
	private void verifyFastActionBulkUpdateTags() throws Exception {
		// Verify Tag link
		explicitlyWaitForElement("link_Action_BulkUpdate_MoveAll_Tags", getTotalWaitTimeInSecs());
		verifyElementPresent("link_Action_BulkUpdate_MoveAll_Tags");
	} 
	
	private void verifyFastActionBulkUpdateCancel() throws Exception {
		explicitlyWaitForElement("button_Action_BulkUpdate_Cancel", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_Cancel");
	} 		
	private void verifyFastActionBulkUpdateSave() throws Exception {
		explicitlyWaitForElement("button_Action_BulkUpdate_Save", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_Save");
	} 		
	
	private void verifyFastActionBulkUpdate_IndividualCancel() throws Exception {
		// verify cancel button
		explicitlyWaitForElement("button_Action_BulkUpdate_MoveIndividual_Cancel", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Cancel");
	}

	private void verifyFastActionBulkUpdate_IndividualSkip() throws Exception {
		// verify Skip button
		explicitlyWaitForElement("button_Action_BulkUpdate_MoveIndividual_Skip", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Skip");
	}

	private void verifyFastActionBulkUpdate_IndividualApply() throws Exception {
		// verify apply button
		explicitlyWaitForElement("button_Action_BulkUpdate_MoveIndividual_Apply", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Apply");
	}

	private void verifyFastActionBulkUpdate_IndividualMove() throws Exception {
		// verify MoveIndividual button
		explicitlyWaitForElement("button_Action_BulkUpdate_MoveIndividual", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual");
	}

	private void verifyFastActionBulkUpdate_IndividualState() throws Exception {
		// verify label MoveIndividual
		explicitlyWaitForElement("label_Action_BulkStateMove_MoveIndividual_Allocated", getTotalWaitTimeInSecs());
		verifyElementPresent("label_Action_BulkStateMove_MoveIndividual_Allocated");
	}
	public String raiseBulkFastActionAndGetURNs(String bulkCount) throws Exception {
		navigateTo(RAISE_FAST_ACTION_URL);
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		// Confirm Create Button on Action Create Page is present
		verifyElementPresent("button_Action_Raise");
		// Pass data to all fields
		sendKeys("textField_Action_Title",
				RAISE_FAST_ACTION_MULTIPLE_QUEU_TITLE);
		//insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_RAISE_FAST_ACTION);

		// Bulk create Entering bulk count
		click("checkBox_Action_BulkRecord");
		sendKeys("textField_Action_BulkRecordCount", bulkCount);
		// Raise Fast Action by click on Create button
		clickAndWait("button_Action_Raise");
		// Verification of View Action Page
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		String succMess1 = getText("successMsg_Action_Create");
		System.out.println(succMess1 + "  succMess1");
		String[] actUrn1 = succMess1.split(" ");
	    bulkAction = actUrn1[5];
		return bulkAction;

	}
		
}

