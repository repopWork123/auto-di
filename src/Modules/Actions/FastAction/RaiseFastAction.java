package Modules.Actions.FastAction;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Modules.Actions.MisrapAction.MirsapAction;
import Modules.Broadcasts.Broadcast;
import Modules.Documents.Document;
import ReusableUtilities.Dataprovider;
import ReusableUtilities.Report;

public class RaiseFastAction extends FastAction {
	
	
	// Data used to create Officer
	private static final String CREATE_OFFICER_SURNAME="SurName";
	private static final String CREATE_OFFICER_FORENAME="ForeName";
	private static final String CREATE_OFFICER_OFFICERNO_MAX_CHAR = "0123456789";
	
	//Data used to Update field values while creating similar action
	private static final String UPDATE_SIMILAR_ACTION= " "+"Update similarAction";
	
	//Data used to Verify Current Action State
	private static final String ALLOCATION= "For Allocation";
	private static final String ALLOCATED = "Allocated";
	private static final String CONVERTACTION_WITHOUT_FORCE = "Force is required";
	private static final String CONVERTACTION_WITHOUT_STATION = "Station is required";
	private static final String CONVERTACTION_WITHOUT_CLASS = "Class is required";
	private static final String CONVERTACTION_WITHOUT_SUBJECT = "Subject(s) is required";
	
	//URN Starting Expression
	private static final String DISCLOSUREEXPRESSION="DI";
	private static final String ACTIONEXPRESSION="A";

	
	//Date used to create nominal entity
    private static final String Nominal_Surname="Surname";
	
	//	IncidentID_full=MORSTA16D61 which is present in config.properties is read and stored in incidentIDfull
	String incidentIdFull = getCurrentIncidentIDFull();

	// Data for Linked Actions/ Documents Grid read from Xl sheet
	private String linkedActionsRecordURN;
	private String linkedDocumentsRecordURN;
	private String linkedActionsRecordURN2;
	private String linkedDocumentsRecordURN2;
	private String takenDocumentsRecordURN;
	
	private String polActionURN;
	private String polURN;
	private String fastActionURN; 
	String MIRSAP_FORCE="ARMY";
	String MIRSAP_CLASS="SCENE";
	String MIRSAP_PRIORITY="Medium";
	String MIRSAP_STATION="HEADQUARTERS";
	
	//Action URN's to be reused during update / similar
	private String fastActionURN_textAndTitle;
	private String fastActionURN_allFieldValues;
	private String fastActionDocumentGridURN;
	private String fastActionHugeTextURN;
	private String fastActionWithAlphaNumericURN;
	private String fastActionWithSpecialCharsURN;
	private String disclosureURN;
	private String disclosureURN_allFieldValues;
	private String fastActionURN_allFieldValues_bulkValidLimit;
	private String fastActionURN_allFieldValues_similar;
	private String actionThemeOne;
	private String actionThemeTwo;
	private String actionThemeThree;


	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}
	

	//default constructor
	public RaiseFastAction() {
		softAssert = new SoftAssert();
	}
	
	@BeforeTest
	public void beforeTest() throws Exception {
		// Steps starting from IE driver instance creation,  Navigate to app URl, Login , select default incident on Home Page	are executed 	
		loginInIEAndSelectIncident();
//			loginAndSelectIncident();

	}
	
	

	@Test//(dataProvider = "RaiseFastAction")
	//priority = 1,
	public void testData() throws Exception{
		
		// create POL
		Broadcast broadcast = new Broadcast(driver);
		polURN = broadcast.raisePOL_AndGetURN("Title");
		
		//getting read only action
		navigateTo(ACTION_DASHBOARD);
		explicitlyWaitForElement("button_Action_Search",getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Dashboard_Title","Title");
		click("button_Action_Search");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		polActionURN = getText("table_Action_SearchResults_URN");
		System.out.println(polActionURN+"fastActionPOL_ReadOnlyURN");
		
		// creating bulk action
	    String bulkURn=raiseBulkFastActionAndGetURNs("2");
		
		String[] actURN_Split = bulkURn.split("-");
		linkedActionsRecordURN = actURN_Split[0];
		System.out.println(linkedActionsRecordURN+"  firstURN");
		String fastactURNcount = linkedActionsRecordURN.substring(1);
		// getting second action urn
		int fastactURNcounts = Integer.parseInt(fastactURNcount)+1;
    	linkedActionsRecordURN2="A"+String.valueOf(fastactURNcounts);
    	System.out.println(linkedActionsRecordURN2+"nextFastActionURn");
		
		
    	//create bulk document
    	Document doc = new Document(driver);
		String bulkDocument = doc.registerBulkOtherDocumentAndGetURNsWithSubjectReference("2");
		String[] docURN_Split = bulkDocument.split("-");
		linkedDocumentsRecordURN = docURN_Split[0];
		String docURNcount = linkedDocumentsRecordURN.substring(1);
		int docURNcounts = Integer.parseInt(docURNcount)+1;
		linkedDocumentsRecordURN2="D"+String.valueOf(docURNcounts);
		System.out.println(linkedDocumentsRecordURN2+":::linkedDocumentsRecordURN2");
		
		//getting themes name form config file
		actionThemeOne=readConfigFile("actionThemeOne");
		actionThemeTwo=readConfigFile("actionThemeTwo");
		actionThemeThree=readConfigFile("actionThemeThree");
		
		System.out.println("linkedActionsRecordURN: " + linkedActionsRecordURN);
		System.out.println("linkedDocumentsRecordURN: " + linkedDocumentsRecordURN);
		System.out.println("linkedDocumentsRecordURN: " + linkedDocumentsRecordURN);
		System.out.println("linkedActionsRecordURN2: " + linkedActionsRecordURN2);
		System.out.println("linkedDocumentsRecordURN2: " + linkedDocumentsRecordURN2);
		System.out.println("takenDocumentsRecordURN: " + takenDocumentsRecordURN);
		System.out.println("actionThemeOne: " + actionThemeOne);
		System.out.println("actionThemeTwo: " + actionThemeTwo);
		System.out.println("polURN: " + polURN);
		System.out.println("polActionURN: " + polActionURN);
	}
	
	
	// Starting Step of Execution is from @Before Test Annotation	
	

	// Test case 26759:001. Raise Fast Action with Mandatory Values
	@Test
	public void createFastAction_withMandatoryFieldDetails(Method method)throws Exception{
		startLine(method);
	
		//navigates to action url = "/group/holmes/raise-fast-action"
		navigateTo(RAISE_FAST_ACTION_URL);
		// verifies Fast Action Tab is enabled
		verifyFastActionTabEnabled();
	
		// Wait for Create button on Action create page for 60secs
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		// Confirm Create Button on Action Create Page is present
		verifyElementPresent("button_Action_Raise");
	
		
		verifyFastActionFieldsInCreatePage();
		// Verify all mandatory field list with '*'
		verifyMandatoryFieldsInFastAction();
	
		//Pass data only to mandatory fields
		sendKeys("textField_Action_Title", ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE);
	
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		// Verification of View Action Page
		verifyElementPresent("successMsg_Action_Create");
		//validate Fast Action Success Message
		validateFastActionSuccMess();
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		fastActionURN = actUrn[2];
		Report.info("fastAction URN " + fastActionURN);
		// Validate inserted title in create page same as in view page
		verifyText( ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE ,
				"label_Action_View_Title");
		//Verify currently incident id is selected
		verifyTextContains(incidentIdFull, "label_Action_View_Section_Title");
		// Validate Protective Marking : Restricted is set as default
		verifyText(PROTECTIVE_MARKING_RESTRICTED, "label_Action_Mirsap_View_ProtectedMarking");
		// Validate  Priority : Low is set as default value
		verifyText(PRIORITY_LOW, "label_Action_Mirsap_View_Priority");
		
		//verify Text field text is not same as Title 
		verifyTextNotEquals(getTextFieldText(), "label_Action_View_Title");
	
		// verify disclosure record in disclosure section in view page
		expandDisclosureSection();
		verifyElementPresent("table_Action_View_Diclosure_URN");
		disclosureURN = getText("table_Action_View_Diclosure_URN");
		Report.info("disclosureURN: " + disclosureURN);
		// verify disclosure association in Association tab
		clickAssociationTabAndVerifyIsInAssociationTab();
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord", getTotalWaitTimeInSecs());
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + disclosureURN);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
	
		 
		endLine(method);
	}


	public void clickAssociationTabAndVerifyIsInAssociationTab()
			throws Exception {
		explicitlyWaitForElement("tab_Association", getTotalWaitTimeInSecs());
		/*click("tab_Marker");
		verifyMarkerTabEnabled();*/
//		click("tab_Associations");
		WebElement element = explicitlyWaitAndGetWebElement("tab_Association", getTotalWaitTimeInSecs());
		clickElementUsingJS(element);
		waitForJQuery(driver);
//		click("tab_Association");
//		explicitlyWaitForElement("tab_Association", getTotalWaitTimeInSecs());
		verifyAssociationTabEnabled();
	}

	// Test case 8121:001a. View Action priority=3,
	@Test
	//( dependsOnMethods = {"createFastAction_withMandatoryFieldDetails"})
	public void viewAction(Method method) throws Exception{
		startLine(method);
		//navigate to view action page
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		// verify cursor focus in URN text field
		verifyCurrentCursorFocus("textField_Action_ViewUrn");
		sendKeys("textField_Action_ViewUrn", fastActionURN);
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		isInFastActionViewPage();
		//Verify currently incident id is selected
		verifyTextContains(incidentIdFull, "label_Action_View_Page_Title");
		
		//verifyHistoryTab
		click("tab_History");
		explicitlyWaitForElement("table_History_HistoryRecord_Username",
				getTotalWaitTimeInSecs());
		verifyText(usernameUpperCase,"table_History_HistoryRecord_Username");
		verifyTextContains(VIEW_FAST_ACTION,"table_Action_FastAction_HistoryRecord_ViewFastAction");
		click("table_Action_FastAction_HistoryRecord_ViewFastAction");
		viewHistoryTabSearchResult(VIEW_FAST_ACTION);

		 
		endLine(method);
	} 
	
	/* Test case 29293:001b. Update Fast Action without Mandatory Values  priority = 4,*/
	@Test
	//( dependsOnMethods = {"createFastAction_withMandatoryFieldDetails"})
	public void updateFastActionWithOutMandatoryFields(Method method) throws Exception{
		startLine(method);
		//navigate to view action page
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		sendKeys("textField_Action_ViewUrn", fastActionURN);
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		isInFastActionViewPage();
		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		//verify is in Edit Fast action page
		isInFastActionEditPage();
		
		//Pass empty text data only to mandatory fields
		sendKeys("textField_Action_Title", "");
		
		//click Save button
		clickAndWait("button_Action_Save");
		
		verifyText(RAISE_ACTION_WITHOUT_MANDATORY_VALUES_VALIDATION_MESS,"errorMsg_Action_Create_WithOutAnyFields");
		verifyText(TITLE_VALIDATION_MESS,"errorMsg_Action_Title");
		
		 
		endLine(method);
	}

	/* Test case 29290:001c. Update Fast Action with Mandatory Values priority = 5,  */
	@Test
	//(dependsOnMethods = {"createFastAction_withMandatoryFieldDetails"})
	public void updateFastActionWithMandatoryFields(Method method) throws Exception{
		startLine(method);
		//navigate to view action page
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		sendKeys("textField_Action_ViewUrn", fastActionURN);
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		//verify is in Fast action View page
		isInFastActionViewPage();
		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		//verify is in Edit Fast action page
		isInFastActionEditPage();
		verifyFastActionTabEnabled();
		
		//Verify the Editable UI Components
		verifyFastActionFieldsInEditPage();
		
		// Verify all mandatory field list with '*'
		verifyMandatoryFieldsInEditFastAction();
		
		//Pass data only to mandatory fields
		sendKeys("textField_Action_Title", ACTION_AUTO_TEST_FOR_UPDATE_FAST_ACTION_TITLE);
		//Also update priority field
		selectByVisibleText("dropDown_Action_Mirsap_Priority", PRIORITY_HIGH);
		//click Save button
		clickAndWait("button_Action_Save");
		waitForPageLoad();
		isInFastActionViewPage();
		validateFastActionUpdateSuccMess(fastActionURN);
		// Validate  Priority : High as update in edit screen
		verifyText(PRIORITY_HIGH, "label_Action_Mirsap_View_Priority");
		//validate title 
		verifyText(ACTION_AUTO_TEST_FOR_UPDATE_FAST_ACTION_TITLE, "label_Action_View_Title");
		endLine(method);
	}
	
	// Test case 26760:002. Raise Fast Action without Mandatory Values
	@Test
	public void raiseFastActionWithoutMandatoryValues(Method method) throws Exception {
		startLine(method);

		navigateTo(RAISE_FAST_ACTION_URL);
		// verifies Fast Action Tab is enabled
		verifyFastActionTabEnabled();

		// Wait for Create button on Action create page for 60secs
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		// Confirm Create Button on Action Create Page is present
		verifyElementPresent("button_Action_Raise");
		verifyFastActionFieldsInCreatePage();

		clickAndWait("button_Action_Raise");

		verifyText(RAISE_ACTION_WITHOUT_MANDATORY_VALUES_VALIDATION_MESS, "errorMsg_Action_Create_WithOutAnyFields");
		selectByVisibleText("dropDown_Action_Create_Incident", SELECT_GROUP);
		clickAndWait("button_Action_Raise");
		verifyText(RAISE_ACTION_WITHOUT_INCIDENT_VALIDATION_MESS, "errorMsg_Action_Create_WithOutAnyFields");
		selectTextByValue("dropDown_Action_Create_Incident", incidentIdFull);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", "");
		clickAndWait("button_Action_Raise");
		// Check for validaiton mess "There are validation errors" , "Title is Required", "Priority is Required"
		verifyText(RAISE_ACTION_WITHOUT_MANDATORY_VALUES_VALIDATION_MESS,"errorMsg_Action_Create_WithOutAnyFields");
		verifyText(TITLE_VALIDATION_MESS,"errorMsg_Action_Title");
		verifyText(PRIORITY_VALIDATION_MESS,"errorMsg_Action_Priority");
		//In Create Officer pop up window : Validation error message should be  displayed as : Please provide a Surname
		click("button_Action_FastAction_CreateOfficer");
		click("button_Action_FastAction_CreateOfficeDialog_Create");
		verifyText(CREATE_OFFICER_VALIDATION_MESS,"errorMsg_Action_CreateOfficer_SurnameAndForname");
		click("icon_Action_Create_CreateOfficer_Dialog_Close");

		endLine(method);
	}
	
	//Test case 26761:003.Raise Fast Action with all field values
	@Test
	public void raiseFastAction_withAllFieldDetails(Method method)throws Exception{
		//Prints the starting of current Method name as  - ------------createFastAction_withAllFieldDetails- Test Starting------------
		startLine(method);
		//From Base url = http://gbhlmmorstana1.uleaf.site:8080 navigates to action url = "/group/holmes/raise-fast-action"
		navigateTo(RAISE_FAST_ACTION_URL);
		// verifies 1st Tab - Fast Action Tab is enabled- Xpath of Fast Action Tab is  passed as parameter
		verifyFastActionTabEnabled();
		// Wait for Create button on Action create page for 60secs
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		// Confirm Create Button on Action Create Page is present
		verifyElementPresent("button_Action_Raise");
		verifyFastActionFieldsInCreatePage();
		// Verify all mandatory field list with '*'
		verifyMandatoryFieldsInFastAction();
		//Verify Default Incident ID in Incident DropDown
		verifyTextContains(incidentIdFull, "dropDown_Action_Create_Incident");
		
		// Validate Protective Marking : Restricted is set as default
		verifyCurrentSelectByText("dropDown_Action_Create_ProtectiveMarking",PROTECTIVE_MARKING_RESTRICTED);
		// Validate  Priority : Low is set as default value
		verifyCurrentSelectByText("dropDown_Action_Mirsap_Priority",PRIORITY_LOW);
		verifyText(ALLOCATION ,"label_Action_Create_State");
		
		//Pass data to all fields
		sendKeys("textField_Action_Title",  ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE );
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_RAISE_FAST_ACTION);
		explicitlyWaitForElement("button_Action_ChangeState_CreateOfficer", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_ChangeState_CreateOfficer");
		click("button_Action_ChangeState_CreateOfficer");
		explicitlyWaitForElement("textfield_Action_Create_CreateOfficer_Surname", getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_Create_CreateOfficer_Surname", CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_Create_CreateOfficer_Forename", CREATE_OFFICER_FORENAME);
		click("button_Action_FastAction_CreateOfficeDialog_Create");
		explicitlyWaitForElement("textField_Action_OriginatingDetails", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_OriginatingDetails",ACTION_ORIGINATING_DETAIL);

		//link_Action_Create_Theme
		click("checkBox_Action_Themes_Item1");
		//click("checkBox_Action_Themes");

		//Adding Linked Action URN
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN",linkedActionsRecordURN);
		addFastActionLinkedActionFrmSelectRecord();
		
		//Adding Linked Document URN
		explicitlyWaitForElement("button_Action_Create_LinkedAssoc_Documents", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN);
		String xpathForCurrentIncident2 = "(//div[@id='" + incidentIdFull + "'])[2]";
		addFastActionAssociatedDocumentFrmSelectRecord(xpathForCurrentIncident2);
				
		// Raise Fast Action by click on Create button 
		explicitlyWaitForElement("button_Action_Raise", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Raise");
		// Verification of View Action Page
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		//validate Fast Action Success Message
		validateFastActionSuccMess();
		verifyElementPresent("successMsg_Action_Create");
		//Get the fastActionURN 
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		fastActionURN_allFieldValues = actUrn[2];
		Report.info("fastActionURN_allFiledValues: " + fastActionURN_allFieldValues );
		//Verify all field values
		verifyText( ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE ,
				"label_Action_View_Title");
		System.out.println("text from tinymce in view page" + getTextFieldText());
		verifyTextCompare(TINY_MCE_TEXT_FOR_RAISE_FAST_ACTION, getTextFieldText());
		
		// Validate Protective Marking : Restricted is set as default
		verifyText(PROTECTIVE_MARKING_RESTRICTED, "label_Action_Mirsap_View_ProtectedMarking");
		// Validate  Priority : Low is set as default value
		verifyText(PRIORITY_LOW, "label_Action_Mirsap_View_Priority");
		verifyText(ALLOCATED ,"label_Action_View_State");
		verifyText(CREATE_OFFICER_SURNAME+" "+CREATE_OFFICER_FORENAME,"textField_Action_view_CreateOfficer");
		verifyText(ACTION_ORIGINATING_DETAIL,
				"label_Action_FastAction_View_OriginatingDetails");
		
		//ToDo - Verify Themes
		verifyText(actionThemeOne,"table_Action_ThemeItem");
		
		//To Verify Grid Section Items
		verifyText(linkedActionsRecordURN, "table_LinkedAction_URN");
		verifyText(linkedDocumentsRecordURN,"table_AssociatedDocuments_URN");
		
		/*//Verify Disclosure Record
		click("link_Action_View_Disclosure");
		verifyElementPresent("table_Action_View_Diclosure_URN");*/
		// verify disclosure record in disclosure section in view page
		expandDisclosureSection();
		verifyElementPresent("table_Action_View_Diclosure_URN");
		disclosureURN_allFieldValues = getText("table_Action_View_Diclosure_URN");
		Report.info("disclosureURN_allFieldValues: " + disclosureURN_allFieldValues);
		//verify Association Tab Records
//		click("tab_Association");
		WebElement element = driver.findElement(map.getLocator("tab_Association"));
		clickElementUsingJS(element);
		/*waitForJQuery(driver);
		verifyAssociationTabEnabled();*/
		verifyAssociationTabEnabled();
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		verifyElementPresent("table_Action_AssociationTab_LinkAction");
		verifyElementPresent("table_Action_AssociationTab_Associated");
		verifyElementPresent("table_Action_AssociationTab_Disclosure");
		// 
		
		//verifyHistoryTab
		click("tab_History");
		explicitlyWaitForElement("table_History_HistoryRecord_Username",
				getTotalWaitTimeInSecs());
		verifyText(usernameUpperCase,"table_History_HistoryRecord_Username");
		verifyTextContains(CREATE_FAST_ACTION,"table_Action_FastAction_HistoryRecord_CreateFastAction");
		click("table_Action_FastAction_HistoryRecord_CreateFastAction");
		viewHistoryTabSearchResult(CREATE_FAST_ACTION);
		
		//Prints the ending of current Method name as  - ------------createFastAction_withAllFieldDetails Test Ending------------ 
		endLine(method);


	}
	
	/* Test case 29273:003a.View Action- Pop Up priority = 8,  */
	@Test
	//(dependsOnMethods = { "raiseFastAction_withAllFieldDetails" })
	public void viewFastAction_InPopup(Method method) throws Exception {

		startLine(method);
		WebDriver driverTemp = driver;
		/*String addToVisualizationDialogContent = "Selected record(s) added successfully to Visualiser\n"
				+ incidentIdFull + "-" + "A558";*/
		// navigate to Raise Fast Action URL
		navigateTo(RAISE_FAST_ACTION_URL);
		String parentWindowHandle = getCurrentWindowHandle();
		Report.info("Parent window handle info: " + parentWindowHandle);
		// Opening new window and switch to that window
		/*openNewWindowAndGetHandle();*/
		WebDriver childViewActionWebDriver = openNewWindowAndGetHandleWithChildWebDriver(driver);
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		String childViewActionWindowHandle = getCurrentWindowHandle();
		Report.info("child View Action popup window handle info: " + childViewActionWindowHandle);
		
		/*		
		fastActionURN_allFieldValues = "A456"; disclosureURN_allFieldValues = "DI460";
		 
		sendKeys("textField_Action_ViewUrn", "A456");*/
		
		if(fastActionURN_allFieldValues == null ){
			fastActionURN_allFieldValues = fastActionURN;
			disclosureURN_allFieldValues = disclosureURN;
		}
		
		
		 sendKeys("textField_Action_ViewUrn", fastActionURN_allFieldValues);
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title",
				getTotalWaitTimeInSecs());
		isInFastActionViewPage();
		// Verify currently incident id is selected
		verifyTextContains(incidentIdFull, "label_Action_View_Page_Title");
		// View Fast Action " Incident -URN" in Incident -URN Title"
		verifyTextContains(incidentIdFull + "-" + fastActionURN_allFieldValues,
				"label_Action_View_Page_Title");
		// Verify Title and Priority field value matches user input.
		// Validate inserted title in create page same as in view page
		verifyText(ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE,
				"label_Action_View_Title");
		// Validate Priority : Low is set as default value
		verifyText(PRIORITY_LOW, "label_Action_Mirsap_View_Priority");
		// Check All Tab alignment's is NOT done as part of validation
		
		// View Action- Add to Visualisation icon
		String childWindowHandle1;
		/*clickOnAddToVisualizationIcon("icon_Action_View_Visualization");*/
		// Fast Action record is added to Favourite list and valid message is
		// displayed
		clickOnAddToFavListIcon("icon_Action_View_FavList");
		// Fast Action record is added to default list and valid message is
		// displayed
		clickOnAddToDefaultListIcon("icon_Action_View_DefaultList");
		// Fast Action record is added to new or existing list and valid message
		// is displayed
		addToNewOrExistingListIcon("icon_Action_View_NewOrExistingList");
		// TO EXPORT TO CSV IS NOT AUTOMATED

		// View in New window
		clickOnViewInNewWindowIcon("icon_Action_View_ViewInNewWindow", FAST_ACTION_WINDOW_TITLE);

		// view the Action / Fast Action record in association tab of other
		// record - By Double Click
		isInFastActionViewPage();
		explicitlyWaitForElement("button_Action_Update",
				getTotalWaitTimeInSecs());
		// verify Association Tab Records
		explicitlyWaitForElement("tab_Association",
				getPoolWaitTime());
		clickAndWait("tab_Association");
		waitForPageLoad();
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getTotalWaitTimeInSecs());
		 verifyAssociationTabEnabled();
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + disclosureURN_allFieldValues);
		waitForJQuery(childViewActionWebDriver);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		String disclourexpath = "//table[contains(@id,'associationPanelGridTable')]//tr[@id='0']/td[contains(.,'" + incidentIdFull +"-" + disclosureURN_allFieldValues + "')][following-sibling::td[contains(.,'Disclosure')]]";
		explicityWaitForAnElementWithXpathAndDoubleClick(disclourexpath, getTotalWaitTimeInSecs());
		/*explicitlyWaitForElement("table_AssociationTab_FirstRow_Disclosure", getTotalWaitTimeInSecs());
		doubleClickAndWait("table_AssociationTab_FirstRow_Disclosure");*/
		verifyText(disclosureURN_allFieldValues, "label_Disclosure_View_URN");
		explicitlyWaitForElement("tab_Association",
				getPoolWaitTime());
		clickAndWait("tab_Association");
		verifyAssociationTabEnabled();
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + fastActionURN_allFieldValues);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		waitForJQuery(childViewActionWebDriver);
		explicitlyWaitForElement("table_Action_AssociationTab_FirstRow",
				getTotalWaitTimeInSecs());
		doubleClickAndWait("table_Action_AssociationTab_FirstRow");

		// view the Action / Fast Action record in association tab of other
		// record - View the selected Entity
		waitForPageLoad();
		
		isInFastActionViewPage();
		explicitlyWaitForElement("button_Action_Update",
				getTotalWaitTimeInSecs());
		
		//verifyHistoryTab
		click("tab_History");
		explicitlyWaitForElement("table_History_HistoryRecord_Username",
				getTotalWaitTimeInSecs());
		verifyText(usernameUpperCase,"table_History_HistoryRecord_Username");
		verifyTextContains(VIEW_FAST_ACTION,"table_Action_FastAction_HistoryRecord_ViewFastAction");
		click("table_Action_FastAction_HistoryRecord_ViewFastAction");
		viewHistoryTabSearchResult(VIEW_FAST_ACTION);
		
		// verify Association Tab Records
		explicitlyWaitForElement("tab_Association",
				getPoolWaitTime());
		clickAndWait("tab_Association");
		waitForPageLoad();
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getTotalWaitTimeInSecs());
		verifyAssociationTabEnabled();
				 
		waitForJQuery(childViewActionWebDriver);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + disclosureURN_allFieldValues);
		waitForJQuery(childViewActionWebDriver);
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getPoolWaitTime());
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		checkCheckBox("checkBox_Action_Association_FirstRecord");
		explicitlyWaitForElement("icon_Action_Association_View", getTotalWaitTimeInSecs());
		click("icon_Action_Association_View");
		verifyText(disclosureURN_allFieldValues, "label_Disclosure_View_URN");
		explicitlyWaitForElement("tab_Association",
				getTotalWaitTimeInSecs());
		clickAndWait("tab_Association");
		verifyAssociationTabEnabled();
		waitForJQuery(childViewActionWebDriver);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + fastActionURN_allFieldValues);
		waitForJQuery(childViewActionWebDriver);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		explicitlyWaitForElement("table_Action_AssociationTab_FirstRow",
				getTotalWaitTimeInSecs());
		doubleClickAndWait("table_Action_AssociationTab_FirstRow");

		// view the Action / Fast Action record in association tab of other
		// record - View the selected entity in a new Window
		isInFastActionViewPage();
		explicitlyWaitForElement("button_Action_Update",
				getTotalWaitTimeInSecs());
		// verify Association Tab Records
		explicitlyWaitForElement("tab_Association",
				getTotalWaitTimeInSecs());
		clickAndWait("tab_Association");
		waitForPageLoad();
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getTotalWaitTimeInSecs());
		verifyAssociationTabEnabled();
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + disclosureURN_allFieldValues);
		waitForJQuery(childViewActionWebDriver);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord", getTotalWaitTimeInSecs());
		click("checkBox_Action_Association_FirstRecord");
		childWindowHandle1 = getCurrentWindowHandle();
		
		ArrayList<String> winHandList = new ArrayList<String>();
		winHandList.add(parentWindowHandle);
		winHandList.add(childWindowHandle1);
		
		int noOfwindowsCount = getWindowCount();
		WebDriver disclosureWinDriver = clickAndWaitForChildWindow(
				DISCLOSURE_WINDOW_TITLE,winHandList,
				"icon_Action_Association_ViewInNewWindow", noOfwindowsCount, noOfwindowsCount+1);
		String childWindowHandle3 = getCurrentWindowHandle();
		explicitlyWaitForElement("label_Disclosure_View_URN", getTotalWaitTimeInSecs());
		verifyText(disclosureURN_allFieldValues, "label_Disclosure_View_URN");
		explicitlyWaitForElement("tab_Association",
				getTotalWaitTimeInSecs());
		clickAndWait("tab_Association");
		verifyAssociationTabEnabled();
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + fastActionURN_allFieldValues);
		waitForJQuery(disclosureWinDriver);
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord", getTotalWaitTimeInSecs());
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		explicitlyWaitForElement("table_Action_AssociationTab_FirstRow", getTotalWaitTimeInSecs());
		doubleClickAndWait("table_Action_AssociationTab_FirstRow");
		
		waitForPageLoad();
		/*waitForJQuery(disclosureWinDriver);*/
		
		//close current window
		disclosureWinDriver.close();
		driver.switchTo().window(childWindowHandle1);
		
	/*	WebDriver childWindowHandleAfterDisclosureNewWindow = closechildWindowAndNavigateToItsParentWindowHandle(
				disclosureWinDriver, childWindowHandle1, 3, 2);
		
		Report.info("Child window handle after done closing view in New Window: "
				+ childWindowHandleAfterDisclosureNewWindow.getWindowHandle()
				+ " Expected is: " + childWindowHandle1);*/
		int noOfwindowsCount2 = getWindowCount();
		closechildWindowAndNavigateToItsParentWindowHandle(childViewActionWebDriver, parentWindowHandle, noOfwindowsCount2, noOfwindowsCount2-1);
		
		driver = driverTemp;
		
		endLine(method);
	}




	//Test case 29295: 003b. Update Fast Action with all field values - Single Action Record
	//Requires Fast Action to already be created and a linked action, document and document taken priority = 9, 
	@Test
	//(dependsOnMethods = {"raiseFastAction_withAllFieldDetails"})
	public void updateFastAction_WithAllFields(Method method) throws Exception {

		startLine(method);
		// navigate to view action page
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		explicitlyWaitForElement("textField_Action_ViewUrn", getTotalWaitTimeInSecs());
		if(fastActionURN_allFieldValues == null ){
			fastActionURN_allFieldValues = fastActionURN;
		}
		// set temp value of A14 for action
		if(fastActionURN_allFieldValues == null){
			fastActionURN_allFieldValues = fastActionURN;
		}
		sendKeys("textField_Action_ViewUrn", fastActionURN_allFieldValues);
		clickAndWaitForPageLoad("button_Action_View");
		waitForJQuery(driver);
		// verify is in Fast action View page
		isInFastActionViewPage();
		explicitlyWaitForElement("button_Action_Update", getTotalWaitTimeInSecs());
		// click update button
		verifyElementPresent("button_Action_Update");
		// click update button
		explicitlyWaitForElement("button_Action_Update", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Update");
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		// verify is in Edit Fast action page
		isInFastActionEditPage();
		
		
		// change PM
		selectByVisibleText("dropDown_Action_Create_ProtectiveMarking",
				"SECRET");
		// change priority
		selectByVisibleText("dropDown_Action_Mirsap_Priority", "High");

		// link_Action_Create_Theme
		click("checkBox_Action_Themes");
		
		// update title field
		explicitlyWaitForElement("textField_Action_Title", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Title", "Updated Title for Action");
		
		// Adding Linked Action URN
		explicitlyWaitForElement("button_Action_Create_LinkedAssoc_Action", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement(
				"textField_Action_Create_LinkedAssoc_Action_URN",
				getTotalWaitTimeInSecs());
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN",
				linkedActionsRecordURN2);
		explicitlyWaitForElement("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon", getTotalWaitTimeInSecs());
		/*click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		String xpathForCurrentIncident = "//div[@id='" + incidentIdFull + "']";
		WebElement we = driver.findElement(By.xpath(xpathForCurrentIncident));
		we.click();
		clickAndWait("button_Action_Create_LinkedAssoc_Action_Update");*/
		addFastActionLinkedActionFrmSelectRecordInEditPage();

		// Adding Linked Document URN
		explicitlyWaitForElement("button_Action_Create_LinkedAssoc_Documents", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement(
				"textField_Action_Create_LinkedAssoc_Document_URN",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",
				linkedDocumentsRecordURN2);
		click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1");
		pressReturnKey("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1");
		clickAndWait("button_Action_Create_LinkedAssoc_Documents_Add");

		// Adding Taken Document URN
		explicitlyWaitForElement("button_Action_Update_DocmentsTaken_Documents", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Update_DocmentsTaken_Documents");
		explicitlyWaitAndGetWebElement(
				"textField_Action_Update_DocumentTaken_Document_URN",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",
				takenDocumentsRecordURN);
		click("dropDown_Action_Fast_SelectRecord_ReferenceIncident_Test");
		pressReturnKey("dropDown_Action_Fast_SelectRecord_ReferenceIncident_Test");
		clickAndWait("button_Action_Create_DocumentTaken_Documents_Add");
		
		// send text to originating details
		explicitlyWaitForElement("textField_Action_OriginatingDetails", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);

		String docContent = readTextFile(System.getProperty("user.dir")
				+ "\\hugeDataForTextField.txt");
		insertIntoAllTinyMCEinPage(docContent);
		
		// click save
		clickAndWait("button_Action_Save");
		validateFastActionUpdateSuccMess(fastActionURN_allFieldValues);
		isInFastActionViewPage();
		
		//verifyHistoryTab
		click("tab_History");
		explicitlyWaitForElement("table_History_HistoryRecord_Username",
				getTotalWaitTimeInSecs());
		verifyText(usernameUpperCase,"table_History_HistoryRecord_Username");
		verifyTextContains(UPDATE_FAST_ACTION,"table_Action_FastAction_HistoryRecord_UpdateFastAction");
		click("table_Action_FastAction_HistoryRecord_UpdateFastAction");
		viewHistoryTabSearchResult(UPDATE_FAST_ACTION);

		endLine(method);
	}
	
	// Test case 26762:004. Raise Fast Action with all field values - Bulk -
	// Valid Limit
	 @Test
	 //(priority=10)
	public void raiseFastAction_withAllFieldDetails_Bulk_ValidLimit(
			Method method) throws Exception {
		// Prints the starting of current Method name as -
		// ------------createFastAction_withAllFieldDetails- Test
		// Starting------------
		startLine(method);
		// From Base url = http://gbhlmmorstana1.uleaf.site:8080 navigates to
		// action url = "/group/holmes/raise-fast-action"
		navigateTo(RAISE_FAST_ACTION_URL);
		// verifies 1st Tab - Fast Action Tab is enabled- Xpath of Fast Action
		// Tab is passed as parameter
		verifyFastActionTabEnabled();
		// Wait for Create button on Action create page for 60secs
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		// Confirm Create Button on Action Create Page is present
		verifyElementPresent("button_Action_Raise");
		verifyFastActionFieldsInCreatePage();
		// Verify all mandatory field list with '*'
		verifyMandatoryFieldsInFastAction();
		// Verify Default Incident ID in Incident DropDown
		verifyTextContains(incidentIdFull, "dropDown_Action_Create_Incident");

		// Validate Protective Marking : Restricted is set as default
		verifyCurrentSelectByText("dropDown_Action_Create_ProtectiveMarking",
				PROTECTIVE_MARKING_RESTRICTED);
		// Validate Priority : Low is set as default value
		verifyCurrentSelectByText("dropDown_Action_Mirsap_Priority",
				PRIORITY_LOW);
		verifyText(ALLOCATION, "label_Action_Create_State");

		// Pass data to all fields
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE);
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_RAISE_FAST_ACTION);
		explicitlyWaitForElement("button_Action_ChangeState_CreateOfficer", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_ChangeState_CreateOfficer");
		click("button_Action_ChangeState_CreateOfficer");
		explicitlyWaitForElement("textfield_Action_Create_CreateOfficer_Surname", getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_Create_CreateOfficer_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_Create_CreateOfficer_Forename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_FastAction_CreateOfficeDialog_Create");
		explicitlyWaitForElement("textField_Action_OriginatingDetails", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);

		// link_Action_Create_Theme
		click("checkBox_Action_Themes_Item1");
		// click("checkBox_Action_Themes");

		// Adding Linked Action URN
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement(
				"textField_Action_Create_LinkedAssoc_Action_URN",
				getTotalWaitTimeInSecs());
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN",
				linkedActionsRecordURN);
		addFastActionLinkedActionFrmSelectRecord();

		// Adding Linked Document URN
		explicitlyWaitForElement("icon_Action_Create_AssociateDocument_Add", getTotalWaitTimeInSecs());
		clickAndWait("icon_Action_Create_AssociateDocument_Add");
		explicitlyWaitAndGetWebElement(
				"textField_Action_Create_LinkedAssoc_Document_URN",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",
				linkedDocumentsRecordURN);
		String xpathForCurrentIncident2 = "(//div[@id='" + incidentIdFull
				+ "'])[2]";
		addFastActionAssociatedDocumentFrmSelectRecord(xpathForCurrentIncident2);

		// Bulk create Entering 50 count
		explicitlyWaitForElement("checkBox_Action_BulkRecord", getTotalWaitTimeInSecs());
		click("checkBox_Action_BulkRecord");
		sendKeys("textField_Action_BulkRecordCount", bulkCreateCount);

		// Raise Fast Action by click on Create button
		explicitlyWaitForElement("button_Action_Raise", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Raise");
		// Verification of View Action Page
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		// validate Fast Action Success Message
		// TODO SHALINI/Suresh
		validateBulkFastActionSuccMess();
		verifyElementPresent("successMsg_Action_Create");
		// Get the fastActionURN
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		fastActionURN_allFieldValues_bulkValidLimit = actUrn[2];
		Report.info("fastActionURN_allFieldValues_bulkValidLimit: " + fastActionURN_allFieldValues_bulkValidLimit);
		// Verify all field values
		verifyText(ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE,
				"label_Action_View_Title");
		// System.out.println("text from tinymce in view page" +
		// getTextFieldText());
		verifyTextCompare(TINY_MCE_TEXT_FOR_RAISE_FAST_ACTION,
				getTextFieldText());

		// Validate Protective Marking : Restricted is set as default
		verifyText(PROTECTIVE_MARKING_RESTRICTED,
				"label_Action_Mirsap_View_ProtectedMarking");
		// Validate Priority : Low is set as default value
		verifyText(PRIORITY_LOW, "label_Action_Mirsap_View_Priority");
		verifyText(ALLOCATED, "label_Action_View_State");
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textField_Action_view_CreateOfficer");
		verifyText(ACTION_ORIGINATING_DETAIL,
				"label_Action_FastAction_View_OriginatingDetails");

		// ToDo - Verify Themes
		verifyText(actionThemeOne, "table_Action_ThemeItem");

		// To Verify Grid Section Items
		verifyText(linkedActionsRecordURN, "table_LinkedAction_URN");
		verifyText(linkedDocumentsRecordURN, "table_AssociatedDocuments_URN");

		// Verify Disclosure Record
		
		click("link_Action_View_Disclosure");
		// Disclosure verify in Last record of bulk create
		verifyElementPresent("table_Action_View_Diclosure_URN");
		String disclosureRecord_URN = getText("table_Action_View_Diclosure_URN");
		// Disclosure verify in first record of bulk create
		System.out.println(disclosureRecord_URN);
		String disclosureRecordCount = disclosureRecord_URN.substring(2);
		System.out.println(disclosureRecordCount);
		int x = Integer.parseInt(disclosureRecordCount);
		x = x - 49;
		String firstBulkDisclosreURN = DISCLOSUREEXPRESSION + x;
		// getting URN from view
		String action_URN = getText("label_Action_View_URN");
		String actionCount = action_URN.substring(1);
		System.out.println(actionCount);
		int y = Integer.parseInt(actionCount);
		y = y - 49;
		String firtBulkURN = ACTIONEXPRESSION + y;
		System.out.println(firtBulkURN);
		navigateTo(VIEWACTION_URL);
		sendKeys("textField_Action_ViewUrn", firtBulkURN);
		click("button_Action_View");
		explicitlyWaitForElement("button_Action_Similar",
				getTotalWaitTimeInSecs());
		click("link_Action_View_Disclosure");
		verifyText(firstBulkDisclosreURN, "table_Action_View_Diclosure_URN");
		endLine(method);

	}
		// Test case 29395:004a. Raise Fast Action with invalid bulk count
		// Create
	@Test
	//(priority = 11)
	public void createFastAction_WithBulkRecords_invalidLimit(Method method)
			throws Exception {
		startLine(method);
		// navigate to Raise Fast Action URL
		navigateTo(RAISE_FAST_ACTION_URL);
		verifyFastActionTabEnabled();
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");

		// Enter Bulk create 100 count
		click("checkBox_Action_BulkRecord");
		sendKeys("textField_Action_BulkRecordCount", bulkCreateCount_Limit);
		explicitlyWaitForElement("button_Action_Raise", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Raise");
		// Verify error when exceeding bulk create count
		verifyText("Provide a valid number between 1 and 50 for bulk create",
				"errorMsg_Action_BulkCreate_Count");
		endLine(method);

	}
		
	/* Test case 29407:Additional: Raise Fast Action with missing value on Number of Bulk Records*/
	//Create
	@Test
	//(priority = 12)
	public void createFastAction_MissingBulkRecords(Method method)
			throws Exception {
		startLine(method);
		// navigate to Raise Fast Action URL
		navigateTo(RAISE_FAST_ACTION_URL);
		verifyFastActionTabEnabled();
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		// verify Mandatory fields
		verifyMandatoryFieldsInFastAction();
		// insert text into mandatory fields
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE);
		insertIntoAllTinyMCEinPage("test");
		// Enter Bulk null
		click("checkBox_Action_BulkRecord");
		sendKeys("textField_Action_BulkRecordCount", "");
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		clickAndWait("button_Action_Raise");
		verifyText("Provide a valid number for bulk create",
				"errorMsg_Action_BulkCreate_Null");
		endLine(method);
	}

	// Test case 26763:005. Raise Fast Action with Huge Text Copy Paste
	@Test
	//(priority = 13)
	public void createFastAction_WithHugeText(Method method) throws Exception {
		startLine(method);
		// navigate to Raise Fast Action URL
		navigateTo(RAISE_FAST_ACTION_URL);
		// verifying Action Tab is Enabled
		verifyFastActionTabEnabled();
		verifyElementPresent("button_Action_Raise");

		// Pass huge text data to all fields
		// verify maximum data acceptable for Title is 70 characters
		WebElement title = getWebElement("textField_Action_Title");
		String titleMaxCharCount = title.getAttribute("size");
		verifyTextCompare(FAST_ACTION_TITLE_MAX_CHAR_SIZE, titleMaxCharCount);
		sendKeys("textField_Action_Title", ACTION_TITLE_MAX_CONTENT);
		String docContent = readTextFile(System.getProperty("user.dir")
				+ "\\hugeDataForTextField.txt");
		// System.out.println("text read: " + docContent);
		insertIntoAllTinyMCEinPage(docContent);

		// verify maximum data acceptable for Originating details is 70
		// characters
		WebElement originatingDetails = getWebElement("textField_Action_OriginatingDetails");
		String orginatingDetailsMaxCharCount = originatingDetails
				.getAttribute("size");
		verifyTextCompare(ORIGINATING_DETAILS_MAX_CHAR_SIZE,
				orginatingDetailsMaxCharCount);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL_MAX_CONTENT);

		// Create officer validation
		click("button_Action_FastAction_CreateOfficer");
		explicitlyWaitForElement("textfield_Action_Create_CreateOfficer_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_Create_CreateOfficer_Surname",
				CREATE_OFFICER_NAME_MAX_CHAR);
		sendKeys("textfield_Action_Create_CreateOfficer_Forename",
				CREATE_OFFICER_NAME_MAX_CHAR);
		sendKeys("textfield_Action_CreateOfficer_OfficerNo",
				CREATE_OFFICER_OFFICERNO_MAX_CHAR);
		click("button_Action_FastAction_CreateOfficeDialog_Create");

		verifyText(CREATE_OFFICER_MAX_CHAR_COUNT_VALIDATION_MESS,
				"errorMsg_Action_CreateOfficer_SurnameAndForname");
		click("icon_Action_Create_CreateOfficer_Dialog_Close");
		explicitlyWaitForElement("button_Action_Raise", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		// Verify Associated Document URN in View page
		isInFastActionViewPage();
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		fastActionHugeTextURN = actUrn[2];

		endLine(method);
	}

	//Test case 29299 005a. Update Fast Action with Huge Text priority = 14, 
    @Test
    //(dependsOnMethods = {"createFastAction_withMandatoryFieldDetails"})
	public void updateFastAction_WithHugeText(Method method) throws Exception{
		startLine(method);
		//open already existing fast action
		navigateTo(VIEWACTION_URL);
		//verifyIsInViewActionPage();
		// verify cursor focus in URN text field
		//verifyCurrentCursorFocus("textField_Action_ViewUrn");
		sendKeys("textField_Action_ViewUrn", fastActionURN);
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		isInFastActionViewPage();
		//click Update Button
		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		//verify is in Edit Fast action page
		isInFastActionEditPage();
		
		//insert large text
		//Pass huge text data to all fields
		// verify maximum data acceptable for Title is 70 characters
		WebElement title = getWebElement("textField_Action_Title");
		String titleMaxCharCount = title.getAttribute("size");
		verifyTextCompare(FAST_ACTION_TITLE_MAX_CHAR_SIZE, titleMaxCharCount);

		sendKeys("textField_Action_Title", ACTION_TITLE_MAX_CONTENT);
		String docContent = readTextFile(System.getProperty("user.dir")+"\\hugeDataForTextField.txt");
		//System.out.println("text read: " + docContent);
		insertIntoAllTinyMCEinPage(docContent);
		
		// verify maximum data acceptable for Originating details is 70 characters
		WebElement originatingDetails = getWebElement("textField_Action_OriginatingDetails");
		String orginatingDetailsMaxCharCount = originatingDetails.getAttribute("size");
		verifyTextCompare(ORIGINATING_DETAILS_MAX_CHAR_SIZE, orginatingDetailsMaxCharCount);

		sendKeys("textField_Action_OriginatingDetails", ACTION_ORIGINATING_DETAIL_MAX_CONTENT);
		
		//Create officer validation
		click("button_Action_FastAction_CreateOfficer");
		explicitlyWaitForElement("textfield_Action_CreateOfficer_Surname", getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname", CREATE_OFFICER_NAME_MAX_CHAR);
		sendKeys("textfield_Action_CreateOfficer_Forename",CREATE_OFFICER_NAME_MAX_CHAR);
		sendKeys("textfield_Action_CreateOfficer_OfficerNo", CREATE_OFFICER_OFFICERNO_MAX_CHAR);
		click("button_Action_FastAction_Edit_CreateOfficeDialog_Create");
		
		verifyText(CREATE_OFFICER_MAX_CHAR_COUNT_VALIDATION_MESS,"errorMsg_Action_CreateOfficer_SurnameAndForname");
		click("icon_Action_Create_CreateOfficer_Dialog_Close");
				
		sendKeys("textField_Action_FastAction_Edit_ReceiversInstructions",docContent);

		clickAndWait("button_Action_Save");
		//currently fails here
		validateFastActionUpdateSuccMess(fastActionURN);
		isInFastActionViewPage();
		endLine(method);
	}

    /*
	 * Test case 26764:006. Raise Fast Action with Special Characters in all fields- Test Date [12^& i]
	 */
	@Test
	//(priority = 15)
	public void createFastAction_WithEnteringAlphaAndSpecialCharacters(Method method)
			throws Exception {

		startLine(method);
		//navigate to Raise Fast Action URL
		navigateTo(RAISE_FAST_ACTION_URL);
		//verifying Action Tab is Enabled
		verifyFastActionTabEnabled();
		verifyElementPresent("button_Action_Raise");

		//insert Alpha numeric characters in Text field
		sendKeys("textField_Action_Title", ALPHA_NUMERIC_TEXT);
		insertIntoAllTinyMCEinPage(ALPHA_NUMERIC_TEXT);
		sendKeys("textField_Action_OriginatingDetails",
				ALPHA_NUMERIC_TEXT);
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		//verifying data in View action page
		verifyText(ALPHA_NUMERIC_TEXT, "label_Action_View_Title");
		verifyText(ALPHA_NUMERIC_TEXT,"label_Action_FastAction_View_OriginatingDetails");
		//navigate to Raise Fast Action URL
		navigateTo(RAISE_FAST_ACTION_URL);
		verifyFastActionTabEnabled();
		verifyElementPresent("button_Action_Raise");

		//inserting special characters in text
		sendKeys("textField_Action_Title", SPECIALCHARACTER_TEXT);
		insertIntoAllTinyMCEinPage(SPECIALCHARACTER_TEXT);
		sendKeys("textField_Action_OriginatingDetails",
				SPECIALCHARACTER_TEXT);
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		fastActionWithSpecialCharsURN = actUrn[2];
		//verifying special characters in view action page
		verifyText(SPECIALCHARACTER_TEXT, "label_Action_View_Title");
		verifyText(SPECIALCHARACTER_TEXT,"label_Action_FastAction_View_OriginatingDetails");

		//Entering  LinkedAction Urn and Document URN
		navigateTo(RAISE_FAST_ACTION_URL);
		verifyFastActionTabEnabled();
		verifyElementPresent("button_Action_Raise");
		sendKeys("textField_Action_Title", ALPHA_NUMERIC_TEXT);
		insertIntoAllTinyMCEinPage(ALPHA_NUMERIC_TEXT);
		sendKeys("textField_Action_OriginatingDetails",ALPHA_NUMERIC_TEXT);
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN",
				getTotalWaitTimeInSecs());

		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN", "A!!");
		addFastActionLinkedActionFrmSelectRecord();
		verifyText("A!! is not a valid URN","errorMsg_Action_LinkedAssociates_URN");
		click("link_Action_FastAction_Linkedction_Close");
		
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Document_URN","A!!");
		addFastActionAssociatedDocumentFrmSelectRecord("(//div[contains(div/@id,'" + incidentIdFull + "')])[2]");
		verifyTextContains(IS_NOT_A_VALID_URN_TEXT, "errorMsg_Action_AssociateDocument_InvalidURN");
		click("link_Action_FastAction_AssociateDocument_Close");
		
		
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		String succMess1 = getText("successMsg_Action_Create");
		String[] actUrn1 = succMess1.split(" ");
		fastActionWithAlphaNumericURN = actUrn1[2];
		
		//navigate to Raise Fast Action URL
		navigateTo(RAISE_FAST_ACTION_URL);
		//verifying Action Tab is Enabled
		verifyFastActionTabEnabled();
		verifyElementPresent("button_Action_Raise");
		
		//Create officer validation
		click("button_Action_FastAction_CreateOfficer");
		explicitlyWaitForElement("textfield_Action_Create_CreateOfficer_Surname", getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_Create_CreateOfficer_Surname", ALPHA_NUMERIC_TEXT);
		sendKeys("textfield_Action_Create_CreateOfficer_Forename",ALPHA_NUMERIC_TEXT);
		click("button_Action_FastAction_CreateOfficeDialog_Create");
		
		verifyText(CREATE_OFFICER_ALPHANUMERIC_VALIDATION_MESS,"errorMsg_Action_CreateOfficer_SurnameAndForname");
		click("icon_Action_Create_CreateOfficer_Dialog_Close");
		
		endLine(method);
	}
	
	/*
	 * Test case 26764:006. Raise Fast Action with Special Characters in all fields- Test Date [12^& i] -priority = 16, 
	 */
	@Test
	//(dependsOnMethods = {"createFastAction_WithEnteringAlphaAndSpecialCharacters"})
	public void updateFastAction_WithEnteringAlphaAndSpecialCharacters(Method method)
			throws Exception {

		startLine(method);
		//navigate to view action page
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		sendKeys("textField_Action_ViewUrn", fastActionWithAlphaNumericURN);
//		sendKeys("textField_Action_ViewUrn", "A36");
		clickAndWaitForPageLoad("button_Action_View");
		isInFastActionViewPage();
		
		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		//verify is in Edit Fast action page
		isInFastActionEditPage();
		verifyFastActionTabEnabled();
		
		//insert Alpha numeric characters in Text field
		sendKeys("textField_Action_Title", ALPHA_NUMERIC_TEXT);
		insertIntoAllTinyMCEinPage(ALPHA_NUMERIC_TEXT);
		sendKeys("textField_Action_OriginatingDetails",
				ALPHA_NUMERIC_TEXT);
		clickAndWait("button_Action_Save");
		explicitlyWaitForElement("successMsg_Action_Update",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Update");
		//verifying data in View action page
		verifyText(ALPHA_NUMERIC_TEXT, "label_Action_View_Title");
		verifyText(ALPHA_NUMERIC_TEXT,"label_Action_FastAction_View_OriginatingDetails");
		//navigate to Raise Fast Action URL
		
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		sendKeys("textField_Action_ViewUrn", fastActionWithSpecialCharsURN);
		clickAndWaitForPageLoad("button_Action_View");
		isInFastActionViewPage();
		
		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		//verify is in Edit Fast action page
		isInFastActionEditPage();
		verifyFastActionTabEnabled();
		
		//Create officer validation
		click("button_Action_FastAction_CreateOfficer");
		explicitlyWaitForElement("textfield_Action_CreateOfficer_Surname", getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname", ALPHA_NUMERIC_TEXT);
		sendKeys("textfield_Action_CreateOfficer_Forename",ALPHA_NUMERIC_TEXT);
		click("button_Action_FastAction_Edit_CreateOfficeDialog_Create");
		
		verifyText(CREATE_OFFICER_ALPHANUMERIC_VALIDATION_MESS,"errorMsg_Action_CreateOfficer_SurnameAndForname");
		click("icon_Action_Create_CreateOfficer_Dialog_Close");

		//inserting special characters in text
		sendKeys("textField_Action_Title", SPECIALCHARACTER_TEXT);
		insertIntoAllTinyMCEinPage(SPECIALCHARACTER_TEXT);
		sendKeys("textField_Action_OriginatingDetails",
				SPECIALCHARACTER_TEXT);
		clickAndWait("button_Action_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		//verifying special characters in view action page
		verifyText(SPECIALCHARACTER_TEXT, "label_Action_View_Title");
		verifyText(SPECIALCHARACTER_TEXT,"label_Action_FastAction_View_OriginatingDetails");
		
		
	
		endLine(method);
	}

	//Test case 24556:007. Raise Fast Action - Text and Title field
	@Test
	//(priority = 17)
	public void raiseFastAction_TextAndTitle(Method method) throws Exception {
		startLine(method);
		//navigate to Raise Fast Action URL
		navigateTo(RAISE_FAST_ACTION_URL);
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		sendKeys("textField_Action_Title", ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE);
		//TODO SHALINI- Rexecute and check - This verification is not possible run time, as of now needs to investigate
		//verify Text field text is not same as Title 
		String currentTextFieldValue = getTextFieldText();
		Report.info("Current Text field value" + currentTextFieldValue + ":");
		String currentTitleText = getText("textField_Action_Title");
		Report.info("Current Title value:" + currentTitleText + ":");
//		verifyTextOnlyNotEquals(currentTitleText, currentTitleText);
		String text= "";
		verifyTextCompare(text, getTextFieldText()); 
		//click Raise button
		clickAndWait("button_Action_Raise");
		// Verification of View Action Page
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		//validate Fast Action Success Message
		validateFastActionSuccMess();
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		fastActionURN_textAndTitle = actUrn[2];
		// Validate inserted title in create page same as in view page
		verifyText( ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE ,
				"label_Action_View_Title");
		//Validate Text
		verifyTextNotEquals(getTextFieldText(), "label_Action_View_Title");
		verifyTextCompare(text, getTextFieldText()); 
		endLine(method);

	}


	// Test case 29317:007a. Update Fast Action - Text and Title field priority=18, 
	@Test
	//(dependsOnMethods = {"raiseFastAction_TextAndTitle"})
	public void updateFastAction_TextAndTitle(Method method) throws Exception {
		startLine(method);
		//navigate to view action page
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		
		//TODO SHALINI- Reexecute and check
		sendKeys("textField_Action_ViewUrn", fastActionURN_textAndTitle);
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		isInFastActionViewPage();
		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		//verify is in Edit Fast action page
		isInFastActionEditPage();
		// Verify  Previous title  field value is retained on all update field- Title
		verifyText(ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE, "textField_Action_Title");
		verifyTextNotEquals(getTextFieldText(), "textField_Action_Title");
		String text= "";
		verifyTextCompare(text, getTextFieldText()); 
		//click Save button
		clickAndWait("button_Action_Save");
		// Verification of View Action Page
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		//validate Fast Action Success Message
		//validateFastActionSuccMess();
		// Validate inserted title in update page same as in view page
		verifyText( ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE ,
				"label_Action_View_Title");
		//Validate Text
		verifyTextNotEquals(getTextFieldText(), "label_Action_View_Title");
		verifyTextCompare(text, getTextFieldText()); 
		endLine(method);

	}
	
	// Test case 26766 : 009. Raise Fast Action from Nominal Entity / Entity Record�
	@Test
	//(priority = 19)
	public void raisefastactionfromNominalEntity(Method method) throws Exception
	{
		startLine(method);
		explicitlyWaitForElement("link_Indexes",getTotalWaitTimeInSecs());
		//click on nominal link
		navigateTo(NOMINAL_URL);
		explicitlyWaitForElement("button_Indexes_Search",getTotalWaitTimeInSecs());
		//click on search button
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("icon_Indexes_SearchResult_CreateNew",getTotalWaitTimeInSecs());
		//click on create new entity icon in search result tab
		clickAndWait("icon_Indexes_SearchResult_CreateNew");
		explicitlyWaitForElement("textField_Indexes_Nominal_Surname",getTotalWaitTimeInSecs());
		//enter surname in nominal create page
		sendKeys("textField_Indexes_Nominal_Surname", Nominal_Surname);
		//select sex from drop down list
		selectByVisibleText("dropDown_Indexes_Create_Sex", NOMINAL_SEX);
		//click on create nominal
		clickAndWait("button_Indexes_Create");
		//verify success message is displayed once nominal is created
		explicitlyWaitForElement("successMsg_Indexes_create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Indexes_create");

		explicitlyWaitForElement("button_Indexes_View_RaiseFastAction",getTotalWaitTimeInSecs());
		//click on raise fast action button 
		//wait for raise fast action pop up to be displayed
		clickAndWaitForBrowserPopup(FAST_ACTION_TITLE_WINDOW, "button_Indexes_View_RaiseFastAction");
		explicitlyWaitForElement("button_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Create");

		raisefastActionNominal();
		//close the pop up window of raise fast action and navigate to parent page

		driver = closePopupWindowAndNavigateToParentWindow(FAST_ACTION_TITLE_WINDOW, NOMINAL_TITLE_WINDOW);

		endLine(method);
	}

	// Test case 29508:010. Similar Fast Actions priority=20, 
	@Test
	//(dependsOnMethods = {"raiseFastAction_withAllFieldDetails"})
	public void similarFastActions_withAllFieldDetails(Method method)throws Exception{
		startLine(method);
		//navigate to view action page
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		if(fastActionURN_allFieldValues == null){
			fastActionURN_allFieldValues = fastActionURN;
		}
		explicitlyWaitForElement("textField_Action_ViewUrn", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_ViewUrn",fastActionURN_allFieldValues);
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		isInFastActionViewPage();
		verifyElementPresent("button_Action_Similar");
		//click Similar button
		clickAndWait("button_Action_Similar");
		//verify is in similar/create - Fast action page
		isInFastActionSimilarPage();
		verifyFastActionTabEnabled();
		// Verify all mandatory field list with '*'
		verifyMandatoryFieldsInFastAction();

		// Verify All Previous values are retained on all mandatory fields on  click ofSimilar
		verifyTextContains(incidentIdFull, "dropDown_Action_Create_Incident");
		verifyText( "Updated Title for Action" ,
				"textField_Action_Title");
		String docContent = readTextFile(System.getProperty("user.dir")+"\\hugeDataForTextField.txt");
		verifyTextCompare(docContent, getTextFieldText());
		// Validate Protective Marking : Restricted is set as default
		verifyCurrentSelectByText("dropDown_Action_Create_ProtectiveMarking",PROTECTIVE_MARKING_SECRET);
		// Validate  Priority : Low is set as default value
		verifyCurrentSelectByText("dropDown_Action_Mirsap_Priority",PRIORITY_HIGH);
		verifyText(ALLOCATION ,"label_Action_Create_State");
		verifyText(CREATE_OFFICER_SURNAME+" "+CREATE_OFFICER_FORENAME,"textField_Action_CreateOfficer");
		//ToDo - Verify Themes
//		verifyText(actionThemeOne,getText("table_Action_ThemeItem").trim());
		// As this checkbox is unchecked by UpdateFastActionWithAllFields
		/*isCheckBoxChecked("checkBox_Action_Themes");*/

		//To Verify Grid Section Items
		verifyText(linkedActionsRecordURN, "table_LinkedAction_Createpage_URN");
		verifyText(linkedDocumentsRecordURN,"table_AssociatedDocuments_Createpage_URN");

		//Verify no Disclosure Record
		verifyElementPresent("section_Action_Create_Disclosure");

		//update title field by appending Similar to all field values 
		sendKeys("textField_Action_Title",ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE+UPDATE_SIMILAR_ACTION);
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_RAISE_FAST_ACTION+UPDATE_SIMILAR_ACTION);
		//change PM
		selectByVisibleText("dropDown_Action_Create_ProtectiveMarking",PROTECTIVE_MARKING_SECRET);
		//change priority
		selectByVisibleText("dropDown_Action_Mirsap_Priority", PRIORITY_HIGH );
		sendKeys("textField_Action_OriginatingDetails",ACTION_ORIGINATING_DETAIL+UPDATE_SIMILAR_ACTION);

		//link_Action_Create_Theme
		click("checkBox_Action_Themes_Item2");


		//Edit Linked Action URN
		click("checkBox_Action_LinkedAction_FirstRecord");
		clickAndWait("icon_Action_LinkedActions_Edit");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Action_URN",linkedActionsRecordURN2);
		addFastActionLinkedActionFrmSelectRecord();
		explicitlyWaitForElement("checkBox_Action_LinkedAction_FirstRecord", getTotalWaitTimeInSecs());
		click("checkBox_Action_LinkedAction_FirstRecord");
		verifyText(linkedActionsRecordURN2, "table_Action_Create_LinkedAction_FirstRecordURN");

		//Edit Associated Document URN
		explicitlyWaitForElement("checkBox_Action_LinkedDocument_FirstRecord", getTotalWaitTimeInSecs());
		click("checkBox_Action_LinkedDocument_FirstRecord");
		clickAndWait("icon_Action_AssociatedDocument_Edit");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN2);
		addFastActionAssociatedDocumentFrmSelectRecord("(//*[@id='" + incidentIdFull + "'])[2]");
		explicitlyWaitForElement("checkBox_Action_LinkedDocument_FirstRecord", getTotalWaitTimeInSecs());
		click("checkBox_Action_LinkedDocument_FirstRecord");
		verifyText(linkedDocumentsRecordURN2, "table_Action_Create_AssociatedDocument_FirstRecordURN");

		// Raise Similar Fast Action by click on Create button
		explicitlyWaitForElement("button_Action_Raise", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Raise");
		// Verification of View Action Page
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		//validate Fast Action Success Message
		validateFastActionSuccMess();
		verifyElementPresent("successMsg_Action_Create");
		//Get the fastActionURN 
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		fastActionURN_allFieldValues_similar = actUrn[2];
		//Verify all field values
		verifyText( ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE +UPDATE_SIMILAR_ACTION ,
				"label_Action_View_Title");
		//ToDo Shalini
		//verifyText(TINY_MCE_TEXT_FOR_RAISE_FAST_ACTION, "textField_Action_Fast_Text");
		System.out.println("text from tinymce in view page" + getTextFieldText());
		verifyTextCompare(TINY_MCE_TEXT_FOR_RAISE_FAST_ACTION+UPDATE_SIMILAR_ACTION, getTextFieldText());

		// Validate Protective Marking : Updated to Secret
		verifyText(PROTECTIVE_MARKING_SECRET, "label_Action_Mirsap_View_ProtectedMarking");
		// Validate  Priority : Updated to High
		verifyText(PRIORITY_HIGH, "label_Action_Mirsap_View_Priority");
		verifyText(ALLOCATED ,"label_Action_View_State");
		verifyText(CREATE_OFFICER_SURNAME+" "+CREATE_OFFICER_FORENAME,"textField_Action_view_CreateOfficer");
		verifyText(ACTION_ORIGINATING_DETAIL+UPDATE_SIMILAR_ACTION,
				"label_Action_FastAction_View_OriginatingDetails");

		//ToDo - Verify Themes
		verifyText(actionThemeTwo,"table_Action_ThemeItem");

		//To Verify Grid Section Items
		verifyText(linkedActionsRecordURN2, "table_LinkedAction_URN");
		verifyText(linkedDocumentsRecordURN2,"table_AssociatedDocuments_URN");

		//Verify Disclosure Record
		click("link_Action_View_Disclosure");
		verifyElementPresent("table_Action_View_Diclosure_URN");

		//verify Association Tab Records
		/*click("tab_Association");
		verifyAssociationTabEnabled();*/
		clickAssociationTabAndVerifyIsInAssociationTab();
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		verifyElementPresent("table_Action_AssociationTab_LinkAction");
		verifyElementPresent("table_Action_AssociationTab_Associated");
		verifyElementPresent("table_Action_AssociationTab_Disclosure");
		// 
		//Prints the ending of current Method name as  - ------------createFastAction_withAllFieldDetails Test Ending------------ 
		endLine(method);	
	}	

	
	/* Test case 28546:012. Raise Fast Action - Add/Edit/Delete Icon buttons in Linked Actions Grid
	 */
//	//@Test(priority = 21)
 	@Test
 	//(priority = 21)
	public void raiseFastActionLinkedActionsGridIcons(Method method) throws Exception{
		startLine(method);
		//navigate to raise fast action
		navigateTo(RAISE_FAST_ACTION_URL);
		verifyFastActionTabEnabled();
		//click on add linked action icon
		waitForJQuery(driver);
		//Add invalid and then valid action URN
		explicitlyWaitForElement("icon_Action_LinkedActions_Create", getTotalWaitTimeInSecs());
		WebElement addIconLinkedAction = driver.findElement((map.getLocator("icon_Action_LinkedActions_Create")));
		clickElementUsingJS(addIconLinkedAction);
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		//add invalid URN
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN","AAA1");
		//clicking and returning in the reference dropdown selects the incident
		click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		pressReturnKey("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		
		clickAndWait("button_Action_Create_LinkedAssoc_Action_Add");
		verifyText("AAA1" + " does not exist", "label_error_does_not_exist");
		//add valid urn - change to from Excel
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN",linkedActionsRecordURN);
		click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		pressReturnKey("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");		
		clickAndWait("button_Action_Create_LinkedAssoc_Action_Add");
		
		//add linked action via default list
		/*clickAndWait("button_Action_Create_LinkedAssoc_Action");*/
		//select action from the default list
		/*sendKeys("dropDown_Action_Select_DefaultList", "A5");
		pressReturnKey("dropDown_Action_Select_DefaultList");
		clickAndWait("button_Action_Create_LinkedAssoc_Action_Add");*/

		//select top linked action and edit it
		//click in check box
		checkCheckBox("checkBox_Action_LinkedAction_FirstRecord");
		//click edit button
		explicitlyWaitForElement("icon_Action_LinkedActions_Edit", getTotalWaitTimeInSecs());
		WebElement editIconLinkedAction = driver.findElement((map.getLocator("icon_Action_LinkedActions_Edit")));
		clickElementUsingJS(editIconLinkedAction);
		/*click("icon_Action_LinkedActions_Edit");*/
		//change the action to a different action
		explicitlyWaitForElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN",linkedActionsRecordURN2);
		/*click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		pressReturnKey("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		clickAndWait("button_Action_Create_LinkedAssoc_Action_Add");*/
		addFastActionLinkedActionFrmSelectRecord();
		
		//select top linked action and delete it
		//record already selected so no need to click in it
		//click delete
		explicitlyWaitForElement("checkBox_Action_LinkedAction_FirstRecord", getTotalWaitTimeInSecs());
		checkCheckBox("checkBox_Action_LinkedAction_FirstRecord");
		explicitlyWaitAndGetWebElements("icon_Action_LinkedAssoc_Delete", getTotalWaitTimeInSecs());
//		click("icon_Action_LinkedAssoc_Delete");
		WebElement deleteIconLinkedAction = explicitlyWaitAndGetWebElement("icon_Action_LinkedAssoc_Delete", getTotalWaitTimeInSecs());
		clickElementUsingJS(deleteIconLinkedAction);
		
		//ideally would like to add more verification points
		endLine(method);
	}
	
	/* Test case 26132:013. Raise Fast Action -ADD/Edit/Delete icon buttons in Associated Document Grid Section
	 */
	// TODO : Default list checking
	@Test
	//(priority = 22)
	public void raiseFastActionAssociatedDocumentGridSection(Method method) throws Exception{
		startLine(method);

		navigateTo(RAISE_FAST_ACTION_URL);
		// verifies Fast Action Tab is enabled
		verifyFastActionTabEnabled();

		// Wait for Create button on Action create page for 60secs
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		// Confirm Create Button on Action Create Page is present
		verifyElementPresent("button_Action_Raise");
		verifyFastActionFieldsInCreatePage();

		//Pass data to title field
		sendKeys("textField_Action_Title",  ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE );

		// validate		"Please select/input an entity" alert
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN"," ");
		click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1");
 		sleep(2000);
 		String xpathForCurrentIncident = "//div[contains(@id,'"+incidentIdFull+"')]";
 		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
 				getTotalWaitTimeInSecs());
 		WebElement element = webDriverWaitForTask.until(ExpectedConditions
					.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
			element.click();
		explicitlyWaitForElement("button_Action_Create_LinkedAssoc_Documents_Add", getTotalWaitTimeInSecs());
		click("button_Action_Create_LinkedAssoc_Documents_Add");
		waitForAlertTextAndClose("Please select/input an entity");

		
		
		//invalid URN validation
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",ASSOCIATED_DOCUMENT_INVALID_URN);
		addFastActionAssociatedDocumentFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");
		verifyTextContains(IS_NOT_A_VALID_URN_TEXT, "errorMsg_Action_AssociateDocument_InvalidURN");

		// Not a valid fast action
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",polURN);
		click("textField_Action_Create_LinkedAssoc_Document_URN");
		addFastActionAssociatedDocumentFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");
		
		explicitlyWaitForElement("button_Action_Raise", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("errorMsg_Action_AssociateDocument_Invalid_FastAction", getTotalWaitTimeInSecs());
		verifyTextContains(NOT_A_VALID_FAST_ACTION_TEXT, "errorMsg_Action_AssociateDocument_Invalid_FastAction");
		click("checkBox_Action_LinkedDocument_FirstRecord");
		click("icon_Action_LinkedDocument_Delete");

		// read only Action
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		//invalid URN validation
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",polActionURN);
		addFastActionAssociatedDocumentFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");
		explicitlyWaitForElement("button_Action_Raise", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("errorMsg_Action_AssociateDocument_MarkedAsReadOnly", getTotalWaitTimeInSecs());
		verifyTextContains(IS_MARKED_AS_READ_ONLY_TEXT, "errorMsg_Action_AssociateDocument_MarkedAsReadOnly");
		click("checkBox_Action_LinkedDocument_FirstRecord");
		click("icon_Action_LinkedDocument_Delete");

		//Adding Linked Document URN
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		// Verify Dialog box title is poped up
		verifyText(ACTION_ASSOCIATEDOCUMENT_DIALOGBOXTITLE, "text_Action_AssociateDocument_DialogboxTitle");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN);
		//		String actualCurrentIncidentVal = getWebElement("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1").getText();
		verifyTextContains(incidentIdFull, "dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1");
		addFastActionAssociatedDocumentFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");
		explicitlyWaitForElement("table_Action_Create_AssociatedDocument_FirstRecordURN", getTotalWaitTimeInSecs());
		verifyText(linkedDocumentsRecordURN, "table_Action_Create_AssociatedDocument_FirstRecordURN");

		//Adding multiple Linked Document URN
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN2);
		addFastActionAssociatedDocumentFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");
		verifyText(linkedDocumentsRecordURN2, "table_Action_Create_AssociatedDocument_SecondRecordURN");

		//Edit Associated Document URN
		explicitlyWaitForElement("checkBox_Action_LinkedAction_SecondRecordRecord", getTotalWaitTimeInSecs());
		checkCheckBox("checkBox_Action_LinkedAction_SecondRecordRecord");
		clickAndWait("icon_Action_AssociatedDocument_Edit");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN);
		addFastActionAssociatedDocumentFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");
		click("checkBox_Action_LinkedAction_SecondRecordRecord");
		verifyText(linkedDocumentsRecordURN, "table_Action_Create_AssociatedDocument_FirstRecordURN");

		//Delete Associated Document URN
		click("checkBox_Action_LinkedAction_SecondRecordRecord");
		clickAndWait("icon_Action_AssocDocument_Delete");

		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		// Verify Associated Document URN in View page
		isInFastActionViewPage();
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		fastActionDocumentGridURN = actUrn[2];
		verifyText(linkedDocumentsRecordURN, "table_Action_View_AssociatedDocument_FirstRecordURN");


		// Verify Associated Document URN in Association Tab
		click("tab_Association");
		verifyAssociationTabEnabled();
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + linkedDocumentsRecordURN);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");

		 
		endLine(method);
	}

	
	/*
	 * Test case 28559: 014. Update Fast Action -Add/Edit/Delete icon buttons in  Linked Actions  Grid Section priority = 23,
	 */
	@Test
	//(priority=3)
	//( dependsOnMethods = {"createFastAction_withMandatoryFieldDetails"})
	public void updateFastActionAssociatedActionGridSection(Method method) throws Exception{
		startLine(method);
		//navigate to view action page
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
//		sendKeys("textField_Action_ViewUrn", "A141");
		sendKeys("textField_Action_ViewUrn", fastActionURN);
		
		clickAndWaitForPageLoad("button_Action_View");
		isInFastActionViewPage();

		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		//verify is in Edit Fast action page
		isInFastActionEditPage();
		verifyFastActionTabEnabled();

		// validate	"Please select/input an entity" alert
		click("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		pressReturnKey("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		click("button_Action_Create_LinkedAssoc_Action_Update");
		waitForAlertTextAndClose("Please select/input an entity");

		//invalid URN validation
		explicitlyWaitForElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Action_URN",ASSOCIATED_ACTION_INVALID_URN);
		click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		pressReturnKey("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		/*click("button_Action_Create_LinkedAssoc_Action_Update");*/
		updateFastActionLinkedActionFrmSelectRecord();
		explicitlyWaitForElement("errorMsg_Action_AssociateDocument_edit_InvalidURN1", getTotalWaitTimeInSecs());
		verifyTextContains(IS_NOT_A_VALID_URN_TEXT, "errorMsg_Action_AssociateDocument_edit_InvalidURN1");

		// Not a valid fast action
		sendKeys("textField_Action_Create_LinkedAssoc_Action_URN", polURN);
		click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		pressReturnKey("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		clickAndWait("button_Action_Create_LinkedAssoc_Action_Update");

		//click Save button
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Save");
		explicitlyWaitForElement("errorMsg_Action_AssociateAction_Invalid_FastAction", getTotalWaitTimeInSecs());
		verifyTextContains(NOT_A_VALID_FAST_ACTION_TEXT, "errorMsg_Action_AssociateAction_Invalid_FastAction");
		checkCheckBox("checkBox_Action_LinkedAction_Update_FirstRecord");
		click("icon_Action_LinkedAssoc_Delete");

		// read only Action
		click("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		//invalid URN validation
		sendKeys("textField_Action_Create_LinkedAssoc_Action_URN", polActionURN);
		click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		pressReturnKey("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		click("button_Action_Create_LinkedAssoc_Action_Update");
		
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Save");
		explicitlyWaitForElement("errorMsg_Action_AssociateAction_MarkedAsReadOnly", getTotalWaitTimeInSecs());
		verifyTextContains(IS_MARKED_AS_READ_ONLY_TEXT, "errorMsg_Action_AssociateAction_MarkedAsReadOnly");
		checkCheckBox("checkBox_Action_LinkedAction_Update_FirstRecord");
		click("icon_Action_LinkedAssoc_Delete");

		//enter valid URN
		click("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Action_URN", linkedActionsRecordURN);
		/*click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		pressReturnKey("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		click("button_Action_Create_LinkedAssoc_Action_Update");*/
		updateFastActionLinkedActionFrmSelectRecord();


		//Edit Associated Action URN
		//TODO - Test case fails here. The application does not think it has selected an entity- CORRECTED - WORKING
		explicitlyWaitForElement("checkBox_Action_LinkedAction_Update_FirstRecord", getTotalWaitTimeInSecs());
		WebElement linkedActionFirstRecord = explicitlyWaitAndGetWebElement("checkBox_Action_LinkedAction_Update_FirstRecord", getTotalWaitTimeInSecs());
		clickElementUsingJS(linkedActionFirstRecord);
		sleep(4000);
		/*checkCheckBox("checkBox_Action_LinkedAction_Update_FirstRecord");*/
		WebElement iconLinkedActionEdit = explicitlyWaitAndGetWebElement("icon_Action_LinkedActions_Edit", getTotalWaitTimeInSecs());
		clickElementUsingJS(iconLinkedActionEdit);
		/*click("icon_Action_AssociatedDocument_Edit");*/

		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Action_URN",linkedActionsRecordURN2);
		click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		pressReturnKey("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
		click("button_Action_Create_LinkedAssoc_Action_Update");
		verifyText(linkedActionsRecordURN2, "table_Action_Update_LinkedAction_FirstRecordURN");

		//TODO- Default List enter valid URN from default list
		/*click("button_Action_Create_LinkedAssoc_Action");
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		//select action from the default list
		sendKeys("dropDown_Action_Select_DefaultList", "A5");
		pressReturnKey("dropDown_Action_Select_DefaultList");
		click("button_Action_Create_LinkedAssoc_Action_Update");*/

		clickAndWait("button_Action_Save");	

		// Verify Associated Document URN in View page
		isInFastActionViewPage();

		verifyText(linkedActionsRecordURN2, "table_Action_View_LinkedActions_FirstRecordURN");

		// Verify Associated Document URN in Association Tab
		click("tab_Association");
		verifyAssociationTabEnabled();
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + linkedActionsRecordURN2);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");

		 
		endLine(method);
	}

	
	/* Test case 28560:015. Update Fast Action -ADD/ Edit / Delete icon buttons in Associated Documents Grid Section */
	// TODO : Default list checking priority = 24, 
	@Test
	//(dependsOnMethods = {"raiseFastActionAssociatedDocumentGridSection"})
	public void updateFastActionAssociatedDocumentGridSection(Method method) throws Exception{
		startLine(method);

		//navigate to view action page
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		sendKeys("textField_Action_ViewUrn", fastActionDocumentGridURN);
		/*sendKeys("textField_Action_ViewUrn", "A3");*/
		clickAndWaitForPageLoad("button_Action_View");
		
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		isInFastActionViewPage();
		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		//verify is in Edit Fast action page
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		isInFastActionEditPage();
		verifyFastActionTabEnabled();

		// validate		"Please select/input an entity" alert
		click("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN"," ");
		click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1");
 		sleep(2000);
 		String xpathForCurrentIncident = "//div[contains(@id,'"+incidentIdFull+"')]";
 		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
 				getTotalWaitTimeInSecs());
 		WebElement element = webDriverWaitForTask.until(ExpectedConditions
					.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
			element.click();
		explicitlyWaitForElement("button_Action_Create_LinkedAssoc_Documents_Add", getTotalWaitTimeInSecs());
		click("button_Action_Create_LinkedAssoc_Documents_Add");
		waitForAlertTextAndClose("Please select/input an entity");

		//invalid URN validation
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",ASSOCIATED_DOCUMENT_INVALID_URN);
		addFastActionAssociatedDocumentFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");
		verifyTextContains(IS_NOT_A_VALID_URN_TEXT, "errorMsg_Action_AssociateDocument_edit_InvalidURN");
		
		

		// Not a valid fast action
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",polURN);
		click("textField_Action_Create_LinkedAssoc_Document_URN");
		addFastActionAssociatedDocumentFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");

		//click Save button
		explicitlyWaitAndGetWebElementText("button_Action_Save", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Save");
		explicitlyWaitForElement("errorMsg_Action_AssociateDocument_Invalid_FastAction", getTotalWaitTimeInSecs());
		verifyTextContains(NOT_A_VALID_FAST_ACTION_TEXT, "errorMsg_Action_AssociateDocument_Invalid_FastAction");
		click("checkBox_Action_LinkedDocument_Edit_SecondRecord");
		click("icon_Action_LinkedDocument_Delete");

		// read only Action
		click("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		//invalid URN validation
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",polActionURN);
		addFastActionAssociatedDocumentFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");

		clickAndWait("button_Action_Save");	
		explicitlyWaitForElement("errorMsg_Action_AssociateDocument_MarkedAsReadOnly", getTotalWaitTimeInSecs());
		verifyTextContains(IS_MARKED_AS_READ_ONLY_TEXT, "errorMsg_Action_AssociateDocument_MarkedAsReadOnly");
		click("checkBox_Action_LinkedDocument_Edit_SecondRecord");
		click("icon_Action_LinkedDocument_Delete");

		//Delete Associated Document URN
		click("checkBox_Action_LinkedDocument_Edit_FirstRecord");
		click("icon_Action_AssocDocument_Delete");

		//Adding Linked Document URN
		click("button_Action_Create_LinkedAssoc_Documents");
		// Verify Dialog box title is poped up
		verifyText(ACTION_ASSOCIATEDOCUMENT_DIALOGBOXTITLE, "text_Action_AssociateDocument_EditDialogboxTitle");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN);
		//		String actualCurrentIncidentVal = getWebElement("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1").getText();
		verifyTextContains(incidentIdFull, "dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1");
		addFastActionAssociatedDocumentFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");
		explicitlyWaitForElement("table_Action_Create_AssociatedDocument_Edit_FirstRecordURN", getTotalWaitTimeInSecs());
		verifyText(linkedDocumentsRecordURN, "table_Action_Create_AssociatedDocument_Edit_FirstRecordURN");

		//Adding multiple Linked Document URN
		click("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN2);
		addFastActionAssociatedDocumentFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");
		verifyText(linkedDocumentsRecordURN2, "table_Action_Create_AssociatedDocument_Edit_SecondRecordURN");

		//Edit Associated Document URN
		click("checkBox_Action_LinkedDocument_Edit_SecondRecord");
		click("icon_Action_AssociatedDocument_Edit");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN);
		addFastActionAssociatedDocumentFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");
		click("checkBox_Action_LinkedDocument_Edit_SecondRecord");
		verifyText(linkedDocumentsRecordURN, "table_Action_Create_AssociatedDocument_Edit_FirstRecordURN");
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Save");	

		// Verify Associated Document URN in View page
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		isInFastActionViewPage();

		verifyText(linkedDocumentsRecordURN, "table_Action_View_AssociatedDocument_FirstRecordURN");

		// Verify Associated Document URN in Association Tab
		click("tab_Association");
		verifyAssociationTabEnabled();
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + linkedDocumentsRecordURN);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");

		 
		endLine(method);
	}

	/* Test case 28562:015a. Update Fast Action -ADD/Edit/Delete icon buttons in Documents Taken Grid Section priority=25, */
	@Test
	//(dependsOnMethods = {"createFastAction_withMandatoryFieldDetails"})
	public void updateFastActionDocumentsTakenGridSection(Method method) throws Exception {
		startLine(method);
		//navigate to view action page
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		sendKeys("textField_Action_ViewUrn", fastActionURN);
		//		sendKeys("textField_Action_ViewUrn", "A25");
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		isInFastActionViewPage();
		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		//verify is in Edit Fast action page
		isInFastActionEditPage();

		// validate		"Please select/input an entity" alert
		click("icon_Action_Edit_DocumentTaken_Add");
		explicitlyWaitAndGetWebElement("textField_Action_Update_DocumentTaken_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN"," ");
		click("icon_Action_FastAction_DocumentTaken_Incident_Arrow");
 		sleep(2000);
 		String xpathForCurrentIncident = "//div[contains(@id,'"+incidentIdFull+"')]";
 		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
 				getTotalWaitTimeInSecs());
 		WebElement element = webDriverWaitForTask.until(ExpectedConditions
					.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
			element.click();
		explicitlyWaitForElement("button_Action_Update_DocumentTaken_Documents_Add", getTotalWaitTimeInSecs());
		click("button_Action_Update_DocumentTaken_Documents_Add");
		waitForAlertTextAndClose("Please select/input an entity");

		//invalid URN validation
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",ASSOCIATED_DOCUMENT_INVALID_URN);
		addFastActionDocumentTakenFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");
		verifyTextContains(IS_NOT_A_VALID_URN_TEXT, "errorMsg_Action_DocumentTaken_edit_InvalidURN");

		// Not a valid fast action
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",polURN);
		click("textField_Action_Update_DocumentTaken_Document_URN");
		addFastActionDocumentTakenFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");

		//click Save button
		waitForJQuery(driver);
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Save");		
		explicitlyWaitForElement("errorMsg_Action_DocumentTaken_Invalid_FastAction", getTotalWaitTimeInSecs());
		verifyTextContains(NOT_A_VALID_FAST_ACTION_TEXT, "errorMsg_Action_DocumentTaken_Invalid_FastAction");
		click("checkBox_Action_DocumentTaken_Edit_FirstRecord");
		click("icon_Action_Edit_DocumentTaken_Delete");

		// read only Action
		click("icon_Action_Edit_DocumentTaken_Add");
		explicitlyWaitForElement("textField_Action_Update_DocumentTaken_Document_URN", getTotalWaitTimeInSecs());
		//invalid URN validation
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",polActionURN);
		addFastActionDocumentTakenFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");
		waitForJQuery(driver);
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Save");	
		explicitlyWaitForElement("errorMsg_Action_DocumentTaken_MarkedAsReadOnly", getTotalWaitTimeInSecs());
		verifyTextContains(IS_MARKED_AS_READ_ONLY_TEXT, "errorMsg_Action_DocumentTaken_MarkedAsReadOnly");
		/*click("checkBox_Action_DocumentTaken_Edit_SecondRecord");
		click("icon_Action_Edit_DocumentTaken_Delete");*/

		//Delete Associated Document URN
		click("checkBox_Action_DocumentTaken_Edit_FirstRecord");
		click("icon_Action_Edit_DocumentTaken_Delete");

		//Adding Linked Document URN
		click("icon_Action_Edit_DocumentTaken_Add");
		// Verify Dialog box title is poped up
		verifyText(ACTION_DOCUMENTTAKEN_DIALOGBOXTITLE, "text_Action_DocumentTaken_EditDialogboxTitle");
		explicitlyWaitForElement("textField_Action_Update_DocumentTaken_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",linkedDocumentsRecordURN);
		//				String actualCurrentIncidentVal = getWebElement("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1").getText();
		addFastActionDocumentTakenFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");
		explicitlyWaitForElement("table_Action_Create_DocumentTaken_Edit_FirstRecordURN", getTotalWaitTimeInSecs());
		verifyText(linkedDocumentsRecordURN, "table_Action_Create_DocumentTaken_Edit_FirstRecordURN");

		//Adding multiple Linked Document URN
		click("icon_Action_Edit_DocumentTaken_Add");
		explicitlyWaitForElement("textField_Action_Update_DocumentTaken_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",linkedDocumentsRecordURN2);
		addFastActionDocumentTakenFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");
		verifyText(linkedDocumentsRecordURN2, "table_Action_Create_DocumentTaken_Edit_SecondRecordURN");

		//Edit Associated Document URN
		click("checkBox_Action_DocumentTaken_Edit_SecondRecord");
		click("icon_Action_DocumentTaken_Edit");
		explicitlyWaitForElement("textField_Action_Update_DocumentTaken_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",linkedDocumentsRecordURN);
		addFastActionDocumentTakenFrmSelectRecord("//div[contains(@id,'"+incidentIdFull+"')]");
		click("table_Action_Create_DocumentTaken_Edit_SecondRecordURN");
		verifyText(linkedDocumentsRecordURN, "table_Action_Create_DocumentTaken_Edit_FirstRecordURN");

		clickAndWait("button_Action_Save");	
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		// Verify Associated Document URN in View page
		
		isInFastActionViewPage();
		explicitlyWaitForElement("button_Action_Update",
				getTotalWaitTimeInSecs());
		// verify Association Tab Records
		explicitlyWaitForElement("tab_Association",
				getPoolWaitTime());
		clickAndWait("tab_Association");
		waitForPageLoad();
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getTotalWaitTimeInSecs());
		verifyAssociationTabEnabled();
		
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + linkedDocumentsRecordURN);
		waitForJQuery(driver);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");

		 
		endLine(method);
	}
	
	/* Test case 23334:016. Cancel Convert	 */
	@Test
	//(priority = 26)
	public void convertFastActionCancel(Method method) throws Exception {
		startLine(method);
		String fastact_Urn= raiseAndGetFastActionURN();
		// navigate to view action page
		navigateTo(VIEWACTION_URL);
		// verify View Action Tab
		verifyElementPresent("tab_View_Action");
		// verify cursor focus in URN text field
		//verifyCurrentCursorFocus("textField_Action_ViewUrn");
		explicitlyWaitForElement("textField_Action_ViewUrn",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Action_ViewUrn", fastact_Urn);// fastActionURN
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title",
				getTotalWaitTimeInSecs());
		isInFastActionViewPage();
		// Clicking convert action button
		click("button_Action_FastAction_Convert");
		// Entering mandatory fields in convert action page
		explicitlyWaitForElement("dropDown_Action_Mirsap_Force",
				getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");
		// clicking X button for close
		click("link_Action_ConvertFast_Close");
		// verifying View Action page
		isInFastActionViewPage();
		// Clicking convert action button
		click("button_Action_FastAction_Convert");
		// clicking Cancel conver button
		click("button_Action_FastAction_CancelConvert");
		// verifying View Action page
		isInFastActionViewPage();
		// Clicking convert action button
		click("button_Action_FastAction_Convert");
		explicitlyWaitForElement("textField_Action_OriginatingDetails",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		// clicking Cancel conver button
		click("button_Action_FastAction_CancelConvert");
		endLine(method);
	}

	/*Test case 26607:016a. Convert Fast Action without Mandatory field Value*/
 	@Test
 	//(priority = 27)
 	public void convertFastActionWithoutMandatoryDetails(Method method)
 			throws Exception {
 		startLine(method);
 		String fastact_Urn= raiseAndGetFastActionURN();
 		// navigate to view action page
 		navigateTo(VIEWACTION_URL);
 		// verify View Action Tab
 		verifyElementPresent("tab_View_Action");
 		// verify cursor focus in URN text field
 		//verifyCurrentCursorFocus("textField_Action_ViewUrn");
 		explicitlyWaitForElement("textField_Action_ViewUrn",
 				getTotalWaitTimeInSecs());
 		sendKeys("textField_Action_ViewUrn", fastact_Urn);// fastActionURN
 		clickAndWaitForPageLoad("button_Action_View");
 		explicitlyWaitForElement("label_Action_View_Page_Title",
 				getTotalWaitTimeInSecs());
 		isInFastActionViewPage();
 		// Clicking convert action button
 		click("button_Action_FastAction_Convert");
 		explicitlyWaitForElement("button_Action_FastAction_ConvertAction",
 				getTotalWaitTimeInSecs());
 		//clicking conver in conver page
 		click("button_Action_FastAction_ConvertAction");
 		
 		//verify validation error message
 		verifyText(RAISE_ACTION_WITHOUT_MANDATORY_VALUES_VALIDATION_MESS,"errorMsg_Action_Create_WithOutAnyFields");
 		verifyText(CONVERTACTION_WITHOUT_FORCE,"errorMsg_Action_Force");
 		verifyText(CONVERTACTION_WITHOUT_STATION,"errorMsg_Action_Station");
 		verifyText(CONVERTACTION_WITHOUT_CLASS,"errorMsg_Action_Class");
 		verifyText(CONVERTACTION_WITHOUT_SUBJECT,"errorMsg_Action_Subject");
 		endLine(method);

 	}
	
	/* Test case 26613:017. Convert Fast Action with Mandatory field Value TODO
	 * 	 *TODO - Liyakath
	 */
 	@Test
 	//(priority = 28)
	public void convertFastActionWithMandatoryFields(Method method)
			throws Exception {
		startLine(method);
		String fastact_Urn= raiseAndGetFastActionURN();
		// navigate to view action page
		navigateTo(VIEWACTION_URL);
		// verifyIsInViewActionPage();
		// verify View Action Tab
		verifyElementPresent("tab_View_Action");
		// verify cursor focus in URN text field
		//verifyCurrentCursorFocus("textField_Action_ViewUrn");
		sleep(5000);
		explicitlyWaitForElement("textField_Action_ViewUrn",
 				getTotalWaitTimeInSecs());
		sendKeys("textField_Action_ViewUrn", fastact_Urn);// fastActionURN
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title",
				getTotalWaitTimeInSecs());
		isInFastActionViewPage();

		// Clicking convert action button
		click("button_Action_FastAction_Convert");

		// Verification for Convert Popup page header
		verifyElementPresent("label_Action_Covert_ConvertEntity");

		// Verify All Previous values are retained in Convert Action page

		verifyText(ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE,
				"textField_Action_Title");
		verifyTextCompare(TINY_MCE_TEXT_FOR_RAISE_FAST_ACTION,
				getTextFieldText());
		// Entering mandatory fields in convert action page

		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");
		clickAndWait("button_Action_FastAction_ConvertAction");
		explicitlyWaitForElement("label_Action_View_Page_Title",
				getTotalWaitTimeInSecs());
		endLine(method);
	}

     /*Test case 26616:021. URN Sequencing after Convert Fast Action*/
     @Test
     //(priority = 29)
     public void convertFastActionSequenceURN(Method method) throws Exception {
    	 startLine(method);
    	 // getting fast action URN
    	 String fastactURN = raiseAndGetFastActionURN();
    	 // navigate to view action page
    	 navigateTo(VIEWACTION_URL);
    	 // verify View Action Tab
    	 verifyElementPresent("tab_View_Action");
    	 // Enter Fast Action with all fields URN
    	 sendKeys("textField_Action_ViewUrn", fastactURN);
    	 clickAndWaitForPageLoad("button_Action_View");
    	 explicitlyWaitForElement("label_Action_View_Page_Title",
    			 getTotalWaitTimeInSecs());
    	 isInFastActionViewPage();
    	 // Clicking convert action button
    	 click("button_Action_FastAction_Convert");
    	 explicitlyWaitForElement("button_Action_FastAction_ConvertAction",
    			 getTotalWaitTimeInSecs());
    	 // Verification for Convert Popup page header
    	 verifyElementPresent("label_Action_Covert_ConvertEntity");

    	 // Entering mandatory fields in convert action page

    	 selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
    	 selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
    	 selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
    	 selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
    	 sendKeys("textField_Action_OriginatingDetails",
    			 ACTION_ORIGINATING_DETAIL);
    	 click("textField_Action_Mirsap_DefaultList");
    	 click("link_Action_Mirsap_List_CheckAll");
    	 insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_RAISE_FAST_ACTION);
    	 clickAndWait("button_Action_FastAction_ConvertAction");
    	 explicitlyWaitForElement("label_Action_View_Page_Title",
    			 getTotalWaitTimeInSecs());
    	 //verifying same urn  after convert fast action
    	 verifyTextContains(fastactURN, "label_Action_View_Page_Title");
    	 //getting next URN count
    	 String fastactURNcount = fastactURN.substring(1);
    	 int fastactURNcounts = Integer.parseInt(fastactURNcount)+1;
    	 String nextFastActionURn="A"+String.valueOf(fastactURNcounts);
    	 Report.info("nextFastActionURn" + nextFastActionURn);
    	 //getting adding two in existing urn
    	 int fastactURNcounts2 = Integer.parseInt(fastactURNcount)+2;
    	 String nextFastActionURn2="A"+String.valueOf(fastactURNcounts2);
    	 Report.info("nextFastActionURn2" + nextFastActionURn2);
    	 // raise  fast Action
    	 String fastactURN2= raiseAndGetFastActionURN();
    	 Report.info("fastactURN2" + fastactURN2);
    	 // verifying Fast action URN sequences
    	 verifyTextCompare(nextFastActionURn, fastactURN2);
    	 //raise mirsap Action URN
    	 MirsapAction mirsap= new MirsapAction(driver);
    	 String mirsapActURN= mirsap.raiseAndGetMirsapActionURN();
    	 Report.info("mirsapActURN" + mirsapActURN);
    	 // verifying Mirsap ActionURN sequences
    	 verifyTextCompare(mirsapActURN, nextFastActionURn2);


    	 // URN sequence checking after close convert
    	 navigateTo(VIEWACTION_URL);
    	 // verify View Action Tab
    	 verifyElementPresent("tab_View_Action");
    	 sendKeys("textField_Action_ViewUrn", fastactURN2);// fastActionURN
    	 clickAndWaitForPageLoad("button_Action_View");
    	 explicitlyWaitForElement("label_Action_View_Page_Title",
    			 getTotalWaitTimeInSecs());
    	 isInFastActionViewPage();
    	 // Clicking convert action button
    	 click("button_Action_FastAction_Convert");
    	 explicitlyWaitForElement("dropDown_Action_Mirsap_Force",
    			 getTotalWaitTimeInSecs());
    	 // Entering mandatory fields in convert action page
    	 selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
    	 selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
    	 selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
    	 selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
    	 sendKeys("textField_Action_OriginatingDetails",
    			 ACTION_ORIGINATING_DETAIL);
    	 click("textField_Action_Mirsap_DefaultList");
    	 click("link_Action_Mirsap_List_CheckAll");
    	 // clicking X button for close
    	 click("link_Action_ConvertFast_Close");
    	 // verifying View Action page
    	 isInFastActionViewPage();
    	 //raise action after close convert
    	 String fastactURN3= raiseAndGetFastActionURN();
    	 Report.info("fastactURN3" + fastactURN3);
    	 //getting next Fast action URN
    	 String secondfastactURNcount = mirsapActURN.substring(1);
    	 int secondfastactURNcounts = Integer.parseInt(secondfastactURNcount)+1;
    	 String secondFastActionURn="A"+String.valueOf(secondfastactURNcounts);
    	 Report.info("secondFastActionURn" + secondFastActionURn);
    	 // getting adding two in existing urn
    	 int secondfastactURNcounts2 = Integer.parseInt(secondfastactURNcount) + 2;
    	 String secondFastActionURn2 = "A" + String.valueOf(secondfastactURNcounts2);
    	 Report.info("secondFastActionURn2" + secondFastActionURn2);
    	 // verifying Fast action URN sequences after close
    	 verifyTextCompare(fastactURN3, secondFastActionURn);
    	 //raise mirsap Action URN sequences after close
    	 MirsapAction mirsapAc= new MirsapAction(driver);
    	 String mirsapActURNAfterClose= mirsapAc.raiseAndGetMirsapActionURN();
    	 Report.info("mirsapActURNAfterClose" + mirsapActURNAfterClose);
    	 // verifying Mirsap ActionURN sequences
    	 verifyTextCompare(secondFastActionURn2, mirsapActURNAfterClose);

    	 // URN sequence checking after cancel convert
    	 navigateTo(VIEWACTION_URL);
    	 // verify View Action Tab
    	 verifyElementPresent("tab_View_Action");
    	 sendKeys("textField_Action_ViewUrn", fastactURN3);// fastActionURN
    	 clickAndWaitForPageLoad("button_Action_View");
    	 explicitlyWaitForElement("label_Action_View_Page_Title",
    			 getTotalWaitTimeInSecs());
    	 isInFastActionViewPage();
    	 // Clicking convert action button
    	 click("button_Action_FastAction_Convert");
    	 explicitlyWaitForElement("dropDown_Action_Mirsap_Force",
    			 getTotalWaitTimeInSecs());
    	 // Entering mandatory fields in convert action page
    	 selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
    	 selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
    	 selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
    	 selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
    	 sendKeys("textField_Action_OriginatingDetails",
    			 ACTION_ORIGINATING_DETAIL);
    	 click("textField_Action_Mirsap_DefaultList");
    	 click("link_Action_Mirsap_List_CheckAll");
    	 // verifying View Action page
    	 isInFastActionViewPage();
    	 // Clicking convert action button
    	 click("button_Action_FastAction_Convert");
    	 // clicking Cancel convert button
    	 click("button_Action_FastAction_CancelConvert");
    	 // verifying View Action page
    	 isInFastActionViewPage();
    	 //raise action after cancel convert
    	 String fastactURN4= raiseAndGetFastActionURN();
    	 Report.info("fastactURN4" + fastactURN4);
    	 //getting next urn
    	 String thirdfastactURNcount = mirsapActURNAfterClose.substring(1);
    	 int thirdfastactURNcounts = Integer.parseInt(thirdfastactURNcount)+1;
    	 String thirdFastActionURn="A"+String.valueOf(thirdfastactURNcounts);
    	 // getting adding two in existing urn
    	 int thirdfastactURNcounts2 = Integer.parseInt(thirdfastactURNcount) + 2;
    	 String thirdFastActionURn2 = "A"+ String.valueOf(thirdfastactURNcounts2);
    	 Report.info("thirdFastActionURn2" + thirdFastActionURn2);
    	 // verifying Fast action URN sequences after Cancel convert
    	 verifyTextCompare(fastactURN4, thirdFastActionURn);
    	 // raise mirsap Action URN sequences after close
    	 MirsapAction mirsapAction= new MirsapAction(driver);
    	 String mirsapActURNAfterCancel = mirsapAc.raiseAndGetMirsapActionURN();
    	 Report.info("mirsapActURNAfterCancel" + mirsapActURNAfterCancel);
    	 // verifying Mirsap ActionURN sequences
    	 verifyTextCompare(thirdFastActionURn2, mirsapActURNAfterCancel);
    	 endLine(method);
     }
     
     private void raisefastActionNominal() throws Exception {

 		// verifies Fast Action Tab is enabled
 		verifyFastActionTabEnabled();

 		// Wait for Create button on Action create page for 60secs
 		explicitlyWaitForElement("button_Action_Raise",
 				getTotalWaitTimeInSecs());
 		// Confirm Create Button on Action Create Page is present
 		verifyElementPresent("button_Action_Raise");


 		verifyFastActionFieldsInCreatePage();
 		// Verify all mandatory field list with '*'
 		verifyMandatoryFieldsInFastAction();

 		//Pass data only to mandatory fields
 		sendKeys("textField_Action_Title", ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE);

 		clickAndWait("button_Action_Raise");

 		// Verification of View Action Page
 		//validate Fast Action Success Message
 		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
 		verifyElementPresent("successMsg_Action_Create");

 	}
 	
     private void addFastActionLinkedActionFrmSelectRecord() throws InterruptedException{
    	 click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
    	 sleep(2000);
    	 String xpathForCurrentIncident = "//div[@id='" + incidentIdFull + "']";
    	/* WebElement we=driver.findElement(By.xpath(xpathForCurrentIncident));
    	 we.click();*/
  		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
 				getTotalWaitTimeInSecs());
 		WebElement element = null;
    	 try {
  			element = webDriverWaitForTask.until(ExpectedConditions
  					.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
  			element.click();
  			Report.info("Element is clicked");
  		} catch (Exception e) {
//  			e.printStackTrace();
  			Report.fail("Could not able to find the element to click from drop down");
  			Assert.fail("Could not able to find the element to click from drop down");

  		}
    	 
    	 clickAndWait("button_Action_Create_LinkedAssoc_Action_Add");
     }

     private void updateFastActionLinkedActionFrmSelectRecord() throws InterruptedException{
    	 click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
    	 sleep(2000);
    	 String xpathForCurrentIncident = "//div[@id='" + incidentIdFull + "']";
    	/* WebElement we=driver.findElement(By.xpath(xpathForCurrentIncident));
    	 we.click();*/
    	 WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
  				getTotalWaitTimeInSecs());
  		WebElement element = null;
     	 try {
   			element = webDriverWaitForTask.until(ExpectedConditions
   					.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
   			element.click();
   			Report.info("Element is clicked");
   		} catch (Exception e) {
//   			e.printStackTrace();
   			Report.fail("Could not able to find the element to click from drop down");
   			Assert.fail("Could not able to find the element to click from drop down");

   		}
    	 clickAndWait("button_Action_Create_LinkedAssoc_Action_Update");
     }

     private void addFastActionLinkedActionFrmSelectRecordInEditPage() throws InterruptedException{
    	 click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
    	 sleep(2000);
    	 String xpathForCurrentIncident = "//div[@id='" + incidentIdFull + "']";
    	 /*WebElement we=driver.findElement(By.xpath(xpathForCurrentIncident));
    	 we.click();*/
    	 WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
  				getTotalWaitTimeInSecs());
  		WebElement element = null;
     	 try {
   			element = webDriverWaitForTask.until(ExpectedConditions
   					.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
   			element.click();
   			Report.info("Element is clicked");
   		} catch (Exception e) {
//   			e.printStackTrace();
   			Report.fail("Could not able to find the element to click from drop down");
   			Assert.fail("Could not able to find the element to click from drop down");

   		}
    	 clickAndWait("button_Action_Create_LinkedAssoc_Action_Update");
     }
     
     private void addFastActionAssociatedDocumentFrmSelectRecord(String xpathForCurrentIncident)
 			throws InterruptedException {
 		click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1");
 		sleep(2000);
// 		String xpathForCurrentIncident = "//div[contains(@id,'"+incidentIdFull+"')]";
// 		String xpathForCurrentIncident = "//*[@id='" + incidentIdFull + "']";
// 		String xpathForCurrentIncident = "(//div[@id=''" + incidentIdFull + "'])[2]";
// 		String xpathForCurrentIncident = "(//div[contains(div/@id,'" + incidentIdFull + "')])[2]";
 		Report.info("xpathForCurrentIncident" + xpathForCurrentIncident);
 		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
 				getTotalWaitTimeInSecs());
 		WebElement element = null;

 		try {
 			element = webDriverWaitForTask.until(ExpectedConditions
 					.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
 			element.click();
 			Report.info("Element is clicked");
 		} catch (Exception e) {
// 			e.printStackTrace();
 			Report.fail("Could not able to find the element to click from drop down");
 			Assert.fail("Could not able to find the element to click from drop down");

 		}
 /*		
 		
 		WebElement we=driver.findElement(By.xpath(xpathForCurrentIncident));
 		we.click();*/
 		clickAndWait("button_Action_Create_LinkedAssoc_Documents_Add");
 	}
 	
 	private void addFastActionDocumentTakenFrmSelectRecord(String xpathForCurrentIncident)
 			throws InterruptedException {
 		click("icon_Action_FastAction_DocumentTaken_Incident_Arrow");
 		sleep(2000);
// 		String xpathForCurrentIncident = "//div[contains(@id,'"+incidentIdFull+"')]";
// 		String xpathForCurrentIncident = "//*[@id='" + incidentIdFull + "']";
// 		String xpathForCurrentIncident = "(//div[@id=''" + incidentIdFull + "'])[2]";
// 		String xpathForCurrentIncident = "(//div[contains(div/@id,'" + incidentIdFull + "')])[2]";
 		Report.info("xpathForCurrentIncident" + xpathForCurrentIncident);
 		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
 				getTotalWaitTimeInSecs());
 		WebElement element = null;

 		try {
 			element = webDriverWaitForTask.until(ExpectedConditions
 					.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
 			element.click();
 			Report.info("Element is clicked");
 		} catch (Exception e) {
// 			e.printStackTrace();
 			Report.fail("Could not able to find the element to click from drop down");
 			Assert.fail("Could not able to find the element to click from drop down");

 		}
 /*		
 		
 		WebElement we=driver.findElement(By.xpath(xpathForCurrentIncident));
 		we.click();*/
 		explicitlyWaitForElement("button_Action_Update_DocumentTaken_Documents_Add",
 				getTotalWaitTimeInSecs());
 		clickAndWait("button_Action_Update_DocumentTaken_Documents_Add");
 		sleep(2000);
 	}
 	
	
  @AfterTest
  public void afterTest() throws Exception {
	  signOut();
	  driver.quit();
  }

}
