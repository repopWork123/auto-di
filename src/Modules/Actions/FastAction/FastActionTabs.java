package Modules.Actions.FastAction;

import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Modules.AuditLogs.AuditLogSearch;
import Modules.Documents.Document;
import Modules.Indexes.Indexes;
import ReusableUtilities.Dataprovider;
import ReusableUtilities.Report;

public class FastActionTabs extends FastAction {

	private static final String AT_LEAST_ONE_TARGET_ENTITY_REQUIRED_TO_CREATE_ASSOCIATION = "At least one Target entity is required to create an association.";
	private static final String AT_LEAST_ONE_TARGET_ENTITY_REQUIRED_TO_CREATE_ASSOCIATION_USAGE_MANDATORY = "At least one Target entity is required to create an association.\nUsage is mandatory";
	private static final String SELECT_VALID_TARGET_INCIDENT_AND_URN_OR_ENTRY_FROM_THE_DEFAULT_LIST = "Please select a valid Target Incident and URN or an entry from the Default List.";
	private static final String FIELD_DESCRIPTION_MANDATORY_IN_DESCRIBED_ASSOCIATION = "Field description is mandatory in a described association";
	private static final String USAGE_IS_MANDATORY = "Usage is mandatory";
	private static final String SELECT_A_VALID_TARGET_INCIDENT = SELECT_VALID_TARGET_INCIDENT_AND_URN_OR_ENTRY_FROM_THE_DEFAULT_LIST;
	private static final String ERROR_MESSAGE_FILE_INTERNAL_OR_EXTERNAL_LINK = "Please provide attachment information for either of File, Internal or External Link";
	private static final String CONFIRMATION_MESSAGE_TO_DELETE_AN_ATTACHMENT = "Are you sure you want to delete this attachment?";
	private String targetLinkURN;
	private String targetDescribedURN;
	private String targetSubjectURN;
	private String targetManualpotentialmatchURN;
	private String actionURN;
	private String documentURN;
	private String disclosureURN;
	private String entityURN;
	private String transferAssociationURN;
	String MIRSAP_FORCE = "ARMY";
	String MIRSAP_CLASS = "SCENE";
	String MIRSAP_PRIORITY = "Medium";
	String MIRSAP_STATION = "HEADQUARTERS";
	public String FOR_ALLOCATION = "For Allocation";
	private String attachement = System.getProperty("user.dir")+"\\img\\log_file.xml";
	private String attachement2 = System.getProperty("user.dir")+"\\img\\log_file2.xml";

	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}


	@BeforeTest
	public void beforeTest() throws Exception {
		// Steps starting from IE driver instance creation, Navigate to app URl,
		// Login , select default incident on Home Page are executed
		loginInIEAndSelectIncident();
//		 loginAndSelectIncident();

	}

	// default constructor
	public FastActionTabs() {
		softAssert = new SoftAssert();
	}
	

	@Test//(priority = 1)
	public void testData() throws Exception {
		
		// raise bulk fast action and splitting

		String bulkURn = raiseBulkFastActionAndGetURNs("5");

		String[] actURN_Split = bulkURn.split("-");
		targetLinkURN = actURN_Split[0];
		System.out.println(targetLinkURN + "  firstURN");
		String fastactURNcount = targetLinkURN.substring(1);
        // getting second urn
		int fastactURNcounts = Integer.parseInt(fastactURNcount) + 1;
		targetDescribedURN = "A" + String.valueOf(fastactURNcounts);

		System.out.println(targetDescribedURN + "  targetDescribedURN");
		int fastactURNcounts2 = Integer.parseInt(fastactURNcount) + 2;
		targetManualpotentialmatchURN = "A" + String.valueOf(fastactURNcounts2);
		System.out.println(targetManualpotentialmatchURN
				+ "  targetManualpotentialmatchURN");

		int fastactURNcounts3 = Integer.parseInt(fastactURNcount) + 3;
		actionURN = "A" + String.valueOf(fastactURNcounts3);
		System.out.println(actionURN + "  actionURN");

		int fastactURNcounts4 = Integer.parseInt(fastactURNcount) + 4;
		transferAssociationURN = "A" + String.valueOf(fastactURNcounts4);
		System.out.println(transferAssociationURN + "  transferAssociationURN");
        // create other document
		Document doc = new Document(driver);
		String bulkDocument = doc
				.registerBulkOtherDocumentAndGetURNsWithSubjectReference("2");
		String[] docURN_Split = bulkDocument.split("-");
		documentURN = docURN_Split[0];

		Indexes nominalIndex = new Indexes(driver);
        // create nominal bulk
		String bulkNominal = nominalIndex.createBulkNominalAndGetURNs(
				"Nominal", "MALE", "2");
		String[] bulkNominal_Split = bulkNominal.split("-");
		entityURN = bulkNominal_Split[0];
		System.out.println(entityURN + "  entityURN");
		String nominalURNcount = entityURN.substring(1);
		int nominalURNcounts = Integer.parseInt(nominalURNcount) + 1;
		targetSubjectURN = "N" + String.valueOf(nominalURNcounts);
		System.out.println(targetSubjectURN + "  targetSubjectURN");
        // getting disclosure urn
		raiseAndGetFastActionURN();
		expandDisclosureSection();
		verifyElementPresent("table_Action_View_Diclosure_URN");
		disclosureURN = getText("table_Action_View_Diclosure_URN");
		System.out.println(disclosureURN + "  disclosureURN");

		Report.info("targetLinkURN: " + targetLinkURN);
		Report.info("targetDescribedURN: " + targetDescribedURN);
		Report.info("targetSubjectURN: " + targetSubjectURN);
		Report.info("targetManualpotentialmatchURN: "
				+ targetManualpotentialmatchURN);
		Report.info("transferAssociationURN: " + transferAssociationURN);

	}


	// Test case 30088:002. Fast action Association Tab � Creating�
	@Test(priority = 2)
	public void createAssociationFastActionAssociationtabVerifyValidationMess(
			Method method) throws Exception {
		startLine(method);

		String fastActionAssociationURN = raiseAndGetFastActionURN();
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		sendKeys("textField_Action_ViewUrn", fastActionAssociationURN);
		clickAndWaitForPageLoad("button_Action_View");
		waitForPageLoad();
		isInFastActionViewPage();

		// click on association tab
		clickAndWait("tab_Association");
		// verify that association tab is enabled
		verifyAssociationTabEnabled();
		
		// click on add association
		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		
		ArrayList<String> linkType = new ArrayList<String>();
		linkType.add("Link");
		linkType.add("Described");
		linkType.add("Manual Potential Match");
		linkType.add("Subject");
		verifyElementPresent("button_Association_Add_Create");
		verifyElementPresent("button_Action_Association_Add_Cancel");

		verifyAllDowndownValues(linkType, "dropDown_Association_Add_Type");
		// Verify Error message
		// "Please select a valid Target Incident and URN or an entry from the Default List."
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_LINK);
		click("button_Association_Add_Create");
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",
				getTotalWaitTimeInSecs());
		verifyText(SELECT_A_VALID_TARGET_INCIDENT,
				"errorMsg_Action_Association_Add_Subject");

		// Verify Error message "Usage is mandatory""
		sendKeys("textField_Association_Add_TargetURN", targetLinkURN);
		click("button_Association_Add_Create");
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",
				getTotalWaitTimeInSecs());
		verifyText(USAGE_IS_MANDATORY,
				"errorMsg_Action_Association_Add_Subject");
		
		// Verify Error message
		// "Field Description is mandatory in a described association"
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_DESCRIBED);
		click("button_Association_Add_Create");
		waitForJQuery(driver);
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",
				getTotalWaitTimeInSecs());
		verifyText(FIELD_DESCRIPTION_MANDATORY_IN_DESCRIBED_ASSOCIATION,
				"errorMsg_Action_Association_Add_Subject");

		// Verify Error message
		// " Please select a valid Target Incident and URN or an entry from the Default List."
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_SUBJECT);
		sendKeys("textField_Association_Add_TargetURN", "");
		click("button_Association_Add_Create");
		waitForJQuery(driver);
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",
				getTotalWaitTimeInSecs());
		verifyText(
				SELECT_VALID_TARGET_INCIDENT_AND_URN_OR_ENTRY_FROM_THE_DEFAULT_LIST,
				"errorMsg_Action_Association_Add_Subject");
		
		// Verify Error message
		// " Please select a valid Target Incident and URN or an entry from the Default List."
		explicitlyWaitForElement("dropDown_Association_Add_Type",
				getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_MANUALPOTENTIALMATCH);
		sendKeys("textField_Association_Add_TargetURN", "");
		click("button_Association_Add_Create");
		waitForJQuery(driver);
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",
				getTotalWaitTimeInSecs());
		verifyText(
				SELECT_VALID_TARGET_INCIDENT_AND_URN_OR_ENTRY_FROM_THE_DEFAULT_LIST,
				"errorMsg_Action_Association_Add_Subject");
		
		// Verify Error message
		// "At least one Target entity is required to create an association.",
		// "Usage is mandatory""
		explicitlyWaitForElement("dropDown_Association_Add_Type",
				getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_LINK);
		explicitlyWaitForElement("button_Action_Association_AddMultipleAssociation", getTotalWaitTimeInSecs());
		click("button_Action_Association_AddMultipleAssociation");
		clickAndWait("button_Association_Add_Create");
		waitForJQuery(driver);
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",
				getTotalWaitTimeInSecs());
		verifyText(AT_LEAST_ONE_TARGET_ENTITY_REQUIRED_TO_CREATE_ASSOCIATION_USAGE_MANDATORY,
				"errorMsg_Action_Association_Add_Subject2");
		click("button_Action_Association_Add_Cancel");
		
		// Verify Error message
		// "At least one Target entity is required to create an association"
		
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_DESCRIBED);
		explicitlyWaitForElement("button_Action_Association_AddMultipleAssociation",
				getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Association_AddMultipleAssociation");
		click("button_Association_Add_Create");
		waitForJQuery(driver);
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",
				getTotalWaitTimeInSecs());
		verifyText(AT_LEAST_ONE_TARGET_ENTITY_REQUIRED_TO_CREATE_ASSOCIATION,
				"errorMsg_Action_Association_Add_Subject");
		click("button_Action_Association_Add_Cancel");
		
		
		// Verify Error message
		// " At least one Target entity is required to create an association"
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_SUBJECT);
		click("button_Action_Association_AddMultipleAssociation");
		click("button_Association_Add_Create");
		waitForJQuery(driver);
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",
				getTotalWaitTimeInSecs());
		verifyText(AT_LEAST_ONE_TARGET_ENTITY_REQUIRED_TO_CREATE_ASSOCIATION,
				"errorMsg_Action_Association_Add_Subject");
		click("button_Action_Association_Add_Cancel");

		
		// Verify Error message
		// " At least one Target entity is required to create an association"
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_MANUALPOTENTIALMATCH);
		click("button_Action_Association_AddMultipleAssociation");
		click("button_Association_Add_Create");
		waitForJQuery(driver);
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",
				getTotalWaitTimeInSecs());
		verifyText(AT_LEAST_ONE_TARGET_ENTITY_REQUIRED_TO_CREATE_ASSOCIATION,
				"errorMsg_Action_Association_Add_Subject");
		click("button_Action_Association_Add_Cancel");

		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_LINK);
		verifyElementIsMandatory("label_Association_Target");
		verifyElementIsMandatory("label_Association_Usage");
		

		click("checkBox_Association_Provisinal");
		sendKeys("textField_Association_Add_TargetURN", targetLinkURN);
		waitForLinkTypeUsageDropdownLoad();
		click("dropDown_Action_Association_Add_Usage");
		click("dropDown_Action_Association_Add_Usage_input");
		explicitlyWaitForElement("dropDown_Action_Association_Add_Usage_List",
				getTotalWaitTimeInSecs());
		click("dropDown_Action_Association_Add_Usage_List");
		sendKeys("textfield_Association_StartDate", "26/06/2016");
		sendKeys("textfield_Association_EndDate", "26/08/2016");
		sendKeys("textField_Action_Association_Add_Description",
				"Dev smoke test - Described association");
		click("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Add_Association");
		String succMess_Link = getText("successMsg_Action_Add_Association");
		String[] actUrn_Link = succMess_Link.split(" ");
		int size1 = actUrn_Link.length;
		String Link_urn = actUrn_Link[size1 - 1];
		Report.info(Link_urn);
		verifyTextCompare(targetLinkURN, Link_urn);
		
		//Update association
		explicitlyWaitForElement("icon_Action_Association_Update",
				getTotalWaitTimeInSecs());
		verifyElementPresent("icon_Action_Association_Update");
		//click("icon_Action_Association_Update");
		String parentWindowHandle = getCurrentWindowHandle();
		Set<String>currentParentWindowHandles = driver.getWindowHandles();
		int currentWindowCount = getWindowCount();
		WebDriver softEntityWinDriver = clickAndWaitForChildWindow("Soft Entity Portlet",currentParentWindowHandles,"icon_Action_Association_Update",currentWindowCount,currentWindowCount+1);
		String softEntityWinHandle = softEntityWinDriver.getWindowHandle();
		explicitlyWaitForElement("textField_Action_Association_Update_Descrption",
				getTotalWaitTimeInSecs());
		click("button_Action_Association_Update_Cancel");
		
		int currentWindowCount1 = getWindowCount();
		driver = closeChildWindowAndNavigateToParentWindowHandle(softEntityWinHandle, parentWindowHandle, currentWindowCount1, currentWindowCount1-1);
		explicitlyWaitForElement("icon_Action_Association_Update",
				getTotalWaitTimeInSecs());
		verifyElementPresent("icon_Action_Association_Update");
	
		String parentWindowHandle1 = getCurrentWindowHandle();
		Set<String>currentParentWindowHandles1 = driver.getWindowHandles();
		int currentWindowCount2 = getWindowCount();
		WebDriver softEntityWinDriver1 = clickAndWaitForChildWindow("Soft Entity Portlet",currentParentWindowHandles,"icon_Action_Association_Update",currentWindowCount2,currentWindowCount2+1);
		String softEntityWinHandle2 = softEntityWinDriver.getWindowHandle();
		explicitlyWaitForElement("textField_Action_Association_Update_Descrption",
				getTotalWaitTimeInSecs());
		sendKeysWithDriver("textField_Action_Association_Update_Descrption", ASSOCIATION_UPDATE_DESCRIPTION);
		verifyElementPresent("button_Action_Association_Update_Save");
		clickAndWait("button_Action_Association_Update_Save");
		explicitlyWaitForElement("successMsg_Action_Association_Update", getTotalWaitTimeInSecs());
		int currentWindowCount3 = getWindowCount();
		driver = closeChildWindowAndNavigateToParentWindowHandle(softEntityWinHandle2, parentWindowHandle1, currentWindowCount3, currentWindowCount3-1);
		
		//Delete association
		explicitlyWaitForElement("icon_Action_Association_Delete",
				getTotalWaitTimeInSecs());
		verifyElementPresent("icon_Action_Association_Delete");
		click("icon_Action_Association_Delete");
		waitForAlertTextAndClose(ASSOCIATION_DELETE_POPUP_MESSAGE);
		verifyElementPresent("successMsg_Action_Delete");
		
		//verifyHistoryTab
		click("tab_History");
		explicitlyWaitForElement("table_History_HistoryRecord_Username",
				getTotalWaitTimeInSecs());
		verifyText(usernameUpperCase,"table_History_HistoryRecord_Username");
		verifyTextContains(CREATE_ASSOCIATION,"table_Action_HistoryRecord_CreateAssociation");
		verifyTextContains(UPDTAE_ASSOCIATION,"table_Action_HistoryRecord_CreateAssociation");
		verifyTextContains(DELETE_ASSOCIATION,"table_Action_HistoryRecord_CreateAssociation");
		
		click("table_Action_HistoryRecord_CreateAssociation");
		viewHistoryTabSearchResult(CREATE_ASSOCIATION);
		
		click("table_Action_HistoryRecord_UpdateAssociation");
		viewHistoryTabSearchResult(UPDTAE_ASSOCIATION);
		
		click("table_Action_HistoryRecord_CreateAssociation");
		viewHistoryTabSearchResult(DELETE_ASSOCIATION);
		
	
		endLine(method);
	}

	// Test case 30088:002. Fast action Association Tab � Creating�
	@Test(priority = 3)
	public void createAssociationFastActionAssociationtab(Method method)
			throws Exception {
		startLine(method);
		raiseAndGetFastActionURN(driver);
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",
				getTotalWaitTimeInSecs());
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficer",
				getTotalWaitTimeInSecs());
		clickAndWait("button_Action_ChangeState_CreateOfficer");
		explicitlyWaitForElement("textfield_Action_CreateOfficer_Surname", getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_Forename",
				CREATE_OFFICER_FORENAME);
		clickAndWait("button_Action_Fast_Create");
		insertTextInAllocatedOfficerTextField("textfield_Action_FastAction_QueueAllocatedOfficer", CREATE_OFFICER_SURNAME);
		
		String officerName = "SurName";
		selectOptionWithText(officerName, "textfield_Action_StateMove_AllocatedOfficerName");
		
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());

		// click on association tab
		click("tab_Association");
		// verify that association tab is enabled
		verifyAssociationTabEnabled();
		// click on add association
		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");

		Report.info("Create Link type of association");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_LINK);
		sendKeys("textField_Association_Add_TargetURN", actionURN);
		waitForLinkTypeUsageDropdownLoad();
		click("dropDown_Action_Association_Add_Usage");
		click("dropDown_Action_Association_Add_Usage_input");
		explicitlyWaitForElement("dropDown_Action_Association_Add_Usage_List",
				getTotalWaitTimeInSecs());
		click("dropDown_Action_Association_Add_Usage_List");
		click("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Add_Association");
		String succMess_Link = getText("successMsg_Action_Add_Association");
		String[] actUrn_Link = succMess_Link.split(" ");
		int size1 = actUrn_Link.length;
		String Link_urn = actUrn_Link[size1 - 1];
		Report.info(Link_urn);
		verifyTextCompare(actionURN, Link_urn);
		// Create Described type of association
		Report.info("Create Described type of association");
		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_DESCRIBED);
		
		sendKeys("textField_Association_Add_TargetURN", targetDescribedURN);
		sendKeys("textField_Action_Association_Add_Description",
				"Dev smoke test - Described association");
		click("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Add_Association");
		String succMess_Described = getText("successMsg_Action_Add_Association");
		String[] actUrn_Described = succMess_Described.split(" ");
		int size2 = actUrn_Described.length;
		String described_urn = actUrn_Described[size2 - 1];
		Report.info(described_urn);
		verifyTextCompare(targetDescribedURN, described_urn);

		Report.info("Create Subject type of association with nominal record");

		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_SUBJECT);
		sendKeys("textField_Association_Add_TargetURN", targetSubjectURN);
		click("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Add_Association");
		String succMess_Subject = getText("successMsg_Action_Add_Association");
		String[] actUrn_Subject = succMess_Subject.split(" ");
		int size3 = actUrn_Subject.length;
		String Subject_urn = actUrn_Subject[size3 - 1];
		Report.info(Subject_urn);
		verifyTextCompare(targetSubjectURN, Subject_urn);

		Report.info("Create manual potential match type of association");
		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_MANUALPOTENTIALMATCH);
		sendKeys("textField_Association_Add_TargetURN",
				targetManualpotentialmatchURN);
		click("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Add_Association");
		String succMess_MPM = getText("successMsg_Action_Add_Association");
		String[] actUrn_MPM = succMess_MPM.split(" ");
		int size4 = actUrn_MPM.length;
		String MPM_urn = actUrn_MPM[size4 - 1];
		Report.info(MPM_urn);
		verifyTextCompare(targetManualpotentialmatchURN, MPM_urn);
		
		
		endLine(method);

	}

	// Pre - requisite : Tags should be assigned to Fast action
	// Test case 30479*:002. Tags Tab
	@Test(priority = 4)
	public void fastActiontagsTab(Method method) throws Exception {

		startLine(method);
		navigateTo(ACTION_DASHBOARD);
		explicitlyWaitForElement("dropDown_Action_ActionDashboard_Search_Type",
				getTotalWaitTimeInSecs());
		// Select type as "fast action"
		selectByVisibleText("dropDown_Action_ActionDashboard_Search_Type",
				SEARCH_TYPE);
		// click on search button
		explicitlyWaitForElement("button_Action_Search", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Search");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify search result tab is enabled
		verifySearchResultTabEnabled();
		// select the first record
		click("checkBox_Action_SearchResulttab_Firstrow");
		explicitlyWaitForElement("icon_Action_Searchresult_View",
				getTotalWaitTimeInSecs());
		// click on view icon and wait for pop up
		clickAndWaitForBrowserPopup(FAST_ACTION_TITLE_WINDOW,
				"icon_Action_Searchresult_View");

		// click on tags tab and add some tags
		explicitlyWaitForElement("tab_tags",
				getTotalWaitTimeInSecs());
		clickAndWait("tab_tags");
		click("button_Tags_Edit");
		// check the first tag
		click("button_Tags_firsttag");
		click("button_Tags_Save");

		explicitlyWaitForElement("successMsg_Tags_Update",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Tags_Update");

		// Click on edit button again and remove tags

		click("button_Tags_Edit");
		// un check the same first tag
		click("button_Tags_firsttag");
		click("button_Tags_Save");
		explicitlyWaitForElement("successMsg_Tags_Update",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Tags_Update");
		
		//verifyHistoryTab
		click("tab_History");
		explicitlyWaitForElement("table_History_HistoryRecord_Username",
				getTotalWaitTimeInSecs());
		verifyText(usernameUpperCase,"table_History_HistoryRecord_Username");
		verifyTextContains(ADD_TAG_TO_RECORD,"table_Action_HistoryRecord_AddTagToRecord");
		click("table_Action_HistoryRecord_AddTagToRecord");
		viewHistoryTabSearchResult(ADD_TAG_TO_RECORD);
		
		// close the pop up window and navigate back to search result tab
		driver = closePopupWindowAndNavigateToParentWindow(FAST_ACTION_TITLE_WINDOW,
				ACTION_DASHBOARD_TITLE_WINDOW);
		
		endLine(method);

	}

	// Test case 9445:005. Attachments Tab
	@Test(priority = 5)
	public void fastActionAttachmentsTab(Method method) throws Exception {
		startLine(method);
		Path p = Paths.get(attachement);
		String fileName = p.getFileName().toString();

		Path p2 = Paths.get(attachement2);
		String fileName2 = p.getFileName().toString();

		// file upload
		// navigate to view action page
		String fastActionAttachmentURN = raiseAndGetFastActionURN();
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		sendKeys("textField_Action_ViewUrn", fastActionAttachmentURN);
		clickAndWaitForPageLoad("button_Action_View");
		waitForPageLoad();
		isInFastActionViewPage();
		explicitlyWaitForElement("tab_Attachment", getTotalWaitTimeInSecs());
		// click on Attachments Tab
		clickAndWait("tab_Attachment");
		explicitlyWaitForElement("tab_Attachment_FileUpload", getTotalWaitTimeInSecs());
		verifyElementPresent("tab_Attachment_FileUpload");
		verifyElementPresent("tab_Attachment_InternalLinkUpload");
		verifyElementPresent("tab_Attachment_ExternalLinkUpload");
		verifyElementPresent("button_Attachment_AddRow");
		verifyElementPresent("button_Attachment_SaveAttachments");
		// verifyElementNotPresent("tab_AttachmentTick");
		Report.info("First file details:" + attachement);
		explicitlyWaitForElement("button_Attachment_Browse", getTotalWaitTimeInSecs());
		sendKeysWithDriverNoClick("button_Attachment_Browse", attachement);
		clickAndWait("button_Attachment_SaveAttachments");
		verifyText(fileName, "link_Attachment_FileUpload_FirstFile");

		// Verification of View Action Page
		verifyElementPresent("successMsg_Attachment");
		String succMess = getText("successMsg_Attachment");
		String[] fileUploadSuccMess = succMess.split(" ");
		String incidentAction = fileUploadSuccMess[4];
		String[] incidentIDWithAction = incidentAction.split("-");
		String incidentID = incidentIDWithAction[0];
		String ActionURN = incidentIDWithAction[1];
		verifyTextCompare(incidentID, incidentIdFull);
		verifyTextCompare(ActionURN, fastActionAttachmentURN);
		// verifyTextCompare(ActionURN, "A1");

		// AddRow validation
		verifyElementNotPresent("table_Attachment_FileUpload_SecondRow");
		click("button_Attachment_AddRow");
		verifyElementPresent("table_Attachment_FileUpload_SecondRow");

		// Delete row validation
		click("checkBox_Attachment_FileUpload_SecondRow");
		click("button_Attachment_DeleteRow");
		verifyElementNotPresent("table_Attachment_FileUpload_SecondRow");

		// Delete File Upload
		sendKeysWithDriverNoClick("button_Attachment_Browse", attachement2);
		clickAndWait("button_Attachment_SaveAttachments");
		verifyElementPresent("successMsg_Attachment");
		verifyText(FilenameUtils.getBaseName(attachement2) 
				+ "." + FilenameUtils.getExtension(attachement2), "link_Attachment_FileUpload_SecondFile");
		click("icon_Attachment_FileUpload_Delete_Second");
		waitForAlertTextAndClose(CONFIRMATION_MESSAGE_TO_DELETE_AN_ATTACHMENT);
		String expectedDelFileUploadText = "File " 
				+ FilenameUtils.getBaseName(attachement2) 
				+ "." + FilenameUtils.getExtension(attachement2)
				+ " deleted from " + incidentIdFull + "-"
				+ fastActionAttachmentURN;

		Report.info("Construted Expected expectedDelFileUploadText "
				+ expectedDelFileUploadText);
		waitForJQuery(driver);
		waitForPageLoad();
		explicitlyWaitForElement("successMsg_Attachment",getTotalWaitTimeInSecs());
		verifyText(expectedDelFileUploadText, "successMsg_Attachment");

		// internal link
		explicitlyWaitForElement("tab_Attachment_InternalLinkUpload", getTotalWaitTimeInSecs());
		click("tab_Attachment_InternalLinkUpload");
		String interLink = "\\\\\\\\gbhlm-vm-fs01\\\\U-LEAF_Test\\\\Holmes Futures\\\\System Management UAT\\\\System_Management_Acceptance_Test_Script.doc";

		String interLinkFileName = FilenameUtils.getBaseName(interLink) + "." + FilenameUtils.getExtension(interLink);
		explicitlyWaitForElement("textfield_Attachment_InternalLinkUpload", getTotalWaitTimeInSecs());
		click("textfield_Attachment_InternalLinkUpload");
		sleep(2000);
		sendKeys("textfield_Attachment_InternalLinkUpload", interLink);
		explicitlyWaitForElement("button_Attachment_SaveAttachments", getTotalWaitTimeInSecs());
		
		clickAndWait("button_Attachment_SaveAttachments");
		verifyElementPresent("successMsg_Attachment");
		String expectedInternalLinkSuccText = "File " + interLinkFileName
				+ " attached to " + incidentIdFull + "-"
				+ fastActionAttachmentURN;
		
		verifyText(expectedInternalLinkSuccText, "successMsg_Attachment");
		clickAndWait("button_Attachment_SaveAttachments");
		explicitlyWaitForElement("errorMsg_Attachment", getTotalWaitTimeInSecs());
		verifyElementPresent("errorMsg_Attachment");
		verifyText(ERROR_MESSAGE_FILE_INTERNAL_OR_EXTERNAL_LINK,
				"errorMsg_Attachment");

		// external link
		explicitlyWaitForElement("tab_Attachment_ExternalLinkUpload", getTotalWaitTimeInSecs());
		click("tab_Attachment_ExternalLinkUpload");
		String externalLink = "http://www.cs.nott.ac.uk/~cah/G53QAT/G53QAT10pdf6up.pdf";
		
		String externalLinkFileName = FilenameUtils.getBaseName(externalLink) + "." + FilenameUtils.getExtension(externalLink);
		click("tab_Attachment_ExternalLinkUpload");
		sendKeys("textfield_Attachment_ExternalLinkUpload", externalLink);
		clickAndWait("button_Attachment_SaveAttachments");
		verifyElementPresent("successMsg_Attachment");
		String expectedExternalLinkSuccText = "File " + externalLinkFileName
				+ " attached to " + incidentIdFull + "-"
				+ fastActionAttachmentURN;
		
		verifyTextContains(expectedExternalLinkSuccText, "successMsg_Attachment");
		click("button_Attachment_SaveAttachments");
		explicitlyWaitForElement("errorMsg_Attachment", getTotalWaitTimeInSecs());
		verifyElementPresent("errorMsg_Attachment");
		verifyText(ERROR_MESSAGE_FILE_INTERNAL_OR_EXTERNAL_LINK,
				"errorMsg_Attachment");
		
		//verifyHistoryTab
		clickAndWait("tab_History");
		explicitlyWaitForElement("table_History_HistoryRecord_Username",
				getTotalWaitTimeInSecs());
		
		verifyText(usernameUpperCase,"table_History_HistoryRecord_Username");
		verifyTextContains(ADD_ATTACHEMENT,"table_Action_HistoryRecord_AddAttachment");
		verifyTextContains(REMOVE_ATTACHEMENT,"table_Action_HistoryRecord_RemoveAttachment");
		click("table_Action_HistoryRecord_AddAttachment");
		viewHistoryTabSearchResult(ADD_ATTACHEMENT);
		click("table_Action_HistoryRecord_RemoveAttachment");
		viewHistoryTabSearchResult(REMOVE_ATTACHEMENT);

		
		endLine(method);
	}

	// Pre- requisite Marker should be added already to fast action
	// Notification testing is not done - Varija
	// Test case 9447:006.FAST Action -Marker tab
	@Test(priority = 6)
	public void fastActionMarkerTab(Method method) throws Exception

	{
		startLine(method);
		// Raise Fast action
		raiseAndGetFastActionURN();
		// View Fast action
		viewAndGetFastActionURN();
		verifyFastActionTabEnabled();
		click("tab_Marker");
		verifyMarkerTabEnabled();
		// Add Marker
		click("icon_Marker_Add");
		verifyElementPresent("text_Marker_MarkerDialogbox");
		verifyElementPresent("textField_Marker_Add_Expirydate");
		verifyElementPresent("button_Marker_Add_Submit");
		verifyElementPresent("button_Marker_Add_Cancel");
		verifyElementPresent("checkbox_Marker_Add_Firstrecord");
		verifyElementPresent("icon_Marker_Add_Refresh");
		explicitlyWaitForElement("textField_Marker_Add_Expirydate",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Marker_Add_Expirydate", MARKER_EXPIRYDATEADD);
		click("checkbox_Marker_Add_Firstrecord");
		click("checkbox_Marker_Add_Secondrecord");
		click("button_Marker_Add_Submit");
		explicitlyWaitForElement("textField_Marker_ExpirydateEdited_Firstrow",
				getTotalWaitTimeInSecs());
		verifyText(MARKER_EXPIRYDATEADD,
				"textField_Marker_ExpirydateEdited_Firstrow");
		verifyText(MARKER_EXPIRYDATEADD,
				"textField_Marker_ExpirydateEdited_Secondrow");

		// Edit Marker

		click("checkbox_Marker_Firstrecord");
		click("icon_Marker_Edit");
		explicitlyWaitForElement("textField_Marker_Edit_Expirydate",
				getTotalWaitTimeInSecs());
		verifyElementPresent("textField_Marker_Edit_Expirydate");
		sendKeys("textField_Marker_Edit_Expirydate", MARKER_EXPIRYDATEEDITED);
		clickAndWait("button_Marker_Edit_Submit");
		explicitlyWaitForElement("textField_Marker_ExpirydateEdited_Secondrow",
				getTotalWaitTimeInSecs());
		verifyText(MARKER_EXPIRYDATEEDITED,
				"textField_Marker_ExpirydateEdited_Secondrow");

		// Delete Marker

		click("checkbox_Marker_Firstrecord");
		click("icon_Marker_Delete");
		// Verify pop up is displayed
		verifyText(MARKER_DELETE_POPUP, "label_Marker_Delete_Popup");
		clickAndWait("button_Marker_Delete_Yes");

		// validation for edit and delete icon
		explicitlyWaitForElement("icon_Marker_Edit", getTotalWaitTimeInSecs());
		click("icon_Marker_Edit");
		verifyText(MARKER_EDIT_POPUP_ERROR, "label_Marker_Delete_Popup");
		click("button_Marker_ErrorPopup_OK");
		click("icon_Marker_Delete");
		verifyText(MARKER_DELETE_POPUP_ERROR, "label_Marker_Delete_Popup");
		click("button_Marker_ErrorPopup_OK");
		
		endLine(method);

	}

	// Test case 30283:001a. Fast/converted Fast action Association Tab -
	// Subject Type Association
	//HLM00009073 - Fast To Mirsap Action Converting does not convert Fast action associations from ' A' type to ' S' type . 
	@Test(priority = 7)
	public void fastActionConvertedFastActionSubjectassociation(Method method)
			throws Exception {
		startLine(method);
		// Raise Fast action
		raiseAndGetFastActionURN();
		// View Fast action
		viewAndGetFastActionURN();
		verifyFastActionTabEnabled();
		explicitlyWaitForElement("button_Action_Update",
				getTotalWaitTimeInSecs());
		// verify Association Tab Records
		explicitlyWaitForElement("tab_Association",
				getPoolWaitTime());
		clickAndWait("tab_Association");
		waitForPageLoad();
		verifyAssociationTabEnabled();
		// click on add association and enter Action urn as Subject urn
		explicitlyWaitForElement("icon_Association_Add", getTotalWaitTimeInSecs());
		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_SUBJECT);
		sendKeys("textField_Association_Add_TargetURN", actionURN);
		click("button_Association_Add_Create");
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",
				getTotalWaitTimeInSecs());
		verifyElementPresent("errorMsg_Action_Association_Add_Subject");
		String actmessage = getText("errorMsg_Action_Association_Add_Subject");
		String[] error_actmessg = actmessage.split(" ");
		Report.info(error_actmessg[5]);
		String[] error_actsubject_URN = error_actmessg[5].split("-");
		verifyTextCompare(error_actsubject_URN[1], actionURN);
		click("button_Action_Association_Add_Cancel");

		// click on add association and enter document urn as Subject urn
		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_SUBJECT);
		sendKeys("textField_Association_Add_TargetURN", documentURN);
		click("button_Association_Add_Create");
		verifyElementPresent("errorMsg_Action_Association_Add_Subject");
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",
				getTotalWaitTimeInSecs());
		String docmessage = getText("errorMsg_Action_Association_Add_Subject");
		String[] error_docmessg = docmessage.split(" ");
		Report.info(error_docmessg[1]);
		String[] error_docsubject_URN = error_docmessg[1].split("-");
		verifyTextCompare(error_docsubject_URN[1], documentURN);
		click("button_Action_Association_Add_Cancel");

		// click on add association and enter disclosure urn as Subject urn
		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_SUBJECT);
		sendKeys("textField_Association_Add_TargetURN", disclosureURN);
		click("button_Association_Add_Create");
		verifyElementPresent("errorMsg_Action_Association_Add_Subject");
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",
				getTotalWaitTimeInSecs());
		String disclosuremessage = getText("errorMsg_Action_Association_Add_Subject");
		String[] error_disclosuremessg = disclosuremessage.split(" ");
		Report.info(error_disclosuremessg[5]);
		String[] error_disclosuresubject_URN = error_disclosuremessg[5]
				.split("-");
		verifyTextCompare(error_disclosuresubject_URN[1], disclosureURN);
		click("button_Action_Association_Add_Cancel");

		// click on add association and enter entity urn as Subject urn for
		// first time
		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_SUBJECT);
		sendKeys("textField_Association_Add_TargetURN", entityURN);
		click("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Add_Association");
		String succMess_SubEntity = getText("successMsg_Action_Add_Association");
		String[] EntityUrn_Subject = succMess_SubEntity.split(" ");
		int size5 = EntityUrn_Subject.length;
		String EntitySubject_urn = EntityUrn_Subject[size5 - 1];
		Report.info(EntitySubject_urn);
		verifyTextCompare(entityURN, EntitySubject_urn);
		explicitlyWaitForElement("icon_Action_Association_Update",
				getTotalWaitTimeInSecs());
		verifyElementPresent("icon_Action_Association_Update");
		verifyElementPresent("icon_Action_Association_Delete");

		// click on add association and enter entity urn as Subject urn for
		// second time
		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_SUBJECT);
		sendKeys("textField_Association_Add_TargetURN", entityURN);
		click("button_Association_Add_Create");
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",
				getTotalWaitTimeInSecs());
		String seconderrormessage = getText("errorMsg_Action_Association_Add_Subject");
		String[] seconderror_messg = seconderrormessage.split(" ");
		Report.info(seconderror_messg[5]);
		String[] second_subject_URN = seconderror_messg[5].split("-");
		verifyTextCompare(second_subject_URN[1], entityURN);
		click("button_Action_Association_Add_Cancel");

		// click on add association and enter entity urn as Subject urn for
		// third time
		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_SUBJECT);
		sendKeys("textField_Association_Add_TargetURN", entityURN);
		click("button_Association_Add_Create");
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",
				getTotalWaitTimeInSecs());
		String thirderrormessage = getText("errorMsg_Action_Association_Add_Subject");
		String[] thirderror_messg = thirderrormessage.split(" ");
		Report.info(thirderror_messg[5]);
		String[] third_subject_URN = thirderror_messg[5].split("-");
		verifyTextCompare(third_subject_URN[1], entityURN);
		click("button_Action_Association_Add_Cancel");

		// Create category and click on association tab and Action URN as
		// Subject type association
		Indexes indexes = new Indexes(driver);
		indexes.createCategory();
		clickAndWait("tab_Association");
		
		verifyAssociationTabEnabled();
		// click on add association and enter Action urn as Subject urn
		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_SUBJECT);
		sendKeys("textField_Association_Add_TargetURN", actionURN);
		click("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Add_Association");
		String succMess_SubActEntity = getText("successMsg_Action_Add_Association");
		String[] ACTUrn_Subject = succMess_SubActEntity.split(" ");
		int size6 = ACTUrn_Subject.length;
		String ACTSubject_urn = ACTUrn_Subject[size6 - 1];
		Report.info(ACTSubject_urn);
		verifyTextCompare(actionURN, ACTSubject_urn);

		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		explicitlyWaitForElement("icon_Action_Association_Update",
				getTotalWaitTimeInSecs());
		verifyElementPresent("icon_Action_Association_Update");
		verifyElementPresent("icon_Action_Association_Delete");
		click("checkBox_Action_Association_FirstRecord");
		explicitlyWaitForElement("icon_Entity_Association_View", getTotalWaitTimeInSecs());
		clickAndWait("icon_Entity_Association_View");
		waitForPageLoad();
		clickAndWait("tab_tags");
		explicitlyWaitForElement("tab_Association", getTotalWaitTimeInSecs());
		clickAndWait("tab_Association");
		
		explicitlyWaitForElement("icon_Action_Association_Update",
				getTotalWaitTimeInSecs());
		verifyElementPresent("icon_Action_Association_Update");
		verifyElementPresent("icon_Action_Association_Delete");

		// convert fast action A10 to Mirsap action and check the association
		
		String fastActionUrn=raiseAndGetFastActionURN();

		navigateTo(VIEWACTION_URL);
		// verifyIsInViewActionPage();
		// verify View Action Tab
		explicitlyWaitForElement("tab_View_Action", getTotalWaitTimeInSecs());
		verifyElementPresent("tab_View_Action");
		sendKeys("textField_Action_ViewUrn", fastActionUrn);// fastActionURN
		clickAndWaitForPageLoad("button_Action_View");
		isInFastActionViewPage();

		// Clicking convert action button and checking the S type is converted
		// to A type
		explicitlyWaitForElement("button_Action_FastAction_Convert", getTotalWaitTimeInSecs());
		click("button_Action_FastAction_Convert");
		// Verification for Convert Popup page header
		verifyElementPresent("label_Action_Covert_ConvertEntity");

		// Verify All Previous values are retained in Convert Action page

		verifyText(ACTION_AUTO_TEST_FOR_RAISE_FAST_ACTION_TITLE,
				"textField_Action_Title");
		verifyTextCompare(TINY_MCE_TEXT_FOR_RAISE_FAST_ACTION,
				getTextFieldText());
		// Entering mandatory fields in convert action page

		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_RAISE_FAST_ACTION);
		clickAndWait("button_Action_FastAction_ConvertAction");
		explicitlyWaitForElement("label_Action_View_Page_Title",
				getTotalWaitTimeInSecs());
		explicitlyWaitForElement("button_Action_Update",
				getTotalWaitTimeInSecs());
		explicitlyWaitForElement("tab_tags", getTotalWaitTimeInSecs());
		clickAndWait("tab_tags");
		waitForJQuery(driver);
		waitForPageLoad();
		explicitlyWaitForElement("tab_Association", getTotalWaitTimeInSecs());
		clickAndWait("tab_Association");
		
		verifyAssociationTabEnabled();
		
		verifyElementNotPresent("icon_Action_Association_Delete");
		
		endLine(method);

	}


	// *Test case 9442:004. Transfer Association*/
	@Test(priority = 8)
	public void transferAssociation(Method method) throws Exception {
		startLine(method);
		// Raise Fast action
		raiseAndGetFastActionURN();
		// View Fast action
		viewAndGetFastActionURN();
		verifyFastActionTabEnabled();
		waitForPageLoad();
		clickAndWait("tab_Association");
		verifyAssociationTabEnabled();

		// Create Link type of association and transfer association

		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_LINK);
		sendKeys("textField_Association_Add_TargetURN", targetLinkURN);
		waitForLinkTypeUsageDropdownLoad();
		click("dropDown_Action_Association_Add_Usage");
		click("dropDown_Action_Association_Add_Usage_input");
		explicitlyWaitForElement("dropDown_Action_Association_Add_Usage_List",
				getTotalWaitTimeInSecs());
		click("dropDown_Action_Association_Add_Usage_List");
		clickAndWait("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Add_Association");
		String succMess_Link = getText("successMsg_Action_Add_Association");
		Report.info("Success Message Text Link Type:" + succMess_Link);
		String[] actUrn_Link = succMess_Link.split(" ");
		int size7 = actUrn_Link.length;
		String Link_urn = actUrn_Link[size7 - 1];
		Report.info(Link_urn);
		verifyTextCompare(targetLinkURN, Link_urn);
		click("checkBox_Action_Association_FirstRecord");
		// transfer association to another action record
		click("icon_Action_Association_TransferAssociation");
		explicitlyWaitForElement(
				"text_Association_TransferAssociation_Dialogbox",
				getTotalWaitTimeInSecs());
		verifyElementPresent("text_Association_TransferAssociation_Dialogbox");
		verifyElementPresent("dropDown_Association_TransferAssociation_ReferenceIncident");
		verifyElementPresent("dropDown_Association_TransferAssociation_DefaultlistRecord");
		verifyElementPresent("textfield_Association_TransferAssociation_URN");
		explicitlyWaitForElement(
				"dropDown_Association_TransferAssociation_ReferenceIncident_Arrow",
				getTotalWaitTimeInSecs());
		clickAndWait("dropDown_Association_TransferAssociation_ReferenceIncident_Arrow");
		click("dropDown_Association_TransferAssociation_ReferenceIncident_Incident");
		explicitlyWaitForElement(
				"textfield_Association_TransferAssociation_URN",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Association_TransferAssociation_URN",
				transferAssociationURN);
		explicitlyWaitForElement(
				"button_Association_TransferAssociation_Submit",
				getTotalWaitTimeInSecs());
		clickAndWait("button_Association_TransferAssociation_Submit");
		verifyElementPresent("successMsg_Action_Add_Association");
		// view the action and verify that it is transfered
		if(isAlertPresent()){
			acceptAlertBox(driver);
		}
		navigateTo(VIEWACTION_URL);
	
		sendKeys("textField_Action_ViewUrn", transferAssociationURN);// fastActionURN
		clickAndWaitForPageLoad("button_Action_View");
		waitForJQuery(driver);
		explicitlyWaitForElement("label_Action_View_Page_Title",
				getTotalWaitTimeInSecs());
		isInFastActionViewPage();
		explicitlyWaitForElement("tab_Association", getTotalWaitTimeInSecs());
		clickAndWait("tab_Association");		
		verifyAssociationTabEnabled();
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + targetLinkURN);
		explicitlyWaitForElement("table_Action_AssociationTab_TargetURN_FirstRecord", getTotalWaitTimeInSecs());
		verifyElementPresent("table_Action_AssociationTab_TargetURN_FirstRecord");
		if(isAlertPresent()){
			acceptAlertBox(driver);
		}
		// Create Described type of association and transfer association

		raiseAndGetFastActionURN();
		// View Fast action
		viewAndGetFastActionURN();
		verifyFastActionTabEnabled();
		clickAndWait("tab_Association");
		verifyAssociationTabEnabled();

		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_DESCRIBED);
		sendKeys("textField_Association_Add_TargetURN", targetDescribedURN);
		sendKeys("textField_Action_Association_Add_Description",
				"Dev smoke test - Described association");
		click("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Add_Association");
		String succMess_Described = getText("successMsg_Action_Add_Association");
		String[] actUrn_Described = succMess_Described.split(" ");
		int size8 = actUrn_Described.length;
		String described_urn = actUrn_Described[size8 - 1];
		Report.info(described_urn);
		verifyTextCompare(targetDescribedURN, described_urn);

		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getTotalWaitTimeInSecs());
		click("checkBox_Action_Association_FirstRecord");
		// transfer association to another action record
		click("icon_Action_Association_TransferAssociation");
		explicitlyWaitForElement(
				"text_Association_TransferAssociation_Dialogbox",
				getTotalWaitTimeInSecs());
		verifyElementPresent("text_Association_TransferAssociation_Dialogbox");
		verifyElementPresent("dropDown_Association_TransferAssociation_ReferenceIncident");
		verifyElementPresent("dropDown_Association_TransferAssociation_DefaultlistRecord");
		verifyElementPresent("textfield_Association_TransferAssociation_URN");
		explicitlyWaitForElement(
				"dropDown_Association_TransferAssociation_ReferenceIncident_Arrow",
				getTotalWaitTimeInSecs());
		clickAndWait("dropDown_Association_TransferAssociation_ReferenceIncident_Arrow");
		
		click("dropDown_Association_TransferAssociation_ReferenceIncident_Incident");
		explicitlyWaitForElement(
				"textfield_Association_TransferAssociation_URN",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Association_TransferAssociation_URN",
				transferAssociationURN);
		clickAndWait("button_Association_TransferAssociation_Submit");
		if(isAlertPresent()){
			acceptAlertBox(driver);
		}
		explicitlyWaitForElement("successMsg_Action_Add_Association", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Add_Association");
		// view the action and verify that it is transfered
		navigateTo(VIEWACTION_URL);
		// verifyIsInViewActionPage();
		// verify View Action Tab
		explicitlyWaitForElement("tab_View_Action", getTotalWaitTimeInSecs());
		verifyElementPresent("tab_View_Action");
		sendKeys("textField_Action_ViewUrn", transferAssociationURN);// fastActionURN
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title",
				getTotalWaitTimeInSecs());
		isInFastActionViewPage();
		clickAndWait("tab_Association");
		explicitlyWaitForElement("tab_Association", getTotalWaitTimeInSecs());
		verifyAssociationTabEnabled();
		verifyElementPresent("table_Action_AssociationTab_TargetURN_FirstRecord");
		if(isAlertPresent()){
			acceptAlertBox(driver);
		}
		// Create Subject type of association with nominal record and then
		// transfer
		raiseAndGetFastActionURN();
		// View Fast action
		viewAndGetFastActionURN();
		verifyFastActionTabEnabled();
		clickAndWait("tab_Association");
		verifyAssociationTabEnabled();
		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_SUBJECT);
		sendKeys("textField_Association_Add_TargetURN", targetSubjectURN);
		click("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Add_Association");
		String succMess_Subject = getText("successMsg_Action_Add_Association");
		String[] actUrn_Subject = succMess_Subject.split(" ");
		int size9 = actUrn_Subject.length;
		String Subject_urn = actUrn_Subject[size9 - 1];
		Report.info(Subject_urn);
		verifyTextCompare(targetSubjectURN, Subject_urn);
		explicitlyWaitForElement("checkBox_Action_Association_SecondRecord",
				getTotalWaitTimeInSecs());
		click("checkBox_Action_Association_SecondRecord");
		// transfer association to another action record
		click("icon_Action_Association_TransferAssociation");
		explicitlyWaitForElement(
				"text_Association_TransferAssociation_Dialogbox",
				getTotalWaitTimeInSecs());
		verifyElementPresent("text_Association_TransferAssociation_Dialogbox");
		verifyElementPresent("dropDown_Association_TransferAssociation_ReferenceIncident");
		verifyElementPresent("dropDown_Association_TransferAssociation_DefaultlistRecord");
		verifyElementPresent("textfield_Association_TransferAssociation_URN");
		explicitlyWaitForElement(
				"dropDown_Association_TransferAssociation_ReferenceIncident_Arrow",
				getTotalWaitTimeInSecs());
		clickAndWait("dropDown_Association_TransferAssociation_ReferenceIncident_Arrow");
		click("dropDown_Association_TransferAssociation_ReferenceIncident_Incident");
		explicitlyWaitForElement(
				"textfield_Association_TransferAssociation_URN",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Association_TransferAssociation_URN",
				transferAssociationURN);
		clickAndWait("button_Association_TransferAssociation_Submit");
		explicitlyWaitForElement("errorMsg_Action_Association_Transfer", getTotalWaitTimeInSecs());
		verifyElementPresent("errorMsg_Action_Association_Transfer");
		if(isAlertPresent()){
			acceptAlertBox(driver);
		}
		// Create manual potential match type of association and transfer
		// association
		raiseAndGetFastActionURN();
		// View Fast action
		viewAndGetFastActionURN();
		verifyFastActionTabEnabled();
		clickAndWait("tab_Association");
		verifyAssociationTabEnabled();
		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_MANUALPOTENTIALMATCH);
		sendKeys("textField_Association_Add_TargetURN",
				targetManualpotentialmatchURN);
		click("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Add_Association");
		String succMess_MPM = getText("successMsg_Action_Add_Association");
		String[] actUrn_MPM = succMess_MPM.split(" ");
		int size10 = actUrn_MPM.length;
		String MPM_urn = actUrn_MPM[size10 - 1];
		Report.info(MPM_urn);
		verifyTextCompare(targetManualpotentialmatchURN, MPM_urn);
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getTotalWaitTimeInSecs());
		click("checkBox_Action_Association_FirstRecord");
		// transfer association to another action record
		click("icon_Action_Association_TransferAssociation");
		explicitlyWaitForElement(
				"text_Association_TransferAssociation_Dialogbox",
				getTotalWaitTimeInSecs());
		verifyElementPresent("text_Association_TransferAssociation_Dialogbox");
		verifyElementPresent("dropDown_Association_TransferAssociation_ReferenceIncident");
		verifyElementPresent("dropDown_Association_TransferAssociation_DefaultlistRecord");
		verifyElementPresent("textfield_Association_TransferAssociation_URN");
		explicitlyWaitForElement(
				"dropDown_Association_TransferAssociation_ReferenceIncident_Arrow",
				getTotalWaitTimeInSecs());
		clickAndWait("dropDown_Association_TransferAssociation_ReferenceIncident_Arrow");
		
		click("dropDown_Association_TransferAssociation_ReferenceIncident_Incident");
		explicitlyWaitForElement(
				"textfield_Association_TransferAssociation_URN",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Association_TransferAssociation_URN",
				transferAssociationURN);
		clickAndWait("button_Association_TransferAssociation_Submit");
		explicitlyWaitForElement("errorMsg_Action_Association_Transfer", getTotalWaitTimeInSecs());
		verifyElementPresent("errorMsg_Action_Association_Transfer");

		// raise fast action and move it to allocated state and add described
		// type association and transfer
		raiseAndGetFastActionURN();
		verifyText(FOR_ALLOCATION, "label_Action_View_State");
		// getList and verify state in dropdown
		getAllListItemAndVerify(ALLOCATED, "dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",
				getTotalWaitTimeInSecs());
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficer",
				getTotalWaitTimeInSecs());
		verifyFastActionFieldsInSingleQueueMove();
		String text = "";
		verifyTextCompare(text, getTextFieldText());
		verifyText(ALLOCATED, "label_Action_Create_State");
		verifyAssociatedDocuments();
		verifyAllocatedOfficer();
		verifyReceiversInstructions();
		click("checkBox_Action_Themes_Item1");
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		click("button_Action_ChangeState_CreateOfficer");
		explicitlyWaitForElement("textfield_Action_CreateOfficer_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_Forename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_Fast_Create");

		// Entering officer name
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficer",
				getTotalWaitTimeInSecs());
		insertTextInAllocatedOfficerTextField("textfield_Action_FastAction_QueueAllocatedOfficer",CREATE_OFFICER_SURNAME);
		String officerName = "SurName";
		selectOptionWithText(officerName, "textfield_Action_StateMove_AllocatedOfficerName");
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyText(ALLOCATED, "label_Action_View_State");
		click("tab_Association");
		verifyAssociationTabEnabled();
		click("icon_Association_Add");
		if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
			click("icon_Association_Add");
		} 
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type",
				ASSOCIATION_TYPE_DESCRIBED);
		sendKeys("textField_Association_Add_TargetURN", targetDescribedURN);
		sendKeys("textField_Action_Association_Add_Description",
				"Dev smoke test - Described association");
		click("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Add_Association");
		String succMess_Desallocated = getText("successMsg_Action_Add_Association");
		String[] actUrn_Desallocated = succMess_Desallocated.split(" ");
		int size11 = actUrn_Desallocated.length;
		String desallocated_urn = actUrn_Desallocated[size11 - 1];
		Report.info(desallocated_urn);
		verifyTextCompare(targetDescribedURN, desallocated_urn);

		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getTotalWaitTimeInSecs());
		click("checkBox_Action_Association_FirstRecord");
		// transfer association to another action record
		click("icon_Action_Association_TransferAssociation");
		explicitlyWaitForElement(
				"text_Association_TransferAssociation_Dialogbox",
				getTotalWaitTimeInSecs());
		verifyElementPresent("text_Association_TransferAssociation_Dialogbox");
		verifyElementPresent("dropDown_Association_TransferAssociation_ReferenceIncident");
		verifyElementPresent("dropDown_Association_TransferAssociation_DefaultlistRecord");
		verifyElementPresent("textfield_Association_TransferAssociation_URN");

		clickAndWait("dropDown_Association_TransferAssociation_ReferenceIncident_Arrow");
		click("dropDown_Association_TransferAssociation_ReferenceIncident_Incident");
		explicitlyWaitForElement("textfield_Association_TransferAssociation_URN",
				getTotalWaitTimeInSecs());
		/*sendKeys("textfield_Association_TransferAssociation_URN",
				transferAssociationURN+Keys.TAB+Keys.TAB+Keys.ENTER);*/
		sendKeys("textfield_Association_TransferAssociation_URN",
				transferAssociationURN);
		explicitlyWaitForElement("button_Association_TransferAssociation_Submit2",
				getTotalWaitTimeInSecs());
		clickAndWait("button_Association_TransferAssociation_Submit2");
		explicitlyWaitForElement("successMsg_Action_Add_Association",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Add_Association");
		// view the action and verify that it is transfered
		clickAndWait("tab_Association");
		explicitlyWaitForElement("tab_Association", getTotalWaitTimeInSecs());
		verifyAssociationTabEnabled();
		verifyElementPresent("table_Action_AssociationTab_TargetURN_FirstRecord");
		explicitlyWaitForElement("icon_Action_Association_TransferAssociation", getTotalWaitTimeInSecs());
		// donot select any record and click on transfer association button
		click("icon_Action_Association_TransferAssociation");
		verifyText(TRANSFER_POPUP_ERROR, "label_Marker_Transfer_Popup");
		click("button_Marker_ErrorPopup_OK");
		
		endLine(method);

	}
	/* Test case 9450:008. Audit Log Entry- Fast Action */
	@Test(priority = 9)
	public void fastAction_AuditLogEntry(Method method) throws Exception {
		startLine(method);

		AuditLogSearch au = new AuditLogSearch(driver);
		// search audit and verify result
		au.auditLogSearchByActivity(SEARCH_FAST_ACTION);
		verifyAuditResult(SEARCH_FAST_ACTION);

		au.auditLogSearchByActivity(VIEW_FAST_ACTION);
		verifyAuditResult(VIEW_FAST_ACTION);

		au.auditLogSearchByActivity(CREATE_ASSOCIATION);
		verifyAuditResult(CREATE_ASSOCIATION);

		au.auditLogSearchByActivity(DELETE_ASSOCIATION);
		verifyAuditResult(DELETE_ASSOCIATION);

		au.auditLogSearchByActivity(ADD_ATTACHEMENT);
		verifyAuditResult(ADD_ATTACHEMENT);

		au.auditLogSearchByActivity(REMOVE_ATTACHEMENT);
		verifyAuditResult(REMOVE_ATTACHEMENT);
		
		au.auditLogSearchByActivity(UPDTAE_ASSOCIATION);
		verifyAuditResult(UPDTAE_ASSOCIATION);

	/*	au.auditLogSearchByActivity(UPDATE_FAST_ACTION);
		verifyAuditResult(UPDATE_FAST_ACTION);
*/
		endLine(method);
	}
	public void verifyAuditResult(String activityDesc) throws Exception {
		verifyText(usernameUpperCase,
				"table_AuditLog_AuditRecord_FirstRecord_Username");
		verifyTextContains(incidentIdFull,
				"table_AuditLog_AuditRecord_FirstRecord_Incident");
		verifyTextContains(activityDesc,
				"table_AuditLog_AuditRecord_FirstRecord_Activity");
	}


	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
