package Modules.Actions;

import java.lang.reflect.Method;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.SeleniumBaseClass;

public class SearchResultTab extends SeleniumBaseClass {
	private static final String FAST_ACTION_TEXT = "Fast Action";
	private static String  fastActionUrn="";
	private static String  fastActionUrnExisList="";
	String RAISE_FASTACTION_URL = "/group/holmes/raise-fast-action";
	String SEARCH_ACTION_URL = "/group/holmes/search-for-actions";
	

	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);

	}

	@BeforeTest
	public void beforeTest() throws Exception {
		loginInIEAndSelectIncident();
	}

	@Test(priority = 1)
	public void FastActionSearchAndView(Method method) throws Exception {
		startLine(method);
		navigateTo(RAISE_FASTACTION_URL);
		sendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		clickAndWait("button_Action_Raise");
		String createMessage=getText("successMsg_Action_Create");
		String [] actUrn= createMessage.split(" ");
	    fastActionUrn= actUrn[2];
	    System.out.println(fastActionUrn+"::::::");
		navigateTo(SEARCH_ACTION_URL);
		//SendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		sendKeys("textField_Action_SearchAction_URN", fastActionUrn);
		clickAndWait("button_Action_Search");
		explicitlyWaitForElement("table_Action_SearchResults",
				getTotalWaitTimeInSecs());
		verifyText(FAST_ACTION_TEXT, "table_Action_SearchResults_Title");
		clickAndWait("checkBox_Action_Search_FirstRecord");
		click("link_Action_Search_View_Entity");
		getHandleToWindow("Soft Entity Portlet");
	//	explicitlyWaitForElement("button_Action_Search_View_Update",
	//			getTotalWaitTimeInSecs());
		getHandleToWindow("Soft Entity Portlet").close();
		getHandleToWindow("Action Dashboard");
		endLine(method);

	}

	@Test(priority = 2)
	public void FastActionSearchAnd_AddToDefaultList(Method method)
			throws Exception {
		startLine(method);
		navigateTo(SEARCH_ACTION_URL);
		//SendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		sendKeys("textField_Action_SearchAction_URN", fastActionUrn);
		clickAndWait("button_Action_Search");
		explicitlyWaitForElement("table_Action_SearchResults",
				getTotalWaitTimeInSecs());
		verifyText(FAST_ACTION_TEXT, "table_Action_SearchResults_Title");
		clickAndWait("checkBox_Action_Search_FirstRecord");
		click("link_Action_Search_DefaultList");
		explicitlyWaitForElement("text_Action_Search_Add_DefaultList",
				getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Search_AddList_Ok");
		endLine(method);

	}

	@Test(priority = 3)
	public void FastActionSearchAnd_AddToExistList(Method method)
			throws Exception {
		startLine(method);
		navigateTo(SEARCH_ACTION_URL);
		//SendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		sendKeys("textField_Action_SearchAction_URN", fastActionUrn);
		clickAndWait("button_Action_Search");
		explicitlyWaitForElement("table_Action_SearchResults",
				getTotalWaitTimeInSecs());
		verifyText(FAST_ACTION_TEXT, "table_Action_SearchResults_Title");
		clickAndWait("checkBox_Action_Search_FirstRecord");
		click("link_Action_Search_ExistList");
		clickAndWait("button_Action_Search_AddList_Ok");
		getHandleToWindow("List Maintenance");
		explicitlyWaitForElement("textField_ListManagement_ListName",
				getTotalWaitTimeInSecs());
		getHandleToWindow("List Maintenance").close();
		getHandleToWindow("Action Dashboard");
		endLine(method);
	}

	@Test(priority = 4)
	public void FastActionSearchAnd_AddToFavouriteList(Method method)
			throws Exception {
		startLine(method);
		navigateTo(RAISE_FASTACTION_URL);
		sendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		clickAndWait("button_Action_Raise");
		String createMessage=getText("successMsg_Action_Create");
		String [] actUrn= createMessage.split(" ");
		fastActionUrnExisList= actUrn[2];
	    System.out.println(fastActionUrnExisList+"::::::");
		navigateTo(SEARCH_ACTION_URL);
		//SendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		sendKeys("textField_Action_SearchAction_URN", fastActionUrnExisList);
		clickAndWait("button_Action_Search");
		explicitlyWaitForElement("table_Action_SearchResults",
				getTotalWaitTimeInSecs());
		verifyText(FAST_ACTION_TEXT, "table_Action_SearchResults_Title");
		clickAndWait("checkBox_Action_Search_FirstRecord");
		click("link_Action_Search_FavouriteList");
		explicitlyWaitForElement("text_Action_Search_Add_FavList",
				getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Search_AddList_Ok");
		endLine(method);
		
	}
	
	@Test(priority = 5)
	public void FastActionSearchAnd_AddToVisualization(Method method)
			throws Exception {
		startLine(method);
		navigateTo(SEARCH_ACTION_URL);
		//SendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		sendKeys("textField_Action_SearchAction_URN", fastActionUrn);
		clickAndWait("button_Action_Search");
		explicitlyWaitForElement("table_Action_SearchResults",
				getTotalWaitTimeInSecs());
		verifyText(FAST_ACTION_TEXT, "table_Action_SearchResults_Title");
		clickAndWait("checkBox_Action_Search_FirstRecord");
		click("link_Action_Search_Visualization");
		explicitlyWaitForElement("text_Action_Search_Add_Visualization",
				getTotalWaitTimeInSecs());
		click("button_Action_Search_AddList_Ok");
		getHandleToWindow("Visualisation Portlet");
		//explicitlyWaitForElement("textField_ListManagement_ListName",
				//getTotalWaitTimeInSecs());
		getHandleToWindow("Visualisation Portlet").close();
		getHandleToWindow("Action Dashboard");
		endLine(method);
		
	}
	
	@Test(priority = 6)
	public void FastActionSearchAnd_UpdateRecorsWithoutSelectingRecord(Method method)
			throws Exception {
		startLine(method);
		navigateTo(SEARCH_ACTION_URL);
		//SendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		sendKeys("textField_Action_SearchAction_URN", fastActionUrn);
		clickAndWait("button_Action_Search");
		explicitlyWaitForElement("table_Action_SearchResults",
				getTotalWaitTimeInSecs());
		verifyText(FAST_ACTION_TEXT, "table_Action_SearchResults_Title");
		//ClickAndWait("checkBox_Action_Search_FirstRecord");
		click("link_Action_Search_UpdateRecords");
		explicitlyWaitForElement("text_Action_Search_Add_UpdateRecord",
				getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Search_AddList_Ok");
		endLine(method);
		
	}
	 @Test(priority = 7)
	public void FastActionSearchAnd_DefineReorder(Method method)
			throws Exception {
		startLine(method);
		navigateTo(SEARCH_ACTION_URL);
		//SendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		sendKeys("textField_Action_SearchAction_URN", fastActionUrn);
		clickAndWait("button_Action_Search");
		explicitlyWaitForElement("table_Action_SearchResults",
				getTotalWaitTimeInSecs());
		verifyText(FAST_ACTION_TEXT, "table_Action_SearchResults_Title");
		//ClickAndWait("checkBox_Action_Search_FirstRecord");
		click("link_Action_Search_DefineReorder");
		//explicitlyWaitForElement("text_Action_Search_Add_UpdateRecord",
		//		getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Search_DifineReorder_Ok");
		endLine(method);
	}
}
