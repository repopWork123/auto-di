package Modules.Actions;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.SeleniumBaseClass;

public class Search extends SeleniumBaseClass {
	private static final String FAST_ACTION_TEXT = "Fast Action";
	String RAISE_FASTACTION_URL = "/group/holmes/raise-fast-action";
	String SEARCH_ACTION_URL = "/group/holmes/search-for-actions";

	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);

	}

	@BeforeTest
	public void beforeTest() throws Exception {
		loginInIEAndSelectIncident();
	}

	//@Test(priority = 1)
	public void Create_andSearchFastAction(Method method) throws Exception {
		startLine(method);
		navigateTo(RAISE_FASTACTION_URL);
		sendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		clickAndWait("button_Action_Raise");
		navigateTo(SEARCH_ACTION_URL);
		sendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		clickAndWait("button_Action_Search");
		explicitlyWaitForElement("table_Action_SearchResults",
				getTotalWaitTimeInSecs());
		String searchTitle = getText("table_Action_SearchResults_Title");
		System.out.println(searchTitle + ":::::::::::::::::::::::::");
		String searchURN = getTextboxValue("table_Action_SearchResults_Title");
		verifyText(FAST_ACTION_TEXT, "table_Action_SearchResults_Title");
		endLine(method);
	}
	//@Test(priority = 2)
	public void SearchFastAction_OR(Method method) throws Exception {
		startLine(method);
		navigateTo(SEARCH_ACTION_URL);
		sendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		sendKeys("textField_Action_SearchAction_URN", "A1");
		click("textField_Action_Search_OR");
		clickAndWait("button_Action_Search");
		explicitlyWaitForElement("table_Action_SearchResults",
				getTotalWaitTimeInSecs());
		endLine(method);
	}
	//@Test(priority = 3)
	public void SearchFastAction_NOT(Method method) throws Exception {
		startLine(method);
		navigateTo(SEARCH_ACTION_URL);
		sendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		sendKeys("textField_Action_SearchAction_URN", "A1");
		click("textField_Action_Search_NOT");
		clickAndWait("button_Action_Search");
		explicitlyWaitForElement("table_Action_SearchResults",
				getTotalWaitTimeInSecs());
		navigateTo(SEARCH_ACTION_URL);
		sendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		sendKeys("textField_Action_SearchAction_URN", "A1");
		clickAndWait("button_Action_Search");
		explicitlyWaitForElement("table_Action_SearchResults",
				getTotalWaitTimeInSecs());
		endLine(method);
	}
	@Test(priority = 4)
	public void SearchFastActionReset(Method method) throws Exception {
		startLine(method);
		navigateTo(SEARCH_ACTION_URL);
		sendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		sendKeys("textField_Action_SearchAction_URN", "A1");
		clickAndWait("button_Action_Search_Reset");
		String searchTtle = null;
		System.out.println(searchTtle+":::");
		verifyText(searchTtle, "textField_Action_ActionText");
		endLine(method);
	}
	@Test(priority = 5)
	public void SearchFastAction_ByState(Method method) throws Exception {
		startLine(method);
		navigateTo(SEARCH_ACTION_URL);
		click("dropDown_Action_Search_State");
		click("checkBox_Action_Search_ForAllocation");
		clickAndWait("button_Action_Search");
		explicitlyWaitForElement("table_Action_SearchResults",
				getTotalWaitTimeInSecs());
		String searchStates = getTextboxValue("table_Action_SearchResults_State");
		System.out.println(searchStates+":::");
		verifyText("For Allocation", "table_Action_SearchResults_State");
		endLine(method);
	}
	//@Test(priority = 6)
	public void Create_andSearchFastAction_WildCard(Method method)
			throws Exception {
		startLine(method);
		navigateTo(RAISE_FASTACTION_URL);
		sendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		clickAndWait("button_Action_Raise");
		navigateTo(SEARCH_ACTION_URL);
		sendKeys("textField_Action_ActionText", "F%");
		clickAndWait("button_Action_Search");
		explicitlyWaitForElement("table_Action_SearchResults",
				getTotalWaitTimeInSecs());
		String searchTitles = getTextboxValue("table_Action_SearchResults_Title");
		verifyText(FAST_ACTION_TEXT, "table_Action_SearchResults_Title");

		endLine(method);
		driver.close();
	}

}
