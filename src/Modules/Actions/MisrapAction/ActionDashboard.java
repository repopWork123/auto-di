package Modules.Actions.MisrapAction;

import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import ReusableUtilities.Dataprovider;
import ReusableUtilities.Report;

public class ActionDashboard extends MirsapAction {
	private static final String ActionTitle = "Action - Auto Test for Raise Action update";
	protected static final String ADD_TO_NEW_EXISTING_LIST = "List Maintenance - Liferay";
	protected static final String Action_Dashboard_Title="Action Dashboard";
	protected static final String MIRSAP_CLASS1 = "SCENE";
	protected static final String SUB_CLASS1 = "SCENESUBCLASS1";
	protected static final String SUB_CLASS2 = "SCENESUBCLASS2";
	//private String targetLinkURN;
	private String state = "For Allocation";


	@BeforeTest
	public void beforeTest() throws Exception {
		loginInIEAndSelectIncident();
		//loginAndSelectIncident();
	}

	@DataProvider(name = "mirsapactiontabs")
	public String[][] dataProvider() {

		String[][] testData = Dataprovider.readExcelData("Actions",
				testDataFilePath, "MirsapActionTabs");
		return testData;
	}

	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}

	//default constructor
	public ActionDashboard() {
		softAssert = new SoftAssert();
	}

	/*
	 * Test Case 26509: 001. Action Dashboard Search
	 */
	@Test(priority = 1)
	public void actionDashboardSearch(Method method) throws Exception{		

		startLine(method);
		//step 1 - navigate to dashboard and verify fields/icons
		navigateTo(ACTION_DASHBOARD);
		verifyActionDashboard();
		performActionDashboardSearch();
		//step 2 - perform search for all types
		//perform search for action type
		navigateTo(ACTION_DASHBOARD);
		selectByVisibleText("dropDown_Action_ActionDashboard_Search_Type","Action");
		performActionDashboardSearch();
		verifyActionDashboardSearchActionType("Action");
		//perform search for fast action type
		navigateTo(ACTION_DASHBOARD);
		selectByVisibleText("dropDown_Action_ActionDashboard_Search_Type","Fast Action");
		performActionDashboardSearch();
		verifyActionDashboardSearchActionType("Fast Action");
		//raise mirsap action and capture URN
		String mirsapURN = raiseAndGetMirsapActionURN();
		//Step 3- return to search tab and perform "AND" search
		navigateTo(ACTION_DASHBOARD);
		selectByVisibleText("dropDown_Action_ActionDashboard_Search_Type","Action");
		sendKeys("textField_Action_Dashboard_URN",mirsapURN);
		sendKeys("textField_Action_Dashboard_Title",ActionTitle);
		performActionDashboardSearch();
		verifyActionDashboardSearchResultUrnTitle(mirsapURN, ActionTitle);
		navigateTo(ACTION_DASHBOARD);
		selectByVisibleText("dropDown_Action_ActionDashboard_Search_Type","Action");
		sendKeys("textField_Action_Dashboard_URN",mirsapURN);
		sendKeys("textField_Action_Dashboard_Title",ActionTitle);
		clickAndWait("radioButton_Or_Search");
		performActionDashboardSearch();
		verifyActionDashboardSearchResultUrnTitle(null, ActionTitle);
		//perform NOT search
		navigateTo(ACTION_DASHBOARD);
		click("button_Action_ActionDashboard_Search_State");
		click("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		click("radioButton_Not_Search");
		performActionDashboardSearch();
		verifyActionDashboardSearchNot(state);
		//perform wildcard search
		navigateTo(ACTION_DASHBOARD);
		sendKeys("textField_Action_Dashboard_Title","Action%");
		performActionDashboardSearch();

		//reset button
		clickAndWait("link_ActionDashBoard_Search");
		click("button_Action_Search_Reset");
		verifyText("","textField_Action_Dashboard_Title");		
		endLine(method);		
	}

	/*
	 * Test Case 30615: 002. Action Dashboard Search Result Tab and icons
	 */
	@Test(priority = 2)
	public void actionDashboardSearchResultsTabIcons(Method method) throws Exception{
		startLine(method);
		//step 1 navigate to action dashboard
		navigateTo(ACTION_DASHBOARD);
		verifyActionDashboard();
		selectByVisibleText("dropDown_Action_ActionDashboard_Search_Type","Action");
		//step 2 click search
		click("button_Action_Search");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());

		//Verify elements present on Action search result page
		Report.info("");
		verifyActionSearchIcons();

		// Create FastAction from ActionSearchResult- Create New Entity Icon
		Report.info("");
		createNewEntityIcon();

		//View FastAction from View the selected entity icon
		Report.info("");
		viewTheSelectedEntityIcon();

		//View FastAction by double click
		Report.info("View FastAction by double click");
		viewFastActionByDoubleClick();

		//Refresh
		Report.info("Refresh");
		refreshTheGrid();

		//AutoRefresh
		Report.info("AutoRefresh");
		autoRefresh();


		//Add to Favourite List
		Report.info("Add to Favourite List");
		addtoFavouriteList();

		//Add to Visualisation List
		Report.info("Add to Visualization List");
		addtoVisualizationList();

		//Add to Default List
		Report.info("Add to Default List");
		addtoDefaultList();

		//Add to New/Existing List
		Report.info("Addto New/Existing List");
		addtoNeworExistingList();

		//Select All columns
		Report.info("Select All columns");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		defineorReorderColumns();
		

		//Bulk Update
		//TODO- SHALINI - ( Values need to verified )
		Report.info("Bulk Update");
		bulkUpdate();

		//Export all to CSV
		/*Report.info("Export all Results to CSV");
		click("icon_Action_SearchResult_ExportAll");
		click("button_Export_Warning_Yes");*/

		//Change Page paignation
		/*selectByVisibleText("dropDown_Action_Page_Paignation","100");		*/

		endLine(method);
	}

	public void verifyActionDashboard() throws Exception
	{
		verifyElementPresent("dropDown_Incident");
		//verifyElementPresent("dropDown_LinkedSeries");
		verifyElementPresent("dropDown_Action_ActionDashboard_Search_Type");
		verifyElementPresent("textField_Action_Dashboard_URN");
		verifyElementPresent("textField_Action_Dashboard_Title");
		verifyElementPresent("dropDown_Action_Mirsap_Priority");
		verifyElementPresent("textField_Officer");
		verifyElementPresent("dropDown_Action_Dashboard_Class");
		verifyElementPresent("dropDown_Action_Dashboard_subClass1");
		verifyElementPresent("dropDown_Action_Dashboard_subClass2");
		verifyElementPresent("button_Action_ActionDashboard_Search_State");
		verifyElementPresent("radioButton_And_Search");
		verifyElementPresent("radioButton_Or_Search");
		verifyElementPresent("radioButton_Not_Search");
	}

	public void verifyActionSearchIcons() throws Exception
	{
		verifyElementPresent("icon_Action_SearchResult_CreateNew");
		verifyElementPresent("icon_Action_SearchResult_View");
		verifyElementPresent("icon_Action_SearchResult_refresh");
		verifyElementPresent("icon_Action_SearchResult_edit");
		verifyElementPresent("icon_Action_SearchResult_bulkMove");
		verifyElementPresent("icon_Action_SearchResult_addToFav");
		verifyElementPresent("icon_Action_SearchResult_addToDef");
		verifyElementPresent("icon_Action_SearchResult_addToNew");
		verifyElementPresent("icon_Action_SearchResult_Visaulise");
		verifyElementPresent("icon_Action_SearchResult_I2");
		verifyElementPresent("icon_Action_SearchResult_ExportSelected");
		verifyElementPresent("icon_Action_SearchResult_Map");
		verifyElementPresent("icon_Action_SearchResult_ReorderColumns");
		verifyElementPresent("icon_Action_SearchResult_ExportAll");
	}	

	public void verifyRaiseMirsapActionFields() throws Exception
	{
		explicitlyWaitForElement("button_Action_Raise",getTotalWaitTimeInSecs());
		verifyElementPresent("dropDown_Action_Create_Incident");
		verifyElementPresent("dropDown_Action_Template");
		verifyElementPresent("checkBox_Action_BulkRecord");
		verifyElementPresent("dropDown_Action_Create_ProtectiveMarking");
		verifyElementPresent("textField_Action_Mirsap_Text");
		verifyElementPresent("textField_Action_Title");
		verifyElementPresent("label_Action_Create_State");
		verifyElementPresent("");
		verifyElementPresent("");
		verifyElementPresent("");
	}	

	// Create FastAction from ActionSearchResult- Create New Entity Icon
	public void createNewEntityIcon() throws Exception{
		//step 3 create new entity
		click("icon_Action_SearchResult_CreateNew");
		waitForJQuery(driver);
		explicitlyWaitForElement("dropDown_Action_Entity_Select",getTotalWaitTimeInSecs());
		//step 4 select entity as action
		selectByVisibleText("dropDown_Action_Entity_Select","Action");
		click("button_Action_EntityToCreate_Select");
		//step 5 populate manadatory fields and click create
		String currentParentWindowHandle = getCurrentWindowHandle();
		// get Handle to Create Fast Action- child Window
		ArrayList<String> parentWinHandler1 = new ArrayList<String>();
		parentWinHandler1.add(currentParentWindowHandle);
		WebDriver childWindDriver = clickAndWaitForChildWindow("Action", parentWinHandler1, null, 1, 2);

		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		// verify Mandatory fields
		verifyMandatoryFieldsInMirsapAction();
		//insert text into mandatory fields
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		selectByVisibleText("dropDown_Action_Template_Class", MIRSAP_CLASS);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		// Click("textField_Action_Mirsap_SourceDocument");
		// SendKeys("textField_Action_Mirsap_Urn", ACTION_DOCUMENT_URN);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		//link_Action_Create_Theme
		click("link_Action_Create_Theme");
		click("checkBox_Action_Themes");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",getTotalWaitTimeInSecs());
		driver = closePopupWindowAndNavigateToParentWindowHandle("Action", currentParentWindowHandle);
		parentWinHandler1.remove(currentParentWindowHandle);
	}

	//View FastAction from View the selected entity icon
	public void viewTheSelectedEntityIcon()throws Exception{
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		//select the first record
		checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");

		/*explicitlyWaitForElement("icon_Action_Searchresult_View",getTotalWaitTimeInSecs());*/
		/*sleep(3000);*/
		String currentParentWindowHandle = getCurrentWindowHandle();
		// get Handle to Create Fast Action- child Window
		ArrayList<String> parentWinHandler1 = new ArrayList<String>();
		parentWinHandler1.add(currentParentWindowHandle);
		int winCount = getWindowCount();
		WebDriver childWindDriver = clickAndWaitForChildWindow("Action", parentWinHandler1, "icon_Action_Searchresult_View", 1, 2);
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		//Verify currently incident id is selected
		verifyTextContains(incidentIdFull, "label_Action_View_Page_Title");
		driver = closePopupWindowAndNavigateToParentWindowHandle("Action", currentParentWindowHandle);
	}

	//View FastAction by double click
	public void viewFastActionByDoubleClick()throws Exception{

		String currentParentWindowHandle = getCurrentWindowHandle();
		// get Handle to Create Fast Action- child Window
		ArrayList<String> parentWinHandler1 = new ArrayList<String>();
		parentWinHandler1.add(currentParentWindowHandle);
		int currentWinCount = getWindowCount();
		WebDriver childWindDriver = doubleClickAndWaitForChildWindow("Action", parentWinHandler1, "checkBox_Action_SearchResulttab_Firstrow", currentWinCount, currentWinCount+1);
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		//Verify currently incident id is selected
		verifyTextContains(incidentIdFull, "label_Action_View_Page_Title");
		driver = closePopupWindowAndNavigateToParentWindowHandle("Action", currentParentWindowHandle);
	}

	public void verifyActionDashboardSearchResultUrnTitle(String actionUrn, String ActionTitle)throws Exception {
		if(actionUrn!=null)
		{
			verifyText(actionUrn, "table_Action_Dashboard_SearchResults_ActionUrn");
		}				
		List<WebElement> tableHeaderForSearchResults = explicitlyWaitAndGetWebElements("table_Action_Dashboard_SearchResults_TitleNameHeader", getTotalWaitTimeInSecs());
		int positonOfActionInTable = tableHeaderForSearchResults.size() + 1;
		String actionTitleInSearchresultXpath = "//table[@id='searchResultsTable']/tbody/tr[2]/td[" + positonOfActionInTable + "]";
		String actionTitleInSearchresult = driver.findElement(By.xpath(actionTitleInSearchresultXpath)).getText();
		verifyTextCompare(ActionTitle,actionTitleInSearchresult);
	}

	public void verifyActionDashboardSearchNot(String actionState) throws Exception
	{
		List<WebElement> tableHeaderForSearchResults = explicitlyWaitAndGetWebElements("table_Action_Dashboard_SearchResults_StateNameHeader", getTotalWaitTimeInSecs());
		int positonOfActionInTable = tableHeaderForSearchResults.size() + 1;
		String actionStateInSearchresultXpath = "//table[@id='searchResultsTable']/tbody/tr[2]/td[" + positonOfActionInTable + "]";
		String actionStateInSearchresult = driver.findElement(By.xpath(actionStateInSearchresultXpath)).getText();
		verifyTextOnlyNotEquals(actionState, actionStateInSearchresult);
	}

	public void verifyActionDashboardSearchActionType(String action) throws Exception
	{
		List<WebElement> tableHeaderForSearchResults = explicitlyWaitAndGetWebElements("table_Action_Dashboard_SearchResults_ActionTypeHeader", getTotalWaitTimeInSecs());
		int positonOfActionInTable = tableHeaderForSearchResults.size() + 1;
		String actionStateInSearchresultXpath = "//table[@id='searchResultsTable']/tbody/tr[2]/td[" + positonOfActionInTable + "]";
		String actionStateInSearchresult = driver.findElement(By.xpath(actionStateInSearchresultXpath)).getText();
		verifyTextCompare(action, actionStateInSearchresult);
	}

	//Refresh and check Pageinfo- Total Record count before refresh and after refresh
	public void refreshTheGrid()throws Exception{
		sleep(3000);
		//wait for pageinfo div and store the total action record count in total_ActionRecord_Count_BeforeRefresh
		explicitlyWaitForElement("label_Action_PageInfo", getTotalWaitTimeInSecs());
		String paging_Info_BeforeRefresh = getText("label_Action_PageInfo");
		String[] paginginfo_Count_BeforeRefresh=paging_Info_BeforeRefresh.split(" ");
		int size1=paginginfo_Count_BeforeRefresh.length;
		String total_ActionRecord_Count_BeforeRefresh= paginginfo_Count_BeforeRefresh[size1-1];
		int totalActionRecordCountBeforeRefresh = Integer.parseInt(total_ActionRecord_Count_BeforeRefresh);
		System.out.println("total_ActionRecord_Count_BeforeRefresh:" + total_ActionRecord_Count_BeforeRefresh);

		// Create FastAction from ActionSearchResult- Create New Entity Icon
		click("icon_Action_Searchresult_CreateNew");
		waitForJQuery(driver);
		explicitlyWaitForElement("dropDown_Action_Entity_Select", getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Action_Entity_Select","Action");
		String currentParentWindowHandle = getCurrentWindowHandle();
		// get Handle to Create Fast Action- child Window
		ArrayList<String> parentWinHandler1 = new ArrayList<String>();
		parentWinHandler1.add(currentParentWindowHandle);
		WebDriver childWindDriver = clickAndWaitForChildWindow("Action", parentWinHandler1, "button_Action_EntityToCreate_Select", 1, 2);

		explicitlyWaitForElement("button_Action_Raise",getTotalWaitTimeInSecs());
		//Pass data only to mandatory fields
		verifyElementPresent("button_Action_Raise");
		// verify Mandatory fields
		verifyMandatoryFieldsInMirsapAction();
		//insert text into mandatory fields
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		selectByVisibleText("dropDown_Action_Template_Class", MIRSAP_CLASS);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		// Click("textField_Action_Mirsap_SourceDocument");
		// SendKeys("textField_Action_Mirsap_Urn", ACTION_DOCUMENT_URN);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		//link_Action_Create_Theme
		click("link_Action_Create_Theme");
		click("checkBox_Action_Themes");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",getTotalWaitTimeInSecs());
		driver = closePopupWindowAndNavigateToParentWindowHandle("Action", currentParentWindowHandle);				

		//After Raise New Fast Action click on refresh icon
		click("icon_Action_Searchresult_Refresh");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		//wait for pageinfo div and store the total action record count in total_ActionRecord_Count_AfterRefresh
		explicitlyWaitForElement("label_Action_PageInfo", getTotalWaitTimeInSecs());
		String paging_Info_AfterRefresh = getText("label_Action_PageInfo");
		String[] paginginfo_Count_AfterRefresh=paging_Info_AfterRefresh.split(" ");
		int size2=paginginfo_Count_AfterRefresh.length;
		String total_ActionRecord_Count_AfterRefresh= paginginfo_Count_AfterRefresh[size1-1];
		int totalActionRecordCountAfterRefresh = Integer.parseInt(total_ActionRecord_Count_AfterRefresh);
		System.out.println("total_ActionRecord_Count_AfterRefresh:"+total_ActionRecord_Count_AfterRefresh);
		//verifyTextCompare(String.valueOf(Integer.parseInt(total_ActionRecord_Count_BeforeRefresh)+1), total_ActionRecord_Count_AfterRefresh);

		if (totalActionRecordCountBeforeRefresh < totalActionRecordCountAfterRefresh){
			Report.info("Refresh increments the total number of record count in page info");
		}
		else{
			Report.warn("Refresh has not incremented the total number of record count in page info");
		}



	}


	//AutoRefresh set to 1 min and check Pageinfo- Total Record count before refresh and after refresh
	public void autoRefresh()throws Exception{
		//wait for pageinfo div and store the total action record count in total_ActionRecord_Count_BeforeRefresh
		explicitlyWaitForElement("label_Action_PageInfo", getTotalWaitTimeInSecs());
		String paging_Info_BeforeRefresh = getText("label_Action_PageInfo");
		String[] paginginfo_Count_BeforeRefresh=paging_Info_BeforeRefresh.split(" ");
		int size1=paginginfo_Count_BeforeRefresh.length;
		String total_ActionRecord_Count_BeforeRefresh= paginginfo_Count_BeforeRefresh[size1-1];
		System.out.println(total_ActionRecord_Count_BeforeRefresh);
		int totalActionRecordCountBeforeRefresh = Integer.parseInt(total_ActionRecord_Count_BeforeRefresh);
		// Create FastAction from ActionSearchResult- Create New Entity Icon
		click("icon_Action_Searchresult_CreateNew");
		waitForJQuery(driver);
		explicitlyWaitForElement("dropDown_Action_Entity_Select",getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Action_Entity_Select","Action");
		String currentParentWindowHandle = getCurrentWindowHandle();
		// get Handle to Create Fast Action- child Window
		ArrayList<String> parentWinHandler1 = new ArrayList<String>();
		parentWinHandler1.add(currentParentWindowHandle);
		WebDriver childWindDriver = clickAndWaitForChildWindow("Action", parentWinHandler1, "button_Action_EntityToCreate_Select", 1, 2);
		explicitlyWaitForElement("button_Action_Raise",getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		// verify Mandatory fields
		verifyMandatoryFieldsInMirsapAction();
		//insert text into mandatory fields
		click("checkBox_Action_BulkRecord");
		sendKeys("textField_Action_BulkRecordCount","5");
		sendKeys("textField_Action_Title",ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		selectByVisibleText("dropDown_Action_Template_Class", MIRSAP_CLASS);
		sendKeys("textField_Action_OriginatingDetails",ACTION_ORIGINATING_DETAIL);
		// Click("textField_Action_Mirsap_SourceDocument");
		// SendKeys("textField_Action_Mirsap_Urn", ACTION_DOCUMENT_URN);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		//link_Action_Create_Theme
		click("link_Action_Create_Theme");
		click("checkBox_Action_Themes");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",getTotalWaitTimeInSecs());
		driver = closePopupWindowAndNavigateToParentWindowHandle("Action", currentParentWindowHandle);
		// Noticed an alert when records are more than 1000
		/*if(isAlertPresent()){
					acceptAlertBox(driver);
				}*/
		//After Raise New Fast Action setauto refresh to 1 min
		//set autorefreshperiod to 1 min
		sendKeys("textfield_Action_Searchresult_Refresh_Period", "1");
		click("button_Action_Searchresult_RefreshPeriod_Set");
		//explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		//Action suspended for 1 min as to later check count after AutoRefresh period
		TimeUnit.SECONDS.sleep(60);
		//wait for pageinfo div and store the total action record count in total_ActionRecord_Count_AfterRefresh
		explicitlyWaitForElement("label_Action_PageInfo", getTotalWaitTimeInSecs());
		String paging_Info_AfterRefresh = getText("label_Action_PageInfo");
		String[] paginginfo_Count_AfterRefresh=paging_Info_AfterRefresh.split(" ");
		int size2=paginginfo_Count_AfterRefresh.length;
		String total_ActionRecord_Count_AfterRefresh= paginginfo_Count_AfterRefresh[size2-1];
		int totalActionRecordCountAfterRefresh = Integer.parseInt(total_ActionRecord_Count_AfterRefresh);
		System.out.println("total_ActionRecord_Count_AfterRefresh:"+total_ActionRecord_Count_AfterRefresh);
		//verifyTextCompare(String.valueOf(Integer.parseInt(total_ActionRecord_Count_BeforeRefresh)+1), total_ActionRecord_Count_AfterRefresh);
		if (totalActionRecordCountBeforeRefresh < totalActionRecordCountAfterRefresh){
			Report.info("Refresh increments the total number of record count in page info");
		}
		else{
			Report.warn("Refresh has not incremented the total number of record count in page info");
		}

	}

	// Bulk Update 
	public void bulkUpdate()throws Exception{
		// Bulk Update- Single Record
		//click("label_Action_SearchResults_URN_Column");
		//explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		//click("label_Action_SearchResults_URN_Column");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
		/*click("table_Action_Dashboard_SearchResults_ActionTheme_FirstRow");
		String ThemesBeforeBulkUpdate=getText("table_Action_Dashboard_SearchResults_ActionTheme_FirstRow");
		Report.info("Themes before Bulk Update:"+ ThemesBeforeBulkUpdate);*/
		click("icon_Action_Searchresult_BulkUpdate");
		explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
		//change PM
		explicitlyWaitForElement("dropDown_Action_Create_ProtectiveMarking", getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Action_Create_ProtectiveMarking",PROTECTIVE_MARKING_SECRET);
		//change priority
		selectByVisibleText("dropDown_Action_Fast_Priority", PRIORITY_HIGH );
		clickAndWait("button_Action_BulkUpdate_Save");

		//Overwrite during Bulk Update- Does not Retain Existing Theme Values
		explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Overwrite",getTotalWaitTimeInSecs());
		clickAndWait("dialog_Action_Searchresult_BulkUpdate_Overwrite");
		explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Text",getTotalWaitTimeInSecs());
		verifyTextContains("Action updated","dialog_Action_Searchresult_BulkUpdate_Text");
		clickAndWait("dialog_Action_Searchresult_BulkUpdate_Ok");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow", getTotalWaitTimeInSecs());


		//Append during Bulk Update- Retains Existing Theme Values
		checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
		click("icon_Action_Searchresult_BulkUpdate");
		explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
		clickAndWait("button_Action_BulkUpdate_Save");
		explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Append",getTotalWaitTimeInSecs());
		clickAndWait("dialog_Action_Searchresult_BulkUpdate_Append");
		explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Text",getTotalWaitTimeInSecs());
		verifyTextContains("Action updated","dialog_Action_Searchresult_BulkUpdate_Text");
		clickAndWait("dialog_Action_Searchresult_BulkUpdate_Ok");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow", getTotalWaitTimeInSecs());

		//cancel Bulk Update- Bulk Update Flow cancelled
		checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
		click("icon_Action_Searchresult_BulkUpdate");
		explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
		clickAndWait("button_Action_BulkUpdate_Save");
		explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Cancel",getTotalWaitTimeInSecs());
		clickAndWait("dialog_Action_Searchresult_BulkUpdate_Cancel");
		explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
		explicitlyWaitForElement("button_Action_BulkUpdate_Cancel",getTotalWaitTimeInSecs());
		clickAndWait("button_Action_BulkUpdate_Cancel");
		waitForJQuery(driver);
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow", getTotalWaitTimeInSecs());
		sleep(3000);

		// Bulk Update- Two Record's
		// Search Fast Action By ForAllocation state
		//raiseFastActionBulkRecord();
		//searchFastActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		// select multiple recored in search result
		//click("label_Action_SearchResults_URN_Column");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		//click("label_Action_SearchResults_URN_Column");
		//explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		selectTwoRecordsFromSearchResultAndVerifyState(ALLOCATION);
		click("icon_Action_Searchresult_BulkUpdate");
		explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
		//change PM
		explicitlyWaitForElement("dropDown_Action_Create_ProtectiveMarking", getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Action_Create_ProtectiveMarking",PROTECTIVE_MARKING_SECRET);
		//change priority
		selectByVisibleText("dropDown_Action_Fast_Priority", PRIORITY_HIGH );
		click("button_Action_BulkUpdate_Save");
		
		//Overwrite during Bulk Update- Does not Retain Existing Theme Values
		explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Overwrite",getTotalWaitTimeInSecs());
		clickAndWait("dialog_Action_Searchresult_BulkUpdate_Overwrite");
		explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Text",getTotalWaitTimeInSecs());
		verifyTextContains("Action updated","dialog_Action_Searchresult_BulkUpdate_Text");
		clickAndWait("dialog_Action_Searchresult_BulkUpdate_Ok");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow", getTotalWaitTimeInSecs());


		//Append during Bulk Update- Retains Existing Theme Values
		checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
		checkCheckBox("checkBox_Action_SearchResulttab_Secondrow");
		click("icon_Action_Searchresult_BulkUpdate");
		explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
		clickAndWait("button_Action_BulkUpdate_Save");
		explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Append",getTotalWaitTimeInSecs());
		clickAndWait("dialog_Action_Searchresult_BulkUpdate_Append");
		explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Text",getTotalWaitTimeInSecs());
		verifyTextContains("Action updated","dialog_Action_Searchresult_BulkUpdate_Text");
		clickAndWait("dialog_Action_Searchresult_BulkUpdate_Ok");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow", getTotalWaitTimeInSecs());


		//cancel Bulk Update- Bulk Update Flow cancelled
		checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
		checkCheckBox("checkBox_Action_SearchResulttab_Secondrow");
		click("icon_Action_Searchresult_BulkUpdate");
		explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
		clickAndWait("button_Action_BulkUpdate_Save");
		explicitlyWaitForElement("dialog_Action_Searchresult_BulkUpdate_Cancel",getTotalWaitTimeInSecs());
		clickAndWait("dialog_Action_Searchresult_BulkUpdate_Cancel");
		explicitlyWaitForElement("tab_BulkUpdate",getTotalWaitTimeInSecs());
		explicitlyWaitForElement("button_Action_BulkUpdate_Cancel",getTotalWaitTimeInSecs());
		clickAndWait("button_Action_BulkUpdate_Cancel");
		waitForJQuery(driver);
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow", getTotalWaitTimeInSecs());
		sleep(3000);
	}

	private void addtoVisualizationList() throws Exception {
		//click("label_Action_SearchResults_URN_Column");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		//click("label_Action_SearchResults_URN_Column");
		//explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
		String addToVisualizationDialogContent = "added successfully to Visualiser";
		explicitlyWaitForElement("icon_Action_Searchresult_Visualise", getTotalWaitTimeInSecs());
		String childWindowHandle1 = getCurrentWindowHandle();
		Report.info("Child window handle before invoking another child: "
				+ childWindowHandle1);
		click("icon_Action_Searchresult_Visualise");
		explicitlyWaitForElement(
				"text_Action_View_VisualizationDialogboxContent",
				getTotalWaitTimeInSecs());
		verifyTextContains(addToVisualizationDialogContent,
				"text_Action_View_VisualizationDialogboxContent");
		int currentWinCount1 = getWindowCount();
		WebDriver visualizationWinWebDriver = clickAndWaitForBrowserPopup(VISUALISATION_PORTLET_WINDOW_TITLE,
				"button_Action_View_VisualizationDialogboxOk", currentWinCount1, currentWinCount1+1);
		int currentWinCount = getWindowCount();
		driver = closechildWindowAndNavigateToItsParentWindowHandle(visualizationWinWebDriver, childWindowHandle1, currentWinCount, currentWinCount-1);
	}

	//Add record to Favourite list
	public void addtoFavouriteList()throws Exception{
		//click("icon_Action_SearchResult_refresh");
		waitForJQuery(driver);
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
		explicitlyWaitForElement("icon_Action_Searchresult_FavouriteList",getTotalWaitTimeInSecs());
		click("icon_Action_Searchresult_FavouriteList");
		explicitlyWaitForElement("dialog_Action_Searchresult_AddtoList",getTotalWaitTimeInSecs());
		verifyTextContains("list","dialog_Action_Searchresult_FavouriteOrDefault_Text");
		click("dialog_Action_Searchresult_AddtoList_Ok");				
	}			

	//Add record to Default list
	public void addtoDefaultList()throws Exception{
		waitForJQuery(driver);
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
		explicitlyWaitForElement("icon_Action_Searchresult_DefaultList",getTotalWaitTimeInSecs());
		click("icon_Action_Searchresult_DefaultList");
		explicitlyWaitForElement("dialog_Action_Searchresult_AddtoList",getTotalWaitTimeInSecs());
		verifyTextContains("list","dialog_Action_Searchresult_FavouriteOrDefault_Text");
		click("dialog_Action_Searchresult_AddtoList_Ok");				
	}

	//Add record to addtoNeworExistingList
	public void addtoNeworExistingList()throws Exception{
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		checkCheckBox("checkBox_Action_SearchResulttab_Firstrow");
		explicitlyWaitForElement("icon_Action_Searchresult_NeworExistingList",
				getTotalWaitTimeInSecs());
		click("icon_Action_Searchresult_NeworExistingList");
		explicitlyWaitForElement("dialog_Action_Searchresult_AddtoList",
				getTotalWaitTimeInSecs());
		int noOfWindows = getWindowCount();
		clickAndWaitForChildWindow(ADD_TO_NEW_EXISTING_LIST, "dialog_Action_Searchresult_AddtoList_Ok", noOfWindows, noOfWindows+1);
		explicitlyWaitForElement("dropDown_ListManagement_NeworExistingList", getTotalWaitTimeInSecs());
		selectTextByIndex ("dropDown_ListManagement_NeworExistingList" , 1);
		click("button_ListManagement_Add");
		explicitlyWaitForElement("successMsg_ListManagement_Create", getTotalWaitTimeInSecs());
		click("button_ListManagement_Update_Save");
		explicitlyWaitForElement("successMsg_ListManagement_Create", getTotalWaitTimeInSecs());
		//driver = closePopupWindowAndNavigateToParentWindow(ADD_TO_NEW_EXISTING_LIST, FAST_ACTION_ACTIVITYLOG_TITLE_WINDOW);
		driver = closePopupWindowAndNavigateToParentWindow(ADD_TO_NEW_EXISTING_LIST, Action_Dashboard_Title);

	}	

	//define or re-order column
	public void defineorReorderColumns() throws Exception {
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		click("icon_Action_Searchresult_DefineorReorderColumns");
		waitForJQuery(driver);
		sleep(2000);
		if(!verifyElementDisplayed("dialog_Action_Searchresult_defineorReorderColumn")){
			click("icon_Action_Searchresult_DefineorReorderColumns");
		}
		explicitlyWaitForElement("dialog_Action_Searchresult_defineorReorderColumn",getTotalWaitTimeInSecs());
		click("dialog_Action_Searchresult_defineorReorderColumn_selected");
		/*explicitlyWaitForElement("dialog_Action_SearchResult_defineorReorderColumn_CheckALL", getTotalWaitTimeInSecs());*/
		/*clickElementUsingJS("dialog_Action_SearchResult_defineorReorderColumn_UnCheckALL");*/
		/*WebElement checkAllLink = explicitlyWaitAndGetWebElement("dialog_Action_SearchResult_defineorReorderColumn_CheckALL", getTotalWaitTimeInSecs());
				clickElementUsingJS(checkAllLink);*/
		click("dialog_Action_SearchResult_defineorReorderColumn_CheckALL4");
		explicitlyWaitForElement("dialog_Action_SearchResult_defineorReorderColumn_Ok", getTotalWaitTimeInSecs());
		click("dialog_Action_SearchResult_defineorReorderColumn_Ok");
	}

	public void performActionDashboardSearch() throws Exception{
		click("button_Action_Search");
		Thread.sleep(3000);
		// Noticed an alert when records are more than 1000
		if(isAlertPresent()){
			acceptAlertBox(driver);
		}
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
	}


	//Test case 45687:Action Dashboard: search Action sub classes

	@Test(priority = 3)
	public void actionDashboardSearchActionSubClasses(Method method) throws Exception{
		startLine(method);
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		verifyMirsapActionTabEnabled();
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		// verify Mandatory fields
		verifyMandatoryFieldsInMirsapAction();
		//insert text into mandatory fields
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS1);
		//explicitlyWaitForElement("dropDown_Action_Template_SubClass1",getTotalWaitTimeInSecs());
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Dashboard_subClass1", SUB_CLASS1);

		ArrayList<String> subclass1 = new ArrayList<String>();
		subclass1.add(SUB_CLASS1);

		verifyAllDowndownValues(subclass1, "dropDown_Action_Dashboard_subClass1");
		//getAllListItemAndVerify(SUB_CLASS1, "dropDown_Action_Template_SubClass1");
		//explicitlyWaitForElement("dropDown_Action_Template_SubClass2",getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Action_Dashboard_subClass2", SUB_CLASS2);
		//getAllListItemAndVerify(SUB_CLASS2, "dropDown_Action_Template_SubClass2");
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		// Click("textField_Action_Mirsap_SourceDocument");
		// SendKeys("textField_Action_Mirsap_Urn", ACTION_DOCUMENT_URN);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		// Verification of View Action Page
		verifyElementPresent("successMsg_Action_Create");
		isInMirsapActionViewPage();
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		String Mirsap_URN = actUrn[1];
		Report.info("mirsapActionURN_allFieldValues: " + Mirsap_URN);

		//navigate to action dashboard
		navigateTo(ACTION_DASHBOARD);
		verifyActionDashboard();
		sendKeys("textField_Action_Dashboard_URN", Mirsap_URN);
		selectByVisibleText("dropDown_Action_ActionDashboard_Search_Type","Action");
		selectByVisibleText("dropDown_Action_Dashboard_Class", MIRSAP_CLASS1);
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Dashboard_subClass1", SUB_CLASS1);
		//verifyTextCompare(SUB_CLASS1,"dropDown_Action_Template_SubClass1");
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Dashboard_subClass2", SUB_CLASS2);
		//verifyTextCompare(SUB_CLASS2,"dropDown_Action_Template_SubClass2");
		//click search
		click("button_Action_Search");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		endLine(method);

	}
	
	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}