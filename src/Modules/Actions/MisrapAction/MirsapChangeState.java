package Modules.Actions.MisrapAction;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import org.apache.poi.ss.usermodel.DateUtil;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Modules.Actions.FastAction.FastAction;
import Modules.AuditLogs.AuditLogSearch;
import ReusableUtilities.Dataprovider;
import ReusableUtilities.Report;

public class MirsapChangeState extends MirsapAction {
	private static final String RAISE_MIRSAP_ACTION_URL = "/group/holmes/raise-action";
	private static final String SEARCH_ACTION_URL = "/group/holmes/search-for-actions";
	private static final String ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE = "Action - Auto Test for Raise Action update";
	private static final String ACTION_ORIGINATING_DETAIL = "Originating Desc";
	private static final String ALPHA_NUMERIC_TEXT = "A1a29F";
	public String FOR_ALLOCATION = "For Allocation";
	public String ALLOCATED = "Allocated";
	public String SUBMITTED = "Submitted";
	public String RECEIVED = "Received";
	public String FILED = "Filed";
	private String fieldDateEmpty="";
	private String actionThemeOne;
	private String actionThemeTwo;
	public String mirsapURN;
	public String fastURN;

	// Data for Login read from Xl sheet
	//private String userName;
	//private String passWord;
	private String linkedDocumentsRecordURN2;
	//public String currentUserName=getUserName();
	//private String takenDocumentsRecordURN;
	




	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}

	// default constructor
	public MirsapChangeState() {
		softAssert = new SoftAssert();
	}

	@BeforeTest
	public void beforeTest() throws Exception {
		loginInIEAndSelectIncident();

	}
	
	@Test
	public void testData() {
		actionThemeOne = readConfigFile("actionThemeOne");
		actionThemeTwo = readConfigFile("actionThemeTwo");
		username = readConfigFile("username2");
		password = readConfigFile("password2");

		System.out.println("userName: " + username);
		System.out.println("passWord: " + password);
		System.out.println("actionThemeOne: " + actionThemeOne);
		System.out.println("actionThemeTwo: " + actionThemeTwo);

	}

	public void verifyActionTabEnabled(String objname) {
		verifyTabActive(objname);
	}

	/* Test case 30772:001. Mirsap Action -Single Queue Move-For Allocation to Allocated*/
	//Allocated
	@Test(priority = 2)
	public void mirsapAction_Queue_Allocated(Method method) throws Exception {
		startLine(method);
		String mirsapURN=raiseAndGetMirsapActionURN();
		verifyText(FOR_ALLOCATION, "label_Action_View_State");
		//getList and verify state in dropdown
		getAllListItemAndVerify( ALLOCATED,"dropDown_Action_Select_State");
		getAllListItemAndVerify( FOR_REFERRAL,"dropDown_Action_Select_State");
		getAllListItemAndVerify( PENDED,"dropDown_Action_Select_State");
		//change state button click
		clickAndWait("button_Action_ChangeState");
		//wait for allocation officer
		explicitlyWaitForElement("button_Action_ChangeState_Save",
				getTotalWaitTimeInSecs());
		explicitlyWaitForElement("textfield_Action_MirsapAction_QueueAllocatedOfficerOne",
				getTotalWaitTimeInSecs());
		verifyMirsapActionFieldsInAllocatedPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		//Add First Create Officer
		click("button_Action_MirsapAction_CreateOfficerOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_Forename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");
		click("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_MirsapAction_QueueAllocatedOfficerOne");

		//Add 2nd Create officer
		click("button_Action_MirsapAction_CreateOfficerTwo");
		explicitlyWaitForElement(
				"textfield_Action_MirsapAction_CreateOfficer_SurnameTwo",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_MirsapAction_CreateOfficer_SurnameTwo",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_MirsapAction_CreateOfficer_ForenameTwo",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateTwo");
		/*click("textfield_Action_MirsapAction_QueueAllocatedOfficerTwo");
		sendKeys("textfield_Action_MirsapAction_QueueAllocatedOfficerTwo",
				CREATE_OFFICER_SURNAME);*/
		//explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",
		//getTotalWaitTimeInSecs());
		//clickAndWait("textfield_Action_StateMove_OfficerName");
		/*verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_MirsapAction_QueueAllocatedOfficerTwo");*/

		// Perform Save on Allocated Page
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(ALLOCATED, "label_Action_View_State");
		//getList and verify item in dropdown
		getAllListItemAndVerify( SUBMITTED,"dropDown_Action_Select_State");
		getAllListItemAndVerify( RECEIVED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_REFERRAL,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textField_Action_MirsapAction_view_CreateOfficerOne");
	/*	verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textField_Action_MirsapAction_view_CreateOfficerTwo");*/
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE, getTextFieldText());
		verifyText(fieldDateEmpty,"textField_Action_MirsapAction_Create_ReceiversInstructions");

		//Check if a notification is generated to Allocated Officer 
		notificationCheck();
		endLine(method);

	}
	/*//Relogin as allocated officer- surName to check generated notification
	private void reLoginasAllocatedOfficer() throws Exception{
		//Relogin as allocated officer- surName
		handleSecurityWindow1();
		explicitlyWaitForElement("textField_Login_UserName", getTotalWaitTimeInSecs());
		sendKeys("textField_Login_UserName", userName);
		sendKeys("textField_Login_PassWord", passWord);
		click("button_Login_SignIn");
		selectAGroupInHomePage();
	}

	private void notificationCheck() throws Exception{

		// Log Out Method
		signOut();

		//Relogin as allocated officer
		reLoginasAllocatedOfficer();

		explicitlyWaitForElement("icon_Home_Workspace_Notification",
				getTotalWaitTimeInSecs());

		// Notification checking
		click("icon_Home_Workspace_Notification");
		explicitlyWaitForElement("table_Action_Workspace_NotificationItems_FirstRow",
				getTotalWaitTimeInSecs());
		verifyTextContains("Low priority Action has been allocated to you", "table_Action_Workspace_NotificationItems_FirstRow"  );
		//validateAllVerificationPoints();
		//endLine(method);

		//check Notification and sign out
		signOut();

		//Relogin as System Admin
		loginInIEAndSelectIncident();

	}*/

	//Test case 26492:002. Mirsap Action -Single Queue Move-Allocated to Submitted
	//Submitted
	@Test(priority=3)//, dependsOnMethods = {"mirsapAction_Queue_Allocated"})
	public void mirsapAction_Queue_Submitted(Method method)throws Exception{
		startLine(method);
		viewAndGetMirsapActionURN(getMirsapActionURN);
		verifyText(ALLOCATED ,"label_Action_View_State");
		//getList and verify state in dropdown
		//getList and verify item in dropdown
		getAllListItemAndVerify( SUBMITTED,"dropDown_Action_Select_State");
		getAllListItemAndVerify( RECEIVED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_REFERRAL,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyMirsapActionFieldsInSubmittedPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(SUBMITTED ,"label_Action_View_State");
		getAllListItemAndVerify(RECEIVED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE, getTextandResultFieldText());
		verifyText(fieldDateEmpty,"textField_Action_MirsapAction_Create_ReceiversInstructions");
		endLine(method);

	}

	//Test case 26493:003. Mirsap Action -Single Queue Move-Submitted to Received
	//Received
	@Test(priority=4)//, dependsOnMethods = {"mirsapAction_Queue_Submitted"})
	public void mirsapAction_Queue_Received(Method method)throws Exception{
		startLine(method);
		viewAndGetMirsapActionURN(getMirsapActionURN);
		verifyText(SUBMITTED ,"label_Action_View_State");
		//getList and verify state in dropdown
		//getList and verify item in dropdown
		getAllListItemAndVerify(RECEIVED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyMirsapActionFieldsInReceivedPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		//sendKeys("textField_Action_MirsapAction_Edit_ReceiversInstructions",TEXTFIELD_TEXT_FOR_SINGLE_QUEUE_MOVE);
		insertIntoAllTinyMCEinPage(TEXTFIELD_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(RECEIVED ,"label_Action_View_State");
		getAllListItemAndVerify(RESULTED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(PARTIALLY_RESULTED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		getAllListItemAndVerify(PENDED,"dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE, getTextandResultFieldText());
		//verifyText(TEXTFIELD_TEXT_FOR_SINGLE_QUEUE_MOVE,"textField_Action_MirsapAction_Create_ReceiversInstructions");
		endLine(method);

	}

	//Test case 26499:004. Mirsap Action -Single Queue Move-Received to Resulted
	//Resulted
	@Test(priority=5)//, dependsOnMethods = {"mirsapAction_Queue_Received"})
	public void mirsapAction_Queue_Resulted(Method method)throws Exception{
		startLine(method);
		viewAndGetMirsapActionURN(getMirsapActionURN);
		verifyText(RECEIVED ,"label_Action_View_State");
		//getList and verify state in dropdown
		//getList and verify item in dropdown
		getAllListItemAndVerify(RESULTED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(PARTIALLY_RESULTED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		getAllListItemAndVerify(PENDED,"dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		//verifyMirsapActionFieldsInResultedPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);

		//Adding  Document Taken table data
		/*clickAndWait("button_Action_ChangeState_DocumentsTaken_Add");
		explicitlyWaitAndGetWebElement("textField_Action_Update_DocumentTaken_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",linkedDocumentsRecordURN2);
		click("dropDown_Action_SelectRecord_ReferenceIncident");
		pressReturnKey("dropDown_Action_SelectRecord_ReferenceIncident");
		explicitlyWaitAndGetWebElement("button_Action_Update_DocumentTaken_Documents_Add", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Update_DocumentTaken_Documents_Add");*/

		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(RESULTED ,"label_Action_View_State");
		getAllListItemAndVerify(FILED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_REFERRAL,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		getAllListItemAndVerify(PENDED,"dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE, getTextandResultFieldText());
		//verifyText(TEXTFIELD_TEXT_FOR_SINGLE_QUEUE_MOVE,"textField_Action_MirsapAction_Create_ReceiversInstructions");
		endLine(method);
	}

	//Test case 26495:005. Mirsap Action -Single Queue Move- Resulted to Filed
	//Filed
	@Test(priority=6)//, dependsOnMethods = {"mirsapAction_Queue_Resulted"})
	public void mirsapAction_Queue_Filed(Method method)throws Exception{
		startLine(method);
		viewAndGetMirsapActionURN(getMirsapActionURN);
		verifyText(RESULTED ,"label_Action_View_State");
		//getList and verify state in dropdown
		//getList and verify item in dropdown
		getAllListItemAndVerify(FILED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_REFERRAL,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		getAllListItemAndVerify(PENDED,"dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyMirsapActionFieldsInFiledPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(FILED ,"label_Action_View_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE, getTextandResultFieldText());
		//verifyText(TEXTFIELD_TEXT_FOR_SINGLE_QUEUE_MOVE,"textField_Action_MirsapAction_Create_ReceiversInstructions");
		endLine(method);
	}	
	
	// Test case 26494:006. Mirsap Action -Single Queue Move-Received to Partially Resulted
	//Partially Resulted-single 
	@Test(priority=7)
	public void mirsapAction_Queue_PartiallyResulted(Method method)throws Exception{

		startLine(method);
		raiseAndGetMirsapActionURN();

		//change state to allocated and check mandatory fields
		clickAndWait("button_Action_ChangeState");
		changeStateAllocated(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);

		//change state to submitted and check mandatory fields
		clickAndWait("button_Action_ChangeState");
		changeStateSubmitted(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE,TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);

		//change state to received (No Mandatory fields)
		clickAndWait("button_Action_ChangeState");
		changeStateReceived(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE,TEXTFIELD_TEXT_FOR_SINGLE_QUEUE_MOVE);
		
		//Change state to Partially Resulted
		verifyText(RECEIVED ,"label_Action_View_State");
		//getList and verify state in dropdown
		getAllListItemAndVerify(RESULTED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(PARTIALLY_RESULTED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		getAllListItemAndVerify(PENDED,"dropDown_Action_Select_State");
		selectByVisibleText("dropDown_Action_Select_State",PARTIALLY_RESULTED);
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		//verifyFastActionFieldsInPartially_ResultedPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE );
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);

		//Adding Document Taken Grid- URN
		/*clickAndWait("button_Action_ChangeState_DocumentsTaken_Add");
		explicitlyWaitAndGetWebElement("textField_Action_Update_DocumentTaken_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",linkedDocumentsRecordURN2);
		click("dropDown_Action_SelectRecord_ReferenceIncident");
		pressReturnKey("dropDown_Action_SelectRecord_ReferenceIncident");
		clickAndWait("button_Action_Update_DocumentTaken_Documents_Add");*/

		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(PARTIALLY_RESULTED ,"label_Action_View_State");
		getAllListItemAndVerify(ALLOCATED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_REFERRAL,"dropDown_Action_Select_State");
		getAllListItemAndVerify(PENDED,"dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE, getTextandResultFieldText());
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE,getAllTextFieldText());
		//verifyText(TEXTFIELD_TEXT_FOR_SINGLE_QUEUE_MOVE,"textField_Action_FastAction_Create_ReceiversInstructions");
		//verifyText(actionThemeOne,"table_Action_ThemeItem");
		endLine(method);

	}
	
	// Test case 26496:009. Mirsap Action -Single Queue Move-For Allocation to For Referral
	//For Referral-single 
	@Test(priority=8)
	public void mirsapAction_Queue_ForReferral(Method method)throws Exception{

		startLine(method);
		raiseAndGetMirsapActionURN();
		verifyText(FOR_ALLOCATION, "label_Action_View_State");
		//getList and verify state in dropdown
		getAllListItemAndVerify( ALLOCATED,"dropDown_Action_Select_State");
		getAllListItemAndVerify( FOR_REFERRAL,"dropDown_Action_Select_State");
		getAllListItemAndVerify( PENDED,"dropDown_Action_Select_State");

		//Select the state- For Referral to perform state change
		selectByVisibleText("dropDown_Action_Select_State",FOR_REFERRAL);
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyFastActionFieldsInFor_ReferralPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE );
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(FOR_REFERRAL ,"label_Action_View_State");
		getAllListItemAndVerify(REFERRED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE, getTextandResultFieldText());
		//verifyText(fieldDateEmpty,"textField_Action_MirsapAction_Create_ReceiversInstructions");
		endLine(method);

	}
	
	// Test case 26498:010. Mirsap Action -Single Queue Move-For Referral to Referred
	//For Referred-single 
	@Test(priority=9)
	public void mirsapAction_Queue_ForReferred(Method method)throws Exception{

		startLine(method);
		viewAndGetMirsapActionURN(getMirsapActionURN);
		verifyText(FOR_REFERRAL, "label_Action_View_State");
		//getList and verify state in dropdown
		getAllListItemAndVerify(REFERRED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		//Perform state change
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyFastActionFieldsInReferredPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE );
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(REFERRED ,"label_Action_View_State");
		getAllListItemAndVerify(FILED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE, getTextandResultFieldText());
		//verifyText(fieldDateEmpty,"textField_Action_MirsapAction_Create_ReceiversInstructions");
		endLine(method);

	}
	
	// Test case 26501:011. Mirsap Action -Single Queue Move-For Allocation to Pended
	//For Pended-single 
	@Test(priority=10)
	public void mirsapAction_Queue_Pended(Method method)throws Exception{

		startLine(method);
		raiseAndGetMirsapActionURN();
		verifyText(FOR_ALLOCATION, "label_Action_View_State");
		//getList and verify state in dropdown
		getAllListItemAndVerify( ALLOCATED,"dropDown_Action_Select_State");
		getAllListItemAndVerify( FOR_REFERRAL,"dropDown_Action_Select_State");
		getAllListItemAndVerify( PENDED,"dropDown_Action_Select_State");
		//Perform state change
		//Select the state- For Referral to perform state change
		selectByVisibleText("dropDown_Action_Select_State",PENDED);
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyFastActionFieldsInPendedPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE );
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		String futureDate= getTomorrowDataStamp();
		sendKeys("textField_Action_MirsapAction_PendDate",futureDate);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(PENDED ,"label_Action_View_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		getAllListItemAndVerify(ALLOCATED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_REFERRAL,"dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE, getTextandResultFieldText());
		verifyText(futureDate,"textField_Action_MirsapAction_view_PendDate");
		//verifyText(fieldDateEmpty,"textField_Action_MirsapAction_Create_ReceiversInstructions");
		endLine(method);

	}
	
	/* Test case 30761:008. Audit Log- Action State Moves */
	@Test(priority = 11)
	public void mirsapAction_AuditLog_SingleStateMoves(Method method)
			throws Exception {
		startLine(method);
		AuditLogSearch auditLog = new AuditLogSearch(driver);
		// verify Audit Single Allocated
		auditLog.auditLogSearchByActivity(ALLOCATED_ACTION);
		verifyAuditLogSearchResult(ALLOCATED_ACTION);

		// verify Audit Single Submitted
		auditLog.auditLogSearchByActivity(SUBMITTED_ACTION);
		verifyAuditLogSearchResult(SUBMITTED_ACTION);

		// verify Audit Single Resulted
		auditLog.auditLogSearchByActivity(RESULTED_ACTION);
		verifyAuditLogSearchResult(RESULTED_ACTION);

		// verify Audit Single Received
		auditLog.auditLogSearchByActivity(RECEIVED_ACTION);
		verifyAuditLogSearchResult(RECEIVED_ACTION);

		// verify Audit Single Filed
		auditLog.auditLogSearchByActivity(FILED_ACTION);
		verifyAuditLogSearchResult(FILED_ACTION);
		
		//verify Audit - Single Partially Resulted 
		// This will currently FAIL as there is no activity description for Partially Resulted 
		auditLog.auditLogSearchByActivity(PARTIALLY_RESULTED_ACTION);
		verifyAuditLogSearchResult(PARTIALLY_RESULTED_ACTION);
		endLine(method);
	}
	

	public void verifyAuditLogSearchResult(String actvityDescription)
			throws Exception {
		verifyText(usernameUpperCase,
				"table_AuditLog_AuditRecord_FirstRecord_Username");
		explicitlyWaitForElement("table_AuditLog_AuditRecord_SecondRecord_Username",getTotalWaitTimeInSecs());
		verifyText(usernameUpperCase,
				"table_AuditLog_AuditRecord_SecondRecord_Username");
		verifyTextContains(incidentIdFull,
				"table_AuditLog_AuditRecord_FirstRecord_Incident");
		verifyTextContains(incidentIdFull,
				"table_AuditLog_AuditRecord_SecondRecord_Incident");
		verifyTextContains(actvityDescription,
				"table_AuditLog_AuditRecord_FirstRecord_Activity");
		verifyTextContains(actvityDescription,
				"table_AuditLog_AuditRecord_SecondRecord_Activity");

	}

	
	
	/*
	 * Test case 30758 007. Single Queue Move - Cancel Button
	 */
	@Test(priority = 12)
	public void mirsapAction_Single_Queue_Move_CancelButton(Method method) throws Exception
	{
		startLine(method);
		String mirsapURN=raiseAndGetMirsapActionURN();
		verifyText(FOR_ALLOCATION, "label_Action_View_State");
		
		//getList and verify state in dropdown
		getAllListItemAndVerify( ALLOCATED,"dropDown_Action_Select_State");
		getAllListItemAndVerify( FOR_REFERRAL,"dropDown_Action_Select_State");
		getAllListItemAndVerify( PENDED,"dropDown_Action_Select_State");
		//change state button click
		clickAndWait("button_Action_ChangeState");
		//wait for allocation officer
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		//click cancel button and check state hasn't changed
		clickAndWait("button_Action_FastAction_Move_State_Cancel");
		//getList and verify state in dropdown
		verifyText(FOR_ALLOCATION, "label_Action_View_State");
		getAllListItemAndVerify( ALLOCATED,"dropDown_Action_Select_State");
		getAllListItemAndVerify( FOR_REFERRAL,"dropDown_Action_Select_State");
		getAllListItemAndVerify( PENDED,"dropDown_Action_Select_State");
		
		//go through all states and check cancel button
		clickAndWait("button_Action_ChangeState");
		//wait for allocation officer
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		/*click("button_Action_MirsapAction_CreateOfficerOne");
		explicitlyWaitForElement("textfield_Action_CreateOfficer_Surname",getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname",CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_Forename",	CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");
		click("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");
		sendKeys("textfield_Action_MirsapAction_QueueAllocatedOfficerOne",CREATE_OFFICER_SURNAME);
		explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_StateMove_OfficerName");
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,"textfield_Action_MirsapAction_QueueAllocatedOfficerOne");*/
		
		click("button_Action_MirsapAction_CreateOfficerOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_Forename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");
		click("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");

		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_MirsapAction_QueueAllocatedOfficerOne");
		
		click("button_Action_MirsapAction_CreateOfficerTwo");
		explicitlyWaitForElement(
				"textfield_Action_MirsapAction_CreateOfficer_SurnameTwo",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_MirsapAction_CreateOfficer_SurnameTwo",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_MirsapAction_CreateOfficer_ForenameTwo",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateTwo");
		
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement(
				"successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		
		//allocated to submitted
		verifyText(ALLOCATED ,"label_Action_View_State");
		//getList and verify state in dropdown
		//getList and verify item in dropdown
		getAllListItemAndVerify( SUBMITTED,"dropDown_Action_Select_State");
		getAllListItemAndVerify( RECEIVED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_REFERRAL,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		clickAndWait("button_Action_FastAction_Move_State_Cancel");
		
		verifyText(ALLOCATED ,"label_Action_View_State");
		//getList and verify state in dropdown
		//getList and verify item in dropdown
		getAllListItemAndVerify( SUBMITTED,"dropDown_Action_Select_State");
		getAllListItemAndVerify( RECEIVED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_REFERRAL,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement(
				"successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(SUBMITTED ,"label_Action_View_State");
		getAllListItemAndVerify(RECEIVED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE, getTextandResultFieldText());
		verifyText(fieldDateEmpty,"textField_Action_MirsapAction_Create_ReceiversInstructions");
	
		//submitted to received
		//getList and verify state in dropdown
		//getList and verify item in dropdown
		getAllListItemAndVerify(RECEIVED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyMirsapActionFieldsInReceivedPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		//click cancel
		clickAndWait("button_Action_FastAction_Move_State_Cancel");
		
		verifyText(SUBMITTED ,"label_Action_View_State");
		getAllListItemAndVerify(RECEIVED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyMirsapActionFieldsInReceivedPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		sendKeys("textField_Action_MirsapAction_Edit_ReceiversInstructions",TEXTFIELD_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement(
				"successMsg_Action_Create",	getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(RECEIVED ,"label_Action_View_State");
		getAllListItemAndVerify(RESULTED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(PARTIALLY_RESULTED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		getAllListItemAndVerify(PENDED,"dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE, getTextandResultFieldText());
		verifyText(TEXTFIELD_TEXT_FOR_SINGLE_QUEUE_MOVE,"textField_Action_MirsapAction_Create_ReceiversInstructions");
		
		//Received to Resulted
		clickAndWait("button_Action_ChangeState");

		verifyMirsapActionFieldsInResultedPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		//click cancel
		clickAndWait("button_Action_FastAction_Move_State_Cancel");
		//getAllListItemAndVerify(RESULTED,"dropDown_Action_Select_State");
		//getAllListItemAndVerify(PARTIALLY_RESULTED,"dropDown_Action_Select_State");
		//getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		//getAllListItemAndVerify(PENDED,"dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyMirsapActionFieldsInResultedPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);	
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement(
				"successMsg_Action_Create",getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(RESULTED ,"label_Action_View_State");
		getAllListItemAndVerify(FILED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_REFERRAL,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		getAllListItemAndVerify(PENDED,"dropDown_Action_Select_State");
		verifyTextCompare(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE, getTextandResultFieldText());
		verifyText(TEXTFIELD_TEXT_FOR_SINGLE_QUEUE_MOVE,"textField_Action_MirsapAction_Create_ReceiversInstructions");
		
		//Resulted to Filed
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyMirsapActionFieldsInFiledPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		//click cancel
		clickAndWait("button_Action_FastAction_Move_State_Cancel");
		verifyText(RESULTED ,"label_Action_View_State");
		getAllListItemAndVerify(FILED,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_REFERRAL,"dropDown_Action_Select_State");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_Select_State");
		getAllListItemAndVerify(PENDED,"dropDown_Action_Select_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyMirsapActionFieldsInFiledPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement(
				"successMsg_Action_Create",getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(FILED ,"label_Action_View_State");
		getAllListItemAndVerify("For Allocation","dropDown_Action_Select_State");
		
		endLine(method);
	}
	
	/*
	 *  Test Case 30759: 007A, Single Queue Move - Missing Mandatory Field Value
	 */
	@Test(priority = 13)
	public void mirsapAction_SingleQueueMove_MandatoryFields(Method method) throws Exception{
		startLine(method);
		String mirsapURN=raiseAndGetMirsapActionURN();
		
		//For Allocation -> Allocated
		verifyText(FOR_ALLOCATION, "label_Action_View_State");
		//getList and verify state in dropdown
		getAllListItemAndVerify( ALLOCATED,"dropDown_Action_Select_State");
		getAllListItemAndVerify( FOR_REFERRAL,"dropDown_Action_Select_State");
		getAllListItemAndVerify( PENDED,"dropDown_Action_Select_State");
		//change state button click
		clickAndWait("button_Action_ChangeState");
		//wait for allocation officer
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		clickAndWait("button_Action_ChangeState_Save");
		//verify error message here
		verifyText("Allocated Officer is required","errorMsg_Action_Create_WithOutAnyFields");
		
		click("button_Action_MirsapAction_CreateOfficerOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_Forename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");
		click("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");
	
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_MirsapAction_QueueAllocatedOfficerOne");

		/*sendKeys("textfield_Action_MirsapAction_QueueAllocatedOfficerOne",CREATE_OFFICER_SURNAME);
		explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_StateMove_OfficerName");
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,"textfield_Action_MirsapAction_QueueAllocatedOfficerOne");*/
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		
		//Allocated -> Submitted
		verifyText(ALLOCATED ,"label_Action_View_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		clickAndWait("button_Action_ChangeState_Save");
		//verify error message here
		verifyText("Submission Text is required","errorMsg_Action_Create_WithOutAnyFields");
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		
		//Submitted -> Received
		verifyText(SUBMITTED ,"label_Action_View_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		clickAndWait("button_Action_ChangeState_Save");

		//Received -> Resulted
		verifyText(RECEIVED ,"label_Action_View_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		clickAndWait("button_Action_ChangeState_Save");
		//verify error message here
		verifyText("Result Text is required","errorMsg_Action_Create_WithOutAnyFields");
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_Save");
		
		//Resulted -> Filed
		verifyText(RESULTED ,"label_Action_View_State");
		clickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		clickAndWait("button_Action_ChangeState_Save");
		//verify error message here
		verifyText("Filing Reason is required","errorMsg_Action_Create_WithOutAnyFields");
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(FILED ,"label_Action_View_State");		
		endLine(method);
	}
	
	/*
	 * Test case 30760:007B. Single Queue Move- Large Text Fields and Special Characters
	 */
	@Test(priority = 14)
	public void mirsapAction_Change_State_Large_Fields(Method method)
			throws Exception {
		startLine(method);

		String docContent = readTextFile(System.getProperty("user.dir")
				+ "\\hugeDataForTextField.txt");
		// navigate to Raise Mirsap Action URL
		// navigate to Raise Mirsap Action URL
		navigateToPage(RAISE_MIRSAP_ACTION_URL);
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		verifyTextContains(incidentIdFull, "dropDown_Action_Create_Incident");
		// verifyMandatoryFieldsInMirsapAction();
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		// insert text Text field
		insertIntoAllTinyMCEinPage(docContent);
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		click("link_Action_Create_LinkedAssociation");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		// Verification of data in View Action Fields
		verifyElementPresent("successMsg_Action_Create");
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		MirsapActionURN_ReUse = actUrn[1];

		// change state to allocated and check mandatory fields
		clickAndWait("button_Action_ChangeState");
		changeStateAllocated(docContent);

		// change state to submitted and check mandatory fields
		clickAndWait("button_Action_ChangeState");
		changeStateSubmitted(docContent, docContent);

		// change state to received (No Mandatory fields)
		clickAndWait("button_Action_ChangeState");
		changeStateReceived(docContent, docContent);

		// change state to Resulted checking cancel button
		clickAndWait("button_Action_ChangeState");
		changeStateResulted(docContent, docContent);

		// change state to Filed checking cancel button
		clickAndWait("button_Action_ChangeState");
		changeStateFiledForChangeStateLargeFields(docContent);

		endLine(method);
	}

	/*
	 * Test case 30762: 012 Multiple Queue Move - Move ALL, Apply & Move Individual & Skip
	 
	@Test(priority = 32)
	public void mirsapAction_MultipleQueueMove_All_Individual_Skip(Method method) throws Exception{
		startLine(method);

		// Search M Action By ForAllocation state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		//click("label_Action_SearchResults_URN_Column");
		//click("label_Action_SearchResults_URN_Column");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(FOR_ALLOCATION);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");

		// verify header Moving BulkState move :allocated
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		// verify BulkUpdate tab
		verifyElementPresent("tab_Action_BulkUpdate");
		// verify label Moving and allocated URN
		verifyElementPresent("label_Action_BulkStateMove_Allocated");
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + "Fast Action , "
				+ incidentIdFull + " - " + searchResult_ActionSecondURN
				+ "Fast Action to : Allocated",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(ALLOCATED, "label_Action_BulkUpdate_State");
		// verify theme
		verifyElementPresent("checkBox_Action_Themes_Item1");
		// Verify Tag link
		verifyElementPresent("link_Action_BulkUpdate_MoveAll_Tags");
		// Entering officer name
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficer",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_FastAction_QueueAllocatedOfficer",
				CREATE_OFFICER_SURNAME);
		explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",
				getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_StateMove_OfficerName");
		
		click("button_Action_MirsapAction_CreateOfficerOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_Forename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");
		click("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");
	
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_MirsapAction_QueueAllocatedOfficerOne");

		// verify cancel button in bulk update page
		verifyElementPresent("button_Action_BulkUpdate_Cancel");
		verifyElementPresent("button_Action_BulkUpdate_Save");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move :allocated
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(FOR_ALLOCATION, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		String firstBulkMoveURN1 = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
		// click Move all button
		//click("button_Action_BulkSateMovePopup_MoveIndividual");
		// get Handle to Move Individual update window
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Cancel");
		// verify Skip button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Skip");
		// verify apply button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Apply");
		// verify MoveIndividual button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual");
		// verify label MoveIndividual
		verifyElementPresent("label_Action_BulkStateMove_MoveIndividual_Allocated");
		// Verify URN and state
		// Move Fast Action A27 in Incident MORSTA16M21 to Allocated
		verifyTextContains("Move Fast Action " + firstBulkMoveURN1
				+ " in Incident " + incidentIdFull + " to Allocated",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state allocted
		// verifyText(ALLOCATED,"label_Action_BulkUpdate_MoveIndividual_State");
		// verify theme
		verifyElementPresent("checkBox_Action_Themes_Item1");
		// Entering officer name
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficer",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_FastAction_QueueAllocatedOfficer",
				CREATE_OFFICER_SURNAME);
		explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",
				getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_StateMove_OfficerName");
		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");
		
		click("button_Action_MirsapAction_CreateOfficerOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_Forename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");
		click("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");
	
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_MirsapAction_QueueAllocatedOfficerOne");

		click("button_Action_BulkUpdate_MoveIndividual");
		explicitlyWaitForElement(
				"button_Action_BulkUpdate_MoveIndividual_Update_OK",
				getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(firstBulkMoveURN1, "successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated", "successMsg_Action_Update");
		// click OK button
		click("button_Action_BulkUpdate_MoveIndividual_Update_OK");		
		endLine(method);
	}*/
	
	
	/*
	 * Test case 30769: 012A Missing Mandatory field Value in Bulk State Move Screen
	 */
	@Test(priority = 16)
	public void mirsapAction_BulkStateMove_MandatoryValues(Method method) throws Exception{
		startLine(method);

		// Search M Action By ForAllocation state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		//click("label_Action_SearchResults_URN_Column");
		//click("label_Action_SearchResults_URN_Column");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(FOR_ALLOCATION);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");

		// verify header Moving BulkState move :allocated
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		// verify BulkUpdate tab
		verifyElementPresent("tab_Action_BulkUpdate");
		// verify label Moving and allocated URN
		verifyElementPresent("label_Action_BulkStateMove_Allocated");
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + "Fast Action , "
				+ incidentIdFull + " - " + searchResult_ActionSecondURN
				+ "Fast Action to : Allocated",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(ALLOCATED, "label_Action_BulkUpdate_State");
		// verify theme
		//verifyElementPresent("checkBox_Action_Themes_Item1");
		// Verify Tag link
		verifyElementPresent("link_Action_BulkUpdate_MoveAll_Tags");
		// Entering officer name
		explicitlyWaitForElement("textfield_Action_FastAction_QueueAllocatedOfficer",getTotalWaitTimeInSecs());
		// clicking Bulk update save button without entering mandatory values
		clickAndWait("button_Action_BulkUpdate_Save");
		explicitlyWaitForElement("errorMsg_Action_Create_WithOutAnyFields",getTotalWaitTimeInSecs());
		verifyText("Allocated Officer is required","errorMsg_Action_Create_WithOutAnyFields");
		/*sendKeys("textfield_Action_FastAction_QueueAllocatedOfficer",
				CREATE_OFFICER_SURNAME);
		explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_StateMove_OfficerName");*/
		
		click("button_Action_MirsapAction_CreateOfficerOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_BulkUpdateSurname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_BulkUpdateSurname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_BulkUpdateForename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");
		click("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");
		// verify cancel button in bulk update page
		verifyElementPresent("button_Action_BulkUpdate_Cancel");
		verifyElementPresent("button_Action_BulkUpdate_Save");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,	"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// Search M Action By Allocation state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Allocated");
		//click("label_Action_SearchResults_URN_Column");
		//click("label_Action_SearchResults_URN_Column");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(ALLOCATED);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", SUBMITTED);
		// verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify header Moving BulkState move :allocated
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		// clicking Bulk update save button without entering mandatory values
		click("button_Action_BulkUpdate_Save");
		explicitlyWaitForElement("errorMsg_Action_Create_WithOutAnyFields",getTotalWaitTimeInSecs());
		verifyText("Submission Text is required","errorMsg_Action_Create_WithOutAnyFields");
		insertIntoAllTinyMCEinPage("Submission Text");
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,	"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",getTotalWaitTimeInSecs());
		click("button_Action_BulkUpdate_UpdatePopup_OK");
		
		// Search M Action By Submitted state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Submitted");
		//click("label_Action_SearchResults_URN_Column");
		//click("label_Action_SearchResults_URN_Column");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(SUBMITTED);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", RECEIVED);
		// verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify header Moving BulkState move :allocated
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		// clicking Bulk update save button without entering mandatory values
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,	"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",getTotalWaitTimeInSecs());
		click("button_Action_BulkUpdate_UpdatePopup_OK");
		
		// Search M Action By Received state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Received");
		//click("label_Action_SearchResults_URN_Column");
		//click("label_Action_SearchResults_URN_Column");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(RECEIVED);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", RESULTED);
		// verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify header Moving BulkState move :allocated
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		// clicking Bulk update save button without entering mandatory values
		click("button_Action_BulkUpdate_Save");
		explicitlyWaitForElement("errorMsg_Action_Create_WithOutAnyFields",getTotalWaitTimeInSecs());
		verifyText("Result Text is required","errorMsg_Action_Create_WithOutAnyFields");
		insertIntoAllTinyMCEinPage("Result Text");
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,	"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",getTotalWaitTimeInSecs());
		click("button_Action_BulkUpdate_UpdatePopup_OK");
		
		// Search M Action By Resulted state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Resulted");
		//click("label_Action_SearchResults_URN_Column");
		//click("label_Action_SearchResults_URN_Column");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(RESULTED);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", FILED);
		// verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify header Moving BulkState move :allocated
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		// clicking Bulk update save button without entering mandatory values
		click("button_Action_BulkUpdate_Save");
		explicitlyWaitForElement("errorMsg_Action_Create_WithOutAnyFields",getTotalWaitTimeInSecs());
		verifyText("Filing Reason is required","errorMsg_Action_Create_WithOutAnyFields");
		insertIntoAllTinyMCEinPage("Filing Reason Text");
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,	"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",getTotalWaitTimeInSecs());
		click("button_Action_BulkUpdate_UpdatePopup_OK");		
		
		// Search M Action By Filed state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Filed");
		//click("label_Action_SearchResults_URN_Column");
		//click("label_Action_SearchResults_URN_Column");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(FILED);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", FOR_ALLOCATION);
		// verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify header Moving BulkState move :allocated
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		// clicking Bulk update save button without entering mandatory values
		click("button_Action_BulkUpdate_Save");
		explicitlyWaitForElement("errorMsg_Action_Create_WithOutAnyFields",getTotalWaitTimeInSecs());
		verifyText("For Allocate Reason is required","errorMsg_Action_Create_WithOutAnyFields");
		insertIntoAllTinyMCEinPage("For Allocate Reason Text");
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,	"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",getTotalWaitTimeInSecs());
		click("button_Action_BulkUpdate_UpdatePopup_OK");
		
		endLine(method);
	}
	
	/* Test case 30764:012b. Cancel button in Bulk State Move Popup */
	@Test(priority=17)
	public void mirsapAction_BulkUpdatePopup_Cancel(Method method)
			throws Exception {
		startLine(method);
		// navigate to Action Dash board
		navigateTo(ACTION_DASHBOARD);
		explicitlyWaitForElement("dropDown_Action_ActionDashboard_Search_Type",
				getTotalWaitTimeInSecs());
		// Search Fast Action in Drop down
		selectByVisibleText("dropDown_Action_ActionDashboard_Search_Type",
				SEARCH_TYPE);
		// Enter Title field for search
		// sendKeys("textField_Action_Title",
		// RAISE_FAST_ACTION_MULTIPLE_QUEU_TITLE);
		// Select For Allocation State
		click("button_Action_ActionDashboard_Search_State");
		click("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		// click Search button
		click("button_Action_Search");
		waitForJQuery(driver);
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verifySearchResultTabEnabled();
		// select multiple recored in search result
		click("checkBox_Action_SearchResulttab_Firstrow");
		click("checkBox_Action_SearchResulttab_Secondrow");
		// getting bulk state move URNs
		String firstBulkMoveURN = getText("table_Action_SearchResults_URN");
		String secondBulkMoveURN = getText("table_Action_SearchResults_SecondRow_URN");
		// verify For allocation State
		verifyText(FOR_ALLOCATION, "table_Action_SearchResults_FirstRow_State");
		verifyText(FOR_ALLOCATION, "table_Action_SearchResults_SecondRow_State");

		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
		// click Cancel button
		click("button_Action_BulkSateMovePopup_Cancel");
		validateAllVerificationPoints();
		endLine(method);

	}
	
	/*
	 * Test case 30773:013. Multiple Queue Move-For Allocation to Allocated
	 */
	// Predata: Office need to create
	@Test(priority = 18)
	public void mirsapAction_MultipleQueue_Allocated(Method method)
			throws Exception {
		// navigate to Raise  Action
		startLine(method);
		// navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		verifyTextContains(incidentIdFull, "dropDown_Action_Create_Incident");
		verifyMandatoryFieldsInMirsapAction();
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		// insert text Text field
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		click("link_Action_Create_LinkedAssociation");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		// Verification of data in View Action Fields
		verifyElementPresent("successMsg_Action_Create");

		// Search M Action By ForAllocation state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		// select multiple recored in search result
		//click("label_Action_SearchResults_URN_Column");
		//click("label_Action_SearchResults_URN_Column");
		selectTwoRecordsFromSearchResultAndVerifyState(FOR_ALLOCATION);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");

		// verify header Moving BulkState move :allocated
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		// verify BulkUpdate tab
		verifyElementPresent("tab_Action_BulkUpdate");
		// verify label Moving and allocated URN
		verifyElementPresent("label_Action_BulkStateMove_Allocated");
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + "Fast Action , "
				+ incidentIdFull + " - " + searchResult_ActionSecondURN
				+ "Fast Action to : Allocated",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(ALLOCATED, "label_Action_BulkUpdate_State");
		// verify theme
		//verifyElementPresent("checkBox_Action_Themes_Item1");
		// Verify Tag link
		verifyElementPresent("link_Action_BulkUpdate_MoveAll_Tags");
		// Entering officer name
	/*	explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficer",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_FastAction_QueueAllocatedOfficer",
				CREATE_OFFICER_SURNAME);
		explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",
				getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_StateMove_OfficerName");*/
		click("button_Action_MirsapAction_CreateOfficerOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_BulkUpdateSurname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_BulkUpdateSurname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_BulkUpdateForename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");
		click("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");

		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_MirsapAction_QueueAllocatedOfficerOne");

		// verify cancel button in bulk update page
		verifyElementPresent("button_Action_BulkUpdate_Cancel");
		verifyElementPresent("button_Action_BulkUpdate_Save");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move :allocated
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(FOR_ALLOCATION, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		String firstBulkMoveURN1 = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
		// click Move all button
		//click("button_Action_BulkSateMovePopup_MoveIndividual");
		// get Handle to Move Individual update window
		//String parentWinHandle = getCurrentWindowHandle();
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		/*Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Soft Entity Portlet", "button_Action_BulkSateMovePopup_MoveIndividual", curentWinCount, 2);*/
		// verify cancel button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Cancel");
		// verify Skip button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Skip");
		// verify apply button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Apply");
		// verify MoveIndividual button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual");
		// verify label MoveIndividual
		verifyElementPresent("label_Action_BulkStateMove_MoveIndividual_Allocated");
		// Verify URN and state
		// Move Fast Action A27 in Incident MORSTA16M21 to Allocated
		verifyTextContains("Move Fast Action " + firstBulkMoveURN1
				+ " in Incident " + incidentIdFull + " to Allocated",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state allocted
		// verifyText(ALLOCATED,"label_Action_BulkUpdate_MoveIndividual_State");
		// verify theme
		//verifyElementPresent("checkBox_Action_Themes_Item1");
	
		click("button_Action_MirsapAction_CreateOfficerOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_MoveIndividual_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_MoveIndividual_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_MoveIndividual_Forename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");
		click("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");
	
		
		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");

		explicitlyWaitForElement(
				"button_Action_BulkUpdate_MoveIndividual_Update_OK",
				getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(firstBulkMoveURN1, "successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated", "successMsg_Action_Update");
		// click OK button
		//click("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// getHandle to parent Action Dashboard window
		//String parentWinHandle1 = getCurrentWindowHandle();
		//WebDriver softEntityWinDriver1 = clickAndWaitForBrowserPopup("Action","button_Action_BulkSateMovePopup_MoveIndividual");
		Set<String> allWinHandle = driver.getWindowHandles();
		int curentWinCounts = allWinHandle.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCounts, curentWinCounts-1);
		endLine(method);
	}

	
	/*
	 *  Test Case 30773 014. Multiple Queue Move - Allocated to Submitted
	 */
	@Test(priority=19)
	public void mirsapAction_MultipleQueue_AllocatedTo_Submitted(Method method)
			throws Exception {
		startLine(method);
		// Search Fast Action By Submitted state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Allocated");
		// select multiple recored in search result
		//click("label_Action_SearchResults_URN_Column");
		//click("label_Action_SearchResults_URN_Column");
		selectTwoRecordsFromSearchResultAndVerifyState(ALLOCATED);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popu window
		verifyActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(SUBMITTED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(RECEIVED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_REFERRAL,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", SUBMITTED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInMirsapActionBulkUpdate();
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + ", " + incidentIdFull + " - "
				+ searchResult_ActionSecondURN + ": Submitted",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(SUBMITTED, "label_Action_BulkUpdate_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("SubmittedText");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move :Submitted
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(ALLOCATED, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		searchResult_ActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(SUBMITTED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(RECEIVED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_REFERRAL,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", SUBMITTED);
		// click Move all button
		//click("button_Action_BulkSateMovePopup_MoveIndividual");
		// get Handle to Move Individual update window
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		// verify all element in bulk update individual page
		verifyAllElementsInActionBulkUpdate_Individual();
		// Verify URN and state
		// Move Fast Action A27 in Incident MORSTA16M21 to Allocated
		verifyTextContains("Move " + searchResult_ActionFirstURN
				+ " in Incident " + incidentIdFull + " to Submitted",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state allocted
		// verifyText(ALLOCATED,"label_Action_BulkUpdate_MoveIndividual_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("SubmittedText");

		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");

		explicitlyWaitForElement(
				"button_Action_BulkUpdate_MoveIndividual_Update_OK",
				getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated", "successMsg_Action_Update");
		// click OK button
		//click("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		
		Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCount, curentWinCount-1);
		validateAllVerificationPoints();
		endLine(method);
	}

	/*
	 * Test Case 30781: 015. Multiple Queue Move - Submitted to Received
	 */
	@Test(priority=20)
	public void mirsapAction_MultipleQueue_SubmittedTo_Received(Method method)
			throws Exception {
		startLine(method);
		// Search Fast Action By Submitted state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Submitted");
		// verifySearchResultTabEnabled();
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(SUBMITTED);
		
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popu window
		verifyActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(RECEIVED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", RECEIVED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInMirsapActionBulkUpdate();
		// verify element Receive instructions
		verifyElementPresent("textField_Action_FastAction_Edit_ReceiversInstructions");
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + ", " + incidentIdFull
				+ " - " + searchResult_ActionSecondURN + ": Submitted",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(RECEIVED, "label_Action_BulkUpdate_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("SubmittedText");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move :Submitted
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(SUBMITTED, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		searchResult_ActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyActionBulkUpdate_Popup();
		getAllListItemAndVerify(RECEIVED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", RECEIVED);
		// click Move all button
		//click("button_Action_BulkSateMovePopup_MoveIndividual");
		// get Handle to Move Individual update window
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		// verify all element in bulk update individual page
		verifyAllElementsInActionBulkUpdate_Individual();
		// Verify URN and state
		verifyTextContains("Move " + searchResult_ActionFirstURN
				+ " in Incident " + incidentIdFull + " to Received",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state allocted
		// verifyText(ALLOCATED,"label_Action_BulkUpdate_MoveIndividual_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("Received Text");

		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");

		explicitlyWaitForElement(
				"button_Action_BulkUpdate_MoveIndividual_Update_OK",
				getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated", "successMsg_Action_Update");
		// click OK button
		//click("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// getHandle to parent Action Dashboard window
		Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCount, curentWinCount-1);
		validateAllVerificationPoints();
		endLine(method);
	}
	
	/*
	 * Test Case 30777: 016. Multiple Queue Move - Received to Resulted
	 */	
	@Test(priority=21)
	public void mirsapAction_MultipleQueue_ReceivedTo_Resulted(Method method)
			throws Exception {
		startLine(method);
		// Search Fast Action By Submitted state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Received");
		// verifySearchResultTabEnabled();
		// select multiple recored in search result
		click("label_Action_SearchResults_URN_Column");
		click("label_Action_SearchResults_URN_Column");
		selectTwoRecordsFromSearchResultAndVerifyState(RECEIVED);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popu window
		verifyActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(RESULTED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(PARTIALLY_RESULTED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(PENDED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", RESULTED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInMirsapActionBulkUpdate();
		// verify element Receive instructions
		verifyElementPresent("textField_Action_FastAction_Edit_ReceiversInstructions");
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + ", " + incidentIdFull
				+ " - " + searchResult_ActionSecondURN + ":  Resulted",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(RESULTED, "label_Action_BulkUpdate_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("SubmittedText");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move :Submitted
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(RECEIVED, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		searchResult_ActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(RESULTED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(PARTIALLY_RESULTED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(PENDED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", RESULTED);
		// click Move all button
		//click("button_Action_BulkSateMovePopup_MoveIndividual");
		// get Handle to Move Individual update window
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		// verify all element in bulk update individual page
		verifyAllElementsInActionBulkUpdate_Individual();
		// Verify URN and state
		verifyTextContains("Move " + searchResult_ActionFirstURN
				+ " in Incident " + incidentIdFull + " to Received",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("Received Text");
		//enter surname of officer
		/*explicitlyWaitForElement("textField_Action_MirsapAction_Edit_RecordedOfficerOne",getTotalWaitTimeInSecs());
		sendKeys("textField_Action_MirsapAction_Edit_RecordedOfficerOne",
				CREATE_OFFICER_SURNAME);*/
		
		click("button_Action_MirsapAction_RecordedOfficerfirstOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_MoveIndividual_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_MoveIndividual_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_MoveIndividual_Forename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");

		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");
		explicitlyWaitForElement("button_Action_BulkUpdate_MoveIndividual_Update_OK",getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated", "successMsg_Action_Update");
		// click OK button
		//click("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// getHandle to parent Action Dashboard window
		Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCount, curentWinCount-1);
		validateAllVerificationPoints();
		endLine(method);
	}

	
	/*
	 * Test case 30779:019. Multiple Queue Move-For Allocation to For Referral 
	 */
	@Test(priority = 22)
	public void mirsapAction_MultipleQueue_ForAllocationTo_ForReferral(Method method)
			throws Exception {
		startLine(method);
		// Search mirsap action By for allocation state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		// select multiple recored in search result
		click("label_Action_SearchResults_URN_Column");
		click("label_Action_SearchResults_URN_Column");
		selectTwoRecordsFromSearchResultAndVerifyState(FOR_ALLOCATION);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popup window
		verifyActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(ALLOCATED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_REFERRAL,	"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(PENDED,	"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", FOR_REFERRAL);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInMirsapActionBulkUpdate();
		// verify element For referral text area
		verifyElementPresent("textField_Action_MirsapAction_Edit_ForReferral");
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + ", " + incidentIdFull
				+ " - " + searchResult_ActionSecondURN + ": For Referral",
				"label_Action_BulkStateMove_Allocated");
		// verify state For Referral
		verifyText(FOR_REFERRAL, "label_Action_BulkUpdate_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("For Referral Text");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move 
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(ALLOCATION, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		searchResult_ActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyActionBulkUpdate_Popup();
		getAllListItemAndVerify(ALLOCATED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_REFERRAL,	"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(PENDED,	"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from drop down
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", FOR_REFERRAL);
		// click Move all button
		//click("button_Action_BulkSateMovePopup_MoveIndividual");
		
		// get Handle to Move Individual update window
		//String currentParentWindowHandle = getCurrentWindowHandle();
		//ArrayList<String> parentWinHandler1 = new ArrayList<String>();
		//parentWinHandler1.add(currentParentWindowHandle);
		//WebDriver childWindDriver = clickAndWaitForChildWindow(MIRSAP_ACTION_TITLE_WINDOW, parentWinHandler1, "button_Action_BulkSateMovePopup_MoveIndividual", 2, 2);
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
	
		verifyAllElementsInActionBulkUpdate_Individual();
		// Verify URN and state
		verifyTextContains("Move " + searchResult_ActionFirstURN
				+ " in Incident " + incidentIdFull + " to ",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state For referral
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("For Referral Text");
		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");

		explicitlyWaitForElement(
				"button_Action_BulkUpdate_MoveIndividual_Update_OK",
				getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated", "successMsg_Action_Update");
		// click OK button
		//click("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCount, curentWinCount-1);
		validateAllVerificationPoints();
		endLine(method);
	}
	
	/*
	 * Test Case 30776: 021. Multiple Queue Move - For Allocation to Pended
	 
	//@Test(priority = 23)
	public void mirsapAction_MultipleQueueMove_ForAllocation_ToPended(Method method) throws Exception{
		startLine(method);
		// Search mirsap action By for allocation state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		// select multiple recored in search result
		click("label_Action_SearchResults_URN_Column");
		click("label_Action_SearchResults_URN_Column");
		selectTwoRecordsFromSearchResultAndVerifyState(FOR_ALLOCATION);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popup window
		verifyActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(ALLOCATED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_REFERRAL,	"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(PENDED,	"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", PENDED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInMirsapActionBulkUpdate();
		// verify element For referral text area
		verifyElementPresent("textField_Action_MirsapAction_Edit_ForReferral");
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
					+ searchResult_ActionFirstURN + ", " + incidentIdFull
					+ " - " + searchResult_ActionSecondURN + ": For Referral",
					"label_Action_BulkStateMove_Allocated");
		// verify state For Referral
		verifyText(PENDED, "label_Action_BulkUpdate_State");
		
		//insert pend date (Current date plus 1 day)
		String currentDate = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
		SimpleDateFormat pendDate = new SimpleDateFormat("dd/MM/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(pendDate.parse(currentDate));
		c.add(Calendar.DATE, 1);  // number of days to add
		currentDate = pendDate.format(c.getTime());		
		
		sendKeys("textField_Action_MirsapAction_PendDate",currentDate);
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("Pended Reason Text");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move 
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
					getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(FOR_REFERRAL, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		searchResult_ActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyActionBulkUpdate_Popup();
		getAllListItemAndVerify(ALLOCATED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_REFERRAL,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(PENDED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from drop down
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", PENDED);
		// click Move all button
		//click("button_Action_BulkSateMovePopup_MoveIndividual");
		// get Handle to Move Individual update window
		ArrayList<String> parentWinHandler1 = new ArrayList<String>();
		parentWinHandler1.add(getCurrentWindowHandle());
		clickAndWaitForChildWindow(MIRSAP_ACTION_TITLE_WINDOW, parentWinHandler1, "icon_Action_Searchresult_View", 1, 2);
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		//getHandleToWindow("Soft Entity Portlet");
		// verify all element in bulk update individual page
		verifyAllElementsInActionBulkUpdate_Individual();
		// Verify URN and state
		verifyTextContains("Move " + searchResult_ActionFirstURN+ " in Incident " + incidentIdFull + " to ",
						"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state For referral
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("For pended Text");
		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");
		explicitlyWaitForElement("button_Action_BulkUpdate_MoveIndividual_Update_OK",getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(searchResult_ActionFirstURN,"successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated","successMsg_Action_Update");
		// click OK button
		//click("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// getHandle to parent Action Dashboard window
		//getHandleToWindow("Action Dashboard");
		Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCount, 1);
		validateAllVerificationPoints();
		endLine(method);
		
		endLine(method);
	}*/
	
	/*
	 * Test Case 30763: 022. Multiple Queue Move - Received to Partially Resulted
	 
	//@Test(priority = 24)
	public void mirsapAction_MultipleQueueMove_Received_ToPartiallyResulted(Method method) throws Exception{
		startLine(method);
		// Search mirsap action By for allocation state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Received");
		// select multiple recored in search result
		click("label_Action_SearchResults_URN_Column");
		click("label_Action_SearchResults_URN_Column");
		selectTwoRecordsFromSearchResultAndVerifyState(RECEIVED);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popup window
		verifyActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(RESULTED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(PARTIALLY_RESULTED,	"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(PENDED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", PARTIALLY_RESULTED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInMirsapActionBulkUpdate();
		// verify element For referral text area
		verifyElementPresent("textField_Action_MirsapAction_Edit_ForReferral");
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
					+ searchResult_ActionFirstURN + ", " + incidentIdFull
					+ " - " + searchResult_ActionSecondURN + ": For Referral",
					"label_Action_BulkStateMove_Allocated");
		// verify state For Referral
		verifyText(PARTIALLY_RESULTED, "label_Action_BulkUpdate_State");
		
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("Partially Resulted Text");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move 
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
					getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(FOR_REFERRAL, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		searchResult_ActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyActionBulkUpdate_Popup();
		getAllListItemAndVerify(RESULTED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(PARTIALLY_RESULTED,	"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_ALLOCATION,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(PENDED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from drop down
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", PARTIALLY_RESULTED);
		// click Move all button
		
		String currentParentWindowHandle = getCurrentWindowHandle();
		// get Handle to Move Individual update window
		ArrayList<String> parentWinHandler1 = new ArrayList<String>();
		parentWinHandler1.add(currentParentWindowHandle);
		WebDriver childWindDriver = clickAndWaitForChildWindow(MIRSAP_ACTION_TITLE_WINDOW, parentWinHandler1, "button_Action_BulkSateMovePopup_MoveIndividual", 2, 2);
		// verify all element in bulk update individual page
		verifyAllElementsInActionBulkUpdate_Individual();
		// Verify URN and state
		verifyTextContains("Move " + searchResult_ActionFirstURN+ " in Incident " + incidentIdFull + " to ",
						"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state For referral
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("Partially Resulted Text");
		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");
		explicitlyWaitForElement("button_Action_BulkUpdate_MoveIndividual_Update_OK",getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(searchResult_ActionFirstURN,"successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated","successMsg_Action_Update");
		// click OK button
		click("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// getHandle to parent Action Dashboard window
		//closePopupWindowAndNavigateToParentWindow(MIRSAP_ACTION_TITLE_WINDOW, ACTION_DASHBOARD_TITLE_WINDOW);
		closePopupWindowAndNavigateToParentWindowHandle(MIRSAP_ACTION_TITLE_WINDOW, currentParentWindowHandle);
		validateAllVerificationPoints();
		endLine(method);
	}
	*/
	/*
    Test case 30775:017. Multiple Queue Move- Resulted to Filed
	 */
	@Test(priority = 25)
	public void mirsapAction_MultipleQueueMove_Resulted_ToFiled(Method method) throws Exception{
		startLine(method);
		// Search Fast Action By Submitted state
		//explicitlyWaitForElement("checkBox_Action_ActionDashboard_Search_State_Resulted", getTotalWaitTimeInSecs());
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Resulted");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(RESULTED);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popu window
		verifyActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(FOR_ALLOCATION,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", FILED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInMirsapActionBulkUpdate();
		// verify element Receive instructions
		//verifyElementPresent("textField_Action_FastAction_Edit_ReceiversInstructions");
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + ", " + incidentIdFull
				+ " - " + searchResult_ActionSecondURN + ": Filed",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(FILED, "label_Action_BulkUpdate_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("Submitted Text");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move :Submitted
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(RESULTED, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		searchResult_ActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyActionBulkUpdate_Popup();
		getAllListItemAndVerify(FOR_ALLOCATION,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from drop down
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", FILED);
		// click Move all button
		/*click("button_Action_BulkSateMovePopup_MoveIndividual");*/
		// get Handle to Move Individual update window
		/*getHandleToWindow("Soft Entity Portlet");*/
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		// verify all element in bulk update individual page
		verifyAllElementsInActionBulkUpdate_Individual();
		// Verify URN and state
		verifyTextContains("Move " + searchResult_ActionFirstURN
				+ " in Incident " + incidentIdFull + " to ",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state allocted
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("Received Text");
		explicitlyWaitForElement("button_Action_BulkUpdate_MoveIndividual",
				getTotalWaitTimeInSecs());
		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");

		explicitlyWaitForElement(
				"button_Action_BulkUpdate_MoveIndividual_Update_OK",
				getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated", "successMsg_Action_Update");
		// click OK button
		/*click("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// getHandle to parent Action Dashboard window
		getHandleToWindow("Action Dashboard");*/
		Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCount, curentWinCount-1);
		endLine(method);
	}
	/*
    Test case 30778:020. Multiple Queue Move-For Referral to Referred
	 */
	
	@Test(priority = 26)
	public void mirsapAction_MultipleQueue_ForRefferelTo_Referred(Method method)
			throws Exception {
		startLine(method);
		MoveAll_ForAllocationTo_ForReferral();
		// Search Fast Action By Submitted state
		//explicitlyWaitForElement("checkBox_Action_ActionDashboard_Search_State_Allocated", getTotalWaitTimeInSecs());
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForReferral");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(FOR_REFERRAL);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popu window
		verifyActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(FOR_ALLOCATION,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(REFERRED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", REFERRED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInMirsapActionBulkUpdate();
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + ", " + incidentIdFull
				+ " - " + searchResult_ActionSecondURN + ": Referred",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(REFERRED, "label_Action_BulkUpdate_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("Reffred Text");
		
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move :Submitted
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(FOR_REFERRAL, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		searchResult_ActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyActionBulkUpdate_Popup();
		getAllListItemAndVerify(FOR_ALLOCATION,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(REFERRED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", REFERRED);
		// click Move all button
		/*click("button_Action_BulkSateMovePopup_MoveIndividual");*/
		// get Handle to Move Individual update window
		/*getHandleToWindow("Soft Entity Portlet");*/
		/*String currentParentWindowHandle = getCurrentWindowHandle();
		ArrayList<String> parentWinHandler1 = new ArrayList<String>();
		parentWinHandler1.add(currentParentWindowHandle);
		WebDriver childWindDriver = clickAndWaitForChildWindow(MIRSAP_ACTION_TITLE_WINDOW, parentWinHandler1, "button_Action_BulkSateMovePopup_MoveIndividual", 2, 2);
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");*/
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		// verify all element in bulk update individual page
		verifyAllElementsInActionBulkUpdate_Individual();
		// Verify URN and state
		// Move Fast Action A27 in Incident MORSTA16M21 to Allocated
		verifyTextContains("Move " + searchResult_ActionFirstURN
				+ " in Incident " + incidentIdFull + " to Referred",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state allocted
		// verifyText(ALLOCATED,"label_Action_BulkUpdate_MoveIndividual_State");
		//add pended date
		//String futureDates= getTomorrowDataStamp();
		//sendKeys("textField_Action_MirsapAction_PendDate",futureDate);
		
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("PendedText");
		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");

		explicitlyWaitForElement(
				"button_Action_BulkUpdate_MoveIndividual_Update_OK",
				getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated", "successMsg_Action_Update");
		// click OK button
		/*click("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// getHandle to parent Action Dashboard window
		getHandleToWindow("Action Dashboard");*/
		Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCount, curentWinCount-1);
		endLine(method);
	}

	
    /*Test case 30776:021. Multiple Queue Move-For Allocation to Pended*/
	 
	@Test (priority = 27) 
	public void mirsapAction_MultipleQueue_ForAllocationTo_Pended(Method method)
			throws Exception {
		startLine(method);
		// Search Fast Action By Submitted state
		//explicitlyWaitForElement("checkBox_Action_ActionDashboard_Search_State_Allocated", getTotalWaitTimeInSecs());
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(FOR_ALLOCATION);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popu window
		verifyActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(ALLOCATED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_REFERRAL,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", PENDED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInMirsapActionBulkUpdate();
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + ", " + incidentIdFull
				+ " - " + searchResult_ActionSecondURN + ": Pended",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(PENDED, "label_Action_BulkUpdate_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("Pended Text");
		
		String futureDate= getTomorrowDataStamp();
		sendKeys("textField_Action_MirsapAction_PendDate",futureDate);
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move :Submitted
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(FOR_ALLOCATION, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		searchResult_ActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyActionBulkUpdate_Popup();
		getAllListItemAndVerify(ALLOCATED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_REFERRAL,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", PENDED);
		// click Move all button
		//click("button_Action_BulkSateMovePopup_MoveIndividual");
		// get Handle to Move Individual update window
		//getHandleToWindow("Soft Entity Portlet");
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		// verify all element in bulk update individual page
		verifyAllElementsInActionBulkUpdate_Individual();
		// Verify URN and state
		// Move Fast Action A27 in Incident MORSTA16M21 to Allocated
		verifyTextContains("Move " + searchResult_ActionFirstURN
				+ " in Incident " + incidentIdFull + " to Pended",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state allocted
		// verifyText(ALLOCATED,"label_Action_BulkUpdate_MoveIndividual_State");
		//add pended date
		
		explicitlyWaitForElement(
				"textField_Action_MirsapAction_PendDate",
				getTotalWaitTimeInSecs());
		String futureDates= getTomorrowDataStamp();
		sendKeys("textField_Action_MirsapAction_PendDate",futureDates);
		
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("PendedText");
		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");

		explicitlyWaitForElement(
				"button_Action_BulkUpdate_MoveIndividual_Update_OK",
				getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated", "successMsg_Action_Update");
		// click OK button
		Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCount, curentWinCount-1);
		endLine(method);
	}

	
	/*
    Test case 30763:022. Multiple Queue Move- Received to Partially Resulted
	 */
	@Test (priority = 31) 
	public void mirsapAction_MultipleQueue_ReceivedTo_PartiallyResulted(Method method)
			throws Exception {
		startLine(method);
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		bulkState_Movell_Allocated("Test");
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Allocated");
		bulkState_Movell_Submitted("Test");
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Submitted");
		bulkState_Movell_Received("Test");
		// Search Fast Action By Submitted state
		//explicitlyWaitForElement("checkBox_Action_ActionDashboard_Search_State_Allocated", getTotalWaitTimeInSecs());
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Received");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(RECEIVED);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popu window
		verifyActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(RESULTED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_ALLOCATION,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(PENDED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", PARTIALLY_RESULTED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInMirsapActionBulkUpdate();
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + ", " + incidentIdFull
				+ " - " + searchResult_ActionSecondURN + ": Partially Resulted",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(PARTIALLY_RESULTED, "label_Action_BulkUpdate_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("Resulted Text");
		/*explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueRecordedOfficer",
				getTotalWaitTimeInSecs());
		sendKeysWithDriver("textfield_Action_FastAction_QueueRecordedOfficer",CREATE_OFFICER_SURNAME);
		explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",
				getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_StateMove_OfficerName");*/
		click("button_Action_MirsapAction_RecordedOfficerfirstOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_BulkUpdateSurname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_BulkUpdateSurname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_BulkUpdateForename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");
		/*click("textfield_Action_MirsapAction_QueueRecordedOfficerOne");
	
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_MirsapAction_QueueRecordedOfficerOne");*/

		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move :Submitted
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(RECEIVED, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		// getting first URN from search table
		searchResult_ActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyActionBulkUpdate_Popup();
		getAllListItemAndVerify(RESULTED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_ALLOCATION,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(PENDED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", PARTIALLY_RESULTED);
		// click Move all button
		/*click("button_Action_BulkSateMovePopup_MoveIndividual");*/
		// get Handle to Move Individual update window
		/*getHandleToWindow("Soft Entity Portlet");*/
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		// verify all element in bulk update individual page
		verifyAllElementsInActionBulkUpdate_Individual();
		// Verify URN and state
		// Move Fast Action A27 in Incident MORSTA16M21 to Allocated
		verifyTextContains("Move " + searchResult_ActionFirstURN
				+ " in Incident " + incidentIdFull + " to Pended",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		click("button_Action_MirsapAction_RecordedOfficerfirstOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_MoveIndividual_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_MoveIndividual_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_MoveIndividual_Forename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");
	/*	click("textfield_Action_MirsapAction_QueueRecordedOfficerOne");
	
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_MirsapAction_QueueRecordedOfficerOne");*/
		
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("ResultedText");
		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");

		explicitlyWaitForElement(
				"button_Action_BulkUpdate_MoveIndividual_Update_OK",
				getTotalWaitTimeInSecs());
		// verify OK button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// verify URN
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_Update");
		// Verify update field
		verifyTextContains("updated", "successMsg_Action_Update");
		// click OK button
		/*click("button_Action_BulkUpdate_MoveIndividual_Update_OK");
		// getHandle to parent Action Dashboard window
		getHandleToWindow("Action Dashboard");*/
		Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCount, curentWinCount-1);
		endLine(method);
	}

	
	//Test case 30765:012c. Cancel button in Bulk Update screen
	/*
	@Test(priority = 29)
	public void mirsapAction_BulkUpdate_Cancel(Method method) throws Exception {
		startLine(method);
		// Search Fast Action By ForAllocation state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(FOR_ALLOCATION);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popu window
		verifyActionBulkUpdate_Popup();
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInMirsapActionBulkUpdate();
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + "Fast Action , "
				+ incidentIdFull + " - " + searchResult_ActionSecondURN
				+ "Fast Action to : Allocated",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(ALLOCATED, "label_Action_BulkUpdate_State");
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficer",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_FastAction_QueueAllocatedOfficer",
				CREATE_OFFICER_SURNAME);
		//explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",
		//		getTotalWaitTimeInSecs());
		//clickAndWait("textfield_Action_StateMove_OfficerName");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Cancel");
		endLine(method);

	}*/
	
/*
	
    Test case 9512:009. Multiple Queue Move-Move ALL, Apply & Move Individual & Skip
	 */
	@Test(priority = 30)
	public void mirsapAction_MultipleQueue_MoveAllAndMoveIndividual(Method method)
			throws Exception {
		startLine(method);
		// raise Bulk fast action
		//raiseActionBulkRecord();
		// Search Fast Action By Submitted state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(FOR_ALLOCATION);
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popu window
		verifyActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(ALLOCATED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		verifyAllElementsInMirsapActionBulkUpdate();
		// verify element Receive instructions
		verifyElementPresent("textField_Action_FastAction_Edit_ReceiversInstructions");
		// verifying incidents,URN,State and Fast Action
		// verify label Moving and allocated URN
		verifyElementPresent("label_Action_BulkStateMove_Allocated");
		// verifying incidents,URN,State and Fast Action
		verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + "Fast Action , "
				+ incidentIdFull + " - " + searchResult_ActionSecondURN
				+ "Fast Action to : Allocated",
				"label_Action_BulkStateMove_Allocated");
		// verify state allocted
		verifyText(ALLOCATED, "label_Action_BulkUpdate_State");
		// verify theme
		//verifyElementPresent("checkBox_Action_Themes_Item1");
		// Verify Tag link
		verifyElementPresent("link_Action_BulkUpdate_MoveAll_Tags");
		// Entering officer name
		click("button_Action_MirsapAction_CreateOfficerOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_BulkUpdateSurname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_BulkUpdateSurname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_BulkUpdateForename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");
		click("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");

		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_MirsapAction_QueueAllocatedOfficerOne");
		//sendKeysAndEnter("textfield_Action_FastAction_QueueAllocatedOfficer",OFFICER_NAME);
		//sendKeys("textfield_Action_FastAction_QueueAllocatedOfficer",OFFICER_NAME);
		//sendKeysWithDriver("textfield_Action_FastAction_QueueAllocatedOfficer",CREATE_OFFICER_SURNAME);
		
		/*explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",
				getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_StateMove_OfficerName");*/
		// verify state allocted
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

		// MoveIndividual StateMove
		// verify header Moving BulkState move :Submitted
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
		// verify For allocation State
		verifyText(FOR_ALLOCATION, "table_Action_SearchResults_FirstRow_State");
		// select One recored in search result
		verifyElementPresent("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Firstrow");
		clickAndWait("checkBox_Action_SearchResulttab_Secondrow");
		clickAndWait("checkBox_Action_SearchResulttab_Thirdrow");
		// getting first URN from search table
		searchResult_ActionFirstURN = getText("table_Action_SearchResults_URN");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify element in popup window
		verifyActionBulkUpdate_Popup();
		getAllListItemAndVerify(ALLOCATED,
				"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from drop down
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// click Move all button
		/*click("button_Action_BulkSateMovePopup_MoveIndividual");*/
		// get Handle to Move Individual update window
		/*getHandleToWindow("Soft Entity Portlet");*/
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","button_Action_BulkSateMovePopup_MoveIndividual");
		// verify all element in bulk update individual page
		verifyAllElementsInActionBulkUpdate_Individual();
		// Verify URN and state
		verifyTextContains("Move " + searchResult_ActionFirstURN
				+ " in Incident " + incidentIdFull + " to ",
				"label_Action_BulkStateMove_MoveIndividual_Allocated");
		// verify state allocted
		// Entering officer name
		click("button_Action_MirsapAction_CreateOfficerOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_MoveIndividual_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_MoveIndividual_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_MoveIndividual_Forename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");
		click("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");

		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_MirsapAction_QueueAllocatedOfficerOne");
		explicitlyWaitForElement("button_Action_BulkUpdate_MoveIndividual",
				getTotalWaitTimeInSecs());
		// click Move Individual button
		click("button_Action_BulkUpdate_MoveIndividual");
		click("button_Action_BulkUpdate_MoveIndividual");
		click("button_Action_BulkUpdate_MoveIndividual");
		Set<String> allWinHandles = driver.getWindowHandles();
		int curentWinCount = allWinHandles.size();
		driver = clickAndWaitForBrowserPopup("Action Dashboard", "button_Action_BulkUpdate_MoveIndividual_Update_OK", curentWinCount, curentWinCount-1);
		endLine(method);
		
	}
	//THIS TEST CASE WILL FAIL- VALID DEFECT EXISTS
	//Action Dashboard summary icon count increments on Allocated officer home page
	//Action Dashboard summary icon count decrements after changing state from Allocated to future state
	//Test case 26489:Action Dashboard Summary Icon on Home Page
	//@Test(priority = 20)
	public void actionDashboardSummaryIcon(Method method) throws Exception {
		startLine(method);

		//System Admin- Raise mirsap Action
		raiseAndGetMirsapActionURN();

		//System Admin-To reuse the method create an instance of Fast Action class
		FastAction fastAct= new FastAction(driver);
		fastURN=fastAct.raiseAndGetFastActionURN();

		//Normal User- Login as Allocated officer and get the count before Allocation
		String countBeforeAllocation=actionDashboardSummaryIconCheck();
		Report.info("countBeforeAllocation is :" + countBeforeAllocation);

		//System Admin-change state of Mirsap Action / Allocate an officer to Mirsap Action
		viewAndGetMirsapActionURN(getMirsapActionURN);
		explicitlyWaitForElement("button_Action_ChangeState",getTotalWaitTimeInSecs());
		//change state to allocated and check mandatory fields
		clickAndWait("button_Action_ChangeState");
		changeStateAllocated(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);

		//System Admin- change state of Fast Action / Allocate an officer to Mirsap Action
		FastAction fastAct1= new FastAction(driver);
		fastAct1.viewAndGetFastActionURNID(fastURN);
		explicitlyWaitForElement("button_Action_ChangeState",getTotalWaitTimeInSecs());
		//change state to allocated and check mandatory fields
		clickAndWait("button_Action_ChangeState");
		fastAct1.changeStateAllocated(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);

		//NormalUser- Login as Allocated officer and get the count After Allocation( 1 FA and 1 MirsapAction Allocated and hence +2)
		String countAfterAllocation= actionDashboardSummaryIconCheck();
		Report.info("countAfterAllocation is :" + countAfterAllocation);
		Assert.assertEquals(String.valueOf(Integer.parseInt(countBeforeAllocation)+2), countAfterAllocation);

		//As System Admin - change state of Misap from Allocated to Submitted
		//change state of Mirsap Action / Allocate an officer to Mirsap Action
		viewAndGetMirsapActionURN(getMirsapActionURN);
		explicitlyWaitForElement("button_Action_ChangeState",getTotalWaitTimeInSecs());
		//change state to allocated and check mandatory fields
		clickAndWait("button_Action_ChangeState");
		changeStateSubmitted(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE,TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);

		//Normal User- Login as Allocated officer and get the count After Submission
		String countAfterSubmission=actionDashboardSummaryIconCheck();
		Report.info("countAfterSubmission is :" + countAfterSubmission);
		Assert.assertEquals(String.valueOf(Integer.parseInt(countAfterAllocation)-1), countAfterSubmission);		
		endLine(method);

	}
	/*Test case 30774:018. Audit Log- Multiple Queue Moves*/
	@Test(priority = 32)
	public void mirsapAction_AuditLog_MultipleStateMoves(Method method)
			throws Exception {

		startLine(method);
		// verify Audit Multiple Allocated
		AuditLogSearch auditLog = new AuditLogSearch(driver);
		auditLog.auditLogSearchByActivity(ALLOCATED_ACTION);
		verifyAuditLogSearchResult(ALLOCATED_ACTION);

		// verify Audit Single Submitted
		auditLog.auditLogSearchByActivity(SUBMITTED_ACTION);
		verifyAuditLogSearchResult(SUBMITTED_ACTION);

		// verify Audit Single Resulted
		auditLog.auditLogSearchByActivity(RESULTED_ACTION);
		verifyAuditLogSearchResult(RESULTED_ACTION);

		// verify Audit Single Received
		auditLog.auditLogSearchByActivity(RECEIVED_ACTION);
		verifyAuditLogSearchResult(RECEIVED_ACTION);

		// verify Audit Single Filed
		auditLog.auditLogSearchByActivity(FILED_ACTION);
		verifyAuditLogSearchResult(FILED_ACTION);
		
		//verify Audit - Single Partially Resulted 
		// This will currently FAIL as there is no activity description for Partially Resulted 
		auditLog.auditLogSearchByActivity(PARTIALLY_RESULTED_ACTION);
		verifyAuditLogSearchResult(PARTIALLY_RESULTED_ACTION);

		endLine(method);

	}
	
	public String actionDashboardSummaryIconCheck() throws Exception{

		// Log Out Method
		signOut();

		//Relogin as allocated officer
		reLoginasAllocatedOfficer();
		waitForPageLoad();

		explicitlyWaitForElement("icon_Home_Workspace_ActionDashboardSummary",
				getTotalWaitTimeInSecs());
		sleep(2000);

		// Icon Count checking
		click("icon_Home_Workspace_ActionDashboardSummary");

		String count=getText("icon_Home_Workspace_ActionDashboardSummary");
		Report.info("Action Dashboarc Summary Count :"+count);

		//check actionDashboardSummaryCount and sign out
		signOut();

		//Relogin as System Admin
		loginInIEAndSelectIncident();

		return count;


	}
	
	/*
	 * Test case 30770:023. Large Text Fields and Special Characters in Move All-Bulk Update Screen
	 */
	@Test(priority = 29)
	public void mirsapAction_Large_Fields_MoveAll_BulkUpdate(Method method)
			throws Exception {
		startLine(method);
		String docContent = readTextFile(System.getProperty("user.dir")
				+ "\\hugeDataForTextField.txt");

		// Search M Action By ForAllocation state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		click("label_Action_SearchResults_URN_Column");
		click("label_Action_SearchResults_URN_Column");
		bulkState_Movell_Allocated(docContent);

		// Inseert Large Text fields during State Move from Allocated to Submitted 
		// Search M Action By Allocated state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Allocated");
		click("label_Action_SearchResults_URN_Column");
		click("label_Action_SearchResults_URN_Column");
		bulkState_Movell_Submitted(docContent);

		// Inseert Large Text fields during State Move from Submitted to Received 
		// Search M Action By Submitted state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_Submitted");
		click("label_Action_SearchResults_URN_Column");
		click("label_Action_SearchResults_URN_Column");
		bulkState_Movell_Received(docContent);

		endLine(method);
	}

	public void bulkState_Movell_Allocated(String textToInsert) throws Exception{
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(FOR_ALLOCATION);
		click("checkBox_Action_SearchResulttab_Thirdrow");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", ALLOCATED);
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");

		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");

		// verify header Moving BulkState move :allocated
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		// verify BulkUpdate tab
		verifyElementPresent("tab_Action_BulkUpdate");
		// verify label Moving and allocated URN
		explicitlyWaitForElement("label_Action_BulkStateMove_Allocated",
				getTotalWaitTimeInSecs());
		verifyElementPresent("label_Action_BulkStateMove_Allocated");
		// verifying incidents,URN,State and Fast Action
		/*verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + "Fast Action , "
				+ incidentIdFull + " - " + searchResult_ActionSecondURN
				+ "Fast Action to : Allocated",
				"label_Action_BulkStateMove_Allocated");*/
		// verify state allocted
		verifyText(ALLOCATED, "label_Action_BulkUpdate_State");
		// verify theme
		//verifyElementPresent("checkBox_Action_Themes_Item1");
		// Verify Tag link
		verifyElementPresent("link_Action_BulkUpdate_MoveAll_Tags");
		click("button_Action_MirsapAction_CreateOfficerOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_BulkUpdateSurname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_BulkUpdateSurname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_BulkUpdateForename",
				CREATE_OFFICER_FORENAME);
		verifyElementPresent("button_Action_MirsapAction_CreateOne");
		// clicking Bulk update save button
		click("button_Action_MirsapAction_CreateOne");
		verifyElementPresent("button_Action_BulkUpdate_Save");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");
	}
	

	public void bulkState_Movell_Submitted(String textToInsert) throws Exception{
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(ALLOCATED);
		click("checkBox_Action_SearchResulttab_Thirdrow");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", SUBMITTED);
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");

		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");

		// verify header Moving BulkState move :Submitted
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		// verify BulkUpdate tab
		verifyElementPresent("tab_Action_BulkUpdate");
		// verify label Moving and allocated URN
		explicitlyWaitForElement("label_Action_BulkStateMove_Allocated",
				getTotalWaitTimeInSecs());
		verifyElementPresent("label_Action_BulkStateMove_Allocated");
		// verifying incidents,URN,State and Fast Action
	/*	verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + "Action , "
				+ incidentIdFull + " - " + searchResult_ActionSecondURN
				+ "Action to : Submitted",
				"label_Action_BulkStateMove_Allocated");*/
		// verify state allocted
		verifyText(SUBMITTED, "label_Action_BulkUpdate_State");

		// Verify Tag link
		verifyElementPresent("link_Action_BulkUpdate_MoveAll_Tags");
		insertIntoAllTinyMCEinPage(textToInsert);
		verifyElementPresent("button_Action_BulkUpdate_Save");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");
	}

	public void bulkState_Movell_Received(String textToInsert) throws Exception{
		// select multiple recored in search result
		selectTwoRecordsFromSearchResultAndVerifyState(SUBMITTED);
		click("checkBox_Action_SearchResulttab_Thirdrow");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",
				getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		// select state from dropdown
		selectByVisibleText(
				"dropDown_Action_BulkStateMove_Popup_AllowedStates", RECEIVED);
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");

		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");

		// verify header Moving BulkState move :Submitted
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
		// verify BulkUpdate tab
		verifyElementPresent("tab_Action_BulkUpdate");
		// verify label Moving and allocated URN
		verifyElementPresent("label_Action_BulkStateMove_Allocated");
		// verifying incidents,URN,State and Fast Action
		/*verifyTextContains("Moving : " + incidentIdFull + " - "
				+ searchResult_ActionFirstURN + "Action , "
				+ incidentIdFull + " - " + searchResult_ActionSecondURN
				+ "Action to : RECEIVED",
				"label_Action_BulkStateMove_Allocated");*/
		// verify state allocted
		verifyText(RECEIVED, "label_Action_BulkUpdate_State");
		// Verify Tag link
		verifyElementPresent("link_Action_BulkUpdate_MoveAll_Tags");
		sendKeys("textField_Action_MirsapAction_Edit_ReceiversInstructions",textToInsert);
		verifyElementPresent("button_Action_BulkUpdate_Save");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");
	}
	

	public void MoveAll_ForAllocationTo_ForReferral()
			throws Exception {
		// Search mirsap action By for allocation state
		searchMirsapActionAndSelectByState("checkBox_Action_ActionDashboard_Search_State_ForAllocation");
		// select multiple recored in search result
		click("label_Action_SearchResults_URN_Column");
		click("label_Action_SearchResults_URN_Column");
		selectTwoRecordsFromSearchResultAndVerifyState(FOR_ALLOCATION);
		click("checkBox_Action_SearchResulttab_Thirdrow");
		explicitlyWaitForElement("icon_Action_Searchresult_BulkStateMove",getTotalWaitTimeInSecs());
		// clicking Bulk State move icon
		click("icon_Action_Searchresult_BulkStateMove");
		// verify elements in popup window
		verifyActionBulkUpdate_Popup();
		// verify dropdown items
		getAllListItemAndVerify(ALLOCATED,"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(FOR_REFERRAL,	"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		getAllListItemAndVerify(PENDED,	"dropDown_Action_BulkStateMove_Popup_AllowedStates");
		// select state from dropdown
		selectByVisibleText("dropDown_Action_BulkStateMove_Popup_AllowedStates", FOR_REFERRAL);
		// click Move all button
		click("button_Action_BulkSateMovePopup_MoveAll");
		// verify all elements in bulk update page
		//verifyAllElementsInMirsapActionBulkUpdate();
		// verify element For referral text area
		explicitlyWaitForElement("textField_Action_MirsapAction_Edit_ForReferral",
				getTotalWaitTimeInSecs());
		verifyElementPresent("textField_Action_MirsapAction_Edit_ForReferral");
		// verifying incidents,URN,State and Fast Action
	
		// verify state For Referral
		verifyText(FOR_REFERRAL, "label_Action_BulkUpdate_State");
		// insert tinymce fields
		insertIntoAllTinyMCEinPage("For Referral Text");
		// clicking Bulk update save button
		click("button_Action_BulkUpdate_Save");
		// clicking bulk update save and ok button
		explicitlyWaitForElement("button_Action_BulkUpdate_UpdatePopup_OK",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_BulkUpdate_UpdatePopup_OK");
		// verify both URN in success alert box
		verifyTextContains(searchResult_ActionFirstURN,
				"successMsg_Action_BulkUpdate_Allocated");
		verifyTextContains(searchResult_ActionSecondURN,
				"successMsg_Action_BulkUpdate_Allocated");
		// verify updated message in success alert
		verifyTextContains("updated", "successMsg_Action_BulkUpdate_Allocated");
		// click OK button
		click("button_Action_BulkUpdate_UpdatePopup_OK");

	}
	
	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
