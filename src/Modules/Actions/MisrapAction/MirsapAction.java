package Modules.Actions.MisrapAction;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import Modules.Actions.Actions;

public class MirsapAction extends Actions{
	
	public static final String RAISE_MIRSAP_ACTION_URL = "/group/holmes/raise-action";
	public static final String CONFIGURE_ACTION_TEAM_URL = "/group/holmes/action-team";
	protected static final String REGISTER_DOCUMENT_URL = "/group/holmes/register-document";
	protected static final String CREATE_ASSOCIATION_URL = "/group/holmes/create-multiple-associations";
	public static final String ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE = "Action - Auto Test for Raise Action update";
	protected static final String TINY_MCE_TEXT_FOR_RAISE_MIRSAP_ACTION = "test";
	public static final String ACTION_ORIGINATING_DETAIL = "Originating Desc";
	protected static final String VIEWACTION_URL = "/group/holmes/view-action";
	protected static final String MIRSAP_ACTION_TITLE_WINDOW = "Action";
	protected static final String ACTION_DASHBOARD_TITLE_WINDOW = "Action Dashboard";
	protected static final String ACTION_TITLE_MAX_CONTENT = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ012345678901234567";
	protected static final String PRIORITY_LOW = "Low";
	protected static final String PRIORITY_HIGH = "High";
	protected static final String PRIORITY_MEDIUM = "Medium";
	public static final String VIEW_ACTION="VIEW ACTION";
	public static final String CREATE_ACTION="CREATE ACTION";
	public static final String SEARCH_FAST_ACTION="SEARCH ACTION";
	public static final String UPDATE_ACTION="UPDATE ACTION";
	public static final String CREATE_ASSOCIATION="CREATE ASSOCIATION";
	public static final String UPDTAE_ASSOCIATION="UPDATE ASSOCIATION";
	public static final String DELETE_ASSOCIATION="DELETE ASSOCIATION";
	public static final String ADD_TAG_TO_RECORD="ADD TAG TO RECORD";
	public static final String ADD_ATTACHEMENT="ADD ATTACHMENT";
	public static final String REMOVE_ATTACHEMENT="REMOVE ATTACHMENT";
	public static final String ATTACH_ENQUIRY_OFFICER = "ATTACH ENQUIRY OFFICER";
	public static final String CREATE_ENQUIRY_OFFICER = "CREATE ENQUIRY OFFICER";
	public static final String DEACTIVATE_ENQUIRY_OFFICER = "DEACTIVATE ENQUIRY OFFICER";
	public static final String ACTIVATE_ENQUIRY_OFFICER = "ACTIVATE ENQUIRY OFFICER";
	public static final String DELETE_ACTION_TEMPLATE = "DELETE ACTION TEMPLATE";
	public static final String CREATE_ACTION_TEMPLATE = "CREATE ACTION TEMPLATE";
	public static final String UPDATE_ACTION_TEMPLATE = "UPDATE ACTION TEMPLATE";
	protected static final String TEMPLATE_NAME = "Automation Template";
	protected static final String TEMPLATE_TEXT = "Automated Testing";
	
	String PROTECTIVE_MARKING="NOT PROTECTIVELY MARKED";
	protected static final String MIRSAP_FORCE="ARMY";
	protected static final String MIRSAP_CLASS="SCENE";
	protected static final String MIRSAP_PRIORITY="Medium";
	protected static final String MIRSAP_STATION="HEADQUARTERS";
	public  String MirsapActionURN_ReUse;
	public  String getMirsapActionURN;
	protected static String bulkAction;
	
	
	//Data used to Verify Current/Future Action State
	protected static final String ALLOCATION= "For Allocation";
	protected static final String ALLOCATED = "Allocated";
	protected static final String SUBMITTED = "Submitted";
	protected static final String RECEIVED = "Received";
	protected static final String RESULTED = "Resulted";
	protected static final String FILED = "Filed";
	protected static final String PENDED= "Pended";
	protected static final String FOR_REFERRAL= "For Referral";
	protected static final String REFERRED="Referred";
	protected static final String PARTIALLY_RESULTED="Partially Resulted";

	protected static final String SEARCH_TYPE = "Action";
    protected static final String ACTION_DASHBOARD = "/group/holmes/search-for-actions";
    protected static String searchResult_ActionFirstURN;
	protected static String searchResult_ActionSecondURN;
	
	//Data used for creating association
	protected static final String ASSOCIATION_TYPE_LINK = "Link";
	protected static final String ASSOCIATION_TYPE_DESCRIBED = "Described";
	protected static final String ASSOCIATION_TYPE_SUBJECT = "Subject";
	protected static final String ASSOCIATION_TYPE_MANUALPOTENTIALMATCH = "Manual Potential Match";
	
	//Data for Transfer association 
	protected static final String TRANSFER_POPUP_ERROR = "Please select a record";
	
	
	//Data for Profile tab
	protected static final String PROFILE_DELETE_POPUP = "Would you like to remove selected Note(s)?";
	

	//Data for Activity Description
	public static final String ALLOCATED_ACTION="ALLOCATED ACTION";
	public static final String SUBMITTED_ACTION="SUBMITTED ACTION";
	public static final String RESULTED_ACTION="RESULTED ACTION";
	public static final String RECEIVED_ACTION="RECEIVED ACTION";
	public static final String FILED_ACTION="FILED ACTION";
	public static final String FOR_REFERRAL_ACTION="FOR REFERRAL ACTION";
	public static final String REFERRED_ACTION="REFERRED ACTION";
	public static final String PENDED_ACTION="PENDED ACTION";
	public static final String PARTIALLY_RESULTED_ACTION="PARTIALLY RESULTED ACTION";
	
	
	
	
	// default constructor
	public MirsapAction() {
		softAssert = new SoftAssert();
	}
	
	public MirsapAction(WebDriver driver) {
		this.driver = driver;
		softAssert = new SoftAssert();
	}
	
	public void verifyMirsapActionTabEnabled(){
		verifyTabActive("tab_action");
	}
	
	public void verifyProfileTabEnabled(){
		verifyTabActive("tab_Profile");
	}
	
	public void verifySearchResultTabEnabled(){
		verifyTabActive("tab_Action_Search_Result");
	}
	
	
	public void verifyMandatoryFieldsInMirsapAction() throws Exception {
		verifyElementIsMandatory("label_Action_Title");
		verifyElementIsMandatory("label_Action_Mirsap_Force");
		verifyElementIsMandatory("label_Action_Mirsap_Class");
		verifyElementIsMandatory("label_Action_Mirsap_Priority");
		verifyElementIsMandatory("label_Action_Mirsap_Station");
		verifyElementIsMandatory("label_Action_Mirsap_Subjects");
	}
	
	public void verifyFieldsInCreateActionTemplate() throws Exception {
		
		verifyElementPresent("label_Action_Template_ProtectiveMarking");
		verifyElementPresent("label_Action_Template_Text");
		verifyElementPresent("label_Action_Template_TitleField");
		verifyElementPresent("label_Action_Template_Force");
		verifyElementPresent("label_Action_Template_Class");
		verifyElementPresent("label_Action_Template_Priority");
		verifyElementPresent("label_Action_Template_Station");
		verifyElementPresent("label_Action_Template_Subclass1");
		verifyElementPresent("label_Action_Template_Subclass2");
		verifyElementPresent("label_Action_Template_AllocatedOfficer1");
		verifyElementPresent("label_Action_Template_AllocatedOfficer2");
		verifyElementPresent("label_Action_Template_Themes");
		verifyElementPresent("label_Action_Template_OriginatingDetails");
	}
	
	public void verifyRaiseMirsapActionPrePopulatedValuesFromTemplate()throws Exception{
		verifyText(TEMPLATE_TEXT, "textField_Action_Title");
		verifyText(TEMPLATE_TEXT,"textField_Action_OriginatingDetails");
		verifyTextCompare(TEMPLATE_TEXT,getTextFieldText());
		verifyCurrentSelectByText("dropDown_Action_Template_Protective_Marking",PROTECTIVE_MARKING);
		verifyCurrentSelectByText("dropDown_Action_Template_Priority",MIRSAP_PRIORITY);
		verifyCurrentSelectByText("dropDown_Action_Template_Force",MIRSAP_FORCE);
		verifyCurrentSelectByText("dropDown_Action_Template_Station",MIRSAP_STATION);
		verifyCurrentSelectByText("dropDown_Action_Template_Class",MIRSAP_CLASS);
		
	}
	
	public void isInMirsapActionViewPage() throws Exception{
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		verifyElementPresent("label_Action_View_Page_Title");
	}
	
	public void isInMirsapActionEditPage() throws Exception{
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Save");
	}
	
	public void isInMirsapActionSimilarPage() throws Exception{
		explicitlyWaitForElement("header_Create_Fast_Action", getTotalWaitTimeInSecs());
		verifyElementPresent("header_Create_Fast_Action");
		explicitlyWaitForElement("button_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Create");
	}

	protected void verifyMirsapActionFieldsInCreatePage() throws Exception {
		// Verify Title has text field
		assertElementPresent("textField_Action_Title");
		
		//Verify currently incident id is selected
		verifyCurrentSelectByText("dropDown_Action_Create_Incident",incidentIdFull);
		// Validate Protective Marking : Restricted is set as default
		verifyCurrentSelectByText("dropDown_Action_Create_ProtectiveMarking",PROTECTIVE_MARKING_RESTRICTED);
		// Validate  Priority : Low is set as default value
		verifyCurrentSelectByText("dropDown_Action_Mirsap_Priority",PRIORITY_LOW);
		
		verifyCreateBulkRecordsCheckBoxPresent();
		verifyAllocatedOfficer();
		verifyThemes();
		verifyState();
		verifyLinkedActions();
		verifyAssociatedDocuments();
		verifyOriginatingDetails();
		verifyReceiversInstructions();
		verifyResultText();
		verifyDocumentTaken();
		verifyMigrationLog();
		verifyCustomFields();		
	}
	
	private void verifyCreateBulkRecordsCheckBoxPresent() throws Exception {
		verifyElementPresent("checkBox_Action_BulkRecord");
	}
	
	private void verifyState() throws Exception {
		verifyElementPresent("label_Action_FastAction_Create_State");
	}

	private void verifyOriginatingDetails() throws Exception {
		verifyElementPresent("textField_Action_OriginatingDetails");
	}

	private void verifyCustomFields() throws Exception {
		verifyElementPresent("section_Action_Create_CustomFields");
	}

	private void verifyMigrationLog() throws Exception {
//		expandMigrationLog();
		verifyElementPresent("textField_Action_FastAction_Create_MigrationLog");
	}

	private void verifyDocumentTaken() throws Exception {
		verifyElementPresent("icon_Action_Create_DocumentTaken_View");
	}
	
	private void verifyLinkedActions() throws Exception {
		verifyElementPresent("table_Action_Create_LinkedAction");
	}

	
	public void searchMirsapActionAndSelectByState(String stateObj) throws Exception {

		// navigate to Action Dash board
		navigateTo(ACTION_DASHBOARD);
		explicitlyWaitForElement("dropDown_Action_ActionDashboard_Search_Type",
				getTotalWaitTimeInSecs());
		// Search Mirsap Action in Drop down
		selectByVisibleText("dropDown_Action_ActionDashboard_Search_Type",
				SEARCH_TYPE);
		// Enter Title field for search
		// Select For Allocation Satate
		click("button_Action_ActionDashboard_Search_State");
		click(stateObj);
		// click Search button
		click("button_Action_Search");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",
				getTotalWaitTimeInSecs());
	}
	
	public void selectTwoRecordsFromSearchResultAndVerifyState(String actionState) throws Exception {
		//select two records
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		click("checkBox_Action_SearchResulttab_Firstrow");
		click("checkBox_Action_SearchResulttab_Secondrow");
		// getting bulkstate move URNs
		searchResult_ActionFirstURN = getText("table_Action_SearchResults_URN");
		searchResult_ActionSecondURN = getText("table_Action_SearchResults_SecondRow_URN");
		// verify For allocation State
		verifyText(actionState, "table_Action_SearchResults_FirstRow_State");
		verifyText(actionState, "table_Action_SearchResults_SecondRow_State");
	}
	protected void verifyAllElementsInMirsapActionBulkUpdate() throws Exception {

		verifyActionBulkUpdateHeader();
		verifyActionBulkUpdateTab();
		verifyActionBulkUpdateState();
		//verifyActionBulkUpdateTheme();
		verifyActionBulkUpdateTags();
		verifyActionBulkUpdateCancel();
		verifyActionBulkUpdateSave();
		//verifyReceiversInstructions();
	}
	
	protected void verifyAllElementsInActionBulkUpdate_Individual()
			throws Exception {
		
		//verifyActionBulkUpdateTheme();
		//verifyReceiversInstructions();
		verifyActionBulkUpdate_IndividualCancel();
		verifyActionBulkUpdate_IndividualSkip();
		verifyActionBulkUpdate_IndividualApply();
		verifyActionBulkUpdate_IndividualMove();
		verifyActionBulkUpdate_IndividualState();
	}
	
	protected void verifyActionBulkUpdate_Popup() throws Exception {
		// verify bulk update popup
		verifyElementPresent("header_Action_BulkSatetMove_Popup");
		  // verify Move individual button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveIndividual");
		// verify cancel button
		verifyElementPresent("button_Action_BulkSateMovePopup_Cancel");
		// verify Move all button
		verifyElementPresent("button_Action_BulkSateMovePopup_MoveAll");
	}

	private void verifyActionBulkUpdateHeader() throws Exception {
		// verify header Moving BulkState move :allocated
		verifyElementPresent("header_Action_BulkSatetMove_Allocated");
	}   
	
	private void verifyActionBulkUpdateTab() throws Exception {
		// verify BulkUpdate tab
		verifyElementPresent("tab_Action_BulkUpdate");
	} 
	
	private void verifyActionBulkUpdateState() throws Exception {
		// verify label Moving and allocated URN
		explicitlyWaitForElement("label_Action_BulkStateMove_Allocated",
				getTotalWaitTimeInSecs());
		verifyElementPresent("label_Action_BulkStateMove_Allocated");
	} 
	private void verifyActionBulkUpdateTheme() throws Exception {
		// verify theme
		verifyElementPresent("checkBox_Action_Themes_Item1");
	} 
	private void verifyActionBulkUpdateTags() throws Exception {
		// Verify Tag link
		verifyElementPresent("link_Action_BulkUpdate_MoveAll_Tags");
	} 
	
	private void verifyActionBulkUpdateCancel() throws Exception {
		verifyElementPresent("button_Action_BulkUpdate_Cancel");
	} 		
	private void verifyActionBulkUpdateSave() throws Exception {
		verifyElementPresent("button_Action_BulkUpdate_Save");
	} 		
	
	private void verifyActionBulkUpdate_IndividualCancel() throws Exception {
		// verify cancel button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Cancel");
	}

	private void verifyActionBulkUpdate_IndividualSkip() throws Exception {
		// verify Skip button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Skip");
	}

	private void verifyActionBulkUpdate_IndividualApply() throws Exception {
		// verify apply button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual_Apply");
	}

	private void verifyActionBulkUpdate_IndividualMove() throws Exception {
		// verify MoveIndividual button
		verifyElementPresent("button_Action_BulkUpdate_MoveIndividual");
	}

	private void verifyActionBulkUpdate_IndividualState() throws Exception {
		// verify label MoveIndividual
		verifyElementPresent("label_Action_BulkStateMove_MoveIndividual_Allocated");
	}
	
	public void viewAndGetMirsapActionURN(String mirsapURN) throws Exception{
		navigateTo(VIEWACTION_URL);
		sendKeys("textField_Action_ViewUrn", mirsapURN);
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
	}
	
	public void verifyMarkerTabEnabled(){
		verifyTabActive("tab_Marker");
	}
	
	public void verifyAddAssociation() throws Exception {
		ArrayList<String> listItems = new ArrayList<String>();
		listItems.add("Link");
		listItems.add("Described");
		listItems.add("Manual Potential Match");
		listItems.add("Subject");
		//verify the right dialog is open
		verifyText("Add Association", "label_Action_Add_Association");
		verifyAllDowndownValues(listItems,"dropDown_Association_Add_Type");
	}
	
	protected void verifyMirsapActionFieldsInAllocatedPage(String expectedText) throws Exception{
		verifyUrn();
		verifyTitleValue();
		verifyTextField(expectedText);
		verifyStateValue(ALLOCATED);
		verifyAssociatedDocuments();
		verifyAllocatedOfficer();
		verifyReceiversInstructions();
	}
	
	protected void verifyMirsapActionFieldsInSubmittedPage(String expectedText) throws Exception{
		verifyUrn();
		verifyTitleValue();
		verifyTextField(expectedText);
		verifyStateValue(SUBMITTED);
		verifySubmissionText();
		verifyReceiversInstructions();
	}
	
	protected void verifyMirsapActionFieldsInReceivedPage(String expectedText) throws Exception{
		verifyUrn();
		verifyTitleValue();
		verifyTextField(expectedText);
		verifyStateValue(RECEIVED);
		verifyResultText();
		verifyReceiversInstructions();
	}
	
	protected void verifyMirsapActionFieldsInResultedPage(String expectedText) throws Exception{
		verifyUrn();
		verifyTitleValue();
		verifyTextField(expectedText);
		verifyStateValue(RESULTED);
		verifyResultTextInEditPage();
		verifyRecordedOfficer();
		verifyReceiversInstructions();
		verifyDocumentTakenInEditPage();
	}
	
	protected void verifyMirsapActionFieldsInFiledPage(String expectedText) throws Exception{
		verifyUrn();
		verifyTitleValue();
		verifyTextField(expectedText);
		verifyStateValue(FILED);
		verifyFilingReason();
	}
	
	protected void verifyFastActionFieldsInPartially_ResultedPage(String expectedText) throws Exception{
		verifyUrn();
		//verifyTitleValue();
		verifyTextField(expectedText);
		verifyStateValue(PARTIALLY_RESULTED);
		verifyResultTextInEditPage();
		verifyRecordedOfficer();
		verifyReceiversInstructions();
		verifyResultText();
		verifyDocumentTakenInEditPage();
	}
	protected void verifyFastActionFieldsInFor_ReferralPage(String expectedText) throws Exception{
		verifyUrn();
		verifyTitleValue();
		verifyTextField(expectedText);
		verifyStateValue(FOR_REFERRAL);
		verifyForReferralReason();
	}
	
	protected void verifyFastActionFieldsInReferredPage(String expectedText) throws Exception{
		verifyUrn();
		verifyTitleValue();
		verifyTextField(expectedText);
		verifyStateValue(REFERRED);
		verifyReferredReason();
		
	}
	
	protected void verifyFastActionFieldsInPendedPage(String expectedText) throws Exception{
		verifyUrn();
		verifyTitleValue();
		verifyTextField(expectedText);
		verifyStateValue(PENDED);
		verifyPendDate();
		verifyPendReason();
		
	}


	private void verifyUrn() throws Exception{
		verifyText(MirsapActionURN_ReUse, "label_Action_Urn_QueueMove");
	}

	private void verifyTitleValue() throws Exception{
		verifyText( ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE ,
				"label_Action_View_Title");
	}
	private void verifyTextField(String textToCompare) throws Exception{
		verifyTextCompare(textToCompare, getTextFieldText()); 
	}

	protected void verifySubmissionText() throws Exception {
		verifyElementPresent("textfield_Action_StateMove_SubmissionText");
	}
	
	private void verifyResultText() throws Exception {
		
		verifyElementPresent("textField_Action_MirsapAction_Create_ResultText");
	}
	private void verifyResultTextInEditPage() throws Exception {
		verifyElementPresent("textField_Action_MirsapAction_Create_ResultText");
		verifyElementPresent("textField_Action_MirsapAction_Edit_ResultText");
	}


	private void verifyStateValue(String expectedState) throws Exception{
		verifyText(expectedState ,"label_Action_Create_State");
	}

	private void verifyThemes() throws Exception {
		verifyElementPresent("checkBox_Action_Themes");
	}

	protected void verifyAssociatedDocuments() throws Exception {
		verifyElementPresent("table_Action_Create_AssociatedDocument");
	}

	protected void verifyRecordedOfficer() throws Exception {
		verifyElementPresent("button_Action_MirsapAction_RecordedOfficerfirstOne");
		verifyElementPresent("button_Action_MirsapAction_RecordedOfficerTwo");
		verifyElementPresent("textField_Action_MirsapAction_Edit_RecordedOfficerOne");
		verifyElementPresent("textField_Action_MirsapAction_Edit_RecordedOfficerTwo");
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textField_Action_MirsapAction_Edit_RecordedOfficerOne");
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textField_Action_MirsapAction_Edit_RecordedOfficerTwo");
	}
	
	protected void verifyAllocatedOfficer() throws Exception {
		verifyElementPresent("button_Action_MirsapAction_CreateOne");
		verifyElementPresent("button_Action_MirsapAction_CreateTwo");
		verifyElementPresent("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");
		verifyElementPresent("textfield_Action_MirsapAction_QueueAllocatedOfficerTwo");
	}

	private void verifyDocumentTakenInEditPage() throws Exception {
		verifyElementPresent("icon_Action_Edit_DocumentTaken_Add");
	}
	
	private void verifyFilingReason() throws Exception{
		verifyElementPresent("textfield_Action_StateMove_Filing_Reason");
	}
	
	protected void verifyReceiversInstructions() throws Exception {
		verifyElementPresent("textField_Action_MirsapAction_Edit_ReceiversInstructions");
	}
	
	private void verifyForReferralReason() throws Exception{
		verifyElementPresent("textField_Action_MirsapAction_Edit_ForReferral");
	}
	
	private void verifyReferredReason() throws Exception{
		verifyElementPresent("textField_Action_MirsapAction_Edit_Referred");
	}
	
	private void verifyPendDate() throws Exception{
		verifyElementPresent("textField_Action_MirsapAction_PendDate");
	}
	
	private void verifyPendReason() throws Exception{
		verifyElementPresent("textField_Action_MirsapAction_Edit_PendedReason");
	}
	
	public  String raiseAndGetMirsapActionURN() throws Exception {

		//navigate to Raise Mirsap Action URL
		navigateToPage(RAISE_MIRSAP_ACTION_URL);
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		verifyTextContains(incidentIdFull, "dropDown_Action_Create_Incident");
		//verifyMandatoryFieldsInMirsapAction();
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		//insert text Text field
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		click("link_Action_Create_LinkedAssociation");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		//Verification of data in View Action Fields
		verifyElementPresent("successMsg_Action_Create");
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		MirsapActionURN_ReUse = actUrn[1];
		return getMirsapActionURN=MirsapActionURN_ReUse;
	}
	
	public void changeStateAllocated(String tinyMceText) throws Exception {
		waitForJQuery(driver);
		//wait for allocation officer
		explicitlyWaitForElement("button_Action_ChangeState_Save",
				getTotalWaitTimeInSecs());
		explicitlyWaitForElement("textfield_Action_MirsapAction_QueueAllocatedOfficerOne",
				getTotalWaitTimeInSecs());
		
		verifyMirsapActionFieldsInAllocatedPage(tinyMceText);
		insertIntoAllTinyMCEinPage(tinyMceText);
		//Add First Create Officer
		click("button_Action_MirsapAction_CreateOfficerOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_Forename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");
		/*click("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");
		sendKeys("textfield_Action_MirsapAction_QueueAllocatedOfficerOne",
				CREATE_OFFICER_SURNAME);
		explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",
				getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_StateMove_OfficerName");*/
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_MirsapAction_QueueAllocatedOfficerOne");

		//Add 2nd Create officer
		click("button_Action_MirsapAction_CreateOfficerTwo");
		explicitlyWaitForElement(
				"textfield_Action_MirsapAction_CreateOfficer_SurnameTwo",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_MirsapAction_CreateOfficer_SurnameTwo",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_MirsapAction_CreateOfficer_ForenameTwo",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateTwo");
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_MirsapAction_QueueAllocatedOfficerTwo");

		// Perform Save on Allocated Page
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(ALLOCATED, "label_Action_View_State");

	}

	public void changeStateSubmitted(String expectedText,String tinyMceText)throws Exception {
		
		waitForJQuery(driver);
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyMirsapActionFieldsInSubmittedPage(expectedText);
		insertIntoAllTinyMCEinPage(tinyMceText);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(SUBMITTED ,"label_Action_View_State");
	}

	public void changeStateReceived(String expectedText, String textInstructions) throws Exception{

		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyMirsapActionFieldsInReceivedPage(expectedText);
		sendKeys("textField_Action_MirsapAction_Edit_ReceiversInstructions",textInstructions);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(RECEIVED ,"label_Action_View_State");
	}
	
	public void changeStateResulted(String expectedText, String tinyMceText) throws Exception{
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyMirsapActionFieldsInResultedPage(expectedText);
		//send keys to result text
		explicitlyWaitForElement("button_Action_ChangeState_Save",
				getTotalWaitTimeInSecs());
		insertIntoAllTinyMCEinPage(tinyMceText);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(RESULTED, "label_Action_View_State");
		getAllListItemAndVerify("Filed","dropDown_Action_Select_State");
	}

	public void changeStateFiled(String expectedText, String tinyMceText) throws Exception{
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyMirsapActionFieldsInFiledPage(expectedText);
		//send keys to result text
		insertIntoAllTinyMCEinPage(tinyMceText);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(FILED, "label_Action_View_State");
		getAllListItemAndVerify("For Allocation","dropDown_Action_Select_State");
	}
	
	public void signOut()throws Exception{
		explicitlyWaitForElement("icon_Home_MyAccount",getTotalWaitTimeInSecs());
		click("icon_Home_MyAccount");
		explicitlyWaitForElement("button_Home_SignOut",getTotalWaitTimeInSecs());
		click("button_Home_SignOut");

	}
	public void changeStateFiledForChangeStateLargeFields(String tinyMceText) throws Exception{
		explicitlyWaitForElement("button_Action_ChangeState_Save",getTotalWaitTimeInSecs());
		verifyMirsapActionFieldsInFiledPage(tinyMceText);
		//send keys to result text
		insertIntoAllTinyMCEinPage(tinyMceText);
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(FILED, "label_Action_View_State");
		getAllListItemAndVerify("For Allocation","dropDown_Action_Select_State");
	}
	
	public  String raiseBulkAndGetMirsapActionURN(String bulkCount) throws Exception {

		//navigate to Raise Mirsap Action URL
		navigateToPage(RAISE_MIRSAP_ACTION_URL);
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		verifyTextContains(incidentIdFull, "dropDown_Action_Create_Incident");
		//verifyMandatoryFieldsInMirsapAction();
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		//insert text Text field
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		click("link_Action_Create_LinkedAssociation");
		// Bulk create Entering bulk count
		click("checkBox_Action_BulkRecord");
		sendKeys("textField_Action_BulkRecordCount", bulkCount);
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		//Verification of data in View Action Fields
		verifyElementPresent("successMsg_Action_Create");
		String succMess = getText("successMsg_Action_Create");
		String succMess1 = getText("successMsg_Action_Create");
		System.out.println(succMess1 + "  succMess1");
		String[] actUrn1 = succMess1.split(" ");
	    bulkAction = actUrn1[4];
		return bulkAction;
	}
	
	
	
}
