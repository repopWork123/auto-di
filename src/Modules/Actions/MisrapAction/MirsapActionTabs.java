package Modules.Actions.MisrapAction;

import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Modules.Actions.FastAction.FastAction;
import Modules.AuditLogs.AuditLogSearch;
import Modules.Documents.Document;
import Modules.Indexes.Indexes;
import Modules.Indexes.Nominal;
import Modules.Search.ListManagement.ListManagement;
import ReusableUtilities.Dataprovider;
import ReusableUtilities.Report;

public class MirsapActionTabs extends MirsapAction {
	
	//Data for Marker tab
	protected static final String MARKER_EXPIRYDATEADD = "18/02/2017 21:26";
	protected static final String MARKER_EXPIRYDATEEDITED = "19/02/2017 21:26";
	protected static final String MARKER_DELETE_POPUP = "Would you like to remove selected Marker(s)?";
	protected static final String MARKER_EDIT_POPUP_ERROR = "Please select one row to edit";
	protected static final String MARKER_DELETE_POPUP_ERROR = "Please select one or more rows to delete";
	protected static final String PROFILE_EDIT_POPUP_ERROR = "Please select one row to edit";
	protected static final String PROFILE_DELETE_POPUP_ERROR = "Please select one or more rows to delete";
	private static final String ERROR_MESSAGE_FILE_INTERNAL_OR_EXTERNAL_LINK = "Please provide attachment information for either of File, Internal or External Link";
	private static final String CONFIRMATION_MESSAGE_TO_DELETE_AN_ATTACHMENT = "Are you sure you want to delete this attachment?";
	private static final String PROFILE_EDIT_NOTE = "profiletest";
	private static final String DOCUMENT_DATE = "01/11/2016 14:48";
	protected static String targetLinkURN;
	protected static String targetDescribedURN;
	protected static String targetSubjectURN;
	protected static String targetManualpotentialmatchURN;
	protected static String actionURN;
	protected static String documentURN;
	protected static String disclosureURN;
	protected static String entityURN;
	protected static String transferAssociationURN;
	protected static String listnomURN;
	protected static String listdocURN;
	protected static String listdisURN;
	protected static String listactURN;
	public String FOR_ALLOCATION = "For Allocation";	
	public String mirsapURN;
	private String attachement = System.getProperty("user.dir")+"\\img\\log_file.xml";
	private String attachement2 = System.getProperty("user.dir")+"\\img\\log_file2.xml";
	
	
	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}

	//default constructor
	public MirsapActionTabs() {
		softAssert = new SoftAssert();
	}
    
		
	@Test
	public void testData() throws Exception{
		
		FastAction fastAction = new FastAction(driver);
		actionURN=fastAction.raiseAndGetFastActionURN();
		expandDisclosureSection();
		verifyElementPresent("table_Action_View_Diclosure_URN");
		disclosureURN = getText("table_Action_View_Diclosure_URN");
		
		//rais bulk action urns and split 
		String actionBulk=raiseBulkAndGetMirsapActionURN("5");
		String[] actURN_Split = actionBulk.split("-");
		targetLinkURN = actURN_Split[0];
		String mirsapURNcount = targetLinkURN.substring(1);
		
		int mirsapURNcounts = Integer.parseInt(mirsapURNcount)+1;
		targetDescribedURN="A"+String.valueOf(mirsapURNcounts);
		
		int mirsapURNcounts2 = Integer.parseInt(mirsapURNcount)+2;
		targetManualpotentialmatchURN="A"+String.valueOf(mirsapURNcounts2);
		
		int mirsapURNcounts3 = Integer.parseInt(mirsapURNcount)+3;
		transferAssociationURN="A"+String.valueOf(mirsapURNcounts3);
		

		Indexes  nominalIndex = new Indexes(driver);
		//create bulk nominal
		String  bulkNominal=nominalIndex.createBulkNominalAndGetURNs("Nominal","MALE","3");
		String[] bulkNominal_Split = bulkNominal.split("-");
		entityURN = bulkNominal_Split[0];
		System.out.println(entityURN+"  entityURN");
		String nominalURNcount = entityURN.substring(1);
		int nominalURNcounts = Integer.parseInt(nominalURNcount)+1;
		targetSubjectURN="N"+String.valueOf(nominalURNcounts);
		System.out.println(targetSubjectURN+"  targetSubjectURN");
		int nominalURNcounts1 = Integer.parseInt(nominalURNcount)+2;
		listnomURN="N"+String.valueOf(nominalURNcounts1);
		
		//create other documents
		Document doc = new Document(driver);
		String bulkDocument = doc.registerBulkOtherDocumentAndGetURNsWithSubjectReference("2");
		String[] docURN_Split = bulkDocument.split("-");
		documentURN = docURN_Split[0];
		
		//add list managent
		ListManagement li = new ListManagement(driver);
		ArrayList<String> as = new ArrayList<String>();
		as.add(documentURN);
		as.add(targetDescribedURN);
		as.add(disclosureURN);
		as.add(listnomURN);
		li.createDefaultList(as);
		
		listdocURN=documentURN;
		listdisURN=disclosureURN;
		listactURN=targetDescribedURN;
		
        System.out.println("targetLinkURN: " + targetLinkURN);
		System.out.println("targetDescribedURN: " + targetDescribedURN);
		System.out.println("targetSubjectURN: " + targetSubjectURN);
		System.out.println("targetManualpotentialmatchURN: " + targetManualpotentialmatchURN);
		System.out.println("transferAssociationURN: " + transferAssociationURN);
		System.out.println("listnomURN: " + listnomURN);
		System.out.println("listdocURN: " + listdocURN);
		System.out.println("listdisURN: " + listdisURN);
		System.out.println("listactURN: " + listactURN);

	}

	@BeforeTest
	public void beforeTest() throws Exception {
		loginInIEAndSelectIncident();
		//loginAndSelectIncident();
		//raise mirsap action to use in all test cases
		//mirsapURN = raiseAndGetMirsapActionURN();
	}
	
	/*
	 * Test Case 27965: 001. Mirsap Association Tab - Add and Verify Validation Messages
	 */
	@Test(priority = 2)
	public void associatiobTab(Method method) throws Exception {
		startLine(method);
		//String linkAssociation = "N9";
		//String DescribedAssociation = "V1";
		//String subjectAssociation = "N6";
		//String manualAssociation = "N8";
		String startDate = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
		String endDate = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
		SimpleDateFormat temp = new SimpleDateFormat("dd/MM/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(temp.parse(endDate));
		c.add(Calendar.DATE, 1);  // number of days to add
		endDate = temp.format(c.getTime());
		
		//change this to be a action already created instead of making a new one
		String mirsapURN = raiseAndGetMirsapActionURN();
		viewAndGetMirsapActionURN(mirsapURN);
		click("button_Action_Association_Tab");
		
		//click add button
		click("icon_Association_Add");
		verifyAddAssociation();
		
		//select link type association
		selectByVisibleText("dropDown_Association_Add_Type","Link");
		//click add association button
		clickAndWait("button_Association_Add_Create");
		//verify error message
		verifyText("Please select a valid Target Incident and URN or an entry from the Default List.","errorMsg_Action_Association_Add_Subject");
		//send keys in urn
		sendKeys("textField_Association_Add_TargetURN",targetLinkURN);
		//click add association button
		click("button_Association_Add_Create");
		//verify error message
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",getTotalWaitTimeInSecs());
		verifyText("Usage is mandatory","errorMsg_Action_Association_Add_Subject");
		sendKeys("textField_Association_Add_TargetURN","");
		
		//select described type association
		selectByVisibleText("dropDown_Association_Add_Type","Described");
		//click add association button
		click("button_Association_Add_Create");
		//verify error message
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",getTotalWaitTimeInSecs());
		verifyText("Please select a valid Target Incident and URN or an entry from the Default List.","errorMsg_Action_Association_Add_Subject");
		
		//select subject type association
		selectByVisibleText("dropDown_Association_Add_Type","Subject");
		//click add association button
		clickAndWait("button_Association_Add_Create");
		//verify error message
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",getTotalWaitTimeInSecs());
		verifyText("Please select a valid Target Incident and URN or an entry from the Default List.","errorMsg_Action_Association_Add_Subject");
		
		//select manual potential type association
		selectByVisibleText("dropDown_Association_Add_Type","Manual Potential Match");
		//click add association button
		click("button_Association_Add_Create");
		//verify error message
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",getTotalWaitTimeInSecs());
		verifyText("Please select a valid Target Incident and URN or an entry from the Default List.","errorMsg_Action_Association_Add_Subject");
				
		//select link type association and click multiple
		selectByVisibleText("dropDown_Association_Add_Type","Link");
		click("button_Action_Association_AddMultipleAssociation");
		clickAndWait("button_Association_Add_Create");
		//verify error message
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",getTotalWaitTimeInSecs());
		verifyText("At least one Target entity is required to create an association.","errorMsg_Action_Association_Add_Subject");
		
		//described link type association multiple
		selectByVisibleText("dropDown_Association_Add_Type","Described");
		clickAndWait("button_Association_Add_Create");
		//verify error message
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",getTotalWaitTimeInSecs());
		verifyText("At least one Target entity is required to create an association.","errorMsg_Action_Association_Add_Subject");
		
		//subject link type association multiple
		selectByVisibleText("dropDown_Association_Add_Type","Subject");
		clickAndWait("button_Association_Add_Create");
		//verify error message
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",getTotalWaitTimeInSecs());
		verifyText("At least one Target entity is required to create an association.","errorMsg_Action_Association_Add_Subject");
		
		//manual potential match link type association multiple
		selectByVisibleText("dropDown_Association_Add_Type","Manual Potential Match");
		clickAndWait("button_Association_Add_Create");
		//verify error message
		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",getTotalWaitTimeInSecs());
		verifyText("At least one Target entity is required to create an association.","errorMsg_Action_Association_Add_Subject");
		
		click("button_Action_Association_Add_Cancel");
		click("button_Action_Association_Add_Cancel");
		
		//add subject type
		click("icon_Association_Add");
								
		verifyAddAssociation();
		//select subject type association
		selectByVisibleText("dropDown_Association_Add_Type","Subject");
		//enter nominal urn
		sendKeys("textField_Association_Add_TargetURN",targetSubjectURN);
		//click add association button
		clickAndWait("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",getTotalWaitTimeInSecs());
		verifyText("Created SUBJECT association from "+mirsapURN+" Action - Auto Test for Raise Action update to "+targetSubjectURN,"successMsg_Action_Add_Association");
		click("icon_Action_Association_Reload");
		
		//add link type
		click("icon_Association_Add");
		
		verifyAddAssociation();
		//select link type association
		explicitlyWaitForElement("dropDown_Association_Add_Type",getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Association_Add_Type","Link");
		//enter nominal urn
		sendKeys("textField_Association_Add_TargetURN",targetLinkURN);
		//select usage
		waitForLinkTypeUsageDropdownLoad();
		click("dropDown_Action_Association_Add_Usage");
		click("dropDown_Action_Association_Add_Usage_input");
		explicitlyWaitForElement("dropDown_Action_Association_Add_Usage_List", getTotalWaitTimeInSecs());
		click("dropDown_Action_Association_Add_Usage_List");
		//check provisional
		click("checkBox_Action_Association_Provisional");
		//add start date & end date
		sendKeys("textField_Action_Association_StartDate",startDate);
		
		sendKeys("textField_Action_Association_EndDate",endDate);
		//send text to description field
		sendKeys("textField_Action_Association_Add_Description","Automation Testing");
		//click add association button
		click("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",getTotalWaitTimeInSecs());
		verifyText("Created qas association from "+mirsapURN+" Action - Auto Test for Raise Action update to "+targetLinkURN,"successMsg_Action_Add_Association");
		click("icon_Action_Association_Reload");
		
		//add described type
		click("icon_Association_Add");
				
		verifyAddAssociation();
		//select described type association
		explicitlyWaitForElement("dropDown_Association_Add_Type",getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Association_Add_Type","Described");
		//enter nominal urn
		sendKeys("textField_Association_Add_TargetURN",targetDescribedURN);
		//send text to description field
		sendKeys("textField_Action_Association_Add_Description","Automation Testing");
		//click add association button
		click("button_Association_Add_Create");
		verifyText("Created Described association from "+mirsapURN+" Action - Auto Test for Raise Action update to "+targetDescribedURN,"successMsg_Action_Add_Association");
		click("icon_Action_Association_Reload");
		
		//add manual type
		click("icon_Association_Add");
		
		verifyAddAssociation();
		//select manual type association
		explicitlyWaitForElement("dropDown_Association_Add_Type",getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Association_Add_Type","Manual Potential Match");
		//enter nominal urn
		sendKeys("textField_Association_Add_TargetURN",targetManualpotentialmatchURN);
		//click add association button
		click("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",getTotalWaitTimeInSecs());
		verifyText("Created MANUAL POTENTIAL MATCH association from "+mirsapURN+" Action - Auto Test for Raise Action update to "+targetManualpotentialmatchURN,"successMsg_Action_Add_Association");
		clickAndWait("icon_Action_Association_Reload");
		
		//update Link Association
		waitForJQuery(driver);
		explicitlyWaitForElement("textField_Action_View_Association_TargetUrn",getTotalWaitTimeInSecs());
		sendKeysAndEnterWithDriver("textField_Action_View_Association_TargetUrn",incidentIdFull+"-"+targetLinkURN);
		waitForJQuery(driver);
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",getTotalWaitTimeInSecs());
		//clickAndWait("checkBox_Action_Association_FirstRecord");
		clickAndWait("checkBox_Action_Association_FirstRecord");
		explicitlyWaitForElement("icon_Action_Association_Update",getTotalWaitTimeInSecs());
		String currentParentWindowHandle1 = getCurrentWindowHandle();
		WebDriver softEntityWinDriver1 = clickAndWaitForBrowserPopup("Soft Entity Portlet","icon_Action_Association_Update");
		explicitlyWaitForElement("textField_Action_Association_Update_Descrption",getTotalWaitTimeInSecs());
		sendKeysWithDriver("textField_Action_Association_Update_Descrption","This is an updated description for automation testing");
		//amend date and time
		clickAndWait("button_Action_Association_Update_Save");
		verifyElementPresent("successMsg_Action_Association_Update_Save");
		//click("button_Action_Association_Update_Cancel");
		closePopupWindowAndNavigateToParentWindowHandle("Soft Entity Portlet", currentParentWindowHandle1);
		
		//update Described Association
		clickAndWait("tab_action");
		clickAndWait("button_Action_Association_Tab");
		clickAndWait("icon_Action_Association_Reload");
		//sendKeysAndEnterWithDriver("textField_Action_View_Association_TargetUrn", " ");
		waitForJQuery(driver);
		sendKeys("textField_Action_View_Association_TargetUrn",incidentIdFull+"-"+targetDescribedURN);
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",getTotalWaitTimeInSecs());
		clickAndWait("checkBox_Action_Association_FirstRecord");
		//clickAndWait("icon_Action_Association_Update");
		String currentParentWindowHandle = getCurrentWindowHandle();
		WebDriver softEntityWinDriver = clickAndWaitForBrowserPopup("Soft Entity Portlet","icon_Action_Association_Update");
		explicitlyWaitForElement("textField_Action_Association_Update_Descrption",getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Association_Update_Descrption","This is an updated description for automation testing");
		clickAndWait("button_Action_Association_Update_Save");
		verifyElementPresent("successMsg_Action_Association_Update_Save");
		//click("button_Action_Association_Update_Cancel");
		closePopupWindowAndNavigateToParentWindowHandle("Soft Entity Portlet", currentParentWindowHandle);
		
		//delete described association
		click("icon_Action_Association_Delete");
		waitForAlertTextAndClose("Are you sure you want to remove the cross reference?");
		explicitlyWaitForElement("successMsg_Action_Add_Association", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Add_Association");
		
		
		//verifyHistoryTab
		click("tab_History");
		explicitlyWaitForElement("table_History_HistoryRecord_Username",
				getTotalWaitTimeInSecs());
		verifyText(usernameUpperCase,"table_History_HistoryRecord_Username");
		verifyTextContains(CREATE_ASSOCIATION,"table_Action_HistoryRecord_CreateAssociation");
		verifyTextContains(UPDTAE_ASSOCIATION,"table_Action_HistoryRecord_CreateAssociation");
		verifyTextContains(DELETE_ASSOCIATION,"table_Action_HistoryRecord_CreateAssociation");

		click("table_Action_HistoryRecord_CreateAssociation");
		viewHistoryTabSearchResult(CREATE_ASSOCIATION);

		click("table_Action_HistoryRecord_UpdateAssociation");
		viewHistoryTabSearchResult(UPDTAE_ASSOCIATION);

		click("table_Action_HistoryRecord_CreateAssociation");
		viewHistoryTabSearchResult(DELETE_ASSOCIATION);
		
		endLine(method);
	}
	
	// Test case 30481:006. Action -Marker tab
	@Test(priority = 9)
	public void markerTab(Method method)
			throws Exception {
		
		startLine(method);
		// Raise Fast action
		String mirsapURN = raiseAndGetMirsapActionURN();
		viewAndGetMirsapActionURN(mirsapURN);
		//verifyFastActionTabEnabled();
		click("tab_Marker");
		verifyMarkerTabEnabled();
		// Add Marker
		click("icon_Marker_Add");
		verifyElementPresent("text_Marker_MarkerDialogbox");
		verifyElementPresent("textField_Marker_Add_Expirydate");
		verifyElementPresent("button_Marker_Add_Submit");
		verifyElementPresent("button_Marker_Add_Cancel");
		verifyElementPresent("checkbox_Marker_Add_Firstrecord");
		verifyElementPresent("icon_Marker_Add_Refresh");
		explicitlyWaitForElement("textField_Marker_Add_Expirydate",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Marker_Add_Expirydate", MARKER_EXPIRYDATEADD);
		click("checkbox_Marker_Add_Firstrecord");
		click("checkbox_Marker_Add_Secondrecord");
		click("button_Marker_Add_Submit");
		explicitlyWaitForElement("textField_Marker_ExpirydateEdited_Firstrow",
				getTotalWaitTimeInSecs());
		verifyText(MARKER_EXPIRYDATEADD,
				"textField_Marker_ExpirydateEdited_Firstrow");
		verifyText(MARKER_EXPIRYDATEADD,
				"textField_Marker_ExpirydateEdited_Secondrow");

		// Edit Marker

		click("checkbox_Marker_Firstrecord");
		click("icon_Marker_Edit");
		explicitlyWaitForElement("textField_Marker_Edit_Expirydate",
				getTotalWaitTimeInSecs());
		verifyElementPresent("textField_Marker_Edit_Expirydate");
		sendKeys("textField_Marker_Edit_Expirydate", MARKER_EXPIRYDATEEDITED);
		clickAndWait("button_Marker_Edit_Submit");
		explicitlyWaitForElement("textField_Marker_ExpirydateEdited_Secondrow",
				getTotalWaitTimeInSecs());
		verifyText(MARKER_EXPIRYDATEEDITED,
				"textField_Marker_ExpirydateEdited_Secondrow");

		// Delete Marker

		click("checkbox_Marker_Firstrecord");
		click("icon_Marker_Delete");
		// Verify pop up is displayed
		verifyText(MARKER_DELETE_POPUP, "label_Marker_Delete_Popup");
		clickAndWait("button_Marker_Delete_Yes");

		// validation for edit and delete icon
		explicitlyWaitForElement("icon_Marker_Edit", getTotalWaitTimeInSecs());
		click("icon_Marker_Edit");
		verifyText(MARKER_EDIT_POPUP_ERROR, "label_Marker_Delete_Popup");
		click("button_Marker_ErrorPopup_OK");
		click("icon_Marker_Delete");
		verifyText(MARKER_DELETE_POPUP_ERROR, "label_Marker_Delete_Popup");
		click("button_Marker_ErrorPopup_OK");
		
		endLine(method);
		
	}
	
	    // Test case 15376:002. Tags Tab
		@Test(priority = 5)
	      public void mirsapActiontagsTab(Method method) throws Exception{
			
			startLine(method);
			navigateTo(ACTION_DASHBOARD);
			explicitlyWaitForElement("dropDown_Action_ActionDashboard_Search_Type",getTotalWaitTimeInSecs());
			//Select type as "action"
			selectByVisibleText("dropDown_Action_ActionDashboard_Search_Type", SEARCH_TYPE);
			//click on search button
			click("button_Action_Search");
			explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
			//verify search result tab is enabled
			//select the first record
			click("checkBox_Action_SearchResulttab_Firstrow");
			explicitlyWaitForElement("icon_Action_Searchresult_View",getTotalWaitTimeInSecs());
			//click on view icon and wait for pop up
			//ArrayList<String> parentWinHandler1 = new ArrayList<String>();
			//parentWinHandler1.add(getCurrentWindowHandle());
			//clickAndWaitForChildWindow(MIRSAP_ACTION_TITLE_WINDOW, parentWinHandler1, "icon_Action_Searchresult_View", 1, 2);
			
			String currentParentWindowHandle = getCurrentWindowHandle();
	    	//click on view icon and wait for pop up window
	    	ArrayList<String> parentWinHandler1 = new ArrayList<String>();
	    	parentWinHandler1.add(currentParentWindowHandle);
	    	WebDriver childWindDriver = clickAndWaitForChildWindow(MIRSAP_ACTION_TITLE_WINDOW, parentWinHandler1, "icon_Action_Searchresult_View", 1, 2);
	    	verifyMirsapActionTabEnabled();
			
			//click on tags tab and add some tags
			
			explicitlyWaitForElement("tab_tags",getTotalWaitTimeInSecs());
			click("tab_tags");
			explicitlyWaitForElement("button_Tags_Edit",getTotalWaitTimeInSecs());
			click("button_Tags_Edit");
			//check the first tag
			click("button_Tags_firsttag");
			click("button_Tags_Save");
			
			explicitlyWaitForElement("successMsg_Tags_Update",
					getTotalWaitTimeInSecs());
			verifyElementPresent("successMsg_Tags_Update");
		
			//Click on edit button again and remove tags
			
			click("button_Tags_Edit");
			//un check the same first tag 
			click("button_Tags_firsttag");
			click("button_Tags_Save");
			explicitlyWaitForElement("successMsg_Tags_Update",
					getTotalWaitTimeInSecs());
			verifyElementPresent("successMsg_Tags_Update");
			
			//verifyHistoryTab
			click("tab_History");
			explicitlyWaitForElement("table_History_HistoryRecord_Username",
					getTotalWaitTimeInSecs());
			verifyText(usernameUpperCase,"table_History_HistoryRecord_Username");
			verifyTextContains(ADD_TAG_TO_RECORD,"table_Action_HistoryRecord_AddTagToRecord");
			click("table_Action_HistoryRecord_AddTagToRecord");
			viewHistoryTabSearchResult(ADD_TAG_TO_RECORD);
			
			//close the pop up window and navigate back to search result tab
			//closePopupWindowAndNavigateToParentWindow(MIRSAP_ACTION_TITLE_WINDOW, ACTION_DASHBOARD_TITLE_WINDOW);
			closechildWindowAndNavigateToItsParentWindowHandle(childWindDriver, currentParentWindowHandle, 2, 2);
			
			endLine(method);
			
		}
		
		// *Test case 30478:004. Transfer Association 
       @Test(priority = 7)
	      public void TransferAssociation(Method method) throws Exception {
	    	  startLine(method);
	    	  //Raise mirsap action
	    	  String mirsapURN = raiseAndGetMirsapActionURN();
	    	  //View mirsap action
	    	  viewAndGetMirsapActionURN(mirsapURN);
	    	  verifyMirsapActionTabEnabled();
	    	  click("tab_Association");
	    	  verifyAssociationTabEnabled();

	    	  //Create Link type of association and transfer association
	    	  click("icon_Association_Add");
	  		  if (!verifyElementDisplayed("dropDown_Association_Add_Type")) {
	  			click("icon_Association_Add");
	  		  } 
	    	  click("icon_Association_Add");
	    	  verifyElementPresent("dropDown_Association_Add_Type");
	    	  selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_LINK);
	    	  sendKeys("textField_Association_Add_TargetURN", targetLinkURN);
	    	  waitForLinkTypeUsageDropdownLoad();
	    	  click("dropDown_Action_Association_Add_Usage");
	    	  click("dropDown_Action_Association_Add_Usage_input");
	    	  explicitlyWaitForElement("dropDown_Action_Association_Add_Usage_List", getTotalWaitTimeInSecs());
	    	  click("dropDown_Action_Association_Add_Usage_List");
	    	  click("button_Association_Add_Create");
	    	  explicitlyWaitForElement("successMsg_Action_Add_Association",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("successMsg_Action_Add_Association");
	    	  String succMess_Link = getText("successMsg_Action_Add_Association");
	    	  String[] actUrn_Link = succMess_Link.split(" ");
	    	  int size7 = actUrn_Link.length;
	    	  String Link_urn = actUrn_Link[size7-1];
	    	  System.out.println(Link_urn);
	    	  verifyTextCompare(targetLinkURN,Link_urn);
	    	  click("checkBox_Action_Association_FirstRecord");
	    	  //transfer association to another action record
	    	  click("icon_Action_Association_TransferAssociation");
	    	  explicitlyWaitForElement("text_Association_TransferAssociation_Dialogbox",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("text_Association_TransferAssociation_Dialogbox");
	    	  verifyElementPresent("dropDown_Association_TransferAssociation_ReferenceIncident");
	    	  verifyElementPresent("dropDown_Association_TransferAssociation_DefaultlistRecord");
	    	  verifyElementPresent("textfield_Association_TransferAssociation_URN");

	    	  click("dropDown_Association_TransferAssociation_ReferenceIncident_Arrow");
	    	  click("dropDown_Association_TransferAssociation_ReferenceIncident_Incident");

	    	  sendKeys("textfield_Association_TransferAssociation_URN", transferAssociationURN);
	    	  click("button_Association_TransferAssociation_Submit");
	    	  verifyElementPresent("successMsg_Action_Add_Association");
	    	  //view the action and verify that it is transfered
	    	  navigateTo(VIEWACTION_URL);
	    	  // verifyIsInViewActionPage();
	    	  // verify View Action Tab
	    	  explicitlyWaitForElement("tab_View_Action",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("tab_View_Action");
	    	  // verify cursor focus in URN text field
	    	  //verifyCurrentCursorFocus("textField_Action_ViewUrn");
	    	  sendKeys("textField_Action_ViewUrn", transferAssociationURN);// fastActionURN
	    	  clickAndWaitForPageLoad("button_Action_View");
	    	  explicitlyWaitForElement("label_Action_View_Page_Title",
	    			  getTotalWaitTimeInSecs());
	    	  isInMirsapActionViewPage();
	    	  click("tab_Association");
	    	  explicitlyWaitForElement("tab_Association",
	    			  getTotalWaitTimeInSecs());
	    	  verifyAssociationTabEnabled();
	    	  verifyElementPresent("table_Action_AssociationTab_TargetURN_FirstRecord");
	    	  sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
	  				incidentIdFull + "-" + targetLinkURN);
	  		  explicitlyWaitForElement("table_Action_AssociationTab_TargetURN_FirstRecord", getTotalWaitTimeInSecs());
	  		   verifyElementPresent("table_Action_AssociationTab_TargetURN_FirstRecord");


	    	  //Create Described type of association and transfer association

	    	  mirsapURN = raiseAndGetMirsapActionURN();
	    	  //View Fast action
	    	  viewAndGetMirsapActionURN(mirsapURN);
	    	  verifyMirsapActionTabEnabled();
	    	  click("tab_Association");
	    	  verifyAssociationTabEnabled();

	    	  click("icon_Association_Add");
	    	  verifyElementPresent("dropDown_Association_Add_Type");
	    	  selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_DESCRIBED);
	    	  sendKeys("textField_Association_Add_TargetURN", targetDescribedURN);
	    	  sendKeys("textField_Action_Association_Add_Description", "Dev smoke test - Described association");
	    	  click("button_Association_Add_Create");
	    	  explicitlyWaitForElement("successMsg_Action_Add_Association",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("successMsg_Action_Add_Association");
	    	  String succMess_Described = getText("successMsg_Action_Add_Association");
	    	  String[] actUrn_Described = succMess_Described.split(" ");
	    	  int size8 = actUrn_Described.length;
	    	  String described_urn = actUrn_Described[size8-1];
	    	  System.out.println(described_urn);
	    	  verifyTextCompare(targetDescribedURN,described_urn);

	    	  explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
	    			  getTotalWaitTimeInSecs());
	    	  click("checkBox_Action_Association_FirstRecord");
	    	  //transfer association to another action record
	    	  click("icon_Action_Association_TransferAssociation");
	    	  explicitlyWaitForElement("text_Association_TransferAssociation_Dialogbox",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("text_Association_TransferAssociation_Dialogbox");
	    	  verifyElementPresent("dropDown_Association_TransferAssociation_ReferenceIncident");
	    	  verifyElementPresent("dropDown_Association_TransferAssociation_DefaultlistRecord");
	    	  verifyElementPresent("textfield_Association_TransferAssociation_URN");

	    	  click("dropDown_Association_TransferAssociation_ReferenceIncident_Arrow");
	    	  click("dropDown_Association_TransferAssociation_ReferenceIncident_Incident");

	    	  sendKeys("textfield_Association_TransferAssociation_URN", transferAssociationURN);
	    	  click("button_Association_TransferAssociation_Submit");
	    	  verifyElementPresent("successMsg_Action_Add_Association");
	    	  //view the action and verify that it is transfered
	    	  navigateTo(VIEWACTION_URL);
	    	  // verifyIsInViewActionPage();
	    	  // verify View Action Tab
	    	  explicitlyWaitForElement("tab_View_Action",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("tab_View_Action");
	    	  // verify cursor focus in URN text field
	    	  //verifyCurrentCursorFocus("textField_Action_ViewUrn");
	    	  sendKeys("textField_Action_ViewUrn", transferAssociationURN);// fastActionURN
	    	  clickAndWaitForPageLoad("button_Action_View");
	    	  explicitlyWaitForElement("label_Action_View_Page_Title",
	    			  getTotalWaitTimeInSecs());
	    	  isInMirsapActionViewPage();
	    	  click("tab_Association");
	    	  explicitlyWaitForElement("tab_Association",
	    			  getTotalWaitTimeInSecs());
	    	  verifyAssociationTabEnabled();
	    	  verifyElementPresent("table_Action_AssociationTab_TargetURN_FirstRecord");


	    	  //Create Subject type of association with nominal record and then transfer 
	    	  mirsapURN = raiseAndGetMirsapActionURN();
	    	  //View Fast action
	    	  viewAndGetMirsapActionURN(mirsapURN);
	    	  verifyMirsapActionTabEnabled();
	    	  click("tab_Association");
	    	  verifyAssociationTabEnabled();
	    	  click("icon_Association_Add");
	    	  verifyElementPresent("dropDown_Association_Add_Type");
	    	  selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_SUBJECT);
	    	  sendKeys("textField_Association_Add_TargetURN", targetSubjectURN);
	    	  click("button_Association_Add_Create");
	    	  explicitlyWaitForElement("successMsg_Action_Add_Association",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("successMsg_Action_Add_Association");
	    	  String succMess_Subject = getText("successMsg_Action_Add_Association");
	    	  String[] actUrn_Subject = succMess_Subject.split(" ");
	    	  int size9 = actUrn_Subject.length;
	    	  String Subject_urn = actUrn_Subject[size9-1];
	    	  System.out.println(Subject_urn);
	    	  verifyTextCompare(targetSubjectURN,Subject_urn);
	    	  explicitlyWaitForElement("checkBox_Action_Association_SecondRecord",
	    			  getTotalWaitTimeInSecs());
	    	  click("checkBox_Action_Association_SecondRecord");
	    	  //transfer association to another action record
	    	  click("icon_Action_Association_TransferAssociation");
	    	  explicitlyWaitForElement("text_Association_TransferAssociation_Dialogbox",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("text_Association_TransferAssociation_Dialogbox");
	    	  verifyElementPresent("dropDown_Association_TransferAssociation_ReferenceIncident");
	    	  verifyElementPresent("dropDown_Association_TransferAssociation_DefaultlistRecord");
	    	  verifyElementPresent("textfield_Association_TransferAssociation_URN");

	    	  click("dropDown_Association_TransferAssociation_ReferenceIncident_Arrow");
	    	  click("dropDown_Association_TransferAssociation_ReferenceIncident_Incident");

	    	  sendKeys("textfield_Association_TransferAssociation_URN", transferAssociationURN);
	    	  click("button_Association_TransferAssociation_Submit");
	    	  verifyElementPresent("errorMsg_Action_Association_Transfer");

	    	  //Create manual potential match type of association and transfer association
	    	  mirsapURN = raiseAndGetMirsapActionURN();
	    	  //View Fast action
	    	  viewAndGetMirsapActionURN(mirsapURN);
	    	  verifyMirsapActionTabEnabled();
	    	  click("tab_Association");
	    	  verifyAssociationTabEnabled();
	    	  click("icon_Association_Add");
	    	  verifyElementPresent("dropDown_Association_Add_Type");
	    	  selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_MANUALPOTENTIALMATCH);
	    	  sendKeys("textField_Association_Add_TargetURN", targetManualpotentialmatchURN);
	    	  click("button_Association_Add_Create");
	    	  explicitlyWaitForElement("successMsg_Action_Add_Association",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("successMsg_Action_Add_Association");
	    	  String succMess_MPM = getText("successMsg_Action_Add_Association");
	    	  String[] actUrn_MPM = succMess_MPM.split(" ");
	    	  int size10 = actUrn_MPM.length;
	    	  String MPM_urn = actUrn_MPM[size10-1];
	    	  System.out.println(MPM_urn);
	    	  verifyTextCompare(targetManualpotentialmatchURN,MPM_urn);
	    	  explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
	    			  getTotalWaitTimeInSecs());
	    	  click("checkBox_Action_Association_FirstRecord");
	    	  //transfer association to another action record
	    	  click("icon_Action_Association_TransferAssociation");
	    	  explicitlyWaitForElement("text_Association_TransferAssociation_Dialogbox",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("text_Association_TransferAssociation_Dialogbox");
	    	  verifyElementPresent("dropDown_Association_TransferAssociation_ReferenceIncident");
	    	  verifyElementPresent("dropDown_Association_TransferAssociation_DefaultlistRecord");
	    	  verifyElementPresent("textfield_Association_TransferAssociation_URN");

	    	  click("dropDown_Association_TransferAssociation_ReferenceIncident_Arrow");
	    	  click("dropDown_Association_TransferAssociation_ReferenceIncident_Incident");

	    	  sendKeys("textfield_Association_TransferAssociation_URN", transferAssociationURN);
	    	  click("button_Association_TransferAssociation_Submit");
	    	  verifyElementPresent("errorMsg_Action_Association_Transfer");

	    	  //raise fast action and move it to allocated state and add described type association and transfer

	    	  startLine(method);
	    	  mirsapURN=raiseAndGetMirsapActionURN();
	    	  verifyText(FOR_ALLOCATION, "label_Action_View_State");
	    	  //getList and verify state in dropdown
	    	  getAllListItemAndVerify( ALLOCATED,"dropDown_Action_Select_State");
	    	  getAllListItemAndVerify( FOR_REFERRAL,"dropDown_Action_Select_State");
	    	  getAllListItemAndVerify( PENDED,"dropDown_Action_Select_State");
	    	  //change state button click
	    	  clickAndWait("button_Action_ChangeState");
	    	  //wait for allocation officer
	    	  explicitlyWaitForElement("button_Action_ChangeState_Save",
	    			  getTotalWaitTimeInSecs());
	    	  explicitlyWaitForElement("textfield_Action_MirsapAction_QueueAllocatedOfficerOne",
	    			  getTotalWaitTimeInSecs());
	    	  verifyMirsapActionFieldsInAllocatedPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
	    	  insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
	    	  //Add First Create Officer
	    	  clickAndWait("button_Action_MirsapAction_CreateOfficerOne");
	    	  explicitlyWaitForElement(
	    			  "textfield_Action_CreateOfficer_Surname",
	    			  getTotalWaitTimeInSecs());
	    	  sendKeys("textfield_Action_CreateOfficer_Surname",
	    			  CREATE_OFFICER_SURNAME);
	    	  sendKeys("textfield_Action_CreateOfficer_Forename",
	    			  CREATE_OFFICER_FORENAME);
	    	  clickAndWait("button_Action_MirsapAction_CreateOne");
	    	  clickAndWait("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");
	    	  insertTextInAllocatedOfficerTextField("textfield_Action_FastAction_QueueAllocatedOfficer",CREATE_OFFICER_SURNAME);
			  selectOptionWithText(CREATE_OFFICER_SURNAME, "textfield_Action_StateMove_AllocatedOfficerName");
				// Perform Save on Allocated Page
			 clickAndWait("button_Action_ChangeState_Save");
			  explicitlyWaitForElement("successMsg_Action_Create",
						getTotalWaitTimeInSecs());
				verifyElementPresent("successMsg_Action_Create");
				verifyText(ALLOCATED, "label_Action_View_State");

	    	  click("tab_Association");
	    	  verifyAssociationTabEnabled();
	    	  click("icon_Association_Add");
	    	  verifyElementPresent("dropDown_Association_Add_Type");
	    	  selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_DESCRIBED);
	    	  sendKeys("textField_Association_Add_TargetURN", targetDescribedURN);
	    	  sendKeys("textField_Action_Association_Add_Description", "Dev smoke test - Described association");
	    	  click("button_Association_Add_Create");
	    	  explicitlyWaitForElement("successMsg_Action_Add_Association",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("successMsg_Action_Add_Association");
	    	  String succMess_Desallocated = getText("successMsg_Action_Add_Association");
	    	  String[] actUrn_Desallocated = succMess_Desallocated.split(" ");
	    	  int size11 = actUrn_Desallocated.length;
	    	  String desallocated_urn = actUrn_Desallocated[size11-1];
	    	  System.out.println(desallocated_urn);
	    	  verifyTextCompare(targetDescribedURN,desallocated_urn);

	    	  explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
	    			  getTotalWaitTimeInSecs());
	    	  click("checkBox_Action_Association_FirstRecord");
	    	  //transfer association to another action record
	    	  click("icon_Action_Association_TransferAssociation");
	    	  explicitlyWaitForElement("text_Association_TransferAssociation_Dialogbox",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("text_Association_TransferAssociation_Dialogbox");
	    	  verifyElementPresent("dropDown_Association_TransferAssociation_ReferenceIncident");
	    	  verifyElementPresent("dropDown_Association_TransferAssociation_DefaultlistRecord");
	    	  verifyElementPresent("textfield_Association_TransferAssociation_URN");

	    	  click("dropDown_Association_TransferAssociation_ReferenceIncident_Arrow");
	    	  click("dropDown_Association_TransferAssociation_ReferenceIncident_Incident");

	    	  sendKeys("textfield_Association_TransferAssociation_URN", transferAssociationURN);
	    	  click("button_Association_TransferAssociation_Submit2");
	    	  verifyElementPresent("successMsg_Action_Add_Association");
	    	  //view the action and verify that it is transfered
	    	  navigateTo(VIEWACTION_URL);
	    	  // verifyIsInViewActionPage();
	    	  // verify View Action Tab
	    	  explicitlyWaitForElement("tab_View_Action",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("tab_View_Action");
	    	  // verify cursor focus in URN text field
	    	  //verifyCurrentCursorFocus("textField_Action_ViewUrn");
	    	  sendKeys("textField_Action_ViewUrn", transferAssociationURN);// fastActionURN
	    	  clickAndWaitForPageLoad("button_Action_View");
	    	  explicitlyWaitForElement("label_Action_View_Page_Title",
	    			  getTotalWaitTimeInSecs());
	    	  isInMirsapActionViewPage();
	    	  click("tab_Association");
	    	  explicitlyWaitForElement("tab_Association",
	    			  getTotalWaitTimeInSecs());
	    	  verifyAssociationTabEnabled();
	    	  verifyElementPresent("table_Action_AssociationTab_TargetURN_FirstRecord");

	    	  //donot select any record and click on transfer association button
	    	  click("icon_Action_Association_TransferAssociation");
	    	  verifyText(TRANSFER_POPUP_ERROR, "label_Transfer_Popup");
	    	  click("button_Marker_ErrorPopup_OK");
	    	 
	    	  endLine(method);



	      }      
	
	      @Test(priority = 8)
	      public void mirsapActionAttachmentsTab(Method method) throws Exception{
	    	  startLine(method);
	  		
	    		Path p = Paths.get(attachement);
	    		String fileName = p.getFileName().toString();

	    		Path p2 = Paths.get(attachement2);
	    		String fileName2 = p.getFileName().toString();

	    		// file upload
	    		// navigate to view action page
	    		String mirsapActionAttachmentURN = raiseAndGetMirsapActionURN();
		    	  navigateTo(VIEWACTION_URL);
		    	  verifyIsInViewActionPage();
	    		sendKeys("textField_Action_ViewUrn", mirsapActionAttachmentURN);
	    		clickAndWaitForPageLoad("button_Action_View");
	    		waitForPageLoad();
	    		isInMirsapActionViewPage();
	    		explicitlyWaitForElement("tab_Attachment", getTotalWaitTimeInSecs());
	    		// click on Attachments Tab
	    		clickAndWait("tab_Attachment");

	    		verifyElementPresent("tab_Attachment_FileUpload");
	    		verifyElementPresent("tab_Attachment_InternalLinkUpload");
	    		verifyElementPresent("tab_Attachment_ExternalLinkUpload");
	    		verifyElementPresent("button_Attachment_AddRow");
	    		verifyElementPresent("button_Attachment_SaveAttachments");
	    		// verifyElementNotPresent("tab_AttachmentTick");
	    		Report.info("First file details:" + attachement);
	    		explicitlyWaitForElement("button_Attachment_Browse", getTotalWaitTimeInSecs());
	    		sendKeysWithDriverNoClick("button_Attachment_Browse", attachement);
	    		clickAndWait("button_Attachment_SaveAttachments");
	    		verifyText(fileName, "link_Attachment_FileUpload_FirstFile");

	    		// Verification of View Action Page
	    		verifyElementPresent("successMsg_Attachment");
	    		String succMess = getText("successMsg_Attachment");
	    		String[] fileUploadSuccMess = succMess.split(" ");
	    		String incidentAction = fileUploadSuccMess[4];
	    		String[] incidentIDWithAction = incidentAction.split("-");
	    		String incidentID = incidentIDWithAction[0];
	    		String ActionURN = incidentIDWithAction[1];
	    		verifyTextCompare(incidentID, incidentIdFull);
	    		verifyTextCompare(ActionURN, mirsapActionAttachmentURN);
	    		// verifyTextCompare(ActionURN, "A1");

	    		// AddRow validation
	    		verifyElementNotPresent("table_Attachment_FileUpload_SecondRow");
	    		click("button_Attachment_AddRow");
	    		verifyElementPresent("table_Attachment_FileUpload_SecondRow");

	    		// Delete row validation
	    		click("checkBox_Attachment_FileUpload_SecondRow");
	    		click("button_Attachment_DeleteRow");
	    		verifyElementNotPresent("table_Attachment_FileUpload_SecondRow");

	    		// Delete File Upload
	    		sendKeysWithDriverNoClick("button_Attachment_Browse", attachement2);
	    		clickAndWait("button_Attachment_SaveAttachments");
	    		verifyElementPresent("successMsg_Attachment");
	    		verifyText(FilenameUtils.getBaseName(attachement2) 
	    				+ "." + FilenameUtils.getExtension(attachement2), "link_Attachment_FileUpload_SecondFile");
	    		click("icon_Attachment_FileUpload_Delete_Second");
	    		waitForAlertTextAndClose(CONFIRMATION_MESSAGE_TO_DELETE_AN_ATTACHMENT);
	    		String expectedDelFileUploadText = "File " 
	    				+ FilenameUtils.getBaseName(attachement2) 
	    				+ "." + FilenameUtils.getExtension(attachement2)
	    				+ " deleted from " + incidentIdFull + "-"
	    				+ mirsapActionAttachmentURN;

	    		Report.info("Construted Expected expectedDelFileUploadText "
	    				+ expectedDelFileUploadText);
	    		waitForJQuery(driver);
	    		waitForPageLoad();
	    		explicitlyWaitForElement("successMsg_Attachment",getTotalWaitTimeInSecs());
	    		verifyText(expectedDelFileUploadText, "successMsg_Attachment");

	    		// internal link
	    		explicitlyWaitForElement("tab_Attachment_InternalLinkUpload", getTotalWaitTimeInSecs());
	    		click("tab_Attachment_InternalLinkUpload");
	    		String interLink = "\\\\\\\\gbhlm-vm-fs01\\\\U-LEAF_Test\\\\Holmes Futures\\\\System Management UAT\\\\System_Management_Acceptance_Test_Script.doc";

	    		String interLinkFileName = FilenameUtils.getBaseName(interLink) + "." + FilenameUtils.getExtension(interLink);
	    		explicitlyWaitForElement("textfield_Attachment_InternalLinkUpload", getTotalWaitTimeInSecs());
	    		click("textfield_Attachment_InternalLinkUpload");
	    		sleep(2000);
	    		sendKeys("textfield_Attachment_InternalLinkUpload", interLink);
	    		explicitlyWaitForElement("button_Attachment_SaveAttachments", getTotalWaitTimeInSecs());
	    		
	    		clickAndWait("button_Attachment_SaveAttachments");
	    		verifyElementPresent("successMsg_Attachment");
	    		String expectedInternalLinkSuccText = "File " + interLinkFileName
	    				+ " attached to " + incidentIdFull + "-"
	    				+ mirsapActionAttachmentURN;
	    		
	    		verifyText(expectedInternalLinkSuccText, "successMsg_Attachment");
	    		clickAndWait("button_Attachment_SaveAttachments");
	    		explicitlyWaitForElement("errorMsg_Attachment", getTotalWaitTimeInSecs());
	    		verifyElementPresent("errorMsg_Attachment");
	    		verifyText(ERROR_MESSAGE_FILE_INTERNAL_OR_EXTERNAL_LINK,
	    				"errorMsg_Attachment");

	    		// external link
	    		explicitlyWaitForElement("tab_Attachment_ExternalLinkUpload", getTotalWaitTimeInSecs());
	    		click("tab_Attachment_ExternalLinkUpload");
	    		String externalLink = "http://www.cs.nott.ac.uk/~cah/G53QAT/G53QAT10pdf6up.pdf";
	    		
	    		String externalLinkFileName = FilenameUtils.getBaseName(externalLink) + "." + FilenameUtils.getExtension(externalLink);
	    		click("tab_Attachment_ExternalLinkUpload");
	    		sendKeys("textfield_Attachment_ExternalLinkUpload", externalLink);
	    		clickAndWait("button_Attachment_SaveAttachments");
	    		verifyElementPresent("successMsg_Attachment");
	    		String expectedExternalLinkSuccText = "File " + externalLinkFileName
	    				+ " attached to " + incidentIdFull + "-"
	    				+ mirsapActionAttachmentURN;
	    		
	    		verifyTextContains(expectedExternalLinkSuccText, "successMsg_Attachment");
	    		click("button_Attachment_SaveAttachments");
	    		explicitlyWaitForElement("errorMsg_Attachment", getTotalWaitTimeInSecs());
	    		verifyElementPresent("errorMsg_Attachment");
	    		verifyText(ERROR_MESSAGE_FILE_INTERNAL_OR_EXTERNAL_LINK,
	    				"errorMsg_Attachment");
	    		
	    		//verifyHistoryTab
	    		clickAndWait("tab_History");
	    		explicitlyWaitForElement("table_History_HistoryRecord_Username",
	    				getTotalWaitTimeInSecs());
	    		
	    		verifyText(usernameUpperCase,"table_History_HistoryRecord_Username");
	    		verifyTextContains(ADD_ATTACHEMENT,"table_Action_HistoryRecord_AddAttachment");
	    		verifyTextContains(REMOVE_ATTACHEMENT,"table_Action_HistoryRecord_RemoveAttachment");
	    		click("table_Action_HistoryRecord_AddAttachment");
	    		viewHistoryTabSearchResult(ADD_ATTACHEMENT);
	    		click("table_Action_HistoryRecord_RemoveAttachment");
	    		viewHistoryTabSearchResult(REMOVE_ATTACHEMENT);

	    		
	    		endLine(method);
	  		
	    	endLine(method);
	    	
	      }	
	
	      //Test case 34665:001b. Mirsap action Association Tab � Creating
	      @Test(priority = 4)
	      public void CreateAssociationMirsapActionAssociationtab(Method method) throws Exception
	      {
	    	  startLine(method);
	    	  String mirsapURN = raiseAndGetMirsapActionURN();
	    	  click("tab_tags");
		  	  click("tab_Association");
			  verifyAssociationTabEnabled();
		  	  explicitlyWaitForElement("icon_Association_Add",
		  				getTotalWaitTimeInSecs());
	    	  //verify that association tab is enabled
	    	  verifyAssociationTabEnabled();
	    	  //click on add association 
	    	  click("icon_Association_Add");
	    	  verifyElementPresent("dropDown_Association_Add_Type");

	    	  //Create Link type of association
	    	  selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_LINK);
	    	  sendKeys("textField_Association_Add_TargetURN", targetLinkURN);
	    	  waitForLinkTypeUsageDropdownLoad();
	    	  click("dropDown_Action_Association_Add_Usage");
	    	  click("dropDown_Action_Association_Add_Usage_input");
	    	  explicitlyWaitForElement("dropDown_Action_Association_Add_Usage_List", getTotalWaitTimeInSecs());
	    	  click("dropDown_Action_Association_Add_Usage_List");
	    	  click("button_Association_Add_Create");
	    	  explicitlyWaitForElement("successMsg_Action_Add_Association",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("successMsg_Action_Add_Association");
	    	  String succMess_Link = getText("successMsg_Action_Add_Association");
	    	  String[] actUrn_Link = succMess_Link.split(" ");
	    	  int size1 = actUrn_Link.length;
	    	  String Link_urn = actUrn_Link[size1-1];
	    	  System.out.println(Link_urn);
	    	  verifyTextCompare(targetLinkURN,Link_urn);

	    	  //Create Described type of association

	    	  click("icon_Association_Add");
	    	  verifyElementPresent("dropDown_Association_Add_Type");
	    	  selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_DESCRIBED);
	    	  sendKeys("textField_Association_Add_TargetURN", targetDescribedURN);
	    	  sendKeys("textField_Action_Association_Add_Description", "Dev smoke test - Described association");
	    	  click("button_Association_Add_Create");
	    	  explicitlyWaitForElement("successMsg_Action_Add_Association",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("successMsg_Action_Add_Association");
	    	  String succMess_Described = getText("successMsg_Action_Add_Association");
	    	  String[] actUrn_Described = succMess_Described.split(" ");
	    	  int size2 = actUrn_Described.length;
	    	  String described_urn = actUrn_Described[size2-1];
	    	  System.out.println(described_urn);
	    	  verifyTextCompare(targetDescribedURN,described_urn);

	    	  //Create Subject type of association with nominal record
	       	  click("tab_tags");
	    	  click("tab_Association");
	    	  clickAndWait("icon_Association_Add");
	    	  verifyElementPresent("dropDown_Association_Add_Type");
	    	  selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_SUBJECT);
	    	  sendKeys("textField_Association_Add_TargetURN", targetSubjectURN);
	    	  explicitlyWaitForElement("button_Association_Add_Create",
	    			  getTotalWaitTimeInSecs());
	    	  clickAndWait("button_Association_Add_Create");
	    	  waitForJQuery(driver);
	    	  explicitlyWaitForElement("successMsg_Action_Add_Association",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("successMsg_Action_Add_Association");
	    	  String succMess_Subject = getText("successMsg_Action_Add_Association");
	    	  String[] actUrn_Subject = succMess_Subject.split(" ");
	    	  int size3 = actUrn_Subject.length;
	    	  String Subject_urn = actUrn_Subject[size3-1];
	    	  System.out.println(Subject_urn);
	    	  verifyTextCompare(targetSubjectURN,Subject_urn);

	    	  //Create manual potential match type of association 
	    	  click("icon_Association_Add");
	    	  verifyElementPresent("dropDown_Association_Add_Type");
	    	  selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_MANUALPOTENTIALMATCH);
	    	  sendKeys("textField_Association_Add_TargetURN", targetManualpotentialmatchURN);
	    	  click("button_Association_Add_Create");
	    	  explicitlyWaitForElement("successMsg_Action_Add_Association",
	    			  getTotalWaitTimeInSecs());
	    	  verifyElementPresent("successMsg_Action_Add_Association");
	    	  String succMess_MPM = getText("successMsg_Action_Add_Association");
	    	  String[] actUrn_MPM = succMess_MPM.split(" ");
	    	  int size4 = actUrn_MPM.length;
	    	  String MPM_urn = actUrn_MPM[size4-1];
	    	  System.out.println(MPM_urn);
	    	  verifyTextCompare(targetManualpotentialmatchURN,MPM_urn);
	    	  //Close the child window and navigate back to parent window
	    	  //closechildWindowAndNavigateToItsParentWindowHandle(childWindDriver, currentParentWindowHandle, 2, 2);
	    	  
	    	  endLine(method);

	      }
       
	      //Test case 11161:003. Profile Tab
	      @Test(priority = 6)
	      public void mirsapActionProfileTab(Method method) throws Exception
	      {
	    	  startLine(method);
	    	  navigateTo(ACTION_DASHBOARD);
	    	  explicitlyWaitForElement("dropDown_Action_ActionDashboard_Search_Type",getTotalWaitTimeInSecs());
	    	  //select search type as action
	    	  selectByVisibleText("dropDown_Action_ActionDashboard_Search_Type", SEARCH_TYPE);
	    	  //Click on search
	    	  click("button_Action_Search");
	    	  explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
	    	  //verify search result tab is enabled
	    	  verifySearchResultTabEnabled();
	    	  //select the first record
	    	  click("checkBox_Action_SearchResulttab_Firstrow");
	    	  explicitlyWaitForElement("icon_Action_Searchresult_View",getTotalWaitTimeInSecs());
	    	  String currentParentWindowHandle = getCurrentWindowHandle();
	    	  //click on view icon and wait for pop up window
	    	  ArrayList<String> parentWinHandler1 = new ArrayList<String>();
	    	  parentWinHandler1.add(currentParentWindowHandle);
	    	  WebDriver childWindDriver = clickAndWaitForChildWindow(MIRSAP_ACTION_TITLE_WINDOW, parentWinHandler1, "icon_Action_Searchresult_View", 1, 2);
	    	  verifyMirsapActionTabEnabled();
	    	  click("tab_Profile");
	    	  verifyProfileTabEnabled();

	    	  //click on add icon
	    	  clickAndWait("icon_Profile_Add");
	    	  explicitlyWaitForElement("text_Profile_Add_Dialogbox",getTotalWaitTimeInSecs());
	    	  verifyElementPresent("text_Profile_Add_Dialogbox");
	    	  explicitlyWaitForElement("checkbox_Profile_Add_Firstrecord",getTotalWaitTimeInSecs());
	    	  clickAndWait("checkbox_Profile_Add_Firstrecord");
	    	  clickAndWait("button_Profile_Add_Submit");
	    	  explicitlyWaitForElement("textField_Profile_Firstrecord",getTotalWaitTimeInSecs());
	    	  verifyElementPresent("textField_Profile_Firstrecord");

	    	  //click on edit icon
	    	  click("checkbox_Profile_Firstrecord");
	    	  click("icon_Profile_Edit");
	    	  verifyElementPresent("text_Profile_Edit_Dialogbox");
	    	  sendKeys("textField_Profile_Edit_Note", PROFILE_EDIT_NOTE);
	    	  click("button_Profile_Edit_Submit");
	    	  explicitlyWaitForElement("textField_Profile_Firstrecord_Note",getTotalWaitTimeInSecs());
	    	  verifyElementPresent("textField_Profile_Firstrecord_Note");

	    	  //click on delete icon
	    	  explicitlyWaitForElement("checkbox_Profile_Firstrecord",getTotalWaitTimeInSecs());
	    	  click("checkbox_Profile_Firstrecord");
	    	  click("icon_Profile_Delete");
	    	  verifyText(PROFILE_DELETE_POPUP,"label_Profile_Delete_Popup");
	    	  click("button_Profile_Delete_Yes");

	    	  //donot select any record and click on edit
	    	  explicitlyWaitForElement("icon_Profile_Edit",
	    			  getTotalWaitTimeInSecs());
	    	  click("icon_Profile_Edit");
	    	  verifyText(PROFILE_EDIT_POPUP_ERROR, "label_Profile_Delete_Popup");
	    	  click("button_Profile_ErrorPopup_OK");
	    	  click("icon_Profile_Delete");
	    	  verifyText(PROFILE_DELETE_POPUP_ERROR, "label_Profile_Delete_Popup");
	    	  click("button_Profile_ErrorPopup_OK");
	    	  //closePopupWindowAndNavigateToParentWindow(MIRSAP_ACTION_TITLE_WINDOW, ACTION_DASHBOARD_TITLE_WINDOW);
              closechildWindowAndNavigateToItsParentWindowHandle(childWindDriver, currentParentWindowHandle, 2, 2);
              
	    	  endLine(method);

	      }
	      
	    // Test case 30483*:008. Audit Log Entry- Action 
	      
	    @Test(priority = 11)
	  	public void mirsapAction_AuditLogEntry(Method method) throws Exception {
	  		startLine(method);

	  		AuditLogSearch au1 = new AuditLogSearch(driver);
	  		// search audit and verify result
	  		au1.auditLogSearchByActivity(SEARCH_FAST_ACTION);
	  		verifyAuditResult(SEARCH_FAST_ACTION);

	  		au1.auditLogSearchByActivity(UPDATE_ACTION);
	  		verifyAuditResult(UPDATE_ACTION);

	  		au1.auditLogSearchByActivity(VIEW_ACTION);
	  		verifyAuditResult(VIEW_ACTION);

	  		au1.auditLogSearchByActivity(CREATE_ASSOCIATION);
	  		verifyAuditResult(CREATE_ASSOCIATION);

	  		au1.auditLogSearchByActivity(UPDTAE_ASSOCIATION);
	  		verifyAuditResult(UPDTAE_ASSOCIATION);

	  		au1.auditLogSearchByActivity(DELETE_ASSOCIATION);
	  		verifyAuditResult(DELETE_ASSOCIATION);

	  		au1.auditLogSearchByActivity(ADD_ATTACHEMENT);
	  		verifyAuditResult(ADD_ATTACHEMENT);

	  		au1.auditLogSearchByActivity(REMOVE_ATTACHEMENT);
	  		verifyAuditResult(REMOVE_ATTACHEMENT);
	  		endLine(method);
	  	}
	  	
	  	public void verifyAuditResult(String activityDesc) throws Exception {
	  		verifyText(usernameUpperCase,
	  				"table_AuditLog_AuditRecord_FirstRecord_Username");
	  		verifyTextContains(incidentIdFull,
	  				"table_AuditLog_AuditRecord_FirstRecord_Incident");
	  		verifyTextContains(activityDesc,
	  				"table_AuditLog_AuditRecord_FirstRecord_Activity");
	  	}

     
	  //Test case 30287:001a. Mirsap Action Association Tab - Subject Association - 10th point in this testcase is not automated
	  
	  @Test(priority = 3)
	  	public void mirsapAction_AssociationTabSubjectAssociation(Method method) throws Exception {
	  		startLine(method);
			
            String mirsapURN = raiseAndGetMirsapActionURN();
	  		click("tab_Association");
			verifyAssociationTabEnabled();
	  		explicitlyWaitForElement("icon_Association_Add",
	  				getTotalWaitTimeInSecs());
	  		//verify subject nature of link is present
	  		verifyElementPresent("text_Association_NatureOfLink_Subject");
	  		//verify delete icon is not present
	  		verifyElementNotPresent("icon_Action_Association_Delete");
	  		
	  		//adding one more subject association
	  		click("icon_Association_Add");
	  		verifyElementPresent("dropDown_Association_Add_Type");
	  		selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_SUBJECT);
	  		sendKeys("textField_Association_Add_TargetURN", targetSubjectURN);
	  		explicitlyWaitForElement("button_Association_Add_Create",
	  				getTotalWaitTimeInSecs());
	  		click("button_Association_Add_Create");
	  		explicitlyWaitForElement("successMsg_Action_Add_Association",
	  				getTotalWaitTimeInSecs());
	  		verifyElementPresent("successMsg_Action_Add_Association");
	  		String succMess_Subject = getText("successMsg_Action_Add_Association");
	  		String[] actUrn_Subject = succMess_Subject.split(" ");
	  		int size3 = actUrn_Subject.length;
	  		String Subject_urn = actUrn_Subject[size3-1];
	  		System.out.println(Subject_urn);
	  		verifyTextCompare(targetSubjectURN,Subject_urn);
	  		verifyElementPresent("text_Association_NatureOfLink_Subject");
	  		verifyElementNotPresent("icon_Action_Association_Delete");
	  		viewAndGetMirsapActionURN(mirsapURN);
	  		explicitlyWaitForElement("text_Action_View_SubjectDetails_URN",
	  				getTotalWaitTimeInSecs());
	  		verifyElementPresent("text_Action_View_SubjectDetails_URN");
	  		
	  		//navigate to another nominal record and add action association
	  		Nominal nominal = new Nominal(driver);
	  		nominal.createNominalAndGetURN();
	  		click("tab_Association");
			verifyAssociationTabEnabled();
	  		click("icon_Association_Add");
	  		verifyElementPresent("dropDown_Association_Add_Type");
	  		selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_SUBJECT);
	  		sendKeys("textField_Association_Add_TargetURN", mirsapURN);
	  		explicitlyWaitForElement("button_Association_Add_Create",
	  				getTotalWaitTimeInSecs());
	  		click("button_Association_Add_Create");
	  		explicitlyWaitForElement("successMsg_Action_Add_Association",
	  				getTotalWaitTimeInSecs());
	  		verifyElementPresent("successMsg_Action_Add_Association");
	  		click("checkBox_Action_Association_FirstRecord");
	  		click("icon_Association_View");
	  		
	  		explicitlyWaitForElement("tab_View_Action",
	  				getTotalWaitTimeInSecs());
	  		explicitlyWaitForElement("text_Action_View_SubjectDetails_URN",
	  				getTotalWaitTimeInSecs());
	  		verifyElementPresent("text_Action_View_SubjectDetails_URN");
	  		//register document with actionurn
	  		navigateToPage(REGISTER_DOCUMENT_URL);
			selectByVisibleText("dropDown_Documents_Register_DocumentType", "English/Welsh Statement");
			click("textField_Document_Register_DateTaken");
			sendKeysAndEnter("textField_Document_Register_DateTaken", " ");
			sendKeysAndEnter("textField_Document_Register_DateTaken", DOCUMENT_DATE);
			//click("button_Documents_Register_Date_Done");
			selectTextByIndex("dropDown_Documents_Register_ReferrenceGroup", 1);
			sendKeysAndEnter("textField_Document_Register_ActionNo",mirsapURN);
			click("text_Document_Register_ActionNo");
			explicitlyWaitForElement("radiobutton_Document_Register_ActionSubjects",
	  				getTotalWaitTimeInSecs());
			click("radiobutton_Document_Register_ActionSubjects");
			click("button_Documents_Register");
			explicitlyWaitForElement("tab_Document_Details",
	  				getTotalWaitTimeInSecs());
	  		click("tab_Document_Details");
	  		explicitlyWaitForElement("text_Document_Details_Usage_Origin",
	  				getTotalWaitTimeInSecs());
	  		
	  		verifyElementPresent("text_Document_Details_Usage_Origin");
	  		verifyElementPresent("text_Document_Details_Usage_Subject");
	  		
	  		//click("link_Document_Details_Xref_ActionURN");
	  		String currentParentWindowHandle = getCurrentWindowHandle();
	    	//click on view icon and wait for pop up window
	    	ArrayList<String> parentWinHandler1 = new ArrayList<String>();
	    	parentWinHandler1.add(currentParentWindowHandle);
	    	WebDriver childWindDriver = clickAndWaitForChildWindow(MIRSAP_ACTION_TITLE_WINDOW, parentWinHandler1, "link_Document_Details_Xref_ActionURN", 1, 2);
	    	verifyMirsapActionTabEnabled();
	    	
	    	//verify Document urn is present in document taken
	    	verifyElementPresent("text_Action_View_DocumentTaken_Document_URN");
	    	//click("tab_Association");
	    	//verifyElementPresent("text_Association_NatureOfLink_Generated");
	    	closechildWindowAndNavigateToItsParentWindowHandle(childWindDriver, currentParentWindowHandle, 2, 2);
	    	
	  		//Register one more document with same action urn but different nominal
	    	
	    	navigateToPage(REGISTER_DOCUMENT_URL);
			selectByVisibleText("dropDown_Documents_Register_DocumentType", "English/Welsh Statement");
			click("textField_Document_Register_DateTaken");
			sendKeysAndEnter("textField_Document_Register_DateTaken", " ");
			sendKeysAndEnter("textField_Document_Register_DateTaken", DOCUMENT_DATE);
			//click("button_Documents_Register_Date_Done");
			selectTextByIndex("dropDown_Documents_Register_ReferrenceGroup", 1);
			sendKeysAndEnter("textField_Document_Register_ActionNo",mirsapURN);
			//click("text_Document_Register_ActionNo");
			click("checkbox_Register_Document_NonSubjectNominal");
			sendKeys("textField_Document_Register_URN", "N8");
			click("button_Documents_Register");
			click("button_Documents_Register");
			explicitlyWaitForElement("tab_Document_Details",
	  				getTotalWaitTimeInSecs());
	  		click("tab_Document_Details");
	  		explicitlyWaitForElement("text_Document_Details_Usage_Origin",
	  				getTotalWaitTimeInSecs());
	  		verifyElementPresent("text_Document_Details_Usage_Origin");
	  		verifyElementPresent("text_Document_Details_Usage_Subject");
	  		
	  		//click("link_Document_Details_Xref_ActionURN");
	  		String currentParentWindowHandle1 = getCurrentWindowHandle();
	    	//click on view icon and wait for pop up window
	    	ArrayList<String> parentWinHandler2 = new ArrayList<String>();
	    	parentWinHandler2.add(currentParentWindowHandle1);
	    	WebDriver childWindDriver1 = clickAndWaitForChildWindow(MIRSAP_ACTION_TITLE_WINDOW, parentWinHandler2, "link_Document_Details_Xref_ActionURN", 1, 2);
	    	verifyMirsapActionTabEnabled();
	    	
	    	//verify Document urn is present in document taken
	    	verifyElementPresent("text_Action_View_DocumentTaken_Document_URN");
	    	click("button_Action_Update");
	    	click("dropDown_Action_Update_SubjectDetails");
	    	//uncheckall in default list
	    	click("link_Action_Mirsap_List_UnCheckAll");
	    	//select only first record
	    	click("checkBox_Action_View_SubjectDetails_FirstRecord");
	    	click("button_Action_Save");
	    	closechildWindowAndNavigateToItsParentWindowHandle(childWindDriver1, currentParentWindowHandle1, 2, 2);
	    	
	    	//Raise action and move it to allocated state
	    	
	    	String mirsapURNallocated=raiseAndGetMirsapActionURN();
			verifyText(FOR_ALLOCATION, "label_Action_View_State");
			//getList and verify state in dropdown
			getAllListItemAndVerify( ALLOCATED,"dropDown_Action_Select_State");
			getAllListItemAndVerify( FOR_REFERRAL,"dropDown_Action_Select_State");
			getAllListItemAndVerify( PENDED,"dropDown_Action_Select_State");
			//change state button click
			clickAndWait("button_Action_ChangeState");
			//wait for allocation officer
			explicitlyWaitForElement("button_Action_ChangeState_Save",
					getTotalWaitTimeInSecs());
			explicitlyWaitForElement("textfield_Action_MirsapAction_QueueAllocatedOfficerOne",
					getTotalWaitTimeInSecs());
			//verifyMirsapActionFieldsInAllocatedPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
			insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
			//Add First Create Officer
			clickAndWait("button_Action_MirsapAction_CreateOfficerOne");
			explicitlyWaitForElement(
					"textfield_Action_CreateOfficer_Surname",
					getTotalWaitTimeInSecs());
			sendKeys("textfield_Action_CreateOfficer_Surname",
					CREATE_OFFICER_SURNAME);
			sendKeys("textfield_Action_CreateOfficer_Forename",
					CREATE_OFFICER_FORENAME);
			click("button_Action_MirsapAction_CreateOne");
			click("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");
			insertTextInAllocatedOfficerTextField("textfield_Action_FastAction_QueueAllocatedOfficer",CREATE_OFFICER_SURNAME);
			selectOptionWithText(CREATE_OFFICER_SURNAME, "textfield_Action_StateMove_AllocatedOfficerName");
			// Perform Save on Allocated Page
			clickAndWait("button_Action_ChangeState_Save");
			explicitlyWaitForElement("successMsg_Action_Create",
					getTotalWaitTimeInSecs());
			verifyElementPresent("successMsg_Action_Create");
			verifyText(ALLOCATED, "label_Action_View_State");
			click("tab_Association");
			verifyAssociationTabEnabled();
	  		explicitlyWaitForElement("icon_Association_Add",
	  				getTotalWaitTimeInSecs());
	  		//verify subject nature of link is present
	  		verifyElementPresent("text_Association_NatureOfLink_Subject");
	  		//verify delete icon is not present
	  		verifyElementNotPresent("icon_Action_Association_Delete");
	  		
	  		//Action to Action subject type of association should not be allowed
	  		raiseAndGetMirsapActionURN();
	  		click("tab_Association");
			verifyAssociationTabEnabled();
	  		click("icon_Association_Add");
	  		verifyElementPresent("dropDown_Association_Add_Type");
	  		selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_SUBJECT);
	  		sendKeys("textField_Association_Add_TargetURN", mirsapURN);
	  		explicitlyWaitForElement("button_Association_Add_Create",
	  				getTotalWaitTimeInSecs());
	  		click("button_Association_Add_Create");
	  		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",getTotalWaitTimeInSecs());
	  		verifyElementPresent("errorMsg_Action_Association_Add_Subject");
	  		click("button_Action_Association_Add_Cancel");
	  		
	  		//Action to any entity should create S type of association
	  		click("icon_Association_Add");
	  		verifyElementPresent("dropDown_Association_Add_Type");
	  		selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_SUBJECT);
	  		sendKeys("textField_Association_Add_TargetURN", targetSubjectURN);
	  		explicitlyWaitForElement("button_Association_Add_Create",
	  				getTotalWaitTimeInSecs());
	  		click("button_Association_Add_Create");
	  		explicitlyWaitForElement("successMsg_Action_Add_Association",
	  				getTotalWaitTimeInSecs());
	  		verifyElementPresent("successMsg_Action_Add_Association");
	  		verifyElementPresent("text_Association_NatureOfLink_Subject");
	  		verifyElementNotPresent("icon_Action_Association_Delete");
	  		
	  		//action to Document should not be allowed
	  		click("icon_Association_Add");
	  		verifyElementPresent("dropDown_Association_Add_Type");
	  		selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_SUBJECT);
	  		sendKeys("textField_Association_Add_TargetURN", documentURN);
	  		explicitlyWaitForElement("button_Association_Add_Create",
	  				getTotalWaitTimeInSecs());
	  		click("button_Association_Add_Create");
	  		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",getTotalWaitTimeInSecs());
	  		verifyElementPresent("errorMsg_Action_Association_Add_Subject");
	  		click("button_Action_Association_Add_Cancel");
	  		
	  		//action to disclosure should not be allowed
	  		click("icon_Association_Add");
	  		verifyElementPresent("dropDown_Association_Add_Type");
	  		selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_SUBJECT);
	  		sendKeys("textField_Association_Add_TargetURN", disclosureURN);
	  		explicitlyWaitForElement("button_Association_Add_Create",
	  				getTotalWaitTimeInSecs());
	  		click("button_Association_Add_Create");
	  		explicitlyWaitForElement("errorMsg_Action_Association_Add_Subject",getTotalWaitTimeInSecs());
	  		verifyElementPresent("errorMsg_Action_Association_Add_Subject");
	  		click("button_Action_Association_Add_Cancel");
	  		
	  		//create multiple association or create association
	  	    //Action to Action subject type of association should not be allowed
	  		ListManagement list =new ListManagement(driver);
	  		ArrayList<String> items = new ArrayList<String>();
	  		items.add(listnomURN);
	  		items.add(listdocURN);
	  		items.add(listdisURN);
	  		items.add(listactURN);
	  		list.createDefaultList(items);
	  		navigateTo(CREATE_ASSOCIATION_URL);
			click("button_Action_Association_AddMultipleAssociation");
	  		verifyElementPresent("dropDown_Association_Add_Type");
	  		selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_SUBJECT);
	  		sendKeys("textField_Association_Add_SourceURN", mirsapURN);
	  		click("dropDown_CreateAssociation_CurrentWorkingList");
	  		click("checkbox_CreateAssociation_CurrentWorkingList_FirstRecord");
	  		click("button_CreateAssociation_AddMultiple");
            explicitlyWaitForElement("errorMsg_CreateAssociation_AddMultiple",getTotalWaitTimeInSecs());
	  		verifyElementPresent("errorMsg_CreateAssociation_AddMultiple");
	  		
	  		
	  		//Action to any entity should create S type of association
	  		
			click("button_Action_Association_AddMultipleAssociation");
	  		verifyElementPresent("dropDown_Association_Add_Type");
	  		selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_SUBJECT);
	  		sendKeys("textField_Association_Add_SourceURN", mirsapURN);
	  		click("dropDown_CreateAssociation_CurrentWorkingList");
	  		click("checkbox_CreateAssociation_CurrentWorkingList_FourthRecord");
	  		click("button_CreateAssociation_AddMultiple");
            explicitlyWaitForElement("successMsg_Action_Add_Association",
	  				getTotalWaitTimeInSecs());
	  		verifyElementPresent("successMsg_Action_Add_Association");
	  		
	  		
	  		//action to Document should not be allowed
	  		click("button_Action_Association_AddMultipleAssociation");
	  		verifyElementPresent("dropDown_Association_Add_Type");
	  		selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_SUBJECT);
	  		sendKeys("textField_Association_Add_SourceURN", mirsapURN);
	  		click("dropDown_CreateAssociation_CurrentWorkingList");
	  		click("checkbox_CreateAssociation_CurrentWorkingList_SecondRecord");
	  		click("button_CreateAssociation_AddMultiple");
            explicitlyWaitForElement("errorMsg_CreateAssociation_AddMultiple",getTotalWaitTimeInSecs());
	  		verifyElementPresent("errorMsg_CreateAssociation_AddMultiple");
	  		
	  		//action to disclosure should not be allowed
	  		click("button_Action_Association_AddMultipleAssociation");
	  		verifyElementPresent("dropDown_Association_Add_Type");
	  		selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_SUBJECT);
	  		sendKeys("textField_Association_Add_SourceURN", mirsapURN);
	  		click("dropDown_CreateAssociation_CurrentWorkingList");
	  		click("checkbox_CreateAssociation_CurrentWorkingList_ThirdRecord");
	  		click("button_CreateAssociation_AddMultiple");
            explicitlyWaitForElement("errorMsg_CreateAssociation_AddMultiple",getTotalWaitTimeInSecs());
	  		verifyElementPresent("errorMsg_CreateAssociation_AddMultiple");
	  		endLine(method);
	  
	  
	  }
         
	  @AfterTest
	  public void afterTest() {
		  driver.quit();
	  }
}



