package Modules.Actions.MisrapAction;

import java.lang.reflect.Method;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class ViewMirsapAction extends MirsapAction {
	private static final String RAISE_MIRSAP_ACTION_URL = "/group/holmes/raise-action";
	private static final String VIEWACTION_URL = "/group/holmes/view-action";
	private static final String ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE = "Action - Auto Test for Raise Action update";
	private static final String ACTION_ORIGINATING_DETAIL = "Originating Desc";
	private static String mirsapActionUrn = "";
	String MIRSAP_FORCE="ARMY";
	String MIRSAP_CLASS="SCENE";
	String MIRSAP_PRIORITY="Medium";
	String MIRSAP_STATION="HEADQUARTERS";

	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}

	//default constructor
	public ViewMirsapAction() {
		softAssert = new SoftAssert();
	}

	
	@BeforeTest
	public void beforeTest() throws Exception {
		loginInIEAndSelectIncident();
		//				loginAndSelectIncident();

	}

	public void verifyActionTabEnabled(String objname){
		verifyTabActive(objname);
	}

	/*
	 * Test case 30749: 001a. View Mirsap Action
	 */
	@Test(priority = 1)
	public void viewMirsap_Action(Method method) throws Exception {
		startLine(method);
		// navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		// insert text into mandatory fields
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		// clicking raise action button
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		// verifying successfully message
		verifyElementPresent("successMsg_Action_Create");
		// getting URN
		String createMessage = getText("successMsg_Action_Create");
		String[] actUrn = createMessage.split(" ");
		mirsapActionUrn = actUrn[1];
		System.out.println(mirsapActionUrn + "::::::");

		// navigate to View action page
		navigateTo(VIEWACTION_URL);
		// verify View Action Tab
		verifyElementPresent("tab_View_Action");
		// enter Action URN to view
		sendKeys("textField_Action_ViewUrn", mirsapActionUrn);
		// clicking view button
		click("button_Action_View");
		waitForPageLoad();
		explicitlyWaitForElement("text_Action_View_Form",
				getTotalWaitTimeInSecs());
		// verifying element present in view
		verifyElementPresent("text_Action_View_Form");
		//verify data
		verifyText(ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE,"label_Action_View_Title");
		verifyText(MIRSAP_PRIORITY, "label_Action_Mirsap_View_Priority");
		
		//verifyTabs
		verifyElementPresent("tab_action");
		verifyElementPresent("tab_Association");
		verifyElementPresent("tab_Profile");
		verifyElementPresent("tab_tags");
		verifyElementPresent("tab_Attachment");
		verifyElementPresent("tab_Access_Control");
		verifyElementPresent("tab_Marker");
		verifyElementPresent("tab_History");
		
		// clicking visualisation 
		click("icon_Action_View_Visualization");
		//verify successfull message in visualisation
		verifyText("added successfully to Visualiser", "textField_Action_View_Visualiser");
		clickAndWait("button_Action_Search_AddList_Ok");
		// Switch to  visualize window
		getHandleToWindow("Visualisation");
		//verify Action Urn is present in visualization
		verifyTextContains(mirsapActionUrn, "link_Action_Action_Visualization_Chart");
		getHandleToWindow("Visualisation").close();
		// swich back to Action window
		getHandleToWindow("Action");
		
		// verify DefaultList check
		click("icon_Action_View_DefaultList");
		explicitlyWaitForElement("text_Action_Search_Add_DefaultList",
				getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Search_AddList_Ok");
		
		//export to csv
		click("icon_Action_View_ExportCSV");
		
		// clicking view action in new window
		click("icon_Action_View_ViewInNewWindow");
		Set<String> allWindow = driver.getWindowHandles();
		String windowHandle = ((String) allWindow.toArray()[1]);
		driver.switchTo().window(windowHandle);
		explicitlyWaitForElement("text_Action_View_Form",
				getTotalWaitTimeInSecs());
		// verifying element present in view
		verifyElementPresent("text_Action_View_Form");
		driver.close();
		getHandleToWindow("Action");
		
		//verify association tab
		click("tab_Association");
		verifyAssociationTabEnabled();
		//view association
		selectByVisibleText("dropDown_Association_TargetType","Nominal");
		click("checkBox_Action_Association_FirstRecord");
		click("icon_Action_SearchResult_View");
		verifyElementPresent("textField_Indexes_Nominal_URN");
		
		endLine(method);
	}

	//@Test(priority = 2)
	public void viewAction_MouseFocus(Method method) throws Exception {
		startLine(method);
		// navigate to View action page
		navigateTo(VIEWACTION_URL);
		// verify View Action Tab
		verifyElementPresent("tab_View_Action");
		//click("textField_Action_ViewUrn");
		verifyCurrentCursorFocus("textField_Action_ViewUrn");
		/*JavascriptExecutor jse = (JavascriptExecutor) driver;
		if(jse.executeScript("document.getElementById('"+"entityUrn"+"').focus();") == null){
			System.out.println("aaa::::");
		}*/
		/*sendKeys("textField_Action_ViewUrn", mirsapActionUrn);
		// clicking view botton
		click("button_Action_View");
		waitForPageLoad();
		explicitlyWaitForElement("text_Action_View_Form",
				getTotalWaitTimeInSecs());
		// verifying element present in view
		verifyElementPresent("text_Action_View_Form");*/
		endLine(method);
	}
	
	//@Test(priority = 3)
	public void viewMirsapAction_InPopup(Method method) throws Exception {

		startLine(method);
		// navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		// Opening new window and switch to that window
		openNewWindowAndGetHandle();
		navigateTo(VIEWACTION_URL);
		// verify View Action Heading
		verifyElementPresent("tab_View_Action");
		// Enter Action URN to view
		sendKeys("textField_Action_ViewUrn", "A1");
		// clicking view botton
		click("button_Action_View");
		waitForPageLoad();
		explicitlyWaitForElement("text_Action_View_Form",
				getTotalWaitTimeInSecs());
		// verifying view Action URN element present in view
		verifyElementPresent("text_Action_View_Form");
		endLine(method);
	}	
}
