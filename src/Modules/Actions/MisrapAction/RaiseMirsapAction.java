package Modules.Actions.MisrapAction;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Modules.Actions.MisrapAction.MirsapAction;
import Modules.AuditLogs.AuditLogSearch;
import Modules.Broadcasts.Broadcast;
import Modules.CasualtyBureau.CasualtyBureau;
import Modules.Documents.Document;
import Modules.Indexes.Indexes;
import Modules.Search.ListManagement.ListManagement;
import ReusableUtilities.Dataprovider;
import ReusableUtilities.Report;

public class RaiseMirsapAction extends MirsapAction {

	// 70 characters length for originating details
	private static final String ACTION_ORIGINATING_DETAIL_MAX_CONTENT = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ012345678901234567";
	private static final String RAISE_MIRSAP_ACTION_URL = "/group/holmes/raise-action";
	private static final String ACTION_DASHBOARD_URL = "/group/holmes/search-for-actions";
	private static final String SEARCH_ACTION_URL = "/group/holmes/search-for-actions";
	private static final String ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE = "Action - Auto Test for Raise Action update";
	private static final String ACTION_ORIGINATING_DETAIL = "Originating Desc";
	private static final String ALPHA_NUMERIC_TEXT="A1a29F";
	private static final String SPECIALCHARACTER_TEXT="@%^*(";
	private static final String TEXT_UPDATED="TEXT_UPDATED";
	private static final String CREATE_OFFICER_NAME_MAX_CHAR = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final String VIEWACTION_URL = "/group/holmes/view-action";
	private static final String INDEXES_NOMINAL_URL = "/group/holmes/nominal";
	private static final String MANAGE_CONSTRAINED_LIST ="/group/holmes/manage-constrained-lists";
	private static final String LIST_MAINTENANCE_WINDOW_TITLE = "List Maintenance - Liferay";
	private static final String NEW_OR_EXISTING_LIST_DIALOG_CONTENT = "List Management has been opened with the selected records";
	private static final String DEFAULT_LIST_DIALOG_CONTENT = "The selected records have been added to your Default list";
	private static final String FAST_ACTION_WINDOW_TITLE = "Fast Action";
	private static final String VISUALISATION_PORTLET_WINDOW_TITLE = "Visualisation Portlet";
	private static final String DISCLOSURE_WINDOW_TITLE = "Disclosure";
	private static final String NOT_A_VALID_FAST_ACTION_TEXT = "not a valid Fast Action";
	private static final String IS_MARKED_AS_READ_ONLY_TEXT = "is marked as read only";
	private static final String LINKEDACTION_SPECIALCHAR = "A!!";
	private static final String BULKCREATE_SUCCESS = "50 action created successfully";
	private static final String BULKCREATE_ERROR = "Provide a valid number between 1 and 50 for bulk create";
	private static final String BULKRECORD_MISSINGVALUE = "Provide a valid number for bulk create";
	private static final String ORIGINATING_DETAILS_MAX_CHAR_SIZE = "70";
	private static final String MIRSAP_ACTION_TITLE_MAX_CHAR_SIZE = "70";
	private static final String CREATE_OFFICER_OFFICERNO_MAX_CHAR = "0123456789";
	private static final String CREATE_OFFICER_MAX_CHAR_COUNT_VALIDATION_MESS = "Surname must not be more than 30 characters\nForename must not be more than 30 characters\nOnly 7 characters are allowed for Officer Number";
	private static final String ORIGINATION_DETAILS_ERROR = "Please provide an Originating Document or Originating Details";
	private static final String NO_INPUT="Please select/input an entity";
	private static final String INVALID_URN="AAA1";
	private static final String TEXT1="XYZ";
	private static final String TEXT2="XYZABC";
	private static final String TEXT3="DEF";
	private static final String NOMINAL_DESCRIPTION="Nominal";
	private static final String VEHICLE_DESCRIPTION="Vehicle";

	String MIRSAP_FORCE="ARMY";
	String MIRSAP_CLASS="SCENE";
	String MIRSAP_PRIORITY="Medium";
	String MIRSAP_STATION="HEADQUARTERS";
	String MIRSAP_FORCE_UPDATE="ESSEX POLICE";
	String MIRSAP_PRIORITY_UPDATE="High";
	String MIRSAP_CLASS_UPDATE="VICTIM";
	String MIRSAP_STATION_UPDATE="BELCHAMP ST PAUL";
	String PROTECTIVE_MARKING="NOT PROTECTIVELY MARKED";
	String PRIORITY="Medium";
	String TestDataFN=readConfigFile("FileName");

	private String linkedActionsRecordURN;
	private String linkedDocumentsRecordURN;
	private String linkedActionsRecordURN2;
	private String linkedActionsRecordURN3;
	private String linkedDocumentsRecordURN2;
	private String linkedDocumentsRecordURN3;
	private String takenDocumentsRecordURN;
	private String actionThemeOne;
	private String actionThemeTwo;
	private String actionThemeThree;
	private String polActionURN;
	private String polURN;
	protected String subClassOne;
	protected String subClassTwo;
	private String nominalUrn,nominalUrn2,vehicleUrn;
	private String locationUrn; 
	private String locationUrn2;
	private String telephoneUrn;
	private String favDefListItem;
	private String callerURN;
	private String announcementURN;

	String bulkCreateCount="50";
	String bulkCreateCount_Limit="100";
	private String MirsapActionHugeTextURN;
	//string to hold the urn of the action created by test case 001
	private String mirsapUrn;
	private String disclosureURN;
	private String mirsapActionURN_allFieldValues;
	private String disclosureURN_allFieldValues;
	private String mirsapActionURN_ForGrids;
	private String mirsapActionURN_textAndTitle;
	private String mirsapActionURN_subjectList;

	//URN Starting Expression
	private static final String DISCLOSUREEXPRESSION="DI";
	private static final String ACTIONEXPRESSION="A";


	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}

	//default constructor
	public RaiseMirsapAction() {
		softAssert = new SoftAssert();
	}


	@BeforeTest
	public void beforeTest() throws Exception {
		loginInIEAndSelectIncident();
		//loginAndSelectIncident();

	}


	@Test
	public void testData() throws Exception{
		
		
		//Creating POL and Announcements
		Broadcast broadcast = new Broadcast(driver);
		polURN = broadcast.raisePOL_AndGetURN("TitleAction");
		announcementURN = broadcast.raiseAnnouncementAndGetURN("Title");
		
		//Getting read only action urn
		navigateTo(ACTION_DASHBOARD);
		explicitlyWaitForElement("button_Action_Search",getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Dashboard_Title","TitleAction");
		click("button_Action_Search");
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		polActionURN = getText("table_Action_SearchResults_URN");
		System.out.println(polActionURN+"fastActionPOL_ReadOnlyURN");
		
		//Creating bulk action urns and splitting 
		String actionBulk=raiseBulkAndGetMirsapActionURN("10");
		String[] actURN_Split = actionBulk.split("-");
		linkedActionsRecordURN = actURN_Split[0];
		String mirsapURNcount = linkedActionsRecordURN.substring(1);
		
		int mirsapURNcounts = Integer.parseInt(mirsapURNcount)+1;
    	linkedActionsRecordURN2="A"+String.valueOf(mirsapURNcounts);
    	
    	int mirsapURNcounts1 = Integer.parseInt(mirsapURNcount)+2;
    	linkedActionsRecordURN3="A"+String.valueOf(mirsapURNcounts1);
    	
    	
    	
    	//Creating Bulk Other documents and splits urn
    	Document doc = new Document(driver);
		String bulkDocument = doc.registerBulkOtherDocumentAndGetURNsWithSubjectReference("2");
		String[] docURN_Split = bulkDocument.split("-");
		linkedDocumentsRecordURN = docURN_Split[0];
		String docURNcount = linkedDocumentsRecordURN.substring(1);
		
		int docURNcounts = Integer.parseInt(docURNcount)+1;
		linkedDocumentsRecordURN2="D"+String.valueOf(docURNcounts);
		System.out.println(linkedDocumentsRecordURN2+":::linkedDocumentsRecordURN2");
		
		
		int docURNcounts1 = Integer.parseInt(docURNcount)+2;
		linkedDocumentsRecordURN3="D"+String.valueOf(docURNcounts1);
		
		Indexes  nominalIndex = new Indexes(driver);
		//Create bulk nominal urns 
		String  bulkNominal=nominalIndex.createBulkNominalAndGetURNs("Nominal","MALE","3");
		String[] bulkNominal_Split = bulkNominal.split("-");
		nominalUrn = bulkNominal_Split[0];
		System.out.println(nominalUrn+"  entityURN");
		String nominalURNcount = nominalUrn.substring(1);
		int nominalURNcounts = Integer.parseInt(nominalURNcount)+1;
		nominalUrn2="N"+String.valueOf(nominalURNcounts);
		System.out.println(nominalUrn2+"  nominalUrn2");
		
		int nominalURNcounts2 = Integer.parseInt(nominalURNcount)+2;
		String nominalUrn3="N"+String.valueOf(nominalURNcounts2);
		
		Indexes  index = new Indexes(driver);
		//Create bulk vehicle
		String  bulkVehicle=index.createBulkVehicleAndGetURNs("Vehicle","2");
		String[] bulkVehicle_Split = bulkVehicle.split("-");
		vehicleUrn = bulkVehicle_Split[0];
		
		// creating locations
		String  bulkLocation=index.createBulkLocationAndGetURNs("street", "2");
		String[] bulkLocation_Split = bulkLocation.split("-");
		locationUrn = bulkLocation_Split[0];
		System.out.println(locationUrn+"  locationUrn");
		String locationURNcount = locationUrn.substring(1);
		int locationURNcounts = Integer.parseInt(locationURNcount)+1;
		locationUrn2="L"+String.valueOf(locationURNcounts);
		System.out.println(locationUrn2+"  locationUrn2");
		
		//creating telephone
		String  bulkTelephone=index.createBulkTelephoneAndGetURNs("3333", "2");
		String[] bulkTelephone_Split = bulkTelephone.split("-");
		telephoneUrn = bulkLocation_Split[0];
		
		Document doc1 = new Document(driver);
		String bulkEnglishSatatement = doc1.registerBulkEnglishStatementAndGetURNsWithSubjectReference("2");
		String[] docURN1_Split = bulkEnglishSatatement.split("-");
	    String favDefListItem= docURN1_Split[0];
	    //add favorile list
		ListManagement li = new ListManagement(driver);
		ArrayList<String> as = new ArrayList<String>();
		
		as.add(favDefListItem);
		as.add(nominalUrn3);
		li.createDefaultAndFavList(as);
		//creating caller
		CasualtyBureau cb = new CasualtyBureau(driver);
		callerURN= cb.createCallerAndGetURN();
		
		//Reading themes  and subclass from config file
		actionThemeOne=readConfigFile("actionThemeOne");
		actionThemeTwo=readConfigFile("actionThemeTwo");
		actionThemeThree=readConfigFile("actionThemeThree");
		
		subClassOne=readConfigFile("subClassOne");
		subClassTwo=readConfigFile("subClassTwo");
		
		System.out.println("linkedActionsRecordURN: " + linkedActionsRecordURN);
		System.out.println("linkedDocumentsRecordURN: " + linkedDocumentsRecordURN);

		System.out.println("linkedActionsRecordURN2: " + linkedActionsRecordURN2);
		System.out.println("linkedActionsRecordURN3: " + linkedActionsRecordURN3);
		System.out.println("linkedDocumentsRecordURN2: " + linkedDocumentsRecordURN2);
		System.out.println("linkedDocumentsRecordURN3: " + linkedDocumentsRecordURN3);
		System.out.println("takenDocumentsRecordURN: " + takenDocumentsRecordURN);
		System.out.println("actionThemeOne: " + actionThemeOne);
		System.out.println("actionThemeTwo: " + actionThemeTwo);
		System.out.println("polURN: " + polURN);
		System.out.println("polActionURN: " + polActionURN);
		System.out.println("subClassOne: " + subClassOne);
		System.out.println("subClassTWO: " + subClassTwo);	
		System.out.println("nominalURN: " + nominalUrn);
		System.out.println("nominalURN2: " + nominalUrn2);
		System.out.println("vehicleURN: "+ vehicleUrn);
		System.out.println("locationUrn: "+ locationUrn);
		System.out.println("locationUrn2: "+ locationUrn2);
		System.out.println("telephoneUrn: "+ telephoneUrn);
		System.out.println("favDefListItem: " + favDefListItem);
		System.out.println("announcementURN: " + announcementURN);
		System.out.println("callerURN: " + callerURN);
	}

	//Test case 24092:001. Raise Mirsap Action with Mandatory Values
	@Test
	//(priority = 2) 
	public void createMirsapAction_WithMandatoryFields(Method method)
			throws Exception {

		startLine(method);
		//navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		verifyTextContains(incidentIdFull, "dropDown_Action_Create_Incident");
		verifyMandatoryFieldsInMirsapAction();
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		//insert text Text field
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		click("link_Action_Create_LinkedAssociation");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		//Verification of data in View Action Fields
		verifyElementPresent("successMsg_Action_Create");
		verifyText(ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE,
				"label_Action_View_Title");
		verifyText(MIRSAP_FORCE, "label_Action_Mirsap_View_Force");
		verifyText(MIRSAP_CLASS, "label_Action_Mirsap_View_Class");
		verifyText(MIRSAP_PRIORITY, "label_Action_Mirsap_View_Priority");
		verifyText(MIRSAP_STATION, "label_Action_Mirsap_View_Station");
		click("link_Action_Mirsap_View_Originating");
		verifyText(ACTION_ORIGINATING_DETAIL,
				"label_Action_Mirsap_View_OriginatingDetails");
		String createMessage = getText("successMsg_Action_Create");
		String[] actUrn = createMessage.split(" ");
		mirsapUrn = actUrn[1];
		validateMirsapActionURNPattern(mirsapUrn);
		//verifyHistoryTab
		click("tab_History");
		explicitlyWaitForElement("table_History_HistoryRecord_Username",
				getTotalWaitTimeInSecs());
		verifyText(usernameUpperCase,"table_History_HistoryRecord_Username");
		verifyTextContains(CREATE_ACTION,"table_Action_HistoryRecord_CreateAction");
		click("table_Action_HistoryRecord_CreateAction");
		viewHistoryTabSearchResult(CREATE_ACTION);
		endLine(method);
	}

	// Test case 30749:001a. View Mirsap Action
	//Defect Exists- Expected to FAIL ( HLM00008166, HLM00009422)
	@Test
	//(priority = 3, dependsOnMethods = {"createMirsapAction_WithMandatoryFields"})
	public void viewAction(Method method) throws Exception{
		startLine(method);
		//navigate to view action page
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		// verify cursor focus in URN text field
		verifyCurrentCursorFocus("textField_Action_ViewUrn");
		sendKeys("textField_Action_ViewUrn", mirsapUrn);
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		isInMirsapActionViewPage();
		//Verify currently incident id is selected
		verifyTextContains(incidentIdFull, "label_Action_View_Page_Title");

		//verifyHistoryTab
		click("tab_History");
		explicitlyWaitForElement("table_History_HistoryRecord_Username",
				getTotalWaitTimeInSecs());
		verifyText(usernameUpperCase,"table_History_HistoryRecord_Username");
		verifyTextContains(VIEW_ACTION,"table_Action_HistoryRecord_ViewAction");
		click("table_Action_HistoryRecord_ViewAction");
		viewHistoryTabSearchResult(VIEW_ACTION); 
		endLine(method);
	} 


	// Test case 29247: 001b. Update Action With Mandatory Values
	// Other Actions need to be present in the incident
	// Other Documents need to be present in the incident
	@Test
	//(priority = 4, dependsOnMethods = {"createMirsapAction_WithMandatoryFields"})
	public void updateMirsapActionWithMandatoryValues(Method method) throws Exception{
		
		startLine(method);
		//navigate to view action page
		navigateTo(ACTION_DASHBOARD_URL);
		sendKeys("textField_Action_Dashboard_URN", mirsapUrn);
		click("button_Action_Search");
		//wait for search results page
		explicitlyWaitForElement("checkBox_Action_SearchResulttab_Firstrow",getTotalWaitTimeInSecs());
		click("checkBox_Action_SearchResulttab_Firstrow");
		//click("icon_Action_SearchResult_View");
		String currentParentWindowHandle = getCurrentWindowHandle();
		// get Handle to Create Fast Action- child Window
		ArrayList<String> parentWinHandler1 = new ArrayList<String>();
		parentWinHandler1.add(currentParentWindowHandle);
		int winCount = getWindowCount();
		WebDriver childWindDriver = clickAndWaitForChildWindow("Action", parentWinHandler1, "icon_Action_Searchresult_View", 1, 2);
		explicitlyWaitForElement("text_Action_View_Form",getTotalWaitTimeInSecs());
		// verifying element present in view
		verifyElementPresent("text_Action_View_Form");
		//wait for page to load with the update button
		explicitlyWaitForElement("button_Action_Update",getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		//verifyTextFields
		verifyText(ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE,"textField_Action_Title");
		verifyCurrentSelectByText("dropDown_Action_Mirsap_Priority",MIRSAP_PRIORITY);
		verifyCurrentSelectByText("dropDown_Action_Mirsap_Force",MIRSAP_FORCE);
		verifyCurrentSelectByText("dropDown_Action_Mirsap_Station",MIRSAP_STATION);
		verifyCurrentSelectByText("dropDown_Action_Mirsap_Class",MIRSAP_CLASS);
		verifyText(	ACTION_ORIGINATING_DETAIL, "textField_Action_OriginatingDetails");
		sendKeys("textField_Action_Title","Automation Testing - Update Action");
		click("button_Action_Save");
		explicitlyWaitForElement("successMsg_Action_Create",getTotalWaitTimeInSecs());
		driver = closePopupWindowAndNavigateToParentWindowHandle("Action", currentParentWindowHandle);
		endLine(method);
	}


	//Test case 24552:002. Raise Mirsap Action without Mandatory Values
	@Test
	//(priority = 5)
	public void createMirsapAction_WithoutMandatoryDetails(Method method)
			throws Exception {
		startLine(method);
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		verifyMirsapActionTabEnabled();
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("errorMsg_Action_Create_WithOutAnyFields",
				getTotalWaitTimeInSecs());
		verifyText("There are validation errors","errorMsg_Action_Create_WithOutAnyFields");
		verifyText("Title is required","errorMsg_Action_Title");
		verifyText("Force is required","errorMsg_Action_Force");
		verifyText("Station is required","errorMsg_Action_Station");
		verifyText("Class is required","errorMsg_Action_Class");
		verifyText("Subject(s) is required","errorMsg_Action_Subject");

		endLine(method);
	}

	/* Test case 29248:0021. Update Mirsap Action without Mandatory Values  priority = 4,*/
	@Test
	//(priority = 4,
	// dependsOnMethods = {"createMirsapAction_WithoutMandatoryDetails"})
	public void updateMirsapActionWithOutMandatoryFields(Method method) throws Exception{
		startLine(method);
		//navigate to view action page
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		sendKeys("textField_Action_ViewUrn", mirsapUrn);
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		isInMirsapActionViewPage();
		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		//verify is in Edit Fast action page
		isInMirsapActionEditPage();

		//Pass empty text data only to mandatory fields
		sendKeys("textField_Action_Title", "");
		selectByVisibleText("dropDown_Action_Mirsap_Force", "");
		selectByVisibleText("dropDown_Action_Mirsap_Priority", "");
		selectByVisibleText("dropDown_Action_Mirsap_Station", "");
		selectByVisibleText("dropDown_Action_Dashboard_Class", "");
		sendKeys("textField_Action_OriginatingDetails","");
		click("button_Action_subject");
		click("link_Action_Mirsap_List_UnCheckAll");

		sendKeys("textField_Action_OriginatingDetails","Updating the action record");

		//click Save button
		clickAndWait("button_Action_Save");

		verifyText("There are validation errors","errorMsg_Action_Create_WithOutAnyFields");
		verifyText("Title is required","errorMsg_Action_Title");
		verifyText("Force is required","errorMsg_Action_Force");
		verifyText("Station is required","errorMsg_Action_Station");
		verifyText("Class is required","errorMsg_Action_Class");
		endLine(method);
	}

	//Test case 24090:003. Raise Mirsap Action with all field values - Single Action Record
	@Test
	//(priority = 7)
	public void createMirsapAction_WithAllFields(Method method) throws Exception {
		startLine(method);
		//navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		verifyMirsapActionTabEnabled();
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		// verify Mandatory fields
		verifyMandatoryFieldsInMirsapAction();
		//insert text into mandatory fields
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		selectByVisibleText("dropDown_Action_Dashboard_Class", MIRSAP_CLASS);
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Dashboard_subClass1", subClassOne);
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Dashboard_subClass2", subClassTwo);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		// Click("textField_Action_Mirsap_SourceDocument");
		// SendKeys("textField_Action_Mirsap_Urn", ACTION_DOCUMENT_URN);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		//Adding Linked Action URN
		click("link_Action_Create_LinkedAssociation");
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN",linkedActionsRecordURN);
		//click("dropDown_Action_Mirsap_SelectRecord_ReferenceIncidentIcon");
		addMirsapActionLinkedActionFrmSelectRecord();

		//Adding Linked Document URN
		explicitlyWaitForElement("button_Action_Create_LinkedAssoc_Documents", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN);
		String xpathForCurrentIncident2 = "(//div[@id='" + incidentIdFull + "'])[2]";
		addMirsapActionAssociatedDocumentFrmSelectRecord(xpathForCurrentIncident2);

		//link_Action_Create_Theme
		click("link_Action_Create_Theme");
		click("checkBox_Action_Themes");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		// Verification of View Action Page
		verifyElementPresent("successMsg_Action_Create");
		isInMirsapActionViewPage();
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		mirsapActionURN_allFieldValues = actUrn[1];
		Report.info("mirsapActionURN_allFieldValues: " + mirsapActionURN_allFieldValues );

		verifyText(ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE,
				"label_Action_View_Title");
		verifyText(MIRSAP_FORCE, "label_Action_Mirsap_View_Force");
		verifyText(MIRSAP_CLASS, "label_Action_Mirsap_View_Class");
		verifyText(MIRSAP_PRIORITY, "label_Action_Mirsap_View_Priority");
		verifyText(MIRSAP_STATION, "label_Action_Mirsap_View_Station");
		click("link_Action_Mirsap_View_Originating");
		verifyText(ACTION_ORIGINATING_DETAIL,
				"label_Action_Mirsap_View_OriginatingDetails");

		// verify disclosure record in disclosure section in view page
		expandDisclosureSection();
		verifyElementPresent("table_Action_View_Diclosure_URN");
		disclosureURN_allFieldValues = getText("table_Action_View_Diclosure_URN");
		Report.info("disclosureURN_allFieldValues: " + disclosureURN_allFieldValues);

		click("button_Action_Association_Tab");
		verifyAssociationTabEnabled();
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getTotalWaitTimeInSecs());
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnterWithDriver("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + linkedActionsRecordURN);
		verifyElementPresent("checkBox_Action_Association_FirstRecord"); 
		endLine(method);
	}


	//Test case 26771:003A. Raise Mirsap Action with all similar field values - Single Action Record
	@Test
	//(priority = 8)
	public void CreateMirsapAction_WithAllSimilarFieldValues(Method method)
			throws Exception {
		startLine(method);
		//navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		verifyMirsapActionTabEnabled();
		explicitlyWaitForElement("button_Action_Raise",getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		// verify Mandatory fields
		verifyMandatoryFieldsInMirsapAction();
		//insert text into mandatory fields
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		// Click("textField_Action_Mirsap_SourceDocument");
		// SendKeys("textField_Action_Mirsap_Urn", ACTION_DOCUMENT_URN);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		//Adding Linked Action URN
		click("link_Action_Create_LinkedAssociation");
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN",linkedActionsRecordURN);
		//click("dropDown_Action_Mirsap_SelectRecord_ReferenceIncidentIcon");
		addMirsapActionLinkedActionFrmSelectRecord();

		//Adding Linked Document URN
		explicitlyWaitForElement("button_Action_Create_LinkedAssoc_Documents", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN);
		String xpathForCurrentIncident2 = "(//div[@id='" + incidentIdFull + "'])[2]";
		addMirsapActionAssociatedDocumentFrmSelectRecord(xpathForCurrentIncident2);

		//link_Action_Create_Theme
		click("link_Action_Create_Theme");
		click("checkBox_Action_Themes");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",getTotalWaitTimeInSecs());
		// Verification of View Action Page
		verifyElementPresent("successMsg_Action_Create");
		verifyText(ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE,
				"label_Action_View_Title");
		verifyText(MIRSAP_FORCE, "label_Action_Mirsap_View_Force");
		verifyText(MIRSAP_CLASS, "label_Action_Mirsap_View_Class");
		verifyText(MIRSAP_PRIORITY, "label_Action_Mirsap_View_Priority");
		verifyText(MIRSAP_STATION, "label_Action_Mirsap_View_Station");
		click("link_Action_Mirsap_View_Originating");
		verifyText(ACTION_ORIGINATING_DETAIL,
				"label_Action_Mirsap_View_OriginatingDetails");
		click("button_Action_Association_Tab");
		verifyAssociationTabEnabled();
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getTotalWaitTimeInSecs());
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + linkedActionsRecordURN);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		validateAllVerificationPoints(); 
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn", " ");
		//Verify Disclosure record
		selectByVisibleText("dropDown_Association_TargetType", "Disclosure");
		waitForJQuery(driver);
		explicitlyWaitForElement("table_AssociationTab_FirstRow_Disclosure",
				getTotalWaitTimeInSecs());
		verifyElementPresent("table_AssociationTab_FirstRow_Disclosure");
		//String disclosureRecord_URN = getText("table_AssociationTab_FirstRow_Disclosure");
		// Disclosure verify in first record of bulk create
		//System.out.println(disclosureRecord_URN);

		// second Action create with same data
		//navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		verifyMirsapActionTabEnabled();
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		// verify Mandatory fields
		verifyMandatoryFieldsInMirsapAction();
		//insert text into mandatory fields
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		// Click("textField_Action_Mirsap_SourceDocument");
		// SendKeys("textField_Action_Mirsap_Urn", ACTION_DOCUMENT_URN);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		//Adding Linked Action URN
		click("link_Action_Create_LinkedAssociation");
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN",linkedActionsRecordURN);
		//click("dropDown_Action_Mirsap_SelectRecord_ReferenceIncidentIcon");
		addMirsapActionLinkedActionFrmSelectRecord();

		//Adding Linked Document URN
		explicitlyWaitForElement("button_Action_Create_LinkedAssoc_Documents", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN);
		String xpathForCurrentIncident3 = "(//div[@id='" + incidentIdFull + "'])[2]";
		addMirsapActionAssociatedDocumentFrmSelectRecord(xpathForCurrentIncident3);

		//link_Action_Create_Theme
		click("link_Action_Create_Theme");
		click("checkBox_Action_Themes");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		// Verification of View Action Page
		verifyElementPresent("successMsg_Action_Create");
		verifyText(ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE,
				"label_Action_View_Title");
		verifyText(MIRSAP_FORCE, "label_Action_Mirsap_View_Force");
		verifyText(MIRSAP_CLASS, "label_Action_Mirsap_View_Class");
		verifyText(MIRSAP_PRIORITY, "label_Action_Mirsap_View_Priority");
		verifyText(MIRSAP_STATION, "label_Action_Mirsap_View_Station");
		click("link_Action_Mirsap_View_Originating");
		verifyText(ACTION_ORIGINATING_DETAIL,
				"label_Action_Mirsap_View_OriginatingDetails");
		click("button_Action_Association_Tab");
		verifyAssociationTabEnabled();
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getTotalWaitTimeInSecs());
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + linkedActionsRecordURN);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn"," ");
		validateAllVerificationPoints(); 
		//disclosure check
		verifyElementPresent("table_Action_View_Diclosure_URN");
		endLine(method);


	}


	//Test case 29251:003b. Update Action with all field values - Single Action Record
	@Test
	//(priority = 36)//,
	//dependsOnMethods = {"createMirsapAction_WithallFields"})
	public void updateMirsapAction_WithAllFields(Method method)
			throws Exception {

		startLine(method);
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();

		sendKeys("textField_Action_ViewUrn", mirsapActionURN_allFieldValues);
		//View mirsap action

		clickAndWaitForPageLoad("button_Action_View");
		waitForJQuery(driver);
		// verify is in Mirsap action View page
		isInMirsapActionViewPage();
		explicitlyWaitForElement("button_Action_Update", getTotalWaitTimeInSecs());

		click("button_Action_Update");
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());

		sendKeys("textField_Action_Title",TEXT_UPDATED);
		insertIntoAllTinyMCEinPage(TEXT_UPDATED);
		sendKeys("textField_Action_OriginatingDetails",TEXT_UPDATED);
		sendKeys("textField_Action_MirsapAction_Edit_ReceiversInstructions",TEXT_UPDATED);

		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE_UPDATE);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY_UPDATE);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION_UPDATE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS_UPDATE);

		// change PM
		selectByVisibleText("dropDown_Action_Create_ProtectiveMarking",
				"SECRET");
		// change priority
		selectByVisibleText("dropDown_Action_Mirsap_Priority", "High");

		//link_Action_Create_Theme
		click("link_Action_Create_Theme");
		click("checkBox_Action_Themes_Item2");

		//click on add linked action Section
		click("link_Action_Create_LinkedAssociation");

		//Adding Linked Action URN
		click("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Update_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_LinkedAssoc_Action_URN",linkedActionsRecordURN3);
		//		String actualCurrentIncidentVal = getWebElement("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1").getText();
		verifyTextContains(incidentIdFull, "dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1");
		click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1");
		pressReturnKey("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1");
		clickAndWait("button_Action_Create_LinkedAssoc_Action_Update");
		explicitlyWaitForElement("table_Action_Update_LinkedAction_SecondRecordURN", getTotalWaitTimeInSecs());
		verifyText(linkedActionsRecordURN3, "table_Action_Update_LinkedAction_SecondRecordURN");


		//Adding Associated Document URN
		click("button_Action_Create_LinkedAssoc_Documents");
		// Verify Dialog box title is poped up
		verifyText(ACTION_ASSOCIATEDOCUMENT_DIALOGBOXTITLE, "text_Action_AssociateDocument_EditDialogboxTitle");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN2);
		//		String actualCurrentIncidentVal = getWebElement("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1").getText();
		verifyTextContains(incidentIdFull, "dropDown_Action_Fast_SelectRecord_ReferenceIncident_Test");
		click("dropDown_Action_Fast_SelectRecord_ReferenceIncident_Test");
		pressReturnKey("dropDown_Action_Fast_SelectRecord_ReferenceIncident_Test");
		clickAndWait("button_Action_Create_LinkedAssoc_Documents_Add");
		explicitlyWaitForElement("table_Action_Create_AssociatedDocument_Edit_SecondRecordURN", getTotalWaitTimeInSecs());
		verifyText(linkedDocumentsRecordURN2, "table_Action_Create_AssociatedDocument_Edit_SecondRecordURN");

		//Adding Documents Taken URN
		click("icon_Action_Edit_DocumentTaken_Add");
		// Verify Dialog box title is poped up
		verifyText(ACTION_DOCUMENTTAKEN_DIALOGBOXTITLE, "text_Action_DocumentTaken_EditDialogboxTitle");
		explicitlyWaitForElement("textField_Action_Update_DocumentTaken_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",linkedDocumentsRecordURN2);
		//				String actualCurrentIncidentVal = getWebElement("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1").getText();
		//addMirsapActionDocumentTakenFrmSelectRecordInEditPage("//div[contains(@id,'"+incidentIdFull+"')]");
		click("dropDown_Action_Mirsap_EditRecord_ReferenceIncidentIcon1");
		pressReturnKey("dropDown_Action_Mirsap_EditRecord_ReferenceIncidentIcon1");
		clickAndWait("button_Action_Update_LinkedAssoc_Actions_Add");
		explicitlyWaitForElement("table_Action_Create_DocumentTaken_Edit_FirstRecordURN", getTotalWaitTimeInSecs());
		verifyText(linkedDocumentsRecordURN2, "table_Action_Create_DocumentTaken_Edit_FirstRecordURN");

		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		//click update button
		click("button_Action_Save");
		explicitlyWaitForElement("successMsg_Action_Create",getTotalWaitTimeInSecs());
		isInMirsapActionViewPage();
		//verifyTextFields
		verifyText(TEXT_UPDATED,"label_Action_View_Title");
		verifyText(MIRSAP_FORCE_UPDATE, "label_Action_Mirsap_View_Force");
		verifyText(MIRSAP_CLASS_UPDATE, "label_Action_Mirsap_View_Class");
		verifyText(MIRSAP_PRIORITY_UPDATE, "label_Action_Mirsap_View_Priority");
		verifyText(MIRSAP_STATION_UPDATE, "label_Action_Mirsap_View_Station");
		click("link_Action_Mirsap_View_Originating");
		verifyText(TEXT_UPDATED, "label_Action_Mirsap_View_OriginatingDetails");
		click("link_Action_Create_LinkedAssociation");
		verifyText(linkedActionsRecordURN3, "table_Action_View_LinkedActions_SecondRecordURN");
		verifyText(linkedDocumentsRecordURN2, "table_Action_View_AssociatedDocument_SecondRecordURN");
		verifyText(linkedDocumentsRecordURN2, "table_Action_View_DocumentTaken_FirstRecordURN");
		endLine(method);		
	}



	/* Test case 30782:003c.View Action- Pop Up */
	@Test
	//(priority = 10)
	//Defect Exists- Expected to FAIL ( HLM00008166, HLM00009422)
	//dependsOnMethods = {"createMirsapAction_WithAllFields"})
	public void viewMirsapAction_InPopup(Method method) throws Exception {

		startLine(method);
		WebDriver driverTemp = driver;
		/*mirsapUrn = "A31";
		disclosureURN = "DI43";*/
		String addToVisualizationDialogContent = "Selected record(s) added successfully to Visualiser\n" + incidentIdFull + "-" + mirsapUrn;
		// navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		String parentWindowHandle = getCurrentWindowHandle();
		Report.info("Parent window handle info: " + parentWindowHandle);
		// Opening new window and switch to that window
		WebDriver childViewActionWebDriver = openNewWindowAndGetHandleWithChildWebDriver(driver);
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		String childViewActionWindowHandle = getCurrentWindowHandle();
		Report.info("child View Action popup window handle info: " + childViewActionWindowHandle);
		// verify cursor focus in URN text field
		verifyCurrentCursorFocus("textField_Action_ViewUrn");
		/*			fastActionURN = "A189";
			disclosureURN = "DI583";*/
		if(mirsapActionURN_allFieldValues == null ){
			mirsapActionURN_allFieldValues = mirsapUrn;
			disclosureURN_allFieldValues = disclosureURN;
		}
		sendKeys("textField_Action_ViewUrn", mirsapActionURN_allFieldValues);
		//			sendKeys("textField_Action_ViewUrn", fastActionURN_allFieldValues);
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		isInMirsapActionViewPage();
		//Verify currently incident id is selected
		verifyTextContains(incidentIdFull, "label_Action_View_Page_Title");
		//  View Mirsap Action " Incident -URN"  in Incident -URN Title" 
		verifyTextContains(incidentIdFull + "-" + mirsapUrn, "label_Action_View_Page_Title");
		// Verify Title and Priority field value matches user input.
		// Validate inserted title in create page same as in view page
		verifyText( ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE ,
				"label_Action_View_Title");
		// Validate  Priority : Low is set as default value but during Raise Action Priority provided is Medium
		verifyText(MIRSAP_PRIORITY, "label_Action_Mirsap_View_Priority");
		// Check All Tab alignment's is NOT done as part of validation
		String childWindowHandle1 = getCurrentWindowHandle();
		Report.info("Child window handle before invoking another child: " + childWindowHandle1);

		// View Action- Add to Visualisation icon
		/*clickOnAddToVisualizationIcon("icon_Action_View_Visualization");*/
		// Mirsap Action record is added to Favourite list and valid message is
		// displayed
		clickOnAddToFavListIcon("icon_Action_View_FavList");
		// Mirsap Action record is added to default list and valid message is
		// displayed
		clickOnAddToDefaultListIcon("icon_Action_View_DefaultList");
		// Mirsap Action record is added to new or existing list and valid message
		// is displayed
		addToNewOrExistingListIcon("icon_Action_View_NewOrExistingList");
		// TO EXPORT TO CSV IS NOT AUTOMATED

		// View in New window
		clickOnViewInNewWindowIcon("icon_Action_View_ViewInNewWindow", FAST_ACTION_WINDOW_TITLE);

		// view the Action / Action record in association tab of other
		// record - By Double Click

		// view the Action /  Action  record in association tab of other record - By Double Click 
		waitForPageLoad();
		isInMirsapActionViewPage();
		explicitlyWaitForElement("button_Action_Update",
				getTotalWaitTimeInSecs());

		//Commenting History Tab as this will fail further execution flow
		/*//verifyHistoryTab
		click("tab_History");
		explicitlyWaitForElement("table_History_HistoryRecord_Username",
				getTotalWaitTimeInSecs());
		verifyText(currentUserName,"table_History_HistoryRecord_Username");
		verifyTextContains(VIEW_ACTION,"table_Action_FastAction_HistoryRecord_ViewFastAction");
		click("table_Action_FastAction_HistoryRecord_ViewFastAction");
		viewHistoryTabSearchResult(VIEW_ACTION);
		 */
		// verify Association Tab Records
		explicitlyWaitForElement("tab_Association",
				getPoolWaitTime());
		clickAndWait("tab_Association");
		waitForPageLoad();
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getTotalWaitTimeInSecs());
		verifyAssociationTabEnabled();

		waitForJQuery(childViewActionWebDriver);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + disclosureURN_allFieldValues);
		waitForJQuery(childViewActionWebDriver);
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getPoolWaitTime());
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		click("checkBox_Action_Association_FirstRecord");
		explicitlyWaitForElement("icon_Action_Association_View", getTotalWaitTimeInSecs());
		WebElement associationViewIcon = explicitlyWaitAndGetWebElement("icon_Action_Association_View", getTotalWaitTimeInSecs());
		clickElementUsingJS(associationViewIcon);
		verifyText(disclosureURN_allFieldValues, "label_Disclosure_View_URN");
		explicitlyWaitForElement("tab_Association",
				getTotalWaitTimeInSecs());
		clickAndWait("tab_Association");
		verifyAssociationTabEnabled();
		waitForJQuery(childViewActionWebDriver);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + mirsapActionURN_allFieldValues);
		waitForJQuery(childViewActionWebDriver);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		explicitlyWaitForElement("table_Action_AssociationTab_FirstRow",
				getTotalWaitTimeInSecs());
		doubleClickAndWait("table_Action_AssociationTab_FirstRow");

		// view the Action /  Action  record in association tab of other record - View the selected Entity
		waitForPageLoad();
		isInMirsapActionViewPage();
		explicitlyWaitForElement("button_Action_Update",
				getTotalWaitTimeInSecs());
		explicitlyWaitForElement("tab_Association",
				getPoolWaitTime());
		clickAndWait("tab_Association");
		waitForPageLoad();
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord", getPoolWaitTime());
		verifyAssociationTabEnabled();
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + disclosureURN_allFieldValues);
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord", getPoolWaitTime());
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		click("checkBox_Action_Association_FirstRecord");
		click("icon_Action_Association_View");
		verifyText(disclosureURN_allFieldValues,"label_Disclosure_View_URN");
		explicitlyWaitForElement("button_Action_Association_Tab", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Association_Tab");
		verifyAssociationTabEnabled();
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + mirsapActionURN_allFieldValues);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		explicitlyWaitForElement("table_Action_AssociationTab_FirstRow", getTotalWaitTimeInSecs());
		doubleClickAndWait("table_Action_AssociationTab_FirstRow");	

		// view the Action /  Action  record in association tab of other record - View the selected entity in a new Window
		isInMirsapActionViewPage();
		explicitlyWaitForElement("button_Action_Update",
				getTotalWaitTimeInSecs());
		// verify Association Tab Records
		explicitlyWaitForElement("tab_Association",
				getPoolWaitTime());
		clickAndWait("tab_Association");
		waitForPageLoad();
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getTotalWaitTimeInSecs());
		verifyAssociationTabEnabled();

		waitForJQuery(childViewActionWebDriver);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + disclosureURN_allFieldValues);
		waitForJQuery(childViewActionWebDriver);
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getPoolWaitTime());
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		checkCheckBox("checkBox_Action_Association_FirstRecord");
		explicitlyWaitForElement("icon_Action_Association_View", getTotalWaitTimeInSecs());
		click("icon_Action_Association_View");
		verifyText(disclosureURN_allFieldValues, "label_Disclosure_View_URN");
		explicitlyWaitForElement("tab_Association",
				getTotalWaitTimeInSecs());
		clickAndWait("tab_Association");
		verifyAssociationTabEnabled();
		waitForJQuery(childViewActionWebDriver);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + mirsapActionURN_allFieldValues);
		waitForJQuery(childViewActionWebDriver);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		explicitlyWaitForElement("table_Action_AssociationTab_FirstRow",
				getTotalWaitTimeInSecs());
		doubleClickAndWait("table_Action_AssociationTab_FirstRow");

		// view the Action /  Action record in association tab of other
		// record - View the selected entity in a new Window
		isInMirsapActionViewPage();
		explicitlyWaitForElement("button_Action_Update",
				getTotalWaitTimeInSecs());
		// verify Association Tab Records
		explicitlyWaitForElement("tab_Association",
				getTotalWaitTimeInSecs());
		clickAndWait("tab_Association");
		waitForPageLoad();
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getTotalWaitTimeInSecs());
		verifyAssociationTabEnabled();
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + disclosureURN_allFieldValues);
		waitForJQuery(childViewActionWebDriver);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord", getTotalWaitTimeInSecs());
		click("checkBox_Action_Association_FirstRecord");
		childWindowHandle1 = getCurrentWindowHandle();

		Set<String> winHandList = driver.getWindowHandles();
		int windowCount = getWindowCount();
		WebDriver disclosureWinDriver = clickAndWaitForChildWindow(
				DISCLOSURE_WINDOW_TITLE,winHandList,
				"icon_Action_Association_ViewInNewWindow", windowCount, windowCount+1);
		String childWindowHandle3 = getCurrentWindowHandle();
		explicitlyWaitForElement("label_Disclosure_View_URN", getTotalWaitTimeInSecs());
		verifyText(disclosureURN_allFieldValues, "label_Disclosure_View_URN");
		explicitlyWaitForElement("tab_Association",
				getTotalWaitTimeInSecs());
		clickAndWait("tab_Association");
		verifyAssociationTabEnabled();
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + mirsapActionURN_allFieldValues);
		waitForJQuery(disclosureWinDriver);
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord", getTotalWaitTimeInSecs());
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		explicitlyWaitForElement("table_Action_AssociationTab_FirstRow", getTotalWaitTimeInSecs());
		doubleClickAndWait("table_Action_AssociationTab_FirstRow");

		waitForPageLoad();
		/*waitForJQuery(disclosureWinDriver);*/

		//close current window
		disclosureWinDriver.close();
		driver.switchTo().window(childWindowHandle1);

		/*	WebDriver childWindowHandleAfterDisclosureNewWindow = closechildWindowAndNavigateToItsParentWindowHandle(
				disclosureWinDriver, childWindowHandle1, 3, 2);

		Report.info("Child window handle after done closing view in New Window: "
				+ childWindowHandleAfterDisclosureNewWindow.getWindowHandle()
				+ " Expected is: " + childWindowHandle1);*/

		closechildWindowAndNavigateToItsParentWindowHandle(childViewActionWebDriver, parentWindowHandle, 2, 1);

		driver = driverTemp;
		endLine(method);
	}	

	/*
	 *  Test case 24091:004. Raise Mirsap Action with all field values - Bulk Create
	 */
	@Test
	//(priority = 11)
	public void createMirsapAction_WithBulkCreateValidLimit(Method method)
			throws Exception {
		startLine(method);
		// navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		verifyMirsapActionTabEnabled();
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		// verify Mandatory fields
		verifyMandatoryFieldsInMirsapAction();
		// insert text into mandatory fields
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		selectByVisibleText("dropDown_Action_Template_Class", MIRSAP_CLASS);
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Dashboard_subClass1", subClassOne);
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Dashboard_subClass2", subClassTwo);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		// Adding Linked Action URN
		click("link_Action_Create_LinkedAssociation");
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN",linkedActionsRecordURN);
		//click("dropDown_Action_Mirsap_SelectRecord_ReferenceIncidentIcon");
		addMirsapActionLinkedActionFrmSelectRecord();

		//Adding Linked Document URN
		explicitlyWaitForElement("button_Action_Create_LinkedAssoc_Documents", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN);
		String xpathForCurrentIncident2 = "(//div[@id='" + incidentIdFull + "'])[2]";
		addMirsapActionAssociatedDocumentFrmSelectRecord(xpathForCurrentIncident2);

		// link_Action_Create_Theme
		click("link_Action_Create_Theme");
		click("checkBox_Action_Themes");
		// Bulk create Entering 50 count
		click("checkBox_Action_BulkRecord");
		sendKeys("textField_Action_BulkRecordCount", bulkCreateCount);
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		// verifying bulk create successfully message
		verifyTextContains(BULKCREATE_SUCCESS,
				"successMsg_Action_Create");
		verifyElementPresent("successMsg_Action_Create");
		verifyText(ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE,
				"label_Action_View_Title");
		verifyText(MIRSAP_FORCE, "label_Action_Mirsap_View_Force");
		verifyText(MIRSAP_CLASS, "label_Action_Mirsap_View_Class");
		verifyText(MIRSAP_PRIORITY, "label_Action_Mirsap_View_Priority");
		verifyText(MIRSAP_STATION, "label_Action_Mirsap_View_Station");
		click("link_Action_Mirsap_View_Originating");
		verifyText(ACTION_ORIGINATING_DETAIL,
				"label_Action_Mirsap_View_OriginatingDetails");
		validateAllVerificationPoints();
		click("link_Action_View_Disclosure");
		// Disclosure verify in Last record of bulk create
		verifyElementPresent("table_Action_View_Diclosure_URN");
		String disclosureRecord_URN = getText("table_Action_View_Diclosure_URN");
		// Disclosure verify in first record of bulk create
		System.out.println(disclosureRecord_URN);
		String disclosureRecordCount = disclosureRecord_URN.substring(2);
		System.out.println(disclosureRecordCount);
		int x = Integer.parseInt(disclosureRecordCount);
		x = x - 49;
		String firstBulkDisclosreURN = DISCLOSUREEXPRESSION + x;
		// getting URN from view
		String action_URN = getText("label_Action_View_URN");
		String actionCount = action_URN.substring(1);
		System.out.println(actionCount);
		int y = Integer.parseInt(actionCount);
		y = y - 49;
		String firtBulkURN = ACTIONEXPRESSION + y;
		System.out.println(firtBulkURN);
		navigateTo(VIEWACTION_URL);
		sendKeys("textField_Action_ViewUrn", firtBulkURN);
		click("button_Action_View");
		explicitlyWaitForElement("button_Action_Similar",
				getTotalWaitTimeInSecs());
		click("link_Action_View_Disclosure");
		verifyText(firstBulkDisclosreURN, "table_Action_View_Diclosure_URN");
		endLine(method);
	}

	/*
	 * 004a. 29395 Raise Mirsap Action with - Bulk Create - Invalid Limit
	 */
	@Test
	//(priority = 12)
	public void createMirsapAction_WithBulkCreateInvalidLimit(Method method)throws Exception {
		startLine(method);
		// navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		verifyMirsapActionTabEnabled();
		explicitlyWaitForElement("button_Action_Raise",getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		// verify Mandatory fields
		verifyMandatoryFieldsInMirsapAction();
		// insert text into mandatory fields
		sendKeys("textField_Action_Title",ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		insertIntoAllTinyMCEinPage("test");
		// Enter Bulk create 100 count
		click("checkBox_Action_BulkRecord");
		sendKeys("textField_Action_BulkRecordCount", bulkCreateCount_Limit);
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		selectByVisibleText("dropDown_Action_Template_Class", MIRSAP_CLASS);
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Dashboard_subClass1", subClassOne);
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Dashboard_subClass2", subClassTwo);
		sendKeys("textField_Action_OriginatingDetails",ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		// Adding Linked Action URN
		click("link_Action_Create_LinkedAssociation");
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN",linkedActionsRecordURN);
		//click("dropDown_Action_Mirsap_SelectRecord_ReferenceIncidentIcon");
		addMirsapActionLinkedActionFrmSelectRecord();

		//Adding Linked Document URN
		explicitlyWaitForElement("button_Action_Create_LinkedAssoc_Documents", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN);
		String xpathForCurrentIncident2 = "(//div[@id='" + incidentIdFull + "'])[2]";
		addMirsapActionAssociatedDocumentFrmSelectRecord(xpathForCurrentIncident2);

		// link_Action_Create_Theme
		click("link_Action_Create_Theme");
		click("checkBox_Action_Themes");
		clickAndWait("button_Action_Raise");
		// Verify error when exceeding bulk create count
		verifyText(BULKCREATE_ERROR,"errorMsg_Action_BulkCreate_Count");
		endLine(method);
	}


	//Test case 23976:004b: Raise Action with missing value on Number of Bulk Records
	@Test
	//(priority = 13)
	public void createMirsapAction_MissingValueOnBulkRecord(Method method)
			throws Exception {
		startLine(method);
		// navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		verifyMirsapActionTabEnabled();
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		// verify Mandatory fields
		verifyMandatoryFieldsInMirsapAction();
		// insert text into mandatory fields
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		insertIntoAllTinyMCEinPage("test");
		// Enter Bulk null
		click("checkBox_Action_BulkRecord");
		sendKeys("textField_Action_BulkRecordCount", "");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");
		clickAndWait("button_Action_Raise");
		verifyText(BULKRECORD_MISSINGVALUE,"errorMsg_Action_BulkCreate_Null");
		verifyElementPresent("button_Action_Raise");
		endLine(method);
	}


	//Test case 24555:005. Raise Mirsap Action with Huge Text Copy Paste
	@Test
	//(priority = 14)
	public void createMirsapAction_WithHugeText(Method method)
			throws Exception {
		startLine(method);
		//navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);

		// Pass huge text data to all fields
		// verify maximum data acceptable for Title is 70 characters
		WebElement title = getWebElement("textField_Action_Title");
		String titleMaxCharCount = title.getAttribute("size");
		verifyTextCompare(MIRSAP_ACTION_TITLE_MAX_CHAR_SIZE, titleMaxCharCount);
		sendKeys("textField_Action_Title", ACTION_TITLE_MAX_CONTENT);
		String docContent = readTextFile(System.getProperty("user.dir")+ "\\hugeDataForTextField.txt");
		// System.out.println("text read: " + docContent);
		insertIntoAllTinyMCEinPage(docContent);

		// verify maximum data acceptable for Originating details is 70
		// characters
		WebElement originatingDetails = getWebElement("textField_Action_OriginatingDetails");
		String orginatingDetailsMaxCharCount = originatingDetails
				.getAttribute("size");
		verifyTextCompare(ORIGINATING_DETAILS_MAX_CHAR_SIZE,
				orginatingDetailsMaxCharCount);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL_MAX_CONTENT);

		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		selectByVisibleText("dropDown_Action_Template_Class", MIRSAP_CLASS);
		explicitlyWaitForElement("dropDown_Action_Template_SubClass1", getTotalWaitTimeInSecs());
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Dashboard_subClass1", subClassOne);
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Dashboard_subClass2", subClassTwo);

		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		explicitlyWaitForElement("button_Action_Raise", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		// Verify Associated Document URN in View page
		isInMirsapActionViewPage();
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		MirsapActionHugeTextURN = actUrn[2];

		//click on change state button
		click("button_Action_ChangeState");
		// Create officer validation
		click("button_Action_FastAction_CreateOfficer");
		explicitlyWaitForElement("textfield_Action_CreateOfficer_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname",
				CREATE_OFFICER_NAME_MAX_CHAR);
		sendKeys("textfield_Action_CreateOfficer_Forename",
				CREATE_OFFICER_NAME_MAX_CHAR);
		sendKeys("textfield_Action_CreateOfficer_OfficerNo",
				CREATE_OFFICER_OFFICERNO_MAX_CHAR);
		click("button_Action_FastAction_Edit_CreateOfficeDialog_Create");

		verifyText(CREATE_OFFICER_MAX_CHAR_COUNT_VALIDATION_MESS,
				"errorMsg_Action_CreateOfficer_SurnameAndForname");
		click("icon_Action_Create_CreateOfficer_Dialog_Close");
		click("button_Action_BulkUpdate_Cancel");
		endLine(method);

	}


	/*
	 * 005a. 29252 Update Action with Huge Text Copy Paste
	 */
	@Test
	//(priority = 15,dependsOnMethods = {"createMirsapAction_WithMandatoryFields"})
	public void updateMirsapAction_WithHugeText(Method method)
			throws Exception {
		startLine(method);
		navigateTo(VIEWACTION_URL);
		sendKeys("textField_Action_ViewUrn", mirsapUrn);
		clickAndWaitForPageLoad("button_Action_View");
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		isInMirsapActionViewPage();
		//click Update Button
		verifyElementPresent("button_Action_Update");
		//click update button
		click("button_Action_Update");
		// Pass huge text data to all fields
		// verify maximum data acceptable for Title is 70 characters
		explicitlyWaitForElement("textField_Action_Title", getTotalWaitTimeInSecs());
		WebElement title = getWebElement("textField_Action_Title");
		String titleMaxCharCount = title.getAttribute("size");
		verifyTextCompare(MIRSAP_ACTION_TITLE_MAX_CHAR_SIZE, titleMaxCharCount);
		sendKeys("textField_Action_Title", ACTION_TITLE_MAX_CONTENT);
		String docContent = readTextFile(System.getProperty("user.dir")+ "\\hugeDataForTextField.txt");
		// System.out.println("text read: " + docContent);
		insertIntoAllTinyMCEinPage(docContent);

		// verify maximum data acceptable for Originating details is 70
		// characters
		WebElement originatingDetails = getWebElement("textField_Action_OriginatingDetails");
		String orginatingDetailsMaxCharCount = originatingDetails
				.getAttribute("size");
		verifyTextCompare(ORIGINATING_DETAILS_MAX_CHAR_SIZE,
				orginatingDetailsMaxCharCount);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL_MAX_CONTENT);
		sendKeys("textField_Action_OriginatingDetails", ACTION_ORIGINATING_DETAIL_MAX_CONTENT);
		sendKeys("textField_Action_FastAction_Edit_ReceiversInstructions",docContent);

		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		// Verify Associated Document URN in View page
		isInMirsapActionViewPage();
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		MirsapActionHugeTextURN = actUrn[2];

		//click on change state button
		click("button_Action_ChangeState");
		// Create officer validation
		explicitlyWaitForElement("button_Action_FastAction_CreateOfficer",
				getTotalWaitTimeInSecs());
		click("button_Action_FastAction_CreateOfficer");
		explicitlyWaitForElement("textfield_Action_CreateOfficer_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname",
				CREATE_OFFICER_NAME_MAX_CHAR);
		sendKeys("textfield_Action_CreateOfficer_Forename",
				CREATE_OFFICER_NAME_MAX_CHAR);
		sendKeys("textfield_Action_CreateOfficer_OfficerNo",
				CREATE_OFFICER_OFFICERNO_MAX_CHAR);
		click("button_Action_FastAction_Edit_CreateOfficeDialog_Create");

		verifyText(CREATE_OFFICER_MAX_CHAR_COUNT_VALIDATION_MESS,
				"errorMsg_Action_CreateOfficer_SurnameAndForname");
		click("icon_Action_Create_CreateOfficer_Dialog_Close");
		click("button_Action_BulkUpdate_Cancel");
		endLine(method);

	}

	//Test case 24553:006. Raise Mirsap Action with Special Characters in all fields- Test Date [12^& i]
	@Test
	//(priority = 16)
	public void createMirsapAction_WithSpecialCharactersInAllFields(Method method)
			throws Exception {

		startLine(method);
		//navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		//verifying Action Tab is Enabled
		verifyMirsapActionTabEnabled();
		verifyElementPresent("button_Action_Raise");
		//insert Alpha numeric characters in Text field
		insertIntoAllTinyMCEinPage(ALPHA_NUMERIC_TEXT);
		selectTextByIndex("dropDown_Action_Mirsap_Force", 1);
		selectTextByIndex("dropDown_Action_Mirsap_Class", 2);
		selectTextByIndex("dropDown_Action_Mirsap_Priority", 2);
		selectTextByIndex("dropDown_Action_Mirsap_Station", 1);
		sendKeys("textField_Action_OriginatingDetails",
				ALPHA_NUMERIC_TEXT);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		//verifying data in View action page
		verifyText(ALPHA_NUMERIC_TEXT, "label_Action_View_Title");
		click("link_Action_Mirsap_View_Originating");
		verifyText(ALPHA_NUMERIC_TEXT,
				"label_Action_Mirsap_View_OriginatingDetails");
		//navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		verifyMirsapActionTabEnabled();
		verifyElementPresent("button_Action_Raise");
		//inserting special characters in text
		insertIntoAllTinyMCEinPage(SPECIALCHARACTER_TEXT);
		selectTextByIndex("dropDown_Action_Mirsap_Force", 1);
		selectTextByIndex("dropDown_Action_Mirsap_Priority", 2);
		selectTextByIndex("dropDown_Action_Mirsap_Station", 1);
		selectByVisibleText("dropDown_Action_Dashboard_Class", MIRSAP_CLASS);
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Dashboard_subClass1", subClassOne);
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Dashboard_subClass2", subClassTwo);
		sendKeys("textField_Action_OriginatingDetails",
				SPECIALCHARACTER_TEXT);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		//verifying special characters in view action page
		verifyText(SPECIALCHARACTER_TEXT, "label_Action_View_Title");
		click("link_Action_Mirsap_View_Originating");
		verifyText(SPECIALCHARACTER_TEXT,
				"label_Action_Mirsap_View_OriginatingDetails");
		//Entering  LinkedAction Urn and Document URN
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		verifyMirsapActionTabEnabled();
		verifyElementPresent("button_Action_Raise");
		insertIntoAllTinyMCEinPage(ALPHA_NUMERIC_TEXT);
		selectTextByIndex("dropDown_Action_Mirsap_Force", 1);
		selectTextByIndex("dropDown_Action_Mirsap_Class", 2);
		selectTextByIndex("dropDown_Action_Mirsap_Priority", 2);
		selectTextByIndex("dropDown_Action_Mirsap_Station", 1);
		sendKeys("textField_Action_OriginatingDetails",
				ALPHA_NUMERIC_TEXT);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");
		click("link_Action_Create_LinkedAssociation");
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement(
				"textField_Action_Create_LinkedAssoc_Action_URN",
				getTotalWaitTimeInSecs());

		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN", LINKEDACTION_SPECIALCHAR);
		addMirsapActionLinkedActionFrmSelectRecord();
		verifyText("A!! is not a valid URN","errorMsg_Action_LinkedAssociates_URN");
		click("link_Action_Mirsap_Linkedaction_Close");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		endLine(method);
	}

	//Test case 29253:006a. Update Action with Special Characters in all fields- Test Date [12^& i]
	@Test
	//(priority = 17)
	public void updateMirsapAction_WithSpecialCharactersInAllFields(Method method)
			throws Exception {

		startLine(method);
		String mirsapURN = raiseAndGetMirsapActionURN();
		//View mirsap action
		click("button_Action_Update");

		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Title"," ");
		sendKeys("textField_Action_Title",ALPHA_NUMERIC_TEXT);
		//insert Alpha numeric characters in Text field
		insertIntoAllTinyMCEinPage(ALPHA_NUMERIC_TEXT);

		sendKeys("textField_Action_OriginatingDetails",
				ALPHA_NUMERIC_TEXT);
		click("button_Action_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		//verifying data in View action page
		verifyText(ALPHA_NUMERIC_TEXT, "label_Action_View_Title");
		click("link_Action_Mirsap_View_Originating");
		verifyText(ALPHA_NUMERIC_TEXT,
				"label_Action_Mirsap_View_OriginatingDetails");
		//navigate to Raise Mirsap Action URL
		String mirsapURN1 = raiseAndGetMirsapActionURN();
		//View mirsap action
		click("button_Action_Update");

		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Title"," ");
		sendKeys("textField_Action_Title",SPECIALCHARACTER_TEXT);
		//inserting special characters in text
		insertIntoAllTinyMCEinPage(SPECIALCHARACTER_TEXT);

		sendKeys("textField_Action_OriginatingDetails",
				SPECIALCHARACTER_TEXT);
		click("button_Action_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		//verifying special characters in view action page
		verifyText(SPECIALCHARACTER_TEXT, "label_Action_View_Title");
		click("link_Action_Mirsap_View_Originating");
		verifyText(SPECIALCHARACTER_TEXT,
				"label_Action_Mirsap_View_OriginatingDetails");
		//Raise Mirsap action
		String mirsapURN2 = raiseAndGetMirsapActionURN();
		click("button_Action_Update");
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Title"," ");
		sendKeys("textField_Action_Title",ALPHA_NUMERIC_TEXT);
		insertIntoAllTinyMCEinPage(ALPHA_NUMERIC_TEXT);

		sendKeys("textField_Action_OriginatingDetails",
				ALPHA_NUMERIC_TEXT);
		click("link_Action_Create_LinkedAssociation");
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement(
				"textField_Action_Update_LinkedAssoc_Action_URN",
				getTotalWaitTimeInSecs());
		//enter special characters in linked action urn
		sendKeysAndTab("textField_Action_Update_LinkedAssoc_Action_URN", LINKEDACTION_SPECIALCHAR);
		explicitlyWaitForElement("dropDown_Action_Mirsap_EditRecord_ReferenceIncidentIcon1",
				getTotalWaitTimeInSecs());
		//select the reference incident dropdown list and clcik on add/update button
		addmirsapActionLinkedActionFrmSelectRecordInEditPage();
		verifyText("A!! is not a valid URN","errorMsg_Action_LinkedAssociates_URN");
		click("link_Action_Mirsap_Linkedaction_Close8");
		click("button_Action_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		endLine(method);
	}


	//Test case 26772:007. Raise MirsapAction - Text and Title field
	@Test
	//(priority = 18)
	public void raiseAction_TextAndTitle(Method method) throws Exception {
		startLine(method);
		//navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		//insert text Text field
		insertIntoAllTinyMCEinPage(TEXT1);

		//Get TextField Value
		String currentTextFieldValue = getTextFieldText();
		Report.info("Current Text field value during Raise: " + currentTextFieldValue );

		//After click on Title Field.verify Text field text is same as Title 
		click("textField_Action_Title");
		String currentTitleText = getText("textField_Action_Title");
		Report.info("Current Title value during Raise: " + currentTitleText);
		verifyTextCompare(currentTitleText, getTextFieldText()); 
		verifyText(TEXT1,"textField_Action_Title");

		//Append ABC to XYZ in Text field and compare Text and Title field are not equal
		insertIntoAllTinyMCEinPage(TEXT2);
		//Get TextField Value
		String currentTextFieldValueAppend = getTextFieldText();
		Report.info("Current Text field value during Raise and Append: " + currentTextFieldValueAppend );
		//After appending,  click on Title Field.verify Text field text is not same as Title 
		click("textField_Action_Title");
		String currentTitleTextAppend = getText("textField_Action_Title");
		Report.info("Current Title value during Raise and Append: " + currentTitleTextAppend);
		verifyTextOnlyNotEquals(currentTitleTextAppend, getTextFieldText()); 
		verifyTextNotEquals(TEXT2,"textField_Action_Title");

		//Clear Title field and then verify appended Text  prepopulated onto Text
		sendKeys("textField_Action_Title","");
		insertIntoAllTinyMCEinPage(TEXT2);
		//Get TextField Value
		String currentTextFieldValueAppend2 = getTextFieldText();
		Report.info("Current Text field value during Raise after clear and click Title: " + currentTextFieldValueAppend2 );
		//After appending,  click on Title Field.verify Text field text is not same as Title 
		click("textField_Action_Title");
		String currentTitleTextAppend2 = getText("textField_Action_Title");
		Report.info("Current Text field value during Raise after clear and click Title: " + currentTitleTextAppend2);
		verifyTextCompare(currentTitleTextAppend2, getTextFieldText()); 
		verifyText(TEXT2,"textField_Action_Title");

		//Fill in with all mandatory field data
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		//click Raise button
		clickAndWait("button_Action_Raise");

		// Verification of View Action Page
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		mirsapActionURN_textAndTitle = actUrn[1];
		// Validate inserted title in create page same as in view page
		verifyText( TEXT2 ,"label_Action_View_Title");

		//Validate Text
		verifyTextCompare(getText("label_Action_View_Title"), getTextFieldText()); 
		endLine(method);

	}

	// Test case 29246:007a. Update Mirsap Action - Text and Title field  
	@Test
	//(priority = 19,
	//dependsOnMethods = {"raiseAction_TextAndTitle"})
	public void updateAction_TextAndTitle(Method method) throws Exception {
		startLine(method);
		//navigate to view action page
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		sendKeys("textField_Action_ViewUrn", mirsapActionURN_textAndTitle);

		clickAndWaitForPageLoad("button_Action_View");
		waitForJQuery(driver);
		explicitlyWaitForElement("label_Action_View_Page_Title",getTotalWaitTimeInSecs());
		// verify is in Mirsap action View page
		isInMirsapActionViewPage();

		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		// Verify  Previous title  field value is retained on all update field- Title
		verifyText(TEXT2, "textField_Action_Title");
		verifyTextCompare(TEXT2, getTextFieldText()); 

		//Append Text3(DEF) to XYZABC(Text2) in Text field and compare Text and Title field are not equal
		insertIntoAllTinyMCEinPage(TEXT3);
		//Get TextField Value
		String currentTextFieldValueAppend = getTextFieldText();
		Report.info("Current Text field value during Update after append: " + currentTextFieldValueAppend );
		//After appending,  click on Title Field.verify Text field text is not same as Title 
		click("textField_Action_Title");
		String currentTitleTextAppend = getText("textField_Action_Title");
		Report.info("Current Title value during Update after append: " + currentTitleTextAppend);
		verifyTextCompare(currentTitleTextAppend, getTextFieldText()); 
		verifyTextNotEquals(TEXT2+TEXT3,"textField_Action_Title");

		//Clear Title field and then verify appended Text  prepopulated onto Text
		sendKeys("textField_Action_Title","");
		insertIntoAllTinyMCEinPage(TEXT3);
		//Get TextField Value
		String currentTextFieldValueAppend2 = getTextFieldText();
		Report.info("Current Text field value during Update after clear and click Title: " + currentTextFieldValueAppend2 );
		//After appending,  click on Title Field.verify Text field text is not same as Title 
		click("textField_Action_Title");
		String currentTitleTextAppend2 = getText("textField_Action_Title");
		Report.info("Current Text field value during Update after clear and click Title: " + currentTitleTextAppend2);
		verifyTextCompare(currentTitleTextAppend2, getTextFieldText()); 
		verifyText(TEXT2+" "+TEXT3,"textField_Action_Title");

		//click Save button
		clickAndWait("button_Action_Save");
		waitForJQuery(driver);
		explicitlyWaitForElement("successMsg_Action_Update",getTotalWaitTimeInSecs());
		// Verification of View Action Page
		verifyElementPresent("successMsg_Action_Update");
		//validate Action Success Message
		// Validate inserted title in update page same as in view page
		verifyText( TEXT2+" "+TEXT3 ,"label_Action_View_Title");
		endLine(method);

	}

	/*
	 * Test Case 26135: 008. Raise Action - Subject Field List
	 * 
	 */
	@Test
	//(priority = 20)
	public void raiseActionSubjectFieldList(Method method) throws Exception{
		startLine(method);
		WebDriver driverTemp = driver;
		//initialise a list of strings to use for default list
		List<String> recordList = new ArrayList<String>();
		recordList.add(linkedActionsRecordURN);
		recordList.add(linkedDocumentsRecordURN);
		recordList.add(linkedActionsRecordURN2);
		recordList.add(linkedActionsRecordURN3);
		recordList.add(nominalUrn);
		recordList.add(vehicleUrn);
		//recordList.add(nominalUrn2);This addition is in code and so commenting out
		recordList.add(locationUrn);
		recordList.add(locationUrn2);
		recordList.add(telephoneUrn);

		//check default list and verify
		ListManagement list = new ListManagement(driver);
		list.verifyDefaultListPresent(recordList);

		//navigate to raise action and check subject list
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		String parentWindowHandle = getCurrentWindowHandle();
		Report.info("Parent window handle info: " + parentWindowHandle);
		click("textField_Action_Mirsap_DefaultList");
		//verify records in the list, we know what to expect from our data above so we will verify against that
		//Verify by URN's
		explicitlyWaitForElement("textField_Action_DefaultList_Filter",getTotalWaitTimeInSecs());
		sendKeysAndEnter("textField_Action_DefaultList_Filter",nominalUrn);
		verifyElementPresent("checkBox_Action_Default_List_FirstRecord");

		sendKeysAndEnter("textField_Action_DefaultList_Filter",vehicleUrn);
		verifyElementPresent("checkBox_Action_Default_List_FirstRecord");


		// verify actions and documents present in default list does not get listed onto subject field list.
		// when Action and documents filter applied - expected not to have list items under //li[@style='display: list-item;']and hence checking if this object is not present
		sendKeysAndEnter("textField_Action_DefaultList_Filter",linkedActionsRecordURN);
		verifyElementPresent("checkbox_Action_Default_List_None");
		verifyElementNotPresent("checkbox_Action_ListItem");

		sendKeysAndEnter("textField_Action_DefaultList_Filter",linkedDocumentsRecordURN);
		verifyElementPresent("checkbox_Action_Default_List_None");
		verifyElementNotPresent("checkbox_Action_ListItem");


		//open new tab to list management to add additional record to default list
		WebDriver childNewTabListWebDriver = openNewWindowAndGetHandleWithChildWebDriver(driver);
		String childNewTabListWindowHandle = getCurrentWindowHandle();
		Report.info("child View Action popup window handle info: " + childNewTabListWindowHandle);
		//openNewWindowAndGetHandle();
		sleep(3000);
		navigateTo("/group/holmes/list-management");
		click("label_ListManagement_User_Tags_Sort");
		waitForJQuery(driver);
		explicitlyWaitForElement("checkBox_ListManagement_SearchResult_Fristrow", getTotalWaitTimeInSecs());

		click("checkBox_ListManagement_SearchResult_Fristrow");
		clickAndWaitForPageLoad("icon_ListManagement_View_List");
		explicitlyWaitForElement("button_ListManagement_Edit", getTotalWaitTimeInSecs());
		clickAndWaitForPageLoad("button_ListManagement_Edit");
		explicitlyWaitForElement("icon_ListManagement_AddNewListItem", getTotalWaitTimeInSecs());
		click("icon_ListManagement_AddNewListItem");
		explicitlyWaitForElement("button_ListManagement_AddNewListItem_Add", getTotalWaitTimeInSecs());
		sendKeys("textField_ListManagement_AddRecord", nominalUrn2);
		click("button_ListManagement_AddNewListItem_Add");
		explicitlyWaitForElement("button_ListManagement_AddNewListItem_Cancel", getTotalWaitTimeInSecs());
		click("button_ListManagement_AddNewListItem_Cancel");
		click("button_ListManagement_Save");
		explicitlyWaitForElement("successMsg_ListManagement_Create", getTotalWaitTimeInSecs());
		verifyTextContains("updated","successMsg_ListManagement_Create");

		//close window and return to previous Parent window
		int noOfWinCount = getWindowCount();
		driver=closechildWindowAndNavigateToItsParentWindowHandle(childNewTabListWebDriver, parentWindowHandle, noOfWinCount,noOfWinCount - 1);
		//closeWindowAndGetHandle();
		waitForPageLoad();
		//explicitlyWaitForElement("icon_Action_Refresh_Default_List", getTotalWaitTimeInSecs());
		waitForJQuery(driver);
		click("icon_Action_Refresh_Default_List");
		click("textField_Action_Mirsap_DefaultList");
		explicitlyWaitForElement("textField_Action_DefaultList_Filter",getTotalWaitTimeInSecs());
		sendKeysAndEnter("textField_Action_DefaultList_Filter",nominalUrn2);
		verifyElementPresent("checkBox_Action_Default_List_FirstRecord");

		//Filter by Nominal Description 
		//click("textField_Action_Mirsap_DefaultList");
		explicitlyWaitForElement("textField_Action_DefaultList_Filter",getTotalWaitTimeInSecs());
		sendKeysAndEnter("textField_Action_DefaultList_Filter",NOMINAL_DESCRIPTION);
		verifyElementPresent("checkBox_Action_Default_List_FirstRecord");

		//Filter by Vehicle Description 
		sendKeysAndEnter("textField_Action_DefaultList_Filter",VEHICLE_DESCRIPTION);
		verifyElementPresent("checkBox_Action_Default_List_FirstRecord");

		//Select Multiple list items 
		sendKeysAndEnter("textField_Action_DefaultList_Filter","");
		click("textField_Action_DefaultList_Filter");
		explicitlyWaitForElement("checkBox_Action_Default_List_FirstRecord", getTotalWaitTimeInSecs());
		click("checkBox_Action_Default_List_FirstRecord");
		click("checkBox_Action_Default_List_SecondRecord");
		click("checkBox_Action_Default_List_ThirdRecord");
		//click("textField_Action_Mirsap_Subject");
		verifyTextContains(getCurrentIncidentIDFull(), "textField_Action_Mirsap_Subject");

		//Check the complete subject List
		//click("textField_Action_Mirsap_Subject");
		click("link_Action_Mirsap_List_CheckAll");
		//click("textField_Action_Mirsap_Subject");
		verifyTextContains("selected", "textField_Action_Mirsap_Subject");

		//Fill in with all mandatory field data
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		//insert text Text field
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",ACTION_ORIGINATING_DETAIL);
		//click("textField_Action_Mirsap_DefaultList");
		//click("link_Action_Mirsap_List_CheckAll");

		//click Raise button
		clickAndWait("button_Action_Raise");

		// Verification of View Action Page
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		mirsapActionURN_subjectList = actUrn[1];
		driver = driverTemp;
		endLine(method);

	}
	/*
	 * Test case 29244:008a. Update Action - Subject field list
	 * 
	 */
	@Test
	//(priority = 21)
	public void updateActionSubjectFieldList(Method method) throws Exception{
		startLine(method);
		//navigate to view action page
		navigateTo(VIEWACTION_URL);
		verifyIsInViewActionPage();
		sendKeys("textField_Action_ViewUrn", mirsapActionURN_subjectList);

		clickAndWaitForPageLoad("button_Action_View");
		waitForJQuery(driver);
		explicitlyWaitForElement("label_Action_View_Page_Title",getTotalWaitTimeInSecs());
		// verify is in Mirsap action View page
		isInMirsapActionViewPage();

		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		click("textField_Action_Mirsap_Subject");
		String selectedItemsBeforeUncheck=getText("button_Action_subject");
		String[] noOfSelectedItemsBeforeUncheck= selectedItemsBeforeUncheck.split(" ");
		String totalSelectedListItemsBeforeUncheck=noOfSelectedItemsBeforeUncheck[0];
		Report.info("Total selected Items:"+ totalSelectedListItemsBeforeUncheck);
		//Uncheck 1 list Item
		// Deselected List Items should be removed from subject List
		click("button_Action_subject");
		click("checkBox_Action_Default_List_FirstRecord");
		//click("checkBox_Action_Default_List_SecondRecord");
		String selectedItemsAfterUncheck=getText("button_Action_subject");
		String[] noOfSelectedItemsAfterUncheck= selectedItemsAfterUncheck.split(" ");
		String totalSelectedListItemsAfterUncheck=noOfSelectedItemsAfterUncheck[0];
		Report.info("Total selected Items:"+ totalSelectedListItemsAfterUncheck);
		Assert.assertEquals(String.valueOf(Integer.parseInt(totalSelectedListItemsBeforeUncheck)-1), totalSelectedListItemsAfterUncheck);

		//Save the changes
		click("button_Action_Save");
		explicitlyWaitForElement("successMsg_Action_Create",getTotalWaitTimeInSecs());
		//Navigate to association Tab
		click("button_Action_Association_Tab");

		//click add button
		click("icon_Association_Add");
		verifyAddAssociation();
		//select subject type association
		selectByVisibleText("dropDown_Association_Add_Type",ASSOCIATION_TYPE_SUBJECT);
		//enter nominal urn
		sendKeys("textField_Association_Add_TargetURN",callerURN);
		//click add association button
		clickAndWait("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",getTotalWaitTimeInSecs());
		click("tab_action");
		waitForJQuery(driver);
		explicitlyWaitForElement("button_Action_Refresh",getTotalWaitTimeInSecs());
		click("button_Action_Refresh");
		waitForPageLoad();
		verifyTextContains(callerURN,"table_Action_View_Subject_CallerURN");

		//Verify on update Page for the callerURN record added from association Tab 
		clickAndWait("button_Action_Update");
		waitForPageLoad();
		click("button_Action_subject");
		verifyTextContains(callerURN,"dropDown_Action_Update_SubjectDetails");
		endLine(method);

	}


	//Test case 23848:008b.Update Mirsap Action : Uncheck all subject field list and Save
	// Defect Exists- HLM00008935 and hence expected Failure. 
	@Test
	//(priority = 22)
	public void updateActionUncheckAllSubjectFieldList(Method method) throws Exception{
		startLine(method);
		ListManagement list =new ListManagement(driver);
		ArrayList<String> items = new ArrayList<String>();
		items.add(nominalUrn);
		items.add(linkedDocumentsRecordURN);
		items.add(callerURN);
		items.add(linkedActionsRecordURN2);
		items.add(polURN);
		items.add(announcementURN);
		list.createDefaultList(items);

		verifyElementPresent("successMsg_ListManagement_Create");
		String ListCreate = getText("successMsg_ListManagement_Create");
		String[] ListName = ListCreate.split(" ");
		System.out.println(ListName[1]);

		String mirsapURN = raiseAndGetMirsapActionURN();
		click("button_Action_Update");
		click("dropDown_Action_Update_SubjectDetails");
		verifyElementPresent("checkBox_Action_Default_List_FirstRecord");

		//navigate to list management
		navigateTo("/group/holmes/list-management");
		sendKeysAndEnterWithDriver("text_ListManagement_Name", ListName[1]);
		waitForJQuery(driver);
		explicitlyWaitForElement("checkBox_ListManagement_SearchResult_Fristrow", getTotalWaitTimeInSecs());
		click("checkBox_ListManagement_SearchResult_Fristrow");
		click("icon_ListManagement_View_List");
		click("button_ListManagement_Edit");
		click("icon_ListManagement_AddNewListItem");
		explicitlyWaitForElement("button_ListManagement_AddNewListItem_Add", getTotalWaitTimeInSecs());
		sendKeys("textField_ListManagement_AddRecord", nominalUrn2);
		waitForJQuery(driver);
		click("button_ListManagement_AddNewListItem_Add");
		waitForJQuery(driver);
		click("button_ListManagement_Save");
		explicitlyWaitForElement("successMsg_ListManagement_Create", getTotalWaitTimeInSecs());
		verifyTextContains("updated","successMsg_ListManagement_Create");

		//View Mirsap action again
		viewAndGetMirsapActionURN(mirsapURN);
		click("button_Action_Update");
		click("dropDown_Action_Update_SubjectDetails");
		sendKeysAndEnter("textField_Action_DefaultList_Filter", nominalUrn2);
		verifyElementPresent("checkBox_Action_Default_List_FirstRecord");
		//Enter blank value in subject filter
		sendKeysAndEnter("textField_Action_DefaultList_Filter", " ");
		click("link_Action_Mirsap_List_CheckAll");
		clickAndWait("button_Action_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		//click on update uncheckall and click on save 
		click("button_Action_Update");
		click("dropDown_Action_Update_SubjectDetails");
		click("link_ListManagement_ListType_UncheckAll");
		clickAndWait("button_Action_Save");
		explicitlyWaitForElement("errorMsg_Action_Subject",
				getTotalWaitTimeInSecs());
		verifyElementPresent("errorMsg_Action_Subject");
		endLine(method);
	}

	// Test case 25048:009. Raise Mirsap Action from Nominal Entity / Entity
	@Test
	//(priority = 23)
	public void createMirsapAction_FromNominal(Method method) throws Exception {
		startLine(method);
		// navigate to Nominal Entity URL
		navigateTo(INDEXES_NOMINAL_URL);
		sendKeys("textField_Indexes_Nominal_URN", nominalUrn);
		clickAndWait("button_Indexes_Search");
		explicitlyWaitForElement("table_Indexes_SearchResults",
				getTotalWaitTimeInSecs());
		verifyElementPresent("table_Indexes_SearchResults");
		clickAndWait("table_Indexes_SearchResults");
		clickAndWait("icon_Indexes_SearchResult_View");
		explicitlyWaitForElement("text_Indexes_Exhibit_URN",
				getTotalWaitTimeInSecs());
		verifyTextContains(nominalUrn, "text_Indexes_Exhibit_URN");

		clickAndWaitForBrowserPopup("Action","button_Action_nominal_RaiseAction");
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		verifyTextContains(nominalUrn, "text_Action_nominal_Create_Subjects");
		insertIntoAllTinyMCEinPage(ALPHA_NUMERIC_TEXT);
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		selectByVisibleText("dropDown_Action_Template_Class", MIRSAP_CLASS);
		sendKeys("textField_Action_OriginatingDetails",
				ALPHA_NUMERIC_TEXT);
		explicitlyWaitForElement("textField_Action_Mirsap_DefaultList",
				getTotalWaitTimeInSecs());
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");
		verifyElementPresent("button_Action_Raise");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		click("button_Action_Association_Tab");
		verifyAssociationTabEnabled();
		explicitlyWaitForElement("checkBox_Action_Association_SecondRecord",
				getTotalWaitTimeInSecs());
		verifyElementPresent("checkBox_Action_Association_SecondRecord");
		sendKeysAndEnterWithDriver("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + nominalUrn);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		closePopupWindowAndNavigateToParentWindow("Action", "Nominal");
		endLine(method);
	}

	// Test case 9432:010. Similar Action
		@Test
		//(priority = 32, dependsOnMethods = {"createMirsapAction_WithAllFields"})
		public void createMirsapAction_Similar(Method method) throws Exception {
			startLine(method);

			navigateTo(VIEWACTION_URL);
			verifyIsInViewActionPage();
			explicitlyWaitForElement("textField_Action_ViewUrn", getTotalWaitTimeInSecs());
			if(mirsapActionURN_allFieldValues == null ){
				mirsapActionURN_allFieldValues = mirsapUrn;
			}
			/*// set temp value of A14 for action
						if(mirsapActionURN_allFieldValues == null){
							mirsapActionURN_allFieldValues = mirsapUrn;
						}*/
			sendKeys("textField_Action_ViewUrn", mirsapActionURN_allFieldValues);
			clickAndWaitForPageLoad("button_Action_View");
			waitForJQuery(driver);
			explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
			isInMirsapActionViewPage();
			verifyElementPresent("button_Action_Similar");
			//click Similar button
			clickAndWait("button_Action_Similar");

			// Verify All Previous values are retained on all mandatory fields on  click ofSimilar
			verifyTextContains(incidentIdFull, "dropDown_Action_Create_Incident");
			//verifyText( ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE ,"textField_Action_Title");
			verifyTextContains(TINY_MCE_TEXT_FOR_RAISE_MIRSAP_ACTION, getTextFieldText());
			// Validate Protective Marking : Restricted is set as default
			verifyCurrentSelectByText("dropDown_Action_Create_ProtectiveMarking",PROTECTIVE_MARKING_SECRET);
			// Validate  Priority : Low is set as default value
			verifyCurrentSelectByText("dropDown_Action_Mirsap_Priority",PRIORITY_HIGH);
			verifyText(ALLOCATION ,"label_Action_Create_State");

			//Verify no Disclosure Record
			verifyElementPresent("section_Action_Create_Disclosure");

			// change PM
			selectByVisibleText("dropDown_Action_Create_ProtectiveMarking",PROTECTIVE_MARKING_SECRET);
			// change priority
			selectByVisibleText("dropDown_Action_Mirsap_Priority", PRIORITY_HIGH);

			// update title field
			explicitlyWaitForElement("textField_Action_Title", getTotalWaitTimeInSecs());
			sendKeys("textField_Action_Title", "Updated Title for Action");

			//click on raise button
			explicitlyWaitForElement("button_Action_Raise", getTotalWaitTimeInSecs());
			clickAndWait("button_Action_Raise");
			explicitlyWaitForElement("successMsg_Action_Create",
					getTotalWaitTimeInSecs());
			verifyElementPresent("successMsg_Action_Create");
			endLine(method);
		}

	/* Test case 42749:012. Raise Mirsap Action -ADD/Edit/Delete icon buttons in Linked Actions Grid Section
	 */ // TODO : Default list checking priority  
	@Test
	//(priority = 26)	
	public void raiseMirsapActionIconsinLinkedGridSection(Method method) throws Exception{
		startLine(method);
		//navigate to raise Mirsap action
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		verifyMirsapActionTabEnabled();
		waitForJQuery(driver);
		//Add icon in Linked Action Grid
		//Add invalid and then valid action URN
		//click on add linked action Section
		click("link_Action_Create_LinkedAssociation");
		explicitlyWaitForElement("icon_Action_LinkedActions_Create", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());

		// validate	"Please select/input an entity" alert
		click("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Action_URN"," ");
		click("dropDown_Action_Mirsap_SelectRecord_ReferenceIncidentIcon");
		sleep(2000);
		String xpathForCurrentIncident = "//div[contains(@id,'"+incidentIdFull+"')]";
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				getTotalWaitTimeInSecs());
		WebElement element = webDriverWaitForTask.until(ExpectedConditions
				.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
		element.click();
		explicitlyWaitForElement("button_Action_Create_LinkedAssoc_Action_Add", getTotalWaitTimeInSecs());
		click("button_Action_Create_LinkedAssoc_Action_Add");
		waitForAlertTextAndClose(NO_INPUT);
		//click("link_Action_Mirsap_Linkedaction_Close");

		//add invalid URN- Linked Action
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN",INVALID_URN);
		addMirsapActionLinkedActionFrmSelectRecord();
		verifyText(INVALID_URN + " does not exist","errorMsg_Action_LinkedAction_InvalidURN");
		click("link_Action_Mirsap_Linkedaction_Close");

		//add valid urn - change to from Excel- Linked Action
		explicitlyWaitForElement("icon_Action_LinkedActions_Create", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN",linkedActionsRecordURN);
		addMirsapActionLinkedActionFrmSelectRecord();
		verifyText(linkedActionsRecordURN, "table_Action_Create_LinkedAction_FirstRecordURN");

		//Adding multiple Linked Action URN
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN",linkedActionsRecordURN2);
		addMirsapActionLinkedActionFrmSelectRecord();
		verifyText(linkedActionsRecordURN2, "table_Action_Create_LinkedAction_SecondRecordURN");



		/*	//enter valid URN from default list
			click("button_Action_Create_LinkedAssoc_Action");
			clickAndWait("button_Action_Create_LinkedAssoc_Action");
			//select action from the default list
			sendKeys("dropDown_Action_Select_DefaultList", "A5");
			addMirsapActionLinkedActionFrmSelectRecord();
		 */


		// read only Action
		clickAndWait("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		//Read Only URN validation
		sendKeys("textField_Action_Create_LinkedAssoc_Action_URN",polActionURN);
		addMirsapActionLinkedActionFrmSelectRecord();
		explicitlyWaitForElement("button_Action_Raise", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("errorMsg_Action_AssociateAction_MarkedAsReadOnly", getTotalWaitTimeInSecs());
		verifyTextContains(IS_MARKED_AS_READ_ONLY_TEXT, "errorMsg_Action_AssociateAction_MarkedAsReadOnly");
		//Delete Linked POL Action URN
		click("checkBox_Action_LinkedAction_ThirdRecord");
		clickAndWait("icon_Action_LinkedAssoc_Delete");


		// Not a valid  action
		click("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Action_URN", polURN);
		addMirsapActionLinkedActionFrmSelectRecord();
		//click Save button
		explicitlyWaitForElement("button_Action_Raise", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("errorMsg_Action_AssociateAction_Invalid_FastAction", getTotalWaitTimeInSecs());
		verifyTextContains(NOT_A_VALID_FAST_ACTION_TEXT, "errorMsg_Action_AssociateAction_Invalid_FastAction");

		//Edit Linked Action URN( A1 to A3 edited )
		explicitlyWaitForElement("checkBox_Action_LinkedAction_FirstRecord", getTotalWaitTimeInSecs());
		checkCheckBox("checkBox_Action_LinkedAction_FirstRecord");
		clickAndWait("icon_Action_LinkedActions_Edit");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Action_URN",linkedActionsRecordURN3);
		addMirsapActionLinkedActionFrmSelectRecord();
		click("checkBox_Action_LinkedAction_FirstRecord");
		verifyText(linkedActionsRecordURN3, "table_Action_Create_LinkedAction_FirstRecordURN");

		//Delete Linked Action URN( Delete top row element- A3)
		click("checkBox_Action_LinkedAction_FirstRecord");
		clickAndWait("icon_Action_LinkedAssoc_Delete");
		endLine(method);

	}

	/* Test case 42750:013. Raise Mirsap Action -ADD/Edit/Delete icon buttons in Associated Document Grid Section
	 */
	@Test
	//(priority = 28)
	public void raiseMirsapActionIconsinAssociatedDocumentGridSection(Method method) throws Exception{
		startLine(method);
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		verifyMirsapActionTabEnabled();
		waitForJQuery(driver);
		//continuation from previous test case	
		expandLinkedActionOrAssociatedDocument();
		click("button_Action_Create_LinkedAssoc_Documents");
		// validate	"Please select/input an entity" alert

		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN"," ");
		click("dropDown_Action_Mirsap_SelectRecord_ReferenceIncidentIcon1");
		sleep(2000);
		String xpathForCurrentIncident = "(//div[@id='" + incidentIdFull + "'])";
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				getTotalWaitTimeInSecs());
		WebElement element = webDriverWaitForTask.until(ExpectedConditions
				.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
		element.click();
		explicitlyWaitForElement("button_Action_Create_LinkedAssoc_Documents_Add", getTotalWaitTimeInSecs());
		click("button_Action_Create_LinkedAssoc_Documents_Add");
		waitForAlertTextAndClose(NO_INPUT);


		//add invalid URN- Linked Action
		sendKeysAndTab("textField_Action_Create_LinkedAssoc_Document_URN",INVALID_URN);
		String xpathForCurrentIncident1 = "(//div[@id='" + incidentIdFull + "'])";
		addMirsapActionAssociatedDocumentFrmSelectRecord(xpathForCurrentIncident1);
		verifyText(INVALID_URN+ " does not exist","errorMsg_Action_AssociatedDoc_InvalidURN");
		click("link_Action_Mirsap_LinkedAssocdocument_Close");

		//Adding valid Linked Document URN
		explicitlyWaitForElement("button_Action_Create_LinkedAssoc_Documents", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN);
		String xpathForCurrentIncident2 = "(//div[@id='" + incidentIdFull + "'])";
		addMirsapActionAssociatedDocumentFrmSelectRecord(xpathForCurrentIncident2);
		explicitlyWaitForElement("table_AssociatedDocuments_Createpage_URN", getTotalWaitTimeInSecs());
		verifyText(linkedDocumentsRecordURN, "table_AssociatedDocuments_Createpage_URN");


		//Adding multiple Linked Document URN
		explicitlyWaitForElement("button_Action_Create_LinkedAssoc_Documents", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN2);
		String xpathForCurrentIncident3 = "(//div[@id='" + incidentIdFull + "'])";
		addMirsapActionAssociatedDocumentFrmSelectRecord(xpathForCurrentIncident3);
		explicitlyWaitForElement("table_AssociatedDocuments_Createpage_URN2", getTotalWaitTimeInSecs());
		verifyText(linkedDocumentsRecordURN2, "table_AssociatedDocuments_Createpage_URN2");

		// read only Action
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		//invalid URN validation
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",polActionURN);
		String xpathForCurrentIncident4 = "(//div[@id='" + incidentIdFull + "'])";
		addMirsapActionAssociatedDocumentFrmSelectRecord(xpathForCurrentIncident4);
		explicitlyWaitForElement("table_AssociatedDocuments_Createpage_URN4", getTotalWaitTimeInSecs());
		verifyText(polActionURN, "table_AssociatedDocuments_Createpage_URN4");
		explicitlyWaitForElement("button_Action_Raise", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("errorMsg_Action_AssociateDocument_MarkedAsReadOnly", getTotalWaitTimeInSecs());
		verifyTextContains(IS_MARKED_AS_READ_ONLY_TEXT, "errorMsg_Action_AssociateDocument_MarkedAsReadOnly");
		//Delete Associated Document URN
		explicitlyWaitForElement("checkBox_Action_LinkedDocument_ThirdRecord", getTotalWaitTimeInSecs());
		checkCheckBox("checkBox_Action_LinkedDocument_ThirdRecord");
		clickAndWait("icon_Action_AssocDocument_Delete");

		// Not a valid  action
		click("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN", polURN);
		String xpathForCurrentIncident5 = "(//div[@id='" + incidentIdFull + "'])";
		addMirsapActionAssociatedDocumentFrmSelectRecord(xpathForCurrentIncident5);
		explicitlyWaitForElement("table_AssociatedDocuments_Createpage_URN4", getTotalWaitTimeInSecs());
		verifyText(polURN, "table_AssociatedDocuments_Createpage_URN4");
		//click Save button
		explicitlyWaitForElement("button_Action_Raise", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("errorMsg_Action_AssociateDocument_Invalid_FastAction", getTotalWaitTimeInSecs());
		verifyTextContains(NOT_A_VALID_FAST_ACTION_TEXT, "errorMsg_Action_AssociateDocument_Invalid_FastAction");


		//Edit Associated Document URN
		explicitlyWaitForElement("checkBox_Action_LinkedDocument_FirstRecord", getTotalWaitTimeInSecs());
		checkCheckBox("checkBox_Action_LinkedDocument_FirstRecord");
		clickAndWait("icon_Action_AssociatedDocument_Edit");
		explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN3);
		String xpathForCurrentIncident6 = "(//div[@id='" + incidentIdFull + "'])";
		addMirsapActionAssociatedDocumentFrmSelectRecord(xpathForCurrentIncident6);
		explicitlyWaitForElement("table_Action_Create_AssociatedDocument_FirstRecordURN", getTotalWaitTimeInSecs());
		verifyText(linkedDocumentsRecordURN3, "table_Action_Create_AssociatedDocument_FirstRecordURN");

		//Delete Associated Document URN
		checkCheckBox("checkBox_Action_LinkedDocument_FirstRecord");
		clickAndWait("icon_Action_AssocDocument_Delete");

		//Delete Few more invalid POL Associations from Linked Action and Associated doc to successfully create the working record.
		checkCheckBox("checkBox_Action_LinkedDocument_SecondRecord");
		//click("checkBox_Action_LinkedDocument_ThirdRecord");
		clickAndWait("icon_Action_AssocDocument_Delete");
		/*
			checkCheckBox("checkBox_Action_LinkedAction_SecondRecord");
			//click("checkBox_Action_LinkedAction_ThirdRecord");
			clickAndWait("icon_Action_LinkedAssoc_Delete");*/

		//insert text into mandatory fields and complete Action Creation for further verification
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		// Verification of View Action Page
		verifyElementPresent("successMsg_Action_Create");
		//verify Association Tab Records
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		String mirsapActionURN_ForGrids = actUrn[1];

		click("tab_Association");
		verifyAssociationTabEnabled();
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord", getTotalWaitTimeInSecs());
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + linkedDocumentsRecordURN);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		endLine(method);
	}

	/*
	 *Test case 42754:014. Update Mirsap Action -Add/Edit/Delete icon buttons in Linked Actions Grid Section
	 */
	@Test
	//(priority=29)
	public void updateMirsapActionLinkedActionGridSection(Method method) throws Exception{
		startLine(method);
		mirsapUrn= raiseAndGetMirsapActionURN();
		isInMirsapActionViewPage();
		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		//verify is in Edit Mirsap action page
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());

		//click on add linked action Section
		click("link_Action_Create_LinkedAssociation");

		// validate		"Please select/input an entity" alert
		click("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Update_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_LinkedAssoc_Action_URN"," ");
		click("dropDown_Action_Mirsap_EditRecord_ReferenceIncidentIcon1");
		sleep(2000);
		String xpathForCurrentIncident1 = "(//div[@id='" + incidentIdFull + "'])";
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				getTotalWaitTimeInSecs());
		WebElement element = webDriverWaitForTask.until(ExpectedConditions
				.elementToBeClickable(By.xpath(xpathForCurrentIncident1)));	
		element.click();
		explicitlyWaitForElement("button_Action_Update_LinkedAssoc_Actions_Add", getTotalWaitTimeInSecs());
		click("button_Action_Update_LinkedAssoc_Actions_Add");
		waitForAlertTextAndClose(NO_INPUT);

		//invalid URN validation
		sendKeys("textField_Action_Update_LinkedAssoc_Action_URN",INVALID_URN);
		addmirsapActionLinkedActionFrmSelectRecordInEditPage();
		verifyTextContains(INVALID_URN + " does not exist","errorMsg_Action_AssociateDocument_edit_InvalidURN");

		// Not a valid Mirsap action
		sendKeys("textField_Action_Update_LinkedAssoc_Action_URN",polURN);
		addmirsapActionLinkedActionFrmSelectRecordInEditPage();


		//click Save button
		explicitlyWaitAndGetWebElementText("button_Action_Save", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Save");
		explicitlyWaitForElement("errorMsg_Action_AssociateDocument_Invalid_FastAction", getTotalWaitTimeInSecs());
		verifyTextContains(NOT_A_VALID_FAST_ACTION_TEXT, "errorMsg_Action_AssociateDocument_Invalid_FastAction");
		click("checkBox_Action_LinkedAction_Update_FirstRecord");
		click("icon_Action_LinkedAssoc_Delete");

		// read only Action
		click("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Update_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		//invalid URN validation
		sendKeys("textField_Action_Update_LinkedAssoc_Action_URN",polActionURN);
		addmirsapActionLinkedActionFrmSelectRecordInEditPage();
		clickAndWait("button_Action_Save");	
		explicitlyWaitForElement("errorMsg_Action_AssociateDocument_MarkedAsReadOnly", getTotalWaitTimeInSecs());
		verifyTextContains(IS_MARKED_AS_READ_ONLY_TEXT, "errorMsg_Action_AssociateDocument_MarkedAsReadOnly");


		//Delete Linked Action URN
		click("checkBox_Action_LinkedAction_Update_FirstRecord");
		click("icon_Action_LinkedAssoc_Delete");

		//Adding Linked Action URN
		click("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Update_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_LinkedAssoc_Action_URN",linkedActionsRecordURN);
		//		String actualCurrentIncidentVal = getWebElement("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1").getText();
		verifyTextContains(incidentIdFull, "dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon4");
		addmirsapActionLinkedActionFrmSelectRecordInEditPage();
		explicitlyWaitForElement("table_Action_Update_LinkedAction_FirstRecordURN", getTotalWaitTimeInSecs());
		verifyText(linkedActionsRecordURN, "table_Action_Update_LinkedAction_FirstRecordURN");

		//Adding multiple Linked Action URN
		click("button_Action_Create_LinkedAssoc_Action");
		explicitlyWaitAndGetWebElement("textField_Action_Update_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_LinkedAssoc_Action_URN",linkedActionsRecordURN2);
		addmirsapActionLinkedActionFrmSelectRecordInEditPage();
		verifyText(linkedActionsRecordURN2, "table_Action_Update_LinkedAction_SecondRecordURN");

		//Edit Linked Action URN
		click("checkBox_Action_LinkedAction_Update_SecondRecord");
		click("icon_Action_LinkedActions_Edit");
		explicitlyWaitAndGetWebElement("textField_Action_Update_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_LinkedAssoc_Action_URN",linkedActionsRecordURN3);
		addmirsapActionLinkedActionFrmSelectRecordInEditPage();
		click("checkBox_Action_LinkedAction_Update_SecondRecord");
		verifyText(linkedActionsRecordURN3, "table_Action_Update_LinkedAction_SecondRecordURN");
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Save");	

		// Verify Linked Action URN in View page
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		isInMirsapActionViewPage();
		click("link_Action_Create_LinkedAssociation");
		verifyText(linkedActionsRecordURN, "table_Action_View_LinkedActions_FirstRecordURN");

		// Verify Linked Action URN in Association Tab
		click("tab_Association");
		verifyAssociationTabEnabled();
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + linkedActionsRecordURN3);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		endLine(method);

	}


	/* Test case 42758:015. Update Mirsap Action -ADD/ Edit / Delete icon buttons in Associated Documents Grid Section */
	// TODO : Default list checking priority  
	@Test
	//(priority=30)
	public void updateMirsapActionAssociatedDocumentGridSection(Method method) throws Exception{
		startLine(method);

		mirsapUrn= raiseAndGetMirsapActionURN();
		isInMirsapActionViewPage();
		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		//verify is in Edit Mirsap action page
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());

		//click on add linked action Section
		click("link_Action_Create_LinkedAssociation");

		// validate		"Please select/input an entity" alert
		click("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Update_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_LinkedAssoc_Document_URN"," ");
		click("dropDown_Action_Mirsap_SelectRecord_ReferenceIncidentIcon5");
		sleep(2000);
		String xpathForCurrentIncident1 = "(//div[@id='" + incidentIdFull + "'])";
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				getTotalWaitTimeInSecs());
		WebElement element = webDriverWaitForTask.until(ExpectedConditions
				.elementToBeClickable(By.xpath(xpathForCurrentIncident1)));	
		element.click();
		explicitlyWaitForElement("button_Action_Update_LinkedAssoc_Documents_Add", getTotalWaitTimeInSecs());
		click("button_Action_Update_LinkedAssoc_Documents_Add");
		waitForAlertTextAndClose(NO_INPUT);

		//invalid URN validation
		sendKeys("textField_Action_Update_LinkedAssoc_Document_URN",INVALID_URN);
		String xpathForCurrentIncident2 = "(//div[@id='" + incidentIdFull + "'])";
		addMirsapActionAssociatedDocumentFrmSelectRecordInEditPage(xpathForCurrentIncident2);
		//			verifyTextContains(INVALID_URN + " does not exist","errorMsg_Action_AssociateDocument_edit_InvalidURN");
		explicitlyWaitForElement("errorMsg_Action_AssociatedDoc_InvalidURN", getTotalWaitTimeInSecs());
		verifyText(INVALID_URN+ " does not exist","errorMsg_Action_AssociatedDoc_InvalidURN");
		/*click("link_Action_Mirsap_LinkedAssocdocument_Close");


			// Not a valid Mirsap action
			explicitlyWaitForElement("button_Action_Update_LinkedAssoc_Documents_Add", getTotalWaitTimeInSecs());
			clickAndWait("button_Action_Update_LinkedAssoc_Documents_Add");*/
		explicitlyWaitForElement("textField_Action_Update_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_LinkedAssoc_Document_URN",polURN);
		//click("textField_Action_Create_LinkedAssoc_Document_URN");
		String xpathForCurrentIncident3 = "(//div[@id='" + incidentIdFull + "'])";
		addMirsapActionAssociatedDocumentFrmSelectRecordInEditPage(xpathForCurrentIncident3);


		//click Save button
		explicitlyWaitAndGetWebElementText("button_Action_Save", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Save");
		explicitlyWaitForElement("errorMsg_Action_AssociateDocument_Invalid_FastAction", getTotalWaitTimeInSecs());
		verifyTextContains(NOT_A_VALID_FAST_ACTION_TEXT, "errorMsg_Action_AssociateDocument_Invalid_FastAction");
		click("checkBox_Action_LinkedDocument_Edit_FirstRecord");
		click("icon_Action_LinkedDocument_Delete");

		// read only Action
		click("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Update_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		//invalid URN validation
		sendKeys("textField_Action_Update_LinkedAssoc_Document_URN",polActionURN);
		String xpathForCurrentIncident4 = "(//div[@id='" + incidentIdFull + "'])";
		addMirsapActionAssociatedDocumentFrmSelectRecordInEditPage(xpathForCurrentIncident4);

		clickAndWait("button_Action_Save");	
		explicitlyWaitForElement("errorMsg_Action_AssociateDocument_MarkedAsReadOnly", getTotalWaitTimeInSecs());
		verifyTextContains(IS_MARKED_AS_READ_ONLY_TEXT, "errorMsg_Action_AssociateDocument_MarkedAsReadOnly");


		//Delete Associated Document URN
		click("checkBox_Action_LinkedDocument_Edit_FirstRecord");
		click("icon_Action_AssocDocument_Delete");

		//Adding Linked Document URN
		click("button_Action_Create_LinkedAssoc_Documents");
		// Verify Dialog box title is poped up
		verifyText(ACTION_ASSOCIATEDOCUMENT_DIALOGBOXTITLE, "text_Action_AssociateDocument_EditDialogboxTitle");
		explicitlyWaitAndGetWebElement("textField_Action_Update_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_LinkedAssoc_Document_URN",linkedDocumentsRecordURN);
		//		String actualCurrentIncidentVal = getWebElement("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1").getText();
		verifyTextContains(incidentIdFull, "dropDown_Action_Mirsap_SelectRecord_ReferenceIncidentIcon5");
		String xpathForCurrentIncident5 = "(//div[@id='" + incidentIdFull + "'])";
		addMirsapActionAssociatedDocumentFrmSelectRecordInEditPage(xpathForCurrentIncident5);
		explicitlyWaitForElement("table_Action_Create_AssociatedDocument_Edit_FirstRecordURN", getTotalWaitTimeInSecs());
		verifyText(linkedDocumentsRecordURN, "table_Action_Create_AssociatedDocument_Edit_FirstRecordURN");

		//Adding multiple Linked Document URN
		click("button_Action_Create_LinkedAssoc_Documents");
		explicitlyWaitAndGetWebElement("textField_Action_Update_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_LinkedAssoc_Document_URN",linkedDocumentsRecordURN2);
		String xpathForCurrentIncident6 = "(//div[@id='" + incidentIdFull + "'])";
		addMirsapActionAssociatedDocumentFrmSelectRecordInEditPage(xpathForCurrentIncident6);
		verifyText(linkedDocumentsRecordURN2, "table_Action_Create_AssociatedDocument_Edit_SecondRecordURN");

		//Edit Associated Document URN
		click("checkBox_Action_LinkedDocument_Edit_SecondRecord");
		click("icon_Action_AssociatedDocument_Edit");
		explicitlyWaitAndGetWebElement("textField_Action_Update_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_LinkedAssoc_Document_URN",linkedDocumentsRecordURN3);
		String xpathForCurrentIncident7 = "(//div[@id='" + incidentIdFull + "'])";
		addMirsapActionAssociatedDocumentFrmSelectRecordInEditPage(xpathForCurrentIncident7);
		click("checkBox_Action_LinkedDocument_Edit_SecondRecord");
		verifyText(linkedDocumentsRecordURN3, "table_Action_Create_AssociatedDocument_Edit_SecondRecordURN");
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Save");	

		// Verify Associated Document URN in View page
		explicitlyWaitForElement("label_Action_View_Page_Title", getTotalWaitTimeInSecs());
		isInMirsapActionViewPage();
		click("link_Action_Create_LinkedAssociation");
		verifyText(linkedDocumentsRecordURN, "table_Action_View_AssociatedDocument_FirstRecordURN");

		// Verify Associated Document URN in Association Tab
		click("tab_Association");
		verifyAssociationTabEnabled();
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord", getTotalWaitTimeInSecs());
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + linkedDocumentsRecordURN);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		endLine(method);
	}

	/* Test case  Test case 42760:016. Update Mirsap Action -ADD/Edit/Delete icon buttons in Documents Taken Grid Section */
	@Test
	//(priority=31)
	public void updateMirsapActionDocumentsTakenGridSection(Method method) throws Exception {
		startLine(method);
		mirsapUrn= raiseAndGetMirsapActionURN();
		isInMirsapActionViewPage();
		verifyElementPresent("button_Action_Update");
		//click update button
		clickAndWait("button_Action_Update");
		//verify is in Edit Mirsap action page
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());


		// validate		"Please select/input an entity" alert
		click("icon_Action_Edit_DocumentTaken_Add");
		explicitlyWaitAndGetWebElement("textField_Action_Update_DocumentTaken_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN"," ");
		click("icon_Action_DocumentTaken_Incident_Arrow");
		sleep(2000);
		String xpathForCurrentIncident = "//div[contains(@id,'"+incidentIdFull+"')]";
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				getTotalWaitTimeInSecs());
		WebElement element = webDriverWaitForTask.until(ExpectedConditions
				.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
		element.click();
		explicitlyWaitForElement("button_Action_Update_DocumentTaken_Documents_Add", getTotalWaitTimeInSecs());
		click("button_Action_Update_DocumentTaken_Documents_Add");
		waitForAlertTextAndClose(NO_INPUT);

		//invalid URN validation
		click("icon_Action_Edit_DocumentTaken_Add");
		explicitlyWaitAndGetWebElement("textField_Action_Update_DocumentTaken_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",INVALID_URN);
		addMirsapActionDocumentTakenFrmSelectRecordInEditPage("//div[contains(@id,'"+incidentIdFull+"')]");
		verifyTextContains(INVALID_URN + " does not exist", "errorMsg_Action_DocumentTaken_edit_InvalidURN4");

		// Not a valid  action
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",polURN);
		click("textField_Action_Update_DocumentTaken_Document_URN");
		addMirsapActionDocumentTakenFrmSelectRecordInEditPage("//div[contains(@id,'"+incidentIdFull+"')]");

		//click Save button
		waitForJQuery(driver);
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Save");		
		explicitlyWaitForElement("errorMsg_Action_DocumentTaken_Invalid_FastAction", getTotalWaitTimeInSecs());
		verifyTextContains(NOT_A_VALID_FAST_ACTION_TEXT, "errorMsg_Action_DocumentTaken_Invalid_FastAction");
		click("checkBox_Action_DocumentTaken_Edit_FirstRecord");
		click("icon_Action_Edit_DocumentTaken_Delete");

		// read only Action
		click("icon_Action_Edit_DocumentTaken_Add");
		explicitlyWaitForElement("textField_Action_Update_DocumentTaken_Document_URN", getTotalWaitTimeInSecs());
		//invalid URN validation
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",polActionURN);
		addMirsapActionDocumentTakenFrmSelectRecordInEditPage("//div[contains(@id,'"+incidentIdFull+"')]");
		waitForJQuery(driver);
		explicitlyWaitForElement("button_Action_Save", getTotalWaitTimeInSecs());
		clickAndWait("button_Action_Save");	
		explicitlyWaitForElement("errorMsg_Action_DocumentTaken_MarkedAsReadOnly", getTotalWaitTimeInSecs());
		verifyTextContains(IS_MARKED_AS_READ_ONLY_TEXT, "errorMsg_Action_DocumentTaken_MarkedAsReadOnly");
		/*click("checkBox_Action_DocumentTaken_Edit_SecondRecord");
			click("icon_Action_Edit_DocumentTaken_Delete");*/

		//Delete Associated Document URN
		click("checkBox_Action_DocumentTaken_Edit_FirstRecord");
		click("icon_Action_Edit_DocumentTaken_Delete");

		//Adding Linked Document URN
		click("icon_Action_Edit_DocumentTaken_Add");
		// Verify Dialog box title is poped up
		verifyText(ACTION_DOCUMENTTAKEN_DIALOGBOXTITLE, "text_Action_DocumentTaken_EditDialogboxTitle");
		explicitlyWaitForElement("textField_Action_Update_DocumentTaken_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",linkedDocumentsRecordURN);
		//				String actualCurrentIncidentVal = getWebElement("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1").getText();
		addMirsapActionDocumentTakenFrmSelectRecordInEditPage("//div[contains(@id,'"+incidentIdFull+"')]");
		explicitlyWaitForElement("table_Action_Create_DocumentTaken_Edit_FirstRecordURN", getTotalWaitTimeInSecs());
		verifyText(linkedDocumentsRecordURN, "table_Action_Create_DocumentTaken_Edit_FirstRecordURN");

		//Adding multiple Linked Document URN
		click("icon_Action_Edit_DocumentTaken_Add");
		explicitlyWaitForElement("textField_Action_Update_DocumentTaken_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",linkedDocumentsRecordURN2);
		addMirsapActionDocumentTakenFrmSelectRecordInEditPage("//div[contains(@id,'"+incidentIdFull+"')]");
		verifyText(linkedDocumentsRecordURN2, "table_Action_Create_DocumentTaken_Edit_SecondRecordURN");

		//Edit Associated Document URN
		click("checkBox_Action_DocumentTaken_Edit_SecondRecord");
		click("icon_Action_DocumentTaken_Edit");
		explicitlyWaitForElement("textField_Action_Update_DocumentTaken_Document_URN", getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Update_DocumentTaken_Document_URN",linkedDocumentsRecordURN3);
		addMirsapActionDocumentTakenFrmSelectRecordInEditPage("//div[contains(@id,'"+incidentIdFull+"')]");
		click("table_Action_Create_DocumentTaken_Edit_SecondRecordURN");
		verifyText(linkedDocumentsRecordURN3, "table_Action_Create_DocumentTaken_Edit_SecondRecordURN");

		clickAndWait("button_Action_Save");	
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		// Verify Associated Document URN in View page

		isInMirsapActionViewPage();
		explicitlyWaitForElement("button_Action_Update",
				getTotalWaitTimeInSecs());
		// verify Association Tab Records
		explicitlyWaitForElement("tab_Association",
				getPoolWaitTime());
		clickAndWait("tab_Association");
		waitForPageLoad();
		explicitlyWaitForElement("checkBox_Action_Association_FirstRecord",
				getTotalWaitTimeInSecs());
		verifyAssociationTabEnabled();

		sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
				incidentIdFull + "-" + linkedDocumentsRecordURN);
		waitForJQuery(driver);
		verifyElementPresent("checkBox_Action_Association_FirstRecord");
		endLine(method);
	}


	// Test case 29259:014. Manage Entity Template
	//Defect Exists and expected to Fail (HLM00009266 )
	@Test
	//(priority = 32)
	public void manageEntityTemplate(Method method)
			throws Exception {
		startLine(method);
		navigateTo("/group/holmes/manage-action-templates");
		explicitlyWaitForElement("header_Action_EntityTemplate",
				getTotalWaitTimeInSecs());
		//If Template Present with Title " Automation Template" - delete the record and proceed to Create New
		if (isElementPresent("table_ActionTemplate_FirstRecord"))
		{
			click("table_ActionTemplate_FirstRecord");
			click("icon_Delete_Manage_Action_Template");
			click("button_Action_Template_Yes_Delete");
			verifyTextContains("deleted","successMsg_Action_Delete");

		}
		//if no existing Template with same name -> Proceed with creating new Template
		{
			clickAndWait("icon_Create_Manage_Action_Template");

			selectByVisibleText("dropDown_Action_Template_Entity_Type", "Action");
			verifyTextContains(incidentIdFull, "dropDown_Action_CreateTemplate_Incident");
			verifyFieldsInCreateActionTemplate();
			sendKeys("label_Action_Template_Name", TEMPLATE_NAME);
			sendKeys("label_Action_Template_Description",TEMPLATE_TEXT );
			//unable to select drop downs here
			selectByVisibleText("dropDown_Action_Template_Protective_Marking", PROTECTIVE_MARKING);
			selectByVisibleText("dropDown_Action_Template_Priority", PRIORITY);
			selectByVisibleText("dropDown_Action_Template_Force", MIRSAP_FORCE);
			selectByVisibleText("dropDown_Action_Template_Station", MIRSAP_STATION);
			selectByVisibleText("dropDown_Action_Dashboard_Class", MIRSAP_CLASS);
			waitForJQuery(driver);
			selectByVisibleText("dropDown_Action_Dashboard_subClass1", subClassOne);
			waitForJQuery(driver);
			selectByVisibleText("dropDown_Action_Dashboard_subClass2", subClassTwo);
			sendKeys("textField_Action_Template",TEMPLATE_TEXT);
			sendKeys("label_Action_Template_Title",TEMPLATE_TEXT);
			insertTextInAllocatedOfficerTextField("textField_Action_Template_AllocatedOfficer1",CREATE_OFFICER_SURNAME);
			selectOptionWithText(CREATE_OFFICER_SURNAME , "textField_Action_Template_AllocatedOfficer1_SelectBox1");
			//sendKeysAndEnter("textField_Action_Template_AllocatedOfficer1",CREATE_OFFICER_SURNAME );
			//sendKeys("textField_Action_Template_AllocatedOfficer1", CREATE_OFFICER_FORENAME);
			click("checkBox_Action_Themes");
			sendKeys("textField_Action_Template_OriginatingDetails",TEMPLATE_TEXT);
			clickAndWait("button_Action_Template_Create");
			verifyTextContains("created","successMsg_Action_Create");
		}

		//return to grid view - assume only 1 template exists
		//View the Template 
		click("checkBox_Action_Template");
		click("icon_View_Manage_Action_Template");
		click("button_Action_Template_Close");
		explicitlyWaitForElement("header_Action_EntityTemplate",
				getTotalWaitTimeInSecs());


		//Update the Template
		click("checkBox_Action_Template");
		click("icon_Update_Manage_Action_Template");
		sendKeys("label_Action_Template_Description", "This is an update Automated Testing description");
		click("button_Action_Template_Save");
		verifyTextContains("updated","successMsg_Action_Update");

		//Cancel Update of Template
		click("checkBox_Action_Template");
		click("icon_Update_Manage_Action_Template");
		click("button_Action_Template_Cancel");
		click("button_Action_Template_Yes_Cancel");


		//Check Refresh Button 
		//Only Refresh button can be verified and loading or any logi after refresh cannot be applied here
		click("icon_Refresh_Manage_Action_Template");

		//Delete of Template
		click("checkBox_Action_Template");
		click("icon_Delete_Manage_Action_Template");
		click("button_Action_Template_Yes_Delete");
		verifyTextContains("deleted","successMsg_Action_Delete");

		//create a new template and use the Template during Raise MIrsap Action
		clickAndWait("icon_Create_Manage_Action_Template");
		selectByVisibleText("dropDown_Action_Template_Entity_Type", "Action");
		verifyTextContains(incidentIdFull, "dropDown_Action_CreateTemplate_Incident");
		verifyFieldsInCreateActionTemplate();
		sendKeys("label_Action_Template_Name", TEMPLATE_NAME);
		sendKeys("label_Action_Template_Description",TEMPLATE_TEXT );

		selectByVisibleText("dropDown_Action_Template_Protective_Marking", PROTECTIVE_MARKING);
		selectByVisibleText("dropDown_Action_Template_Priority", PRIORITY);
		selectByVisibleText("dropDown_Action_Template_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Template_Station", MIRSAP_STATION);
		selectByVisibleText("dropDown_Action_Dashboard_Class", MIRSAP_CLASS);
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Dashboard_subClass1", subClassOne);
		waitForJQuery(driver);
		selectByVisibleText("dropDown_Action_Dashboard_subClass2", subClassTwo);
		sendKeys("textField_Action_Template",TEMPLATE_TEXT);
		sendKeys("label_Action_Template_Title",TEMPLATE_TEXT);
		insertTextInAllocatedOfficerTextField("textField_Action_Template_AllocatedOfficer1",CREATE_OFFICER_SURNAME);
		selectOptionWithText(CREATE_OFFICER_SURNAME , "textField_Action_Template_AllocatedOfficer1_SelectBox1");

		//sendKeysAndEnter("textField_Action_Template_AllocatedOfficer1",CREATE_OFFICER_SURNAME );
		//sendKeys("textField_Action_Template_AllocatedOfficer1", CREATE_OFFICER_FORENAME);
		click("checkBox_Action_Themes");
		sendKeys("textField_Action_Template_OriginatingDetails",TEMPLATE_TEXT);
		clickAndWait("button_Action_Template_Create");

		//Navigate to Raise Mirsap Action Page and select the template 
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		selectByVisibleText("dropDown_Action_Template","Automation Template");
		//verify the prepopulated values from Template match the Template
		verifyRaiseMirsapActionPrePopulatedValuesFromTemplate();
		verifyCurrentSelectByText("dropDown_Action_Dashboard_subClass1",subClassOne);
		verifyCurrentSelectByText("dropDown_Action_Dashboard_subClass2",subClassTwo);


		//Select Subject field List Items 
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		//Click Create
		click("button_Action_Raise");

		//Verify on View Action Page 
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(PROTECTIVE_MARKING,"label_Action_Mirsap_View_ProtectedMarking");
		verifyText(TEMPLATE_TEXT, "label_Action_View_Title");
		verifyText(MIRSAP_FORCE, "label_Action_Mirsap_View_Force");
		verifyText(MIRSAP_CLASS, "label_Action_Mirsap_View_Class");
		verifyText(MIRSAP_PRIORITY, "label_Action_Mirsap_View_Priority");
		verifyText(MIRSAP_STATION, "label_Action_Mirsap_View_Station");

		verifyText(subClassOne, "label_Action_Mirsap_View_SubClass1");
		verifyText(subClassTwo, "label_Action_Mirsap_View_SubClass2");
		click("link_Action_Mirsap_View_Originating");
		verifyText(TEMPLATE_TEXT,
				"label_Action_Mirsap_View_OriginatingDetails");
		click("link_Action_Create_Theme");
		//Existing Defect as Themes not getting created
		verifyElementPresent("table_Action_ThemeItem");
		verifyText(actionThemeOne,"table_Action_ThemeItem");
		endLine(method);
	}


	//Test case 9599:015. Audit Log- Entity Template
	//Defect Exists (HLM00009276) 
	@Test
	//(priority = 33)
	public void auditlog_EntityTemplate(Method method) throws Exception
	{
		startLine(method);

		AuditLogSearch au1 = new AuditLogSearch(driver);
		// search audit and verify result
		au1.auditLogSearchByActivity(CREATE_ACTION_TEMPLATE);
		verifyAuditResult(CREATE_ACTION_TEMPLATE);

		au1.auditLogSearchByActivity(UPDATE_ACTION_TEMPLATE);
		verifyAuditResult(UPDATE_ACTION_TEMPLATE);

		au1.auditLogSearchByActivity(DELETE_ACTION_TEMPLATE);
		verifyAuditResult(DELETE_ACTION_TEMPLATE);

		endLine(method);
	}



	/*
	 * Test case 27691:Additional - Mirsap - Prevent Subject Associations
	 * to/from Actions being created via the association tab
	 */
	@Test
	//(priority = 34)
	public void CraeteSubjectAssociation_FromAssocTab(Method method)
			throws Exception {
		startLine(method);
		//navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		verifyTextContains(incidentIdFull, "dropDown_Action_Create_Incident");
		verifyMandatoryFieldsInMirsapAction();
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		//insert text Text field
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");

		click("link_Action_Create_LinkedAssociation");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		//clicking Association tab
		click("tab_Association");
		verifyAssociationTabEnabled();
		click("icon_Association_Add");
		verifyElementPresent("dropDown_Association_Add_Type");
		selectByVisibleText("dropDown_Association_Add_Type", ASSOCIATION_TYPE_SUBJECT);
		sendKeys("textField_Association_Add_TargetURN", "N10");
		click("button_Association_Add_Create");
		explicitlyWaitForElement("successMsg_Action_Add_Association",
				getTotalWaitTimeInSecs());
		//click("tab_action");
		//verifyMirsapActionTabEnabled();
		//wait for action update
		//explicitlyWaitForElement("button_Action_Update",
		//	getTotalWaitTimeInSecs());
		//verifyElementPresent("button_Action_Update");
		//click update button
		//clickAndWait("button_Action_Update");
		//(//span[contains(text(),'N1')])[2]
		endLine(method);
	}
	
	@AfterTest
	public void afterTest() {
		driver.quit();
	}

	// Test case 9455:015. Constrained List for Soft Actions
	//@Test(priority = 11)
	public void constrainedList_Action(Method method) throws Exception {
		startLine(method);
		// navigate to Nominala Entity URL
		navigateTo(MANAGE_CONSTRAINED_LIST);
		verifyElementPresent("dropDown_ManageConstrainedList_StaticDataSet");
		selectByVisibleText("dropDown_ManageConstrainedList_StaticDataSet", "Priority");
		click("button_ManageConstrainedList_Add");
		sendKeys("textfield_ManageConstrainedList_Add_Code",
				"T");
		sendKeys("textfield_ManageConstrainedList_Add_Value",
				"H");
		click("button_ManageConstrainedList_Add_submit");
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		verifyMirsapActionTabEnabled();
		explicitlyWaitForElement("but"
				+ "ton_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		//insert text Text field
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", "H");
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		//Verification of data in View Action Fields
		verifyElementPresent("successMsg_Action_Create");
	}


	// Test case 27711:Bulk Update - Existing document tags are being removed
	// from Actions
	//@Test(priority = 13)
	public void bulkUpdateVerify_ExistingDocument(Method method)
			throws Exception {
		startLine(method);
		navigateTo(SEARCH_ACTION_URL);
		click("dropDown_Action_Search_State");
		click("checkBox_Action_Search_ForAllocation");
		clickAndWait("button_Action_Search");
		explicitlyWaitForElement("table_Action_SearchResults",
				getTotalWaitTimeInSecs());
		String searchStates = getTextboxValue("table_Action_SearchResults_State");
		System.out.println(searchStates + ":::");
		verifyText("For Allocation", "table_Action_SearchResults_State");
		click("checkBox_Action_Search_FirstRecord");
		click("checkBox_Action_Search_SecondRecord");
		click("link_Action_SearchResult_BulkStateMove");
		explicitlyWaitForElement("button_Action_SearchResult_BulkMove_MoveAll",
				getTotalWaitTimeInSecs());
		click("button_Action_SearchResult_BulkMove_MoveAll");
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficer",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_FastAction_QueueAllocatedOfficer",
				"Surnames");
		explicitlyWaitForElement(
				"textfield_Action_FastAction_QueueAllocatedOfficerName",
				getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_FastAction_QueueAllocatedOfficerName");

		click("button_Action_Save");
		endLine(method);
	}
















	public void clickAssociationTabAndVerifyIsInAssociationTab()
			throws Exception {
		explicitlyWaitForElement("tab_Association", getTotalWaitTimeInSecs());
		WebElement element = explicitlyWaitAndGetWebElement("tab_Association", getTotalWaitTimeInSecs());
		clickElementUsingJS(element);
		waitForJQuery(driver);
		verifyAssociationTabEnabled();
	}

















	/*
	 * Test case 29229:012(a.2.4). RaiseAction -ADD - Associated Documents Grid
	 * Section - Enter InValid URN 
	 * TODO-need to add verify read only data
	 * TODO - needs merging with 26132
	 */
	//@Test(priority = 31)
	public void createMirsapAction_WithAssociateDocumentInvalidURN(Method method)
			throws Exception {
		startLine(method);
		// navigate to Raise Mirsap Action URL
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		verifyElementPresent("button_Action_Raise");
		// insert text into mandatory fields
		sendKeys("textField_Action_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		insertIntoAllTinyMCEinPage("test");
		selectByVisibleText("dropDown_Action_Mirsap_Force", MIRSAP_FORCE);
		selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		selectByVisibleText("dropDown_Action_Mirsap_Priority", MIRSAP_PRIORITY);
		selectByVisibleText("dropDown_Action_Mirsap_Station", MIRSAP_STATION);
		sendKeys("textField_Action_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		click("textField_Action_Mirsap_DefaultList");
		click("link_Action_Mirsap_List_CheckAll");
		// Adding Linked Linked URN
		click("link_Action_Create_LinkedAssociation");
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");

		// Verify Dialog box title is poped up
		verifyText(ACTION_ASSOCIATEDOCUMENT_DIALOGBOXTITLE,
				"text_Action_AssociateDocument_DialogboxTitle");
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN", "A!!");
		click("dropDown_Action_Mirsap_SelectRecord_ReferenceIncidentIcon1");
		click("textField_Action_Create_LinkedAssoc_Document_URN");
		clickAndWait("button_Action_Create_LinkedAssoc_Documents_Add");
		// verify invalid data
		verifyText("A!! is not a valid URN",
				"errorMsg_Action_LinkedAssociates_URN");
		click("link_Action_Mirsap_LinkedAssocdocument_Close");

		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		// Verify Dialog box title is poped up
		verifyText(ACTION_ASSOCIATEDOCUMENT_DIALOGBOXTITLE,
				"text_Action_AssociateDocument_DialogboxTitle");
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN"," ");
		click("dropDown_Action_Mirsap_SelectRecord_ReferenceIncidentIcon1");
		click("textField_Action_Create_LinkedAssoc_Document_URN");
		clickAndWait("button_Action_Create_LinkedAssoc_Documents_Add");
		// verify null urn
		waitForAlertTextAndClose("Please select/input an entity");
		click("link_Action_Mirsap_LinkedAssocdocument_Close");

		// verify enter invalid URN
		clickAndWait("button_Action_Create_LinkedAssoc_Documents");
		// Verify Dialog box title is poped up
		verifyText(ACTION_ASSOCIATEDOCUMENT_DIALOGBOXTITLE,
				"text_Action_AssociateDocument_DialogboxTitle");
		sendKeys("textField_Action_Create_LinkedAssoc_Document_URN", "N1");
		click("dropDown_Action_Mirsap_SelectRecord_ReferenceIncidentIcon1");
		click("textField_Action_Create_LinkedAssoc_Document_URN");
		clickAndWait("button_Action_Create_LinkedAssoc_Documents_Add");

		clickAndWait("button_Action_Raise");
		// verify invalid urn add
		verifyTextContains("is not a valid Fast Action,Action",
				"errorMsg_Action_LinkedAction_InvalideUrn");
		// click("link_Action_Mirsap_Linkedaction_Close");
		endLine(method);

	}

	

	private void addmirsapActionLinkedActionFrmSelectRecordInEditPage() throws InterruptedException{
		click("dropDown_Action_Mirsap_EditRecord_ReferenceIncidentIcon1");
		sleep(2000);
		String xpathForCurrentIncident = "//div[@id='" + incidentIdFull + "']";
		/*WebElement we=driver.findElement(By.xpath(xpathForCurrentIncident));
	    	 we.click();*/
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				getTotalWaitTimeInSecs());
		WebElement element = null;
		try {
			element = webDriverWaitForTask.until(ExpectedConditions
					.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
			element.click();
			Report.info("Element is clicked");
		} catch (Exception e) {
			//	   			e.printStackTrace();
			Report.fail("Could not able to find the element to click from drop down");
			Assert.fail("Could not able to find the element to click from drop down");

		}
		clickAndWait("button_Action_Update_LinkedAssoc_Actions_Add");
	}

	private void addMirsapActionLinkedActionFrmSelectRecord() throws InterruptedException{
		click("dropDown_Action_Mirsap_SelectRecord_ReferenceIncidentIcon");
		sleep(2000);
		String xpathForCurrentIncident = "//div[@id='" + incidentIdFull + "']";
		/* WebElement we=driver.findElement(By.xpath(xpathForCurrentIncident));
    	 we.click();*/
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				getTotalWaitTimeInSecs());
		WebElement element = null;
		try {
			element = webDriverWaitForTask.until(ExpectedConditions
					.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
			element.click();
			Report.info("Element is clicked");
		} catch (Exception e) {
			//  			e.printStackTrace();
			Report.fail("Could not able to find the element to click from drop down");
			Assert.fail("Could not able to find the element to click from drop down");

		}

		clickAndWait("button_Action_Create_LinkedAssoc_Action_Add");
	}

	private void addMirsapActionAssociatedDocumentFrmSelectRecord(String xpathForCurrentIncident)
			throws InterruptedException {
		click("dropDown_Action_Mirsap_SelectRecord_ReferenceIncidentIcon1");
		sleep(2000);
		//	 		String xpathForCurrentIncident = "//div[contains(@id,'"+incidentIdFull+"')]";
		//	 		String xpathForCurrentIncident = "//*[@id='" + incidentIdFull + "']";
		//	 		String xpathForCurrentIncident = "(//div[@id=''" + incidentIdFull + "'])[2]";
		//	 		String xpathForCurrentIncident = "(//div[contains(div/@id,'" + incidentIdFull + "')])[2]";
		Report.info("xpathForCurrentIncident" + xpathForCurrentIncident);
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				getTotalWaitTimeInSecs());
		WebElement element = null;

		try {
			element = webDriverWaitForTask.until(ExpectedConditions
					.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
			element.click();
			Report.info("Element is clicked");
		} catch (Exception e) {
			//	 			e.printStackTrace();
			Report.fail("Could not able to find the element to click from drop down");
			Assert.fail("Could not able to find the element to click from drop down");

		}
		/*		

	 		WebElement we=driver.findElement(By.xpath(xpathForCurrentIncident));
	 		we.click();*/
		clickAndWait("button_Action_Create_LinkedAssoc_Documents_Add");
	}
	private void addMirsapActionAssociatedDocumentFrmSelectRecordInEditPage(String xpathForCurrentIncident)
			throws InterruptedException {
		click("dropDown_Action_Mirsap_SelectRecord_ReferenceIncidentIcon5");
		sleep(2000);
		//	 		String xpathForCurrentIncident = "//div[contains(@id,'"+incidentIdFull+"')]";
		//	 		String xpathForCurrentIncident = "//*[@id='" + incidentIdFull + "']";
		//	 		String xpathForCurrentIncident = "(//div[@id=''" + incidentIdFull + "'])[2]";
		//	 		String xpathForCurrentIncident = "(//div[contains(div/@id,'" + incidentIdFull + "')])[2]";
		Report.info("xpathForCurrentIncident" + xpathForCurrentIncident);
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				getTotalWaitTimeInSecs());
		WebElement element = null;

		try {
			element = webDriverWaitForTask.until(ExpectedConditions
					.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
			element.click();
			Report.info("Element is clicked");
		} catch (Exception e) {
			//	 			e.printStackTrace();
			Report.fail("Could not able to find the element to click from drop down");
			Assert.fail("Could not able to find the element to click from drop down");

		}
		/*		

	 		WebElement we=driver.findElement(By.xpath(xpathForCurrentIncident));
	 		we.click();*/
		clickAndWait("button_Action_Update_LinkedAssoc_Documents_Add");
	}

	private void addMirsapActionDocumentTakenFrmSelectRecordInEditPage(String xpathForCurrentIncident)
			throws InterruptedException {
		click("dropDown_Action_Mirsap_EditRecord_ReferenceIncidentIcon");
		sleep(2000);
		//	 		String xpathForCurrentIncident = "//div[contains(@id,'"+incidentIdFull+"')]";
		//	 		String xpathForCurrentIncident = "//*[@id='" + incidentIdFull + "']";
		//	 		String xpathForCurrentIncident = "(//div[@id=''" + incidentIdFull + "'])[2]";
		//	 		String xpathForCurrentIncident = "(//div[contains(div/@id,'" + incidentIdFull + "')])[2]";
		Report.info("xpathForCurrentIncident" + xpathForCurrentIncident);
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				getTotalWaitTimeInSecs());
		WebElement element = null;

		try {
			element = webDriverWaitForTask.until(ExpectedConditions
					.elementToBeClickable(By.xpath(xpathForCurrentIncident)));	
			element.click();
			Report.info("Element is clicked");
		} catch (Exception e) {
			//	 			e.printStackTrace();
			Report.fail("Could not able to find the element to click from drop down");
			Assert.fail("Could not able to find the element to click from drop down");

		}
		/*		

	 		WebElement we=driver.findElement(By.xpath(xpathForCurrentIncident));
	 		we.click();*/
		clickAndWait("button_Action_Update_DocumentTaken_Documents_Add");
	}



	public void verifyAuditResult(String activityDesc) throws Exception {
		verifyText(usernameUpperCase,
				"table_AuditLog_AuditRecord_FirstRecord_Username");
		verifyTextContains(incidentIdFull,
				"table_AuditLog_AuditRecord_FirstRecord_Incident");
		verifyTextContains(activityDesc,
				"table_AuditLog_AuditRecord_FirstRecord_Activity");
	}

}
