package Modules.Actions.MisrapAction;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Modules.AuditLogs.AuditLogSearch;
import ReusableUtilities.Dataprovider;

public class ConfigureActionTeam extends MirsapAction {
	
	private static final String CONFIGURE_ACTIONTEAM_MASTERENQUIRY_SURNAME = "Mathew";
	private static final String CONFIGURE_ACTIONTEAM_MASTERENQUIRY_FORENAME = "David";
	private static final String CONFIGURE_ACTIONTEAM_EXISTING_SURNAME = "Surname";
	private static final String CONFIGURE_ACTIONTEAM_EXISTING_FORENAME = "Forename";

	private static final String CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_SURNAME = "QUEEN";
	private static final String CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_FORENAME = "KING";
	private static final String CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_SURNAME1 = "JACK";
	private static final String CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_FORENAME1 = "JILL";
	
	private static final String CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_SURNAMETODELETE = "CCC";
	private static final String CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_FORENAMETODELETE = "BBB";
	
	private static final String CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_EXISTING_SURNAME = "Surname";
	
	private static final String CONFIGURE_ACTIONTEAM_ACTIVE_SURNAME = "PRETTY";
	private static final String CONFIGURE_ACTIONTEAM_ACTIVE_FORENAME = "Name";

	public String FOR_ALLOCATION = "For Allocation";

	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}

	//default constructor
	public ConfigureActionTeam() {
		softAssert = new SoftAssert();
	}

	@BeforeTest
	public void beforeTest() throws Exception {
		loginInIEAndSelectIncident();
	}

	@DataProvider(name = "login")
	public String[][] dataProvider() {

		String[][] testData = Dataprovider.readExcelData("Actions",
				testDataFilePath, "Login");
		return testData;
	}

	
	//Test case 30755:001. Enquiry team Maintenance - Rework required for Sorting
	
	@Test(priority = 1)
	public void EnquiryTeamMaintenance(Method method) throws Exception {
		startLine(method);
		//click on configure action team link
		navigateTo(CONFIGURE_ACTION_TEAM_URL);
		explicitlyWaitForElement("text_Action_Configure_ActionTeam_EnquiryTeamMember",getTotalWaitTimeInSecs());
		verifyElementPresent("text_Action_Configure_ActionTeam_EnquiryTeamMember");
		//click on cretae new team member
		click("icon_Action_Configure_ActionTeam_EnquiryTeam_CreateNewTeamMember");
		verifyElementPresent("text_Action_Configure_ActionTeam_EnquiryTeamList_CreateOfficer");
		verifyElementPresent("textField_Action_Configure_ActionTeam_Surname");
		verifyElementPresent("textField_Action_Configure_ActionTeam_Forename");
		verifyElementPresent("dropDown_Action_Configure_ActionTeam_MasterEnquiryTeam_Rank");
		verifyElementPresent("textField_Action_Configure_ActionTeam_MasterEnquiryTeam_OfficerNumber");
		verifyElementPresent("textField_Action_Configure_ActionTeam_MasterEnquiryTeam_UsualPartner");
		verifyElementPresent("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Active");

		//click on create button
		click("button_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer_Create");
		explicitlyWaitForElement("errorMsg_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer",getTotalWaitTimeInSecs());
		verifyElementPresent("errorMsg_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer");
		//enter surname and forename
		sendKeys("textField_Action_Configure_ActionTeam_Surname", CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_SURNAME);
		sendKeys("textField_Action_Configure_ActionTeam_Forename", CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_FORENAME);
		//click on create
		click("button_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer_Create");
		
		/*if (getText("errorMsg_Action_Configure_ActionTeam_MasterEnquiryTeam_ExistingOfficer").equals("Enquiry Officer already exists or is Inactive"))
		{
			sendKeys("textField_Action_Configure_ActionTeam_Surname", " ");
			sendKeys("textField_Action_Configure_ActionTeam_Forename", " ");
			sendKeys("textField_Action_Configure_ActionTeam_Surname", CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_SURNAME1);
			sendKeys("textField_Action_Configure_ActionTeam_Forename", CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_FORENAME1);
			//click on create
			click("button_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer_Create");
		}*/
        
		//Click on attach icon and attach an officer
	    explicitlyWaitForElement("text_Action_Configure_ActionTeam_EnquiryTeamMember",getTotalWaitTimeInSecs());
		verifyElementPresent("text_Action_Configure_ActionTeam_EnquiryTeamMember");
		waitForJQuery(driver);
		clickAndWait("icon_Action_Configure_ActionTeam_EnquiryTeam_Attachement");
		explicitlyWaitForElement("text_Action_Configure_ActionTeam_EnquiryTeamList_Attachement",getTotalWaitTimeInSecs());
		verifyElementPresent("text_Action_Configure_ActionTeam_EnquiryTeamList_Attachement");
		explicitlyWaitForElement("checkBox_Action_Configure_ActionTeam_EnquiryTeam_Attachement_Firstrecord",getTotalWaitTimeInSecs());
		click("checkBox_Action_Configure_ActionTeam_EnquiryTeam_Attachement_Firstrecord");
		//click on attach officer selected
		click("icon_Action_Configure_ActionTeam_EnquiryTeam_Attachement_AttachOfficerSelected");
		waitForAlertTextAndClose("Successfully attached");

		//raise action and allocate it to officer - login with system admin user to get notification and change OFFICER_SURNAME="VARIJA" and OFFICER_FORENAME="A" in the same class
		String mirsapURN=raiseAndGetMirsapActionURN();
		verifyText(ALLOCATION, "label_Action_View_State");
		//getList and verify state in dropdown
		getAllListItemAndVerify( ALLOCATED,"dropDown_Action_Select_State");
		//change state button click
		clickAndWait("button_Action_ChangeState");
		//wait for allocation officer
		explicitlyWaitForElement("button_Action_ChangeState_Save",
				getTotalWaitTimeInSecs());
		explicitlyWaitForElement("textfield_Action_MirsapAction_QueueAllocatedOfficerOne",
				getTotalWaitTimeInSecs());
		verifyMirsapActionFieldsInAllocatedPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		insertIntoAllTinyMCEinPage(TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE);
		//Add First Create Officer
		click("button_Action_MirsapAction_CreateOfficerOne");
		explicitlyWaitForElement(
				"textfield_Action_CreateOfficer_Surname",
				getTotalWaitTimeInSecs());
		sendKeys("textfield_Action_CreateOfficer_Surname",
				CREATE_OFFICER_SURNAME);
		sendKeys("textfield_Action_CreateOfficer_Forename",
				CREATE_OFFICER_FORENAME);
		click("button_Action_MirsapAction_CreateOne");
		click("textfield_Action_MirsapAction_QueueAllocatedOfficerOne");
		insertTextInAllocatedOfficerTextField("textfield_Action_FastAction_QueueAllocatedOfficer",CREATE_OFFICER_SURNAME);
		selectOptionWithText(CREATE_OFFICER_SURNAME , "textfield_Action_StateMove_AllocatedOfficerName");
	/*	//sendKeys("textfield_Action_MirsapAction_QueueAllocatedOfficerOne",CREATE_OFFICER_SURNAME);
		explicitlyWaitForElement("textfield_Action_StateMove_OfficerName",
				getTotalWaitTimeInSecs());
		clickAndWait("textfield_Action_StateMove_OfficerName");
		verifyText(CREATE_OFFICER_SURNAME + " " + CREATE_OFFICER_FORENAME,
				"textfield_Action_MirsapAction_QueueAllocatedOfficerOne");*/
		explicitlyWaitForElement("button_Action_ChangeState_Save",
				getTotalWaitTimeInSecs());
		clickAndWait("button_Action_ChangeState_Save");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		verifyElementPresent("successMsg_Action_Create");
		verifyText(ALLOCATED, "label_Action_View_State");

		explicitlyWaitForElement("icon_Home_Workspace_Notification",
				getTotalWaitTimeInSecs());

		// Notification checking - 
		notificationCheck();
		
		/*click("icon_Home_Workspace_Notification");
		explicitlyWaitForElement("table_Action_Workspace_NotificationItems_FirstRow",
				getTotalWaitTimeInSecs());*/
		//verifyTextContains("Low priority Action has been allocated to you", "table_Action_Workspace_NotificationItems_FirstRow");
		//navigate back to configure action team
		navigateTo(CONFIGURE_ACTION_TEAM_URL);
		explicitlyWaitForElement("text_Action_Configure_ActionTeam_EnquiryTeamMember",getTotalWaitTimeInSecs());
		//create a new officer and delete it
		click("icon_Action_Configure_ActionTeam_EnquiryTeam_CreateNewTeamMember");
		verifyElementPresent("text_Action_Configure_ActionTeam_EnquiryTeamList_CreateOfficer");
		sendKeys("textField_Action_Configure_ActionTeam_Surname", CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_SURNAMETODELETE);
		sendKeys("textField_Action_Configure_ActionTeam_Forename", CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_FORENAMETODELETE);
		//click on create
		clickAndWait("button_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer_Create");
		sendKeysAndEnter("textField_Action_Configure_ActionTeam_EnquiryTeamMemberList_Surname", CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_SURNAMETODELETE);
		//select first record
		explicitlyWaitForElement("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord",
				getTotalWaitTimeInSecs());
		clickAndWait("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord");
		//click on remove icon it should remove the officer
		clickAndWait("icon_Action_Configure_ActionTeam_EnquiryTeam_Remove");
		//enter existing officer name and hit enter
		waitForJQuery(driver);
		sendKeysAndEnter("textField_Action_Configure_ActionTeam_EnquiryTeamMemberList_Surname", " ");
		sendKeysAndEnter("textField_Action_Configure_ActionTeam_EnquiryTeamMemberList_Surname", CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_EXISTING_SURNAME);
		explicitlyWaitForElement("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord",
				getTotalWaitTimeInSecs());
		waitForJQuery(driver);
		//select the officer
		clickAndWait("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord");
		//click on remove icon
		clickAndWait("icon_Action_Configure_ActionTeam_EnquiryTeam_Remove");
		explicitlyWaitForElement("errorMsg_Action_Configure_ActionTeam_MasterEnquiryTeam_Remove",
				getTotalWaitTimeInSecs());
		verifyElementPresent("errorMsg_Action_Configure_ActionTeam_MasterEnquiryTeam_Remove");
		clickAndWait("errorMsg_Action_Configure_ActionTeam_MasterEnquiryTeam_Remove_Ok");
		sendKeysAndEnterWithDriver("textField_Action_Configure_ActionTeam_EnquiryTeamMemberList_Surname", " ");
		
		//select the records which are active
		selectByVisibleText("dropDown_Action_Configure_ActionTeam_EnquiryTeam_Active","Active");
		waitForJQuery(driver);
		sendKeysAndEnter("textField_Action_Configure_ActionTeam_EnquiryTeamMemberList_Surname", CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_SURNAME);
		explicitlyWaitForElement("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord",
				getTotalWaitTimeInSecs());
		clickAndWait("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord");
		//click on deactivate button
		clickAndWait("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Deactivate");
		//select the same record and click on activate
		selectByVisibleText("dropDown_Action_Configure_ActionTeam_EnquiryTeam_Active","In-Active");
		sendKeysAndEnter("textField_Action_Configure_ActionTeam_EnquiryTeamMemberList_Surname", " ");
		sendKeysAndEnter("textField_Action_Configure_ActionTeam_EnquiryTeamMemberList_Surname", CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_SURNAME);
		explicitlyWaitForElement("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord",
				getTotalWaitTimeInSecs());
		clickAndWait("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord");
		//click on activate button
		clickAndWait("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Activate");
		
		//sendKeysAndEnterWithDriver("textField_Action_Configure_ActionTeam_EnquiryTeamMemberList_Surname", " ");
		selectByVisibleText("dropDown_Action_Configure_ActionTeam_EnquiryTeam_Active","Active");
		
		//without selecting any record click on activate and de activate
		/*explicitlyWaitForElement("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Deactivate",
				getTotalWaitTimeInSecs());
		clickAndWait("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Deactivate");
		waitForAlertTextAndClose("Please select at least one record");*/
		clickAndAcceptAlert("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Deactivate");
		clickAndAcceptAlert("icon_Action_Configure_ActionTeam_EnquiryTeam_Remove");
		clickAndAcceptAlert("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Activate");
		
		//create an existing officer again multiple times
		click("icon_Action_Configure_ActionTeam_EnquiryTeam_CreateNewTeamMember");
		sendKeys("textField_Action_Configure_ActionTeam_Surname", CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_SURNAME);
		sendKeys("textField_Action_Configure_ActionTeam_Forename", CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_FORENAME);
		//click on create
		click("button_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer_Create");
		explicitlyWaitForElement("errorMsg_Action_Configure_ActionTeam_MasterEnquiryTeam_ExistingOfficer",
				getTotalWaitTimeInSecs());
		verifyElementPresent("errorMsg_Action_Configure_ActionTeam_MasterEnquiryTeam_ExistingOfficer");
		click("button_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer_Cancel");
		
		//sorting on surname and forename
		verifySortTableColumn("icon_Action_Configure_ActionTeam_EnquiryTeam_SurnameSort");
		verifySortTableColumn("icon_Action_Configure_ActionTeam_EnquiryTeam_ForenameSort");
		//filtering on surname, forename
		sendKeysAndEnter("textField_Action_Configure_ActionTeam_EnquiryTeamMemberList_Surname", CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_SURNAME);
		verifyElementPresent("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord");
		sendKeysAndEnter("textField_Action_Configure_ActionTeam_EnquiryTeamMemberList_Forename", CONFIGURE_ACTIONTEAM_ENQUIRYTEAM_FORENAME);
		verifyElementPresent("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord");
		endLine(method);
		

	}


	//Test case 30750:002. Master Enquiry team Maintenance 

  @Test(priority = 2)
	public void masterEnquiryTeamMaintenance(Method method) throws Exception {
		startLine(method);
		//click on configure action team link
		navigateTo(CONFIGURE_ACTION_TEAM_URL);
		explicitlyWaitForElement("text_Action_Configure_ActionTeam_EnquiryTeamMember",getTotalWaitTimeInSecs());
		verifyElementPresent("text_Action_Configure_ActionTeam_EnquiryTeamMember");
		explicitlyWaitForElement("link_Action_Configure_ActionTeam_MasterEnquiryTeamMaintenance",
				getTotalWaitTimeInSecs());
		//click on master enquiry team maintenance link
		click("link_Action_Configure_ActionTeam_MasterEnquiryTeamMaintenance");
		explicitlyWaitForElement("text_Action_Configure_ActionTeam_MasterEnquiryTeamList",
				getTotalWaitTimeInSecs());
		verifyElementPresent("text_Action_Configure_ActionTeam_MasterEnquiryTeamList");
		//click on create new officer icon
		click("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer");
		verifyElementPresent("text_Action_Configure_ActionTeam_MasterEnquiryTeamList_CreateOfficer");
		verifyElementPresent("textField_Action_Configure_ActionTeam_Surname");
		verifyElementPresent("textField_Action_Configure_ActionTeam_Forename");
		verifyElementPresent("dropDown_Action_Configure_ActionTeam_MasterEnquiryTeam_Rank");
		verifyElementPresent("textField_Action_Configure_ActionTeam_MasterEnquiryTeam_OfficerNumber");
		verifyElementPresent("textField_Action_Configure_ActionTeam_MasterEnquiryTeam_UsualPartner");
		verifyElementPresent("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Active");
		//click on create button
		click("button_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer_Create");
		//verify error message
		explicitlyWaitForElement("errorMsg_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer",
				getTotalWaitTimeInSecs());
		verifyElementPresent("errorMsg_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer");
		//enter surname and forename
		sendKeys("textField_Action_Configure_ActionTeam_Surname", CONFIGURE_ACTIONTEAM_MASTERENQUIRY_SURNAME);
		sendKeys("textField_Action_Configure_ActionTeam_Forename", CONFIGURE_ACTIONTEAM_MASTERENQUIRY_FORENAME);
		//click on create
		clickAndWait("button_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer_Create");
		//sendkeys and hit enter in surname
		waitForJQuery(driver);
		sendKeysAndEnter("textField_Action_Configure_ActionTeam_MasterEnquiryTeamList_Surname", CONFIGURE_ACTIONTEAM_EXISTING_SURNAME);
		explicitlyWaitForElement("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord",
				getTotalWaitTimeInSecs());
		clickAndWait("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord");
		click("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Remove");
		explicitlyWaitForElement("label_Marker_Delete_Popup",
				getTotalWaitTimeInSecs());
		verifyElementPresent("label_Marker_Delete_Popup");
		click("button_Profile_ErrorPopup_OK");
		//sendkeys and hit enter in surname
		waitForJQuery(driver);
		sendKeysAndEnter("textField_Action_Configure_ActionTeam_MasterEnquiryTeamList_Surname", CONFIGURE_ACTIONTEAM_MASTERENQUIRY_SURNAME);
		explicitlyWaitForElement("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord",
				getTotalWaitTimeInSecs());
		clickAndWait("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord");
		//remove the first record
		click("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Remove");
		
		//create a new officer
		click("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer");
		sendKeys("textField_Action_Configure_ActionTeam_Surname", CONFIGURE_ACTIONTEAM_ACTIVE_SURNAME);
		sendKeys("textField_Action_Configure_ActionTeam_Forename", CONFIGURE_ACTIONTEAM_ACTIVE_FORENAME);
		//click on create
		clickAndWait("button_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer_Create");
		
		//select text by visible as active
		selectByVisibleText("dropDown_Action_Configure_ActionTeam_MasterEnquiryTeam_Active","Active");
		waitForJQuery(driver);
		sendKeysAndEnter("textField_Action_Configure_ActionTeam_MasterEnquiryTeamList_Surname", CONFIGURE_ACTIONTEAM_ACTIVE_SURNAME);
		//sendKeysAndEnter("textField_Action_Configure_ActionTeam_MasterEnquiryTeamList_Surname", " ");
		explicitlyWaitForElement("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord",
				getTotalWaitTimeInSecs());
		clickAndWait("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord");
		//click on deactivate button
		click("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Deactivate");
		selectByVisibleText("dropDown_Action_Configure_ActionTeam_MasterEnquiryTeam_Active","In-Active");
		sendKeysAndEnter("textField_Action_Configure_ActionTeam_MasterEnquiryTeamList_Surname", " ");
		sendKeysAndEnter("textField_Action_Configure_ActionTeam_MasterEnquiryTeamList_Surname", CONFIGURE_ACTIONTEAM_ACTIVE_SURNAME);
		explicitlyWaitForElement("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord",
				getTotalWaitTimeInSecs());
		verifyElementPresent("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord");

		clickAndWait("checkBox_Action_Configure_ActionTeam_MasterEnquiryTeam_Firstrecord");
		click("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Activate");

		click("link_Action_Configure_ActionTeam_EnquiryTeamMaintenance");
		explicitlyWaitForElement("text_Action_Configure_ActionTeam_EnquiryTeamMember",getTotalWaitTimeInSecs());
		verifyElementPresent("text_Action_Configure_ActionTeam_EnquiryTeamMember");
		click("link_Action_Configure_ActionTeam_MasterEnquiryTeamMaintenance");
		explicitlyWaitForElement("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Activate",
				getTotalWaitTimeInSecs());

		//without select any record click on "ok" in pop up window
     	clickAndAcceptAlert("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Activate");
		/*explicitlyWaitForElement("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Activate",
				getTotalWaitTimeInSecs());
		clickAndWait("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Activate");
		waitForAlertTextAndClose("Please select at least one record");*/
        
		clickAndAcceptAlert("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Deactivate");
		//clickAndWait("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Deactivate");
		//waitForAlertTextAndClose("Please select at least one record");
        
		clickAndAcceptAlert("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Remove");
		//clickAndWait("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_Remove");
		//waitForAlertTextAndClose("Please select at least one record");
		//create existing officer
		explicitlyWaitForElement("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer",
				getTotalWaitTimeInSecs());
		click("icon_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer");
		sendKeys("textField_Action_Configure_ActionTeam_Surname", CONFIGURE_ACTIONTEAM_EXISTING_SURNAME);
		sendKeys("textField_Action_Configure_ActionTeam_Forename", CONFIGURE_ACTIONTEAM_EXISTING_FORENAME);
		click("button_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer_Create");
		verifyElementPresent("errorMsg_Action_Configure_ActionTeam_MasterEnquiryTeam_ExistingOfficer");
		click("button_Action_Configure_ActionTeam_MasterEnquiryTeam_CreateNewOfficer_Cancel");
		endLine(method);

	}

	//Test case 30751:003. Audit Log Entry- Action Team 

   @Test(priority = 3)
	public void actionTeam_AuditLogEntry(Method method) throws Exception {
		startLine(method);

		AuditLogSearch au1 = new AuditLogSearch(driver);
		// search audit and verify result
		au1.auditLogSearchByActivity(ATTACH_ENQUIRY_OFFICER);
		verifyAuditResult(ATTACH_ENQUIRY_OFFICER);

		au1.auditLogSearchByActivity(CREATE_ENQUIRY_OFFICER);
		verifyAuditResult(CREATE_ENQUIRY_OFFICER);

		au1.auditLogSearchByActivity(DEACTIVATE_ENQUIRY_OFFICER);
		verifyAuditResult(DEACTIVATE_ENQUIRY_OFFICER);

		au1.auditLogSearchByActivity(ACTIVATE_ENQUIRY_OFFICER);
		verifyAuditResult(ACTIVATE_ENQUIRY_OFFICER);

		endLine(method);
	}

	public void verifyAuditResult(String activityDesc) throws Exception {
		verifyText(usernameUpperCase,
				"table_AuditLog_AuditRecord_FirstRecord_Username");
		verifyTextContains(incidentIdFull,
				"table_AuditLog_AuditRecord_FirstRecord_Incident");
		verifyTextContains(activityDesc,
				"table_AuditLog_AuditRecord_FirstRecord_Activity");
	}
	
	@AfterTest
	public void afterTest() {
		driver.quit();
	}
}
