package Modules.Actions;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Modules.Entity.Entity;
import ReusableUtilities.Report;
import ReusableUtilities.SeleniumBaseClass;

public class Actions extends Entity{
	protected static final String ACTION_ASSOCIATEDOCUMENT_DIALOGBOXTITLE = "Select Record";
	protected static final String ACTION_DOCUMENTTAKEN_DIALOGBOXTITLE = "Select Record";
	public static String SEARCH_FOR_AUDITS= "/group/holmes/search-for-audits";
	
	
	protected static final String VIEW_ACTION_TEXT = "View Action";
	public static final String RAISE_MIRSAP_ACTION_URL = "/group/holmes/raise-action";
	public static final String ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE = "Action - Auto Test for Raise Action update";
	public static final String ACTION_ORIGINATING_DETAIL = "Originating Desc";
	public static final String TINY_MCE_TEXT_FOR_SINGLE_QUEUE_MOVE= "TinyMCEText-Action - Auto Test for single Queue Move";
	public static final String TEXTFIELD_TEXT_FOR_SINGLE_QUEUE_MOVE= "TextFieldText-Action - Auto Test for single Queue Move";
	
	protected static final String DISCLOSURE_WINDOW_TITLE = "Disclosure";
	protected static final String LIST_MAINTENANCE_WINDOW_TITLE = "List Maintenance - Liferay";
	protected static final String NEW_OR_EXISTING_LIST_DIALOG_CONTENT = "List Management has been opened with the selected records";
	protected static final String DEFAULT_LIST_DIALOG_CONTENT = "The selected records have been added to your Default list";
	protected static final String FAST_ACTION_WINDOW_TITLE = "Fast Action";
	protected static final String VISUALISATION_PORTLET_WINDOW_TITLE = "Visualisation Portlet";
	protected static final String CREATE_OFFICER_NAME_MAX_CHAR = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	protected static final String ORIGINATING_DETAILS_MAX_CHAR_SIZE = "70";
	protected static final String FAST_ACTION_TITLE_MAX_CHAR_SIZE = "70";
	protected static final String IS_MARKED_AS_READ_ONLY_TEXT = "is marked as read only";
	protected static final String NOT_A_VALID_FAST_ACTION_TEXT = "not a valid Fast Action";
	protected static final String IS_NOT_A_VALID_URN_TEXT = "is not a valid URN";
	protected static final String ASSOCIATED_DOCUMENT_INVALID_URN = "$1";
	protected static final String ASSOCIATED_ACTION_INVALID_URN = "&1";
	protected static final String CREATE_OFFICER_VALIDATION_MESS = "Please provide a Surname\nPlease provide a Forename";
	protected static final String CREATE_OFFICER_ALPHANUMERIC_VALIDATION_MESS ="Surname must contain only alphabetic chars and hyphens\nForename must contain only alphabetic chars and hyphens";
	protected static final String CREATE_OFFICER_MAX_CHAR_COUNT_VALIDATION_MESS = "Surname must not be more than 30 characters\nForename must not be more than 30 characters\nOnly 7 characters are allowed for Officer Number";
	protected static final String TITLE_VALIDATION_MESS = "Title is required";
	protected static final String PRIORITY_VALIDATION_MESS = "Priority is required";
	protected static final String RAISE_ACTION_WITHOUT_INCIDENT_VALIDATION_MESS = "Incident not found";
	protected static final String RAISE_ACTION_WITHOUT_MANDATORY_VALUES_VALIDATION_MESS = "There are validation errors";
	protected static final String SELECT_GROUP = "Select Group";
	
	protected static final String CREATE_OFFICER_SURNAME="SurName";
	protected static final String CREATE_OFFICER_FORENAME="ForeName";
	
	protected static final String MIRSAP_FORCE="ARMY";
	protected static final String MIRSAP_CLASS="SCENE";
	protected static final String MIRSAP_PRIORITY="Medium";
	protected static final String MIRSAP_STATION="HEADQUARTERS";
	

	public boolean verifyAssociationTabEnabled(){
		return verifyTabActive("tab_Association");
	}
	
	/*
	 * URN pattern matching
	 */
	
	public void validateMirsapActionURNPattern(String URN) {
		String pattern = "^A\\d+";
		Pattern patternObj = Pattern.compile(pattern);
		Matcher matcherObj = patternObj.matcher(URN);
		
		int count = 0;
		
		while(matcherObj.find()){
			count++;
		}
		
		if (count == 1) {
			Report.pass("Matching with the Patern");
		} else {
			Report.warn("NOT matching with the pattern, as noticed URN is: " + URN);
		}
	}
	
	
	
	public String getTextFieldText() throws Exception{
		return getTinyMCEText("tinyMCE_Action_Create_Text");
	}
	public String getTextandResultFieldText() throws Exception{
		return getTinyMCEText("tinyMCE_Action_View_TextorResultText");
	}
	public String getAllTextFieldText()throws Exception{
		return getTinyMCEText("tinyMCE_Action_View_AllText");
	}

	public void expandDisclosureSection(){
		expandSection("section_Action_View_Disclosure");
	}
	
	public void collapseDisclosureSection(){
		collapseSection("section_Action_View_Disclosure");
	}
	
	public void expandThemesSection(){
		expandSection("section_Action_View_Themes");
	}
	
	public void collapseThemesSection(){
		collapseSection("section_Action_View_Themes");
	}
	
	public void verifyIsInViewActionPage() throws Exception {
		verifyText(VIEW_ACTION_TEXT, "label_Action_View_Section_Title");
	}
	
	
	public void expandMigrationLog() {
		expandSection("section_Action_Create_MigrationLog");
	}
	
	public void collapseMigrationLog() {
		collapseSection("section_Action_Create_MigrationLog");
	}
	
	public void expandLinkedActionOrAssociatedDocument() {
		expandSection("section_Action_Create_LinkedAssociation");
	}
	
	public void collapseLinkedActionOrAssociatedDocument() {
		collapseSection("section_Action_Create_LinkedAssociation");
	}
	
	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}
	
	public void notificationCheck() throws Exception {

		// Log Out Method
		signOut();

		// Relogin as allocated officer
		reLoginasAllocatedOfficer();
		waitForJQuery(driver);
		waitForPageLoad();

		explicitlyWaitForElement("icon_Home_Workspace_Notification",
				getTotalWaitTimeInSecs());

		// Notification checking
		clickAndWait("icon_Home_Workspace_Notification");
		// TODO- Liyakath( Notification click not working)
		explicitlyWaitForElement(
				"table_Action_Workspace_NotificationItems_FirstRow",
				getTotalWaitTimeInSecs());
		verifyTextContains("Medium priority Action has been allocated to you",
				"table_Action_Workspace_NotificationItems_FirstRow");
		// validateAllVerificationPoints();
		// endLine(method);

		// check Notification and sign out
		signOut();
		driver.close();
		// Relogin as System Admin
		username = getUserName();
		loginInIEAndSelectIncident();

	}
	
	public void signOut()throws Exception{
		 click("icon_Home_MyAccount");
		 click("button_Home_SignOut");

	 }
	
	public void reLoginasAllocatedOfficer() throws Exception {
		// Relogin as allocated officer- surName
		handleSecurityWindow1();
		explicitlyWaitForElement("textField_Login_UserName",
				getTotalWaitTimeInSecs());
		username = "surname";
		sendKeys("textField_Login_UserName", username);
		sendKeys("textField_Login_PassWord", password);
		click("button_Login_SignIn");
		waitForPageLoad();
		selectAGroupInHomePage();
	}
	
	
	
	/*	
	public void expandSection(){
		
	}
	
	public void collapseSection(){
		
	}*/

	
}
