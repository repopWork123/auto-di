package Modules.Actions;

import java.lang.reflect.Method;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.SeleniumBaseClass;

public class RaiseMirsapAction extends SeleniumBaseClass {

	private static final String RAISE_MIRSAP_ACTION_URL = "/group/holmes/raise-action";
	private static final String ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE = "Action - Auto Test for Raise Action update";
	private static final String ACTION_ORIGINATING_DETAIL = "Originating Desc";

	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);

	}

	@BeforeTest
	public void beforeTest() throws Exception {
		loginInIEAndSelectIncident();

	}

	@Test(priority = 1)
	public void createMirsapAction_WithoutMandatoryDetails(Method method)
			throws Exception {
		startLine(method);
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		ClickAndWait("button_Action_Raise");
		explicitlyWaitForElement("errorMsg_Action_Create_WithOutAnyFields",
				getTotalWaitTimeInSecs());
		VerifyElementPresent("errorMsg_Action_Create_WithOutAnyFields");
		VerifyElementPresent("label_Action_Title");
		VerifyElementPresent("label_Action_Force");
		VerifyElementPresent("label_Action_Station");
		VerifyElementPresent("label_Action_Class");
		VerifyElementPresent("label_Action_Subject");
		endLine(method);
	}

	 @Test(priority = 2)
	public void createMirsapAction_WithAllFields(Method method)
			throws Exception {
		startLine(method);
		navigateTo(RAISE_MIRSAP_ACTION_URL);
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		VerifyElementPresent("button_Action_Raise");
		String incidentId="MORSTA16G28";
		VerifyTextContains(incidentId, "dropDown_Action_Create_Incident");
		SendKeys("textField_Action_Mirsap_Title",
				ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
		insertIntoAllTinyMCEinPage();
		SelectTextByIndex("dropDown_Action_Mirsap_Force", 2);
		SelectTextByIndex("dropDown_Action_Mirsap_Class", 2);
		SelectTextByIndex("dropDown_Action_Mirsap_Priority", 2);
		SelectTextByIndex("dropDown_Action_Mirsap_Station", 1);
		SendKeys("textField_Action_Mirsap_OriginatingDetails",
				ACTION_ORIGINATING_DETAIL);
		// Click("textField_Action_Mirsap_SourceDocument");
		// SendKeys("textField_Action_Mirsap_Urn", ACTION_DOCUMENT_URN);
		Click("textField_Action_Mirsap_DefaultList");
		Click("link_Action_Mirsap_List_CheckAll");
		
		Click("link_Action_Create_LinkedAssociation");
		ClickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		//Click("textField_Action_Link");
		//Click("link_Action_Assoc_link");
		//Click("textField_Action_Mirsap_URN");
		//Click("link_Action_Mirsap_Assoc");
		//explicitlyWaitForElement("textField_Action_Mirsap_URN",
		//		getTotalWaitTimeInSecs());
		
		
		
		//createIncidentSelect
		//div[contains(text(),'Linked Actions')]
		//div[contains(text(),'Themes')]
		//(//div[contains(text(),'Originating Details')])[1]
		VerifyElementPresent("successMsg_Action_Create");
		endLine(method);
	}
}
