package Modules.Actions;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.Report;
import ReusableUtilities.SeleniumBaseClass;

public class FastAction extends SeleniumBaseClass {

	private static final String FAST_ACTION_TEXT = "Fast action";
	private static final String ACTION_AUTO_TEST_FOR_RAISE_ACTION_UPDATE = "Action - Auto Test for Raise Action update";
	private static final String NO_OF_BULK_RECORD = "5";
	private static final String ACTION_AUTO_TEST_FOR_RAISE_ACTION = "Action - Auto Test for Raise Action";
	private String actionUrnToView = "A1";
	String RAISE_FASTACTION_URL = "/group/holmes/raise-fast-action";
	String SEARCH_MIRSAPACTION_URL = "/group/holmes/search-for-actions";
	String VIEWACTION_URL = "/group/holmes/view-action";
	String ACTIONTEAM_URL = "/group/holmes/action-team";

	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);

	}
	
/*	public void swichToFrameAndEnterText(String frameName,String Field) {
		driver.switchTo().frame("edit-2513-htmlText_ifr");
		driver.findElement(By.id("tinymce")).sendKeys(Field);
		driver.switchTo().defaultContent();

	}*/

	@BeforeTest
	public void beforeTest() throws Exception {
//		loginAndSelectIncident();
		 loginInIEAndSelectIncident();
	}

	 @Test (priority = 1)
	public void CreateFastAction_WithoutMandatoryDetails(Method method) throws Exception {
		startLine(method);
		navigateTo(RAISE_FASTACTION_URL);
		ClickAndWait("button_Action_Raise");
		explicitlyWaitForElement("label_Action_Title", getTotalWaitTimeInSecs());
		VerifyElementPresent("label_Action_Title");
		endLine(method);
	}

	 @Test (priority = 2)
	public void CreateFastAction(Method method) throws Exception {
		startLine(method);
		navigateTo(RAISE_FASTACTION_URL);
		SendKeys("textField_Action_ActionText",
				ACTION_AUTO_TEST_FOR_RAISE_ACTION);


		ClickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		VerifyElementPresent("successMsg_Action_Create");
		endLine(method);
	}

	 @Test (priority = 3)
	public void SearchAction(Method method) throws Exception {
		startLine(method);
		navigateTo(SEARCH_MIRSAPACTION_URL);
		ClickAndWait("button_Action_Search");
		endLine(method);
	}

	 @Test (priority = 4)
	public void ViewAction(Method method) throws Exception {
		startLine(method);
		navigateTo(VIEWACTION_URL);
		SendKeys("textField_Action_ViewUrn", actionUrnToView);
		Click("button_Action_View");
		waitForPageLoad();
		endLine(method);

	}

	 @Test (priority = 5)
	public void CreateFastAction_WithBulkRecords(Method method) throws Exception {
		 startLine(method);
		navigateTo(RAISE_FASTACTION_URL);
		SendKeys("textField_Action_ActionText",
				ACTION_AUTO_TEST_FOR_RAISE_ACTION);
		ClickAndWait("checkBox_Action_BulkRecord");

		SendKeys("textField_Action_BulkRecordCount", NO_OF_BULK_RECORD);
		ClickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		VerifyElementPresent("successMsg_Action_Create");
		endLine(method);

	}

	 @Test (priority = 6)
	public void UpdateAction(Method method) throws Exception {
		startLine(method);
		navigateTo(RAISE_FASTACTION_URL);
		SendKeys("textField_Action_ActionText",
				ACTION_AUTO_TEST_FOR_RAISE_ACTION);
		ClickAndWait("button_Action_Raise");
		ClickAndWait("button_Action_Update");
		explicitlyWaitForElement("textField_Action_ActionText", getTotalWaitTimeInSecs());
		SendKeys("textField_Action_ActionText",
				ACTION_AUTO_TEST_FOR_RAISE_ACTION_UPDATE);
		ClickAndWait("button_Action_Save");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		VerifyElementPresent("successMsg_Action_Create");
		endLine(method);

	}

	 @Test(priority = 7)
	public void Create_andSearchFastAction_WithTitle(Method method) throws Exception {
		startLine(method);
		navigateTo(RAISE_FASTACTION_URL);
		SendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		ClickAndWait("button_Action_Raise");
		navigateTo(SEARCH_MIRSAPACTION_URL);
		SendKeys("textField_Action_ActionText", FAST_ACTION_TEXT);
		ClickAndWait("button_Action_Search");
		endLine(method);

	}

	 @Test (priority = 8)
	public void CreateFastAction_Similar(Method method) throws Exception {
		startLine(method);
		navigateTo(RAISE_FASTACTION_URL);
		SendKeys("textField_Action_ActionText",
				ACTION_AUTO_TEST_FOR_RAISE_ACTION);
		ClickAndWait("button_Action_Raise");
		ClickAndWait("button_Action_Similar");
		ClickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		VerifyElementPresent("successMsg_Action_Create");
		endLine(method);

	}

	@Test (priority = 9)
	public void CreateOfficer(Method method) throws Exception {
		startLine(method);
		navigateTo(ACTIONTEAM_URL);
		ClickAndWait("button_Action_CreateOfficer");
		explicitlyWaitForElement("textField_Action_Configure_ActionTeam_Surname", getTotalWaitTimeInSecs());
		SendKeys("textField_Action_Configure_ActionTeam_Surname", "Surnames");
		SendKeys("textField_Action_Configure_ActionTeam_Forename", "Forenames");
		ClickAndWait("button_Action_CreateOfficerSave");
		endLine(method);

	}

	@Test(priority = 10, dependsOnMethods = {"CreateOfficer"})
	public void FastAction_QUEU_Allocated(Method method) throws Exception {
		startLine(method);
		navigateTo(RAISE_FASTACTION_URL);
		SendKeys("textField_Action_ActionText",
				ACTION_AUTO_TEST_FOR_RAISE_ACTION);
		ClickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create", getTotalWaitTimeInSecs());
		VerifyElementPresent("successMsg_Action_Create");
		Click("checkBox_Action_Allocated");
		ClickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("textfield_Action_FastAction_QueueAllocatedOfficer", getTotalWaitTimeInSecs());
		SendKeys("textfield_Action_FastAction_QueueAllocatedOfficer", "Surnames");
		explicitlyWaitForElement("textfield_Action_FastAction_QueueAllocatedOfficerName", getTotalWaitTimeInSecs());
		ClickAndWait("textfield_Action_FastAction_QueueAllocatedOfficerName");
			
		explicitlyWaitAndGetWebElement("button_Action_ChangeState_Save", getTotalWaitTimeInSecs());
		ClickAndWait("button_Action_ChangeState_Save");
		endLine(method);
	}

	@Test(priority = 11, dependsOnMethods = {"FastAction_QUEU_Allocated"})
	public void FastAction_QUEU_Submitted(Method method) throws Exception {
		startLine(method);
		explicitlyWaitForElement("checkBox_Action_Submitted", getTotalWaitTimeInSecs());
		Click("checkBox_Action_Submitted");
		ClickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("text_Action_FastAction_QueueSubmittedSubmissionVerification", getTotalWaitTimeInSecs());
		insertIntoAllTinyMCEinPage();
		ClickAndWait("button_Action_ChangeState_Save");
		endLine(method);

	}

	 @Test(priority = 12, dependsOnMethods = {"FastAction_QUEU_Submitted"})
	public void FastAction_QUEU_Received(Method method) throws Exception {
		startLine(method);
		explicitlyWaitForElement("checkBox_Action_Received", getTotalWaitTimeInSecs());
		Click("checkBox_Action_Received");
		ClickAndWait("button_Action_ChangeState");
		ClickAndWait("button_Action_ChangeState_Save");
		endLine(method);

	}

	 @Test(priority = 13, dependsOnMethods = {"FastAction_QUEU_Received"})
	public void FastAction_QUEU_Resulted(Method method) throws Exception {
		startLine(method);
		explicitlyWaitForElement("checkBox_Action_Resulted", getTotalWaitTimeInSecs());
		Click("checkBox_Action_Resulted");
		ClickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("text_Action_FastAction_QueueSubmittedResultedVerification", getTotalWaitTimeInSecs());
		insertIntoAllTinyMCEinPage();
		ClickAndWait("button_Action_ChangeState_Save");
		endLine(method);

	}

	 @Test(priority = 14, dependsOnMethods = {"FastAction_QUEU_Resulted"})
	public void FastAction_QUEU_Filed(Method method) throws Exception {
		startLine(method);
		explicitlyWaitForElement("checkBox_Action_Filed", getTotalWaitTimeInSecs());
		Click("checkBox_Action_Filed");
		ClickAndWait("button_Action_ChangeState");
		explicitlyWaitForElement("text_Action_FastAction_QueueSubmittedFiledVerification", getTotalWaitTimeInSecs());
		insertIntoAllTinyMCEinPage();
		ClickAndWait("button_Action_ChangeState_Save");
		endLine(method);
	}

}
