package Modules.Messaging;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.SeleniumBaseClass;

	

	public class MessagingSuite extends SeleniumBaseClass {
		
	private static final String CREATE_MESSAGE_URL = "/group/holmes/create-message";
	private static final String MESSAGE_INBOX = "/group/holmes/inbox";

	@BeforeTest	
	public void beforeTest() throws Exception
	{
		loginInIEAndSelectIncident();
	}
	
	@Test (priority=1)
	public void verifyForMandatoryFields(Method method)throws Exception
	{	Thread.sleep(10000);
		startLine(method);
/*		WebElement internalMessaging = driver.findElement(By.xpath("//a[contains(.,'Internal Messaging')]"));
		mouseHoverJScript(internalMessaging);
		WebElement messages = driver.findElement(By.xpath("//a[contains(.,'Messages')]"));
		mouseHoverJScript(messages);
		
		WebElement createMessage = driver.findElement(By.xpath("//a[contains(.,'Create Message')]"));
		mouseHoverJScript(createMessage);
		clickElementUsingJS(createMessage);*/
/*		mouseHoverJScript("link_Messaging");
		mouseHoverJScript("link_Messaging_Messages");
		mouseHoverJScript("link_Messaging_Messages_CreateMessag");
		clickElementUsingJS("link_Messaging_Messages_CreateMessag");*/
		
		
		
//		createMessage.click();
		//sleep(4000);
//		ClickMenu("link_Messaging");
		
		//sleep(4000);
//		ClickMenu("link_Messaging_Messages");
		//sleep(4000);
//		hoverAndClick(new String[]{"link_Messaging","link_Messaging_Messages"},"link_Messaging_Messages_CreateMessag");
//		navigateToCreateMessage();
//		hoverAndClick1("link_Messaging","link_Messaging_Messages","link_Messaging_Messages_CreateMessag");
//		explictlyWaitandClickonHyperlink("link_Messaging_Messages",getTotalWaitTimeInSecs());
		//sleep(4000);
		click("button_Messaging_Send");
		waitForPageLoad();
		verifyText("Field Priority is mandatory in Message", "text_Messaging_ErrorMsgForpriority");
		verifyText("Field Subject is mandatory in Message", "text_Messaging_ErrorMsgForsubject");
		verifyText("Field Recipients is mandatory in Message", "text_Messaging_ErrorMsgForrecipients");
		endLine(method);
	}
	private void navigateToCreateMessage() {
		navigateToPage(CREATE_MESSAGE_URL);
	}
	private void navigateToMessageInbox() {
		navigateToPage(MESSAGE_INBOX);
	}
	//@Test (priority=2)
	public void sendMessageToRecipients(Method method)throws Exception
	{	
		startLine(method);
		//sleep(4000);
//		ClickMenu("link_Messaging");
		//sleep(4000);
//		ClickMenu("link_Messaging_Messages");
//		sleep(4000);
//		ClickMenu("link_Messaging_Messages_CreateMessag");
/*		hoverAndClick(new String[]{"link_Messaging","link_Messaging_Messages"},"link_Messaging_Messages_CreateMessag");
		hoverAndClick("link_Messaging","link_Messaging_Messages");*/
		navigateToCreateMessage();
		//sleep(4000);
//		new Select(driver.findElement(By.id("protectiveMarkingCode"))).selectByIndex(1);
//		//sleep(4000);
		selectTextByIndex("radioButton_Messaging_Create_Priority", 2);
		/*new Select(driver.findElement(By.id("priority"))).selectByIndex(2);*/

		click("button_Messaging_User");
		sendKeys("textfield_Messaging_ViewAlert_UserId", "VARIJA");
		driver.findElement(By.id("gs_userId")).sendKeys(Keys.RETURN);
		//sleep(3000);
		explicitlyWaitForElement("checkBox_Messaging_UserDialog_FirstRecord", getTotalWaitTimeInSecs());
		click("checkBox_Messaging_UserDialog_FirstRecord");
		/*driver.findElement(By.xpath(".//*[@id='1']/td[1]/input")).click();
		//sleep(3000);
		driver.findElement(By.xpath(".//*[@id='1']/td[2]"));
		//sleep(3000);
*/		click("button_Messaging_User_Submits");
		//sleep(3000);
		sendKeys("text_Messaging_Subject", "Messaging sent for Auto Test");
		//sleep(4000);
		click("button_Messaging_Send");
		waitForPageLoad();
		endLine(method);
		
	}
	//@Test (priority=3)
	public void viewMessageInbox(Method method)throws Exception
	{	
		startLine(method);
/*		//sleep(4000);
		Click("link_Messaging");
		//sleep(4000);
		Click("link_Messaging_Messages");
		//sleep(4000);
		Click("link_Messaging_Messages_Inbox");
		//sleep(4000);
*/		
//		hoverAndClick(new String[]{"link_Messaging","link_Messaging_Messages"},"link_Messaging_Messages_Inbox");
		navigateToMessageInbox();
	    /*WebElement e = driver.findElement(By.xpath("(//*[@class='jstree-leaf']/a/span)[1]"));*/
	    click("link_Messaging_Messages_InboxTab");
	    //sleep(4000);
	    explicitlyWaitForElement("icon_Messaging_Inbox_Firstrow_ViewMessage", getTotalWaitTimeInSecs());
	    doubleClickAndWaitForBrowserPopup("Message", "icon_Messaging_Inbox_Firstrow_ViewMessage");
		//sleep(5000);
		click("button_Messages_ViewMessage_Close");
		getHandleToWindow("Inbox - Liferay");
		endLine(method);	
	}
	//@Test (priority=4)
	public void viewMessageSent(Method method)throws Exception
	{	
		startLine(method);
/*		//sleep(6000);
		Click("link_Messaging");
		//sleep(4000);
		Click("link_Messaging_Messages");
		//sleep(4000);
		Click("link_Messaging_Messages_Inbox");
		//sleep(4000);
*/		/*hoverAndClick(new String[]{"link_Messaging","link_Messaging_Messages"},"link_Messaging_Messages_Inbox");*/
		navigateToMessageInbox();
		click("link_Messaging_Messages_SentTab");
		//sleep(5000);
		explicitlyWaitForElement("icon_Messaging_Inbox_Firstrow_ViewMessage", getTotalWaitTimeInSecs());
		doubleClickAndWaitForBrowserPopup("Message", "icon_Messaging_Inbox_Firstrow_ViewMessage");
		/*doubleClick("icon_Messaging_Inbox_Firstrow_ViewMessage");	
		
		//sleep(5000);
		getHandleToWindow("Message");*/
		//sleep(5000);
		click("button_Messages_ViewMessage_Close");
		getHandleToWindow("Inbox - Liferay");
		endLine(method);
			
	}
	//@Test (priority=5)
	public void viewMessageFromToolbarMessages(Method method)throws Exception
	{	
		startLine(method);
		//sleep(6000);
		click("link_Home");
		//sleep(4000);
		click("link_Messaging_ToolBar_Messages");
		
		doubleClickAndWaitForBrowserPopup("Message", "icon_Messaging_StatusMessage_Firstrow_ViewMessage");
		/*doubleClick("icon_Messaging_StatusMessage_Firstrow_ViewMessage");
		//sleep(5000);
		getHandleToWindow("Message");*/
		//sleep(5000);
		click("button_Messages_ViewMessage_Close");
		getHandleToWindow("Inbox - Liferay");
		endLine(method);	
	}
	//@Test (priority=6)
	public void viewInboxFromToolbarMessages(Method method)throws Exception
	{	
		startLine(method);
		//sleep(6000);
		click("link_Home");
		//sleep(4000);
		click("link_Messaging_ToolBar_Messages");
		//sleep(4000);
		clickAndWaitForBrowserPopup("Message", "link_Messaging_ToolBar_Inbox");
		/*Click("link_Messaging_ToolBar_Inbox");
		//sleep(12000);
		getHandleToWindow("Message");*/
		//sleep(5000);
		driver.close();
		getHandleToWindow("Inbox - Liferay");
		endLine(method);
		
	}
	//@Test (priority=7)
	public void replyMessageToRecipients(Method method)throws Exception
	{
		startLine(method);
		/*//sleep(6000);
		Click("link_Messaging");
		//sleep(4000);
		Click("link_Messaging_Messages");
		//sleep(4000);
		Click("link_Messaging_Messages_Inbox");
		//sleep(4000);
*/	    navigateToMessageInbox();
		/*WebElement e = driver.findElement(By.xpath("//table[@id='messsageGrid']"));
	    
	    //sleep(4000);
	    Actions action = new Actions(driver);
		action.doubleClick(e.findElement(By.xpath("//tbody/tr[@id='1']"))).perform();
		//sleep(4000);
*/		explicitlyWaitForElement("text_Messaging_Inbox_ResultsTable", getTotalWaitTimeInSecs());
		doubleClickAndWaitForBrowserPopup("Message", "icon_Messaging_Inbox_Secondrow");
		getHandleToWindow("Message");
		//sleep(5000);		
		click("button_Messages_Inbox_Reply");
		//sleep(4000);
		getHandleToWindow("Message");
		//sleep(5000);
		sendKeys("textfield_Messageing_ReplyText", "Replying the text for the message");
		//sleep(5000);
		driver.close();
		getHandleToWindow("Inbox - Liferay");
		endLine(method);	
	}
	
	//@Test (priority=8)
	public void forwardMessageToRecipients(Method method)throws Exception
	{
		startLine(method);
		/*//sleep(6000);
		Click("link_Messaging");
		//sleep(4000);
		Click("link_Messaging_Messages");
		//sleep(4000);
		Click("link_Messaging_Messages_Inbox");
		//sleep(4000);
*/	    navigateToMessageInbox();
		/*WebElement e = driver.findElement(By.xpath("//table[@id='messsageGrid']"));*/
	    explicitlyWaitForElement("text_Messaging_Inbox_ResultsTable", getTotalWaitTimeInSecs());
	    //sleep(4000);
/*	    Actions action = new Actions(driver);
		action.doubleClick(e.findElement(By.xpath("//tbody/tr[@id='1']"))).perform();*/
		//sleep(4000);
	    doubleClickAndWaitForBrowserPopup("Message", "icon_Messaging_Inbox_Secondrow");
		getHandleToWindow("Message");
		//sleep(5000);
		
		click("button_Messages_Inbox_Forward");
		//sleep(4000);
		getHandleToWindow("Forward Message");
		//sleep(5000);
		sendKeys("textfield_Messageing_ReplyText", "Replying the text for the message");
		//sleep(5000);
		driver.close();
		getHandleToWindow("Inbox - Liferay");
		endLine(method);	
	}
	
	//@Test (priority=9)
	public void DeleteInboxMessage(Method method)throws Exception
	{	
		startLine(method);
		/*//sleep(6000);
		Click("link_Messaging");
		//sleep(4000);
		Click("link_Messaging_Messages");
		//sleep(4000);
		Click("link_Messaging_Messages_Inbox");
		//sleep(4000);
*/	    navigateToMessageInbox();
		explicitlyWaitForElement("checkbox_Messaging_InoxOrSentResultsTable_FirstRow", getTotalWaitTimeInSecs());
		click("checkbox_Messaging_InoxOrSentResultsTable_FirstRow");
		/*WebElement e = driver.findElement(By.xpath("//tr[@id='0']/td[1]/input"));*/
	    
	    //sleep(4000);
//	    Actions action = new Actions(driver);
//		action.doubleClick(e.findElement(By.xpath("//td[@id='deleteId']/div/span"))).perform();
	    click("icon_Messaging_Inbox_Delete");
		//sleep(4000);
	    explicityWaitForAnElementAndClick("button_Messages_Delete_Selected_Message_Ok", getTotalWaitTimeInSecs());
		explicitlyWaitForElement("text_Messaging_Delete_SuccessMsg", getTotalWaitTimeInSecs());
		verifyTextContains("deleted successfully", "text_Messaging_Delete_SuccessMsg");
		endLine(method);
			
	}
	//@Test (priority=10)
	public void DeleteSentMessage(Method method)throws Exception
	{	
		startLine(method);
/*		//sleep(6000);
		Click("link_Messaging");
		//sleep(4000);
		Click("link_Messaging_Messages");
		//sleep(4000);
		Click("link_Messaging_Messages_Inbox");
		//sleep(4000);
*/		navigateToMessageInbox();
		click("link_Messaging_Messages_SentTab");
		//sleep(5000);
		explicitlyWaitForElement("checkbox_Messaging_InoxOrSentResultsTable_FirstRow", getTotalWaitTimeInSecs());
		click("checkbox_Messaging_InoxOrSentResultsTable_FirstRow");
		click("icon_Messaging_Inbox_Delete");
	/*	WebElement e = driver.findElement(By.xpath("//tr[@id='0']/td[1]/input"));
		Actions action = new Actions(driver);
		action.click(driver.findElement(By.xpath("//td[@id='deleteId']/div/span"))).perform();		*/				
		explicityWaitForAnElementAndClick("button_Messages_Delete_Selected_Message_Ok", getTotalWaitTimeInSecs());
		explicitlyWaitForElement("text_Messaging_Delete_SuccessMsg", getTotalWaitTimeInSecs());
		verifyTextContains("deleted successfully", "text_Messaging_Delete_SuccessMsg");
		endLine(method);
	}
  
  
//	@AfterTest
	public void afterTest() throws Exception
	{
		closeApp();
	}

}
