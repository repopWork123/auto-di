package Modules.UserManagement;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.SeleniumBaseClass;

public class Teams extends SeleniumBaseClass {
	
	private static final String EXISTING_USER = "VARIJA";
	private static final String newTeamName = "newteam";
	private static final String USEMANAGEMET_ADMIN_URL = "/useradmin-ui/userAdmin.action";
	
	@BeforeTest
	public void beforeTest() throws Exception {
		// loginAndSelectIncident();
		loginInIEAndSelectIncident();
	}



	private void navigateToUseManagement() {
		navigateToPage(USEMANAGEMET_ADMIN_URL);
	}

	@Test(priority = 1)
	public void addTeam(Method method) throws Exception {
		startLine(method);
		navigateToUseManagement();
		click("link_Usermanagement_Teams");
		verifyElementPresent("textfield_UserManagement_Team_Search");
		sendKeys("textfield_UserManagement_Team_Search", newTeamName +getCurrentDataStampwithoutSlash());
		verifyElementPresent("checkBox_UserManagement_Team_Add");
		verifyElementPresent("button_UserManagement_Team_Search");
		verifyElementPresent("button_UserManagement_Team_Reset");
		verifyElementPresent("link_Usermanagement_Teams_Export");
		verifyElementPresent("link_Usermanagement_Teams_ExportWithUsers");
		verifyElementPresent("button_UserManagement_Team_Close");
		verifyElementPresent("button_UserManagement_Team_Help");
		click("checkBox_UserManagement_Team_Add");
		click("button_UserManagement_Team_Add");
		waitForAlertTextAndClose("Successfully");
		endLine(method);
	}

	@Test(priority = 2)
	public void searchTeams_WithoutEnterDetails(Method method) throws Exception {
		startLine(method);
		
		navigateToUseManagement();
		clickAndWait("link_Usermanagement_Teams");
		clickAndWait("button_UserManagement_Team_Search");
		
		endLine(method);
	}


	@Test(priority = 3)
	public void searchTeams_WithTeam(Method method) throws Exception {
		startLine(method);
		
		navigateToUseManagement();
		clickAndWait("link_Usermanagement_Teams");
		sendKeys("textfield_UserManagement_Team_Search", newTeamName);
		clickAndWait("button_UserManagement_Team_Search");
		
		endLine(method);
	}
	
	 @Test(priority = 4)
	public void assignUsersTeam(Method method) throws Exception {
		startLine(method);
		
		navigateToUseManagement();
		clickAndWait("link_Usermanagement_Teams");
		sendKeys("textfield_UseManagement_Team_Search", newTeamName);
		clickAndWait("button_UserManagement_Team_Search");
		click("checkbox_UserManagement_Team_FirstRecord");
		clickAndWait("button_UserManagement_Team_Assign");
		explicitlyWaitForElement("textfield_UseManagement_Team_UserId",
									getTotalWaitTimeInSecs());
		sendKeys("textfield_UseManagement_Team_UserId", EXISTING_USER);
		clickAndWait("button_UserManagement_Team_SearchUser");
		explicitlyWaitForElement("checkbox_UserManagement_Team_AssignUser_FirstRecord",
									getTotalWaitTimeInSecs());
		click("checkbox_UserManagement_Team_AssignUser_FirstRecord");
		clickAndWait("button_UserManagement_Team_AssignUser");
		click("checkBox_UserManagement_Team_AssignUser_SelectRole1");
		click("button_UserManagement_Team_AssignUser_ApplyDefaultsToAll");
		explicitlyWaitForElement("CheckBox_UserManagement_Team_AssignUser_SelectRole2",getTotalWaitTimeInSecs());
		click("checkBox_UserManagement_Team_AssignUser_SelectRole2");
		clickAndWait("button_UserManagement_Team_AssignUser_Submit");
		getHandleToWindow("Messages");
		getHandleToWindow("Messages").close();
		
		endLine(method);

	}


}
