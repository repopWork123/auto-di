package Modules.UserManagement;

import java.lang.reflect.Method;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.SeleniumBaseClass;

public class Users extends SeleniumBaseClass {
	
	private static final String USEMANAGEMET_ADMIN_URL = "/useradmin-ui/userAdmin.action";
	private static final String HOME_URL = "/home";
	private static final String SEARCH_USERID="Auto1";
	private static final String CREATE_USERID="USERID";
	private static final String CREATE_PASSWORD="Oracle0";
	private static final String CREATE_ADMIN_USERID="USERID1";
	
	
	@BeforeTest
	public void beforeTest() throws Exception {
		// loginAndSelectIncident();
		loginInIEAndSelectIncident();
	}

	private void navigateToUseManagement() {
		navigateToPage(USEMANAGEMET_ADMIN_URL);
	}
	private void navigateToHome() {
		navigateToPage(HOME_URL);
	}
	 @Test(priority = 1)
	public void searchUser_WithoutEnterDetails(Method method) throws Exception {
		startLine(method);
		navigateToUseManagement();
		explicitlyWaitForElement("link_Usermanagement_User", getTotalWaitTimeInSecs());
		verifyElementPresent("link_Usermanagement_User");
		click("link_Usermanagement_User");
		clickAndWait("button_UserManagement_User_Search");
		endLine(method);

	}

	 @Test(priority = 2)
	public void searchUser_WithEnterUser(Method method) throws Exception {
		startLine(method);
		navigateToUseManagement();
		explicitlyWaitForElement("link_Usermanagement_User", getTotalWaitTimeInSecs());
		verifyElementPresent("link_Usermanagement_User");
		click("link_Usermanagement_User");
		sendKeys("textfield_UserManagement_User_Search_UserId", SEARCH_USERID);
		clickAndWait("button_UserManagement_User_Search");
		endLine(method);

	}

	 //@Test(priority = 3)
	public void createUser_WithotMandatoryDetails(Method method) throws Exception {
		startLine(method);
		navigateToUseManagement();
		explicitlyWaitForElement("link_Usermanagement_User", getTotalWaitTimeInSecs());
		verifyElementPresent("link_Usermanagement_User");
		click("link_Usermanagement_User");
		clickAndWait("button_UserManagement_User_Create");
		getHandleToWindow("Create User");
		explicitlyWaitForElement("button_UserManagement_User_Create_Save", getTotalWaitTimeInSecs());
		verifyElementPresent("button_UserManagement_User_Create_Save");
		click("button_UserManagement_User_Create_Save");
		clickAndWait("button_UserManagement_User_Create_Cancel");
		getHandleToWindow("ULEAF");
		endLine(method);
	}

	@Test(priority = 4)
	public void create_NormalUser(Method method) throws Exception {
		startLine(method);
		navigateToHome();
		navigateToUseManagement();
		explicitlyWaitForElement("link_Usermanagement_User", getTotalWaitTimeInSecs());
		verifyElementPresent("link_Usermanagement_User");
		click("link_Usermanagement_User");
		clickAndWait("button_UserManagement_User_Create");
		getHandleToWindow("Create User");
		explicitlyWaitForElement("textfield_UserManagement_User_Create_UserId", getTotalWaitTimeInSecs());
		verifyElementPresent("textfield_UserManagement_User_Create_UserId");
		sendKeys("textfield_UserManagement_User_Create_UserId", CREATE_USERID);
		sendKeys("textfield_UserManagement_User_Create_Password", CREATE_PASSWORD);
		sendKeys("textfield_UserManagement_User_Create_ConfirmPassword", CREATE_PASSWORD);
		clickAndWait("button_UserManagement_User_Create_Save");
		getHandleToWindow("Messages");
		getHandleToWindow("Messages").close();
		getHandleToWindow("Create User");
		getHandleToWindow("Create User").close();
		endLine(method);
	}

	@Test(priority = 5)
	public void create_AdminUser(Method method) throws Exception {
		startLine(method);
		getHandleToWindow("ULEAF");
		navigateToUseManagement();
		explicitlyWaitForElement("link_Usermanagement_User", getTotalWaitTimeInSecs());
		verifyElementPresent("link_Usermanagement_User");
		click("link_Usermanagement_User");
		clickAndWait("button_UserManagement_User_Create");
		getHandleToWindow("Create User");
		explicitlyWaitForElement("textfield_UserManagement_User_Create_UserId", getTotalWaitTimeInSecs());
		verifyElementPresent("textfield_UserManagement_User_Create_UserId");
		sendKeys("textfield_UserManagement_User_Create_UserId", CREATE_ADMIN_USERID);
		sendKeys("textfield_UserManagement_User_Create_Password", CREATE_PASSWORD);
		sendKeys("textfield_UserManagement_User_Create_ConfirmPassword", CREATE_PASSWORD);
		click("checkBox_UserManagement_User_SysAdmin");
		clickAndWait("button_UserManagement_User_Create_Save");
		getHandleToWindow("Messages");
		getHandleToWindow("Messages").close();
		getHandleToWindow("Create User").close();
		getHandleToWindow("ULEAF");
		endLine(method);
	}

	@Test(priority = 6)
	public void createUser_AndSearchUser(Method method) throws Exception {
		startLine(method);
		navigateToUseManagement();
		explicitlyWaitForElement("link_Usermanagement_User", getTotalWaitTimeInSecs());
		verifyElementPresent("link_Usermanagement_User");
		click("link_Usermanagement_User");
		clickAndWait("button_UserManagement_User_Create");
		getHandleToWindow("Create User");
		explicitlyWaitForElement("textfield_UserManagement_User_Create_UserId", getTotalWaitTimeInSecs());
		verifyElementPresent("textfield_UserManagement_User_Create_UserId");
		sendKeys("textfield_UserManagement_User_Create_UserId", CREATE_ADMIN_USERID);
		sendKeys("textfield_UserManagement_User_Create_Password", CREATE_PASSWORD);
		sendKeys("textfield_UserManagement_User_Create_ConfirmPassword", CREATE_PASSWORD);
		click("checkBox_UserManagement_User_SysAdmin");
		clickAndWait("button_UserManagement_User_Create_Save");
		getHandleToWindow("Messages");
		getHandleToWindow("Messages").close();
		getHandleToWindow("Create User").close();
		getHandleToWindow("ULEAF");
		navigateToHome();
		navigateToUseManagement();
		explicitlyWaitForElement("link_Usermanagement_User", getTotalWaitTimeInSecs());
		verifyElementPresent("link_Usermanagement_User");
		click("link_Usermanagement_User");
		sendKeys("textfield_UserManagement_User_Search_UserId", "userId1");
		clickAndWait("button_UserManagement_User_Search");
		verifyElementPresent("checkBox_UserManagement_User_Search_FirstRecord");
		navigateToHome();
		getHandleToWindow("ULEAF");
		endLine(method);
	}

	@Test(priority = 7)
	public void createUser_AndDeleteUser(Method method) throws Exception {
		startLine(method);
		navigateToUseManagement();
		explicitlyWaitForElement("link_Usermanagement_User", getTotalWaitTimeInSecs());
		verifyElementPresent("link_Usermanagement_User");
		click("link_Usermanagement_User");
		clickAndWait("button_UserManagement_User_Create");
		getHandleToWindow("Create User");
		explicitlyWaitForElement("textfield_UserManagement_User_Create_UserId", getTotalWaitTimeInSecs());
		verifyElementPresent("textfield_UserManagement_User_Create_UserId");
		sendKeys("textfield_UserManagement_User_Create_UserId", "userId1234");
		sendKeys("textfield_UserManagement_User_Create_Password", "Oracle0!");
		sendKeys("textfield_UserManagement_User_Create_ConfirmPassword", "Oracle0!");
		clickAndWait("button_UserManagement_User_Create_Save");
		getHandleToWindow("Messages");
		getHandleToWindow("Messages").close();
		getHandleToWindow("Create User").close();
		getHandleToWindow("ULEAF");
		//navigateToHome();
		navigateToUseManagement();
		explicitlyWaitForElement("link_Usermanagement_User", getTotalWaitTimeInSecs());
		verifyElementPresent("link_Usermanagement_User");
		click("link_Usermanagement_User");
		System.out.println("leeeee");
		explicitlyWaitForElement("textfield_UserManagement_User_Search_UserId", getTotalWaitTimeInSecs());
		verifyElementPresent("textfield_UserManagement_User_Search_UserId");
		sendKeys("textfield_UserManagement_User_Search_UserId", "userId1234");
		click("button_UserManagement_User_Search");
		verifyElementPresent("checkBox_UserManagement_User_Search_FirstRecord");
		clickAndWait("checkBox_UserManagement_User_Search_FirstRecord");
		clickAndWait("button_UserManagement_User_Delete");
		clickAndWait("button_UserManagement_User_Delete_Submit");
		getHandleToWindow("Messages");
		getHandleToWindow("Messages").close();
		endLine(method);
	}
}
