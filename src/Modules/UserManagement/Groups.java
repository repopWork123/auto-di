package Modules.UserManagement;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.SeleniumBaseClass;

public class Groups extends SeleniumBaseClass {
	
	private String groupsId="V16ULF16B27";
	private static final String USEMANAGEMET_ADMIN_URL = "/useradmin-ui/userAdmin.action";
	@BeforeTest
	public void beforeTest() throws Exception {
		// loginAndSelectIncident();
		loginInIEAndSelectIncident();
	}
	
	private void navigateToUseManagement() {
		navigateToPage(USEMANAGEMET_ADMIN_URL);
	}

	@Test(priority=1)
	public void searchGroup() throws Exception
	{
		navigateToUseManagement();
		click("link_UserManagement_GroupsLink");
		sendKeys("textfield_UseManagement_Group_IncidentId", groupsId);
		click("button_UserManagement_Group_SeacrchButton");
		click("link_UserManagement_CloseLink");
		getHandleToWindow("Home -  Uleaf");
		
	}
	
}
