package Modules.UserManagement;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.SeleniumBaseClass;

public class Roles extends SeleniumBaseClass {
	
	private static final String USEMANAGEMET_ADMIN_URL = "/useradmin-ui/userAdmin.action";

	@BeforeTest
	public void beforeTest() throws Exception {
		// loginAndSelectIncident();
		loginInIEAndSelectIncident();
	}
	private void navigateToUseManagement() {
		navigateToPage(USEMANAGEMET_ADMIN_URL);
	}

	// @Test(priority = 1)
	public void searchRoles_WithoutEnterDetails() throws Exception {
		navigateToUseManagement();
		click("link_Usermanagement_Roles");
		click("button_UserManagement_Roles_Search");
		
	}

	 @Test(priority = 2)
	public void createRole() throws Exception {
		navigateToUseManagement();
		click("link_Usermanagement_Roles");
		sendKeys("textfield_UserManagement_Role_NameSerch", "AutoRole");
		sendKeys("textfield_UserManagement_Role_DescSerch", "Desc");
		clickAndWait("checkBox_Usermanagement_Role_flag_add");
		click("checkBox_UserManagement_Role_add");
		click("button_UserManagement_Role_Create");
		getHandleToWindow("Messages").close();
		getHandleToWindow("Uleaf -  User Management");
	}
}
