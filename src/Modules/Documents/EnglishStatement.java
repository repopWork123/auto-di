package Modules.Documents;


import Modules.Indexes.Indexes;
import org.testng.annotations.*;

import ReusableUtilities.SeleniumBaseClass;

public class EnglishStatement extends SeleniumBaseClass {
	
	protected static final String REGISTER_DOCUMENT_URL = "/group/di/register-document";
	
	@BeforeTest
	public void startBrowser() throws Exception {

		loginInIEAndSelectIncident();
	}

	//@AfterTest
	/*public void closeBrowser() throws Exception {

		closeApp();

	}*/
	
	@Test(priority=1)
	public void registerEnglishStatement() throws Exception{
		Indexes nominalIndex = new Indexes(driver);
		String  nominalUrn=nominalIndex.createBulkNominalAndGetURNs("Nominal","MALE","1");
		navigateToPage(REGISTER_DOCUMENT_URL);
		selectByVisibleText("dropDown_Documents_Register_DocumentType", "English Statement");
		click("textField_Document_Register_DateTaken");
		click("button_Documents_Register_Date_Done");
		selectTextByIndex("dropDown_Documents_Register_ReferrenceGroup", 1);
		sendKeys("textField_Document_Register_URN",nominalUrn);//In future need to create new Nominal in the script itself and use that
		click("button_Documents_Register");
	}
	
	@Test(priority=2)
	public void assignTagsandOtherInfo() throws Exception{
		explicitlyWaitForElement("text_Tags", 10);//make sure current group has tags assigned to english statement
		click("button_Edit");
		click("checkBox_Document_Tags");
		click("button_SaveTags");
	}
	
	
	@Test(priority=3)
	public void TypingService() throws Exception{
		Thread.sleep(8000);
		if(isEnabled("list_TypingService")==true){//if false then need to change state in document detail page, need to write code
			click("link_TypingService");
			explicitlyWaitForElement("text_DocumentHeader", 10);
			//Need to check 'Find address' button and need to add data to all fields on header and footer.
			//Read document content to be added and write data to tinyMCE
			String docContent = readTextFile(System.getProperty("user.dir")+"\\testData.txt");
			String mWindow = switchToFrame1("tinymce_iFrame");
			writeToTinyMCE(docContent,"frame_EditorBody");
			driver.switchTo().window(mWindow);
			//need to write code for save button
			//need to write code to test reset button
			//need to write code to test cancel button
			click("button_CompleteTypingService");
		}
	}
	
	@Test(priority=4)
	public void ProofReading() throws Exception{
		click("link_ProofReading");
		sleep(15000);
		//Need to check 'Find address' button.
		//Need to verify data entered in typing service is displayed on proof reading or not to document content.
		//need to write code for save button
		//need to write code to test reset button
		//need to write code to test cancel button
		click("button_CompleteTypingService");
	}
	
	/*@Test(priority=5)
	public void ViewDocumentContent() throws Exception{
		explicitlyWaitForElement("link_ViewDocumentCOntent", 10);
		hoverAndClick(new String[]{"link_Documents"}, "link_ViewDocumentCOntent");
		explicitlyWaitForElement("text_DocumentURN", 10);
		SendKeys("text_DocumentURN", urn);
		Click("button_Submit");
	}*/
	
}
	

