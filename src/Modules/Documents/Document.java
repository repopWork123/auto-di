package Modules.Documents;

import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import Modules.Indexes.Nominal;
import ReusableUtilities.SeleniumBaseClass;

public class Document extends SeleniumBaseClass{
	
	protected static final String REGISTER_DOCUMENT_URL = "/group/holmes/register-document";
	protected static String doc_URNs;
	private String documentDate="01/11/2016 12:42";
	
	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}
	
	public Document() {
		softAssert = new SoftAssert();
	}
	
	public Document(WebDriver driver) {
		softAssert = new SoftAssert();
		this.driver = driver;
	}
	
	public void registerEnglishStatement() throws Exception{
		  
		navigateToPage(REGISTER_DOCUMENT_URL);
		selectByVisibleText("dropDown_Documents_Register_DocumentType", "English/Welsh Statement");
		click("textField_Document_Register_DateTaken");
		sendKeys("textField_Document_Register_DateTaken", " ");
		sendKeys("textField_Document_Register_DateTaken", "01/11/2016/ 14:48");
		//click("button_Documents_Register_Date_Done");
		selectTextByIndex("dropDown_Documents_Register_ReferrenceGroup", 1);
		sendKeys("textField_Document_Register_URN","A45");//In future need to create new Nominal in the script itself and use that
		click("button_Documents_Register");
	}
	
	
	public String  registerBulkOtherDocumentAndGetURNsWithSubjectReference(String bulkCount) throws Exception{
		
		Nominal nomi = new Nominal(driver);
		String nominal =nomi.createNominalAndGetURN();
		  
		navigateToPage(REGISTER_DOCUMENT_URL);
		explicitlyWaitForElement("text_Document_Register",
				getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Documents_Register_DocumentType", "Other Document");
		sendKeys("textField_Document_Register_DateTaken", " ");
		sendKeys("textField_Document_Register_DateTaken", documentDate);
		selectTextByIndex("dropDown_Documents_Register_ReferrenceGroup", 1);
		sendKeys("textField_Document_Register_Title", "Officer report testing");
		// Bulk create Entering 50 count
		sendKeys("textField_Document_Register_URN", nominal);
		click("textField_Document_Register_URN");
		explicitlyWaitForElement("button_Documents_Register",
				getTotalWaitTimeInSecs());
		click("button_Documents_Register");
		
		click("checkBox_Document_BulkRecord");
		sendKeys("textField_Document_BulkRecordCount", bulkCount);
		click("button_Documents_Register");
		explicitlyWaitForElement("button_Documents_Register_BulkCreate_OK",
				getTotalWaitTimeInSecs());
		click("button_Documents_Register_BulkCreate_OK");
		explicitlyWaitForElement("successMsg_Document_Create",
				getTotalWaitTimeInSecs());
		String succMess = getText("successMsg_Document_Create");
		String[] docUrn = succMess.split(" ");
		doc_URNs = docUrn[5];
		
		return doc_URNs;
	}
	

	public String  registerBulkEnglishStatementAndGetURNsWithSubjectReference(String bulkCount) throws Exception{
		
		Nominal nomi = new Nominal(driver);
		String nominal =nomi.createNominalAndGetURN();
		  
		navigateToPage(REGISTER_DOCUMENT_URL);
		explicitlyWaitForElement("text_Document_Register",
				getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Documents_Register_DocumentType", "English/Welsh Statement");
		sendKeys("textField_Document_Register_DateTaken", " ");
		sendKeys("textField_Document_Register_DateTaken", documentDate);
		selectTextByIndex("dropDown_Documents_Register_ReferrenceGroup", 1);
		//sendKeys("textField_Document_Register_Title", "Officer report testing");
		// Bulk create Entering 
		sendKeys("textField_Document_Register_URN", nominal);
		click("textField_Document_Register_URN");
		explicitlyWaitForElement("button_Documents_Register",
				getTotalWaitTimeInSecs());
		click("button_Documents_Register");
		
		click("checkBox_Document_BulkRecord");
		sendKeys("textField_Document_BulkRecordCount", bulkCount);
		click("button_Documents_Register");
		explicitlyWaitForElement("button_Documents_Register_BulkCreate_OK",
				getTotalWaitTimeInSecs());
		click("button_Documents_Register_BulkCreate_OK");
		explicitlyWaitForElement("successMsg_Document_Create",
				getTotalWaitTimeInSecs());
		String succMess = getText("successMsg_Document_Create");
		String[] docUrn = succMess.split(" ");
		doc_URNs = docUrn[5];
		
		return doc_URNs;
	}
	

}
