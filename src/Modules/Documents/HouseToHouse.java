package Modules.Documents;

import org.testng.annotations.*;

import ReusableUtilities.SeleniumBaseClass;

public class HouseToHouse extends SeleniumBaseClass {
	/*@BeforeTest
	public void startBrowser() throws Exception {

		loginInIEAndSelectIncident();
	}*/

	/*@AfterTest
	public void closeBrowser() throws Exception {

		closeApp();

	}*/
	
	@Test
	public void RegisterHouseToHouse() throws Exception{
		//explicitlyWaitForElement("link_Documents", 10);
		//hoverAndClick(new String[]{"link_Documents"}, "link_RegisterDocuments");
		sleep(6000);
		driver.get("http://ustr-erl-4427.na.uis.unisys.com:8080/group/holmes/register-document");
		explicitlyWaitForElement("text_Document_Register", 10);
		Thread.sleep(5000);
		selectByVisibleText("dropDown_Documents_Register_DocumentType", "House to House");
		click("textField_Document_Register_DateTaken");
		click("button_Documents_Register_Date_Done");
		selectTextByIndex("dropDown_Documents_Register_ReferrenceGroup", 1);
		sendKeys("textField_Document_Register_URN", "N1");//In future need to create new Nominal in the script itself and use that
		sendKeys("textField_Document_Register_Folder", "1234");
		sendKeys("textField_Document_Register_Premises", "12");
		sendKeys("textField_Document_Register_Occupants", "34");
		click("button_Documents_Register");
	}
	

}
