package Modules.Broadcasts;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.SeleniumBaseClass;

public class ProofOfLife extends SeleniumBaseClass {
	@BeforeTest
	public void beforeTest() throws Exception {
		loginAndSelectIncident();
		// loginInIEAndSelectIncident();
	}

	 @Test (priority=1)
	public void raiseProofOfLife_WithoutMandatoryDetails() throws Exception {
		sleep(3000);
		click("link_Broadcasts");
		sleep(1000);
		click("link_Broadcasts_POL");
		sleep(3000);
		click("button_Broadcast_Create");
		sleep(3000);
	}

	 @Test (priority=2)
	public void raiseProofOfLife() throws Exception {
		sleep(3000);
		click("link_Broadcasts");
		sleep(1000);
		click("link_Broadcasts_POL");
		sleep(3000);
		sendKeys("textField_Broadcast_POL_Title", "Proof of life tiltle");
		driver.findElement(
				By.xpath("//td[contains(.,'POL Date/Time')]/following-sibling::td[1]/input")).clear();
		sendKeys("textField_Broadcast_POL_Date", "16/03/2016 11:28");
		click("button_Broadcast_Create");
		sleep(3000);
		verifyElementPresent("SuccessMsg_CreateAction");
	}

	 @Test (priority=3)
	public void updateProofOfLife() throws Exception {
		sleep(3000);
		click("link_Broadcasts");
		sleep(1000);
		click("link_Broadcasts_POL");
		sleep(3000);
		sendKeys("textField_Broadcast_POL_Title", "Proof of life tiltle");
		driver.findElement(
				By.xpath("//td[contains(.,'POL Date/Time')]/following-sibling::td[1]/input")).clear();
		sendKeys("textField_Broadcast_POL_Date", "16/03/2016 11:28");
		click("button_Broadcast_Create");
		sleep(3000);
		click("button_Broadcast_POL_Update");
		sleep(3000);
		click("button_Broadcast_POL_Update_Save");
		verifyElementPresent("SuccessMsg_CreateAction");
	}

	 @Test (priority=4)
	public void raiseProofOfLife_similar() throws Exception {
		sleep(3000);
		click("link_Broadcasts");
		sleep(1000);
		click("link_Broadcasts_POL");
		sleep(3000);
		sendKeys("textField_Broadcast_POL_Title", "Proof of life tiltle");
		driver.findElement(
				By.xpath("//td[contains(.,'POL Date/Time')]/following-sibling::td[1]/input")).clear();
		sendKeys("textField_Broadcast_POL_Date", "16/03/2016 11:28");
		click("button_Broadcast_Create");
		sleep(3000);
		click("button_Broadcast_POL_Similar");
		sleep(2000);
		sendKeys("textField_Broadcast_POL_Text",
				"Similar Proof of life text area");
		click("button_Broadcast_Create");
		sleep(3000);
	}

	@Test(priority = 5)
	public void raiseProofOfLife_WithTheme() throws Exception {
		sleep(3000);
		click("link_Broadcasts");
		sleep(1000);
		click("link_Broadcasts_POL");
		sleep(3000);
		sendKeys("textField_Broadcast_POL_Title", "Proof of life tiltle");
		driver.findElement(
				By.xpath("//td[contains(.,'POL Date/Time')]/following-sibling::td[1]/input")).clear();
		sendKeys("textField_Broadcast_POL_Date", "16/03/2016 11:28");
		click("checkbox_Broadcast_POL_Theme");
		click("button_Broadcast_Create");
		sleep(3000);
		verifyElementPresent("SuccessMsg_CreateAction");
	}
}
