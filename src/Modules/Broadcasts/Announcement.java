package Modules.Broadcasts;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.SeleniumBaseClass;

public class Announcement extends SeleniumBaseClass {
	
	
	
	@BeforeTest
	public void beforeTest() throws Exception {
		loginAndSelectIncident();
		// loginInIEAndSelectIncident();
	}

	@Test(priority = 1)
	public void raiseAnnouncement_withoutMandatorydetails() throws Exception {
		sleep(3000);
		click("link_Broadcasts");
		sleep(1000);
		click("link_Broadcasts_Announcemrnt");
		sleep(3000);
		click("button_Broadcast_Create");
		sleep(3000);
	}

	 @Test (priority=2)
	public void raiseAnnouncement() throws Exception {
		sleep(3000);
		click("link_Broadcasts");
		sleep(1000);
		click("link_Broadcasts_Announcemrnt");
		sleep(3000);
		sendKeys("textField_Broadcast_Announcement_Title", "Announcement tiltle");
		sleep(3000);
		click("button_Broadcast_Create");
		sleep(3000);

	}

	 @Test (priority=3)
	public void updateAnnouncement() throws Exception {
		sleep(3000);
		click("link_Broadcasts");
		sleep(1000);
		click("link_Broadcasts_Announcemrnt");
		sleep(3000);
		sendKeys("textField_Broadcast_Announcement_Title", "Announcement tiltle");
		click("button_Broadcast_Create");
		sleep(3000);
		click("button_Broadcast_Announcement_Update");
		sleep(3000);
		click("button_Broadcast_Announcement_Update_Save");
	}

	 @Test (priority=4)
	public void raiseAnnouncement_Similar() throws Exception {
		sleep(3000);
		click("link_Broadcasts");
		sleep(1000);
		click("link_Broadcasts_Announcemrnt");
		sleep(3000);
		sleep(3000);
		sendKeys("textField_Broadcast_Announcement_Title", "Announcement tiltle");
		driver.findElement(By.id("createButton")).click();
		sleep(3000);
		click("button_Broadcast_Announcement_Similar");
		sleep(2000);
		sendKeys("textField_Broadcast_Announcement_Text",
				"Similar Announcement text area");
		click("button_Broadcast_Create");
		sleep(3000);
	}

	 @Test (priority=5)
	public void raiseAnnouncement_WithTheme() throws Exception {
		sleep(3000);
		click("link_Broadcasts");
		sleep(1000);
		click("link_Broadcasts_Announcemrnt");
		sleep(3000);
		sleep(3000);
		sendKeys("textField_Broadcast_Announcement_Title", "Announcement tiltle");
		click("checkbox_Broadcast_AnnouncementTheme");
		click("button_Broadcast_Create");
		sleep(3000);

	}

}
