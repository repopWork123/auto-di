package Modules.Broadcasts;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Modules.Entity.Entity;
import ReusableUtilities.SeleniumBaseClass;

public class Broadcast  extends Entity{
	protected static final String RAISE_POL_URL = "/group/holmes/raise-proof-of-life";
	protected static final String RAISE_AnNNOUNCEMENT_URL = "/group/holmes/raise-announcement";
	protected static String pol_URN;
	protected static String announcement_URN;
	
	
	public void navigateTo(String linkUrl) {
		navigateToPage(linkUrl);
	}
	
	public Broadcast() {
		softAssert = new SoftAssert();
	}
	
	public Broadcast(WebDriver driver) {
		softAssert = new SoftAssert();
		this.driver = driver;
	}

	public String raisePOL_AndGetURN(String polTitle) throws Exception {
		
		navigateTo(RAISE_POL_URL);
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Action_Title",polTitle);
		sendKeys("textField_Broadcast_POL_Date", "09/11/2016 16:12");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		pol_URN = actUrn[3];
		System.out.println(pol_URN + "pol_URN");
		return pol_URN;
	}
	

	public String raiseAnnouncementAndGetURN(String announcementTitle) throws Exception {
		
		navigateTo(RAISE_AnNNOUNCEMENT_URL);
		explicitlyWaitForElement("button_Action_Raise",
				getTotalWaitTimeInSecs());
		sendKeys("textField_Broadcast_Announcement_Title", "Announcement tiltle");
		clickAndWait("button_Action_Raise");
		explicitlyWaitForElement("successMsg_Action_Create",
				getTotalWaitTimeInSecs());
		String succMess = getText("successMsg_Action_Create");
		String[] actUrn = succMess.split(" ");
		announcement_URN = actUrn[1];
		return announcement_URN;
	}
 
}
