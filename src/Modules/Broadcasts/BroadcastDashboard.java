package Modules.Broadcasts;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.Report;
import ReusableUtilities.SeleniumBaseClass;

public class BroadcastDashboard extends SeleniumBaseClass {

	@BeforeTest
	public void beforeTest() throws Exception {
		loginAndSelectIncident();
		// loginInIEAndSelectIncident();
	}

	@Test(priority = 1)
	public void serachProofOflife_ByURN() throws Exception {
		sleep(3000);
		click("link_Broadcasts");
		sleep(1000);
		click("link_Broadcasts_DashBoard");
		sleep(3000);

		sendKeys("textField_Broadcast_Dashboard_URN", "POL1");

		click("button_BroadcastSearch");
		sleep(3000);

		if (driver.findElements(By.id("jqg_searchResultsTable_1")).size() >= 1) {
			Report.pass("serachProofOflife_ByURN is Successfull");
		}

	}

	@Test(priority = 2)
	public void serachAnnouncement_ByURN() throws Exception {
		sleep(3000);
		click("link_Broadcasts");
		sleep(1000);
		click("link_Broadcasts_DashBoard");
		sleep(3000);
		sendKeys("textField_Broadcast_Dashboard_URN", "ANN1");

		click("button_BroadcastSearch");
		sleep(3000);
		if (driver.findElements(By.id("jqg_searchResultsTable_1")).size() >= 1) {
			Report.pass("serachProofOflife_ByURN is Successfull");
		}
	}

	@Test(priority = 3)
	public void serachAnnouncement_ByTitle() throws Exception {
		sleep(3000);
		click("link_Broadcasts");
		sleep(1000);
		click("link_Broadcasts_DashBoard");
		sleep(3000);
		sendKeys("textField_Broadcast_Dashboard_Title", "Announcement Title");

		click("button_BroadcastSearch");
		sleep(3000);
		if (driver.findElements(By.id("jqg_searchResultsTable_1")).size() >= 1) {
			Report.pass("serachAnnouncement_ByTitle is Successfull");
		}
	}

	@Test(priority = 4)
	public void serachBroadcastAll() throws Exception {
		sleep(3000);
		click("link_Broadcasts");
		sleep(1000);
		click("link_Broadcasts_DashBoard");
		sleep(3000);

		click("button_BroadcastSearch");
		sleep(3000);
		if (driver.findElements(By.id("jqg_searchResultsTable_1")).size() >= 1) {
			Report.pass("serachBroadcastAll is Successfull");
		}
	}

	@Test(priority = 5)
	public void serachPOL_ByTheme() throws Exception {
		sleep(3000);
		click("link_Broadcasts");
		sleep(1000);
		click("link_Broadcasts_DashBoard");
		sleep(3000);
		click("checkbox_Broadcast_Dashboard_Theme");

		click("button_BroadcastSearch");
		sleep(3000);
		if (driver.findElements(By.id("jqg_searchResultsTable_1")).size() >= 1) {
			Report.pass("serachPOL_ByTheme is Successfull");
		}
	}

}
	

