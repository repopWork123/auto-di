package Modules.FastAction;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.asserts.SoftAssert;

import ReusableUtilities.Dataprovider;
import ReusableUtilities.SeleniumBaseClass;

public class RaiseFastAction extends SeleniumBaseClass  {
		//Todo Shalini
		// Hard Coded data which gets added to respective fields during Fast Action Raise
		private static final String ACTION_ORIGINATING_DETAIL_MAX_CONTENT = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ012345678901234567";
		private static final String RAISE_FAST_ACTION_URL = "/group/holmes/raise-fast-action";
		private static final String ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE = "Action - Auto Test for Raise Action update";
		private static final String ACTION_ORIGINATING_DETAIL = "Originating Desc";
		private static final String ALPHA_NUMERIC_TEXT="A1a29F";
		private static final String SPECIALCHARACTER_TEXT="@%^*(";
		
		// Data used to create Officer
		private static final String Create_Officer_Surname="Surname";
		private static final String Create_Officer_Forename="Forename";
		
		//	IncidentID_full=MORSTA16D61 which is present in config.properties is read and stored in incidentIDfull
		String incidentIdFull = getCurrentIncidentIDFull();

		// Data for Linked Actions/ Documents Grid read from Xl sheet
		private String linkedActionsRecordURN;
		private String linkedDocumentsRecordURN; 

		public void navigateTo(String linkUrl) {
			navigateToPage(linkUrl);
		}
		

		//default constructor
		public RaiseFastAction() {
			softAssert = new SoftAssert();
		}
		
		
		public void verifyMandatoryFieldsInFastAction() throws Exception{
			verifyElementIsMandatory("label_Action_Mirsap_Title");
			
		}
		
		// Method to verify active tab by passing Action Tab xpath as parameter
		public void verifyActionTabEnabled(String objname){
			verifyTabActive(objname);
		}
		
		@DataProvider(name = "raiseMirsapAction")
		public String[][] dataProvider() {

			String[][] testData = Dataprovider.readExcelData("Actions",
					testDataFilePath, "RaiseMirsapAction");
			return testData;
		}

		@Test(dataProvider = "raiseMirsapAction", priority = 1)
		public void testData(String linkedActionsRecordURN, String linkedDocumentsRecordURN) throws Exception {
			this.linkedActionsRecordURN = linkedActionsRecordURN;
			this.linkedDocumentsRecordURN = linkedDocumentsRecordURN;
		}
		
		
		// Starting Step of Execution is from @Before Test Annotation	
		
		@BeforeTest
		public void beforeTest() throws Exception {
			// Steps starting from IE driver instance creation,  Navigate to app URl, Login , select default incident on Home Page	are executed 	
			loginInIEAndSelectIncident();
			//	loginAndSelectIncident();

		}
		
		
	
		@Test(priority = 1)
		public void createFastAction_WithoutMandatoryDetails(Method method)
				throws Exception {
			//Prints the starting of current Method name as  - ------------createFastAction_WithoutMandatoryDetails- Test Starting------------
			startLine(method);
			//From Base url = http://gbhlmmorstana1.uleaf.site:8080 navigates to action url = "/group/holmes/raise-fast-action"
			navigateTo(RAISE_FAST_ACTION_URL);
			// verifies 1st Tab - Fast Action Tab is enabled- Xpath of Fast Action Tab is  passed as parameter
			verifyActionTabEnabled("tab_Fast_Action");
			//Without entering any field value - click on Create Fast Action button
			clickAndWait("button_Action_Raise");
			// wait for validation errors and verify the same. 
			explicitlyWaitForElement("errorMsg_Action_Create_WithOutAnyFields",
					getTotalWaitTimeInSecs());
			verifyText("There are validation errors","errorMsg_Action_Create_WithOutAnyFields");
			verifyText("Title is required","errorMsg_Action_Title");
			//Prints the ending of current Method name as  - ------------createFastAction_WithoutMandatoryDetails Test Starting------------ 
			endLine(method);
		}

	@Test(priority=2)
		public void createFastAction_withAllFieldDetails(Method method)throws Exception{
			//Prints the starting of current Method name as  - ------------createFastAction_withAllFieldDetails- Test Starting------------
			startLine(method);
			//From Base url = http://gbhlmmorstana1.uleaf.site:8080 navigates to action url = "/group/holmes/raise-fast-action"
			navigateTo(RAISE_FAST_ACTION_URL);
			// verifies 1st Tab - Fast Action Tab is enabled- Xpath of Fast Action Tab is  passed as parameter
			verifyActionTabEnabled("tab_Fast_Action");
			// Wait for Create button on Action create page for 60secs
			explicitlyWaitForElement("button_Action_Raise",
					getTotalWaitTimeInSecs());
			// Confirm Create Button on Action Create Page is present
			verifyElementPresent("button_Action_Raise");
			// Verify all mandatory field list with '*'
			verifyMandatoryFieldsInFastAction();
			//Verify Default Incident ID in Incident DropDown
			verifyTextContains(incidentIdFull, "dropDown_Action_Create_Incident");
			//Pass data to all fields
			sendKeys("textField_Action_Mirsap_Title", ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE);
			insertIntoAllTinyMCEinPage("test");
			click("button_Action_Fast_Create_Officer");
			sendKeys("textfield_Action_CreateOfficer_Surname", Create_Officer_Surname);
			sendKeys("textfield_Action_CreateOfficer_Forename", Create_Officer_Forename);
			click("button_Action_Fast_Create");
			sendKeys("textField_Action_Mirsap_OriginatingDetails",ACTION_ORIGINATING_DETAIL);
			
			//Adding Linked Action URN
			clickAndWait("button_Action_Create_LinkedAssoc_Action");
			explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Action_URN", getTotalWaitTimeInSecs());
			sendKeysAndTab("textField_Action_Create_LinkedAssoc_Action_URN",linkedActionsRecordURN);
			click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon");
			String xpathForCurrentIncident = "//div[@id='" + incidentIdFull + "']";
			WebElement we=driver.findElement(By.xpath(xpathForCurrentIncident));
			we.click();
			click("button_Action_Create_LinkedAssoc_Action_Add");
			
			//Adding Linked Document URN
			clickAndWait("button_Action_Create_LinkedAssoc_Documents");
			explicitlyWaitAndGetWebElement("textField_Action_Create_LinkedAssoc_Document_URN", getTotalWaitTimeInSecs());
			sendKeys("textField_Action_Create_LinkedAssoc_Document_URN",linkedDocumentsRecordURN);
			click("dropDown_Action_Fast_SelectRecord_ReferenceIncidentIcon1");
			we=driver.findElement(By.xpath(xpathForCurrentIncident));
			String xpathForCurrentIncident2 = "(//div[@id='" + incidentIdFull + "'])[2]";
			WebElement element = driver.findElement(By.xpath(xpathForCurrentIncident2));
			element.click();
			clickAndWait("button_Action_Create_LinkedAssoc_Documents_Add");
			
			//link_Action_Create_Theme
			click("checkBox_Action_Mirsap_Themes");
			clickAndWait("button_Action_Raise");
			explicitlyWaitForElement("successMsg_Action_Create",
					getTotalWaitTimeInSecs());
			
			// Verification of View Action Page
			verifyElementPresent("successMsg_Action_Create");
			verifyText(ACTION_AUTO_TEST_FOR_RAISE_MIRSAP_ACTION_TITLE,
					"label_Action_Mirsap_View_Title");
			verifyText(ACTION_ORIGINATING_DETAIL,
					"label_Action_FastAction_View_OriginatingDetails");
			click("button_Action_Association_Tab");
			verifyActionTabEnabled("tab_Association");
			verifyElementPresent("checkBox_Action_Association_FirstRecord");
			sendKeysAndEnter("textField_Action_View_Association_TargetUrn",
					incidentIdFull + "-" + linkedActionsRecordURN);
			validateAllVerificationPoints(); 
			//Prints the ending of current Method name as  - ------------createFastAction_withAllFieldDetails Test Ending------------ 
			endLine(method);
			
			
	}

  @AfterTest
  public void afterTest() {
  }

}
