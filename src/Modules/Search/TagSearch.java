package Modules.Search;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.Report;
import ReusableUtilities.SeleniumBaseClass;

public class TagSearch extends SeleniumBaseClass
{
	
	@BeforeTest
	public void beforeTest() throws Exception {
		 loginAndSelectIncident();
		//loginInIEAndSelectIncident();
	}
	
	 public void NavigationToTagEnquiry() throws Exception
	  {
		  sleep(8000);
		  getHandleToWindow("HOME - U-LEAF");
		  driver.findElement(By.xpath(".//*[@id='navigation']/ul/li[6]/a/span")).click();
		  sleep(3000);
		  driver.findElement(By.xpath(".//*[@id='navigation']/ul/li[6]/ul/li[2]/a/span")).click();
		  sleep(3000);
	  }
	 
	  @Test (priority=1)
	  public void searchAllRecords () throws Exception {
		 
		  try {
			 // loginAndSelectIncident();
			  NavigationToTagEnquiry();
			  sleep(8000);
			  getHandleToWindow("Tag Search");
			  sleep(3000);
			  	 
			  driver.findElement(By.id("searchReasonId_input")).sendKeys("Create default list");
			  driver.findElement(By.xpath(".//*[@id='Create default list']")).click();
			  sleep(3000);
			  driver.findElement(By.name("searchButt")).click();
			  sleep(10000);
			  driver.switchTo().alert().accept();
			  sleep(10000);
		   
			  String actual = driver.findElement(By.xpath("//form[@id='TagInquiryAction']/table[2]/tbody/tr[4]/td/table/tbody/tr/td")).getText();
			  if(actual.contains("Total Number of Records"))
			  {		  
				  Report.pass("Tag Search is Successfull");
				  driver.close();
			  }
			  else
			  {
				  Report.fail("Tag Search is Not Successfull");
				  driver.close(); 
			  }	 
		  }
		  catch ( Exception e)
		  {
			  
			  
		  }
		 }
	 
	
	  @Test (priority=2)
	  public void addTagToSearch() throws Exception {
		 
		  try{ 
			  NavigationToTagEnquiry();
			  sleep(8000);
			  getHandleToWindow("Tag Search");
			  sleep(3000);
		  
			  driver.findElement(By.name("tagId")).click();
			  driver.findElement(By.id("TagInquiryAction_tagId")).click();
			  
			  new Select(driver.findElement(By.id("TagInquiryAction_tagId"))).selectByIndex(1);
			  
			  sleep(6000);
			  driver.findElement(By.name("tagValueId")).click();
			  driver.findElement(By.id("TagInquiryAction_tagValueId")).click();
			  new Select(driver.findElement(By.id("TagInquiryAction_tagValueId"))).selectByIndex(1);
		
			  driver.findElement(By.id("searchReasonId_input")).sendKeys("Create default list");
			  driver.findElement(By.xpath(".//*[@id='Create default list']")).click();
			  sleep(3000);
			  
			  driver.findElement(By.name("addTag")).click();
			  
			  Report.pass("addTag To Search is successfull");
			  driver.close();
			  
		  }
		  catch( Exception e)
		  {
				
		  } 	
	  }
	  
}
