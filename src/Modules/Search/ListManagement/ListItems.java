package Modules.Search.ListManagement;

import java.awt.ItemSelectable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Modules.Actions.Actions;
import Modules.Actions.FastAction.FastAction;
import Modules.Actions.MisrapAction.MirsapAction;
import Modules.Broadcasts.Broadcast;
import Modules.Indexes.Indexes;
import ReusableUtilities.Report;

public class ListItems extends ListManagement{
	
	private static final String LIST_PORTLET_WINDOW_TITLE = "List Portlet";
	private static final String BULKUPDATE_RECORDS_CONFIRMATION = "Are you sure you want to update these records?\n\nAny existing values will be overwritten with the given values where provided.";
	private static final String SOFT_ENTITY_PORTLET_WINDOW_TITLE = "Soft Entity Portlet";
	private static final String EXISTING_LIST_ITEMS_FROM_DIFFERENT_INCIDENT = "Existing List item(s) are from different incident";
	private static final String MIRSAP_CLASS="SCENE";
	// Data to add to List
	private String announcementURN="ANN1";
	private String polURN="POL1";
	private String fastActionURN="A1";
	private String mirsapActionURN="A2";
	private String disclosureURN="DI1";
	private String caseFileURN="CF1";
	private String englishStatementURN="S1";
	private String otherDocumentURN="D1";
	private String messageDocumentURN="M1";
	private String pdfDocumentURN="P1";
	private String questionarrieDocumentURN="Q1";
	private String officerReportDocumentURN="R1";
	private String interviewDocumentURN="Y1";
	private String transmissionDocumentURN="T1";
	private String intelligenceDocumentURN="Z1";
	private String housetToHouseDocumentURN="H1";
	private String policyFileURN="PL1";
	private String generalInstructionsURN="GI1";
	private String logOfEventsURN="LE1";
	private String nominalURN="N1";
	private String categoryURN="C1";
	private String exhibitURN="X1";
	private String locationURN="L1";
	private String organisationURN="O1";
	private String telephoneURN="TE1";
	private String sequenceOfEventsURN="EV1";
	private String vehicleURN="V1";
	private String callHistoryURN="CL1";
	private String bodyPartURN="CN1";//((Survivor/evacuee/casulaity/misper/caller-CN1))
	private String simQuestionURN="SQ1";
	private String nokURN="NK1";
	private String listName;
	String COPYLIST = "Copylist"+getCurrentDataStampwithoutSlash();
	
	
	//Alert Texts to be verified
	private static final String DELETE_LIST_TEXT = "Please select one row to edit";
	private static final String DELETE_LIST_CONFIRMATION="Do you want to Delete the selected List(s)?";

	//Messages to be Verified
	private static final String DELETE_SUCESS="successfully deleted";
	private static final String BULKUPDATE_RECORDS_INFORMATION = "Fast Action updated";
	private static final String BULKUPDATE_ENITY_RECORDS_INFORMATION = "Entities ready for bulk update";
	
	
	public void navigateToListManagement(String linkUrl) {
		navigateToPage(linkUrl);
	}

	//default constructor
	public ListItems() {
		softAssert = new SoftAssert();
	}
	
	public ListItems(WebDriver driver) {
		softAssert = new SoftAssert();
		this.driver = driver;
	}


	@BeforeTest
	public void beforeTest() throws Exception {
		loginInIEAndSelectIncident();
//		loginAndSelectIncident();

	}
	
	@Test(priority=1)
	public void testData() throws Exception{

		/*Broadcast broadcast = new Broadcast(driver);
		polURN = broadcast.raisePOL_AndGetURN("title");*/

	}

	//Test case 8621:List Mgmt - 001. Create a list from List management without Incident Id
	@Test(priority = 2)
	public void createList_WithoutIncidentID(Method method)throws Exception {

		startLine(method);
		
    	//initialise a list of strings to create  list without Incident ID
    	List<String> itemsToAdd = new ArrayList<String>();
    	itemsToAdd.add(announcementURN);
    	itemsToAdd.add(polURN);
    	/* 
    	itemsToAdd.add(fastActionURN);//Fast Action URN
    	itemsToAdd.add(mirsapActionURN);//Mirsap Action URN
    	itemsToAdd.add(disclosureURN); 
    	itemsToAdd.add(caseFileURN);
    	itemsToAdd.add(englishStatementURN);
    	itemsToAdd.add(otherDocumentURN);
    	itemsToAdd.add(messageDocumentURN);
    	itemsToAdd.add(pdfDocumentURN);
    	itemsToAdd.add(questionarrieDocumentURN);
    	itemsToAdd.add(officerReportDocumentURN);
    	itemsToAdd.add(interviewDocumentURN);
    	itemsToAdd.add(transmissionDocumentURN);
    	itemsToAdd.add(intelligenceDocumentURN);
    	itemsToAdd.add(housetToHouseDocumentURN);
    	itemsToAdd.add(policyFileURN);
    	itemsToAdd.add(generalInstructionsURN);
    	itemsToAdd.add(logOfEventsURN);
    	itemsToAdd.add(nominalURN);
    	itemsToAdd.add(categoryURN);
    	itemsToAdd.add(exhibitURN);
    	itemsToAdd.add(locationURN);
    	itemsToAdd.add(organisationURN);
    	itemsToAdd.add(telephoneURN);
    	itemsToAdd.add(sequenceOfEventsURN);
    	itemsToAdd.add(vehicleURN);
    	itemsToAdd.add(callHistoryURN);
    	itemsToAdd.add(bodyPartURN);
    	itemsToAdd.add(simQuestionURN);
    	itemsToAdd.add(nokURN);
    	*/
    	createListManagment(itemsToAdd ,"both", false);
		endLine(method);
	}
     
	
	//Test case 15811:List Mgmt - 002. Create a list from List management with Incident Id 
	@Test(priority = 3)
	public void createList_WithIncidentID(Method method)
			throws Exception {

		startLine(method);
		List<String> itemsToAdd = new ArrayList<String>();
		itemsToAdd.add(nominalURN);
		itemsToAdd.add(fastActionURN);
		itemsToAdd.add(otherDocumentURN);
		itemsToAdd.add(disclosureURN);

		listName = navigateToListManagmentAndAddItemsToList(itemsToAdd ,"both", true);

		//Readd a duplicate entry to the list and verify the duplicate validation error
		/*clickAndWaitForPageLoad("icon_ListManagement_AddNewListItem");
		explicitlyWaitForElement("textField_ListManagement_AddRecord",getTotalWaitTimeInSecs());*/
		sendKeys("textField_ListManagement_AddRecord", "N1");
		click("button_ListManagement_AddNewListItem_Add");
		waitForJQuery(driver);

		//error message displayed as "duplicate list item"
		verifyElementPresent("errorMsg_Search_ListManagement_CreateNewlist_AddNewListItem");
		explicitlyWaitForElement("button_ListManagement_AddNewListItem_Cancel", getTotalWaitTimeInSecs());
		click("button_ListManagement_AddNewListItem_Cancel");
		clickAndWaitForPageLoad("button_ListManagement_Create");
		explicitlyWaitForElement("successMsg_ListManagement_Create", getTotalWaitTimeInSecs());
		verifyTextContains("created","successMsg_ListManagement_Create");
		endLine(method);

	}
	//Test case 8625:List Mgmt - 003. Delete a list from list management 
	@Test(priority = 4)
	public void deleteList(Method method) throws Exception {

		startLine(method);
		navigateToListManagement(LIST_MANAGEMENT_URL);
		waitForPageLoad();
		explicitlyWaitForElement("header_ListManagement",getTotalWaitTimeInSecs());
		verifyElementPresent("header_ListManagement");
		//Without selecting a List , click delete icon and check for the message line 
		explicitlyWaitForElement("icon_ListManagement_DeleteList", getTotalWaitTimeInSecs());
		click("icon_ListManagement_DeleteList");
		explicitlyWaitForElement("dialog_ListManagement_DeleteList_Text", getTotalWaitTimeInSecs());
		verifyTextContains(DELETE_LIST_TEXT,"dialog_ListManagement_DeleteList_Text");
		click("dialog_ListManagement_DeleteList_Ok");

		boolean listExists = isDefaultListExists();
		if(!listExists){
			List<String> itemToAdd = new ArrayList<String>();
			itemToAdd.add(announcementURN);
			itemToAdd.add(nominalURN);
			itemToAdd.add(polURN);
			listName = createDefaultAndFavList(itemToAdd);
			listExists = isDefaultListExists();
		}
		
		// Select the firstrow list and perform Delete
		explicitlyWaitForElement("checkBox_ListManagement_SearchResult_Fristrow", getTotalWaitTimeInSecs());
		click("checkBox_ListManagement_SearchResult_Fristrow");
		explicitlyWaitForElement("icon_ListManagement_DeleteList", getTotalWaitTimeInSecs());
		click("icon_ListManagement_DeleteList");
		isAlertPresent();
		waitForAlertTextAndClose(DELETE_LIST_CONFIRMATION);
		explicitlyWaitForElement("successMsg_ListManagement", getTotalWaitTimeInSecs());
		String succMess = getText("successMsg_ListManagement");
		String[] listName = succMess.split(" ");
		String deletedListName = listName[1];
		//Verify List is Successfully Deleted
		verifyTextContains(DELETE_SUCESS,"successMsg_ListManagement");
		//Verify Deleted List Name does not get listed 
		click("textField_ListManagement_CurrentListName");
		sendKeysAndEnter("textField_ListManagement_CurrentListName", deletedListName);
		waitForJQuery(driver);
		verifyElementNotPresent("checkBox_ListManagement_SearchResult_Fristrow");

		//Cancel Deletion 
		click("textField_ListManagement_CurrentListName");
		//Previously Deleted List Name was queried and now query by " " to fetch all list items
		sendKeysAndEnter("textField_ListManagement_CurrentListName", "");
		waitForJQuery(driver);
		explicitlyWaitForElement("checkBox_ListManagement_SearchResult_Fristrow", getTotalWaitTimeInSecs());
		String selectedlistName=getText("table_ListManagement_FirstRow_ListName");
		Report.info("selectedListName for Deletion is:"+ selectedlistName);
		verifyElementPresent("checkBox_ListManagement_SearchResult_Fristrow");
		click("checkBox_ListManagement_SearchResult_Fristrow");
		explicitlyWaitForElement("icon_ListManagement_DeleteList", getTotalWaitTimeInSecs());
		click("icon_ListManagement_DeleteList");
		isAlertPresent();
		String alertText = alert.getText();
		//String alertText= closeAlertAndGetItsText(driver);
		alert.dismiss();
		verifyTextCompare(DELETE_LIST_CONFIRMATION,alertText);
		//TODO- No changes in the LIST Deletion
		click("textField_ListManagement_CurrentListName");
		sendKeysAndEnter("textField_ListManagement_CurrentListName", selectedlistName);
		waitForJQuery(driver);
		//Filtering by Name of List and if the 1st row element is present then confirms list is canelled from deletion
		explicitlyWaitForElement("checkBox_ListManagement_SearchResult_Fristrow", getTotalWaitTimeInSecs());
		verifyElementPresent("checkBox_ListManagement_SearchResult_Fristrow");
	}
	
	//Test case 8631:List Mgmt - 004. Edit a list from List Management 
	@Test(priority = 5)
	public void editList(Method method)	throws Exception {
		/*navigateToPage(HOMEPAGE_URL);
		String secondGroup = getSecondIncidentIDFull();
		selectSecondGroupInHomePage(secondGroup);*/
		
		boolean listExists = isDefaultListExists();
		if(!listExists){
			List<String> itemToAdd = new ArrayList<String>();
			itemToAdd.add(announcementURN);
			itemToAdd.add(nominalURN);
			itemToAdd.add(polURN);
			listName = createDefaultAndFavList(itemToAdd);
			listExists = isDefaultListExists();
		}
		
		click("checkBox_ListManagement_SearchResult_Fristrow");
		click("icon_ListManagement_Edit_List");
		waitForPageLoad();

		String listName = "updatelist"+getCurrentDataStampwithoutSlash();
		sendKeysWithDriver("textField_ListManagement_ListName", listName);
		sendKeysWithDriver("textField_ListManagement_ListDescription", "updated test des");
		click("dropDown_ListManagement_IncidentID");
		explicitlyWaitForElement("dropDown_ListManagement_IncidentID_CurrentIncident", getTotalWaitTimeInSecs());

		// Handle Existing list items in different group
		WebElement firstIncidentInput = driver.findElement(map.getLocator("dropDown_ListManagement_IncidentID_FirstIncidentInput"));
		WebElement secondIncidentInput = driver.findElement(map.getLocator("dropDown_ListManagement_IncidentID_SecondIncidentInput"));
		if(isAttribtuePresent(firstIncidentInput, "checked")){
			click("dropDown_ListManagement_IncidentID_SecondIncidentSpan");
			handleExistingListItemsInDifferentGroup();
		} else if(isAttribtuePresent(secondIncidentInput, "checked")){
			click("dropDown_ListManagement_IncidentID_FirstIncidentSpan");
			handleExistingListItemsInDifferentGroup();
		} else {
			click("dropDown_ListManagement_IncidentID_CurrentIncident");
		}
	
		clickAndWaitForPageLoad("button_ListManagement_Save");
		explicitlyWaitForElement("successMsg_ListManagement_Create", getTotalWaitTimeInSecs());
		verifyTextContains("updated","successMsg_ListManagement_Create");
		listExists = isDefaultListExists();
		click("checkBox_ListManagement_SearchResult_Fristrow");
		click("icon_ListManagement_Edit_List");
		waitForPageLoad();
		verifyListItemsTabEnabled();
		clearExistingListItems();
		
		//click add record icon
		explicitlyWaitForElement("icon_ListManagement_AddNewListItem", getTotalWaitTimeInSecs());
		clickAndWaitForPageLoad("icon_ListManagement_AddNewListItem");
		//wait for add record box to appear
		
		List<String> itemsToAdd = new ArrayList<String>();
		itemsToAdd.add(nominalURN);
		itemsToAdd.add(fastActionURN);
		//for each item in the initialised list add to the application list
		for (int i = 0; i < itemsToAdd.size(); i++) {
			explicitlyWaitForElement("button_ListManagement_AddNewListItem_Add", getTotalWaitTimeInSecs());
			sendKeys("textField_ListManagement_AddRecord", itemsToAdd.get(i));
			click("button_ListManagement_AddNewListItem_Add");
			waitForJQuery(driver);
		}
		explicitlyWaitForElement("button_ListManagement_AddNewListItem_Cancel", getTotalWaitTimeInSecs());
		click("button_ListManagement_AddNewListItem_Cancel");
		
		int noOfListItemsBeforeDelete = getNoOfListItemsInListPortlet();
		//Delete first record from the list
		explicitlyWaitForElement("checkBox_ListManagement_ListPortlet_ListItem_First", getTotalWaitTimeInSecs());
		click("checkBox_ListManagement_ListPortlet_ListItem_First");
		explicitlyWaitForElement("icon_ListManagement_DeleteSelectedListItem", getTotalWaitTimeInSecs());
		clickAndWaitForPageLoad("icon_ListManagement_DeleteSelectedListItem");
		explicitlyWaitAndGetWebElement("dialog_ListManagment_ListPortlet_Delete", getTotalWaitTimeInSecs());
		click("dialog_ListManagment_ListPortlet_Delete_Delete");
		int noOfListItemsAfterDelete = getNoOfListItemsInListPortlet();
		if(noOfListItemsAfterDelete == (noOfListItemsBeforeDelete - 1) ){
			Report.pass("List Item Record deleted succesfully");
		} else {
			Report.fail("List item Record NOT deleted");
		}
		
		//click save
		clickAndWaitForPageLoad("button_ListManagement_Save");
		explicitlyWaitForElement("successMsg_ListManagement_Create", getTotalWaitTimeInSecs());
		verifyTextContains("updated","successMsg_ListManagement_Create");
		
		endLine(method);
	}
	
	
	//Test case 9565:List Mgmt - 005. Copy list from List Management 
		   @Test(priority = 6)
	public void copyList(Method method)	throws Exception {
		startLine(method);
		navigateToListManagement(LIST_MANAGEMENT_URL);
		waitForPageLoad();
		explicitlyWaitForElement("header_ListManagement",getTotalWaitTimeInSecs());
		verifyElementPresent("header_ListManagement");
		//wait for first row
		explicitlyWaitForElement("checkBox_ListManagement_SearchResult_Fristrow", getTotalWaitTimeInSecs());
		//click on first checkbox
		click("checkBox_ListManagement_SearchResult_Fristrow");
		//click on viewlist
		click("icon_ListManagement_View_List");
		//get no of records from list
		int noofrecordsexistinginlist = getNoOfListItemsInListPortlet();

		if (noofrecordsexistinginlist==0)
		{
			List<String> itemsToAdd = new ArrayList<String>();
			itemsToAdd.add(nominalURN);
			itemsToAdd.add(fastActionURN);
			itemsToAdd.add(otherDocumentURN);
			itemsToAdd.add(disclosureURN);
			//create a new list
			listName = navigateToListManagmentAndAddItemsToList(itemsToAdd ,"both", true);
			click("button_ListManagement_AddNewListItem_Cancel");
			clickAndWaitForPageLoad("button_ListManagement_Create");
			explicitlyWaitForElement("successMsg_ListManagement_Create", getTotalWaitTimeInSecs());
			verifyTextContains("created","successMsg_ListManagement_Create");
			//enter the listname and select the select
			click("textField_ListManagement_CurrentListName");
			sendKeysAndEnter("textField_ListManagement_CurrentListName", listName);
			waitForJQuery(driver);
			verifyElementPresent("checkBox_ListManagement_SearchResult_Fristrow");
			click("checkBox_ListManagement_SearchResult_Fristrow");
			click("icon_ListManagement_View_List");
			//get no of records in list
			int noofrecordsnewlist = getNoOfListItemsInListPortlet();
			click("icon_ListManagement_ViewList_Back");
			waitForPageLoad();
			//enter listname
			sendKeysAndEnter("textField_ListManagement_CurrentListName", listName);
			waitForJQuery(driver);
			verifyElementPresent("checkBox_ListManagement_SearchResult_Fristrow");
			click("checkBox_ListManagement_SearchResult_Fristrow");
			//click on copy icon
			click("icon_ListManagement_CopyAndCreateNewList");
			sendKeys("textField_ListManagement_ListName", COPYLIST);
			//sendKeys("textField_ListManagement_ListDescription", "test des");
			clickAndWaitForPageLoad("button_ListManagement_Create");
			explicitlyWaitForElement("successMsg_ListManagement_Create", getTotalWaitTimeInSecs());
			verifyTextContains("created","successMsg_ListManagement_Create");
			//enter the listname of copy list
			sendKeysAndEnter("textField_ListManagement_CurrentListName", COPYLIST);
			waitForJQuery(driver);
			verifyElementPresent("checkBox_ListManagement_SearchResult_Fristrow");
			click("checkBox_ListManagement_SearchResult_Fristrow");
			click("icon_ListManagement_View_List");
			int noofrecordscopylists = getNoOfListItemsInListPortlet();
			if(noofrecordscopylists==noofrecordsnewlist)
			{
				Report.pass("List copied with all items");
			}
			else
			{
				Report.fail("List is not copied with all items");
			}

		}

		else{
			navigateToListManagement(LIST_MANAGEMENT_URL);
			waitForPageLoad();
			explicitlyWaitForElement("checkBox_ListManagement_SearchResult_Fristrow", getTotalWaitTimeInSecs());
			click("checkBox_ListManagement_SearchResult_Fristrow");
			click("icon_ListManagement_CopyAndCreateNewList");
			sendKeys("textField_ListManagement_ListName", COPYLIST);
			//sendKeys("textField_ListManagement_ListDescription", "test des");
			clickAndWaitForPageLoad("button_ListManagement_Create");
			explicitlyWaitForElement("successMsg_ListManagement_Create", getTotalWaitTimeInSecs());
			verifyTextContains("created","successMsg_ListManagement_Create");
			sendKeysAndEnter("textField_ListManagement_CurrentListName", COPYLIST);
			waitForJQuery(driver);
			verifyElementPresent("checkBox_ListManagement_SearchResult_Fristrow");
			click("checkBox_ListManagement_SearchResult_Fristrow");
			click("icon_ListManagement_View_List");
			int noofrecordscopylist = getNoOfListItemsInListPortlet();
			if(noofrecordsexistinginlist==noofrecordscopylist)
			{
				Report.pass("List copied with all items");
			}
			else
			{
				Report.fail("List is not copied with all items");
			}

		}
		endLine(method);

	}
	
   @Test(priority = 7)
    public void bulkUpdate(Method method)	throws Exception {
	   startLine(method);
/*	   
	   String announcementURN="ANN1";
		String polURN="POL1";
		String fastActionURN="A1";
		String mirsapActionURN="A2";
		String disclosureURN="DI1";
		String caseFileURN="CF1";
		String englishStatementURN="S1";
		String otherDocumentURN="D1";
		String messageDocumentURN="M1";
		String pdfDocumentURN="P1";
		String questionarrieDocumentURN="Q1";
		String officerReportDocumentURN="R1";
		String interviewDocumentURN="Y1";
		String transmissionDocumentURN="T1";
		String intelligenceDocumentURN="Z1";
		String housetToHouseDocumentURN="H1";
		String policyFileURN="PL1";
		String generalInstructionsURN="GI1";
		String logOfEventsURN="LE1";
		String nominalURN="N1";
		String categoryURN="C1";
		String exhibitURN="X1";
		String locationURN="L1";
		String organisationURN="O1";
		String telephoneURN="TE1";
		String sequenceOfEventsURN="EV1";
		String vehicleURN="V1";
		String callHistoryURN="CL1";
		String bodyPartURN="CN1";//((Survivor/evacuee/casulaity/misper/caller-CN1))
		String simQuestionURN="SQ1";
		String nokURN="NK1";
		String announcementURN1="ANN2";
		String announcementURN2="ANN3";
		String announcementURN3="ANN4";
		String announcementURN4="ANN5";
		String polURN1="POL2";
		String polURN2="POL3";
		String polURN3="POL4";
		String polURN4="POL5";
		String polURN5="POL6";
		
		String fastActionURN1="A3";
		String fastActionURN2="A4";
		String fastActionURN3="A5";
		String fastActionURN4="A5";
		String mirsapActionURN1="A4";
		String disclosureURN1="DI2";
		String caseFileURN1="CF2";
		String englishStatementURN1="S2";
		String otherDocumentURN1="D2";
		String messageDocumentURN1="M2";
		String pdfDocumentURN1="P2";
		String questionarrieDocumentURN1="Q2";
		String officerReportDocumentURN1="R2";
		String interviewDocumentURN1="Y2";
		String transmissionDocumentURN1="T2";
		String intelligenceDocumentURN1="Z2";
		String housetToHouseDocumentURN1="H2";
		String policyFileURN1="PL2";
		String generalInstructionsURN1="GI2";
		String logOfEventsURN1="LE2";
		String nominalURN1="N2";
		String categoryURN1="C2";
		String exhibitURN1="X2";
		String locationURN1="L2";
		String organisationURN1="O2";
		String telephoneURN1="TE2";
		String sequenceOfEventsURN1="EV2";
		String vehicleURN1="V2";
		String callHistoryURN1="CL2";
		String bodyPartURN1="CN2";//((Survivor/evacuee/casulaity/misper/caller-CN1))
		String simQuestionURN1="SQ2";
		String nokURN1="NK2";
	   
		List<String> itemsToAdd = new ArrayList<String>();
		itemsToAdd.add(announcementURN);
		itemsToAdd.add(announcementURN1);
		itemsToAdd.add(announcementURN2);
		itemsToAdd.add(announcementURN3);
		itemsToAdd.add(announcementURN4);
		
		itemsToAdd.add(polURN);
		itemsToAdd.add(polURN1);
		itemsToAdd.add(polURN2);
		itemsToAdd.add(polURN3);
		itemsToAdd.add(polURN4);
		
		itemsToAdd.add(fastActionURN);
		itemsToAdd.add(fastActionURN1);
		itemsToAdd.add(fastActionURN2);
		itemsToAdd.add(fastActionURN3);
		itemsToAdd.add(fastActionURN4);
		
		itemsToAdd.add(mirsapActionURN);
		itemsToAdd.add(disclosureURN);
		itemsToAdd.add(caseFileURN);
		itemsToAdd.add(englishStatementURN);
		itemsToAdd.add(otherDocumentURN);
		itemsToAdd.add(messageDocumentURN);
		itemsToAdd.add(pdfDocumentURN);
		itemsToAdd.add(questionarrieDocumentURN);
		itemsToAdd.add(officerReportDocumentURN);
		itemsToAdd.add(interviewDocumentURN);
		itemsToAdd.add(transmissionDocumentURN);
		itemsToAdd.add(intelligenceDocumentURN);
		itemsToAdd.add(housetToHouseDocumentURN);
		itemsToAdd.add(policyFileURN);
		itemsToAdd.add(generalInstructionsURN);
		itemsToAdd.add(logOfEventsURN);
		itemsToAdd.add(nominalURN);
		itemsToAdd.add(categoryURN);
		itemsToAdd.add(exhibitURN);
		itemsToAdd.add(locationURN);
		itemsToAdd.add(organisationURN);
		itemsToAdd.add(telephoneURN);
		itemsToAdd.add(sequenceOfEventsURN);
		itemsToAdd.add(vehicleURN);
		itemsToAdd.add(callHistoryURN);
		itemsToAdd.add(bodyPartURN);//((Survivor/evacuee/casulaity/misper/caller-CN1))
		itemsToAdd.add(simQuestionURN);
		itemsToAdd.add(nokURN);
		
		
		String listName = createDefaultAndFavList(itemsToAdd);*/
		/*String mirsapURN1;
		String mirsapURN2;
		String mirsapURN3;
		String mirsapURN4;
		String mirsapURN5;
		MirsapAction mirsapActions = new MirsapAction(driver);
		String actionBulk=mirsapActions.raiseBulkAndGetMirsapActionURN("5");
		String[] mirsapactURN_Split = actionBulk.split("-");
		mirsapURN1 = mirsapactURN_Split[0];
		System.out.println(mirsapURN1);
		String mirsapURNcount = mirsapURN1.substring(2);
		
		int mirsapURNcounts = Integer.parseInt(mirsapURNcount)+1;
    	mirsapURN2="AX"+String.valueOf(mirsapURNcounts);
    	
    	int mirsapURNcounts1 = Integer.parseInt(mirsapURNcount)+2;
    	mirsapURN3="AX"+String.valueOf(mirsapURNcounts1);
    	
    	int mirsapURNcounts2 = Integer.parseInt(mirsapURNcount)+3;
    	mirsapURN4="AX"+String.valueOf(mirsapURNcounts2);
    	int mirsapURNcounts3 = Integer.parseInt(mirsapURNcount)+4;
    	mirsapURN5="AX"+String.valueOf(mirsapURNcounts3);
    	
    	
    	System.out.println("mirsapURN1: " + mirsapURN1);
    	System.out.println("mirsapURN2: " + mirsapURN2);
    	System.out.println("mirsapURN3: " + mirsapURN3);
    	System.out.println("mirsapURN4: " + mirsapURN4);
    	System.out.println("mirsapURN5: " + mirsapURN5);*/
    	
    	String actionURN1;
		String actionURN2;
		String actionURN3;
		String actionURN4;
		String actionURN5;
		
		FastAction actions = new FastAction(driver);
		String fastActionBulk = actions.raiseBulkFastActionAndGetURNs("5");
		String[] fastactURN_Split = fastActionBulk.split("-");
		actionURN1 = fastactURN_Split[0];
		String fastActURNcount = actionURN1.substring(2);
		
		int fastActURNcounts = Integer.parseInt(fastActURNcount)+1;
		actionURN2="AX"+String.valueOf(fastActURNcounts);
    	
    	int fastActURNcounts1 = Integer.parseInt(fastActURNcount)+2;
    	actionURN3="AX"+String.valueOf(fastActURNcounts1);
    	
    	int fastActURNcounts2 = Integer.parseInt(fastActURNcount)+3;
    	actionURN4="AX"+String.valueOf(fastActURNcounts2);
    	int fastActURNcounts3 = Integer.parseInt(fastActURNcount)+4;
    	actionURN5="AX"+String.valueOf(fastActURNcounts3);
		
    	System.out.println("actionURN1: " + actionURN1);
    	System.out.println("actionURN2: " + actionURN2);
    	System.out.println("actionURN3: " + actionURN3);
    	System.out.println("actionURN4: " + actionURN4);
    	System.out.println("actionURN5: " + actionURN5);
    	
    	String nominalURN1,nominalURN2,nominalURN3,nominalURN4,nominalURN5;
    	Indexes  nominalIndex = new Indexes(driver);
		
		String  bulkNominal=nominalIndex.createBulkNominalAndGetURNs("Nominal","MALE","5");
		String[] bulkNominal_Split = bulkNominal.split("-");
		nominalURN1 = bulkNominal_Split[0];
		String nominalURNcount = nominalURN1.substring(1);
		int nominalURNcounts = Integer.parseInt(nominalURNcount)+1;
		nominalURN2="N"+String.valueOf(nominalURNcounts);
		int nominalURNcounts1 = Integer.parseInt(nominalURNcount)+2;
		nominalURN3="N"+String.valueOf(nominalURNcounts1);
		int nominalURNcounts2 = Integer.parseInt(nominalURNcount)+3;
		nominalURN4="N"+String.valueOf(nominalURNcounts2);
		int nominalURNcounts3 = Integer.parseInt(nominalURNcount)+4;
		nominalURN5="N"+String.valueOf(nominalURNcounts3);
		
		System.out.println("nominalURN1" + nominalURN1);
    	System.out.println("nominalURN2" + nominalURN2);
    	System.out.println("nominalURN3" + nominalURN3);
    	System.out.println("nominalURN4" + nominalURN4);
    	System.out.println("nominalURN5" + nominalURN5);
	   
    	List<String> itemsToAdd = new ArrayList<String>();
		itemsToAdd.add(nominalURN1);
		itemsToAdd.add(nominalURN2);
		/*itemsToAdd.add(mirsapURN1);
		itemsToAdd.add(mirsapURN2);
		itemsToAdd.add(mirsapURN3);
		itemsToAdd.add(mirsapURN4);
		itemsToAdd.add(mirsapURN5);*/
		itemsToAdd.add(actionURN1);
		itemsToAdd.add(actionURN2);
		itemsToAdd.add(actionURN3);
		itemsToAdd.add(actionURN4);
		itemsToAdd.add(actionURN5);
		itemsToAdd.add(nominalURN3);
		itemsToAdd.add(nominalURN4);
		itemsToAdd.add(nominalURN5);
		
		String listNameBulkUpdate = createListWithNoFavDef(itemsToAdd);
		
		sendKeysWithDriver("textField_ListManagement_Search_ListName", listNameBulkUpdate+Keys.RETURN);
		explicitlyWaitForElement("checkBox_ListManagement_SearchResult_Fristrow", getTotalWaitTimeInSecs());
		checkCheckBox("checkBox_ListManagement_SearchResult_Fristrow");
		click("icon_ListManagement_View_List");
		clickAndWaitForPageLoad("icon_ListManagement_View_List");
		explicitlyWaitForElement("checkBox_ListManagement_ListPortlet_ListItem_First", getTotalWaitTimeInSecs());
		click("checkBox_ListManagement_ListPortlet_ListItem_First");
		explicitlyWaitForElement("checkBox_ListManagement_ListPortlet_ListItem_Second", getTotalWaitTimeInSecs());
		click("checkBox_ListManagement_ListPortlet_ListItem_Second");
		String parentWindowHandle = getCurrentWindowHandle();
		String parentWindowTitle = driver.getTitle();
		int noOfWindows = getWindowCount();
		WebDriver bulkUpdateDriver = clickAndWaitForChildWindow(SOFT_ENTITY_PORTLET_WINDOW_TITLE,"icon_ListManagement_ListPortlet_BulkUpdate", noOfWindows, noOfWindows+1);
		System.out.println(bulkUpdateDriver.getTitle());
		if(!(getCurrentWindowHandle()==parentWindowHandle)){
			getHandleToWindow(bulkUpdateDriver.getTitle());
		}
	   System.out.println(driver.getTitle());
	   explicitlyWaitForElement("dropDown_Action_Create_ProtectiveMarking", getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Action_Create_ProtectiveMarking",
				PROTECTIVE_MARKING_SECRET);
		// change priority
		explicitlyWaitForElement("dropDown_Action_Mirsap_Priority", getTotalWaitTimeInSecs());
		selectByVisibleText("dropDown_Action_Mirsap_Priority", PRIORITY_HIGH);
		//selectByVisibleText("dropDown_Action_Mirsap_Class", MIRSAP_CLASS);
		
		explicitlyWaitForElement("button_ListManagement_BulkUpdate_Bulkpdate", getTotalWaitTimeInSecs());
		click("button_ListManagement_BulkUpdate_Bulkpdate");
		explicitlyWaitForElement("dialog_ListManagement_EntitiesBulkUpdate_Warning_Text", getTotalWaitTimeInSecs());
		verifyTextContains(BULKUPDATE_RECORDS_CONFIRMATION,"dialog_ListManagement_EntitiesBulkUpdate_Warning_Text");
	    click("dialog_ListManagement_EntitiesBulkUpdate_Warning_Ok");
		waitForJQuery(driver);
		explicitlyWaitForElement("dialog_ListManagement_EntitiesBulkUpdate_Information_Text", getTotalWaitTimeInSecs());
		verifyTextContains(BULKUPDATE_RECORDS_INFORMATION,"dialog_ListManagement_EntitiesBulkUpdate_Information_Text");
		click("dialog_ListManagement_EntitiesBulkUpdate_Information_Ok");
//		log_ListManagement_EntitiesBulkUpdate_Information_Ok", noOfWindows1, noOfWindows1-1);
		getHandleToWindow(parentWindowTitle);
		explicitlyWaitForElement("dialog_ListManagement_EntitiesBulkUpdate_Text", getTotalWaitTimeInSecs());
		verifyTextContains(BULKUPDATE_ENITY_RECORDS_INFORMATION,"dialog_ListManagement_EntitiesBulkUpdate_Text");
		click("dialog_ListManagement_EntitiesBulkUpdate_Ok");
		explicitlyWaitForElement("icon_ListManagement_View_List", getTotalWaitTimeInSecs());
		
		// View Bulk updated list items
		String parentWindowHandle1 = getCurrentWindowHandle();
		String parentWindowTitle1 = driver.getTitle();
		int noOfWindows1 = getWindowCount();
		WebDriver viewUpdateDriver = clickAndWaitForChildWindow(LIST_PORTLET_WINDOW_TITLE,"icon_ListManagement_View_List", noOfWindows1, noOfWindows1+1);
	   if(!(getCurrentWindowHandle()==parentWindowHandle)){
		   getHandleToWindow(bulkUpdateDriver.getTitle());
	   }
		waitForJQuery(driver);
		explicitlyWaitForElement("button_ListManagement_ViewBulk_Next", getTotalWaitTimeInSecs());
	   switchToFrame1("frame_ListManagement_ViewListItems");
		String currentPriority = getText("label_Action_Mirsap_View_Priority");
	   System.out.println(currentPriority);
	   verifyTextCompare(PRIORITY_HIGH, currentPriority);
		verifyText(PROTECTIVE_MARKING_SECRET, "label_Action_Mirsap_View_ProtectedMarking");
		//verifyText(MIRSAP_CLASS, "label_Action_Mirsap_View_Class");
	   switchToDefaultContentFrame();
		click("button_ListManagement_ViewBulk_Next");
		waitForJQuery(viewUpdateDriver);
	   switchToFrame1("frame_ListManagement_ViewListItems");

	   explicitlyWaitForElement("label_Action_Mirsap_View_Priority", getTotalWaitTimeInSecs());
		verifyText(PRIORITY_HIGH, "label_Action_Mirsap_View_Priority");
		verifyText(PROTECTIVE_MARKING_SECRET, "label_Action_Mirsap_View_ProtectedMarking");
	   switchToDefaultContentFrame();

	   //verifyText(MIRSAP_CLASS, "label_Action_Mirsap_View_Class");
		click("button_ListManagement_ViewBulk_Close");
		getHandleToWindow(parentWindowTitle1);
    	
	   endLine(method);
   }
	   
	public void handleExistingListItemsInDifferentGroup() throws Exception {
		explicitlyWaitForElement("dialog_ListManagement_ExistingList_Text", getTotalWaitTimeInSecs());
		verifyTextContains(EXISTING_LIST_ITEMS_FROM_DIFFERENT_INCIDENT, "dialog_ListManagement_ExistingList_Text");
		click("dialog_ListManagement_ExistingList_Ok");
	}
	


	//Test case 9574:List Mgmt - 006. Merge more than 1 list in list Management
		@Test(priority = 8)
		public void mergeMoreThanOneList(Method method)	throws Exception {
			startLine(method);
			navigateToListManagement();
			waitForPageLoad();
			explicitlyWaitForElement("header_ListManagement",getTotalWaitTimeInSecs());
			verifyElementPresent("header_ListManagement");
			//wait for first row
			explicitlyWaitForElement("checkBox_ListManagement_SearchResult_Fristrow", getTotalWaitTimeInSecs());
			//click on first checkbox
			click("checkBox_ListManagement_SearchResult_Fristrow");
			//click on viewlist
			click("icon_ListManagement_View_List");
			//get no of records from list
			int noofrecordsinfirstlist = getNoOfListItemsInListPortlet();   //need to change it
			//click on back button in view page
			click("icon_ListManagement_ViewList_Back");
			explicitlyWaitForElement("checkBox_ListManagement_SearchResult_Secondrow", getTotalWaitTimeInSecs());
			//click on first checkbox
			click("checkBox_ListManagement_SearchResult_Secondrow");
			//click on viewlist
			click("icon_ListManagement_View_List");
			//get no of records from list
			int noofrecordsinsecondlist = getNoOfListItemsInListPortlet();
			click("icon_ListManagement_ViewList_Back");
			int noofrecordsmergelist=noofrecordsinfirstlist+noofrecordsinsecondlist;

			//select first two list and click on merge icon
			click("checkBox_ListManagement_SearchResult_Fristrow");
			click("checkBox_ListManagement_SearchResult_Secondrow");
			//click on merge icon
			click("icon_ListManagement_MergeTheSelectedLists");
			int noofrecordsinmerge = getNoOfListItemsInListPortlet();

			//if(noofrecordsmergelist==noofrecordsinmerge)
			//{
				//select a list from dropdown
				click("dropDown_ListManagement_NeworExistingList");
				selectTextByIndex("dropDown_ListManagement_NeworExistingList", 1);
				click("button_ListManagement_AddoNewExistingList_Add");
				click("button_ListManagement_Update_Save");
				verifyTextContains("updated","successMsg_ListManagement_Create");
			//}

			//select first two list and click on merge icon
			click("checkBox_ListManagement_SearchResult_Fristrow");
			click("checkBox_ListManagement_SearchResult_Secondrow");
			//click on merge icon
			click("icon_ListManagement_MergeTheSelectedLists");
			int noofrecordsmerge = getNoOfListItemsInListPortlet();

			/*if(noofrecordsmergelist==noofrecordsmerge)
			{*/
				//enter listname
				String NewlistName = "Newlist"+getCurrentDataStampwithoutSlash();
				sendKeys("textField_ListManagement_ListName", NewlistName);
				sendKeys("textField_ListManagement_ListDescription", "test des");
				clickAndWaitForPageLoad("button_ListManagement_Create");
				explicitlyWaitForElement("successMsg_ListManagement_Create", getTotalWaitTimeInSecs());
				verifyTextContains("created","successMsg_ListManagement_Create");
				sendKeysAndEnter("textField_ListManagement_CurrentListName", NewlistName);
				waitForJQuery(driver);
				verifyElementPresent("checkBox_ListManagement_SearchResult_Fristrow");

		//	}

			endLine(method);

		}
		
		    @Test(priority = 9)
			public void searchListAndIncident(Method method) throws Exception {
				startLine(method);

				List<String> itemsToAdd = new ArrayList<String>();
				itemsToAdd.add(nominalURN);
				listName = navigateToListManagmentAndAddItemsToList(itemsToAdd, "both",
						true);
				clickAndWaitForPageLoad("button_ListManagement_Create");
				// navigate to List management
				navigateToListManagement();
				waitForPageLoad();
				explicitlyWaitForElement("header_ListManagement",
						getTotalWaitTimeInSecs());
				// verify List management header
				verifyElementPresent("header_ListManagement");
				// wait for first row
				explicitlyWaitForElement(
						"checkBox_ListManagement_SearchResult_Fristrow",
						getTotalWaitTimeInSecs());

				sendKeysAndEnter("textField_ListManagement_CurrentListName", listName);
				waitForJQuery(driver);
				verifyElementPresent("checkBox_ListManagement_SearchResult_Fristrow");

				sendKeysAndEnter("textField_ListManagement_Filter_Description", "test");
				waitForJQuery(driver);
				verifyElementPresent("checkBox_ListManagement_SearchResult_Fristrow");

				sendKeysAndEnter("textField_ListManagement_Filter_IncidetId", incidentIdFull);
				waitForJQuery(driver);
				verifyElementPresent("checkBox_ListManagement_SearchResult_Fristrow");

				/*sendKeysAndEnter("textField_ListManagement_CurrentListName", "n%");
				waitForJQuery(driver);
				verifyElementPresent("checkBox_ListManagement_SearchResult_Fristrow"); */

				endLine(method);

			}
			
}
