package Modules.Search.ListManagement;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import Modules.Entity.Entity;
import ReusableUtilities.SeleniumBaseClass;

public class ListManagement extends Entity{

	private static final String CLEAR_THE_LIST_ITEMS = "Are you sure you want to clear the List Item(s)?";
	protected static final String LIST_MANAGEMENT_URL="/group/di/list-management";
	


	public void navigateToListManagement() {
		navigateToPage(LIST_MANAGEMENT_URL);
	}

	public ListManagement() {
		softAssert = new SoftAssert();
	}

	public ListManagement(WebDriver driver) {
		softAssert = new SoftAssert();
		this.driver = driver;
	}

	public boolean isDefaultListExists() throws Exception{
		boolean defaultListExists = false;
		navigateToListManagement();
		//click user tags to put default list on top
		click("label_ListManagement_User_Tags_Sort");
		waitForJQuery(driver);
		//verify if default list is present
		if(getText("label_ListManagement_User_Tag_List1").contains("Default")){
			defaultListExists = true;
		}
		
		return defaultListExists;
	}
	
	//method will verify if a default list is already present
	//if it is present, will enter default list and add records
	//if not present, will create default list and add records
	public void verifyDefaultListPresent(List<String>urnList) throws Exception{
		navigateToListManagement();
		//click user tags to put default list on top
		click("label_ListManagement_User_Tags_Sort");
		waitForJQuery(driver);
		//verify if default list is present
		if(getText("label_ListManagement_User_Tag_List1").contains("Default")){
			//default list does exist - enter it and verify/add records
			explicitlyWaitForElement("checkBox_ListManagement_SearchResult_Fristrow", getTotalWaitTimeInSecs());
			click("checkBox_ListManagement_SearchResult_Fristrow");
			clickAndWaitForPageLoad("icon_ListManagement_View_List");
			//click edit and clear all existing items
			//ULEAF interupption popup handling in firefox
			/*if(isAlertPresent()){
	    		acceptAlertBox(driver);
	    	}*/
			explicitlyWaitForElement("button_ListManagement_Edit", getTotalWaitTimeInSecs());
			clickAndWaitForPageLoad("button_ListManagement_Edit");
			//ULEAF interupption popup handling in firefox
			/*if(isAlertPresent()){
	    		acceptAlertBox(driver);
	    	}*/
			//if there are existing records clear them
			if(getText("label_ListManagement_RecordInfoRight").equals("No records to view")){
				click("checkBox_ListManagement_List_Select_All");
				click("icon_ListManagement_ClearList");
				//click clear in popup
				explicitlyWaitForElement("button_ListManagement_Clear_List_Items", getTotalWaitTimeInSecs());
				click("button_ListManagement_Clear_List_Items");
				waitForJQuery(driver);
			}
			//click add record icon
			explicitlyWaitForElement("icon_ListManagement_AddNewListItem", getTotalWaitTimeInSecs());
			clickAndWaitForPageLoad("icon_ListManagement_AddNewListItem");
			//wait for add record box to appear

			//for each item in the initialised list add to the application list
			for (int i = 0; i < urnList.size(); i++) {
				explicitlyWaitForElement("button_ListManagement_AddNewListItem_Add", getTotalWaitTimeInSecs());
				sendKeys("textField_ListManagement_AddRecord", urnList.get(i));
				click("button_ListManagement_AddNewListItem_Add");
				waitForJQuery(driver);
			}
			explicitlyWaitForElement("button_ListManagement_AddNewListItem_Cancel", getTotalWaitTimeInSecs());
			click("button_ListManagement_AddNewListItem_Cancel");
			//click save
			clickAndWaitForPageLoad("button_ListManagement_Save");
			explicitlyWaitForElement("successMsg_ListManagement_Create", getTotalWaitTimeInSecs());
			verifyTextContains("updated","successMsg_ListManagement_Create");

			//open a new tab and navigate to list management

		}
		else
		{
			//default list does not exist - create it
			createDefaultList(urnList);			
		}		
	}

	public void clearExistingListItems() throws Exception {
		if(getText("label_ListManagement_RecordInfoRight").contains("of")){
			click("checkBox_ListManagement_List_Select_All");
			click("icon_ListManagement_ClearList");
			//click clear in popup
			explicitlyWaitForElement("button_ListManagement_Clear_List_Items", getTotalWaitTimeInSecs());
			verifyTextContains(CLEAR_THE_LIST_ITEMS, "dialog_ListManagment_ListPortlet_ClearList_Text");
			click("button_ListManagement_Clear_List_Items");
			waitForJQuery(driver);
		}
	}
	
	public int getNoOfListItemsInListPortlet() throws Exception{
		int noOfListItems = 0;
		if(getText("label_ListManagement_RecordInfoRight").equals("No records to view")){
			
		} else{
			String getViewValue = getText("label_ListManagement_RecordInfoRight");
			String[] allViewValues = getViewValue.split(" ");
			noOfListItems = Integer.parseInt(allViewValues[allViewValues.length-1]);
		}
		
		return noOfListItems;
	}

	public String createDefaultList(List<String> itemToAdd) throws Exception {
		return createListManagment(itemToAdd, "default", true);
	}
	
	public String createListWithNoFavDef(List<String> itemToAdd) throws Exception {
		return createListManagment(itemToAdd, "NONE", true);
	}

	public String createListManagment(List<String> itemToAdd, final String listType, boolean withOrWithoutIncidentid) throws Exception {
		String listName = navigateToListManagmentAndAddItemsToList(itemToAdd, listType, withOrWithoutIncidentid);
		explicitlyWaitForElement("button_ListManagement_AddNewListItem_Cancel", getTotalWaitTimeInSecs());
		click("button_ListManagement_AddNewListItem_Cancel");
		clickAndWaitForPageLoad("button_ListManagement_Create");
		explicitlyWaitForElement("successMsg_ListManagement_Create", getTotalWaitTimeInSecs());
		verifyTextContains("created","successMsg_ListManagement_Create");
		return listName;

	}
	
	public String navigateToListManagmentAndAddItemsToList(List<String> itemToAdd, final String listType, boolean withOrWithoutIncidentid) throws Exception {
		navigateToListManagement();
		//ULEAF interupption popup handling in firefox
		/*if(isAlertPresent()){
    		acceptAlertBox(driver);
    	}*/
		waitForPageLoad();
		explicitlyWaitForElement("header_ListManagement",
				getTotalWaitTimeInSecs());
		verifyElementPresent("header_ListManagement");
		explicitlyWaitForElement("icon_ListManagement_Create_NewList", getTotalWaitTimeInSecs());
		clickAndWaitForPageLoad("icon_ListManagement_Create_NewList");
		verifyIsInCreateListPage();

		String listName = "Newlist"+getCurrentDataStampwithoutSlash();
		sendKeys("textField_ListManagement_ListName", listName);
		sendKeys("textField_ListManagement_ListDescription", "test des");
		if (withOrWithoutIncidentid==true) {
			click("dropDown_ListManagement_IncidentID");
			click("dropDown_ListManagement_IncidentID_CurrentIncident");
		}

		click("dropDown_ListManagement_ListType");
		if (listType.contains("default")) {
			click("checkBox_ListManagement_DefaultList");
		} else if(listType.contains("favorite")){
			click("checkBox_ListManagement_FavouriteList");
		} else if(listType.contains("both")){
			click("checkBox_ListManagement_DefaultList");
			click("checkBox_ListManagement_FavouriteList");

		}
		clickAndWaitForPageLoad("icon_ListManagement_AddNewListItem");
		// Verify Default Incident ID in Incident DropDown
		verifyTextContains(incidentIdFull, "dropDown_ListManagement_IncidentID");
		//ULEAF interupption popup handling in firefox
		/*if(isAlertPresent()){
    		acceptAlertBox(driver);
    	}*/

		for (int i = 0; i < itemToAdd.size(); i++) {
			explicitlyWaitForElement("textField_ListManagement_AddRecord",getTotalWaitTimeInSecs());
			sendKeys("textField_ListManagement_AddRecord", itemToAdd.get(i));
			click("button_ListManagement_AddNewListItem_Add");
			waitForJQuery(driver);
		}

		return listName;
	}

	public String createDefaultAndFavList(List<String> itemToAdd) throws Exception {
		return createListManagment(itemToAdd, "both", true);
	}

	public String createFavList(List<String> itemToAdd) throws Exception {
		return createListManagment(itemToAdd, "favorite", true);
	}
	public void verifyIsInCreateListPage() throws Exception{
		explicitlyWaitForElement("button_ListManagement_Create", getTotalWaitTimeInSecs());
		verifyElementPresent("button_ListManagement_Create");
		explicitlyWaitForElement("button_ListManagement_Cancel", getTotalWaitTimeInSecs());
		verifyElementPresent("button_ListManagement_Cancel");
		explicitlyWaitForElement("textField_ListManagement_ListName", getTotalWaitTimeInSecs());
		verifyElementPresent("textField_ListManagement_ListName");
		explicitlyWaitForElement("textField_ListManagement_ListDescription", getTotalWaitTimeInSecs());
		verifyElementPresent("textField_ListManagement_ListDescription");
		explicitlyWaitForElement("dropDown_ListManagement_ListType", getTotalWaitTimeInSecs());
		verifyElementPresent("dropDown_ListManagement_ListType");
		explicitlyWaitForElement("dropDown_ListManagement_IncidentID", getTotalWaitTimeInSecs());
		verifyElementPresent("dropDown_ListManagement_IncidentID");
		explicitlyWaitForElement("icon_ListManagement_AddNewListItem", getTotalWaitTimeInSecs());
		verifyElementPresent("icon_ListManagement_AddNewListItem");
		explicitlyWaitForElement("icon_ListManagement_ClearList", getTotalWaitTimeInSecs());
		verifyElementPresent("icon_ListManagement_ClearList");
		explicitlyWaitForElement("icon_ListManagement_RemoveList", getTotalWaitTimeInSecs());
		verifyElementPresent("icon_ListManagement_RemoveList");


	}

	public boolean verifyListItemsTabEnabled(){
		return verifyTabActive("tab_ListManagement_ListItems");
	}
	
	public void verifyIsInViewListItemsPage() throws Exception{
		explicitlyWaitForElement("icon_ListManagement_ViewList_Back", getTotalWaitTimeInSecs());
		verifyElementPresent("icon_ListManagement_ViewList_Back");
		explicitlyWaitForElement("icon_ListManagement_ViewList_Refresh", getTotalWaitTimeInSecs());
		verifyElementPresent("icon_ListManagement_ViewList_Refresh");
		explicitlyWaitForElement("icon_ListManagement_ViewList_ViewListItems", getTotalWaitTimeInSecs());
		verifyElementPresent("icon_ListManagement_ViewList_ViewListItems");
		explicitlyWaitForElement("icon_ListManagement_Viewlist_BulkUpdate", getTotalWaitTimeInSecs());
		verifyElementPresent("icon_ListManagement_Viewlist_BulkUpdate");
		explicitlyWaitForElement("icon_ListManagement_ViewList_VisualiseSelectedListItems", getTotalWaitTimeInSecs());
        verifyElementPresent("icon_ListManagement_ViewList_VisualiseSelectedListItems");
        explicitlyWaitForElement("icon_ListManagement_ViewList_i2analyst", getTotalWaitTimeInSecs());
        verifyElementPresent("icon_ListManagement_ViewList_i2analyst");
        explicitlyWaitForElement("icon_ListManagement_ViewList_SpatiallyMapSelectedRecords", getTotalWaitTimeInSecs());
        verifyElementPresent("icon_ListManagement_ViewList_SpatiallyMapSelectedRecords");
        explicitlyWaitForElement("icon_ListManagement_ViewList_CreateDisclosureRecordsForlistItems", getTotalWaitTimeInSecs());
        verifyElementPresent("icon_ListManagement_ViewList_CreateDisclosureRecordsForlistItems");
        explicitlyWaitForElement("icon_ListManagement_ViewList_ExportToCSV", getTotalWaitTimeInSecs());
        verifyElementPresent("icon_ListManagement_ViewList_ExportToCSV");
        explicitlyWaitForElement("icon_ListManagement_ViewList_MergeSelectedrecords", getTotalWaitTimeInSecs());
        verifyElementPresent("icon_ListManagement_ViewList_MergeSelectedrecords");
        explicitlyWaitForElement("icon_ListManagement_ViewList_ReorderListItems", getTotalWaitTimeInSecs());
        verifyElementPresent("icon_ListManagement_ViewList_ReorderListItems");
		
		
		
	}
	
}


