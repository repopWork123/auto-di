package Modules.Search;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ReusableUtilities.Report;
import ReusableUtilities.SeleniumBaseClass;

public class SmartSearch extends SeleniumBaseClass{
	
	private static final String SMARTSEARCH_URL = "/smart-ui/app/basicSearchInit.do";
	public static String nominal ="N1";
	public static String nominal2 ="N2";
	public static String urn ="S1";
	public static String entityText ="FOSTER";
	public static String document ="D1";
	
	java.util.Date d = new java.util.Date();
	String saveDate = new SimpleDateFormat("ddhhmmss").format(d.getTime());
	
	@BeforeTest
	public void startBrowser() throws Exception {

//		loginAndSelectIncident();
		loginInIEAndSelectIncident();
//		//sleep(4000);
//		Click("link_Search");
		naviageToSmartSearch();
//		//sleep(4000);
//		Click("link_Search_SmartSearch");
//	    //sleep(6000);
//	    getHandleToWindow("ULEAF SMART     Search"); 
//	    getHandleToWindow("ULEAF SMART     Search").manage().window().maximize();
	}
	
	private void naviageToSmartSearch() {
		navigateToPage(SMARTSEARCH_URL);
		
	}

	@Test(priority=1)
	public void smartSearchNominal(Method method) throws Exception {
		startLine(method);
		
		Report.info("******Smart Nominal Search Started ******");
	    //sleep(6000);
	    sendKeys("textField_SmartSearch_QueryText", nominal);
	    //sleep(2000);
	    //SendKeys("textField_SmartSearch_SearchReson", "Create default list");
	   // //sleep(2000);
	    click("button_SmarttSearch_SearchButton");
	    //sleep(4000);
	  
	    if (driver.findElements(By.className("resultGridPagerCenter")).size()>=1){
	      driver.findElement(By.xpath(".//*[@id='WebPartWPQ3']/table/tbody/tr[2]/td/a")).click();
	      //sleep(10000);
	      String linkText= driver.findElement(By.xpath(".//*[@id='WebPartWPQ3']/table/tbody/tr[2]/td/a")).getText();
	      System.out.println(linkText);
	      //sleep(2000);
	      getHandleToWindow(linkText);
	      getHandleToWindow(linkText).close();
	      //sleep(4000);
	    }else{
	      Report.info("No Result Found in Nominal Search");
	    }
	    
	    Report.info("Smart  Search Nominal is Successfull");
	    endLine(method);
	}
	
	@Test(priority=2)
	public void smartSearchURN(Method method) throws Exception {
		startLine(method);
		
		Report.info("******Smart URN Search Started ******");
		
		getHandleToWindow("ULEAF SMART     Search"); 
		//sleep(6000);
		sendKeys("textField_SmartSearch_QueryText", urn);
		//sleep(2000);
		//SendKeys("textField_SmartSearch_SearchReson", "Create default list");
		////sleep(2000);
		click("button_SmarttSearch_SearchButton");
		//sleep(4000);
	  
	    if (driver.findElements(By.className("resultGridPagerCenter")).size()>=1){
	      driver.findElement(By.xpath(".//*[@id='WebPartWPQ3']/table/tbody/tr[2]/td/a")).click();
	      //sleep(10000);
	      String linkText= driver.findElement(By.xpath(".//*[@id='WebPartWPQ3']/table/tbody/tr[2]/td/a")).getText();
	      System.out.println("linkText");
	      //sleep(2000);
	      getHandleToWindow(linkText);
	      getHandleToWindow(linkText).close();
	    }else{
	      Report.info("No Result Found in URN Search");
	    }
	     
	    Report.info("Smart  Search URN is Successfull");
	    endLine(method);
	}
	
	@Test(priority=3)
	public void smartSearchDocument(Method method) throws Exception {
		startLine(method);
		Report.info("******Smart Document Search Started ******");
		
		getHandleToWindow("ULEAF SMART     Search"); 
		//sleep(6000);
		sendKeys("textField_SmartSearch_QueryText", document);
		//sleep(2000);
		//SendKeys("textField_SmartSearch_SearchReson", "Create default list");
		////sleep(2000);
		click("button_SmarttSearch_SearchButton");
		//sleep(4000);
	  
	    if (driver.findElements(By.className("resultGridPagerCenter")).size()>=1){
	      driver.findElement(By.xpath(".//*[@id='WebPartWPQ3']/table/tbody/tr[2]/td/a")).click();
	      //sleep(10000);
	      String linkText= driver.findElement(By.xpath(".//*[@id='WebPartWPQ3']/table/tbody/tr[2]/td/a")).getText();
	      System.out.println("linkText");
	      //sleep(2000);
	      getHandleToWindow(linkText);
	      getHandleToWindow(linkText).close();
	    }else{
	      Report.info("No Result Found in Document Search");
	    }
	    
	    Report.info("Smart  Search Document is Successfull");
	    endLine(method);
	}
	
	@Test(priority=4)
	public void smartSearchEntityText(Method method) throws Exception {
		startLine(method);
		
		Report.info("******Smart EntityText Search Started ******");
		
		getHandleToWindow("ULEAF SMART     Search"); 
		//sleep(6000);
		sendKeys("textField_SmartSearch_QueryText", entityText);
		//sleep(2000);
		//SendKeys("textField_SmartSearch_SearchReson", "Create default list");
		////sleep(2000);
		click("button_SmarttSearch_SearchButton");
		//sleep(4000);
	  
	    if (driver.findElements(By.className("resultGridPagerCenter")).size()>=1){
	      driver.findElement(By.xpath(".//*[@id='WebPartWPQ3']/table/tbody/tr[2]/td/a")).click();
	      //sleep(10000);
	      String linkText= driver.findElement(By.xpath(".//*[@id='WebPartWPQ3']/table/tbody/tr[2]/td/a")).getText();
	      System.out.println("linkText");
	      //sleep(2000);
	      getHandleToWindow(linkText);
	      getHandleToWindow(linkText).close();
	    }else{
	      Report.info("No Result Found in EntityText Search");
	    }
	    
	    Report.info("Smart  Search EntityText is Successfull");
	    endLine(method);
	}
	@Test(priority=5)
	public void recordTypeSelect(Method method) throws Exception {
		startLine(method);
		Report.info("SmarttSearch Record Type Select and Unslect Started");
		//sleep(6000);
		getHandleToWindow("ULEAF SMART     Search"); 
		driver.findElement(By.id("rectypes_head")).click(); 
		//sleep(2000);
		click("link_SmartSearch_RecordType_SelectAll");
		//sleep(2000);
		click("link_SmartSearch_RecordType_ClearAll");
		//sleep(2000);
		click("link_SmartSearch_RecordType_Category_Selected");
		//sleep(2000);
		click("link_SmartSearch_RecordType_Nominal_Selected");
		//sleep(2000);
		driver.findElement(By.id("rectypes_head")).click(); 
		Report.info("SmartSearch  Recrd type select and Unselect Successfull");
		endLine(method);
	}
	
	
}