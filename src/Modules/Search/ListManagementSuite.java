package Modules.Search;

import java.text.SimpleDateFormat;

import org.openqa.selenium.Keys;
import org.testng.annotations.*;

import ReusableUtilities.SeleniumBaseClass;

public class ListManagementSuite extends SeleniumBaseClass{
	java.util.Date d = new java.util.Date();
	String listName = new SimpleDateFormat("ddhhmmss").format(d.getTime());
	
	//String listName = "ListIdeOne1126";
	
     @BeforeTest
		public void startBrowser() throws Exception {
    	 loginAndSelectIncident();

			//loginInIEAndSelectIncident();
			//driver.get("http://ustr-erl-4427.na.uis.unisys.com:8080/group/holmes/list-management");
		}

/*	@AfterTest
		public void closeBrowser() throws Exception {

			closeApp();

		}*/
	
	@Test(priority=1)
	  public void createNormalList() throws Exception {
		
		click("link_Search");
		click("link_ListManagement");
		explicitlyWaitForElement("icon_ListManagement_Create_NewList", 10);
		click("icon_ListManagement_Create_NewList");
		sendKeys("textField_ListManagement_ListName", listName);
		sendKeys("textField_ListManagement_ListDescription", "ideListDes");
		click("button_ListManagement_Create");
		explicitlyWaitForElement("successMsg_ListManagement_Create", 60);
		verifyTextContains("created",
				"successMsg_ListManagement_Create");
		
	  }
	@Test(priority=2)
	  public void createDefaultList() throws Exception {
		   
		click("link_Search");
		click("link_ListManagement");
		//driver.get("http://ustr-erl-4427.na.uis.unisys.com:8080/group/holmes/list-management");
		explicitlyWaitForElement("icon_ListManagement_Create_NewList", 10);
		click("icon_ListManagement_Create_NewList");
		sendKeys("textField_ListManagement_ListName", listName+"Default");
		sendKeys("textField_ListManagement_ListDescription", "ideDefaultListDes");
		click("dropDown_ListManagement_ListType");
		click("link_ListManagement_ListType_UncheckAll");
		click("checkBox_ListManagement_DefaultList");
		click("button_ListManagement_Create");
		explicitlyWaitForElement("successMsg_ListManagement_Create", 60);
		verifyTextContains("created",
				"successMsg_ListManagement_Create");
		
	  }
	@Test(priority=3)
	  public void createFavouriteList() throws Exception {
		
		click("link_Search");
		click("link_ListManagement");
		explicitlyWaitForElement("icon_ListManagement_Create_NewList", 10);
		click("icon_ListManagement_Create_NewList");
		sendKeys("textField_ListManagement_ListName", listName+"Fav");
		sendKeys("textField_ListManagement_ListDescription", "ideFavListDes");
		click("dropDown_ListManagement_ListType");
		click("link_ListManagement_ListType_UncheckAll");
		click("checkBox_ListManagement_FavouriteList");
		click("button_ListManagement_Create");
		explicitlyWaitForElement("successMsg_ListManagement_Create", 60);
		verifyTextContains("created",
				"successMsg_ListManagement_Create");		
		
	  }
	@Test(priority=4)
	  public void createDefaultFavouriteList() throws Exception {
		    	
		click("link_Search");
		click("link_ListManagement");
		explicitlyWaitForElement("icon_ListManagement_Create_NewList", 10);
		click("icon_ListManagement_Create_NewList");
		sendKeys("textField_ListManagement_ListName", listName+"Favs");
		sendKeys("textField_ListManagement_ListDescription", "ideFavListDes");
		click("dropDown_ListManagement_ListType");
		click("link_ListManagement_ListType_CheckAll");
		click("button_ListManagement_Create");
		explicitlyWaitForElement("successMsg_ListManagement_Create", 60);
		verifyTextContains("created",
				"successMsg_ListManagement_Create");
		
	  }
	@Test(priority=5)
	  public void updateList() throws Exception {
		click("link_Search");
		click("link_ListManagement");
		sendKeys("textField_ListManagement_Search_ListName"+Keys.RETURN, listName);
		explicitlyWaitForElement("checkBox_ListManagement_SearchResult_Fristrow", 5);
		click("checkBox_ListManagement_SearchResult_Fristrow");
		click("icon_ListManagement_Edit_List");
		sendKeys("textField_ListManagement_ListDescription", "ideEditedListDes");
		click("button_ListManagement_Update_Save");
		explicitlyWaitForElement("successMsg_ListManagement_Update", 60);
		verifyTextContains("updated",
				"successMsg_ListManagement_Update");
		
	  }
	/*@Test(priority=1)
	  public void updateListToDefaultList() throws Exception {
		    		
		
	  }
	@Test(priority=1)
	  public void updateDefaultListToNormalList() throws Exception {
		    		
		
	  }
	@Test(priority=1)
	  public void  updateFavoriteListToNormalLis() throws Exception {
		    		
		
	  }
	@Test(priority=1)
	  public void updateDefaultListToFavoriteList() throws Exception {
		    		
		
	  }
	@Test(priority=1)
	  public void updateFavoriteListToDefaultList() throws Exception {
		    		
		
	  }
*/
}
