package ReusableUtilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.asserts.SoftAssert;

import atu.testng.reports.listeners.ConfigurationListener;
import atu.testng.reports.listeners.MethodListener;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.thoughtworks.selenium.webdriven.commands.IsAlertPresent;

@Listeners({ ConfigurationListener.class, MethodListener.class })
public class SeleniumBaseClass{


	public String objmapPath = System.getProperty("user.dir")+"\\src\\ObjectLib\\objectMap.properties";
	public String logfilePath = "C://UleafSelenium/logs/";
	public String testDataFN = readConfigFile("RegFileName");
	public String testDataFilePath = System.getProperty("user.dir") + "\\" + testDataFN;
	public String incidentIdFull = getCurrentIncidentIDFull();
	public String baseWindow;
	
	protected String username = getUserName();
	protected String usernameUpperCase = username.toUpperCase();
	
	public static final String HOMEPAGE_URL = "/group/di/home";

	/*protected String username;*/
	protected String password = getPassword();
	public String baseUrl = readConfigFile("TestRunAppURL");
	public WebDriver driver;
	public SoftAssert softAssert;
	public int softAssertCount = 0;
	public static Alert alert = null;
	private static boolean acceptNextAlert = true;

	{
		System.setProperty("atu.reporter.config", System.getProperty("user.dir") + "\\atu.properties");
	}


	public ObjectLib map = new ObjectLib(objmapPath);


	public void openBrowserLaunhApp() 
	{
		try{
			baseUrl=readConfigFile("TestRunAppURL");			
			ProfilesIni profile = new ProfilesIni();
			FirefoxProfile Uleafprofile = profile.getProfile("Selenium_User");					
			/*final String firebugPath = "F:\\Projects\\Uleaf\\Software\\Firefox Selenium Plugins\\extensions\\firebug@software.joehewitt.com.xpi";
			final String fireXpath = "F:\\Projects\\Uleaf\\Software\\Firefox Selenium Plugins\\extensions\\FireXPath@pierre.tholence.com.xpi";
			Uleafprofile.addExtension(new File(firebugPath));
			Uleafprofile.addExtension(new File(fireXpath));*/
			driver = new FirefoxDriver(Uleafprofile);
			
			handleSecurityWindow3();
			sleep(3000);
			driver.get(baseUrl);
			sleep(2000);			
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().pageLoadTimeout(getPageLoadWaitTime(), TimeUnit.SECONDS);
//			driver.manage().timeouts().implicitlyWait(2,TimeUnit.SECONDS);			
			Report.pass("Open Browser and Application is launched");	

		}
		catch(Exception e)
		{			
			Report.fail("Application is not launched " + e);			
		}
	}	

	public void openIEBrowserLaunhApp() 
	{
		try{
			baseUrl=readConfigFile("TestRunAppURL");
			
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			// to clear cache in IE
			capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			capabilities.setCapability("nativeEvents", false);
			capabilities.setCapability("ie.ensureCleanSession", true);
			capabilities.setCapability("unexpectedAlertBehaviour", "accept");
			capabilities.setCapability("ignoreProtectedModeSettings", true);
			capabilities.setCapability("disable-popup-blocking", true);
			capabilities.setCapability("enablePersistentHover", true);

			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\webdriver\\IEDriverServer.exe");
			driver = new InternetExplorerDriver(capabilities);
			handleSecurityWindow1();
			sleep(3000);
			driver.get(baseUrl);
			sleep(2000);			
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
//			driver.manage().timeouts().implicitlyWait(getTotalWaitTimeInSecs(),TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(getPageLoadWaitTime(), TimeUnit.SECONDS);
			Report.pass("Opened IEBrowser and Application is launched");			
		}
		catch(Exception e)
		{			
			Report.fail("Application is not launched " + e);			
		}
	}	

	public void launchApp()
	{
		display_Execution_DateTime();
		openBrowserLaunhApp();
	}

	public void launchAppInIE()
	{
		display_Execution_DateTime();
		openIEBrowserLaunhApp();
	}

	public void closeApp() throws Exception
	{
		try {
			driver.close();
			Report.pass("Browser closed properly");
		} catch (Exception e) {
			Report.fail("Browser did not closed properly");
		}
	}


	/*
	 * Logins and Selects Default group
	 */
	public void loginAndSelectIncident() throws Exception
	{	
		//Login credentials
		/*username=readConfigFile("username");
		password=readConfigFile("password");*/
		launchApp();
		sleep(5000);
		sendKeys("textField_Login_UserName", username);
		sendKeys("textField_Login_PassWord", password);
		explicitlyWaitForElement("button_Login_SignIn", getTotalWaitTimeInSecs());
		click("button_Login_SignIn");
		selectAGroupInHomePage();
		sleep(5000);
	}
	
	/*
	 * Logins and Selects specific incident
	 */
	public void loginAndSelectIncident(String incidentID) throws Exception
	{	
		//Login credentials
		/*username=readConfigFile("username");
		password=readConfigFile("password");*/
		launchApp();
		sleep(5000);
		sendKeys("textField_Login_UserName", username);
		sendKeys("textField_Login_PassWord", password);
		explicitlyWaitForElement("button_Login_SignIn", getTotalWaitTimeInSecs());
		click("button_Login_SignIn");
		selectAGroupInHomePage(incidentID);
		sleep(5000);
	}

	public void selectAGroupInHomePage() {
		explicitlyWaitForElement("radioButton_Home_Select_IncidentId", getTotalWaitTimeInSecs());
		sendKeys("textField_Home_Id",incidentIdFull);
		click("button_Home_Filter");
		if(isAlertPresent()){
			acceptAlertBox(driver);
		}
		waitForPageLoad();
		click("radioButton_Home_Select_IncidentId");
//		checkCheckBox("checkBox_Home_IncidentID_Select_SecondRow");
		click("button_Home_Apply");
		if(isAlertPresent()){
			acceptAlertBox(driver);
		}
		waitForPageLoad();
	}
	
	public void selectSecondGroupInHomePage(String SecondIncidentID) throws Exception {
		explicitlyWaitForElement("radioButton_Home_Select_IncidentId", getTotalWaitTimeInSecs());
		sendKeys("textField_Home_Id",SecondIncidentID);
		click("button_Home_Filter");
		if(isAlertPresent()){
			acceptAlertBox(driver);
		}
		waitForPageLoad();
		checkCheckBox("checkBox_Home_IncidentID_Select_SecondRow");
		click("button_Home_Apply");
		if(isAlertPresent()){
			acceptAlertBox(driver);
		}
		waitForPageLoad();
	}
	
	public void clickAndAcceptAlert(String objname) {
			try
			{
//				driver.findElement(map.getLocator(objname)).click();
//				fluentWaitAndGetWebElementAndClick(getTotalWaitTimeInSecs(), getPoolWaitTime(), objname);
	/*			explicitlyWaitForElement(objname, getTotalWaitTimeInSecs());
				driver.findElement(map.getLocator(objname)).click();*/
				explicityWaitForAnElementAndClick(objname, getTotalWaitTimeInSecs());
				Report.pass("clicked on '"+objname+"'");
				try {
					driver.switchTo().alert().accept();
				} catch (Exception e) {
					 System.out.println("unexpected alert not present");  
				}
				
				
			}
			catch(Exception e)
			{
				Report.fail("element '"+objname+"' is not displayed");
				Assert.fail("element '"+objname+"' is not displayed");
			}
		
	}

	public void selectAGroupInHomePage(String incidentID) {
		navigateToPage(HOMEPAGE_URL);
		explicitlyWaitForElement("radioButton_Home_Select_IncidentId", getTotalWaitTimeInSecs());
		sendKeys("textField_Home_Id",incidentID);
		click("button_Home_Filter");
		
		click("radioButton_Home_Select_IncidentId");
		click("button_Home_Apply");
	}

	public void loginInIEAndSelectIncident() throws Exception
	{	
		//Login credentials
		/*username=readConfigFile("username");
		password=readConfigFile("password");*/
		launchAppInIE();
//		sleep(5000);
		explicitlyWaitForElement("textField_Login_UserName", getTotalWaitTimeInSecs());
		sendKeys("textField_Login_UserName", username);
		sendKeys("textField_Login_PassWord", password);
		click("button_Login_SignIn");
/*		sleep(3000);
		click("radioButton_Home_Select_IncidentId");
		click("button_Home_Apply");
		sleep(5000);*/
		selectAGroupInHomePage();
	}
	
	public void login() throws Exception
	{		
		explicitlyWaitForElement("textField_Login_UserName", getTotalWaitTimeInSecs());
		sendKeys("textField_Login_UserName", username);
		sendKeys("textField_Login_PassWord", password);
		click("button_Login_SignIn");
	}

	public String readConfigFile(String value)
	{
		try{
			Properties props=new Properties();
			FileInputStream fis=new FileInputStream(System.getProperty("user.dir")+"\\Config.properties");
			props.load(fis);
			return props.getProperty(value);			
		}
		catch(Exception e)
		{
			Report.info("Config file is not read");
			return "";
		}
	}
	
	
	public String getCurrentIncidentIDFull(){
		return readConfigFile("incidentID_full");
	}
	
	public String getSecondIncidentIDFull(){
		return readConfigFile("secondIncidentID_full");
	}
	

	/*** Send console result to text file */
	public void logResult(String testScriptName){	
		try	{
			System.setOut(new  PrintStream(new FileOutputStream(logfilePath+"/"+testScriptName+".txt",true)));
		}
		catch(Exception E1)
		{
			Report.info("The Result file has not been found. Please check the path of Results File and Re-Execute");
		}
	}

	public void sleep(int miliSec) throws InterruptedException
	{
		Thread.sleep(miliSec);
	}

	/** print current system date and time*/
	public void display_Execution_DateTime()

	{
		Date myDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy: HH:mm:ss");
		String myDateString = sdf.format(myDate);
		Report.info("............................");
		Reporter.log("............................");
		Report.info("Execution Date/Time:");
		Report.info(myDateString);
		Reporter.log("Execution Startes @ :" + myDateString);
		Report.info("............................");
		Reporter.log("............................");
	}

	/** Prints current time, seconds.This method is useful to append current time seconds with any string*/
	public String getSecondsStamp()
	{
		Date myDate = new Date();
		SimpleDateFormat seconds = new SimpleDateFormat("ss");
		String mySecondsString = seconds.format(myDate);
		return mySecondsString;
	}

	
	public String getCurrentDataStamp()
	{
		Date myDate = new Date();
		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
		String mySecondsString = date.format(myDate);
		return mySecondsString;
	}
	
	//Get current date Time and add 1 day to get future date
	public String getTomorrowDataStamp()
	{
		Date myDate = new Date();
		Calendar c = Calendar.getInstance(); 
		c.setTime(myDate); 
		c.add(Calendar.DATE, 1);
		myDate = c.getTime();
		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
		String mySecondsString = date.format(myDate);
		return mySecondsString;
		
	}
	
	public String getCurrentDataStampwithoutSlash()
	{
		Date myDate = new Date();
		SimpleDateFormat date = new SimpleDateFormat("ddMMyyyyhhmmss");
		String mySecondsString = date.format(myDate);
		return mySecondsString;
	}

	public String randomChar()
	{
		final String alphabet = "0123456789ABCDE";
		final int N = alphabet.length();
		Random r = new Random();

		String randomString=Character.toString(alphabet.charAt(r.nextInt(N)));

		return randomString;
	}

	// To handle security window 1
	public void handleSecurityWindow1() throws IOException
	{
		Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\autoItfiles\\SecurityWindow1.exe");
	}

	// To handle security window 2
	public void handleSecurityWindow2() throws IOException
	{

		Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\autoItfiles\\SecurityWindow2.exe");
	}

	// To handle security window 2
	public void handleSecurityWindow3() throws IOException
	{

		Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\autoItfiles\\SecurityWindow3.exe");
	}

	public void sendKeys(String objname, String value)
	{
		try
		{
			/*System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\AutoItfiles\\IEDriverServer32bit.exe");
			Report.info("System webdriver ie driver value is:" +System.getProperty("webdriver.ie.driver"));*/
			WebElement e = explicitlyWaitAndGetWebElement(objname, getTotalWaitTimeInSecs());
			/*e.click();
			e.clear();
			e.sendKeys(value);*/
//			driver.findElement(map.getLocator(objname)).clear();
//			driver.findElement(map.getLocator(objname)).sendKeys(value);
			/*WebElement searchbox = driver.findElement(By.xpath("//input[@name='q']"));*/
			JavascriptExecutor myExecutor = ((JavascriptExecutor) driver);
			myExecutor.executeScript("arguments[0].value='" + value + "';", e);
			
			Report.pass("'"+value+"' is entered in '"+objname+"'");
			/*System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\AutoItfiles\\IEDriverServer32bit.exe");
			Report.info("System webdriver ie driver value is:" +System.getProperty("webdriver.ie.driver"));*/
		}
		catch(Exception e)
		{
			Report.fail("element '"+objname+"' is not displayed");
			Assert.fail("element '"+objname+"' is not displayed");
		}
	}
	
	public void sendKeysWithDriverNoClick(String objname, String value)
	{
		try
		{
			WebElement e = explicitlyWaitAndGetWebElement(objname, getTotalWaitTimeInSecs());
			e.clear();
			e.sendKeys(value);
			Report.pass("'"+value+"' is entered in '"+objname+"'");
		}
		catch(Exception e)
		{
			Report.fail("element '"+objname+"' is not displayed");
			Assert.fail("element '"+objname+"' is not displayed");
		}
	}
	
	
	public void sendKeysWithDriver(String objname, String value)
	{
		try
		{
			/*System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\AutoItfiles\\IEDriverServer32bit.exe");
			Report.info("System webdriver ie driver value is:" +System.getProperty("webdriver.ie.driver"));*/
			WebElement e = explicitlyWaitAndGetWebElement(objname, getTotalWaitTimeInSecs());
			e.click();
			e.clear();
			e.sendKeys(value);
			Report.pass("'"+value+"' is entered in '"+objname+"'");
		}
		catch(Exception e)
		{
			Report.fail("element '"+objname+"' is not displayed");
			Assert.fail("element '"+objname+"' is not displayed");
		}
	}
	
	public void sendKeysAndEnter(String objname, String value)
	{
		try
		{
			WebElement e = explicitlyWaitAndGetWebElement(objname, getTotalWaitTimeInSecs());
			e.click();
			e.clear();
			/*e.sendKeys(value);*/
			JavascriptExecutor myExecutor = ((JavascriptExecutor) driver);
			myExecutor.executeScript("arguments[0].value='" + value + "';", e);
			e.sendKeys(Keys.RETURN);
			
//			driver.findElement(map.getLocator(objname)).clear();
//			driver.findElement(map.getLocator(objname)).sendKeys(value);
			Report.pass("'"+value+"' is entered in '"+objname+"'");
		}
		catch(Exception e)
		{	e.printStackTrace();
			Report.fail("element '"+objname+"' is not displayed");
			Assert.fail("element '"+objname+"' is not displayed");
		}
	}
	
	public void sendKeysAndKeyDown(String objname, String value)
	{
		try
		{
			WebElement e = explicitlyWaitAndGetWebElement(objname, getTotalWaitTimeInSecs());
			e.click();
			e.clear();
			/*e.sendKeys(value);*/
			JavascriptExecutor myExecutor = ((JavascriptExecutor) driver);
			myExecutor.executeScript("arguments[0].value='" + value + "';", e);
			e.sendKeys(Keys.DOWN);
			
//			driver.findElement(map.getLocator(objname)).clear();
//			driver.findElement(map.getLocator(objname)).sendKeys(value);
			Report.pass("'"+value+"' is entered in '"+objname+"'");
		}
		catch(Exception e)
		{
			Report.fail("element '"+objname+"' is not displayed");
			Assert.fail("element '"+objname+"' is not displayed");
		}
	}
	
	public void sendKeysAndArrowDown(String objname, String value)
	{
		try
		{
			WebElement e = explicitlyWaitAndGetWebElement(objname, getTotalWaitTimeInSecs());
			e.click();
			/*e.sendKeys(value);*/
			JavascriptExecutor myExecutor = ((JavascriptExecutor) driver);
			myExecutor.executeScript("arguments[0].value='" + value + "';", e);
			String selectAll = Keys.chord(Keys.CONTROL, "a");
			e.sendKeys(selectAll);
			sleep(4000);
			Actions builder = new Actions(driver);
			/*Action arrowdown = builder.keyDown(Keys.ARROW_DOWN).build();
			arrowdown.perform();*/
			
			Action moveCursorLeft = builder.moveToElement(e).moveByOffset(-2, 0).build();
			moveCursorLeft.perform();
			/*e.sendKeys(Keys.CONTROL);
			e.sendKeys(Keys.ARROW_DOWN);*/
			
//			driver.findElement(map.getLocator(objname)).clear();
//			driver.findElement(map.getLocator(objname)).sendKeys(value);
			Report.pass("'"+value+"' is entered in '"+objname+"'");
		}
		catch(Exception e)
		{
			Report.fail("element '"+objname+"' is not displayed");
			Assert.fail("element '"+objname+"' is not displayed");
		}
	}
	
	public void sendKeysAndEnterWithDriver(String objname, String value)
	{
		try
		{
			WebElement e = explicitlyWaitAndGetWebElement(objname, getTotalWaitTimeInSecs());
			e.click();
			e.clear();
			e.sendKeys(value);
			e.sendKeys(Keys.RETURN);
			
//			driver.findElement(map.getLocator(objname)).clear();
//			driver.findElement(map.getLocator(objname)).sendKeys(value);
			Report.pass("'"+value+"' is entered in '"+objname+"'");
		}
		catch(Exception e)
		{	
			e.printStackTrace();
			Report.fail("element '"+objname+"' is not displayed");
			Assert.fail("element '"+objname+"' is not displayed");
		}
	}
	public void sendKeysAndTab(String objname, String value){
		try{
			WebElement e = explicitlyWaitAndGetWebElement(objname, getTotalWaitTimeInSecs());
			e.click();
			e.clear();
			/*e.sendKeys(value + Keys.TAB);*/
			JavascriptExecutor myExecutor = ((JavascriptExecutor) driver);
			myExecutor.executeScript("arguments[0].value='" + value + "';", e);
			e.sendKeys(Keys.TAB);
			e.sendKeys(Keys.RETURN);
			
//			driver.findElement(map.getLocator(objname)).clear();
//			driver.findElement(map.getLocator(objname)).sendKeys(value);
			Report.pass("'"+value+"' is entered in '"+objname+"'");
		} catch(Exception e){
			Report.fail("element '"+ objname +"' is not displayed");
			Assert.fail("element '"+ objname +"' is not displayed");
		}
	}
	
	public void sendKeysAndTabWithDriver(String objname, String value){
		try{
			WebElement e = explicitlyWaitAndGetWebElement(objname, getTotalWaitTimeInSecs());
			e.click();
			e.clear();
			e.sendKeys(value + Keys.TAB);
			e.sendKeys(Keys.TAB);
			e.sendKeys(Keys.RETURN);
//			driver.findElement(map.getLocator(objname)).clear();
//			driver.findElement(map.getLocator(objname)).sendKeys(value);
			Report.pass("'"+value+"' is entered in '"+objname+"'");
		} catch(Exception e){
			Report.fail("element '"+ objname +"' is not displayed");
			Assert.fail("element '"+ objname +"' is not displayed");
		}
	}
	
	public void selectOptionWithText(String textToSelect, String autoCompleteDivIdentiferInfo) {
		try {
						
			Report.info("Option to select:'" + textToSelect + "'" + " Or '" + textToSelect.toLowerCase() + "'" + " Or '" + textToSelect.toUpperCase() + "'" );
			WebElement autoOptions = explicitlyWaitAndGetWebElement(autoCompleteDivIdentiferInfo, getTotalWaitTimeInSecs());
			WebDriverWait wait = new WebDriverWait(driver,	getTotalWaitTimeInSecs());
			wait.until(ExpectedConditions.visibilityOf(autoOptions));
			List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("div"));
			// To find the different suggestion values
			for(WebElement option : optionsToSelect){
				Report.info("Values: "+ "'" + option.getAttribute("val") + "'");
		    }
			for(WebElement option : optionsToSelect){
				String attVal = option.getAttribute("val");
				/*Report.info("Att Value '" + attVal + "'");
				Report.info("value evalution '" + attVal.contains(textToSelect));
				Report.info("value evalution '" + attVal.contains(textToSelect.toLowerCase()));
				Report.info("value evalution '" + attVal.contains(textToSelect.toUpperCase()));
				Report.info("value evalution '" + textToSelect.equalsIgnoreCase(attVal));*/
		        if(attVal.contains(textToSelect)	|| 
		        		attVal.contains(textToSelect.toLowerCase()) || 
		        		attVal.contains(textToSelect.toUpperCase()) || 
		        		textToSelect.equalsIgnoreCase(attVal)) {
		        	Report.info("Trying to select: "+textToSelect);
		            option.click();
		            Report.info("Clicked the required option");
		            break;
		        }
		    }
			
		} catch (NoSuchElementException e) {
			System.out.println(e.getStackTrace());
		}
		catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public void doubleClickAndWait(String objname){
		try{
			explicityWaitForAnElementAndDoubleClick(objname, getTotalWaitTimeInSecs());
			Report.pass("Double Clicked on '"+ objname +"'");
			waitForPageLoad();
		}catch(Exception e){
			Report.fail("element '"+objname+"' is not displayed");
			Assert.fail("element '"+objname+"' is not displayed");
		}
	}
	
	
	public void doubleClick(String objname)
	{
		try
		{
			explicityWaitForAnElementAndDoubleClick(objname, getTotalWaitTimeInSecs());
			Report.pass("Double Clicked on '"+ objname +"'");
		}
		catch(Exception e)
		{
			Report.fail("element '"+objname+"' is not displayed");
			Assert.fail("element '"+objname+"' is not displayed");
		}
	}
	

	public void click(String objname)
	{
		try
		{
//			driver.findElement(map.getLocator(objname)).click();
//			fluentWaitAndGetWebElementAndClick(getTotalWaitTimeInSecs(), getPoolWaitTime(), objname);
/*			explicitlyWaitForElement(objname, getTotalWaitTimeInSecs());
			driver.findElement(map.getLocator(objname)).click();*/
			explicityWaitForAnElementAndClick(objname, getTotalWaitTimeInSecs());
			Report.pass("clicked on '"+objname+"'");
		}
		catch(Exception e)
		{
			Report.fail("element '"+objname+"' is not displayed");
			Assert.fail("element '"+objname+"' is not displayed");
		}
	}
	
	public void clickAndWait(String objname)
	{
		try
		{
//			fluentWaitAndGetWebElementAndClick(getTotalWaitTimeInSecs(), getPoolWaitTime(), objname);
			explicityWaitForAnElementAndClick(objname, getTotalWaitTimeInSecs());
//			driver.findElement(map.getLocator(objname)).click();
			Report.pass("clicked on '"+objname+"'");
			waitForJQuery(driver);
			
		}
		catch(Exception e)
		{
			Report.fail("element '"+objname+"' is not displayed");
			Assert.fail("element '"+objname+"' is not displayed");
		}
	}
	
	public void clickAndWaitForPageLoad(String objname)
	{
		try
		{
			explicityWaitForAnElementAndClick(objname, getTotalWaitTimeInSecs());
//			driver.findElement(map.getLocator(objname)).click();
			Report.pass("clicked on '"+objname+"'");
			waitForPageLoad();
			
		}
		catch(Exception e)
		{
			Report.fail("element '"+objname+"' is not displayed");
			Assert.fail("element '"+objname+"' is not displayed");
		}
	}

	public void clickMenu(String objname)
	{
		try
		{
//			driver.findElement(map.getLocator(objname)).click();
//			fluentWaitAndGetWebElementAndClick(getTotalWaitTimeInSecs(), getPoolWaitTime(), objname);
/*			explicitlyWaitForElement(objname, getTotalWaitTimeInSecs());
			driver.findElement(map.getLocator(objname)).click();*/
			explicityWaitForAnElementAndClick(objname, getTotalWaitTimeInSecs());
			Report.pass("clicked on '"+objname+"'");
		}
		catch(Exception e)
		{
			Report.fail("element '"+objname+"' is not displayed");
			Assert.fail("element '"+objname+"' is not displayed");
		}
	}
	
	
/*	protected boolean IsElementPresent(String objname) throws Exception {

		try {
			driver.findElement(map.getLocator(objname));
			return true;
		} catch (NoSuchElementException e)
		{
			return false;
		}
	}*/

	public boolean isElementPresent(String objname) throws Exception {

		try {
			if(driver.findElement(map.getLocator(objname)) != null){
				//Report.info("Element '" + objname + "' is present");
				return true;
			}else{
				Report.info("Element '" + objname + "' is NOT present");
				return false;
			}
			
		} catch (NoSuchElementException e){
			return false;
		} catch (Exception e){
			return false;
		}
	}
	
	public boolean isElementPresent(WebElement element) {
		boolean flag = false;
		try {
			if (element.isDisplayed() || element.isEnabled())
				flag = true;
			Report.info("Element '" + element + "' is present");
		} catch (NoSuchElementException e) {
			flag = false;
//			e.printStackTrace();
			Report.fail("Element '" + element + "' is NOT present");
		} catch (StaleElementReferenceException e) {
			flag = false;
//			e.printStackTrace();
			Report.fail("Element '" + element + "' is NOT present");
		}
		return flag;
	}
	
	public boolean isAttribtuePresent(WebElement element, String attribute) {
	    Boolean result = false;
	    String tagName = element.getTagName();
	    try {
	        String value = element.getAttribute(attribute);
	        if (value != null){
	        	Report.info("Attribute '" + attribute + "' for Element '" + tagName + "' is present");
	            result = true;
	        }
	    } catch (Exception e) {
	    	Report.info("Got in exception for the is attribute present");
	    }	    
	    Report.info("Attribute '" + attribute + "' for Element '" + tagName + "' is NOT present");
	    return result;
	}
	
	public boolean isAttribtuePresent(String objName, String attribute) {
	    Boolean result = false;
	    WebElement element = explicitlyWaitAndGetWebElement(objName, getTotalWaitTimeInSecs());
	    String tagName = "";
		try {
			tagName = element.getTagName();
		} catch (StaleElementReferenceException e1) {
			//e1.printStackTrace();
			element = explicitlyWaitAndGetWebElement(objName, getTotalWaitTimeInSecs());
			tagName = element.getTagName();
		} catch (Exception e1) {
			//e1.printStackTrace();
			Report.warn("could not able to get the TagName");
		}
	    try {
	        String value = element.getAttribute(attribute);
	        if (value != null){
	        	Report.info("Attribute '" + attribute + "' for Element '" + tagName + "' is present");
	            result = true;
	        }
	    } catch (Exception e) {
	    	Report.info("Got in exception for the is attribute present");
	    }	    
	    Report.info("Attribute '" + attribute + "' for Element '" + tagName + "' is NOT present");
	    return result;
	}
	
	public boolean verifyElementDisplayed(String objname) throws Exception {

		try {
			WebElement element = driver.findElement(map.getLocator(objname));
			if(element != null && element.isDisplayed()){
				Report.info("Element '" + objname + "' is displayed");
				return true;
			}else{
				Report.info("Element '" + objname + "' is NOT displayed");
				return false;
			}
			
		} catch (NoSuchElementException e){
			return false;
		}
	}
	

	public void assertTrue (String objname) throws Exception{
		try{
			Assert.assertTrue(isElementPresent(objname));
			Report.pass("'"+objname+"' is Present");
		} catch (Error e){
//			Report.fail("'"+objname+"' is not Present");
			Assert.fail("'"+objname+"' is not Present");
		}

	}
	
	public void verifyTrue (String objname) throws Exception{
		//softAssert.assertTrue(isElementPresent(objname));
		if(isElementPresent(objname)){
			Report.pass("'"+objname+"' is Present");
		} else {
			softAssertCount++;
			Report.warn("'"+objname+"' is not Present");
			softAssert.fail("'"+objname+"' is not Present");
		}
		
	}

	private boolean isElementDisplayed(String objName) {
		try {
			if (driver.findElement(map.getLocator(objName)).isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			//e.printStackTrace();
			Report.fail("Locator type is not defined!!");
		}
		return false;
	}

	public void verifyElementPresent(String objname) throws Exception {

		try{
			verifyTrue(objname);
		} 
		catch (Error e){
			Report.fail("'" + objname + "' is not Present");
			//verificationErrors.append(e.toString());
		}
	}

	public void assertElementPresent(String objname) throws Exception {

		try 
		{
			assertTrue(objname);
		} 
		catch (Error e)
		{
			Report.fail("'" + objname + "' is not displayed");
			//verificationErrors.append(e.toString());
		}
	}
	
	public void verifyElementIsMandatory(String objname) throws Exception {
		explicitlyWaitForElement(objname, getTotalWaitTimeInSecs());
		if (getText(objname).contains("*")) {
			Report.pass("The element '"+ objname +"' is marked as mandatory");	
		} else{
			Report.warn("'"+ objname +"' is Not marked as mandatory: "+ getText(objname));
		}
	}

	public void verifyElementNotPresent(String objname) throws Exception {
		Boolean flag = false;		

		try{
			flag = (isElementPresent(objname));
			if (flag==true){
				Report.fail("'"+objname+"' is displayed");
			}else
			{
				Report.pass("'" + objname + "' is not displayed");
			}

		} 
		catch (Error e)
		{
			Report.pass("'" + objname + "' is not displayed");
			//verificationErrors.append(e.toString());
		}
	}

	/** getText method (objname = the object name from objectmap file) */
	public String getText(String objname) throws Exception{
//		WebElement element = driver.findElement(map.getLocator(objname));
		WebElement element = explicitlyWaitAndGetWebElement(objname, getTotalWaitTimeInSecs());
		/*WebDriverWait wait = new WebDriverWait(driver, getTotalWaitTimeInSecs());
		wait.until(ExpectedConditions.elementToBeClickable(element));*/
		
		/*.until(ExpectedConditions.not(ExpectedConditions.stalenessOf(element))*/
		
		/*new WebDriverWait(driver, getTotalWaitTimeInSecs())
		.ignoring(StaleElementReferenceException.class)		
		.until(new Predicate<WebDriver>(){
			@Override
			public boolean apply(@Nullable WebDriver driver) {
				try {
					driver.findElement(map.getLocator(objname)).getText();
				} catch (Exception e) {
					//e.printStackTrace();
					Report.fail("Could not able to find the element in wait time: '"+ objname + "' " + getTotalWaitTimeInSecs() + " Seconds");
					Assert.fail("Could not able to find the element in wait time: '"+ objname + "' " +getTotalWaitTimeInSecs() + " Seconds");

				}
				return true;
			}
			
		});*/
		
		String tagName = explicitlyWaitAndGetWebElementTagName(objname, getTotalWaitTimeInSecs());
		String elementText = explicitlyWaitAndGetWebElementText(objname, getTotalWaitTimeInSecs());
		
		if(tagName.equals("input")){
			Report.info(element.getTagName() + "," + element.getAttribute("type") + ",");
			if((elementText.trim().equals("")) && (element.getAttribute("type").equals("text"))){
				return element.getAttribute("value");
			} else{
				return elementText;
			}
		} else {
			return elementText;
		}
	}
	
	
	


	public String getTextFrmTextfieldWhileInserting(String objname) throws Exception{
		WebElement element = driver.findElement(map.getLocator(objname));
	JavascriptExecutor js = (JavascriptExecutor) driver;
	    return (String) js.executeScript(" return jQuery(arguments[0]).contents().filter(function() { return this.nodeType == Node.TEXT_NODE;}).text();", element);
	}
	
	public void validateAllVerificationPoints() {
		if (softAssert == null) {
			softAssert = new SoftAssert();
			softAssert.assertAll();
			
		} else{
			try {
				softAssert.assertAll();
			} catch (AssertionError e) {
				if(softAssertCount > 0){
					try {
						Assert.fail("Failed because of a warning noticed Above:");
					} catch (Exception e2) {
					}
				}
			}
		}
	}

	/** verifyTextPresent method (objname = the object name from objectmap file) */
	public void verifyText(String string_to_compare, String objname) throws Exception{
		explicitlyWaitForElement(objname, getTotalWaitTimeInSecs());
		String elementText = getText(objname);
		softAssert.assertEquals(string_to_compare, elementText,"Comparsion failed: expected text '" + string_to_compare +"' is not same as actual text '" + elementText + "'");
		if(string_to_compare.equals(elementText)){
			Report.pass("The text '"+string_to_compare+"' is verified");
		}else{
			softAssertCount++;
			Report.warn("For object '"+ objname + "' expected text '" + string_to_compare +"' is not same as actual text '" + elementText + "'");
			softAssert.fail("For object '"+ objname + "' expected text '" + string_to_compare +"' is not same as actual text '" + elementText + "'");
		}
	}
	
	/** verifyTinymceElementtext method (elementText = returns textInserted from selenium base class ) */
	public void verifyTextCompare(String string_to_compare, String elementText)throws Exception{
		softAssert.assertEquals(string_to_compare, elementText,"Comparsion failed: expected text '" + string_to_compare +"' is not same as actual text '" + elementText + "'");
		if(string_to_compare.contains(elementText)){
			Report.pass("The text '"+string_to_compare+"' is verified");
		}else{
			softAssertCount++;
			Report.warn("expected text '" + string_to_compare +"' is not same as actual text '" + elementText + "'");
			softAssert.fail("expected text '" + string_to_compare +"' is not same as actual text '" + elementText + "'");
		}
	}
	
	
	
	/** verifyTextPresent method (objname = the object name from objectmap file) */
	public void verifyTextNotEquals(String string_to_compare, String objname) throws Exception{
		explicitlyWaitForElement(objname, getTotalWaitTimeInSecs());
		String elementText = getText(objname);
		softAssert.assertNotEquals(string_to_compare, elementText,"Comparsion failed: expected text '" + string_to_compare +"' is same as actual text '" + getText(objname) + "'");
		if(!string_to_compare.equals(elementText)){
			Report.pass("The text '"+string_to_compare+"' is not maching with " + elementText);
		}else{
			softAssertCount++;
			Report.warn("For object '"+ objname + " expected text '" + string_to_compare +"' is same as actual text '" + getText(objname) + "'");
			softAssert.fail("For object '"+ objname + " expected text '" + string_to_compare +"' is same as actual text '" + getText(objname) + "'");
		}
	}
	
	public void verifyTextOnlyNotEquals(String string_to_compare, String elementText) throws Exception{
			softAssert.assertNotEquals(string_to_compare, elementText,"Comparsion failed: expected text '" + string_to_compare +"' is same as element text '" + elementText + "'");
		if(!string_to_compare.equals(elementText)){
			Report.pass("The text '"+string_to_compare+"' is not maching with " + elementText);
		}else{
			softAssertCount++;
			Report.warn("expected text '" + string_to_compare +"' is same as element text '" + elementText + "'");
			softAssert.fail(" expected text '" + string_to_compare +"' is same as element text '" + elementText + "'");
		}
	}
	
	public void assertText(String string_to_compare, String objname) throws Exception{
		try {
			if (isElementPresent(objname)) {
				Assert.assertEquals(string_to_compare, getText(objname));
				Report.pass("The text '"+string_to_compare+"' is verified");
			} else {
				explicitlyWaitForElement(objname, getTotalWaitTimeInSecs());
				Assert.assertEquals(string_to_compare, getText(objname));
				Report.pass("The text '"+string_to_compare+"' is verified");
			}
			
		} 
		catch (Error e) 
		{
			Report.fail("'"+ objname +"' is not as expected and found to be as: "+ getText(objname));
			Assert.fail("'"+ objname +"' is not as expected and found to be as: "+ getText(objname));

		}
	}
	
	/** verifyTextPresent method (objname = the object name from objectmap file) */
	public void verifyTextPresent(String string_to_compare, String objname, long waitTimeInSec) throws Exception
	{
		try {
//			Assert.assertEquals(string_to_compare, GetText(objname));
			Assert.assertEquals(string_to_compare, explicitlyWaitAndGetWebElementText(objname, waitTimeInSec));
			Report.pass("The text '"+string_to_compare+"' is verified");
		} 
		catch (Error e) 
		{
			Report.fail("'"+objname+"' is not as expected and found to be as:"+ getText(objname));

		}
	}
	

	/** verifyTextContains method (objname = the object name from objectmap file) */
	public void verifyTextContains(String string_to_compare, String objname) throws Exception
	{
		try {
			getText(objname).contains(string_to_compare);
			Report.pass("The text '"+string_to_compare+"' is verified");
		} 
		catch (Error e) 
		{
			Report.fail("'"+objname+"' is not as expected");

		}
	}


	public String getTitle(){

		String title=driver.getTitle();
		return title;
	}

	public void verifyTitle(String title_to_be_matched) throws Exception
	{
		String actulTitel=getTitle();

		try {
			Assert.assertEquals(actulTitel, title_to_be_matched);
			//actulTitel.equalsIgnoreCase(title_to_be_matched);
			Report.pass("The title '"+title_to_be_matched+"' is verified");
		} catch (Error e) {

			Report.fail("The given title: "+title_to_be_matched+" is not matched and found to be: "+actulTitel);
			Assert.fail("The given title: "+title_to_be_matched+" is not matched and found to be: "+actulTitel);
		}

	}

	public int getTableRowCount(String objname){

		try {
			return driver.findElements(map.getLocator(objname)).size();
		} catch (Exception e) {
			Report.fail("Table '"+objname+"' is not displayed");
			Assert.fail("Table '"+objname+"' is not displayed");
			return 0;
		}

	}

	public void verifyTableRowCount(String objname, int expValue ){
		int actualVaule=0;
		try {
			actualVaule = getTableRowCount(objname);
			Report.info("Table contains"+actualVaule+" rows");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			Report.fail("Table '"+objname+"' is not displayed");
			Assert.fail("Table '"+objname+"' is not displayed");
		}
		if (actualVaule == expValue){

			Report.pass("Verified table rows and the Table contains:"+actualVaule+" row");
		} else {

			Report.fail("Actual table row count:"+actualVaule+" and expected table row count:"+expValue+" did not match");
			Assert.fail("Table '"+objname+"' is not displayed");
		}

	}

	public void waitForAlertTextAndClose(String text_tobe_verified){

		try {
			
			WebDriverWait wait = new WebDriverWait(driver, getTotalWaitTimeInSecs());
			wait.until(ExpectedConditions.alertIsPresent());
			
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			alert.accept();

			try {
				alertText.contains(text_tobe_verified);
				Report.pass("The text '"+text_tobe_verified+"' in the alert is verified");
			} catch (Error e) {

				Report.fail("The given text : "+text_tobe_verified+" is not matched in the alert or the alert may not be present");
			}
		} catch (Error e) {
			Report.fail("Alert is not present to verify: "+text_tobe_verified);
		}

	}
	
	public boolean isAlertPresent(){

		boolean ispresent = false;

		try{
			alert = driver.switchTo().alert();
			ispresent= true;
		} catch(NoAlertPresentException e){
			ispresent = false;
		}
		return ispresent;
	}
	
	/**
	 * <p>
	 * The method finds existence of an Alert in the web page, and returns 
	 * either true or false
	 * </p>
	 * @param driver The {@link WebDriver } Instance
	 * @return Returns boolean true or false
	 */	
	public boolean isAlertPresent(WebDriver driver) {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}
	
	/**
	 * <p>
	 * The method gets text of an existing alert in webpage and cancels the alert
	 * </p>
	 * @param driver The {@link WebDriver } Instance
	 */	
	public String closeAlertAndGetItsText(WebDriver driver) {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
	
	/**
	 * <p>
	 * The method accepts the alert box that is currently displayed.
	 * </p>
	 * @param driver The {@link WebDriver } Instance
	 */
	public void acceptAlertBox(WebDriver driver) {
		driver.switchTo().alert().accept();
	}

	public void uploadFile(String objname, String value)
	{
		try
		{

			driver.findElement(map.getLocator(objname)).sendKeys(value);
			Report.info("'"+value+"' is entered in '"+objname+"'");
		}
		catch(Exception e)
		{
			Report.fail("element '"+objname+"' is not displayed");
			Assert.fail("element '"+objname+"' is not displayed");
		}
	}

	public String getTextboxValue(String objname){

		try {
			return driver.findElement(map.getLocator(objname)).getAttribute("value");
		} catch (Exception e) {
			Report.fail("element '"+objname+"' is not displayed");
			return "";
		}

	}

	public void verifyTextboxValuePresent(String string_to_compare, String objname) throws Exception
	{
		String str = "";
		try {
			str = getTextboxValue(objname);
			Assert.assertEquals(string_to_compare, str );
			Report.pass("The value '"+string_to_compare+"' in texbox "+objname+" is verified");
		} 
		catch (Error e) 
		{
			Report.fail("'"+objname+"' is not present in the textbox: "+ objname+" and found to be '"+ getTextboxValue(objname)+"'");

		}
	}

	public void verifyTitleContains(String title_to_be_matched) throws Exception
	{
		String actulTitel=getTitle();

		try {
			actulTitel.contains(title_to_be_matched);
			Report.pass("Title is verified and it contains '"+title_to_be_matched+"'.");
		} catch (Error e) {

			Report.fail("In the title: "+title_to_be_matched+" is not present");
		}

	}


	/*
	 *
	 * <p>
	 * The method The method fluent waits for an {@link WebElement} based on
	 * <code>identifier</code>, and click on the {@link WebElement}
	 * 
	 * </p>
	 * 
	 * @param driver
	 *            The {@link WebDriver } Instance
	 * @param identifier
	 *            The <code>identifier</code> to locate the WebElement.
	 * @param totalWaitTimeInSecs
	 *            The total time to wait for an existence of {@link WebElement}
	 *            in seconds
	 * @param poolWaitTimeInSecs
	 *            The amount of time to wait and recursively check for
	 *            <p>
	 *            The <code>identifier</code> value can be an Xpath or CSS or ID
	 *            or name or linktext or partial link text or classname or
	 *            tagname
	 *            </p>
	 *            existence of {@link WebElement} in seconds
	 * @param identifierType
	 *            The value type of <code>identifier</code> is specified in this
	 *            argument
	 *            <p>
	 *            See {@link IdentifierTypes } to know about different Identifier
	 *            type values
	 *            </p>
	 * @throws NoSuchFieldException 
	 */
	public void fluentWaitAndGetWebElementAndClick(long totalWaitTimeInSecs,
			long poolWaitTimeInSecs, String objName) throws NoSuchFieldException {
		WebElement webElement = fluentWaitAndGetWebElement(totalWaitTimeInSecs, poolWaitTimeInSecs,
				objName);
		try {
			webElement.click();
			Report.info("Clicked on'" + objName + "'");
		} catch (Exception e) {
			e.printStackTrace();
			// log.error("Could not able to load element" + webElement.getText()
			// );
			Report.fail("Could not able to load element '" + objName + "'");
			Assert.fail("Could not able to load element '" + objName + "'");
		}
	}

	/*
	 *
	 * <p>
	 * The method The method fluent waits for an {@link WebElement} based on
	 * <code>identifier</code>
	 * </p>
	 * 
	 * @param driver
	 *            The {@link WebDriver } Instance
	 * @param identifier
	 *            The <code>identifier</code> to locate the WebElement.
	 *            <p>
	 *            The <code>identifier</code> value can be an Xpath or CSS or ID
	 *            or name or linktext or partial link text or classname or
	 *            tagname
	 *            </p>
	 * @param totalWaitTimeInSecs
	 *            The total time to wait for an existence of {@link WebElement}
	 *            in seconds
	 * @param poolWaitTimeInSecs
	 *            The amount of time to wait and recursively check for existence
	 *            of {@link WebElement} in seconds
	 * @param identifierType
	 *            The value type of <code>identifier</code> is specified in this
	 *            argument
	 *            <p>
	 *            See {@link IdentifierTypes } to know about different Identifier
	 *            type values
	 *            </p>
	 * @return WebElement Returns {@link WebElement}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public WebElement fluentWaitAndGetWebElement(final long totalWaitTimeInSecs, long poolWaitTimeInSecs, final String objName)
			throws NoSuchFieldException {

		// Waiting 30 seconds for an element to be present on the page, checking
		// for its presence once every 5 seconds.
		Wait wait = new FluentWait(driver).withTimeout(totalWaitTimeInSecs, TimeUnit.SECONDS)
				.pollingEvery(poolWaitTimeInSecs, TimeUnit.SECONDS);
		
		WebElement webElement = (WebElement) wait.until(new Function<WebDriver, WebElement>() {
			@Override
			public WebElement apply(WebDriver driver) {
				try {
					Report.info("WebElement '" + objName + "'" + " is Dispalyed");
					return driver.findElement(map.getLocator(objName));
				} catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					Report.fail("Could not able to find the element in wait time: "+objName+ totalWaitTimeInSecs + " Seconds");
					Assert.fail("Could not able to find the element in wait time: "+objName+ totalWaitTimeInSecs + " Seconds");
					
				}
				return null;
			}
		});
		return webElement;
	}


	/**Describes the Method name at the stating of each Test.*/
	public void startLine(Method methodName)
	{
		Report.info("------------" +methodName.getName() +" Test Starting------------");
		softAssertCount = 0;
	}

	/**Describes the Method name at the  ending of each Test.*/
	public void endLine(Method methodName)
	{
		validateAllVerificationPoints(); 
		Report.info("------------" + methodName.getName() + " Test Ended------------");
	}

	public int getPoolWaitTime() {
		return Integer.parseInt(readConfigFile("Fluent_WaitTime"));
	}

	public int getTotalWaitTimeInSecs() {
		return Integer.parseInt(readConfigFile("ElementLoad_WaitTime"));
	}
	

	public long getPageLoadWaitTime() {
		// TODO Auto-generated method stub
		return Integer.parseInt(readConfigFile("PageLoad_WaitTime"));
	}

	/**
	 * <p>
	 * The method explicitly waits for a {@link WebElement} based on <code>identifier</code>
	 * </p>
	 * 
	 * @param driver The {@link WebDriver } Instance
	 * @param identifier The <code>identifier</code> to locate the WebElement.
	 * <p>The <code>identifier</code> value can be an Xpath or CSS or ID or 
	 * name or linktext or partial link text or classname or tagname
	 * </p>
	 * @param waitTimeInSeconds No of Seconds to wait for that WebElement
	 * @param identifierType The value type of <code>identifier</code> is specified in this argument
	 * <p>
	 * See {@link IdentifierTypes }  to know about different Identifier type values
	 * </p>
	 */
	public void explicitlyWaitForElement(String objName,
			long waitTimeInSeconds) {

		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				waitTimeInSeconds);
		WebElement element = null;
		try {
			element = webDriverWaitForTask.until(ExpectedConditions
					.elementToBeClickable(map.getLocator(objName)));
			Report.info("'"+ objName + "' Element is clickable");
		} catch (Exception e) {
//			e.printStackTrace();
			element = handleStaleElementExpection(objName, waitTimeInSeconds,
					webDriverWaitForTask, element, e);
		}
	}
	
	/**
	 * <p>
	 * The method explicitly waits for a {@link WebElement}, 
	 * and gets it bases on <code>identifier</code> value
	 * </p>
	 * @param driver The {@link WebDriver } Instance
	 * @param identifier The <code>identifier</code> to locate the WebElement.
	 * <p>The <code>identifier</code> value can be an Xpath or CSS or ID or 
	 * name or linktext or partial link text or classname or tagname
	 * </p>
	 * @param waitTimeInSeconds No of Seconds to wait for that WebElement
	 * @param identifierType The value type of <code>identifier</code> is specified in this argument
	 * <p>
	 * See {@link IdentifierTypes }  to know about different Identifier type values
	 * </p>
	 * @return {@link WebElement} located based on <code>identifier</code>
	 */
	public WebElement explicitlyWaitAndGetWebElement(String objName, long waitTimeInSeconds) {

		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				waitTimeInSeconds);
		WebElement element = null;

		try {
			element = webDriverWaitForTask.until(ExpectedConditions
					.elementToBeClickable(map.getLocator(objName)));			
			Report.info("'" + objName + "' Element is fetched");
		} catch (Exception e) {
//			e.printStackTrace();
			
			element = handleStaleElementExpection(objName, waitTimeInSeconds,
					webDriverWaitForTask, element, e);
			
		}
		return element;
	}

	private WebElement handleStaleElementExpection(String objName,
			long waitTimeInSeconds, WebDriverWait webDriverWaitForTask,
			WebElement element, Exception e) {
		if (e instanceof StaleElementReferenceException) {
			try {
				element = webDriverWaitForTask.until(ExpectedConditions
						.elementToBeClickable(map.getLocator(objName)));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				Report.fail("Could not able to find the element in wait time: '"+ objName + "' " + waitTimeInSeconds + " Seconds");
				Assert.fail("Could not able to find the element in wait time: '"+ objName + "' " +waitTimeInSeconds + " Seconds");

			}
		} else {
			Report.fail("Could not able to find the element in wait time: '"+ objName + "' " + waitTimeInSeconds + " Seconds");
			Assert.fail("Could not able to find the element in wait time: '"+ objName + "' " +waitTimeInSeconds + " Seconds");

		}
		return element;
	}
	
	/*
	 * <p>
	 * The method explicitly waits for a {@link WebElement}, 
	 * and gets it bases on <code>identifier</code> value
	 * </p>
	 * @param driver The {@link WebDriver } Instance
	 * @param identifier The <code>identifier</code> to locate the WebElement.
	 * <p>The <code>identifier</code> value can be an Xpath or CSS or ID or 
	 * name or linktext or partial link text or classname or tagname
	 * </p>
	 * @param waitTimeInSeconds No of Seconds to wait for that WebElement
	 * @param identifierType The value type of <code>identifier</code> is specified in this argument
	 * <p>
	 * See {@link IdentifierTypes }  to know about different Identifier type values
	 * </p>
	 * @return {@link WebElement} located based on <code>identifier</code>
	 */
	public List<WebElement> explicitlyWaitAndGetWebElements(String objName, long waitTimeInSeconds) {

		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				waitTimeInSeconds);
		List<WebElement> elements = null;

		try {
			elements =  webDriverWaitForTask.until(ExpectedConditions.presenceOfAllElementsLocatedBy(map.getLocator(objName)));			
			Report.info(objName + "Element is fetched");
		} catch (TimeoutException e) {
//			e.printStackTrace();
			Report.fail("Could not able to find the element in wait time:"+objName+ waitTimeInSeconds + " Seconds");
			Assert.fail("Could not able to find the element in wait time: "+objName+ waitTimeInSeconds + " Seconds");

		} catch (Exception e) {
//			e.printStackTrace();
			Report.fail("Please check the locator info for element : "+ objName);
			Assert.fail("Please check the locator info for element : "+ objName);

		}
		return elements;
	}
	
	/* *
	 * <p>
	 * The method explicitly waits for a {@link WebElement}, 
	 * and gets it bases on <code>identifier</code> value
	 * </p>
	 * @param driver The {@link WebDriver } Instance
	 * @param identifier The <code>identifier</code> to locate the WebElement.
	 * <p>The <code>identifier</code> value can be an Xpath or CSS or ID or 
	 * name or linktext or partial link text or classname or tagname
	 * </p>
	 * @param waitTimeInSeconds No of Seconds to wait for that WebElement
	 * @param identifierType The value type of <code>identifier</code> is specified in this argument
	 * <p>
	 * See {@link IdentifierTypes }  to know about different Identifier type values
	 * </p>
	 * @return {@link WebElement} located based on <code>identifier</code>
	 * @throws Exception 
	 */
	public String explicitlyWaitAndGetWebElementText(String objName, long waitTimeInSeconds){

		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				waitTimeInSeconds);
		String elementText = "";
			try {
				webDriverWaitForTask.until(ExpectedConditions
						.elementToBeClickable(driver.findElement(map.getLocator(objName))));
				return driver.findElement(map.getLocator(objName)).getText();
			} catch(StaleElementReferenceException e){
				elementText = explicitlyWaitAndGetWebElementText(objName, getTotalWaitTimeInSecs());
			} catch (Exception e) {
				Report.fail("Could not able to find the element '" + objName + "' " + "in wait time " + waitTimeInSeconds + " Seconds");
				Assert.fail("Could not able to find the element '" + objName + "' " + "in wait time " + waitTimeInSeconds + " Seconds");
				
			}
			return elementText;
	}
	
	private String explicitlyWaitAndGetWebElementTagName(String objName,
			int totalWaitTimeInSecs) {
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				totalWaitTimeInSecs);
		String elementTagName = "";
			try {
			elementTagName =	webDriverWaitForTask.until(ExpectedConditions
						.elementToBeClickable(driver.findElement(map.getLocator(objName)))).getTagName();
			} catch(StaleElementReferenceException e ){
				elementTagName = explicitlyWaitAndGetWebElementTagName(objName, totalWaitTimeInSecs);
			} catch (Exception e) {
				Report.fail("Could not able to find the element '" + objName + "' " + "in wait time " + totalWaitTimeInSecs + " Seconds");
				Assert.fail("Could not able to find the element '" + objName + "' " + "in wait time " + totalWaitTimeInSecs + " Seconds");
			}
			return elementTagName;
	}

	/*
	 *
	 * <p>
	 * The method explicitly waits for a {@link WebElement} based on <code>identifier</code> value, and
	 * clicks on the {@link WebElement}
	 * </p>
	 * @param driver The {@link WebDriver } Instance
	 * @param identifier The <code>identifier</code> to locate the WebElement.
	 * <p>The <code>identifier</code> value can be an Xpath or CSS or ID or 
	 * name or linktext or partial link text or classname or tagname
	 * </p>
	 * @param waitTimeInSeconds No of Seconds to wait for that WebElement
	 * @param identifierType The value type of <code>identifier</code> is specified in this argument
	 * <p>
	 * See {@link IdentifierTypes }  to know about different Identifier type values
	 * </p>
	 * @throws Exception 
	 */
	public void explicityWaitForAnElementAndClick(String objName, long waitTimeInSeconds) throws Exception {
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				waitTimeInSeconds);
		WebElement element = null;

		try {
			element = webDriverWaitForTask.until(ExpectedConditions
					.elementToBeClickable(map.getLocator(objName)));
			element.click();
			Report.info("'" + objName + "' Element is clicked");
		} catch(StaleElementReferenceException e){
			explicityWaitForAnElementAndClick(objName, waitTimeInSeconds);
		} catch (Exception e) {
			//	e.printStackTrace();
			Report.fail("Could not able to find the element '" + objName + "' "
					+ "in wait time " + waitTimeInSeconds + " Seconds");
			Assert.fail("Could not able to find the element '" + objName + "' "
					+ "in wait time " + waitTimeInSeconds + " Seconds");
		}
	}
	
	/**
	 * <p>
	 * The method explicitly waits for a {@link WebElement} based on <code>identifier</code> value, and
	 * clicks on the {@link WebElement}
	 * </p>
	 * @param driver The {@link WebDriver } Instance
	 * @param identifier The <code>identifier</code> to locate the WebElement.
	 * <p>The <code>identifier</code> value can be an Xpath or CSS or ID or 
	 * name or linktext or partial link text or classname or tagname
	 * </p>
	 * @param waitTimeInSeconds No of Seconds to wait for that WebElement
	 * @param identifierType The value type of <code>identifier</code> is specified in this argument
	 * <p>
	 * See {@link IdentifierTypes }  to know about different Identifier type values
	 * </p>
	 * @throws Exception 
	 */
	public void explicityWaitForAnElementWithXpathAndClick(String objXpath, long waitTimeInSeconds) throws Exception {
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				waitTimeInSeconds);
		WebElement element = null;

		try {
			element = webDriverWaitForTask.until(ExpectedConditions
					.elementToBeClickable(By.xpath(objXpath)));
			element.click();
			Report.info("'" + objXpath + "' Element is clicked");
		} catch (Exception e) {
//			e.printStackTrace();
			Report.fail("Could not able to find the element in wait time:" + objXpath + " " + waitTimeInSeconds + " Seconds");
			Assert.fail("Could not able to find the element in wait time: " + objXpath + " " + waitTimeInSeconds + " Seconds");

		}
	}

	/**
	 * <p>
	 * The method explicitly waits for a {@link WebElement} based on <code>identifier</code> value, and
	 * clicks on the {@link WebElement}
	 * </p>
	 * @param driver The {@link WebDriver } Instance
	 * @param identifier The <code>identifier</code> to locate the WebElement.
	 * <p>The <code>identifier</code> value can be an Xpath or CSS or ID or 
	 * name or linktext or partial link text or classname or tagname
	 * </p>
	 * @param waitTimeInSeconds No of Seconds to wait for that WebElement
	 * @param identifierType The value type of <code>identifier</code> is specified in this argument
	 * <p>
	 * See {@link IdentifierTypes }  to know about different Identifier type values
	 * </p>
	 * @throws Exception 
	 */
	public void explicityWaitForAnElementWithXpathAndDoubleClick(String objXpath, long waitTimeInSeconds) throws Exception {
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				waitTimeInSeconds);
		WebElement element = null;

		try {
			element = webDriverWaitForTask.until(ExpectedConditions
					.elementToBeClickable(By.xpath(objXpath)));
			Actions action = new Actions(driver);
			action.doubleClick(element).perform();
			Report.info("'" + objXpath + "' Element is Double clicked");
		} catch (Exception e) {
//			e.printStackTrace();
			Report.fail("Could not able to find the element in wait time:" + objXpath + " " + waitTimeInSeconds + " Seconds");
			Assert.fail("Could not able to find the element in wait time: " + objXpath + " " + waitTimeInSeconds + " Seconds");

		}
	}
	
	/**
	 * <p>
	 * The method explicitly waits for a {@link WebElement} based on <code>identifier</code> value, and
	 * clicks on the {@link WebElement}
	 * </p>
	 * @param driver The {@link WebDriver } Instance
	 * @param identifier The <code>identifier</code> to locate the WebElement.
	 * <p>The <code>identifier</code> value can be an Xpath or CSS or ID or 
	 * name or linktext or partial link text or classname or tagname
	 * </p>
	 * @param waitTimeInSeconds No of Seconds to wait for that WebElement
	 * @param identifierType The value type of <code>identifier</code> is specified in this argument
	 * <p>
	 * See {@link IdentifierTypes }  to know about different Identifier type values
	 * </p>
	 * @throws Exception 
	 */
	public void explicityWaitForAnElementAndDoubleClick(String objName, long waitTimeInSeconds) throws Exception {
		WebDriverWait webDriverWaitForTask = new WebDriverWait(driver,
				waitTimeInSeconds);
		WebElement element = null;

		try {
			element = webDriverWaitForTask.until(ExpectedConditions
					.elementToBeClickable(map.getLocator(objName)));
			Actions action = new Actions(driver);
			action.doubleClick(element).perform();
			Report.info(objName + "Element is Double clicked");
		} catch (Exception e) {
//			e.printStackTrace();
			Report.fail("Could not able to find the element in wait time:"+objName+ waitTimeInSeconds + " Seconds");
			Assert.fail("Could not able to find the element in wait time: "+objName+ waitTimeInSeconds + " Seconds");

		}
	}
	
	/**
	 * <p>
	 * The method explicitly waits for an existence of WebElement(either button or Hyperlink), 
	 * and Click on it
	 * </p>
	 * @param driver The {@link WebDriver } Instance
	 * @param identifier The <code>identifier</code> to locate the WebElement. 
	 * <p>The <code>identifier</code> value can be an Xpath or CSS or ID or 
	 * name or linktext or partial link text or classname or tagname
	 * </p>
	 * @param waitTimeInSeconds No of Seconds to wait for that WebElement
	 * @param identifierType The value type of <code>identifier</code> is specified in this argument
	 * <p>
	 * See {@link IdentifierTypes }  to know about different Identifier type values
	 * </p>
	 * @throws Exception 
	 */
	public void explictlyWaitandClickonHyperlink(String objName, long waitTimeInSeconds) throws Exception {

		explicitlyWaitForElement(objName, waitTimeInSeconds);

		if (isElementPresent(objName)) {
			explicityWaitForAnElementAndClick(objName,waitTimeInSeconds);
		}

	}

	public void hoverAndClick(String[] hoverer, String element) throws Exception {
		//declare new Action

		Actions actions = new Actions(driver);
		for (String we : hoverer) {
			// hover each of them
			try {
//				explicitlyWait(we, getPoolWaitTime());
				Action hovering = actions.moveToElement(driver.findElement(map.getLocator(we))).build();
				hovering.perform();
				Thread.sleep(5000L);
				Report.info("Clicked on Menu '" + we + "'");
			} catch (Exception e) {
				Report.info("Could not able click on menu '" + we + "'");
				Assert.fail("Could not able to click on menu '" + we + "'");
			}

		}
		
		try {
//			explicityWaitForAnElementAndClick(element, getTotalWaitTimeInSecs());
			WebElement lastSubmenu = explicitlyWaitAndGetWebElement(element, getTotalWaitTimeInSecs());
			actions.click(lastSubmenu).perform();
			Report.info("Clicked on submenu item '" + element + "'");
			
		} catch (Exception e) {
			Report.info("Could not able click on submenu '" + element + "'");
			Assert.fail("Could not able click on submenu '" + element + "'");
		}

	}
	
	public void hoverAndClick(String mainMenu, String submenu1) throws Exception {
		
		Actions actions = new Actions(driver);
		Action hovering = actions.moveToElement(driver.findElement(map.getLocator(mainMenu))).click().build();
		Report.info("Mainmenu opened");
		hovering.perform();
		WebDriverWait wait = new WebDriverWait(driver, getPoolWaitTime()); 
		wait.until(ExpectedConditions.presenceOfElementLocated(map.getLocator(submenu1)));  // until this submenu is found

		//identify menu option from the resulting menu display and click
		actions.moveToElement(driver.findElement(map.getLocator(submenu1))).click().build();
		Report.info("Submenu opened");
		hovering.perform();	
	}
	
	public void hoverAndClick1(String mainMenu, String submenu1) throws Exception {
		
		WebElement menu1 = explicitlyWaitAndGetWebElement(mainMenu, getTotalWaitTimeInSecs());
		
		Actions actions = new Actions(driver);
		actions.moveToElement(menu1).perform();
		Thread.sleep(getPoolWaitTime());
		actions.moveToElement(explicitlyWaitAndGetWebElement(submenu1, getTotalWaitTimeInSecs())).click().perform();
//		explicityWaitForAnElementAndClick(submenu1, getTotalWaitTimeInSecs());
	}
	
	public void hoverAndClick1(String mainMenu, String submenu1, String submenu2) throws Exception {

		WebElement menu1 = explicitlyWaitAndGetWebElement(mainMenu, getTotalWaitTimeInSecs());

		Actions actions = new Actions(driver);
		actions.moveToElement(menu1).perform();
		Thread.sleep(10000);
		WebElement menu2 = driver.findElement(map.getLocator(submenu1));
		actions.moveToElement(menu2).perform();
		Thread.sleep(10000);
		WebElement menu3 = driver.findElement(map.getLocator(submenu2));
		actions.click(menu3).perform();
		//		explicityWaitForAnElementAndClick(submenu1, getTotalWaitTimeInSecs());
	}


	/**
	 * Select the options that have a @value value matching with the given Value
	 * @param objname object identification information
	 * @param Value argument that have a matching value for options, these @value values are visible only when we see the html code
	 * <pre>
	 * {@code
	 * For example: for html code as below: Use attribute @value 
	 * <select id="create-2504-select" class="requiredField" name="values[2504]">
	 *		<option value=""/>
	 *		<option value="L" selected="selected">Low</option>
	 *		<option value="M">Medium</option>
	 *		<option value="H">High</option>
	 *	</select>
	 * }
	 * </pre>
	 */
	public void selectTextByValue (String objname, String Value){
		try{
			Select select = new Select(driver.findElement(map.getLocator(objname)));
			Report.info("dropdown '"+ objname +"' is selected");
			select.selectByValue(Value);
			Report.pass("text '"+Value+"' is selected from the dropdown '"+objname+"'");
		} catch(Exception e){
			Report.fail("'"+objname+"' is not present or text '"+Value+"' is not selected");
			Assert.fail("'"+objname+"' is not present or text '"+Value+"' is not selected");
		}

	}

	/**
	 * To Select an options that display text matching the given argument
	 * @param objname object identification information
	 * @param text Visble text that happen to see in the UI
	 * <pre>
	 * {@code
	 * For example: for html code as below: Use value of <option> element 
	 * <select id="create-2504-select" class="requiredField" name="values[2504]">
	 *		<option value=""/>
	 *		<option value="L" selected="selected">Low</option>
	 *		<option value="M">Medium</option>
	 *		<option value="H">High</option>
	 *	</select>
	 * }
	 * </pre>
	 */
	public void selectByVisibleText (String objname, String text){
		try{
			Select select = new Select(driver.findElement(map.getLocator(objname)));
			Report.info("dropdown '"+ objname +"' is selected");
			select.selectByVisibleText(text);
			Report.pass("text '"+text+"' is selected from the dropdown '"+objname+"'");
		}catch(Exception e){
			Report.fail("'"+objname+"' is not present or text '"+text+"' is not selected");
			Assert.fail("'"+objname+"' is not present or text '"+text+"' is not selected");
		}

	}
				

	/** SelectTextByIndex method (objname = the object name from objectmap file)**/
	public void selectTextByIndex (String objname, int indexno){
		try{

			Select select = new Select(driver.findElement(map.getLocator(objname)));
			Report.info("dropdown '"+ objname +"' is selected");
			select.selectByIndex(indexno);
			Report.info("text '"+indexno+"' is selected from the dropdown '"+objname+"'");
		} catch(Exception e){
			Report.fail("'"+objname+"' is not present or text '"+indexno+"' is not selected");
			Assert.fail("'"+objname+"' is not present or text '"+indexno+"' is not selected");
		}

	}
	
	/** SelectTextByValue method (objname = the object name from objectmap file)*/
	public void verifyCurrentSelectByText (String objname, String Value){
		try
		{
			Select select = new Select(driver.findElement(map.getLocator(objname)));
			//			Report.info("dropdown " + objname +"' is selected");
			String currentSelectedOptionvalue = select.getFirstSelectedOption().getText();

			if(currentSelectedOptionvalue.contains(Value)){
				Report.pass("dropdown '" + objname +"' with value '" + currentSelectedOptionvalue +"' is verified");
			}else{
				softAssertCount++;
				Report.warn("For object '"+ objname + " expected text '" + Value +"' is not same as actual text '" + currentSelectedOptionvalue + "'");
				softAssert.fail("For object '"+ objname + " expected text '" + Value +"' is not same as actual text '" + currentSelectedOptionvalue + "'");
			}
		}
		catch(Exception e)
		{
			Report.fail("'"+objname+"' is not present or text '"+Value+"' is not selected");
			Assert.fail("'"+objname+"' is not present or text '"+Value+"' is not selected");
		}

	}
	
	public String getCurrentSelectOption (String objname){
		String currentSelectedOptionvalue = "dummy";
		try{
			Select select = new Select(driver.findElement(map.getLocator(objname)));
			//			Report.info("dropdown " + objname +"' is selected");
			currentSelectedOptionvalue = select.getFirstSelectedOption().getText();

		}
		catch(Exception e){
			Report.fail("'"+objname+"' is not present ");
			Assert.fail("'"+objname+"' is not present ");
		}
		return currentSelectedOptionvalue;
	}
	
	/**
	 * Getting all the items in dropdown list and verify the element is List
	 */
	public void getAllListItemAndVerify(String value, String objname) {
		try {
			ArrayList<String> listItems = new ArrayList<String>();
			WebElement mySelectElm = explicitlyWaitAndGetWebElement(objname, getTotalWaitTimeInSecs());
			Select mySelect = new Select(mySelectElm);
			List<WebElement> options = mySelect.getOptions();
			for (WebElement option : options) {
				String element = option.getText();
				listItems.add(element);
			}
			if (listItems.contains(value)) {
				Report.pass("dropdown '" + objname +"' with value '" + value +"' is verified");
			} else {
				softAssertCount++;
				Report.warn("The item '" + value +"' is not in Dropdown List ");
				softAssert.fail("The item '" + value +"' is not in Dropdown List ");
			}
			
		} catch (Exception e) {
			Report.fail("Unable to find on element " + e.getStackTrace());
		}
	}
	
	/*
	 * This method will verify if a specified text value does not
	 * appear in the values in a specified drop down list
	 */
	public void verifyDropDownNotContainText(String objname, String Value)
	{
		try
		{
			Select select = new Select(driver.findElement(map.getLocator(objname)));
		
			List<WebElement> options = select.getOptions();
			boolean match = false;
			for(WebElement we:options)  
			{  		  
				String temp = we.getText();
				if (temp.equals(Value)){
					
		            match = true;  
				}
			}
			if(match == true){
				Assert.fail("Text is displaying in drop down when it should not");
			}			
		}
		catch(Exception e)
		{
			Report.fail("'"+objname+"' is not present or text '"+Value+"' is present");
			Assert.fail("'"+objname+"' is not present or text '"+Value+"' is present");
		}
	}
	
	
	public void verifyAllDowndownValues(ArrayList<String> value, String objname) {
		try {
			ArrayList<String> listItems = new ArrayList<String>();
			WebElement mySelectElm = driver
					.findElement(map.getLocator(objname));
			Select mySelect = new Select(mySelectElm);
			List<WebElement> options = mySelect.getOptions();
			for (WebElement option : options) {
				String element = option.getText();
				listItems.add(element);
			}
			if (listItems.containsAll((value))) {
				Report.pass("Elements in dropdown " + objname + " matches with expected list of values");
			} else {
				softAssertCount++;
				Report.warn("Elements in dropdown " + objname + " does not match with expected list of values");
				softAssert.fail("Elements in dropdown " + objname + " does not match with expected list of values");
			}
			
		} catch (Exception e) {
			Report.fail("Unable to find on element " + e.getStackTrace());
		}
	}
	
	/**
	 * Returns true for everything but disabled input elements.
	 * @param objName object locator info
	 * @return true or false
	 * @throws Exception
	 */
	public boolean isEnabled(String objName){
		try {
			if(driver.findElement(map.getLocator(objName)).isEnabled())
					return true;
			else
				return false;
		} catch (Exception e) {
			//e.printStackTrace();
			Report.fail("Locator type is not defined!!");
		}
		return false;
		
	}

	/*
	 * 
	 */
	public String readTextFile(String path) throws Exception{
		BufferedReader br = new BufferedReader(new FileReader(path));
		String line = null;
		StringBuilder sb = new StringBuilder();
		String lineSeperator = System.getProperty("line.seperator");
		String data = null;
				
		while((line = br.readLine())!=null){
			sb.append(line);
			sb.append(lineSeperator);
			
		}
		
		data = sb.toString();
		br.close();
		return data;
		
	}
	
	/**
	 * The method is used to get WebElement for a specific object information
	 * @param objName takes object identification information for a web element
	 * @return WebElement matching the object information passed to this method, otherwise return null 
	 */
	public WebElement getWebElement(String objName){
		WebElement element = null;
		try {
			element = driver.findElement(map.getLocator(objName));
			Report.info("Able to find the element: " + objName);
			return element;
		} catch (NoSuchElementException e) {
			Report.fail("Not able to find the element: " + objName);
			e.printStackTrace();
		} catch (Exception e) {
			Report.fail("Please check locator info for finding the element: " + objName);
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	/**
	 * Writes some data to tinyMCE in page
	 * @param data Data to be passed to tinyMCE
	 * @param objName object identification information for tinmyMCE
	 * @throws Exception Returns NoSuchElementException if no WebElement is noticed with object identification information
	 */
	public void writeToTinyMCE(String data, String objName) throws Exception{
		WebElement element = driver.findElement(map.getLocator(objName));
		JavascriptExecutor jsExecute = (JavascriptExecutor) driver;
		jsExecute.executeScript("arguments[0].innerHTML = ('<p>"+ data +"</p>')", element);
		
	}
	
	/**
	 * Inserts date and time into Date fields
	 * @param object object identification information for date field
	 * @param date date to be passed in DD/MM/YYYY format
	 * @param time time to be passed in HH:MM
	 * @throws Exception Returns NoSuchElementException if no WebElement is noticed with object identification information
	 */
	public void datePicker(String object, String date, String time) throws Exception{
		WebElement dateBox =  driver.findElement(map.getLocator(object));
		dateBox.clear();
		dateBox.sendKeys(date);
		dateBox.sendKeys(Keys.SPACE);
		dateBox.sendKeys(time);
		dateBox.sendKeys(Keys.TAB);
		
	}
	
	public String switchToFrame1(String object) throws Exception{
		String mainWindow = driver.getWindowHandle();
		WebElement myFrame = driver.findElement(map.getLocator(object));
		driver.switchTo().frame(myFrame);
		return mainWindow;
		
	}
	
	/**
	 * Strip document URN from the string passed
	 * @param object Success message which contains the Document urn details
	 * @return Document URN
	 */
	public String getDocumentURN(String object){
		String[] str=object.split(" ");
		return str[1];
	}
	
	/**
	 * This used is used instead of Menu hovering in IE.
	 * Navigates to specific page, by passing in the relative URL for a specific menu item
	 * @param url pass the relative url - not the absolute url
	 */
	public void navigateToPage(String url) {
		driver.get(baseUrl + url);
		waitForPageLoad();
		validateNavigationtoPage(baseUrl + url);
		
	}
	
	/**
	 * Validate the application is in current url 
	 * @param url - current url of the application
	 */
	private void validateNavigationtoPage(String url) {
		String currentPageURL = getCurrentPageURL();
		Report.info("current Page URL: " + currentPageURL);
		Report.info("expected page URL:" + url);
		if(currentPageURL.contains(url)){
			Report.pass("Navigated to " + url + " Successfully");
		} else{
			driver.get(url);
		}
		
	}

	/**
	 * Get current URL of the application
	 * @return current URL of the application
	 */
	public String getCurrentPageURL(){
		return driver.getCurrentUrl();
	}
	
	/* 
	 * Synchronization
	 * */
	public void waitForPageLoad1() {

	    Wait<WebDriver> wait = new WebDriverWait(driver, getPageLoadWaitTime());
	    wait.until(new Function<WebDriver, Boolean>() {
	        public Boolean apply(WebDriver driver) {
	        	Report.info("Current Window State       : "
	                + String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState")));
	            return String
	                .valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
	                .equals("complete");
	        }
	    });
	}
	
	/**
	 * This methods halts the selenium execution until the page loading waggon whell is stopped/completed/disappeared
	 */
    public void waitForPageLoad() {
    	Report.info("JS: Wating for ready state complete");
	    (new WebDriverWait(driver, getPageLoadWaitTime())).until(new ExpectedCondition<Boolean>() {
	        public Boolean apply(WebDriver d) {
	            JavascriptExecutor js = (JavascriptExecutor) d;
	            String readyState = js.executeScript("return document.readyState").toString();
	            Report.info("JS: Ready State: " + readyState);
	            return (Boolean) readyState.equals("complete");
	        }
	    });
	    
	 /*   WebDriverWait wait = new WebDriverWait(driver, getPageLoadWaitTime());

	    Predicate<WebDriver> pageLoaded = new Predicate<WebDriver>() {

	        @Override
	        public boolean apply(WebDriver d) {
	        	 JavascriptExecutor js = (JavascriptExecutor) d;
		            String readyState = js.executeScript("return document.readyState").toString();
		            Report.info("JS: Ready State: " + readyState);
	            return (Boolean) readyState.equals("complete");
	        }

	    };
	    wait.until(pageLoaded);*/
	    
    }
    
    /**
     * This methods halts the selenium execution until the specific Jquery loading waggon whell is stopped/completed/disappeared
     * Usually noticed on click of buttons in applications
     * @param driver
     */
    public void waitForJQuery(WebDriver driver) {
    	Report.info("JQuery: Wating for JQuery to be active");
	    (new WebDriverWait(driver, getPageLoadWaitTime())).until(new ExpectedCondition<Boolean>() {
	        public Boolean apply(WebDriver d) {
	            JavascriptExecutor js = (JavascriptExecutor) d;
	            String readyState = js.executeScript("return !!window.jQuery && window.jQuery.active == 0").toString();
	            Report.info("JQuery: Ready State: " + readyState);
	            return (Boolean) readyState.equals("true");
	        }
	    });
    }
	
	public void WaitForAjax() throws InterruptedException {
		while (true) {
			Boolean ajaxIsComplete = (Boolean) ((JavascriptExecutor) driver).executeScript("return jQuery.active == 0");
			if (ajaxIsComplete) {
				break;
			}
			Thread.sleep(getPageLoadWaitTime());
		}
	}

	/** Switching to New Window **/
	/**
	 * User only when accepting a dialog box in child window, closes clild windows and navigates to Parent window.
	 * @param title
	 * @return
	 */
	public WebDriver getHandleToWindow(String title) {

		//parentWindowHandle = WebDriverInitialize.getDriver().getWindowHandle(); // save the current window handle.

		WebDriver popupWindow = null;
		Set<String> handles = driver.getWindowHandles();
		Report.info("No of windows :  " + handles.size());

		for (String s : handles) {
			String windowHandle = s; 
			popupWindow = driver.switchTo().window(windowHandle);
			Report.info("Window Title : " + popupWindow.getTitle());
			Report.info("Window Url : " + popupWindow.getCurrentUrl());

			if (popupWindow.getTitle().contains(title)){
				Report.info("Selected Window Title : " + popupWindow.getTitle());
				return popupWindow;
			}
		}
		Report.info("Window Title :" + popupWindow.getTitle());
		waitForPageLoad();
		return popupWindow;
	}
	
	/**
	 * @param title
	 * @return
	 */
	public String getCurrentWindowHandle() {

		return driver.getWindowHandle();
	}
	
	public int getWindowCount() {
		return driver.getWindowHandles().size();
	}
	
	/**
	 * Usually a successor method to {@link ReusableUtilities.SeleniumBaseClass#clickAndWaitForBrowserPopup(String childWindowTitle, String objName)}.
	 * Called when the operations on child windows are done and wanted to navigate back to parent window 
	 * @param childWindowTitle Name of the child window
	 * @param parentWindowTitle name of the parent window
	 * @return handler to parent window
	 */
	public WebDriver closePopupWindowAndNavigateToParentWindow(String childWindowTitle, String parentWindowTitle){
		WebDriver popupWindow = null;
		Set<String> handles = driver.getWindowHandles();
		Report.info("No of windows Noticed :  " + handles.size());
		boolean isChildWindowClosed = false;
		
		for (String s : handles) {
			String windowHandle = s; 
			popupWindow = driver.switchTo().window(windowHandle);
			
			if (popupWindow.getTitle().contains(childWindowTitle) && isChildWindowClosed == false){
				driver.close();
				Report.info("Successully closed child window with Title : " + childWindowTitle);
				isChildWindowClosed = true;
				break;
			}
		}
		handles = driver.getWindowHandles();
		Report.info("No of windows after closing child window :  " + handles.size());
		for (String s : handles) {
			String windowHandle = s; 
			popupWindow = driver.switchTo().window(windowHandle);
			
			if (popupWindow.getTitle().contains(parentWindowTitle)){
				Report.info("Selected Window Title : " + popupWindow.getTitle());
				
				return popupWindow;
			}
		}
		Report.info("Window Title :" + popupWindow.getTitle());
		return popupWindow;
		
	}
	
	/**
	 * Usually a successor method to {@link ReusableUtilities.SeleniumBaseClass#clickAndWaitForBrowserPopup(String childWindowTitle, String objName)}.
	 * Called when the operations on child windows are done and wanted to navigate back to parent window 
	 * @param childWindowTitle Name of the child window
	 * @param parentWindowTitle name of the parent window
	 * @return handler to parent window
	 */
	public WebDriver closeChildWindowAndNavigateToParentWindowHandle(String childWindowHandle, String parentWindowHandle, int noOfWinBefore, int noOfWinAfter){
		WebDriver popupWindow = null;
		Set<String> winHandles = driver.getWindowHandles();
		Report.info("No of windows Noticed :  " + winHandles.size() + "No of windows Expected : " + noOfWinBefore  );
		boolean isChildWindowClosed = false;
		// removing the parent window handle
		winHandles.remove(parentWindowHandle);
		
		for (String s : winHandles) {
			String windowHandle = s;
			String childWindowTitle = "";
			popupWindow = driver.switchTo().window(windowHandle);
			childWindowTitle = popupWindow.getTitle();
			if (isChildWindowClosed == false && 
					!popupWindow.getWindowHandle().equals(parentWindowHandle)){
				driver.close();
				isChildWindowClosed = true;
				Report.info("Successully closed child window with Title : " + childWindowTitle);
				break;
			}
		}
		winHandles = driver.getWindowHandles();
		return driver.switchTo().window(parentWindowHandle);
		
	}
	
	/**
	 * Usually a successor method to {@link ReusableUtilities.SeleniumBaseClass#clickAndWaitForBrowserPopup(String childWindowTitle, String objName)}.
	 * Called when the operations on child windows are done and wanted to navigate back to parent window 
	 * @param childWindowTitle Name of the child window
	 * @param parentWindowTitle name of the parent window
	 * @return handler to parent window
	 */
	public WebDriver closePopupWindowAndNavigateToParentWindowHandle(String childWindowTitle, String parentWindowHandle){
		WebDriver popupWindow = null;
		Set<String> handles = driver.getWindowHandles();
		Report.info("No of windows Noticed :  " + handles.size());
		boolean isChildWindowClosed = false;
		
		for (String s : handles) {
			String windowHandle = s; 
			popupWindow = driver.switchTo().window(windowHandle);
			
			if (popupWindow.getTitle().contains(childWindowTitle) && isChildWindowClosed == false && 
					!popupWindow.getWindowHandle().equals(parentWindowHandle)){
				driver.close();
				isChildWindowClosed = true;
				Report.info("Successully closed child window with Title : " + childWindowTitle);
				break;
			}
		}
		handles = driver.getWindowHandles();
		Report.info("No of windows after closing child window :  " + handles.size());
		for (String s : handles) {
			String windowHandle = s; 
			popupWindow = driver.switchTo().window(windowHandle);
			
			if (popupWindow.getWindowHandle().equals(parentWindowHandle)){
				Report.info("Selected Window Title : " + popupWindow.getTitle());
				Report.info("Selected Window Handle : " + popupWindow.getWindowHandle());
//				return popupWindow;
				break;
			}
		}
		Report.info("Window Title :" + popupWindow.getTitle());
		return popupWindow;
		
	}
	
	/**
	 * Used when a new window popup comes up on clicking a button/link/icon in IE
	 * An alternative for getHandleToWindow in IE
	 * @param childWindowTitle Name of the child window to which the windows focus is transferred to 
	 * @param objName object identification information for the link/button/icon which opens up a popup window
	 * @return handler to the new popup window
	 */
	
	/**
	 * Usually a successor method to {@link ReusableUtilities.SeleniumBaseClass#clickAndWaitForBrowserPopup(String childWindowTitle, String objName)}.
	 * Called when the operations on child windows are done and wanted to navigate back to parent window 
	 * @param childWindowTitle Name of the child window
	 * @param parentWindowTitle name of the parent window
	 * @param noOfWindowsBefore total number of windows before a child window popup is clicked
	 * @param noOfWindowsAfter total number of windows after a child window popup appears
	 * @return handler to parent window
	 */
	public WebDriver closePopupWindowAndNavigateToParentWindow(String childWindowTitle, String parentWindowTitle, int noOfWindowsBefore, int noOfWindowsAfter){
		WebDriver popupWindow = null;
		Set<String> handles = driver.getWindowHandles();
		Report.info("No of windows Noticed :  " + handles.size());
		boolean isChildWindowClosed = false;
		
		for (String s : handles) {
			String windowHandle = s; 
			popupWindow = driver.switchTo().window(windowHandle);
			
			if (popupWindow.getTitle().contains(childWindowTitle) && isChildWindowClosed == false){
				driver.close();
				Report.info("Successully closed child window with Title : " + childWindowTitle);
				isChildWindowClosed = true;
				break;
			}
		}
		handles = driver.getWindowHandles();
		Report.info("No of windows after closing child window :  " + handles.size());
		for (String s : handles) {
			String windowHandle = s; 
			popupWindow = driver.switchTo().window(windowHandle);
			
			if (popupWindow.getTitle().contains(parentWindowTitle)){
				Report.info("Selected Window Title : " + popupWindow.getTitle());
				
				return popupWindow;
			}
		}
		Report.info("Window Title :" + popupWindow.getTitle());
		return popupWindow;
		
	}
	
	/**
	 * Usually a successor method to {@link ReusableUtilities.SeleniumBaseClass#clickAndWaitForBrowserPopup(String childWindowTitle, String objName)}.
	 * Called when the operations on child windows are done and wanted to navigate back to parent window 
	 * @param childWindowTitle Name of the child window
	 * @param parentWindowTitle name of the parent window
	 * @param noOfWindowsBefore total number of windows before a child window popup is clicked
	 * @param noOfWindowsAfter total number of windows after a child window popup appears
	 * @return handler to parent window
	 */
	public WebDriver closePopupWindowAndNavigateToParentWindowHandle(String childWindowTitle, String parentWindowHandle, int noOfWindowsBefore, int noOfWindowsAfter){
		WebDriver popupWindow = null;
		Set<String> handles = driver.getWindowHandles();
		Report.info("No of windows Noticed :  " + handles.size());
		boolean isChildWindowClosed = false;
		
		for (String s : handles) {
			String windowHandle = s; 
			popupWindow = driver.switchTo().window(windowHandle);
			
			if (popupWindow.getTitle().contains(childWindowTitle) && isChildWindowClosed == false & 
					!popupWindow.getWindowHandle().equals(parentWindowHandle)){
				driver.close();
				isChildWindowClosed = true;
				Report.info("Successully closed child window with Title : " + childWindowTitle);
				break;
			}
		}
		handles = driver.getWindowHandles();
		Report.info("No of windows after closing child window :  " + handles.size());
		for (String s : handles) {
			String windowHandle = s; 
			popupWindow = driver.switchTo().window(windowHandle);
			
			if (popupWindow.getWindowHandle().equals(parentWindowHandle)){
				Report.info("Selected Window Title : " + popupWindow.getTitle());
				return popupWindow;
			}
		}
		Report.info("Window Title :" + popupWindow.getTitle());
		return popupWindow;
		
	}
	
	/**
	 * Used when a new window popup comes up on clicking a button/link/icon in IE
	 * An alternative for getHandleToWindow in IE
	 * Usually this method is called when only one window (ie parent window) is present, 
	 * and invoking a link/button/icon triggers child window, and total window count would be 2
	 * @param childWindowTitle Name of the child window to which the windows focus is transferred to 
	 * @param objName object identification information for the link/button/icon which opens up a popup window
	 * @return handler to the new popup window
	 */
	
	public WebDriver clickAndWaitForBrowserPopup(String childWindowTitle, String objName) {
		WebDriver popupWindow = null;
		Set<String> handles = driver.getWindowHandles();
		Report.info("No of windows should be 1 :  " + handles.size());

	   click(objName); 
	   
/*	   int timeCount = 1;
		
		do
		{
		   handles = driver.getWindowHandles();
		   try {
			Thread.sleep(2000);
			} catch (InterruptedException e) {
				Report.info("Could not wait for interuppted time");
				e.printStackTrace();
			}
		   timeCount++;
		   if ( timeCount > getTotalWaitTimeInSecs() ) 
		   {
		       break;
		   }
		}
		while ( handles.size() == 1 );*/
		
		WebDriverWait wait = new WebDriverWait(driver, getTotalWaitTimeInSecs());
		wait.until(ExpectedConditions.numberOfWindowsToBe(2));
		handles = driver.getWindowHandles();
		Report.info("No of windows should be 2 :  " + handles.size());
		for (String s : handles) {
			String windowHandle = s; 
			popupWindow = driver.switchTo().window(windowHandle);
			waitForPageLoad();
			Report.info("Window Title : " + popupWindow.getTitle());
			Report.info("Window Url : " + popupWindow.getCurrentUrl());

			if (popupWindow.getTitle().contains(childWindowTitle)){
				Report.info("Selected Window Title : " + popupWindow.getTitle());
				return popupWindow;
			}
		}
		Report.info("Window Title :" + popupWindow.getTitle());
//		waitForPageLoad();
		return popupWindow;
	}
	
	/**
	 * Used when a new window popup comes up on clicking a button/link/icon in IE
	 * An alternative for getHandleToWindow in IE
	 * @param childWindowTitle Name of the child window to which the windows focus is transferred to 
	 * @param objName object identification information for the link/button/icon which opens up a popup window
	 * @param noOfWindowsBefore total number of windows before a child window popup is clicked
	 * @param noOfWindowsAfter total number of windows after a child window popup appears
	 * @return handler to the new popup window
	 */
	public WebDriver clickAndWaitForBrowserPopup(String childWindowTitle, String objName, int noOfWindowsBefore, int noOfWindowsAfter) {
		WebDriver popupWindow = null;
		Set<String> handles = driver.getWindowHandles();
		Report.info("No of windows should be " + noOfWindowsBefore + " : " + handles.size());

	   click(objName); 
	   
/*	   int timeCount = 1;
		
		do
		{
		   handles = driver.getWindowHandles();
		   try {
			Thread.sleep(2000);
			} catch (InterruptedException e) {
				Report.info("Could not wait for interuppted time");
				e.printStackTrace();
			}
		   timeCount++;
		   if ( timeCount > getTotalWaitTimeInSecs() ) 
		   {
		       break;
		   }
		}
		while ( handles.size() == 1 );*/
		
		WebDriverWait wait = new WebDriverWait(driver, getTotalWaitTimeInSecs());
		wait.until(ExpectedConditions.numberOfWindowsToBe(noOfWindowsAfter));
		handles = driver.getWindowHandles();
		Report.info("No of windows should be "+ noOfWindowsAfter + " :  " + handles.size());
		for (String s : handles) {
			String windowHandle = s; 
			popupWindow = driver.switchTo().window(windowHandle);
			waitForPageLoad();
			Report.info("Window Title : " + popupWindow.getTitle());
			Report.info("Window Url : " + popupWindow.getCurrentUrl());

			if (popupWindow.getTitle().contains(childWindowTitle)){
				Report.info("Selected Window Title : " + popupWindow.getTitle());
				return popupWindow;
			}
		}
		Report.info("Window Title :" + popupWindow.getTitle());
//		waitForPageLoad();
		return popupWindow;
	}
	
	/**
	 * Used when a new window popup comes up on clicking a button/link/icon in IE
	 * An alternative for getHandleToWindow in IE
	 * *** Advised to store the parentWindow handle before calling this method
	 * @param childWindowTitle Name of the child window to which the windows focus is transferred to 
	 * @param objName object identification information for the link/button/icon which opens up a popup window
	 * @param noOfWindowsBefore total number of windows before a child window popup is clicked
	 * @param noOfWindowsAfter total number of windows after a child window popup appears
	 * @return handler to the new popup window
	 */
	public WebDriver clickAndWaitForChildWindow(String childWindowTitle, String objName, int noOfWindowsBefore, int noOfWindowsAfter) {
		WebDriver newChildWindow = null;
		ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
		Report.info("No of windows expected to be " + noOfWindowsBefore + " : and notice as " + windows.size());

	   click(objName); 
	
		WebDriverWait wait = new WebDriverWait(driver, getTotalWaitTimeInSecs());
		wait.until(ExpectedConditions.numberOfWindowsToBe(noOfWindowsAfter));
		windows = new ArrayList<String> (driver.getWindowHandles());
		Report.info("No of windows expected to be "+ noOfWindowsAfter + " : and noticed as " + windows.size());
		newChildWindow = driver.switchTo().window(windows.get(noOfWindowsBefore));
		
		if (newChildWindow.getTitle().contains(childWindowTitle)){
			Report.info("Selected Window Title : " + newChildWindow.getTitle());
		}
		Report.info("Current Window Title :" + newChildWindow.getTitle());
		waitForPageLoad();
		return newChildWindow;
	}
	
	/**
	 * Used when a new window popup comes up on clicking a button/link/icon in IE
	 * An alternative for getHandleToWindow in IE
	 * *** Advised to store the parentWindow handle before calling this method
	 * @param childWindowTitle Name of the child window to which the windows focus is transferred to 
	 * @param objName object identification information for the link/button/icon which opens up a popup window
	 * @param noOfWindowsBefore total number of windows before a child window popup is clicked
	 * @param noOfWindowsAfter total number of windows after a child window popup appears
	 * @return handler to the new popup window
	 */
	public WebDriver clickAndWaitForChildWindow(String childWindowTitle, Set<String> parentWindowHandles, String objName, int noOfWindowsBefore, int noOfWindowsAfter) {
		WebDriver newChildWindowDriver = null;
		Set<String> windows = driver.getWindowHandles();
		Report.info("No of windows expected to be " + noOfWindowsBefore + " : and notice as " + windows.size());

		click(objName); 

		WebDriverWait wait = new WebDriverWait(driver, getTotalWaitTimeInSecs());
		wait.until(ExpectedConditions.numberOfWindowsToBe(noOfWindowsAfter));
		windows = driver.getWindowHandles();
		Report.info("No of windows expected to be "+ noOfWindowsAfter + " : and noticed as " + windows.size());
		//		newChildWindow = driver.switchTo().window(windows.get(noOfWindowsBefore));
		Report.info("Windows of parent windows should be " +  noOfWindowsBefore + " and identified as :" + parentWindowHandles.size() );
		windows.removeAll(parentWindowHandles);
		Report.info("Windows of child windows should be 1 and identified as :" + windows.size() );
		for (String s : windows) {
			String windowHandle = s; 
			newChildWindowDriver = driver.switchTo().window(windowHandle);
			waitForPageLoad();
			if (newChildWindowDriver.getTitle().contains(childWindowTitle)){
				Report.info("Selected Window Title : " + newChildWindowDriver.getTitle());
			break;
			}
		}
		Report.info("Current Window Title :" + newChildWindowDriver.getTitle());
		waitForPageLoad();
		return newChildWindowDriver;
	}
	
	/**
	 * Used when a new window popup comes up on clicking a button/link/icon in IE
	 * An alternative for getHandleToWindow in IE
	 * *** Advised to store the parentWindow handle before calling this method
	 * @param childWindowTitle Name of the child window to which the windows focus is transferred to 
	 * @param objName object identification information for the link/button/icon which opens up a popup window
	 * @param noOfWindowsBefore total number of windows before a child window popup is clicked
	 * @param noOfWindowsAfter total number of windows after a child window popup appears
	 * @return handler to the new popup window
	 */
	public WebDriver clickAndWaitForChildWindow(String childWindowTitle, ArrayList<String> parentWindowHandles, String objName, int noOfWindowsBefore, int noOfWindowsAfter) {
		WebDriver newChildWindowDriver = null;
		ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
		Report.info("No of windows expected to be " + noOfWindowsBefore + " : and notice as " + windows.size());

		if(objName!=null)
		{
			click(objName); 
		}		

		WebDriverWait wait = new WebDriverWait(driver, getTotalWaitTimeInSecs());
		wait.until(ExpectedConditions.numberOfWindowsToBe(noOfWindowsAfter));
		windows = new ArrayList<String>(driver.getWindowHandles());
		Report.info("No of windows expected to be "+ noOfWindowsAfter + " : and noticed as " + windows.size());
		//		newChildWindow = driver.switchTo().window(windows.get(noOfWindowsBefore));
		Report.info("Windows of parent windows should be " +  noOfWindowsBefore + " and identified as :" + parentWindowHandles.size() );
		windows.removeAll(parentWindowHandles);
		Report.info("Windows of child windows should be 1 and identified as :" + windows.size() );
		for (String s : windows) {
			String windowHandle = s; 
			newChildWindowDriver = driver.switchTo().window(windowHandle);
			waitForPageLoad();
			if (newChildWindowDriver.getTitle().contains(childWindowTitle)){
				Report.info("Selected Window Title : " + newChildWindowDriver.getTitle());
			break;
			}
		}
		Report.info("Current Window Title :" + newChildWindowDriver.getTitle());
		waitForPageLoad();
		return newChildWindowDriver;
	}
	
	/**
	 * Used when a new window popup comes up on doubleclicking a button/link/icon in IE
	 * An alternative for getHandleToWindow in IE
	 * *** Advised to store the parentWindow handle before calling this method
	 * @param childWindowTitle Name of the child window to which the windows focus is transferred to 
	 * @param objName object identification information for the link/button/icon which opens up a popup window
	 * @param noOfWindowsBefore total number of windows before a child window popup is doubleclicked
	 * @param noOfWindowsAfter total number of windows after a child window popup appears
	 * @return handler to the new popup window
	 */
	public WebDriver doubleClickAndWaitForChildWindow(String childWindowTitle, ArrayList<String> parentWindowHandles, String objName, int noOfWindowsBefore, int noOfWindowsAfter) {
		WebDriver newChildWindowDriver = null;
		ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
		Report.info("No of windows expected to be " + noOfWindowsBefore + " : and notice as " + windows.size());

		doubleClick(objName); 

		WebDriverWait wait = new WebDriverWait(driver, getTotalWaitTimeInSecs());
		wait.until(ExpectedConditions.numberOfWindowsToBe(noOfWindowsAfter));
		windows = new ArrayList<String>(driver.getWindowHandles());
		Report.info("No of windows expected to be "+ noOfWindowsAfter + " : and noticed as " + windows.size());
		//		newChildWindow = driver.switchTo().window(windows.get(noOfWindowsBefore));
		Report.info("Windows of parent windows should be " +  noOfWindowsBefore + " and identified as :" + parentWindowHandles.size() );
		windows.removeAll(parentWindowHandles);
		Report.info("Windows of child windows should be 1 and identified as :" + windows.size() );
		for (String s : windows) {
			String windowHandle = s; 
			newChildWindowDriver = driver.switchTo().window(windowHandle);
			waitForPageLoad();
			if (newChildWindowDriver.getTitle().contains(childWindowTitle)){
				Report.info("Selected Window Title : " + newChildWindowDriver.getTitle());
			break;
			}
		}
		Report.info("Current Window Title :" + newChildWindowDriver.getTitle());
		waitForPageLoad();
		return newChildWindowDriver;
	}
	
	
	/**
	 * Usually a successor method to {@link ReusableUtilities.SeleniumBaseClass#clickAndWaitForBrowserPopup(String childWindowTitle, String objName)}.
	 * Called when the operations on child windows are done and wanted to navigate back to parent window 
	 * @param childWindowTitle Name of the child window
	 * @param parentWindowTitle name of the parent window
	 * @param noOfWindowsBefore total number of windows before a child window popup is clicked
	 * @param noOfWindowsAfter total number of windows after a child window popup appears
	 * @return handler to parent window
	 */
	public WebDriver closechildWindowAndNavigateToItsParentWindowHandle(WebDriver toCloseChildWindowDriver, String parentWindowHandle, int noOfWindowsBefore, int noOfWindowsAfter){
		WebDriver childWindowDriver = null;
		WebDriver parentWindowDriver = null;
		Set<String> handles = driver.getWindowHandles();
		Report.info("No of windows Noticed :  " + handles.size() + " : Expected window count: " + noOfWindowsBefore);
		boolean isChildWindowClosed = false;
		
		String expectedChildWinHandleToClose = toCloseChildWindowDriver.getWindowHandle();
		String expectedChildWinTitleToClose = toCloseChildWindowDriver.getTitle();
		
		// Closing the child windows
		for (String s : handles) {
			String windowHandle = s; 
			childWindowDriver = driver.switchTo().window(windowHandle);
			
			if (childWindowDriver.getTitle().equals(expectedChildWinTitleToClose) && 
				(!childWindowDriver.getWindowHandle().equals(parentWindowHandle)) && 
					childWindowDriver.getWindowHandle().equals(expectedChildWinHandleToClose) && 
				isChildWindowClosed == false){
				String childWindowTitle = childWindowDriver.getTitle();
				String childWindowHandleDetails = childWindowDriver.getWindowHandle();
				driver.close();
				isChildWindowClosed = true;
				Report.info("Successully closed child window with Title : " + childWindowTitle);
				Report.info("Successully closed child window Handle : " + childWindowHandleDetails + " : Expected window to be closed :" + expectedChildWinHandleToClose);
				break;
			}
		}
		
//		handles = driver.getWindowHandles();
		Report.info("No of windows after closing child window :  " + handles.size() + " : Expected window count: " + noOfWindowsAfter);
		parentWindowDriver = driver.switchTo().window(parentWindowHandle);
		
/*		for (String s : handles) {
			String windowHandle = s; 
			parentWindowDriver = driver.switchTo().window(windowHandle);
			waitForPageLoad();
			Report.info("Window Title : " + parentWindow.getTitle());
			Report.info("Window Url : " + parentWindow.getCurrentUrl());

			if (parentWindowDriver.getWindowHandle().equals(parentWindowHandle)){
				Report.info("Selected Window Title : " + parentWindowDriver.getTitle());
				Report.info("Selected Window Handle : " + parentWindowDriver.getWindowHandle());
//				return parentWindow;
				break;
			}
		}*/
		Report.info("Current Window Title :" + parentWindowDriver.getTitle());
		Report.info("Current Window handle : " + parentWindowDriver.getWindowHandle() + " Expected window handle: " + parentWindowHandle);
		return parentWindowDriver;
		
	}
	
	
	/**
	 * Used when a new window popup comes up on double clicking a button/link/icon in IE
	 * An alternative for getHandleToWindow in IE
	 * @param childWindowTitle Name of the child window to which the windows focus is transferred to 
	 * @param objName object identification information for the link/button/icon which opens up a popup window
	 * @return handler to the new popup window
	 */
	public WebDriver doubleClickAndWaitForBrowserPopup(String childWindowTitle, String objName) {
		WebDriver popupWindow = null;
		Set<String> handles = driver.getWindowHandles();
		Report.info("No of windows should be 1 :  " + handles.size());

	   doubleClick(objName);
	  /* int timeCount = 1;
		
		do
		{
		   handles = driver.getWindowHandles();
		   try {
			Thread.sleep(2000);
			} catch (InterruptedException e) {
				Report.info("Could not wait for interuppted time");
				e.printStackTrace();
			}
		   timeCount++;
		   if ( timeCount > getTotalWaitTimeInSecs() ) 
		   {
		       break;
		   }
		}
		while ( handles.size() == 1 );*/
		
	   WebDriverWait wait = new WebDriverWait(driver, getTotalWaitTimeInSecs());
		wait.until(ExpectedConditions.numberOfWindowsToBe(2));
		handles = driver.getWindowHandles();
		Report.info("No of windows should be 2 :  " + handles.size());

		for (String s : handles) {
			String windowHandle = s; 
			popupWindow = driver.switchTo().window(windowHandle);
			Report.info("Window Title : " + popupWindow.getTitle());
			Report.info("Window Url : " + popupWindow.getCurrentUrl());

			if (popupWindow.getTitle().contains(childWindowTitle)){
				Report.info("Selected Window Title : " + popupWindow.getTitle());
				return popupWindow;
			}
		}
		Report.info("Window Title :" + popupWindow.getTitle());
		waitForPageLoad();
		return popupWindow;
	}
	
	/*
	 * Frames
	 */
	
	/*
	 * Deprecated method, not used in IE
	 */
	public void switchToFrame(final String frameXpath) {
		/*try {
			WebDriverWait wait = new WebDriverWait(driver, getTotalWaitTimeInSecs());
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath(frameXpath)));
			Report.info("Could able to swith to frame");
		} catch (Exception e) {
			e.printStackTrace();
			Report.fail("Could able to swith to frame " + frameXpath);
		}*/
		
		try {
			WebDriverWait wait = new WebDriverWait(driver, getTotalWaitTimeInSecs());
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath(frameXpath)));
			Report.info("Navigated to frame with id " + frameXpath);
		} catch (NoSuchFrameException e) {
			Report.fail("Unable to locate frame with id " + frameXpath
					+ e.getStackTrace());
		} catch (Exception e) {
			Report.fail("Unable to navigate to frame with id " + frameXpath
					+ e.getStackTrace());
		}
//		driver.switchTo().frame(driver.findElement(By.xpath(frameXpath)));
	}
	
	/*
	 * Deprecated method, not used in IE
	 */
	public void switchToDefaultContentFrame() {
		try {
			driver.switchTo().defaultContent();
			Report.info("Navigated back to webpage from frame");
		} catch (Exception e) {
			Report.fail("unable to navigate back to main webpage from frame"
							+ e.getStackTrace());
		}
	}
	
	/**
	 * 
	 */
	public void enterTextInTinyMCEInIE(){
		/*JavascriptExecutor js = (JavascriptExecutor) driver;
		String returnVal = js.executeScript("tinyMCE.activeEditor.setContent('<p>Native API text</p> TinyMCE')").toString();
		 Report.info("JS: Ready State: " + returnVal);*/
		((JavascriptExecutor)driver).executeScript("tinyMCE.activeEditor.setContent('<p>Native API text</p> TinyMCE')");
	}
	
	/**
	 * Insert text into all tinyMCE editors in a page which are writable
	 * @param text text to be inserted into tinyMCE editor
	 */
	public void insertIntoAllTinyMCEinPage(String text){
		
		List<WebElement> tinyMCE = getTinyMCEInPage();
		Report.info("No of tinyMCE in page is: " + tinyMCE.size());

		for (WebElement tiny: tinyMCE ) {
			String tinyMCEId = tiny.getAttribute("id");
			if(!tinyMCEId.contains("readOnly")){
				String tinyId = tinyMCEId.replaceAll("_ifr","");
				Report.info("TinyMCE id " + tinyId);
				enterTextInTinyMCE(tinyId, text);
			}
		}
		
	}
	
	/**
	 * Get all TinyMCEs in page
	 * @return all iframe WebElements in a webpage
	 */
	public List<WebElement> getTinyMCEInPage(){
		return explicitlyWaitAndGetWebElements("frame_Action_FastAction_QueueSubmittedSubmissionText", getTotalWaitTimeInSecs());
	}
	
	/**
	 * Enter text into TinyMCE editor, usually the id is the same as hidden corresponding textarea id of a iframe
	 * @param textAreaId textArea id
	 * @param textToInsert text to insert in tinyMCE editor
	 */
	public void enterTextInTinyMCE(String textAreaId, String textToInsert){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("tinymce.getInstanceById('" + textAreaId + "').setContent('" + textToInsert + "')");
	}
	
	/**
	 * Get text from tinyMCE editor and remove starting <p> tag and ending </p> tag
	 * For Dubgging - DON'T NOT SWITCH TO FIREFOX BROWSER
	 * @param textAreaId texArea id
	 * @return text contained in TinyMCE editor/ textarea field
	 * @throws Exception NoSuchElement exception
	 */
	public String getTinyMCEText(String textAreaId) throws Exception{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement element = driver.findElement(map.getLocator(textAreaId));
		String tinyMCEId = element.getAttribute("id");
		String textInserted = null;
		Report.info("Text id is - Textarea id:" + tinyMCEId);
		String script = "return tinymce.getInstanceById('" + tinyMCEId + "').getContent()";
								//tinyMCE.getInstanceById('create-2506-htmlText').getContent()
//		String script = "tinyMCE.activeEditor.getContent()";
		textInserted =  (String) js.executeScript(script);
		// remove the starting <p> tag and ending </p> tag
		textInserted = textInserted.replaceAll("\\<[^>]*>","");
		return textInserted;
	}

	/*
	 * Hovering over Menus
	 */
	/**
	 * 
	 * @param hoverElement
	 */
	
	public void mouseHoverJScript(WebElement hoverElement) {
		
		try {
			//if (isElementPresent(hoverElement)) {
				String mouseOverScript = "if(document.createEvent){"
											+ "var evObj = document.createEvent('MouseEvents');"
											+ "evObj.initEvent('mouseover',	true, false); "
											+ "arguments[0].dispatchEvent(evObj);"
										+ "} else if(document.createEventObject) { "
											+ "	arguments[0].fireEvent('onmouseover');"
										+ "}";
				((JavascriptExecutor) driver).executeScript(mouseOverScript, hoverElement);
				Report.pass("Successfully hovered on Element :" + hoverElement);
		//	} else {
			//	Report.fail("Element was not visible to hover " + "\n");
			//}
		} catch (StaleElementReferenceException e) {
			Report.fail("Element with " + hoverElement + "is not attached to the page document" + e.getStackTrace());
		} catch (NoSuchElementException e) {
			Report.fail("Element " + hoverElement + " was not found in DOM" + e.getStackTrace());
		} catch (Exception e) {
			e.printStackTrace();
			Report.fail("Error occurred while hovering" + e.getStackTrace());
		}
	}
	

	/**
	 * Click an element using Javascript
	 * @param element WebElement information 
	 * @throws Exception
	 */
	public void clickElementUsingJS(WebElement element) throws Exception {
		try {
			if (isElementPresent(element)) {
				Report.pass("Clicking on element with using java script click");

				((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
			} else {
				Report.fail("Unable to click on element either element not enabled or not displayed");
			}
		} catch (StaleElementReferenceException e) {
			Report.fail("Element is not attached to the page document "+ e.getStackTrace());
		} catch (NoSuchElementException e) {
			Report.fail("Element was not found in DOM "+ e.getStackTrace());
		} catch (Exception e) {
			Report.fail("Unable to click on element "+ e.getStackTrace());
		}
	}
	
	/**
	 * Click an element using Javascript
	 * @param element WebElement information 
	 * @throws Exception
	 */
	public void clickElementUsingJS(String elementIdenfier) throws Exception {
		WebElement element = explicitlyWaitAndGetWebElement(elementIdenfier, getTotalWaitTimeInSecs());
		
		try {
			if (isElementPresent(element) && element.isDisplayed() && element.isEnabled()) {
				Report.pass("Clicking on element with using java script click");

				((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
			} else if(isElementPresent(element) && !(element.isDisplayed())){
				clickElementUsingJS(elementIdenfier);
			} else {
				Report.fail("Unable to click on element either element not enabled or not displayed");
			}
		} catch (StaleElementReferenceException e) {
			Report.fail("Element is not attached to the page document "+ e.getStackTrace());
		} catch (NoSuchElementException e) {
			Report.fail("Element was not found in DOM "+ e.getStackTrace());
		} catch (Exception e) {
			Report.fail("Unable to click on element "+ e.getStackTrace());
		}
	}
	
	
	/*
	 * Expand or collapse Sections 
	 */
	/**
	 * Expand a section within a webpage
	 * @param obj object identification information of the section
	 */
	public void expandSection(String obj){
		WebElement element = explicitlyWaitAndGetWebElement(obj, getTotalWaitTimeInSecs());
		String collapseOrExpand = element.getAttribute("class");
		
		if(collapseOrExpand.contains("collapsed")){
			element.click();
			Report.info("Section " + obj + " Successfully expanded");
		} else{
			Report.info("Section " + obj + " already in expanded state");
		}
		
	}
	
	/**
	 * Collapse a section in webpage
	 * @param obj object identification information of the section
	 */
	public void collapseSection(String obj){
		WebElement element = explicitlyWaitAndGetWebElement(obj, getTotalWaitTimeInSecs());
		String collapseOrExpand = element.getAttribute("class");
		
		if(collapseOrExpand.contains("expanded")){
			element.click();
			Report.info("Section " + obj + " Successfully collapsed");
		} else {
			Report.info("Section " + obj + " already in collapsed state");
		}
	}

	/**
	 * Verifying cursor focus on a specific element is present or not
	 * @param objName object identification information of an element
	 */
	public void verifyCurrentCursorFocus(String objName) {

		try {

			WebElement obj = driver.findElement(map.getLocator(objName));
			WebElement element = driver.switchTo().activeElement();
			softAssert.assertEquals(obj.getText(), element.getText());
			if (obj.equals(element)) {
				Report.info("Current cursor focus is  in '" + objName
						+ "' element");
			} else {
				softAssertCount++;
				Report.warn("Current cursor focus is Not in '" + objName
						+ "' element");
			}

		} catch (Exception e) {
			Report.fail("Unable to find on element '" + objName + "' " + e.getStackTrace());
		}
	}

	/**
	 * Open new browser instance and get focus of that window
	 * Used for navigating to menu items which opens popup window; eg - View Action in popup
	 */
	public void openNewWindowAndGetHandle() {

		try {
			baseWindow = driver.getWindowHandle();
			((JavascriptExecutor) driver).executeScript("window.open();");
			Set<String> allWindow = driver.getWindowHandles();
			String windowHandle = ((String) allWindow.toArray()[1]);
			driver.switchTo().window(windowHandle);
			Report.info("Opening New Window and Swich To window");

		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	public void closeWindowAndGetHandle(){
		driver.close();
		driver.switchTo().window(baseWindow);
		Report.info("Closing window and and Swich To window");
	}
	
	public WebDriver openNewWindowAndGetHandleWithChildWebDriver(WebDriver driver) {

		try {

			((JavascriptExecutor) driver).executeScript("window.open();");
			Set<String> allWindow = driver.getWindowHandles();
			String windowHandle = ((String) allWindow.toArray()[1]);
			WebDriver winDriver = driver.switchTo().window(windowHandle);
			winDriver.manage().window().maximize();
			Report.info("Opening New Window and Swich To window");
			return winDriver;

		} catch (Exception e) {

			e.printStackTrace();
		}
		return driver;
	}
	
	public void pressReturnKey(String objName)
	{
		try
		{
			WebElement e = fluentWaitAndGetWebElement(getTotalWaitTimeInSecs(), getPoolWaitTime(), objName);
			e.sendKeys(Keys.RETURN);
//			driver.findElement(map.getLocator(objname)).clear();
//			driver.findElement(map.getLocator(objname)).sendKeys(value);
			Report.pass("return key is hit in '"+objName+"'");
		}
		catch(Exception e)
		{
			Report.fail("element '"+objName+"' is not displayed");
			Assert.fail("element '"+objName+"' is not displayed");
		}
	}
	
		
	/**
	 * Tabs
	 */
	
	/**
	 * Verifies if a tab is active in application, by passing the object identification information 
	 * @param objname pass object identification information
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyTabActive(String objname) {
		try {
			
/*			@SuppressWarnings("rawtypes")
			Wait wait = new FluentWait(driver).withTimeout(getTotalWaitTimeInSecs(), TimeUnit.SECONDS)
					.pollingEvery(getPoolWaitTime(), TimeUnit.SECONDS);
				wait.until(ExpectedConditions.visibilityOfElementLocated(map.getLocator(objname)));
				WebElement element = fluentWaitAndGetWebElement(getTotalWaitTimeInSecs(), getPoolWaitTime(), objname);*/
				WebElement element = explicitlyWaitAndGetWebElement(objname, getTotalWaitTimeInSecs());
				WebElement parentLiElement = getToLiElement(element);
			if(isElementPresent(objname) && parentLiElement.getAttribute("class").contains("ui-state-active")){
					Report.pass("Tab " + objname + " Enabled");
				return true;
			} else{
				Report.fail("Tab " + objname + " NOT Enabled");
				Assert.fail("Tab " + objname + " NOT Enabled");
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public WebElement getToLiElement(WebElement element){
		if(element.getTagName().equals("span")){
			WebElement a =  getParentElement(element);
			Report.info("Current Element is " + element.getTagName());
			return getParentElement(a);
		} else{
			if (element.getTagName().equals("a")){
				Report.info("Current Element is " + element.getTagName());
				return getParentElement(element);
			} else {
				Report.info("Current Element is " + element.getTagName());
				return element;
			}
		}
	}

	public WebElement getParentElement(WebElement element) {
		WebElement parentElement = element.findElement(By.xpath("./.."));
		Report.info("Navigated to parent elemement '" + parentElement.getTagName() + "'");
		return parentElement;
	}
	
	public boolean verifyAssociationTabEnabled(){
		return verifyTabActive("tab_Association");
	}

	public boolean verifyAttachmentTabEnabled(){
		return verifyTabActive("tab_Attachment");
	}
	
	/**
	 * The method is called when an entity urn is inserted in URN field when establishing an Assocation Link type, 
	 * and wanted to load the Usage dropdown based on the URN entity type
	 */
	public void waitForLinkTypeUsageDropdownLoad(){
		JavascriptExecutor jsExecute = (JavascriptExecutor) driver;
		jsExecute.executeScript("softEntity.updateUsageUrl(softEntityJQuery('#getAssociationUsagesUrl').val(), true)");
	}
	
	/**
	 * checkbox specific methods
	 * @throws Exception 
	 */
	public boolean isCheckBoxChecked(String objName) throws Exception {
		WebElement element = explicitlyWaitAndGetWebElement(objName, getTotalWaitTimeInSecs());
		if (isElementPresent(objName) && 
				element.getAttribute("type").contains("checkbox") && 
				isAttribtuePresent(objName, "checked")) {
			Report.info("Checkbox '" + objName + "' is checked now");
			return true;
		}
		Report.info("Checkbox '" + objName + "' is not checked");
		return false;
	}
	
	public boolean isCheckBoxUnChecked(String objName) throws Exception{
		WebElement element = explicitlyWaitAndGetWebElement(objName, getTotalWaitTimeInSecs());
		String elementTypeAttVal = null;
		try {
			elementTypeAttVal = element.getAttribute("type");
		} catch (StaleElementReferenceException e) {
			element = explicitlyWaitAndGetWebElement(objName, getTotalWaitTimeInSecs());
			elementTypeAttVal = element.getAttribute("type");
		}
		catch (Exception e) {
			Report.warn("The attribute value for element could not be found.");
		}
		if (isElementPresent(objName) && 
				elementTypeAttVal.contains("checkbox") && 
				!(isAttribtuePresent(objName, "checked"))) {
			Report.info("Checkbox '" + objName + "' is not checked now");
			return true;
		}
		Report.info("Checkbox '" + objName + "' is checked");
		return false;
	}
	
	public void checkCheckBox(String objName) throws Exception{
		if(isCheckBoxUnChecked(objName)){
			click(objName);
			Report.info("Checkbox '" + objName + "' is marked as checked");
		} else {
			Report.info("Checkbox '" + objName + "' is already checked");
		}
	}

	public void uncheckCheBox(String objName) throws Exception{
		if(isCheckBoxChecked(objName)){
			click(objName);
			Report.info("Checkbox '" + objName + "' is marked as un-checked");
		} else {
			Report.info("Checkbox '" + objName + "' is already un-checked");
		}
	}
	
	public String getUserName(){
		return readConfigFile("username");
	}
	
	public String getPassword(){
		return readConfigFile("password");
	}
	

	public void insertTextInAllocatedOfficerTextField(String allocationOfficerIdentifierInfo, String textToInsert) {
		WebElement textField = explicitlyWaitAndGetWebElement(allocationOfficerIdentifierInfo, getTotalWaitTimeInSecs());
		String textFieldId = textField.getAttribute("id");
		Report.info("Id value for text field" + textFieldId);
		String jsCode = "var el = document.getElementById(\'" + textFieldId + "\'); el.value = \'" + textToInsert + "\'; el.focus(); var evt = document.createEvent('Event'); evt.initEvent('keydown', false, true); el.dispatchEvent(evt);";
		Report.info("Java script code constructed is:" + jsCode);
		JavascriptExecutor myExecutor = ((JavascriptExecutor) driver);
		myExecutor.executeScript(jsCode);
		Report.pass("Officer name successfully inserted using java script in Alloated officer textfield");
	}
	
	// verifying element are sorted in table column
	// passing objname should be column xpath
	public void verifySortTableColumn(String objName) throws Exception {
		try {
			ArrayList<String> obtainedList = new ArrayList<>();

			List<WebElement> elementList = driver.findElements(map
					.getLocator(objName));
			for (WebElement we : elementList) {
				System.out.println(we.getText());
				obtainedList.add(we.getText().toUpperCase());
			}

			ArrayList<String> sortedList = new ArrayList<>();
			for (String s : obtainedList) {
				sortedList.add(s);
			}
			Collections.sort(sortedList);
			// Assert.assertTrue(sortedList.equals(obtainedList));
			if (sortedList.equals(obtainedList)) {
				Report.info("Table Colummn  is sorted in ascending order");
			} else {
				softAssertCount++;
				Report.info("Table Colummn  is Not sorted ");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
