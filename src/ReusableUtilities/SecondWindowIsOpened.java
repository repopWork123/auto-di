package ReusableUtilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;


/**
 * Implementation of ExpectedCondition that waits until a second window is opened
 * Useful when you need to wait for a pop-up window 
 * @author cliff.darling
 *
 */
public class SecondWindowIsOpened implements ExpectedCondition<Boolean> {

	/**
	 * Default Constructor
	 */
	public SecondWindowIsOpened(){

	}
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	public Boolean apply(WebDriver input) {
		return input.getWindowHandles().size()>1;
	}

}