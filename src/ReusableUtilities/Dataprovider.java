package ReusableUtilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

public class Dataprovider {	
	
	public static String[][] readExcelData(String sheetName, String filePath, String tableName) {
		  
		String[][] testData = null;
		 
	  try {
		   XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(filePath));
		   XSSFSheet sheet = workbook.getSheet(sheetName);	   
		   XSSFCell[] boundaryCells = findCell(sheet, tableName);
		   XSSFCell startCell = boundaryCells[0];
		   XSSFCell endCell = boundaryCells[1];
		   int startRow = startCell.getRowIndex() + 1;
		   int endRow = endCell.getRowIndex() - 1;
		   int startCol = startCell.getColumnIndex() + 1;
		   int endCol = endCell.getColumnIndex() - 1;
		 
		   testData = new String[endRow - startRow + 1][endCol - startCol + 1];
		 
		   for (int i = startRow; i < endRow + 1; i++)
		   {
			    for (int j = startCol; j < endCol + 1; j++)
			    {
			     testData[i - startRow][j - startCol] = sheet.getRow(i).getCell(j).getStringCellValue();
			 
			    }
		   }
		 
		  } catch (FileNotFoundException e) {
		   System.out.println("Could not read the Excel sheet");
		   e.printStackTrace();
		  } catch (IOException e) {
		   System.out.println("Could not read the Excel sheet");
		   e.printStackTrace();
		  }
		 
		  return testData;
		 
		 }
	
	public static XSSFCell[] findCell(XSSFSheet sheet, String text) {
		 
		  String pos = "start";
		 
		  XSSFCell[] cells = new XSSFCell[2];
		 
		  for (Row row : sheet) {
		   for (Cell cell : row) {
		    if (text.equals(cell.getStringCellValue())) {
		     if (pos.equalsIgnoreCase("start")) {
		      cells[0] = (XSSFCell)cell;
		      pos = "end";
		     } else {
		      cells[1] = (XSSFCell) cell;
		     }
		    }
		 
		   }
		  }
		  return cells;
		 }
	
	@DataProvider(name = "Login")
	 public Object[][] Dataprovider() throws IOException {		

		Properties props=new Properties();
		FileInputStream fis=new FileInputStream("D:\\PROJECTS\\Holmes\\Uleaf_2_Automation_Selenium\\"
				+ "Uleaf_Selenium\\UleafSeleniumPOC\\"
				+ "src\\uleafPOC\\OR.properties");
		props.load(fis);		
	    Object[][] testData = readExcelData(props.getProperty("sheetName"),
	    props.getProperty("excel.path"),
	    props.getProperty("table.name"));
	    
	    return testData;
	 
	 }	

//	public static void main(String[] args) throws IOException
//	{
//		
//		ExcelUtility eu=new ExcelUtility();
//		
//		Object[][] obt;
//		
//		obt = eu.dataProvider();
//		
//		System.out.println(obt.length);
//		System.out.println(obt[0].length);
//
//		for(int i=0; i<obt.length;i++)
//		{
//			for(int j=0;j<obt[0].length;j++)
//			{
//				System.out.print(obt[i][j]);
//				System.out.print("  ");
//			}
//			
//			System.out.println("\n");
//		}
//		
//	}
}


