package ReusableUtilities;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class Report {
	
	
		public static void pass(String message){
			
			System.out.println("PASS: "+message);
			//Reporter.log("Pass: "+message);
			ATUReports.add(message, LogAs.PASSED, null);
		}
		
		public static void fail(String message){
			
			System.out.println("FAIL: "+message);
			//Reporter.log("Fail: "+message);
			ATUReports.add(message, LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		
		public static void warn(String message){
	
			System.out.println("Warn: "+message);
			//Reporter.log("Fail: "+message);
			ATUReports.add(message, LogAs.WARNING, new CaptureScreen(ScreenshotOf.DESKTOP));
		}		
		
		
		public static void info(String message){
			
			System.out.println("Info: "+message);
			//Reporter.log("Info: "+message);
			ATUReports.add(message, true);
		}

}
